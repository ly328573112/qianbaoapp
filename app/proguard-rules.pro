# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/jay/Tools/android-sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keepattributes Signature
-keepattributes SourceFile,LineNumberTable
# -ignorewarnings

# support
-keep class android.support.**{*;}

# jayfeng
-dontwarn com.jayfeng.lesscode.**

# ProGuard configurations for squareup
-keep class retrofit2.** { *; }
-keep class com.squareup.** { *; }
-keep class okhttp3.** { *; }
-keep class okio.** { *; }
-dontwarn retrofit2.**
-dontwarn com.squareup.**
-dontwarn okhttp3.**
-dontwarn okio.**

# EventBus
-keepattributes *Annotation*
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

# GreenDao
-keepclassmembers class * extends org.greenrobot.greendao.AbstractDao {
    public static java.lang.String TABLENAME;
}
-keep class **$Properties

# If you do not use SQLCipher:
-dontwarn org.greenrobot.greendao.database.**
# If you do not use Rx:
-dontwarn rx.**

# Only required if you use AsyncExecutor
-keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(Java.lang.Throwable);
}

# Bugly
-dontwarn com.tencent.bugly.**
-keep public class com.tencent.bugly.**{*;}

## Wechat
-keep class com.tencent.**{*;}
-dontwarn com.tencent.**
-keep class com.baidu.**{*;}
-dontwarn com.baidu.**
-keep class com.sina.**{*;}
-dontwarn com.sina.**
-keep class com.alipay.**{*;}
-dontwarn com.alipay.**
-keep class com.xiaomi.**{*;}
-dontwarn com.xiaomi.**
-keep class com.bumptech.**{*;}
-dontwarn com.bumptech.**

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class * extends io.dcloud.DHInterface.IPlugin
-keep public class * extends io.dcloud.DHInterface.IFeature
-keep public class * extends io.dcloud.DHInterface.IBoot
-keep public class * extends io.dcloud.DHInterface.IReflectAble
-keep public class * extends io.dcloud.js.geolocation.IGeoManager

# Talking umengAnalytics
-keepclassmembers class * {
   public <init> (org.json.JSONObject);
}

# JPush
-dontoptimize
-dontpreverify
-dontwarn cn.jpush.**
-keep class cn.jpush.** { *; }
-dontwarn cn.jiguang.**
-keep class cn.jiguang.** { *; }
-dontwarn com.google.**
-keep class com.google.gson.** {*;}
-keep class com.google.protobuf.** {*;}

# com.google.zxing
-dontwarn com.google.zxing.**
-keep public class com.google.zxing.**{*;}

# AMap
-keep public class com.amap.api.**{*;}
-dontwarn com.amap.api.**
-keep public class com.autonavi.amap.mapcore2d.**{*;}
-dontwarn com.autonavi.amap.mapcore2d.**
-keep public class com.autonavi.aps.amapapi.model.**{*;}
-dontwarn com.autonavi.aps.amapapi.model.**
-keep public class com.loc.**{*;}
-dontwarn com.loc.**
-keep public class com.amap.api.services.**{*;}
-dontwarn com.amap.api.services.**
-keep   class com.amap.api.maps.**{*;}
-keep   class com.autonavi.**{*;}
-keep   class com.amap.api.trace.**{*;}

# glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

# banner
-keep class com.youth.banner.** {
    *;
}

# countly
-dontwarn com.countly.android.sdk.**
-keep class com.countly.android.sdk.**{*;}

# persistentcookiejar
-dontwarn com.franmontiel.persistentcookiejar.**
-keep class com.franmontiel.persistentcookiejar.**
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    !static !transient <fields>;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

# -keep public class [您的应用包名].R$*{
# public static final int *;
# }

# Renderscript support
-keepclasseswithmembernames class * {
    native <methods>;
}
-keep class android.support.v8.renderscript.** { *; }

## mta
-keep class com.qq.**{*;}
-dontwarn com.qq.**
-keep class lc.com.xinge.visual.**{*;}
-dontwarn lc.com.xinge.visual.**
-keep class org.apache.thrift.**{*;}
-dontwarn org.apache.thrift.**
-keep class org.slf4j.**{*;}
-dontwarn org.slf4j.**

# BHH
-keep class exocr.**{*;}
-dontwarn exocr.**
-keep class com.fasterxml.jackson.**{*;}
-dontwarn com.fasterxml.jackson.**
-keep class com.minivision.**{*;}
-dontwarn com.minivision.**
-keep class org.apache.**{*;}
-dontwarn org.apache.**
-keep class org.codehaus.stax2.**{*;}
-dontwarn org.codehaus.stax2.**
-keep class org.opencv.engine.**{*;}
-dontwarn org.opencv.engine.**
-keep class today.youcanbe.** {*;}
-dontwarn today.youcanbe.**
-keep class com.qianbao.mobilecashier.** {*;}
-dontwarn com.qianbao.mobilecashier.**
-keep class com.qianbao.volley.** {*;}
-dontwarn com.qianbao.volley.**
-keep class com.ta.utdid2.** {*;}
-dontwarn com.ta.utdid2.**
-keep class com.ut.device.** {*;}
-dontwarn com.ut.device.**
-keep class org.json.alipay.** {*;}
-dontwarn org.json.alipay.**
-keep class com.qianbao.shiningwhitelibrary.** {*;}
-dontwarn com.qianbao.shiningwhitelibrary.**
-keep class com.qianbao.android.logger.** {*;}
-dontwarn com.qianbao.android.logger.**
-keep class ly.count.android.sdk.** {*;}
-dontwarn ly.count.android.sdk.**
-keep class org.openudid.** {*;}
-dontwarn org.openudid.**
-keep class com.sensetime.** {*;}
-dontwarn com.sensetime.**



# rxjava1.0混淆
-dontwarn sun.misc.**

-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
   long producerIndex;
   long consumerIndex;
}

-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode producerNode;
}

-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueConsumerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode consumerNode;
}

-dontnote rx.internal.util.PlatformDependent

#-dontwarn com.sensetime.**
#-keep class com.sensetime.** { *; }
#-keepclasseswithmembernames class * {
#    native <methods>;
#}
#
#-keepclasseswithmembers class * {
#    public <init>(android.content.Context, android.util.AttributeSet);
#}
#
#-keepclasseswithmembers class * {
#    public <init>(android.content.Context, android.util.AttributeSet, int);
#}
#
#-keepclassmembers class * extends android.app.Activity {
#   public void *(android.view.View);
#   public void *(android.os.Bundle);
#}
#
#-keepclassmembers enum * {
#    public static **[] values();
#    public static ** valueOf(java.lang.String);
#}
#
#-keep class * implements android.os.Parcelable {
#  public static final android.os.Parcelable$Creator *;
#}
#
##-keep class exocr.**
##-keepclassmembers class exocr.** {
##   *;
##}
#
-keep class **.R$* {
*;
}

#-keepattributes Exceptions,InnerClasses,Signature,Deprecated,SourceFile,LineNumberTable,*Annotation*,Synthetic,EnclosingMethod,JavascriptInterface
#
### Good practice so that you don't end up logging sensitive info.
## Remove debug, verbose, and info Log calls
#-assumenosideeffects class android.util.Log {
#    public static *** d(...);
#    public static *** v(...);
#    public static *** i(...);
#    ## Uncomment to remove warnings and errors as well
#    # public static *** w(...);
#    # public static *** e(...);
#}