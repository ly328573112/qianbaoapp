package com.haodaibao.fanbeiapp.fileuploading;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.baseandroid.config.Constant;
import com.baseandroid.config.Global;
import com.baseandroid.utils.FileUtils;
import com.baseandroid.utils.LogUtil;
import com.baseandroid.utils.NetworkTool;
import com.google.gson.Gson;
import com.haodaibao.fanbeiapp.repository.ConfigRepository;
import com.haodaibao.fanbeiapp.repository.json.PhotoInfo;
import com.haodaibao.fanbeiapp.repository.json.UploadResult;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

import static com.baseandroid.config.Constant.PACKAGE_STATUS;


/**
 * 开发者：LuoYi
 * Time: 2017 15:49 2017/7/13 07
 */
public class FileUploaderManager {

    private UploaderResultListener uploaderResult;
    private List<UploadResult.UploadInfo> uploadFileList = new ArrayList<>();
    private List<PhotoInfo>               photoInfoList  = new ArrayList<>();
    private List<PhotoInfo>               uriList        = new ArrayList<>();
    private List<String>                  failUrlList    = new ArrayList<>();
    private int                           processNumer   = 0;
    private Observable<ResponseBody>[] observables;

    private static FileUploaderManager instance;

    public static FileUploaderManager getInstance() {
        if (instance == null) {
            instance = new FileUploaderManager();
        }
        return instance;
    }

    public void updateImage(List<PhotoInfo> imageUriList, UploaderResultListener uploaderResult) {
        this.uriList.clear();
        this.uriList.addAll(imageUriList);
        this.uploaderResult = uploaderResult;
        int netWorkType = NetworkTool.getInstance(Global.getContext()).checkNetWorkType();
        if (netWorkType == NetworkTool.NONETWORK) {
            uploaderResult.onNetworkType(false);
            return;
        }
        // 去掉加号图片
        for (PhotoInfo photoInfo : uriList) {
            if (photoInfo.getPhotoNumber() == 1) {
                uriList.remove(photoInfo);
            }
        }
        if (uploadFileList == null) {
            uploadFileList = new ArrayList<>();
        } else {
            uploadFileList.clear();
        }
        if (photoInfoList == null) {
            photoInfoList = new ArrayList<>();
        } else {
            if (photoInfoList.size() != 0) {
                photoInfoList.clear();
            }
        }
        processNumer = 0;
        uploaderResult.onUploadProcessNumber(processNumer, uriList.size());
        /**
         * 图片压缩
         */
        new BitmapThreedTask().new LoadlocalBitmapThread(uriList, handler).start();
    }

    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            // 可能有反复调用相册的情况，所以需要多重迭代增加
            @SuppressWarnings("unchecked")
            ArrayList<File> callBacklist = (ArrayList<File>) msg.obj;
            for (int i = 0; i < callBacklist.size(); i++) {
                //                LogUtil.e("压缩后图片大小：" + BitmapThreedTask.formatFileSize(callBacklist.get(i)));
                PhotoInfo photoInfo = new PhotoInfo(0, callBacklist.get(i).toString(), false);
                photoInfoList.add(photoInfo);
            }
            uploaderResult(photoInfoList);
        }
    };

    /**
     * 上传图片
     */
    private void uploaderResult(List<PhotoInfo> photoInfoList) {
        int netWorkType = NetworkTool.getInstance(Global.getContext()).checkNetWorkType();
        if (netWorkType == NetworkTool.NONETWORK) {
            uploaderResult.onNetworkType(false);
            return;
        }
        if (observables != null) {
            observables = null;
        }
        observables = new Observable[photoInfoList.size()];
        for (int i = 0; i < photoInfoList.size(); i++) {
            String imageUrl = photoInfoList.get(i).getPhotoUri();
            if (!judgeFileExist(imageUrl)) {
                break;
            }
            observables[i] = ConfigRepository.getInstance().uploadFileWithPartMap(new HashMap<String, String>(), "file", imageUrl);
        }
        Observable.concatArray(observables)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getObderverResult(photoInfoList.size()));
    }

    /**
     * 图片上传（String“二次上传”）
     *
     * @param imageList
     */
    public void uploaderStringFile(List<String> imageList, int successNumber) {
        int netWorkType = NetworkTool.getInstance(Global.getContext()).checkNetWorkType();
        if (netWorkType == NetworkTool.NONETWORK) {
            uploaderResult.onNetworkType(true);
            return;
        }
        processNumer = (successNumber - 1);
        observables = null;
        observables = new Observable[imageList.size()];
        for (int i = 0; i <= imageList.size(); i++) {
            String imageUrl = imageList.get(i);
            if (!judgeFileExist(imageUrl)) {
                break;
            }
            observables[i] = ConfigRepository.getInstance().uploadFileWithPartMap(new HashMap<String, String>(), "file", imageUrl);
        }
        Observable.concatArray(observables)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getObderverResult(imageList.size()));
    }

    private boolean judgeFileExist(String imageUrl) {
        if (TextUtils.isEmpty(imageUrl)) {
            failUrlList.add(imageUrl);
            return false;
        }
        try {
            File file = new File(imageUrl);
            if (file.exists()) {
                return true;
            }
            failUrlList.add(imageUrl);
        } catch (Exception e) {
            e.printStackTrace();
            failUrlList.add(imageUrl);
            return false;
        }
        return false;
    }

    private Observer<ResponseBody> getObderverResult(final int photoSize) {
        return new Observer<ResponseBody>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull ResponseBody responseBody) {
                try {
                    String repone = responseBody.string();
                    Gson gson = new Gson();
                    UploadResult uploadResult = gson.fromJson(repone, UploadResult.class);
                    UploadResult.UploadInfo uploadFile = uploadResult.getUploadFile();
                    if (TextUtils.equals(uploadResult.getStatus(), Constant.SUCCESS_CODE) || TextUtils.equals(uploadResult.getResultCode(), PACKAGE_STATUS)) {
                        // 文件上传成功
                        uploaderResult.onUploadProcessNumber(processNumer++, photoSize);
                        if (!TextUtils.isEmpty(uploadFile.getFileUrl())) {
                            uploadFileList.add(uploadFile);
                        }
                    } else {
                        failUrlList.add(uploadFile.getFileUrl());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                uploaderResult.onUploaderFail(null, 0);
            }

            @Override
            public void onComplete() {
                try {
                    if (failUrlList != null && failUrlList.size() > 0) {
                        uploaderResult.onUploaderFail(failUrlList, photoInfoList.size() - failUrlList.size());
                    } else {
                        for (PhotoInfo photoInfo : photoInfoList) {
                            FileUtils.deleteImage(photoInfo.getPhotoUri(), BitmapThreedTask.FILENAME);
                        }
                        photoInfoList.clear();
                        uploaderResult.onUploaderSuccess(uploadFileList);
                    }
                } catch (Exception e) {
                    LogUtil.e(e);
                }
            }
        };
    }

    public void cleanFileList() {
        uploadFileList.clear();
        photoInfoList.clear();
        failUrlList.clear();
        uriList.clear();
        processNumer = 0;
    }

    public interface UploaderResultListener {

        void onNetworkType(boolean cleanList);

        void onUploadProcessNumber(int successNumber, int fileListSize);

        void onUploaderFail(List<String> failUrl, int successNumber);

        void onUploaderSuccess(List<UploadResult.UploadInfo> uploadFileList);

    }

}
