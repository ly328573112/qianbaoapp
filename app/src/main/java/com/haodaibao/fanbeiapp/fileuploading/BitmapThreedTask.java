package com.haodaibao.fanbeiapp.fileuploading;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;

import com.baseandroid.utils.FileUtils;
import com.baseandroid.utils.LogUtil;
import com.haodaibao.fanbeiapp.repository.json.PhotoInfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class BitmapThreedTask {

    public static String FILEPATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
    public static String FILENAME = "QianbaoEvaluate_";
    public static String SUFFIX = ".png";

    public List<File> nameList = new ArrayList<File>();
    // 开始的倒数锁
    CountDownLatch begin = new CountDownLatch(1);
    // 结束的倒数锁
    CountDownLatch end = new CountDownLatch(10);
    // 线程集
    ExecutorService exec;

    public class LoadlocalBitmapThread extends Thread {
        private List<PhotoInfo> pathList;
        private Handler handler;

        public LoadlocalBitmapThread(List<PhotoInfo> pathList, Handler handler) {
            this.pathList = pathList;
            this.handler = handler;
        }

        @Override
        public void run() {
            begin = new CountDownLatch(1);
            end = new CountDownLatch(pathList.size());
            exec = Executors.newSingleThreadExecutor();

            for (int i = 0; i < pathList.size(); i++) {
                exec.execute(new BitmapCacheThread(pathList.get(i).getPhotoUri()));
            }
            begin.countDown();
            try {
                end.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            exec.shutdown();
            LogUtil.d("Game Over" + nameList.size() + "");
            Message msg = new Message();
            msg.obj = nameList;
            handler.sendMessage(msg);
        }

    }

    public class BitmapCacheThread extends Thread {
        private Bitmap bit;
        private String path;

        public BitmapCacheThread(String path) {
            this.path = path;
        }

        @Override
        public void run() {
            super.run();
            try {
                bit = getimage(path);// 图片按比例缩放
                File dir = new File(path);
                if (!dir.exists()) {
                    dir.mkdirs();
                }
//                LogUtil.e("压缩前图片大小:" + formatFileSize(dir));
                File bitmapFile = new File(FILEPATH, FILENAME + System.currentTimeMillis() + FileUtils.getExtension(dir.getName()));
                bitmapFile.createNewFile();
                FileOutputStream fos;
                fos = new FileOutputStream(bitmapFile);
                bit.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.close();
                bit.recycle();
                begin.await();
                Thread.sleep((long) (Math.random() * 100));
                nameList.add(bitmapFile);
            } catch (Exception e1) {
                e1.printStackTrace();
            } finally {
                end.countDown();
            }
        }
    }

    // 压缩图片尺寸
    public Bitmap compressBySize(String pathName) {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = true;// 不去真的解析图片，只是获取图片的头部信息，包含宽高等；
        Bitmap bitmap = BitmapFactory.decodeFile(pathName, opts);
        // 得到图片的宽度、高度；
        float imgWidth = opts.outWidth;
        float imgHeight = opts.outHeight;
        // 分别计算图片宽度、高度与目标宽度、高度的比例；取大于等于该比例的最小整数；
        int widthRatio = (int) Math.ceil(imgWidth / (float) 400);
        int heightRatio = (int) Math.ceil(imgHeight / (float) 800);
        opts.inSampleSize = 1;
        if (widthRatio > 1 || widthRatio > 1) {
            if (widthRatio > heightRatio) {
                opts.inSampleSize = widthRatio;
            } else {
                opts.inSampleSize = heightRatio;
            }
        }
        // 设置好缩放比例后，加载图片进内容；
        opts.inJustDecodeBounds = false;
        bitmap = BitmapFactory.decodeFile(pathName, opts);
        return bitmap;
    }

    /**
     * 图片大小压缩
     *
     * @param srcPath
     * @return
     */
    public static Bitmap getimage(String srcPath) {
        BitmapFactory.Options newOpts = new BitmapFactory.Options();
        // 开始读入图片，此时把options.inJustDecodeBounds 设回true了
        newOpts.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(srcPath, newOpts);// 此时返回bm为空
        newOpts.inSampleSize = computeSampleSize(newOpts, -1, 128 * 128);
        newOpts.inJustDecodeBounds = false;

        int w = newOpts.outWidth;
        int h = newOpts.outHeight;
        // 现在主流手机比较多是800*480分辨率，所以高和宽我们设置为
        float hh = 800f;// 这里设置高度为800f
        float ww = 480f;// 这里设置宽度为480f
        // 缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
        int be = 1;// be=1表示不缩放
        if (w > h && w > ww) {// 如果宽度大的话根据宽度固定大小缩放
            be = (int) (newOpts.outWidth / ww);
        } else if (w < h && h > hh) {// 如果高度高的话根据宽度固定大小缩放
            be = (int) (newOpts.outHeight / hh);
        }
        if (be <= 0)
            be = 1;
        newOpts.inSampleSize = be;// 设置缩放比例
        // 重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
        try {
            bitmap = BitmapFactory.decodeFile(srcPath, newOpts);
        } catch (OutOfMemoryError err) {
        }

        int degree = readPictureDegree(srcPath);
        if (degree != 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(degree);
            // 创建新的图片
            try {
                Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                return resizedBitmap;
            } catch (OutOfMemoryError err) {
            }
        }

        return bitmap;// 压缩好比例大小后再进行质量压缩
    }

    /**
     * 读取图片属性：旋转的角度
     *
     * @param path 图片绝对路径
     * @return degree旋转的角度
     */
    public static int readPictureDegree(String path) {
        int degree = 0;
        try {
            ExifInterface exifInterface = new ExifInterface(path);
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return degree;
    }

    public static int computeSampleSize(BitmapFactory.Options options, int minSideLength, int maxNumOfPixels) {
        int initialSize = computeInitialSampleSize(options, minSideLength, maxNumOfPixels);
        int roundedSize;
        if (initialSize <= 8) {
            roundedSize = 1;
            while (roundedSize < initialSize) {
                roundedSize <<= 1;
            }
        } else {
            roundedSize = (initialSize + 7) / 8 * 8;
        }
        return roundedSize;
    }

    private static int computeInitialSampleSize(BitmapFactory.Options options, int minSideLength, int maxNumOfPixels) {
        double w = options.outWidth;
        double h = options.outHeight;
        int lowerBound = (maxNumOfPixels == -1) ? 1 : (int) Math.ceil(Math.sqrt(w * h / maxNumOfPixels));
        int upperBound = (minSideLength == -1) ? 128 : (int) Math.min(Math.floor(w / minSideLength), Math.floor(h / minSideLength));
        if (upperBound < lowerBound) {
            // return the larger one when there is no overlapping zone.
            return lowerBound;
        }
        if ((maxNumOfPixels == -1) && (minSideLength == -1)) {
            return 1;
        } else if (minSideLength == -1) {
            return lowerBound;
        } else {
            return upperBound;
        }
    }

    public static String formatFileSize(File f) {// 转换文件大小

        String fileSizeString = "";
        try {
            long fileS = getFileSize(f);
            DecimalFormat df = new DecimalFormat("#.00");
            if (fileS < 1024) {
                fileSizeString = df.format((double) fileS) + "B";
            } else if (fileS < 1048576) {
                fileSizeString = df.format((double) fileS / 1024) + "KB";
            } else if (fileS < 1073741824) {
                fileSizeString = df.format((double) fileS / 1048576) + "MB";
            } else {
                fileSizeString = df.format((double) fileS / 1073741824) + "GB";
            }
        } catch (Exception e) {
        }
        return fileSizeString;
    }

    /**
     * 获得文件大小
     *
     * @param f
     * @return
     * @throws IOException
     */
    public static long getFileSize(File f) throws IOException {
        long s = 0;
        if (f.exists()) {
            FileInputStream fis = null;
            fis = new FileInputStream(f);
            s = fis.available();
        }
        return s;
    }

}
