package com.haodaibao.fanbeiapp;

import android.content.Context;

import com.baseandroid.base.BaseApplication;
import com.baseandroid.config.Api;
import com.baseandroid.config.BizUtils;
import com.baseandroid.download.DownloadConfiguration;
import com.baseandroid.download.DownloadManager;
import com.baseandroid.gaode.GaoDeMap;
import com.baseandroid.logger.ReleaseTree;
import com.baseandroid.logger.ThreadAwareDebugTree;
import com.qianbao.mobilecashier.QBCashier;
import com.qianbao.shiningwhitelibrary.BHHManager;
import com.tencent.smtt.sdk.QbSdk;

import ly.count.android.sdk.Countly;
import timber.log.Timber;

public class UIApplication extends BaseApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        // 初始化环境设置
        Api.setEnviroment(BuildConfig.APP_ENV);
        BizUtils.buglyInit(Api.isDevelop);
        GaoDeMap.getInstance().init(this);

        QBCashier.init(this);

        BizUtils.initSWhiteSDK(this);

        BizUtils.buglyInit(Api.isDevelop);

        initDownloader(this);

        //数据埋点
        Countly.sharedInstance().setLoggingEnabled(BuildConfig.DEBUG);
        Countly.sharedInstance().setViewTracking(true);
        Countly.sharedInstance().init(this, "https://www.baidu.com", "221212", Countly.CountlyMode.TEST);

        if (BuildConfig.DEBUG) {
            Timber.plant(new ThreadAwareDebugTree());
        } else {
            Timber.plant(new ReleaseTree());
        }

        // 白花花初始化,组织业务号
        if (Api.isDevelop) {
            BHHManager.initSDK(this, "101000050940442");
        } else {
            BHHManager.initSDK(this, "101000050450551");

        }
      //tbs x5 webview
        QbSdk.initX5Environment(getApplicationContext(), new QbSdk.PreInitCallback() {
            @Override
            public void onCoreInitFinished() {

            }

            @Override
            public void onViewInitFinished(boolean b) {
            }
        });
    }

    private static void initDownloader(Context context) {
        DownloadConfiguration configuration = new DownloadConfiguration();
        configuration.setMaxThreadNum(6);
        DownloadManager.getInstance().init(context, configuration);
    }

}
