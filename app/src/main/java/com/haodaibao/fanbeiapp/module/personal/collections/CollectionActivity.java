package com.haodaibao.fanbeiapp.module.personal.collections;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.StatusBarHelper;
import com.baseandroid.widget.recycleadpter.CustomPtrHeader;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.merchant.MerchantDetailActivity;
import com.haodaibao.fanbeiapp.repository.json.CollectionsBean;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;

public class CollectionActivity extends BaseActivity implements CollectionsContract.View {

    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.rv)
    RecyclerView mRv;
    @BindView(R.id.ptr)
    PtrClassicFrameLayout mPtr;
    private CollectionsPresenter mPresenter;
    private CollectionsRvAdapter mRvAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_collection;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(CollectionActivity.this);
        mToolbarTitle.setText("收藏的店");
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        mPtr.disableWhenHorizontalMove(true);
        mPtr.setPtrHandler(new PtrDefaultHandler() {
            @Override
            public void onRefreshBegin(final PtrFrameLayout frame) {
                getCollectionsNet();
            }
        });
        CustomPtrHeader customPtrHeader = new CustomPtrHeader(this);
        mPtr.setHeaderView(customPtrHeader);
        mPtr.addPtrUIHandler(customPtrHeader);
        mRv.setLayoutManager(new LinearLayoutManager(this));
        if (mRvAdapter == null) {
            mRvAdapter = new CollectionsRvAdapter(this);
        }
        mRv.setAdapter(mRvAdapter);
        mRvAdapter.setClickListener(new CollectionsRvAdapter.OnClickListener() {
            @Override
            public void click(String str) {
                Intent i = new Intent(CollectionActivity.this, MerchantDetailActivity.class);
                i.putExtra("merchantno", str);
                startActivity(i);
            }
        });
        getCollectionsNet();
    }

    @OnClick(R.id.toolbar_back)
    public void onViewClicked() {
        ActivityJumpUtil.goBack(CollectionActivity.this);
    }

    public void getCollectionsNet() {
        if (mPresenter == null) {
            mPresenter = new CollectionsPresenter(this);
        }
        mPresenter.getCollections();
    }

    @Override
    public void setResultView(CollectionsBean bean) {
        if (bean == null) {
            return;
        }
        List<MerchantInfoEntiy> list = bean.getMerchant();
        if (list.size() == 0) {
            mRvAdapter.setEmptyView(R.layout.layout_empty_merchant_collections);
            TextView tvEmptyText = (TextView) mRvAdapter.getEmptyView().findViewById(R.id.tv_empty_text);
            tvEmptyText.setText("还没有收藏的店哦~");
        }
        mRvAdapter.resetData(list);
        mPtr.refreshComplete();
    }
}
