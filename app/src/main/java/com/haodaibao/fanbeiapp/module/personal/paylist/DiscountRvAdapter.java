package com.haodaibao.fanbeiapp.module.personal.paylist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baseandroid.utils.StringUtils;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.PayInfoDetailBean;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Description：优惠活动列表adapter
 * <p>
 * Created by Flower.G on 2018/4/17.
 */
public class DiscountRvAdapter extends BaseRecycleViewAdapter<PayInfoDetailBean.ActivitiesBean> {

    public DiscountRvAdapter(Context context) {
        super(context);
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, PayInfoDetailBean.ActivitiesBean item) {

        DiscountVH discountVH = (DiscountVH) holder;
        if (item == null || StringUtils.isEmpty(item.getLabelName()) || StringUtils.equals("0", item.getBenefitAmount())) {
            discountVH.setVisibility(false);
        } else {
            discountVH.setVisibility(true);
            discountVH.discount_label_tv.setText(item.getLabelName());
            discountVH.discount_amount_tv.setText("-" + mContext.getString(R.string.rmb) + " " + (Double.parseDouble(item.getBenefitAmount()) / 100));
        }

    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_discount, parent, false);
        return new DiscountVH(view);
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class DiscountVH extends RecyclerView.ViewHolder {

        @BindView(R.id.discount_label_tv)
        TextView discount_label_tv;
        @BindView(R.id.discount_amount_tv)
        TextView discount_amount_tv;

        public DiscountVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setVisibility(boolean isVisible) {
            RecyclerView.LayoutParams param = (RecyclerView.LayoutParams) itemView.getLayoutParams();
            if (isVisible) {
                param.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                param.width = LinearLayout.LayoutParams.MATCH_PARENT;
                itemView.setVisibility(View.VISIBLE);
            } else {
                itemView.setVisibility(View.GONE);
                param.height = 0;
                param.width = 0;
            }
            itemView.setLayoutParams(param);
        }
    }
}
