package com.haodaibao.fanbeiapp.module.map;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.amap.api.maps.model.LatLng;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 开发者：LuoYi
 * Time: 2017 15:20 2017/9/14 09
 */

public class MapSelectAdapter extends BaseRecycleViewAdapter<MapInfo> {

    private OnSelectItemListener onSelectItemListener;

    MapSelectAdapter(Context context) {
        super(context);
    }

    void setSelectItemListener(OnSelectItemListener onSelectItemListener) {
        this.onSelectItemListener = onSelectItemListener;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, final MapInfo item) {
        MapSelectHolder mapSelectHolder = (MapSelectHolder) holder;
        mapSelectHolder.tv1.setText("用" + item.getMapname() + "导航");
        mapSelectHolder.tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectItemListener.onSelectItemClick(item.getLatLng1(), item.getMapname(), item.getName(), item.getContent());
            }
        });
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new MapSelectHolder(View.inflate(mContext, R.layout.adapter_mapselect, null));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class MapSelectHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_1)
        Button tv1;

        MapSelectHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    interface OnSelectItemListener {
        void onSelectItemClick(LatLng latLng1, String mapName, String name, String content);
    }
}
