package com.haodaibao.fanbeiapp.module.dialog;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Selection;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseDialog;
import com.haodaibao.fanbeiapp.R;
import com.jayfeng.lesscode.core.DisplayLess;

import butterknife.BindView;

public class MessageDialog extends BaseDialog {

    public static final int CONTENT_MODE_CUSTOM = -1;      // customize the content
    public static final int CONTENT_MODE_TEXT = 0;         // default
    public static final int CONTENT_MODE_EDIT = 1;         // edit text dialog
    public static final int CONTENT_MODE_LIST = 2;         // list dialog

    public int contentMode = CONTENT_MODE_TEXT;

    @BindView(R.id.title)
    protected TextView titleView;
    @BindView(R.id.confirm)
    protected Button confirmButton;
    @BindView(R.id.cancel)
    protected Button cancelButton;
    @BindView(R.id.content)
    protected TextView contentView;
    @BindView(R.id.edit)
    protected EditText editTextView;
    @BindView(R.id.button_divider)
    protected View buttonDividerView;

    @BindView(R.id.text_container)
    protected LinearLayout textContainer;
    @BindView(R.id.list_container)
    protected LinearLayout listContainer;
    @BindView(R.id.edit_container)
    protected LinearLayout editContainer;
    @BindView(R.id.custom_container)
    protected LinearLayout customContainer;

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_layout_message;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Full Screen Area
        getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        getDialog().getWindow().getDecorView().setBackgroundColor(Color.TRANSPARENT);
        getDialog().getWindow().getDecorView().setPadding(DisplayLess.$dp2px(0), DisplayLess.$dp2px(0), DisplayLess.$dp2px(0), DisplayLess.$dp2px(0));
        WindowManager.LayoutParams lp = getDialog().getWindow().getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes(lp);
        if (mOnShowListener != null) {
            getDialog().setOnShowListener(mOnShowListener);
        }
    }

    @Override
    protected void setupView() {
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    public void setTitle(String title) {
        if (titleView != null) {
            titleView.setText(title);
        }
    }

    public void setContent(String content) {
        contentView.setText(content);
    }

    public void setConfirmText(String text) {
        if (confirmButton != null) {
            confirmButton.setText(text);
        }
    }

    public void setCancelText(String text) {
        if (cancelButton != null) {
            cancelButton.setText(text);
        }
    }


    public void setConfirmOnClickListener(View.OnClickListener confirmOnClickListener) {
        if (confirmButton != null && confirmOnClickListener != null) {
            confirmButton.setOnClickListener(confirmOnClickListener);
        }
    }

    public void setCancelOnClickListener(View.OnClickListener cancelOnClickListener) {
        if (cancelButton != null && cancelOnClickListener != null) {
            cancelButton.setOnClickListener(cancelOnClickListener);
        }
    }


    /**
     * *******************************************************************
     * SETTING DIALOG STYLE
     * *******************************************************************
     */
    public void hideTitle() {
        titleView.setVisibility(View.GONE);
        textContainer.setBackgroundResource(R.drawable.view_dialog_top_shape);
    }

    public void hideCancelButton() {
        cancelButton.setVisibility(View.GONE);
        buttonDividerView.setVisibility(View.GONE);
        confirmButton.setBackgroundResource(R.drawable.view_dialog_bottom);
    }

    public void hideConfirmButton() {
        confirmButton.setVisibility(View.GONE);
        buttonDividerView.setVisibility(View.GONE);
        cancelButton.setBackgroundResource(R.drawable.view_dialog_bottom);
    }

    public void setConfirmTextColor(int color) {
        confirmButton.setTextColor(color);
    }

    public void setCancelTextColor(int color) {
        cancelButton.setTextColor(color);
    }

    public void setConfirmButtonBackground(int backgroundResource) {
        confirmButton.setBackgroundResource(backgroundResource);
    }

    public void setCancelButtonBackground(int backgroundResource) {
        cancelButton.setBackgroundResource(backgroundResource);
    }

    /**
     * *******************************************************************
     * CONTENT MODE
     * *******************************************************************
     */
    public void setContentMode(int mode) {
        contentMode = mode;

        if (contentMode == CONTENT_MODE_EDIT) {
            textContainer.setVisibility(View.GONE);
            editContainer.setVisibility(View.VISIBLE);
        }
    }

    // LIST DIALOG
    public void addItem(String text, View.OnClickListener clickListener) {
        TextView itemView = (TextView) LayoutInflater.from(getContext())
                .inflate(R.layout.view_dialog_common_list_item, null);
        itemView.setText(text);
        itemView.setOnClickListener(clickListener);
        listContainer.addView(itemView);

        // set mode to list
        listContainer.setVisibility(View.VISIBLE);
        textContainer.setVisibility(View.GONE);
    }

    public void addListDivider() {
        View dividerView = new View(getContext());
        dividerView.setBackgroundColor(getContext().getResources()
                .getColor(R.color.list_item_divider_color));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 1);
        dividerView.setLayoutParams(params);
        listContainer.addView(dividerView);
    }

    // EDIT DIALOG
    public String getEditValue() {
        return editTextView.getText().toString();
    }

    public void setEditValue(String value) {
        editTextView.setText(value);
        Editable etext = editTextView.getText();
        Selection.setSelection(etext, etext.length());
    }

    public void setMaxLength(int max) {
        editTextView.setFilters(new InputFilter[]{new InputFilter.LengthFilter(max)});
    }

    public static MessageDialog newInstance() {
        MessageDialog messageDialog = new MessageDialog();
        Bundle args = new Bundle();
        args.putString("param", "param");
        messageDialog.setArguments(args);
        return messageDialog;
    }

}
