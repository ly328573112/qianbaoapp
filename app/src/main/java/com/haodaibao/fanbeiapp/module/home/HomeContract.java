package com.haodaibao.fanbeiapp.module.home;

import com.haodaibao.fanbeiapp.repository.json.AdInfo;
import com.haodaibao.fanbeiapp.repository.json.LableInfo;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoResp;
import com.haodaibao.fanbeiapp.repository.json.NearbyRequest;
import com.haodaibao.fanbeiapp.repository.json.PackageAllBean;
import com.haodaibao.fanbeiapp.repository.json.ShopKindListBean;
import com.haodaibao.fanbeiapp.repository.json.YaoheListBean;

import java.util.List;

public interface HomeContract {

    interface Presenter {

        void requestHomeInfo(String siteType, String siteCode, String cityCode, String adid, String adid2, int pagerNumber, int pagerSize, boolean forcenetwork);

        void getShanghuListInfo(String cityCode, int pagerNumber, int pagerSize);

        void getShanghuListInfo(
                boolean forcenetwork,
                String merchantname,
                String categoryno,
                String districtcode,
                String areacode,
                String sort,
                String refundmode,
                String isProduct,
                String isVoucher,
                String siteType,
                String siteCode,
                String cityCode,
                int pagerNumber,
                int pagerSize,
                String headMerchant,
                String tagNo);

        void getOpenCitys();

        void getClassification(String siteType, String siteCode, String cityCode, String keyWord);

        void getShangquan(String siteType, String siteCode, String cityCode);

        void getComboDetail(MerchantInfoEntiy merchantInfoEntiy, String packageno);

        void getTopLabelList();
    }

    interface View {

        /**
         * @param adInfoList
         */
        void onAdListInfo(List<AdInfo> adInfoList);

        void onAdListInfo2(List<AdInfo> adInfoList);

        /**
         * @param yaoheBeanList
         */
        void onYaoheListInfo(List<YaoheListBean.YaoheBean> yaoheBeanList);

        /**
         * @param merchantInfoResp
         */
        void onShanghuListInfo(MerchantInfoResp merchantInfoResp);

        void onShanghuListInfoError();

        void onHomeInfoRefresh();

        void updateCityShanghuList();

        void onRetry();

        void onContent();

        /**
         * @param nearbyRequest
         */
        void onNearbySelect(NearbyRequest nearbyRequest);

        void onStopLocation();

        void onShangquan();

        void onComboDetail(MerchantInfoEntiy merchantInfoEntiy, PackageAllBean packageAllBean);

        void onLebelList(List<LableInfo> lableInfoList);

        void onShopKindsRefresh(ShopKindListBean result);
    }

    interface HomeNewView extends View {

    }

    interface VersionUpdateView {

        void onCheckUpdate();

    }

    interface OnBaseClickListener {
        <T> void onBaseClick(MerchantInfoEntiy merchantInfoEntiy, T t);

    }

    interface HomeClickListener {

        void onHomeClick();

    }
}
