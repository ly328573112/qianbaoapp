package com.haodaibao.fanbeiapp.module.map;

import android.content.Intent;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.amap.api.maps.model.LatLng;
import com.baseandroid.config.Global;
import com.haodaibao.fanbeiapp.R;

import java.net.URISyntaxException;
import java.util.List;

/**
 * 开发者：LuoYi
 * Time: 2017 14:33 2017/9/14 09
 */

public class MapSelectView implements GaodeMapContract.SelectMapAppView, MapSelectAdapter.OnSelectItemListener, View.OnClickListener {

    private final GaodeMapActivity activity;
    private BottomSheetDialog sheetDialog;

    MapSelectView(GaodeMapContract.GaodeMapView gaodeMapView) {
        activity = (GaodeMapActivity) gaodeMapView;
    }

    @Override
    public void selectMapApp(List<MapInfo> list) {
        RecyclerView rvSelectList = (RecyclerView) LayoutInflater.from(activity).inflate(R.layout.item_mapselectwindows, null);
        rvSelectList.setItemAnimator(new DefaultItemAnimator());
        rvSelectList.setLayoutManager(new LinearLayoutManager(activity));

        MapSelectAdapter mapSelectAdapter = new MapSelectAdapter(activity);
        rvSelectList.setAdapter(mapSelectAdapter);
        mapSelectAdapter.resetData(list);
        mapSelectAdapter.setSelectItemListener(this);

        View v = LayoutInflater.from(activity).inflate(R.layout.adapter_mapfooter, null);
        Button tv = v.findViewById(R.id.tv_1);
        tv.setText("取消");
        tv.setOnClickListener(this);
        mapSelectAdapter.addFooterView(v);

        sheetDialog = new BottomSheetDialog(activity);
        sheetDialog.setContentView(rvSelectList);
        sheetDialog.show();
    }

    @Override
    public void onSelectItemClick(LatLng latLng1, String mapName, String name, String content) {
        switch (mapName) {
            case "百度地图":
                try {
                    // 移动APP调起Android百度地图方式举例
                    Intent intent = Intent.getIntent("intent://map/marker?location="
                            + latLng1.latitude + ","//
                            + latLng1.longitude //
                            + "&title=" + name //
                            + "&content=" + content //
                            + "&src=卡惠#Intent;scheme=bdapp;package=com.baidu.BaiduMap;end");
                    activity.startActivity(intent);
                } catch (URISyntaxException e) {
                    Log.e("", e.toString());
                }
                break;
            case "高德地图":
                try {
                    Intent intent = Intent.getIntent("androidamap://route?sourceApplication=softname"
                            + "&slat=" + Global.getMyLocation().latitude //
                            + "&slon=" + Global.getMyLocation().longitude //
                            + "&sname=" + Global.getMyLocation().address //
                            + "&dlat=" + latLng1.latitude //
                            + "&dlon=" + latLng1.longitude //
                            + "&dname=" + name //
                            + "&dev=1" //
                            + "&m=2" //
                            + "&t=0");
                    intent.setPackage("com.autonavi.minimap");
                    activity.startActivity(intent);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
        sheetDialog.dismiss();
    }

    @Override
    public void onClick(View v) {
        sheetDialog.dismiss();
    }


}
