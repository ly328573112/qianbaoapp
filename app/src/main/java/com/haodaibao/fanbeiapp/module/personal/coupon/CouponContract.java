package com.haodaibao.fanbeiapp.module.personal.coupon;

import com.haodaibao.fanbeiapp.repository.json.CouponBean;

/**
 * Created by Routee on 2017/9/12.
 * description: ${cusor}
 */

public interface CouponContract {
    interface View{
        void setUpCouponData(CouponBean bean);
        void onUpdateFailed();
    }

    interface Presenter{
        void getCoupons(String statue, String page);
    }
}
