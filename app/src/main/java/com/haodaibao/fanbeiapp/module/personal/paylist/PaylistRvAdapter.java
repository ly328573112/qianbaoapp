package com.haodaibao.fanbeiapp.module.personal.paylist;

import android.content.Context;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.FormatUtil;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.bumptech.glide.Glide;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.invoice.detail.InvoiceDetailActivity;
import com.haodaibao.fanbeiapp.module.invoice.makeout.MakeoutInvoiceActivity;
import com.haodaibao.fanbeiapp.repository.json.PaylistItemBean;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Routee on 2017/9/4.
 * description: ${cusor}
 */

class PaylistRvAdapter extends BaseRecycleViewAdapter<PaylistItemBean.OrdersBean.ItemsBean> implements View.OnClickListener {

    private OnClickListener mListener;
    public int pos;

    public PaylistRvAdapter(Context context) {
        super(context);
    }

    @Override
    protected void onBindBaseViewHolder(final RecyclerView.ViewHolder holder, final PaylistItemBean.OrdersBean.ItemsBean item) {
        PaylistViewHolder h = (PaylistViewHolder) holder;
        //整个条目点击监听
        h.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.itemClick(item);
                }
            }
        });
        //再买一单点击监听
        h.mTvPayAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.payAginClick(item);
                }
            }
        });
        //问题反馈点击监听
        h.mTvFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.feedbackClick(item);
                }
            }
        });
        //发表评论点击监听
        h.mTvComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.commentClick(item);
                    pos = holder.getLayoutPosition() - getHeaderLayoutCount();
                }
            }
        });
        h.mTvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.nameClick(item);
                }
            }
        });

        Glide.with(mContext).load(CommonUtils.getImageUrl(item.getImageUrl())).asBitmap()
                .placeholder(R.drawable.image_merchant_placehold).error(R.drawable.image_merchant_placehold)
                .into(h.mIvHead);
        //商户名
        h.mTvName.setText(item.getMerchantname());
        //支付时间
//        h.mTvTime.setText("买单时间: " + TimeUtils.getPayTime(item.getPaytime()));
        //支付方式
        h.mTvChannel.setText(getPayType(item.getPayType()));
        //原价

        if (TextUtils.equals("1", item.relationType)) {
            h.combo_info_price.setVisibility(View.VISIBLE);
            h.combo_info_price_yuan.setVisibility(View.VISIBLE);
            h.combo_info_price_yuan.setText(mContext.getString(R.string.rmb) + " " + FormatUtil.formatAmount(2, item.originalPrice));
            h.combo_info_price.setText(mContext.getString(R.string.rmb) + " " + FormatUtil.formatAmount(2, item.preferentialPrice));
            if (TextUtils.equals("02", item.faceUserTypeCode)) {
                h.mTvMoney.setText("[新顾客专享]" + item.packageName);
            } else if (TextUtils.equals("03", item.faceUserTypeCode)) {
                h.mTvMoney.setText("[老顾客专享]" + item.packageName);
            } else {
                h.mTvMoney.setText(item.packageName);
            }

        } else {
            h.combo_info_price.setVisibility(View.GONE);
            h.combo_info_price_yuan.setVisibility(View.GONE);
            h.mTvMoney.setText("优惠买单，消费" + mContext.getString(R.string.rmb) + " " + FormatUtil.formatAmount(2, item.getOrderamt()));
        }

        //优惠金额
        double reduce = Double.parseDouble(item.getReduceamount());
        if (reduce > 0) {
            h.tv_disount_name.setVisibility(View.VISIBLE);
            h.mTvDiscount.setVisibility(View.VISIBLE);
            h.mTvDiscount.setText("-" + mContext.getString(R.string.rmb) + " " + FormatUtil.formatAmount(2, item.getReduceamount()));
        } else {
            h.tv_disount_name.setVisibility(View.INVISIBLE);
            h.mTvDiscount.setVisibility(View.INVISIBLE);
        }

        //付款金额
        h.mTvPay.setText(mContext.getString(R.string.rmb) + " " + FormatUtil.formatAmount(2, item.getCashamt()));
        //订单状态
//        h.mTvStatue.setText(item.getStatusmessage());
        //再买一单及问题反馈
        String refundstatus = item.getRefundstatus();
        if (!TextUtils.isEmpty(refundstatus) && refundstatus.equals("11") || refundstatus.equals("20")) {
            h.mTvStatue.setTextColor(ContextCompat.getColor(mContext, R.color.text_gray2));
            h.mTvStatue.setText("已退款");
            h.mTvPayAgain.setVisibility(View.GONE);
            h.mTvComment.setVisibility(View.GONE);
            h.mTvFeedback.setVisibility(View.VISIBLE);
            if (refundstatus.equals("20")) {
                h.mTvStatue.setText("已退款");
            } else {
                h.mTvStatue.setText("退款中");
            }
        } else {
            h.mTvStatue.setTextColor(ContextCompat.getColor(mContext, R.color.red));
            h.mTvStatue.setText("买单成功");
            if (TextUtils.equals(item.getCommentstatus(), "50")) {
                h.mTvComment.setVisibility(View.VISIBLE);
            } else {
                h.mTvComment.setVisibility(View.GONE);
            }
            h.mTvPayAgain.setVisibility(View.VISIBLE);
            h.mTvFeedback.setVisibility(View.GONE);
        }

        String date = item.getPaytime();
        if (TextUtils.isEmpty(date)) {
            date = item.getOrdertime();
        }
        String str = FormatUtil.changeTime(item.getPaytime(), "yyyy-MM-dd HH:mm");
        h.mTvPayDate.setText(str);
        setInvoice(h, item);
    }

    private void setInvoice(final PaylistViewHolder holder, final PaylistItemBean.OrdersBean.ItemsBean item) {
        TextView tv_write_invoice = holder.tv_write_invoice;
        if (TextUtils.equals(item.canInvoice, "0")) {
            tv_write_invoice.setVisibility(View.GONE);
        } else if (TextUtils.equals(item.canInvoice, "1")) {
            if (TextUtils.equals(item.invoiceStatus, "1")) {
                tv_write_invoice.setVisibility(View.VISIBLE);
                tv_write_invoice.setText("查看发票");
            } else if (TextUtils.equals(item.invoiceStatus, "2") || TextUtils.equals(item.invoiceStatus, "3")) {
                tv_write_invoice.setVisibility(View.VISIBLE);
                tv_write_invoice.setText("重开发票");
            } else {
                tv_write_invoice.setVisibility(View.VISIBLE);
                tv_write_invoice.setText("申请发票");
            }
        }
        tv_write_invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.equals(item.invoiceStatus, "1")) {
                    InvoiceDetailActivity.start((BaseActivity) mContext, item.invoiceId);
                } else {
                    pos = holder.getLayoutPosition() - getHeaderLayoutCount();
                    MakeoutInvoiceActivity.start((BaseActivity) mContext, item.getOrderno(), item.getMerchantno(), item.invoiceId, false);
                }

            }
        });
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_paylist, parent, false);
        return new PaylistViewHolder(view);
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_pay_again:
                break;
            default:
                break;
        }
    }

    class PaylistViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_pay_date)
        TextView mTvPayDate;
        @BindView(R.id.combo_info_icon)
        ImageView mIvHead;
        @BindView(R.id.tv_name)
        TextView mTvName;
        //        @BindView(R.id.tv_time)
//        TextView         mTvTime;
        @BindView(R.id.tv_channel)
        TextView mTvChannel;
        @BindView(R.id.combo_info_name)
        TextView mTvMoney;
        @BindView(R.id.tv_discount)
        TextView mTvDiscount;
        @BindView(R.id.tv_disount_name)
        TextView tv_disount_name;
        @BindView(R.id.tv_pay)
        TextView mTvPay;
        @BindView(R.id.tv_statue)
        TextView mTvStatue;
        @BindView(R.id.tv_pay_again)
        TextView mTvPayAgain;
        @BindView(R.id.tv_comment)
        TextView mTvComment;
        @BindView(R.id.tv_feedback)
        TextView mTvFeedback;
        @BindView(R.id.combo_info_price)
        TextView combo_info_price;
        @BindView(R.id.combo_info_price_yuan)
        TextView combo_info_price_yuan;
        @BindView(R.id.tv_write_invoice)
        TextView tv_write_invoice;

        public PaylistViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            combo_info_price_yuan.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }

    public interface OnClickListener {
        void itemClick(PaylistItemBean.OrdersBean.ItemsBean item);

        void payAginClick(PaylistItemBean.OrdersBean.ItemsBean item);

        void feedbackClick(PaylistItemBean.OrdersBean.ItemsBean item);

        void commentClick(PaylistItemBean.OrdersBean.ItemsBean item);

        void nameClick(PaylistItemBean.OrdersBean.ItemsBean item);
    }

    public void setClickListener(OnClickListener listener) {
        this.mListener = listener;
    }

    private String getPayType(String payType) {
        if (TextUtils.isEmpty(payType)) {
            return "";
        }
        switch (payType) {
            case "01":
                return "现金";
            case "02":
                return "银行卡";
            case "03":
                return "微信";
            case "06":
                return "刷卡";
            case "04":
            case "07":
            case "08":
            case "24":
                return "支付宝";
            case "05":
                return "微信";
            case "11":
            default:
                //                return "红包";
                return "";
            case "23":
                return "微信";
            case "26":
                return "白花花";
        }
    }
}
