package com.haodaibao.fanbeiapp.module.personal.balance;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;

/**
 * Created by Routee on 2017/9/18.
 * description: ${cusor}
 */

public class BalanceRvAdapter extends BaseRecycleViewAdapter {

    public BalanceRvAdapter(Context context) {
        super(context);
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, Object item) {

    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }
}
