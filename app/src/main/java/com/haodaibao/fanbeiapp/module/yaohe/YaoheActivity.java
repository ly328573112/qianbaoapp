package com.haodaibao.fanbeiapp.module.yaohe;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.CommonUtils;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.YaoheDetailsBean;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import java.util.List;

import butterknife.BindView;

public class YaoheActivity extends BaseActivity implements YaoheContract.IYaoheView {
    @BindView(R.id.mesage_list_rv)
    RecyclerView mesage_list_rv;
    @BindView(R.id.toolbar_title)
    TextView title;
    @BindView(R.id.toolbar_back)
    RelativeLayout leftIcon;

    private YaoheDetailsAdapter adapter;
    private YaoheContract.Presenter yaohePresenter;
    private String messageNo;


    public static void open(Activity activity, String messageNo) {
        Bundle bundle = new Bundle();
        bundle.putString("messageNo", messageNo);
        ActivityJumpUtil.next(activity, YaoheActivity.class, bundle, 0);
    }

    /**
     * 数据处理移到子线程
     *
     * @param yaoheDetailsBean
     */
    @Override
    public void updateDetails(List<YaoheDetailsBean.MessageBody> yaoheDetailsBean) {
        adapter.addData(yaoheDetailsBean);
    }

    @Override
    public void onFail(String msg) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_preview_effect;
    }

    @Override
    protected void setupView() {
        adapter = new YaoheDetailsAdapter(this);
        mesage_list_rv.setLayoutManager(new LinearLayoutManager(this));
        mesage_list_rv.setAdapter(adapter);
        leftIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                finish();
            }
        });
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        messageNo = getIntent().getStringExtra("messageNo");
        title.setText("详情");
        yaohePresenter = new YaohePresenter(YaoheActivity.this);
        yaohePresenter.getYaoheDetails(messageNo);
    }
}