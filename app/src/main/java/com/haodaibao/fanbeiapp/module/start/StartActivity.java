package com.haodaibao.fanbeiapp.module.start;

import android.Manifest;
import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Api;
import com.baseandroid.config.Global;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.LogUtil;
import com.baseandroid.utils.RxUtils;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.database.GreenDaoDbHelp;
import com.haodaibao.fanbeiapp.module.home.HomeActivity;
import com.haodaibao.fanbeiapp.module.login.GuideActivity;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.CheckUpdate;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.tencent.stat.MtaSDkException;
import com.tencent.stat.StatService;
import com.umeng.analytics.MobclickAgent;
import com.umeng.analytics.MobclickAgent.UMAnalyticsConfig;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class StartActivity extends BaseActivity implements StartContract.View {

    private StartPresenter mStartPresenter;
    private ValueAnimator mValueAnimator;

    @BindView(R.id.start_root)
    View start_root;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_start;
    }

    @Override
    protected void setupView() {
        CommonUtils.setFullScreen(StartActivity.this, true);
        CommonUtils.hideNavigationBar(this);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        mStartPresenter = new StartPresenter(StartActivity.this, StartActivity.this);
        delayShowAnimate();
    }

    private void delayShowAnimate() {
        Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> e) throws Exception {
                e.onNext("");
                e.onComplete();
            }
        })
                .delay(400, TimeUnit.MILLISECONDS)
                .compose(RxUtils.applySchedulersLifeCycle(this))
                .subscribe(new RxObserver<Object>() {

                    @Override
                    public void onNext(Object object) {
                        requestAppPermiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        requestAppPermiss();
                    }

                });
        // 删除数据库OpenCity文件
        GreenDaoDbHelp.deleteallCityInfo();
        //异步进行配置需要初始化的信息
        initUmengData();
        initMTA();
    }

    private void requestAppPermiss() {
        final RxPermissions rxPermissions = new RxPermissions(StartActivity.this);
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION)
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull Boolean aBoolean) {

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        gotoMainActivity();
                    }
                });
    }

    @Override
    public void OnVersionUpdate(CheckUpdate checkUpdate) {
    }

    @Override
    public void OnVersionUpdateError() {
    }


    private void gotoMainActivity() {
        //引导页与首页判断
        if (false/*!Global.getFirstUsing()*/) {
            Global.setFirstUsing(true);
            startActivity(new Intent(this, GuideActivity.class));
            finish();
        } else {
            if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                gotoActivity();
            } else {
                mStartPresenter.requestUserInfo();
            }
        }
    }

    @Override
    public void OnUserInfoSuccess() {
        gotoActivity();
    }

    @Override
    public void OnUserInfoError() {
        gotoActivity();
    }

    private void gotoActivity() {
        mValueAnimator = ValueAnimator.ofFloat(1.0f, 1.1f);
        mValueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                //                Intent intent = new Intent(StartActivity.this, (Global.getHomeType() == 0) ? MaphomeActivity.class : HomeActivity.class);
                Intent intent = new Intent(StartActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.face_in, R.anim.face_out);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        mValueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
            }
        });
        mValueAnimator.setDuration(1200);
        mValueAnimator.start();
    }

    private void initUmengData() {
        MobclickAgent.startWithConfigure(new UMAnalyticsConfig(Global.getContext(), "55e3c03767e58ec996004f53", CommonUtils
                .getChannel()));
    }

    private void initMTA() {
        if (Api.isDevelop) {//线上环境才统计
            return;
        }
        String appkey = "A3N7H1SLN8YI";
        // 初始化并启动MTA
        try {
            // 第三个参数必须为：com.tencent.stat.common.StatConstants.VERSION
            StatService.startStatService(this, appkey, com.tencent.stat.common.StatConstants.VERSION);
            LogUtil.d("MTA : 初始化成功");
        } catch (MtaSDkException e) {
            // MTA初始化失败
            LogUtil.d("MTA : 初始化失败" + e);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mValueAnimator != null) {
            mValueAnimator.removeAllUpdateListeners();
            mValueAnimator.cancel();
        }
    }
}
