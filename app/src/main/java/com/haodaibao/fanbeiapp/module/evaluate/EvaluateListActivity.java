package com.haodaibao.fanbeiapp.module.evaluate;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.photocameralib.media.ImageGalleryActivity;
import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.StatusBarHelper;
import com.baseandroid.widget.RouteeFlowLayout;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.baseandroid.widget.recycleadpter.CustomPtrHeader;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.CommentClickListener;
import com.haodaibao.fanbeiapp.repository.json.CommentListBean;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import java.util.List;

import butterknife.BindView;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;

/**
 * 开发者：LuoYi
 * Time: 2017 16:29 2017/9/12 09
 */

public class EvaluateListActivity extends BaseActivity implements EvaluateContract.EvaluateView, BaseRecycleViewAdapter.OnLoadMoreListener, View.OnClickListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.ptr_ly)
    PtrClassicFrameLayout ptrLy;
    @BindView(R.id.mrcv)
    RecyclerView mrcv;

    private EvaluateListAdapter listAdapter;
    private RouteeFlowView routeeFlowView;
    private EvaluatePresenter evaluatePresenter;
    private String merchantno;
    private String queryType = "1";
    private int page = 1;
    private int limit = 10;

    public static void open(Activity activity, String merchantno) {
        Bundle bundle = new Bundle();
        bundle.putString("merchantno", merchantno);
        ActivityJumpUtil.next(activity, EvaluateListActivity.class, bundle, 0);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_evaluate_list;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(this);
        routeeFlowView = new RouteeFlowView(this, changeMark);
        evaluatePresenter = new EvaluatePresenter(this);
        toolbarTitle.setText(R.string.text_commentlist_title);
        toolbarBack.setOnClickListener(this);

        listAdapter = new EvaluateListAdapter(this, commentClickListener);
        mrcv.setLayoutManager(new LinearLayoutManager(this));
        listAdapter.setOnLoadMoreListener(this, mrcv);
        mrcv.setAdapter(listAdapter);

        CustomPtrHeader customPtrHeader = new CustomPtrHeader(this);
        ptrLy.setHeaderView(customPtrHeader);
        ptrLy.addPtrUIHandler(customPtrHeader);
        ptrLy.disableWhenHorizontalMove(true);
        ptrLy.setPtrHandler(ptrDefaultHandler);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        merchantno = getIntent().getExtras().getString("merchantno");
        getNetWorkData();
    }

    private void getNetWorkData() {
        evaluatePresenter.getEvaluateData(merchantno, queryType, "Y", page + "", limit + "");
    }

    PtrDefaultHandler ptrDefaultHandler = new PtrDefaultHandler() {

        @Override
        public void onRefreshBegin(PtrFrameLayout frame) {
            page = 1;
            listAdapter.setEnableLoadMore(false);
            getNetWorkData();
        }
    };

    @Override
    public void onLoadMore() {
        getNetWorkData();
    }

    @Override
    public void onEvaluateList(CommentListBean commentListBean) {
        if (page == 1) {
            if (listAdapter.getHeaderLayout() != null) {
                listAdapter.getHeaderLayout().removeAllViews();
            }
            View inflate = LayoutInflater.from(this).inflate(R.layout.comment_mark_ly, null);
            RouteeFlowLayout tagComment = (RouteeFlowLayout) inflate.findViewById(R.id.tag_comment);
            routeeFlowView.setRouteeFlowView(tagComment, commentListBean.getHead(), queryType);
            listAdapter.addHeaderView(inflate);
            listAdapter.resetData(commentListBean.getList().getItems());
            if (commentListBean.getList().getItems().size() < limit) {
                listAdapter.loadMoreEnd(true);
            }
            ptrLy.refreshComplete();
        } else {
            listAdapter.addData(commentListBean.getList().getItems());
            if (commentListBean.getList().getItems().size() < limit) {
                listAdapter.loadMoreEnd(false);
                return;
            }
            listAdapter.loadMoreComplete();
        }
        page++;
    }

    @Override
    public void onEvaluateListError() {
        if (page == 1) {
            ptrLy.refreshComplete();
        } else {
            ptrLy.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!EvaluateListActivity.this.isFinishing()) {
                        listAdapter.loadMoreFail();
                    }
                }
            }, 200);
        }
    }

    @Override
    public void onSubmint() {

    }

    @Override
    public void onSubmintError() {

    }

    private EvaluateContract.ChangeMark changeMark = new EvaluateContract.ChangeMark() {
        @Override
        public void changeMark(String type) {
            queryType = type;
            page = 1;
            getNetWorkData();
        }
    };

    private CommentClickListener commentClickListener = new CommentClickListener() {
        @Override
        public void goToAllComment() {

        }

        @Override
        public void goToEnlarge(List<String> imglists, int pos) {
            String[] strings = imglists.toArray(new String[imglists.size()]);
            ImageGalleryActivity.show(EvaluateListActivity.this, strings, pos, false);
        }

        @Override
        public void goToMerchantDetail(String merchantno) {

        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            default:

                break;
        }
    }
}
