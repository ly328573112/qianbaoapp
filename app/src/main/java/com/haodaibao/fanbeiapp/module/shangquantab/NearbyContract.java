package com.haodaibao.fanbeiapp.module.shangquantab;

import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;

/**
 * 开发者：LuoYi
 * Time: 2017 17:56 2017/12/11 12
 */

public interface NearbyContract {

    interface NearbyView {

        void onNearbyLocation(String locationType);

        void onCatagoryView(CheckBox cb_catagory_id, View view);

        void onScopeView(CheckBox cb_scope_id, View view);

        void onSortwaysView(CheckBox cb_sortways_id, View view);

        void onReductionTabSelect();

    }

}
