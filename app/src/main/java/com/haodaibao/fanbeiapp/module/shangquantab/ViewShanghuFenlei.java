package com.haodaibao.fanbeiapp.module.shangquantab;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.baseandroid.config.Global;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.search.view.ViewBaseAction;
import com.haodaibao.fanbeiapp.module.shangquantab.adapter.TextAdapterFenleiLeft;
import com.haodaibao.fanbeiapp.module.shangquantab.adapter.TextAdapterFenleiRight;
import com.haodaibao.fanbeiapp.repository.json.ShanghufenleiInfo;
import com.haodaibao.fanbeiapp.repository.json.ShanghufenleiList;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ViewShanghuFenlei extends LinearLayout implements ViewBaseAction {

    private Context context;
    private ListView regionListView;
    private ListView plateListView;
    private List<ShanghufenleiInfo> groups = new ArrayList<>();
    private LinkedList<ShanghufenleiInfo> childrenItem = new LinkedList<>();
    private SparseArray<LinkedList<ShanghufenleiInfo>> children = new SparseArray<>();
    private TextAdapterFenleiRight plateListViewAdapter;
    private TextAdapterFenleiLeft earaListViewAdapter;
    private OnSelectListener mOnSelectListener;
    private int tEaraPosition = 0;
    private int tBlockPosition = 1;
    private String showString = "全部";
    private List<ShanghufenleiList> fenleiList;
    private ShanghufenleiInfo gronupsInfo;


    public ViewShanghuFenlei(Context context) {
        super(context);
    }

    public ViewShanghuFenlei(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void updateShowText(String showArea, String showBlock) {
        if (showArea == null || showBlock == null) {
            return;
        }
        for (int i = 0; i < groups.size(); i++) {
            if (groups.get(i).getName().equals(showArea)) {
                earaListViewAdapter.setSelectedPosition(i);
                childrenItem.clear();
                if (i < children.size()) {
                    childrenItem.addAll(children.get(i));
                }
                tEaraPosition = i;
                break;
            }
        }
        for (int j = 0; j < childrenItem.size(); j++) {
            if (childrenItem.get(j).getName().replace("全部", "").equals(showBlock.trim())) {
                plateListViewAdapter.setSelectedPosition(j);
                tBlockPosition = j;
                break;
            }
        }
        setDefaultSelect();
    }

    /**
     * @param context      上下文
     * @param isAllVisible 父分类是否显示全部
     */
    public void init(Context context, final boolean isAllVisible) {
        childrenItem.clear();
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_region, this, true);
        regionListView = (ListView) findViewById(R.id.listView);
        regionListView.setVerticalScrollBarEnabled(false);
        plateListView = (ListView) findViewById(R.id.listView2);
        regionListView.setVerticalScrollBarEnabled(false);
        initData(context, Global.getCategorys());
    }

    public void initData(Context context, List<ShanghufenleiList> fenleiList1) {
        this.fenleiList = fenleiList1;
        groups.clear();
        children.clear();
        if (fenleiList != null) {
            groups.add(new ShanghufenleiInfo("", "全部", "ROOT", "", ""));
            LinkedList<ShanghufenleiInfo> tItem = new LinkedList<ShanghufenleiInfo>();
            tItem.add(new ShanghufenleiInfo("", "全部", "", "", ""));
            children.put(0, tItem);

            for (int i = 0; i < fenleiList.size(); i++) {
                ShanghufenleiList fenLei = fenleiList.get(i);
                // 添加区域中剩余区域
                ShanghufenleiInfo info = new ShanghufenleiInfo(
                        fenLei.getCategoryno(),
                        fenLei.getName(),
                        fenLei.getParentno(),
                        fenLei.getHot(),
                        fenLei.getStatus());
                groups.add(info);

                // 添加点击剩余区域每一项在商圈中的第一个全部
                LinkedList<ShanghufenleiInfo> tItem1 = new LinkedList<>();
                tItem1.add(new ShanghufenleiInfo("", "全部", fenLei.getCategoryno(), "", ""));
                List<ShanghufenleiInfo> list = fenLei.getList();
                for (int j = 0; j < list.size(); j++) {
                    tItem1.add(list.get(j));
                }
                children.put(i + 1, tItem1);
            }
        } else {
            groups.clear();
            groups.add(new ShanghufenleiInfo("", "全部", "", "", ""));
        }
        earaListViewAdapter = new TextAdapterFenleiLeft(context, groups, R.color.c_F62241, R.drawable.choose_item_selector, R.drawable.choose_eara_item_selector);
        earaListViewAdapter.setTextSize(14);
        earaListViewAdapter.setSelectedPositionNoNotify(tEaraPosition);
        regionListView.setAdapter(earaListViewAdapter);
        earaListViewAdapter.setOnItemClickListener(new TextAdapterFenleiLeft.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position < children.size()) {
                    if (position == 0 && mOnSelectListener != null) {
                        mOnSelectListener.getValue(new ShanghufenleiInfo("", "全部", "", "", ""));
                        childrenItem.clear();
                        childrenItem.addAll(new LinkedList<ShanghufenleiInfo>());
                        plateListViewAdapter.notifyDataSetChanged();
                        if (plateListViewAdapter != null) {
                            plateListViewAdapter.cleanRightData();
                        }
                    } else {
                        childrenItem.clear();
                        childrenItem.addAll(children.get(position));
                        gronupsInfo = groups.get(position);
                        plateListViewAdapter.notifyDataSetChanged();
                    }
                }
            }
        });
        if (tEaraPosition < children.size()) {
            if (tEaraPosition == 0) {
                LinkedList<ShanghufenleiInfo> linkedList = children.get(tEaraPosition);
                if (linkedList.get(0).getName().equals("全部")) {
                    childrenItem.addAll(new LinkedList<ShanghufenleiInfo>());
                }
            } else {
                childrenItem.addAll(children.get(tEaraPosition));
            }
        }

        plateListViewAdapter = new TextAdapterFenleiRight(context, childrenItem, R.color.c_F62241, R.drawable.choose_item_right, R.drawable.choose_plate_item_selector);
        plateListViewAdapter.setTextSize(14);
        plateListViewAdapter.setSelectedPositionNoNotify(tBlockPosition);
        plateListView.setAdapter(plateListViewAdapter);
        plateListViewAdapter.setOnItemClickListener(new TextAdapterFenleiRight.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {
                ShanghufenleiInfo info = childrenItem.get(position);
                if (mOnSelectListener != null) {
                    if (!TextUtils.isEmpty(info.getName()) && info.getName().contains("全部")) {
                        showString = gronupsInfo.getName();
                        mOnSelectListener.getValue(gronupsInfo);
                    } else {
                        showString = info.getName();
                        mOnSelectListener.getValue(info);
                    }
                }
            }
        });
        if (tBlockPosition < childrenItem.size()) {
            showString = childrenItem.get(tBlockPosition).getName();
        }
        if (!TextUtils.isEmpty(showString) && showString.contains("全部")) {
            showString = showString.replace("全部", "");
        }
        setDefaultSelect();
    }

    public void setDefaultSelect() {
        regionListView.setSelection(tEaraPosition);
        plateListView.setSelection(tBlockPosition);
    }

    public String getShowText() {
        return showString;
    }

    public void setOnSelectListener(OnSelectListener onSelectListener) {
        mOnSelectListener = onSelectListener;
    }

    public interface OnSelectListener {
        void getValue(ShanghufenleiInfo info);
    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void show() {
        // TODO Auto-generated method stub

    }

    public List<ShanghufenleiList> getFenleiList() {
        return fenleiList;
    }

    public void setFenleiList(List<ShanghufenleiList> fenleiList) {
        this.fenleiList = fenleiList;
    }

    public int gettEaraPosition() {
        return tEaraPosition;
    }

    public void settEaraPosition(int tEaraPosition) {
        this.tEaraPosition = tEaraPosition;
    }

    public int gettBlockPosition() {
        return tBlockPosition;
    }

    public void settBlockPosition(int tBlockPosition) {
        this.tBlockPosition = tBlockPosition;
    }

}
