package com.haodaibao.fanbeiapp.module.map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.BitmapDescriptor;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Global;
import com.baseandroid.gaode.GaoDeMap;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.FormatUtil;
import com.baseandroid.utils.Util;
import com.bumptech.glide.Glide;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.event.LocationResultEvent;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.net.URISyntaxException;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;


public class GaodeMapActivity extends BaseActivity implements GaodeMapContract.GaodeMapView {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.gaode_bmapView)
    MapView gaodeBmapView;
    //    @BindView(R.id.textView1)
//    TextView textView1;
//    @BindView(R.id.textView2)
//    TextView textView2;
//    @BindView(R.id.button1)
//    Button button1;
//    @BindView(R.id.button2)
//    Button button2;
    @BindView(R.id.iv_merchant)
    ImageView iv_merchant;
    @BindView(R.id.tv_merchant_name)
    TextView tv_merchant_name;
    @BindView(R.id.tv_merchant_address)
    TextView tv_merchant_address;
    @BindView(R.id.tv_distance)
    TextView tv_distance;
    @BindView(R.id.iv_golines)
    ImageView iv_golines;
    @BindView(R.id.merchant_rl)
    RelativeLayout merchant_rl;
    @BindView(R.id.map_dingwei)
    ImageView map_dingwei;

    BitmapDescriptor bdB = BitmapDescriptorFactory.fromResource(R.drawable.map_merchant_icon);
    BitmapDescriptor bdB1 = BitmapDescriptorFactory.fromResource(R.drawable.map_my_icon);
    private AMap aMap;
    private GaodeMapPresenter gaodeMapPresenter;
    private String longitude;
    private String latitude;
    private String merchantname;
    private String merchantno;
    private String phone;
    private String merchantaddress;
    private String imgurl;
    private boolean fromShanghuDetails;
    private MapSelectView mapSelectView;
    private double distance;

    public static void open(Activity activity, String longitude, String latitude, String imgurl, String merchantname, String merchantno, String phone, String merchantaddress) {
        Bundle bundle = new Bundle();
        bundle.putString("longitude", longitude);
        bundle.putString("imgurl", imgurl);
        bundle.putString("latitude", latitude);
        bundle.putString("merchantname", merchantname);
        bundle.putString("merchantno", merchantno);
        bundle.putString("phone", phone);
        bundle.putString("merchantaddress", merchantaddress);
        bundle.putBoolean("fromShanghuDetails", true);
        ActivityJumpUtil.next(activity, GaodeMapActivity.class, bundle, 0);
    }

    Handler locateHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:// 定位成功
                default:
                    addMarkersToMap();
                    break;
            }
            super.handleMessage(msg);
        }
    };

    public Handler getLocateHandler() {
        return locateHandler;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_gaode_map;
    }

    @Override
    protected void setupView() {
//        StatusBarHelper.translateStatusBar(GaodeMapActivity.this);
        gaodeMapPresenter = new GaodeMapPresenter(this);
        mapSelectView = new MapSelectView(this);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        gaodeBmapView.onCreate(savedInstanceState); // 此方法必须重写
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        imgurl = bundle.getString("imgurl");
        longitude = bundle.getString("longitude");
        latitude = bundle.getString("latitude");
        merchantname = bundle.getString("merchantname");
        merchantno = bundle.getString("merchantno");
        phone = bundle.getString("phone");
        fromShanghuDetails = bundle.getBoolean("fromShanghuDetails");
        merchantaddress = bundle.getString("merchantaddress");
        initMerchant();
        if (aMap == null) {
            aMap = gaodeMapPresenter.initMapView(gaodeBmapView.getMap());
        }

    }

    private void initMerchant() {
        Glide.with(this)
                .load(CommonUtils.getImageUrl(imgurl))
                .asBitmap()
                .placeholder(R.drawable.image_merchant_placehold)
                .error(R.drawable.image_merchant_placehold).into(iv_merchant);
        tv_merchant_name.setText(merchantname);
        tv_merchant_address.setText(merchantaddress);

    }

    /**
     * 在地图上添加marker
     */
    private void addMarkersToMap() {
        aMap.clear();
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        LatLng ll = null;
        if (!TextUtils.isEmpty(latitude) && !TextUtils.isEmpty(longitude)) {
            ll = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
            MarkerOptions mo = new MarkerOptions().title(merchantaddress).position(ll).icon(bdB);
            Marker mMarker = aMap.addMarker(mo);
            // 设置所有maker显示在当前可视区域地图中
            builder.include(mMarker.getPosition());

            //构造一个圆形来保证地图标识的显示
            double x1 = Double.parseDouble(latitude);
            double y1 = Double.parseDouble(longitude);

            double x3 = Global.getMyLocation().latitude;
            double y3 = Global.getMyLocation().longitude;

            //圆心
            double x0 = (x1 + x3) / 2;
            double y0 = (y1 + y3) / 2;

            //半径
//            double r = Math.sqrt((x1 - x3) * (x1 - x3) + (y1 - y3) * (y1 - y3)) / 2;
            double r = Util.getlo(new LatLng(Global.getMyLocation().latitude, Global.getMyLocation().longitude), ll) / 2;

            double x2 = x0;
            double y2 = y0 + r;

            double x4 = x0;
            double y4 = y0 - r;

            LatLng point_b = new LatLng(x2, y2);
            LatLng point_d = new LatLng(x4, y4);
            LatLng point_0 = new LatLng(x0, y0);

            builder.include(point_b);
            builder.include(point_d);
            builder.include(point_0);
        }
        LatLng location = new LatLng(Global.getMyLocation().latitude, Global.getMyLocation().longitude);
        MarkerOptions mo = new MarkerOptions().position(location).icon(bdB1);
        aMap.addMarker(mo);
        builder.include(mo.getPosition());
        LatLngBounds bounds = builder.build();
        distance = Util.getDistance(location, ll);
        distance = Math.abs(distance);
//        if (distance >= 5000) {
//            LatLng latLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
//            aMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
//        } else {
        aMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 150));
//        }
        tv_distance.setText("距你" + FormatUtil.formatDistance((int) distance + ""));
    }

    @OnClick({R.id.merchant_rl, R.id.toolbar_back, R.id.iv_golines, R.id.map_dingwei})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.merchant_rl:
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.iv_golines:
                List<MapInfo> mapInfoList = gaodeMapPresenter.checkAppInstall(latitude, longitude, merchantaddress, merchantname);
                if (mapInfoList.size() == 0) {
                    Toast.makeText(this, "请安装百度地图或高德地图！", Toast.LENGTH_SHORT).show();
                    return;
                } else if (mapInfoList.size() == 1) {
                    try {
                        if (TextUtils.equals(mapInfoList.get(0).getMapname(), "百度地图")) {
                            Intent intent = Intent.getIntent("intent://map/marker?location="
                                    + mapInfoList.get(0).getLatLng1().latitude + ","
                                    + mapInfoList.get(0).getLatLng1().longitude
                                    + "&title=" + mapInfoList.get(0).getName()
                                    + "&content=" + mapInfoList.get(0).getContent()
                                    + "&src=卡惠#Intent;scheme=bdapp;package=com.baidu.BaiduMap;end");
                            startActivity(intent);
                        } else if (TextUtils.equals(mapInfoList.get(0).getMapname(), "高德地图")) {
                            Intent intent = Intent.getIntent("androidamap://route?sourceApplication=softname"
                                    + "&slat=" + Global.getMyLocation().latitude //
                                    + "&slon=" + Global.getMyLocation().longitude //
                                    + "&sname=" + Global.getMyLocation().address //
                                    + "&dlat=" + mapInfoList.get(0).getLatLng1().latitude //
                                    + "&dlon=" + mapInfoList.get(0).getLatLng1().longitude //
                                    + "&dname=" + mapInfoList.get(0).getName() //
                                    + "&dev=1" + "&m=2" + "&t=0");
                            intent.setPackage("com.autonavi.minimap");
                            startActivity(intent);
                        }
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                } else {
                    mapSelectView.selectMapApp(mapInfoList);
                }
                break;
            case R.id.map_dingwei:
                GaoDeMap.getInstance().startMap();
                break;
            //            case R.id.button1:
//                GaodeRouteActivity.open(GaodeMapActivity.this, latitude, longitude, merchantaddress);
//                break;
//            case R.id.button2:
//                List<MapInfo> mapInfoList = gaodeMapPresenter.checkAppInstall(latitude, longitude, merchantaddress, merchantname);
//                if (mapInfoList.size() == 0) {
//                    Toast.makeText(this, "请安装百度地图或高德地图！", Toast.LENGTH_SHORT).show();
//                    return;
//                }
//                mapSelectView.selectMapApp(mapInfoList);
//                break;
            default:
                break;
        }
    }


    /**
     * 方法必须重写
     */
    @Override
    protected void onResume() {
        gaodeBmapView.onResume();
        super.onResume();
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onPause() {
        gaodeBmapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        gaodeMapPresenter.onDestroyMap();
        gaodeBmapView.onDestroy();
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(LocationResultEvent event) {
        addMarkersToMap();
    }

}
