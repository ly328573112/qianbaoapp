package com.haodaibao.fanbeiapp.module.merchant.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.LableInfo;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 开发者：LuoYi
 * Time: 2018 10:54 2018/4/12 04
 */

public class DiscountDialogAdapter extends BaseRecycleViewAdapter<LableInfo> {

    private LayoutInflater inflater;

    public DiscountDialogAdapter(Context context) {
        super(context);
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, LableInfo item) {
        if (holder instanceof DiscountDialogHolder) {
            DiscountDialogHolder dialogHolder = (DiscountDialogHolder) holder;
            dialogHolder.labelTitleTv.setText(item.getLabelname());
            dialogHolder.labelContentTv.setText(item.getLabelremark());
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.dialog_discount, null);
        return new DiscountDialogHolder(view);
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class DiscountDialogHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.label_title_tv)
        TextView labelTitleTv;
        @BindView(R.id.label_content_tv)
        TextView labelContentTv;

        public DiscountDialogHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
