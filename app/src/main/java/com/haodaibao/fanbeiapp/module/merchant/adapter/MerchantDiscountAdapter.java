package com.haodaibao.fanbeiapp.module.merchant.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.utils.SpannableStringUtils;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.PriedListBean;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 开发者：LuoYi
 * Time: 2017 11:23 2017/10/10 10
 */

public class MerchantDiscountAdapter extends BaseRecycleViewAdapter<PriedListBean> {

    private OnItemClickListener onItemClickListener;

    public MerchantDiscountAdapter(Context context, OnItemClickListener onItemClickListener) {
        super(context);
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, final PriedListBean item) {
        DiscountHolder discountHolder = (DiscountHolder) holder;
//        discountHolder.tv_new_user_lable.setVisibility(TextUtils.equals(item.adaptType, "1") ? View.VISIBLE : View.GONE);
        SpannableStringUtils.Builder builder = SpannableStringUtils.getBuilder(TextUtils.equals(item.adaptType, "1") ? "新顾客专享  " : "");
        if (TextUtils.equals(item.isShow, "1")) {
//             discountHolder.tv_new_user_lable.setTextColor(mContext.getResources().getColor(R.color.red));
//             discountHolder.tv_discount_type.setTextColor(mContext.getResources().getColor(R.color.c_252628));
            builder.setForegroundColor(mContext.getResources().getColor(R.color.red));
            builder.append(item.refundContent).setForegroundColor(mContext.getResources().getColor(R.color.c_252628));
            discountHolder.iv_discount_icon.setVisibility(View.VISIBLE);
        } else if (TextUtils.equals(item.isShow, "2")) {
//             discountHolder.tv_new_user_lable.setTextColor(mContext.getResources().getColor(R.color.c_fba9b0));
//             discountHolder.tv_discount_type.setTextColor(mContext.getResources().getColor(R.color.text_hint));
            builder.setForegroundColor(mContext.getResources().getColor(R.color.c_fba9b0));
            builder.append(item.refundContent).setForegroundColor(mContext.getResources().getColor(R.color.text_hint));
            discountHolder.iv_discount_icon.setVisibility(View.INVISIBLE);
        }
//         discountHolder.tv_discount_type.setText(item.refundContent);
        discountHolder.tv_new_user_lable.setText(builder.create());
        discountHolder.tv_discount_time.setText(item.dateStr);
        if (TextUtils.isEmpty(item.discountContent)) {
            discountHolder.tv_discount_explain.setVisibility(View.GONE);
        } else {
            discountHolder.tv_discount_explain.setVisibility(View.VISIBLE);
            discountHolder.tv_discount_explain.setText(item.discountContent);
        }

        discountHolder.view_discount.setVisibility((holder.getLayoutPosition() + 1) == mData.size() ? View.INVISIBLE : View.VISIBLE);

        discountHolder.discountRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(v, item);
                }
            }
        });
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new DiscountHolder(View.inflate(mContext, R.layout.layout_merchant_discount, null));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class DiscountHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.discount_rl)
        RelativeLayout discountRl;
        @BindView(R.id.iv_discount_icon)
        ImageView iv_discount_icon;
        @BindView(R.id.tv_new_user_lable)
        TextView tv_new_user_lable;
        @BindView(R.id.tv_discount_type)
        TextView tv_discount_type;
        @BindView(R.id.tv_discount_time)
        TextView tv_discount_time;
        @BindView(R.id.view_discount)
        View view_discount;
        @BindView(R.id.tv_discount_explain)
        TextView tv_discount_explain;

        DiscountHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
