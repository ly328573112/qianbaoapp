package com.haodaibao.fanbeiapp.module.yaohe;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baseandroid.utils.FormatUtil;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.bumptech.glide.Glide;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.merchant.MerchantDetailActivity;
import com.haodaibao.fanbeiapp.repository.json.YaoheDetailsBean;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 开发者：LuoYi
 * Time: 2017 13:40 2017/10/11 10
 */

public class YaoheDetailsAdapter extends BaseRecycleViewAdapter<YaoheDetailsBean.MessageBody> {

    public YaoheDetailsAdapter(Context context) {
        super(context);
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, final YaoheDetailsBean.MessageBody model) {
        final YaoheHolder yaoheHolder = (YaoheHolder) holder;
        if (model.getType().toUpperCase().equals("TITLE")) {
            yaoheHolder.ll_preview_title.setVisibility(View.VISIBLE);
            yaoheHolder.ll_preview_text.setVisibility(View.GONE);
            yaoheHolder.ll_image.setVisibility(View.GONE);
            yaoheHolder.tv_title.setText(model.getContent());
            yaoheHolder.tv_time.setText(FormatUtil.changeTime(model.getTime(), "yyyy-MM-dd"));
            yaoheHolder.tv_merchantname.setText(model.getMerchantName());
            yaoheHolder.tv_merchantname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle b = new Bundle();
                    b.putString("merchantno", model.getMerNo());
                    ActivityJumpUtil.next((YaoheActivity) mContext, MerchantDetailActivity.class, b, 0);
                }
            });
        } else if (model.getType().toUpperCase().equals("TEXT")) {
            yaoheHolder.ll_preview_title.setVisibility(View.GONE);
            yaoheHolder.ll_preview_text.setVisibility(View.VISIBLE);
            yaoheHolder.ll_image.setVisibility(View.GONE);
            yaoheHolder.tv_text.setText(model.getContent());
        } else if (model.getType().toUpperCase().equals("IMAGE")) {
            yaoheHolder.ll_preview_title.setVisibility(View.GONE);
            yaoheHolder.ll_preview_text.setVisibility(View.GONE);
            yaoheHolder.ll_image.setVisibility(View.VISIBLE);

            Glide.with(mContext)
                    .load(model.getContent())
                    .asBitmap()
                    .fitCenter()
                    .into(yaoheHolder.iv_image);
        } else {
            yaoheHolder.ll_preview_title.setVisibility(View.GONE);
            yaoheHolder.ll_preview_text.setVisibility(View.GONE);
            yaoheHolder.ll_image.setVisibility(View.GONE);
        }

    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new YaoheHolder(LayoutInflater.from(mContext).inflate(R.layout.item_preview_effect, parent, false));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class YaoheHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ll_preview_title)
        LinearLayout ll_preview_title;
        @BindView(R.id.ll_preview_text)
        LinearLayout ll_preview_text;
        @BindView(R.id.ll_image)
        LinearLayout ll_image;
        @BindView(R.id.tv_title)
        TextView tv_title;
        @BindView(R.id.tv_time)
        TextView tv_time;
        @BindView(R.id.tv_text)
        TextView tv_text;
        @BindView(R.id.iv_image)
        ImageView iv_image;
        @BindView(R.id.tv_merchantname)
        TextView tv_merchantname;

        public YaoheHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
