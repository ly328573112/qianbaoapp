package com.haodaibao.fanbeiapp.module.map;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.LocationSource;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MyLocationStyle;
import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Global;
import com.baseandroid.gaode.GaoDeMap;
import com.haodaibao.fanbeiapp.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 开发者：LuoYi
 * Time: 2017 13:51 2017/9/13 09
 */

public class GaodeMapPresenter implements GaodeMapContract.Presenter, AMap.OnMarkerClickListener, AMap.OnMapLoadedListener, LocationSource {

    private final GaodeMapActivity mapActivity;
    private LocationSource.OnLocationChangedListener mListener;
    private Object aMapLocation;
    private AMap aMap;

    public GaodeMapPresenter(GaodeMapContract.GaodeMapView gaodeMapView) {
        mapActivity = (GaodeMapActivity) gaodeMapView;
    }

    @Override
    public AMap initMapView(AMap aMap) {
        this.aMap = aMap;
        // 自定义系统定位小蓝点
//        setMapCustomStyleFile(mapActivity);
        //		setMapCustomTextureFile(this);

        //开启自定义样式
        aMap.setMapCustomEnable(true);
        aMap.getUiSettings().setTiltGesturesEnabled(false);//禁用双手拖拽3D效果
        aMap.getUiSettings().setRotateGesturesEnabled(false);
        MyLocationStyle myLocationStyle = new MyLocationStyle();
        myLocationStyle.myLocationIcon(BitmapDescriptorFactory.fromResource((R.drawable.location_marker)));// 设置小蓝点的图标
        aMap.setMyLocationStyle(myLocationStyle);
        aMap.moveCamera(CameraUpdateFactory.zoomTo(13.8f));//设置地图缩放
        aMap.setOnMapLoadedListener(this);// 设置amap加载成功事件监听器
        aMap.setOnMarkerClickListener(this);// 设置点击marker事件监听器
        aMap.setLocationSource(this);// 设置定位监听
        aMap.getUiSettings().setMyLocationButtonEnabled(false);// 设置默认定位按钮是否显示
        aMap.getUiSettings().setCompassEnabled(false);
        aMap.getUiSettings().setLogoBottomMargin(-100);
        aMap.getUiSettings().setZoomControlsEnabled(false);
        aMap.setMyLocationEnabled(true);// 设置为true表示显示定位层并可触发定位，false表示隐藏定位层并不可触发定位，默认是false
        return aMap;
    }

    @Override
    public List<MapInfo> checkAppInstall(String latitude, String longitude, String merchantaddress, String merchantname) {
        List<MapInfo> list = new ArrayList<>();
        LatLng latLng1 = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
        if (isAvilible(mapActivity, "com.baidu.BaiduMap")) {// 百度地图
            MapInfo info = new MapInfo("百度地图", merchantname, merchantaddress, latLng1);
            list.add(info);
        }
        if (isAvilible(mapActivity, "com.autonavi.minimap")) {// 高德地图{
            MapInfo info = new MapInfo("高德地图", merchantname, merchantaddress, latLng1);
            list.add(info);
        }
        return list;
    }

    /**
     * 检查手机上是否安装了指定的软件
     *
     * @param context
     * @param packageName ：应用包名
     * @return
     */
    private boolean isAvilible(Context context, String packageName) {
        // 获取packagemanager
        final PackageManager packageManager = context.getPackageManager();
        // 获取所有已安装程序的包信息
        List<PackageInfo> packageInfos = packageManager.getInstalledPackages(0);
        // 用于存储所有已安装程序的包名
        List<String> packageNames = new ArrayList<String>();
        // 从pinfo中将包名字逐一取出，压入pName list中
        if (packageInfos != null) {
            for (int i = 0; i < packageInfos.size(); i++) {
                String packName = packageInfos.get(i).packageName;
                packageNames.add(packName);
            }
        }
        // 判断packageNames中是否有目标程序的包名，有TRUE，没有FALSE
        return packageNames.contains(packageName);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMapLoaded() {
        if (Global.getMyLocation() == null) {
            GaoDeMap.getInstance().startMap();
        } else {
            if (Global.getMyLocation().getLatitude() == 0) {
                GaoDeMap.getInstance().startMap();
            } else {
                mapActivity.getLocateHandler().sendEmptyMessage(1);
            }
        }
    }

    @Override
    public void activate(OnLocationChangedListener onLocationChangedListener) {
        this.mListener = onLocationChangedListener;
    }

    @Override
    public void deactivate() {
        mListener = null;
    }


    @Override
    public void onDestroyMap() {
        deactivate();
    }

    private void setMapCustomStyleFile(Context context) {
        //        String styleName = "style_json_texture.json";
        String styleName = "style.data";
        FileOutputStream outputStream = null;
        InputStream inputStream = null;
        String filePath = null;
        try {
            inputStream = context.getAssets().open(styleName);
            byte[] b = new byte[inputStream.available()];
            inputStream.read(b);

            filePath = context.getFilesDir().getAbsolutePath();
            File file = new File(filePath + "/" + styleName);
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            outputStream = new FileOutputStream(file);
            outputStream.write(b);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        aMap.setCustomMapStylePath(filePath + "/" + styleName);

    }
}
