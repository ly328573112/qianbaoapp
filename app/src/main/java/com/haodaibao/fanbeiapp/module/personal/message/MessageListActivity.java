package com.haodaibao.fanbeiapp.module.personal.message;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.StatusBarHelper;
import com.flyco.tablayout.SlidingTabLayout;
import com.haodaibao.fanbeiapp.R;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class MessageListActivity extends BaseActivity {

    @BindView(R.id.toolbar_back)
    FrameLayout mToolbarBack;
    @BindView(R.id.tl)
    SlidingTabLayout mTl;
    @BindView(R.id.vp)
    ViewPager mVp;
    @BindView(R.id.bt_read_all)
    Button mBtSelectAll;
    @BindView(R.id.tv_right_title)
    TextView mTvRightTitle;
    @BindView(R.id.ll_bottom)
    LinearLayout mLlBottom;

    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private String[] mTitleList = {"全部", "提醒", "优惠", "系统"};
    private String[] mTypeList = {"ALL", "01", "02", "03"};
    private boolean isEdit;
    private MessageFragment mMsgAllFragment;
    private MessageFragment mMsgNotifyFragment;
    private MessageFragment mMsgDiscountFragment;
    private MessageFragment mMsgSystemFragment;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_message_list;
    }

    public boolean isEdit() {
        return isEdit;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(MessageListActivity.this);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        initFragments();
        mTl.setViewPager(mVp, mTitleList, this, mFragments);
        mVp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (mFragments != null && mFragments.size() != 0) {
                    MessageFragment fragment = (MessageFragment) mFragments.get(position);
                    calEditable(fragment);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    //判断是否可编辑
    public void calEditable(MessageFragment fragment) {
        if (mTypeList[mVp.getCurrentItem()].equals(fragment.getType())) {
            if (fragment.checkEditable()) {
                mTvRightTitle.setVisibility(View.VISIBLE);
                if (isEdit) {
                    if (mLlBottom.getVisibility() != View.VISIBLE) {
                        mLlBottom.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (mLlBottom.getVisibility() != View.GONE) {
                        mLlBottom.setVisibility(View.GONE);
                    }
                }
            } else {
                if (mTvRightTitle.getVisibility() != View.GONE) {
                    mTvRightTitle.setVisibility(View.GONE);
                }
                if (mLlBottom.getVisibility() != View.GONE) {
                    mLlBottom.setVisibility(View.GONE);
                }
            }
            if (mLlBottom.getVisibility() == View.VISIBLE) {
                int height = mLlBottom.getHeight();
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mVp.getLayoutParams();
                params.setMargins(0, 0, 0, height);
                mVp.setLayoutParams(params);
            } else {
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mVp.getLayoutParams();
                params.setMargins(0, 0, 0, 0);
                mVp.setLayoutParams(params);
            }
        }
    }

    private void initFragments() {
        mFragments.clear();

        mMsgAllFragment = new MessageFragment();
        Bundle allBubdle = new Bundle();
        allBubdle.putString("type", "ALL");
        mMsgAllFragment.setArguments(allBubdle);

        mMsgNotifyFragment = new MessageFragment();
        Bundle notifyBundle = new Bundle();
        notifyBundle.putString("type", "01");
        mMsgNotifyFragment.setArguments(notifyBundle);

        mMsgDiscountFragment = new MessageFragment();
        Bundle discountBundle = new Bundle();
        discountBundle.putString("type", "02");
        mMsgDiscountFragment.setArguments(discountBundle);

        mMsgSystemFragment = new MessageFragment();
        Bundle systemBundle = new Bundle();
        systemBundle.putString("type", "03");
        mMsgSystemFragment.setArguments(systemBundle);

        mFragments.add(mMsgAllFragment);
        mFragments.add(mMsgNotifyFragment);
        mFragments.add(mMsgDiscountFragment);
        mFragments.add(mMsgSystemFragment);
    }

    @OnClick({R.id.toolbar_back, R.id.tv_right_title, R.id.bt_read_all, R.id.bt_del})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.bt_read_all:
                if (mFragments == null) {
                    return;
                }
                MessageFragment frag = (MessageFragment) mFragments.get(mVp.getCurrentItem());
                frag.setMsgReaded();
                break;
            case R.id.bt_del:
                if (mFragments == null) {
                    return;
                }
                MessageFragment deleteFrag = (MessageFragment) mFragments.get(mVp.getCurrentItem());
                deleteFrag.deleteMsg();
                break;
            case R.id.toolbar_back:
                ActivityJumpUtil.goBack(MessageListActivity.this);
                break;
            case R.id.tv_right_title:
                isEdit = !isEdit;
                for (Fragment fragment : mFragments) {
                    MessageFragment f = (MessageFragment) fragment;
                    f.notifyEditable(isEdit);
                }
                mTvRightTitle.setText(isEdit ? "取消" : "编辑");
                calEditable((MessageFragment) mFragments.get(mVp.getCurrentItem()));
                break;
        }
    }
}
