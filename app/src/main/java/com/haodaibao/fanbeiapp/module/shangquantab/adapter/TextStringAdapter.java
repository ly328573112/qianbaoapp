package com.haodaibao.fanbeiapp.module.shangquantab.adapter;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 开发者：LuoYi
 * Time: 2017 19:31 2017/12/11 12
 */

public class TextStringAdapter extends BaseRecycleViewAdapter<String> {

    private int normalDrawbleId;
    private int selectedTextColor = -1;
    private Drawable selectedDrawble;
    private float textSize;
    private String selectedText = "";
    private int selectedPos = -1;
    private OnPositionClickListener onPositionClickListener;

    public TextStringAdapter(Context context, int sIdText, int sId, int nId) {
        super(context);
        selectedTextColor = mContext.getResources().getColor(sIdText);
        selectedDrawble = mContext.getResources().getDrawable(sId);
        normalDrawbleId = nId;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, String item) {
        if (holder instanceof TextHolder) {
            TextHolder textHolder = (TextHolder) holder;
            if (item.contains("不限")) {
                textHolder.contentText.setText("不限");
            } else {
                textHolder.contentText.setText(item);
            }

            textHolder.contentText.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) textHolder.rightIcon.getLayoutParams();
            params.width = getTextViewWidth(textHolder.contentText, item);
            textHolder.rightIcon.setLayoutParams(params);

            if (selectedText != null && selectedText.equals(item)) {
                textHolder.rightIcon.setVisibility(View.INVISIBLE);
                textHolder.contentText.setTextColor(selectedTextColor);// 设置选中的字体颜色
            } else {
                textHolder.rightIcon.setVisibility(View.INVISIBLE);
                textHolder.contentText.setTextColor(mContext.getResources().getColor(R.color.text_gray3));
            }
            int position = holder.getLayoutPosition();
            textHolder.itemView.setOnClickListener(new AdapterClick(position, item));
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new TextHolder(View.inflate(mContext, R.layout.choose_item1, null));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    /**
     * 设置列表字体大小
     */
    public void setTextSize(float tSize) {
        textSize = tSize;
    }

    private int getTextViewWidth(TextView textView, String text) {
        Rect rect = new Rect();
        textView.getPaint().getTextBounds(text, 0, text.length(), rect);
        int width = rect.width();
        return width;
    }

    /**
     * 设置选中的position,并通知列表刷新
     */
    public void setSelectedPosition(int pos, String selectedText) {
        selectedPos = pos;
        this.selectedText = selectedText;
        notifyDataSetChanged();
    }

    /**
     * 设置选中的position,但不通知刷新
     */
    public void setSelectedPositionNoNotify(int pos, String selectedText) {
        selectedPos = pos;
        this.selectedText = selectedText;
    }

    public void setOnItemClickListener(OnPositionClickListener l) {
        onPositionClickListener = l;
    }

    class AdapterClick implements View.OnClickListener {

        int position;
        String selectedText;

        AdapterClick(int position, String selectedText) {
            this.position = position;
            this.selectedText = selectedText;
        }

        @Override
        public void onClick(View view) {
            selectedPos = position;
            setSelectedPosition(selectedPos, selectedText);
            if (onPositionClickListener != null) {
                onPositionClickListener.onItemClick(view, selectedPos);
            }
        }
    }

    class TextHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.content_text)
        TextView contentText;
        @BindView(R.id.right_icon)
        ImageView rightIcon;

        TextHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
