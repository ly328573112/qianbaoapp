package com.haodaibao.fanbeiapp.module.personal.message;

import com.haodaibao.fanbeiapp.repository.json.MessageListBean;

/**
 * Created by Routee on 2017/9/21.
 * description: ${cusor}
 */

public interface MessageListContract {
    interface View {
        void setupMessageListData(MessageListBean bean);

        void setMsgReadedSuccess();

        void deleteMsgSuccess();
    }

    interface Presenter {
        MessageJumpBean messageJump(MessageListBean.ResultBean.ItemsBean itemsBean);

        void getMessageList(String page, String type);

        void setMsgReaded(String msgNo);

        void deleteMsg(String msgNo);
    }
}
