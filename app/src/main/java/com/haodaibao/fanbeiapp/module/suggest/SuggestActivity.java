package com.haodaibao.fanbeiapp.module.suggest;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.photocameralib.media.ImageGalleryActivity;
import com.baseandroid.base.BaseActivity;
import com.baseandroid.camera.CameraUtil;
import com.baseandroid.config.Global;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.EmojiUtil;
import com.baseandroid.utils.ToastUtils;
import com.baseandroid.widget.customtext.MaxLengthWatcher;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.fileuploading.FileUploaderManager;
import com.haodaibao.fanbeiapp.imageadapter.CameraSelectView;
import com.haodaibao.fanbeiapp.imageadapter.ImageClickListener;
import com.haodaibao.fanbeiapp.imageadapter.PhotoImageAdapter;
import com.haodaibao.fanbeiapp.module.dialog.MessageDialog;
import com.haodaibao.fanbeiapp.repository.json.PhotoInfo;
import com.haodaibao.fanbeiapp.repository.json.SuggestBean;
import com.haodaibao.fanbeiapp.repository.json.SuggestInfo;
import com.haodaibao.fanbeiapp.repository.json.UploadResult;
import com.jaeger.library.StatusBarUtil;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * date: 2017/9/26.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class SuggestActivity extends BaseActivity implements ImageClickListener, CameraSelectView.CameraPotoListener, SuggestContract.View {
    @BindView(R.id.iv_back_suggest)
    protected ImageView iv_back_suggest;
    @BindView(R.id.bt_submit)
    protected TextView bt_submit;
    @BindView(R.id.et_comment)
    protected EditText et_comment;
    @BindView(R.id.tv_count)
    protected TextView tv_count;
    @BindView(R.id.gv_getphoto)
    protected RecyclerView gv_getphoto;
    @BindView(R.id.suggestlist)
    protected RecyclerView suggestlist;

    private SuggestPresenter presenter;
    private PhotoImageAdapter imageAdapter;
    private List<PhotoInfo> photoList = new ArrayList<>();// 照片uri列表
    private int currentItemId = 1;// 加号
    private int COLUMN = 5;//图片可选数量
    private int imageColumn = 1;//图片集合是否带有加号
    private CameraUtil cameraUtil;
    private CameraSelectView selectView;
    private FileUploaderManager uploaderManager;
    private MessageDialog messageDialog;
    private SuggestAdapter adapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_suggest;
    }

    @OnClick({R.id.iv_back_suggest, R.id.bt_submit})
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back_suggest:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                ActivityJumpUtil.goBack(SuggestActivity.this);
                break;
            case R.id.bt_submit:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                String text = et_comment.getText().toString();
                text = EmojiUtil.replaceEmoji(text);
                if (TextUtils.isEmpty(text)) {
                    showToast("请输入反馈意见");
                    return;
                }

                bt_submit.setEnabled(false);
                showUploading();
                if (photoList.size() > 1) {
                    if (uploaderManager == null) {
                        uploaderManager = FileUploaderManager.getInstance();
                    }
                    uploaderManager.updateImage(photoList, uploaderResult);
                } else {
                    StringBuffer imgUriBuffer = new StringBuffer();// 拼接上传图片名称路径
                    String s = imgUriBuffer.toString();
                    processUploading("提交中");
                    presenter.submitSuggest(Global.getUserInfo().getUserno(), text, s);
                }
                break;
        }
    }

    @Override
    protected void setupView() {
        StatusBarUtil.setTranslucentForImageViewInFragment(this, 0, null);
        presenter = new SuggestPresenter(this);
        selectView = new CameraSelectView(this);
        selectView.setPictureCut(false);
        cameraUtil = new CameraUtil(this);
        photoList.clear();
        PhotoInfo info = new PhotoInfo(currentItemId, null, false);
        photoList.add(info);
        et_comment.addTextChangedListener(new MaxLengthWatcher(500, et_comment, tv_count));
        initListView(gv_getphoto);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        presenter.getSuggestList(Global.getUserInfo().getUserno());
        adapter = new SuggestAdapter(this);
        suggestlist.setLayoutManager(new LinearLayoutManager(this));
        suggestlist.setAdapter(adapter);
        suggestlist.setNestedScrollingEnabled(false);
    }

    private FileUploaderManager.UploaderResultListener uploaderResult = new FileUploaderManager.UploaderResultListener() {

        @Override
        public void onNetworkType(boolean cleanList) {
            closeUploading();
            if (cleanList) {
                uploaderManager.cleanFileList();
            }
            bt_submit.setEnabled(true);
            ToastUtils.showShortToastSafe("网络访问失败", 1000);
        }

        @Override
        public void onUploadProcessNumber(int successNumber, int fileListSize) {
            processUploading((successNumber + 1) + "/" + fileListSize);
        }

        @Override
        public void onUploaderFail(final List<String> failUrl, final int successNumber) {
            closeUploading();
            if (failUrl == null && successNumber == 0) {
                ToastUtils.showShortToastSafe("图片上传终止,请检查网络是否连接!", 1000);
                return;
            }
            if (messageDialog == null) {
                messageDialog = new MessageDialog().newInstance();
            }
            if (!messageDialog.isVisible()) {
                messageDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        messageDialog.hideTitle();
                        messageDialog.setContent("有" + failUrl.size() + "张图片上传失败,请点击继续上传！");
                        messageDialog.setConfirmText("继续上传");
                        messageDialog.setConfirmOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showUploading();
                                uploaderManager.uploaderStringFile(failUrl, successNumber);
                                messageDialog.dismiss();
                            }
                        });
                        messageDialog.setCancelOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                bt_submit.setEnabled(true);
                                uploaderManager.cleanFileList();
                                messageDialog.dismiss();
                            }
                        });
                    }
                });

                messageDialog.show(getSupportFragmentManager(), "MessageDialog");
            }
        }

        @Override
        public void onUploaderSuccess(List<UploadResult.UploadInfo> uploadFileList) {
            closeAnimatorUploading();
            StringBuffer imgUriBuffer = new StringBuffer();// 拼接上传图片名称路径
            for (UploadResult.UploadInfo uploadInfo : uploadFileList) {
                imgUriBuffer.append(uploadInfo.getName());
                imgUriBuffer.append(",");
            }
            String s = imgUriBuffer.toString();
            String text = et_comment.getText().toString();
            text = EmojiUtil.replaceEmoji(text);
            presenter.submitSuggest(Global.getUserInfo().getUserno(), text, s);
        }
    };

    private void initListView(RecyclerView comment_gridview) {
        GridLayoutManager layoutManager = new GridLayoutManager(this, 4, LinearLayoutManager.VERTICAL, false);
        comment_gridview.setLayoutManager(layoutManager);
        comment_gridview.setNestedScrollingEnabled(false);
        comment_gridview.setHasFixedSize(true);
        imageAdapter = new PhotoImageAdapter(this);
        comment_gridview.setAdapter(imageAdapter);
        comment_gridview.setItemAnimator(new DefaultItemAnimator());
        imageAdapter.setImageClickListener(this);
        imageAdapter.addData(photoList);
    }

    @Override
    public void openCamera(int position) {
        int photoNumber = photoList.get(position).getPhotoNumber();
        if (photoNumber == currentItemId) {
            int optionalNumber = COLUMN - (photoList.size() - imageColumn);
            selectView.showCameraDialog(cameraUtil, optionalNumber);
            selectView.setCameraPotoListener(this);
        } else if (photoNumber == 0) {
            String[] strings = new String[photoList.size() - imageColumn];
            for (int i = 0; i < photoList.size() - imageColumn; i++) {
                strings[i] = photoList.get(i).getPhotoUri();
            }
            ImageGalleryActivity.show(this, strings, position, false);
        }
    }

    @Override
    public void onDeleteImage(int position) {
        photoList.remove(position);
        confineListSize();
        imageAdapter.resetData(photoList);
    }

    @Override
    public void onResultPotoList(List<PhotoInfo> imgList) {
        this.photoList.addAll(imgList);
        confineListSize();
        imageAdapter.resetData(photoList);
    }

    /**
     * 加号始终放在List最后
     */
    private void confineListSize() {
        for (PhotoInfo photoInfo : photoList) {
            if (photoInfo.getPhotoNumber() == currentItemId) {
                photoList.remove(photoInfo);
                break;
            }
        }
        if (photoList.size() < COLUMN) {
            PhotoInfo info = new PhotoInfo(currentItemId, null, false);
            photoList.add(info);
            imageColumn = 1;
        } else {
            imageColumn = 0;
        }
    }

    @Override
    public void subSucc(SuggestBean suggestBean) {
        closeUploading();
        finish();
    }

    @Override
    public void updateList(List<SuggestInfo> list) {
        adapter.addData(list);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        cameraUtil.onHandleActivityResult(requestCode, resultCode, data);
    }
}
