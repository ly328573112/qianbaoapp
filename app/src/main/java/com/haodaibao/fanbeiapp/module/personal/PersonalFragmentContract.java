package com.haodaibao.fanbeiapp.module.personal;

import com.haodaibao.fanbeiapp.event.PersonalFragmentEvent;
import com.haodaibao.fanbeiapp.repository.json.BalanceBean;
import com.haodaibao.fanbeiapp.repository.json.BhhInfoBean;
import com.haodaibao.fanbeiapp.repository.json.BhhStatueBean;
import com.haodaibao.fanbeiapp.repository.json.CouponBean;
import com.haodaibao.fanbeiapp.repository.json.UserDate;

/**
 * Created by Routee on 2017/9/19.
 * description: ${cusor}
 */

public interface PersonalFragmentContract {
    interface View {
        void setupBhhStatueData(BhhStatueBean bean);

        void getBhhStatueFailed();

        void setupUserInfoData(UserDate bean);

        void setupBalanceData(BalanceBean bean);

        void setupCouponCountData(CouponBean bean);

        void setUnReadMsgView(String unReadNumber);

        void updateBhhInfo(BhhInfoBean bhhInfoBean);

        void getBhhInfoFailed();
    }

    interface Presenter {
        void getBhhStatue();

        void getBalance();

        void getUserInfo();

        void getCouponsCount();

        void getUnReadMsgsNum();

        void getMyBhhInfo(String token);
    }

    interface MenuView {

        void setMenuLayout();

        void initMenuData();

        void onOpenMenu();

        void onCloseMenu();

        void onLoginEvent(PersonalFragmentEvent event);

        void onUnRadMsg();

        void backPressed();

        boolean keyDown();

        void destroy();

    }
}
