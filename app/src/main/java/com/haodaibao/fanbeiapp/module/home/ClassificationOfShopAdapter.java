package com.haodaibao.fanbeiapp.module.home.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.baseandroid.config.Api;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.FormatUtil;
import com.baseandroid.widget.RouteeFlowLayout;
import com.bumptech.glide.Glide;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.home.OnItemClickShopListener;
import com.haodaibao.fanbeiapp.repository.json.MerchantsBean;
import com.haodaibao.fanbeiapp.repository.json.ShopKindsBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * created by 曹伟 on 2018/05/11
 * e-mail : caowei@qianbaocard.com
 * pro : 首页展示城市没事分类的九宫格适配器
 */

public class ClassificationOfShopAdapter extends RecyclerView.Adapter<ClassificationOfShopAdapter.ViewHolder> implements View.OnClickListener {

    private Context mContext ;
    private List<ShopKindsBean> shopKindsBeans = new ArrayList<>();
    private OnItemClickShopListener mOnItemClickListener;//声明接口

    public ClassificationOfShopAdapter(Context context, List<ShopKindsBean> shopImages,OnItemClickShopListener mOnItemClickListener) {
        mContext = context ;
        this.shopKindsBeans = shopImages ;
        this.mOnItemClickListener = mOnItemClickListener ;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.item_grid_shop,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(this);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        String imageurl = Api.sImageDoneUrl+shopKindsBeans.get(position).getImage();
//        String imageurl = CommonUtils.getImageUrl(shopKindsBeans.get(position).getImage());
        Glide.with(mContext).load(imageurl)
                .asBitmap().placeholder(R.mipmap.shop_icon_default)
                .error(R.mipmap.shop_icon_default)
                .into(holder.shop_icon_img);
        holder.shop_kind_tv.setText(shopKindsBeans.get(position).getName());
        holder.shop_kind_sale.setText(Html.fromHtml("<big><big><font color='#F62241'>"
                + FormatUtil.formatAmount(1, "" + (1 - Double.valueOf(shopKindsBeans.get(position).getReturnrate())) * 10) + "</font></big></big>" + "折起"));
        setShopView(holder.item_shop_name_rfl,shopKindsBeans.get(position));

        (holder.itemView).setTag(position);
    }

    /**
     * 动态设置最近商铺名称
     * @param item_shop_name_rfl
     * @param shopKindsBean
     */
    private void setShopView(RouteeFlowLayout item_shop_name_rfl, ShopKindsBean shopKindsBean) {
        item_shop_name_rfl.setShowInLaseLine(true);
        item_shop_name_rfl.removeAllViews();
        item_shop_name_rfl.setVerticalSpacing(8);
        item_shop_name_rfl.setHorizontalSpacing(20);
        for (final MerchantsBean bean : shopKindsBean.getMerchants()) {
            TextView itemNewPackge = (TextView) LayoutInflater.from(mContext)
                    .inflate(R.layout.item_shop_name, null);
            TextView packageNameTv = itemNewPackge.findViewById(R.id.item_package_name_tv);
            packageNameTv.setText(bean.getMerchantname());
            item_shop_name_rfl.addView(itemNewPackge);
        }
    }

    @Override
    public int getItemCount() {
        return shopKindsBeans.size();
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener!=null){
            mOnItemClickListener.onItemClick((Integer) v.getTag());
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
       @BindView(R.id.shop_icon_img)
       ImageView shop_icon_img ;    //分类一级图片
       @BindView(R.id.shop_kind_tv)
       TextView shop_kind_tv ;   //类别名称
       @BindView(R.id.item_shop_name_rfl)
       RouteeFlowLayout item_shop_name_rfl ;
       @BindView(R.id.shop_kind_sale)
       TextView shop_kind_sale ;    //最低折扣

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setItemClickListener(OnItemClickShopListener itemClickListener) {
        mOnItemClickListener = itemClickListener;
    }

    public List<ShopKindsBean> getShopKindsBeans() {
        return shopKindsBeans;
    }

    public void setShopKindsBeans(List<ShopKindsBean> shopKindsBeans) {
        this.shopKindsBeans = shopKindsBeans;
    }
}
