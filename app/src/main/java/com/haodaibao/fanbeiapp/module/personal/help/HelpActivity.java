package com.haodaibao.fanbeiapp.module.personal.help;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.StatusBarHelper;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.HelpBean;
import com.haodaibao.fanbeiapp.repository.json.HelpInterface;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class HelpActivity extends BaseActivity implements HelpContract.View {
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.rv)
    RecyclerView mRv;
    HelpPresenter mPresenter;
    private String mTitle = "";
    private String mUid = "";
    private HelpRvAdapter mRvAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_help;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(HelpActivity.this);
        initIntent();
        if (mRvAdapter == null) {
            mRvAdapter = new HelpRvAdapter(this);
        }
        mRv.setAdapter(mRvAdapter);
        mRv.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initIntent() {
        mTitle = getIntent().getStringExtra("title");
        mUid = getIntent().getStringExtra("uid");
        if (TextUtils.isEmpty(mTitle)) {
            mToolbarTitle.setText("帮助中心");
        } else {
            mToolbarTitle.setText(mTitle);
        }
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        getHelpList();
    }

    @OnClick(R.id.toolbar_back)
    public void onViewClicked() {
        finish();
    }

    public void getHelpList() {
        if (mPresenter == null) {
            mPresenter = new HelpPresenter(this);
        }
        mPresenter.getHelpList(mUid);
    }

    //获取帮助列表成功回调
    @Override
    public void setUpHelpListData(HelpBean bean) {
        List<HelpInterface> list = new ArrayList();
        for (int i = 0; i < bean.getHelpList().size(); i++) {
            if (bean.getHelpList().size() > 1) {
                list.add(bean.getHelpList().get(i));
            }
            for (int j = 0; j < bean.getHelpList().get(i).getHelpContentList().size(); j++) {
                list.add(bean.getHelpList().get(i).getHelpContentList().get(j));
            }
        }
        mRvAdapter.resetData(list);
    }

    //设置空页面
    @Override
    public void setEmptyView() {

    }
}
