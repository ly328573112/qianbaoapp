package com.haodaibao.fanbeiapp.module.search.view;

public interface ViewBaseAction {

    /**
     * 菜单隐藏操作
     */
    void hide();

    /**
     * 菜单显示操作
     */
    void show();
}
