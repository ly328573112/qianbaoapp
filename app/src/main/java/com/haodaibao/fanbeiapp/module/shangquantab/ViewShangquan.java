package com.haodaibao.fanbeiapp.module.shangquantab;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.baseandroid.config.Global;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.shangquantab.adapter.TextAdapterShangquanLeft;
import com.haodaibao.fanbeiapp.module.shangquantab.adapter.TextAdapterShangquanRight;
import com.haodaibao.fanbeiapp.module.search.view.ViewBaseAction;
import com.haodaibao.fanbeiapp.repository.json.AreaList;
import com.haodaibao.fanbeiapp.repository.json.DistrictList;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ViewShangquan extends LinearLayout implements ViewBaseAction {

    private ListView regionListView;
    private ListView plateListView;
    private ArrayList<DistrictList> groups = new ArrayList<DistrictList>();
    private LinkedList<AreaList> childrenItem = new LinkedList<AreaList>();
    private SparseArray<LinkedList<AreaList>> children = new SparseArray<LinkedList<AreaList>>();
    private TextAdapterShangquanRight plateListViewAdapter;
    private TextAdapterShangquanLeft earaListViewAdapter;
    private OnSelectListener mOnSelectListener;
    private int tEaraPosition = 0;
    private int tBlockPosition = 0;
    private String showString = "全部";
    private List<DistrictList> districtList;

    public ViewShangquan(Context context, List<DistrictList> districtList) {
        super(context);
        init(context, districtList);
    }

    public ViewShangquan(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, districtList);
    }

    public void updateShowText(String showArea, String showBlock) {
        if (showArea == null || showBlock == null) {
            return;
        }
        for (int i = 0; i < groups.size(); i++) {
            if (groups.get(i).getName().equals(showArea)) {
                earaListViewAdapter.setSelectedPosition(i);
                childrenItem.clear();
                if (i < children.size()) {
                    childrenItem.addAll(children.get(i));
                }
                tEaraPosition = i;
                break;
            }
        }
        for (int j = 0; j < childrenItem.size(); j++) {
            if (childrenItem.get(j)
                    .getName()
                    .replace("不限", "")
                    .equals(showBlock.trim())) {
                plateListViewAdapter.setSelectedPosition(j);
                tBlockPosition = j;
                break;
            }
        }
        setDefaultSelect();
    }

    public void init(final Context context, List<DistrictList> dList) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_region, this, true);
        regionListView = (ListView) findViewById(R.id.listView);
        regionListView.setVerticalScrollBarEnabled(false);
        plateListView = (ListView) findViewById(R.id.listView2);
        plateListView.setVerticalScrollBarEnabled(false);
        if (districtList != null) {
            districtList.clear();
        } else {
            districtList = new ArrayList<>();
        }
        districtList.addAll(dList);
        if (groups != null) {
            groups.clear();
        }

        if (districtList != null && districtList.size() > 0) {
            // 区域选项中的全部
            groups.add(new DistrictList("", "全城", districtList.get(0).getCitycode(), null));
            // 点区域中的全部，显示在商圈中的全部
            LinkedList<AreaList> tItem = new LinkedList<>();
            tItem.add(new AreaList("", "全部", "", ""));
            children.put(0, tItem);

            for (int i = 0; i < districtList.size(); i++) {
                DistrictList district = this.districtList.get(i);
                // 添加区域中剩余区域
                DistrictList info = new DistrictList(
                        district.getCode(),
                        district.getName(),
                        district.getCitycode(),
                        district.getAreaList());
                groups.add(info);
                // 添加点击剩余区域每一项在商圈中的第一个全部
                LinkedList<AreaList> tItem1 = new LinkedList<AreaList>();
                if (!TextUtils.equals(district.getCode(), "0000")) {
                    tItem1.add(new AreaList("", "全部", district.getCode(), ""));
                }
                List<AreaList> list1 = district.getAreaList();
                for (int j = 0; j < list1.size(); j++) {
                    tItem1.add(list1.get(j));
                }
                children.put(i + 1, tItem1);
            }
        }

        earaListViewAdapter = new TextAdapterShangquanLeft(context, groups, R.color.c_F62241, R.drawable.choose_item_selector, R.drawable.choose_eara_item_selector);
        earaListViewAdapter.setTextSize(14);
        earaListViewAdapter.setSelectedPositionNoNotify(tEaraPosition);
        earaListViewAdapter.setTextPaddingLeft(20);
        regionListView.setAdapter(earaListViewAdapter);
        earaListViewAdapter.setOnItemClickListener(new TextAdapterShangquanLeft.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                if (position < children.size()) {
                    if (position == 0 && mOnSelectListener != null) {
                        mOnSelectListener.getValue("全城", "", "");
                        childrenItem.clear();
                        childrenItem.addAll(new LinkedList<AreaList>());
                        plateListViewAdapter.notifyDataSetChanged();
                        if (plateListViewAdapter != null) {
                            plateListViewAdapter.cleanRightData();
                        }
                    } else {
                        childrenItem.clear();
                        childrenItem.addAll(children.get(position));
                        plateListViewAdapter.notifyDataSetChanged();
                    }
                }
            }
        });
        if (tEaraPosition < children.size()) {
            if (tEaraPosition == 0) {
                LinkedList<AreaList> linkedList = children.get(tEaraPosition);
                if (linkedList.get(0).getName().equals("全部")) {
                    childrenItem.clear();
                    childrenItem.addAll(new LinkedList<AreaList>());
                }
            } else {
                childrenItem.addAll(children.get(tEaraPosition));
            }
        }

        plateListViewAdapter = new TextAdapterShangquanRight(context, childrenItem, R.color.c_F62241, R.drawable.choose_item_right, R.drawable.choose_plate_item_selector);
        plateListViewAdapter.setTextSize(14);
        plateListViewAdapter.setSelectedPositionNoNotify(tBlockPosition);
        plateListViewAdapter.setTextPaddingLeft(100);
        plateListView.setAdapter(plateListViewAdapter);
        plateListViewAdapter.setOnItemClickListener(new TextAdapterShangquanRight.OnItemClickListener() {

            @Override
            public void onItemClick(View view, final int position) {
                String areacode = childrenItem.get(position).getCode();
                String districtcode = childrenItem.get(position).getDistrictcode();// 所属区域的编号
                if (TextUtils.isEmpty(areacode)) {// 商圈选择全部，则返回区域名称
                    showString = getDistrictNameByCode(districtcode);
                    if (TextUtils.isEmpty(showString)) {
                        showString = "全部";
                    }
                } else {
                    String name = childrenItem.get(position).getName();
                    if (!TextUtils.isEmpty(name) && name.contains("附近（智能范围）")) {
                        showString = "附近";
                    } else {
                        showString = name;
                    }
                }

                if (mOnSelectListener != null) {
                    mOnSelectListener.getValue(showString, districtcode, areacode);
                }

            }
        });
        if (tBlockPosition < childrenItem.size()) {
            showString = childrenItem.get(tBlockPosition).getName();
        }
        if (showString.contains("全部")) {
            showString = showString.replace("全部", "");
        }
        setDefaultSelect();

    }

    public void setDefaultSelect() {
        regionListView.setSelection(tEaraPosition);
        plateListView.setSelection(tBlockPosition);
    }

    public String getShowText() {
        return showString;
    }

    public void setOnSelectListener(OnSelectListener onSelectListener) {
        mOnSelectListener = onSelectListener;
    }

    public interface OnSelectListener {
        void getValue(String showText, String districtcode, String areacode);
    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void show() {
        // TODO Auto-generated method stub

    }

    private String getDistrictNameByCode(String code) {
        String name = null;
        for (DistrictList district : Global.getShangquans()) {
            if (district.getCode().equals(code)) {
                name = district.getName();
                break;
            }
        }
        return name;
    }
}
