package com.haodaibao.fanbeiapp.module.dialog;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.view.WindowManager;
import android.widget.TextView;

import com.baseandroid.base.BaseDialog;
import com.haodaibao.fanbeiapp.R;
import com.jayfeng.lesscode.core.DisplayLess;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;

/**
 * date: 2018/2/11.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class InvoiceDialog extends BaseDialog {
    @BindView(R.id.iv_top)
    RoundedImageView iv_top;
    @BindView(R.id.invoice_make_statue_text)
    TextView invoice_make_statue_text;
    @BindView(R.id.invoice_make_statue_message)
    TextView invoice_make_statue_message;
    @BindView(R.id.invoice_make_btn)
    public TextView invoice_make_btn;

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_invoice;
    }

    @Override
    protected void setupView() {
        setCancelable(false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Full Screen Area
        getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        getDialog().getWindow().getDecorView().setBackgroundColor(Color.TRANSPARENT);
        getDialog().getWindow().getDecorView().setPadding(DisplayLess.$dp2px(0), DisplayLess.$dp2px(0), DisplayLess.$dp2px(0), DisplayLess.$dp2px(0));
        WindowManager.LayoutParams lp = getDialog().getWindow().getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes(lp);
        if (mOnShowListener != null) {
            getDialog().setOnShowListener(mOnShowListener);
        }
    }

    public void setIv_top(@DrawableRes int d) {
        iv_top.setImageResource(d);
    }

    public void setInvoice_make_statue_text(String s) {
        invoice_make_statue_text.setText(s);
    }

    public void setInvoice_make_statue_message(String s) {
        invoice_make_statue_message.setText(s);
    }

    public void setInvoice_make_btn(int show) {
        if (invoice_make_btn != null) {
            invoice_make_btn.setVisibility(show);
        }
    }

    public static InvoiceDialog newInstance() {
        InvoiceDialog invoiceDialog = new InvoiceDialog();
        Bundle args = new Bundle();
        args.putString("param", "param");
        invoiceDialog.setArguments(args);
        return invoiceDialog;
    }
}
