package com.haodaibao.fanbeiapp.module.personal.help;

import com.haodaibao.fanbeiapp.repository.json.HelpBean;

/**
 * Created by Routee on 2017/9/26.
 * description: ${cusor}
 */

public interface HelpContract {
    interface View {
        void setUpHelpListData(HelpBean bean);

        void setEmptyView();
    }

    interface Presenter {
        void getHelpList(String uid);
    }
}
