package com.haodaibao.fanbeiapp.module.search;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.baseandroid.base.BaseFragment;
import com.baseandroid.config.Global;
import com.baseandroid.utils.RxUtils;
import com.haodaibao.fanbeiapp.module.Consent;
import com.haodaibao.fanbeiapp.repository.LifeRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.AreaList;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.DistrictList;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoResp;
import com.haodaibao.fanbeiapp.repository.json.ShanghufenleiInfo;
import com.haodaibao.fanbeiapp.repository.json.ShanghufenleiList;
import com.haodaibao.fanbeiapp.repository.json.ShanghufenleiResp;
import com.haodaibao.fanbeiapp.repository.json.ShangquanResp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.schedulers.Schedulers;

public class NearbyPresenter implements NearbyContract.Presenter {

    NearbyContract.View mView;

    public NearbyPresenter(NearbyContract.View view) {
        mView = view;
    }

    @Override
    public void getShanghuListInfo(/*boolean isFirst,*/String merchantname, String categoryno, String districtcode, String areacode, String sort, String refundmode, String isProduct, String isVoucher, String cityCode, int pagerNumber, int pagerSize, String headMerchant, String longitude, String latitude) {
        Map<String, String> hasMap = new HashMap<>();
        hasMap.put("page", "" + pagerNumber);
        hasMap.put("pageSize", "" + pagerSize);
        String arearange = "";
        if (TextUtils.isEmpty(sort)) {
            sort = "distance.asc";
        }
        if (TextUtils.equals(districtcode, "0000")) {
            if (TextUtils.isEmpty(sort)) {
                sort = "distance.asc";
            }
            if (TextUtils.equals(areacode, "00001")) {
                arearange = "";
            } else if (TextUtils.equals(areacode, "00002")) {
                arearange = "1000";
            } else if (TextUtils.equals(areacode, "00003")) {
                arearange = "2000";
            } else {
                arearange = "5000";
            }
            hasMap.put("arearange", arearange);
            districtcode = "";
            areacode = "";
        }
//        if(isFirst){
//            sort = "" ;
//        }
        if (!TextUtils.isEmpty(merchantname)) {
            hasMap.put("merchantname", merchantname);// 商户名称
        }
        if (!TextUtils.isEmpty(categoryno)) {
            hasMap.put("categoryno", categoryno);// 商户类别
        }
        if (!TextUtils.isEmpty(districtcode)) {
            hasMap.put("districtcode", districtcode);// 区域
        }
        if (!TextUtils.isEmpty(areacode)) {
            hasMap.put("areacode", areacode);// 商圈
        }
        if (!TextUtils.isEmpty(sort)) {
            hasMap.put("sort", sort);// 排序条件
        }
        if (!TextUtils.isEmpty(refundmode)) {
            hasMap.put("refundmode", refundmode);// 返现商户
        }
        if (!TextUtils.isEmpty(isProduct)) {
            hasMap.put("discountProduct", isProduct);// 优惠套餐
        }
        if (!TextUtils.isEmpty(isVoucher)) {
            hasMap.put("voucher", isVoucher);// 优惠套餐
        }
        hasMap.put("siteType", Global.getSelectCitySiteType());
        hasMap.put("siteCode", Global.getSelectCityCode());
        hasMap.put("citycode", cityCode);
        hasMap.put("longitude", longitude);
        hasMap.put("latitude", latitude);
        hasMap.put("headMerchant", headMerchant);
//        hasMap.put("tagno", Constant.TAGNO);

        LifeRepository.getInstance()
                .getShanghuListInfo(hasMap, true)
                .compose(RxUtils.<Data<MerchantInfoResp>>applySchedulersLifeCycle((BaseFragment) (mView)))
                .subscribe(new RxObserver<Data<MerchantInfoResp>>() {

                    @Override
                    public void onNext(@NonNull Data<MerchantInfoResp> merchantInfoRespData) {
                        if (RxObserver.checkJsonCode(merchantInfoRespData, true)) {
                            mView.onShanghuListInfo(merchantInfoRespData.getResult());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                        mView.onShanghuListInfoError();
                    }
                });
    }

    public void getClassification(String siteType, String siteCode, String cityCode, String keyWord) {
        Map<String, String> hasMap = new HashMap<>();
        hasMap.put("cityCode", cityCode);
        hasMap.put("siteType", siteType);
        hasMap.put("siteCode", cityCode);
        hasMap.put("keyWord", keyWord);
        LifeRepository.getInstance()
                .getClassification(hasMap)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.newThread())
                .compose(RxUtils.<Data<ShanghufenleiResp>>bindToLifecycle((BaseFragment) (mView)))
                .subscribe(new RxObserver<Data<ShanghufenleiResp>>() {

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull Data<ShanghufenleiResp> shanghufenleiRespData) {
                        if (checkJsonCode(shanghufenleiRespData, false)) {
                            parseCatagoryList(shanghufenleiRespData);
                        }
                    }
                });
    }

    public void getShangquan(String siteType, String siteCode, String cityCode) {
        Map<String, String> hasMap = new HashMap<>();
        hasMap.put("siteType", siteType);
        hasMap.put("siteCode", siteCode);

        LifeRepository.getInstance()
                .getShangquan(hasMap)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.newThread())
                .compose(RxUtils.<Data<ShangquanResp>>bindToLifecycle((BaseFragment) (mView)))
                .subscribe(new RxObserver<Data<ShangquanResp>>() {

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull Data<ShangquanResp> shangquanRespData) {
                        if (checkJsonCode(shangquanRespData, false)) {
                            List<DistrictList> districtListOne = shangquanRespData.getResult()
                                    .getCdaList()
                                    .get(0)
                                    .getDistrictList();
                            if (Consent.currentLocation) {
                                List<AreaList> areaLists = new ArrayList<>();
                                areaLists.add(new AreaList("00001", "附近（智能范围）", "0000", ""));
                                areaLists.add(new AreaList("00002", "1km", "0000", ""));
                                areaLists.add(new AreaList("00003", "2km", "0000", ""));
                                areaLists.add(new AreaList("00004", "5km", "0000", ""));
                                DistrictList districtList = new DistrictList("0000", "附近", Global
                                        .getSelectCityCode(), areaLists);
                                districtListOne.add(0, districtList);
                            }

                            Global.setShangquans(districtListOne);
                        }
                    }
                });
    }

    private void parseCatagoryList(@io.reactivex.annotations.NonNull Data<ShanghufenleiResp> shanghufenleiRespData) {
        List<ShanghufenleiInfo> shanghufenleiList = shanghufenleiRespData.getResult()
                .getCategoryList();
        List<ShanghufenleiList> group = new ArrayList<ShanghufenleiList>();
        if (shanghufenleiList.size() > 0) {
            // 取出全部父类
            for (int i = 0; i < shanghufenleiList.size(); i++) {
                String parentno1 = shanghufenleiList.get(i).getParentno();
                String categoryno1 = shanghufenleiList.get(i).getCategoryno();
                String name1 = shanghufenleiList.get(i).getName();
                String hot1 = shanghufenleiList.get(i).getHot();
                String status1 = shanghufenleiList.get(i).getStatus();
                if (parentno1.equals("ROOT")) {
                    List<ShanghufenleiInfo> infoList = new ArrayList<ShanghufenleiInfo>();
                    ShanghufenleiList parentInfo = new ShanghufenleiList(categoryno1, name1, parentno1, hot1, status1, infoList);
                    group.add(parentInfo);
                }
            }
            for (int i = 0; i < group.size(); i++) {
                String parentCategoryno = group.get(i).getCategoryno();
                ShanghufenleiList info = group.get(i);
                List<ShanghufenleiInfo> infoList = info.getList();
                for (int j = 0; j < shanghufenleiList.size(); j++) {
                    // 如果当前父类编号为已存父类编号中的一个，则将此子分类加入该父类list中
                    if (parentCategoryno.equals(shanghufenleiList.get(j).getParentno())) {
                        infoList.add(shanghufenleiList.get(j));
                    }
                }
                info.setList(infoList);
                group.set(i, info);
            }
        }
        Global.setCategorys(group);

    }
}
