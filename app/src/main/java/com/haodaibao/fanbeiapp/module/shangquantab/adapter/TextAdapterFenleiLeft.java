package com.haodaibao.fanbeiapp.module.shangquantab.adapter;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.text.TextPaint;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.utils.LogUtil;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.ShanghufenleiInfo;

import java.util.List;

public class TextAdapterFenleiLeft extends ArrayAdapter<ShanghufenleiInfo> {

    private Context mContext;
    private List<ShanghufenleiInfo> mListData;
    private ShanghufenleiInfo[] mArrayData;
    private int selectedPos = -1;
    private String selectedText = "";
    private int normalDrawbleId;
    private int selectedTextColor = -1;
    private Drawable selectedDrawble;
    private float textSize;
    private OnItemClickListener mOnItemClickListener;
    private int tLeft;

    public TextAdapterFenleiLeft(Context context, List<ShanghufenleiInfo> listData, int sIdText, int sId, int nId) {
        super(context, R.string.no_data, listData);
        mContext = context;
        mListData = listData;
        selectedTextColor = mContext.getResources().getColor(sIdText);
        selectedDrawble = mContext.getResources().getDrawable(sId);
        normalDrawbleId = nId;
    }


    public TextAdapterFenleiLeft(Context context, ShanghufenleiInfo[] arrayData, int sIdText, int sId, int nId) {
        super(context, R.string.no_data, arrayData);
        mContext = context;
        mArrayData = arrayData;
        selectedTextColor = mContext.getResources().getColor(sIdText);
        selectedDrawble = mContext.getResources().getDrawable(sId);
        normalDrawbleId = nId;
    }

    public void refresh(List<ShanghufenleiInfo> listData) {
        this.mListData = listData;
        notifyDataSetChanged();
    }

    /**
     * 设置选中的position,并通知列表刷新
     */
    public void setSelectedPosition(int pos) {
        if (mListData != null && pos < mListData.size()) {
            selectedPos = pos;
            selectedText = mListData.get(pos).getName();
            notifyDataSetChanged();
        } else if (mArrayData != null && pos < mArrayData.length) {
            selectedPos = pos;
            selectedText = mArrayData[pos].getName();
            notifyDataSetChanged();
        }
    }

    /**
     * 设置选中的position,但不通知刷新
     */
    public void setSelectedPositionNoNotify(int pos) {
        selectedPos = pos;
        if (mListData != null && pos < mListData.size()) {
            selectedText = mListData.get(pos).getName();
        } else if (mArrayData != null && pos < mArrayData.length) {
            selectedText = mArrayData[pos].getName();
        }
    }

    /**
     * 获取选中的position
     */
    public int getSelectedPosition() {
        if (mArrayData != null && selectedPos < mArrayData.length) {
            return selectedPos;
        }
        if (mListData != null && selectedPos < mListData.size()) {
            return selectedPos;
        }

        return -1;
    }

    /**
     * 设置列表字体大小
     */
    public void setTextSize(float tSize) {
        textSize = tSize;
    }

    /**
     * 设置列表文字距离
     */
    public void setTextPaddingLeft(int left) {
        tLeft = left;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        FenLeiHolder fenLeiHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.choose_item, parent, false);
            fenLeiHolder = new FenLeiHolder(convertView);
            convertView.setTag(fenLeiHolder);
        } else {
            fenLeiHolder = (FenLeiHolder) convertView.getTag();
        }
        String mString = "";
        if (mListData != null) {
            if (position < mListData.size()) {
                mString = mListData.get(position).getName();
            }
        } else if (mArrayData != null) {
            if (position < mArrayData.length) {
                mString = mArrayData[position].getName();
            }
        }
        if (mString.contains("不限")) {
            fenLeiHolder.view.setText("不限");
        } else {
            fenLeiHolder.view.setText(mString);
        }
        fenLeiHolder.view.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) fenLeiHolder.rightIcon.getLayoutParams();
        params.width = getTextViewWidth(fenLeiHolder.view, mString);
        fenLeiHolder.rightIcon.setLayoutParams(params);
        if (selectedText != null && selectedText.equals(mString)) {
            fenLeiHolder.view.setTextColor(selectedTextColor);// 设置选中的字体颜色
            fenLeiHolder.rightIcon.setVisibility(View.INVISIBLE);
        } else {
            fenLeiHolder.view.setTextColor(mContext.getResources().getColor(R.color.text_gray3));
            fenLeiHolder.rightIcon.setVisibility(View.INVISIBLE);
        }
        convertView.setOnClickListener(new AdapterClick(position));
        return convertView;
    }

    private int getTextViewWidth(TextView textView, String text) {
        Rect rect = new Rect();
        textView.getPaint().getTextBounds(text, 0, text.length(), rect);
        int width = rect.width();
        return width;
    }

    class AdapterClick implements View.OnClickListener {

        int position;

        AdapterClick(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            selectedPos = position;
            setSelectedPosition(selectedPos);
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(view, selectedPos);
            }
        }
    }

    public void setOnItemClickListener(OnItemClickListener l) {
        mOnItemClickListener = l;
    }

    /**
     * 重新定义菜单选项单击接口
     */
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    class FenLeiHolder {
        TextView view;
        ImageView rightIcon;

        FenLeiHolder(View rootView) {
            view = (TextView) rootView.findViewById(R.id.choose_left_tv);
            rightIcon = (ImageView) rootView.findViewById(R.id.left_icon);
        }
    }
}
