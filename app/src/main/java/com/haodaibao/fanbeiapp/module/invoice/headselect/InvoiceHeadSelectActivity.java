package com.haodaibao.fanbeiapp.module.invoice.headselect;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.InvoiceHeadBean;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static com.baseandroid.config.Constant.HEADER_SELECT_RESULTCODE;
import static com.baseandroid.config.Constant.INVOICEHEADITEM;

/**
 * date: 2018/2/6.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class InvoiceHeadSelectActivity extends BaseActivity implements InvoiceHeadContract.View, BaseRecycleViewAdapter.OnItemClickListener {
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @BindView(R.id.toolbar_back)
    RelativeLayout toolbar_back;
    @BindView(R.id.mrv)
    RecyclerView mrv;

    private InvoiceHeadAdapter adapter;
    private InvoiceHeadPresenter presenter;
    private int page = 1;
    private final int pageSize = 10;
    private String riseId;

    public static void startForResult(BaseActivity context, String riseId) {
        Intent intent = new Intent(context, InvoiceHeadSelectActivity.class);
        intent.putExtra("riseId", riseId);
        context.startActivityForResult(intent, 0);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_invoice_head_select;
    }

    @Override
    protected void setupView() {
        toolbar_title.setText(R.string.title_my_invoice_list);
        riseId = getIntent().getStringExtra("riseId");
        initRecylerView();
    }

    private void initRecylerView() {
        mrv.setLayoutManager(new LinearLayoutManager(this));
        adapter = new InvoiceHeadAdapter(this, this);
        mrv.setAdapter(adapter);
        adapter.setOnLoadMoreListener(new BaseRecycleViewAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                presenter.getRise(page + "", pageSize + "");
            }
        }, mrv);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        presenter = new InvoiceHeadPresenter(this);
        presenter.getRise(page + "", pageSize + "");
    }

    @OnClick({R.id.toolbar_back})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                finish();
                break;
        }
    }

    @Override
    public void updateUI(List<InvoiceHeadBean.InvoiceHeadItem> list) {
        if (page == 1) {
            adapter.resetData(list);
        } else {
            adapter.loadMoreComplete();
            adapter.addData(list);
            if (list.size() < pageSize) {
                adapter.loadMoreEnd(false);
            }
        }
        page++;
    }

    @Override
    public void updateFailed() {
        if (page == 1) {

        } else {
            adapter.loadMoreFail();
        }
    }

    @Override
    public <T> void onItemClick(View view, T t) {
        InvoiceHeadBean.InvoiceHeadItem invoiceHeadItem = (InvoiceHeadBean.InvoiceHeadItem) t;
        Intent intent = new Intent();
        intent.putExtra(INVOICEHEADITEM, invoiceHeadItem);
        setResult(HEADER_SELECT_RESULTCODE, intent);
        finish();
    }

    public String getRiseId() {
        return riseId;
    }
}
