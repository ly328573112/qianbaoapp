package com.haodaibao.fanbeiapp.module.combo;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.RxUtils;
import com.haodaibao.fanbeiapp.repository.LifeRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.PackageAllBean;

import java.util.HashMap;

/**
 * date: 2017/11/7.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class ComboDetailPresenter implements ComboDetailContract.Presenter {
    private ComboDetailContract.View mView;

    public ComboDetailPresenter(ComboDetailContract.View mView) {
        this.mView = mView;
    }

    @Override
    public void getComboDetail(String merchantno, String packageno) {
        HashMap<String, String> map = new HashMap<>();
        map.put("merchantno", merchantno);
        map.put("packageno", packageno);
        LifeRepository.getInstance().getPackageDetail(map)
                .compose(RxUtils.<Data<PackageAllBean>>applySchedulersLifeCycle((BaseActivity) mView))
                .subscribe(new RxObserver<Data<PackageAllBean>>() {
                    @Override
                    public void onNext(Data<PackageAllBean> packageAllBeanData) {
                        if (checkJsonCode(packageAllBeanData, true)) {
                            mView.updateUI(packageAllBeanData.getResult());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        mView.showRetry();
                    }
                });

    }

    @Override
    public void checkMerchantPackageRule(String merchantno, String packageno) {
        HashMap<String, String> map = new HashMap<>();
        map.put("merchantno", merchantno);
        map.put("packageno", packageno);
        LifeRepository.getInstance().checkMerchantPackageRule(map)
                .compose(RxUtils.<Data<PackageAllBean>>applySchedulersLifeCycle((BaseActivity) mView))
                .subscribe(new RxObserver<Data<PackageAllBean>>() {
                    @Override
                    public void onNext(Data<PackageAllBean> packageAllBeanData) {
                        if (checkJsonCode(packageAllBeanData, true)) {
                            mView.checkedUI(packageAllBeanData.getResult().packageAuth);
                        }
                    }
                });
    }
}
