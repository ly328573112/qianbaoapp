package com.haodaibao.fanbeiapp.module.home;


import android.content.Context;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.baseandroid.widget.customimageview.GlideRoundTransform;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.haodaibao.fanbeiapp.R;
import com.jayfeng.lesscode.core.DisplayLess;
import com.makeramen.roundedimageview.RoundedImageView;
import com.youth.banner.loader.ImageLoaderInterface;

public class GlideImageLoader implements ImageLoaderInterface<FrameLayout> {
    private int riv_corner_radius;

    public GlideImageLoader() {
        super();
    }

    public GlideImageLoader(int riv_corner_radius) {
        this();
        this.riv_corner_radius = riv_corner_radius;
    }

    public void setImagePath(Context context, Object path, ImageView imageView) {
        Glide.with(context)
                .load(path)
                .bitmapTransform(new GlideRoundTransform(context))
                .placeholder(R.drawable.home_default_backgroud)
                .error(R.drawable.home_default_backgroud)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(new GlideDrawableImageViewTarget(imageView, 10000));
    }

    @Override
    public void displayImage(Context context, Object path, FrameLayout frameLayout) {
        Glide.with(context)
                .load(path)
                .asBitmap()
                .placeholder(R.drawable.home_default_backgroud)
                .error(R.drawable.home_default_backgroud)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into((ImageView) frameLayout.findViewById(R.id.iv_banner));
    }

    @Override
    public FrameLayout createImageView(Context context) {
        FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(context)
                .inflate(R.layout.banner_image_layout, null);
        if (riv_corner_radius != 0) {
            RoundedImageView roundedImageView = frameLayout.findViewById(R.id.iv_banner);
            roundedImageView.setCornerRadius(DisplayLess.$dp2px(riv_corner_radius));
        }
        return frameLayout;
    }
}