package com.haodaibao.fanbeiapp.module.invoice;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.StatusBarHelper;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.event.InvoiceRiseChangeEvent;
import com.haodaibao.fanbeiapp.module.invoice.headselect.InvoiceHeadAdapter;
import com.haodaibao.fanbeiapp.module.invoice.headselect.InvoiceHeadContract;
import com.haodaibao.fanbeiapp.module.invoice.headselect.InvoiceHeadPresenter;
import com.haodaibao.fanbeiapp.module.invoice.updaterise.UpdateInvoiceRiseActivity;
import com.haodaibao.fanbeiapp.repository.json.InvoiceHeadBean;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;

/**
 * 发票抬头
 */
public class InvoiceRiseActivity extends BaseActivity implements View.OnClickListener, BaseRecycleViewAdapter.OnItemClickListener, InvoiceHeadContract.View {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.invoice_ptr_fl)
    PtrClassicFrameLayout invoicePtrFl;
    @BindView(R.id.invoice_rv)
    RecyclerView invoiceRv;
    @BindView(R.id.c)
    RelativeLayout c;


    //    @BindView(R.id.invoice_add_rl)
//    RelativeLayout invoiceAddRl;
    InvoiceHeadAdapter adapter;
    private InvoiceHeadPresenter presenter;
    private int page = 1;
    private final int pageSize = 10;

    public static void start(BaseActivity context) {
        Intent intent = new Intent(context, InvoiceRiseActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_invoice_rise;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(this);
        invoicePtrFl.setEnabled(false);
        EventBus.getDefault().register(this);
        toolbarTitle.setText("我的发票抬头");
        toolbarBack.setOnClickListener(this);
        c.setOnClickListener(this);
        initRecyclerView();
        presenter = new InvoiceHeadPresenter(this);
//        invoiceAddRl.setOnClickListener(this);
    }

    private void initRecyclerView() {
        adapter = new InvoiceHeadAdapter(this, this);
        invoiceRv.setLayoutManager(new LinearLayoutManager(this));
        invoiceRv.setAdapter(adapter);
        adapter.setOnLoadMoreListener(new BaseRecycleViewAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                presenter.getRise(page + "", pageSize + "");
            }
        }, invoiceRv);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        presenter.getRise(page + "", pageSize + "");
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.c:
                UpdateInvoiceRiseActivity.start(this, null, getString(R.string.add_invoice_rise_title));
                break;
//            case R.id.invoice_add_rl:
//
//                break;
            default:
                break;
        }
    }

    @Override
    public <T> void onItemClick(View view, T t) {
        InvoiceHeadBean.InvoiceHeadItem invoiceHeadItem = (InvoiceHeadBean.InvoiceHeadItem) t;
        UpdateInvoiceRiseActivity.start(this, invoiceHeadItem, getString(R.string.change_invoice_rise_title));
    }

    @Override
    public void updateUI(List<InvoiceHeadBean.InvoiceHeadItem> list) {
        if (page == 1) {
            adapter.resetData(list);
            if (list.size() == 0) {
                adapter.setEmptyView(R.layout.invoicerise_list_empty);
            }
        } else {
            adapter.loadMoreComplete();
            adapter.addData(list);
            if (list.size() < pageSize) {
                adapter.loadMoreEnd(false);
            }
        }
        page++;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(InvoiceRiseChangeEvent invoiceRiseChangeEvent) {
        if (invoiceRiseChangeEvent.invoiceRiseDetailBean == null) {
            page = 1;
            presenter.getRise(page + "", pageSize + "");
        } else {
            InvoiceHeadBean.InvoiceHeadItem invoiceHeadItem = adapter.getData().get(adapter.position);
            invoiceHeadItem.setData(invoiceRiseChangeEvent.invoiceRiseDetailBean.rise);
            adapter.notifyItemChanged(adapter.position);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void updateFailed() {
        if (page == 1) {

        } else {
            adapter.loadMoreFail();
        }
    }
}
