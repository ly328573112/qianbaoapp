package com.haodaibao.fanbeiapp.module.start;

import com.haodaibao.fanbeiapp.repository.json.CheckUpdate;

public interface StartContract {

    interface Presenter {
        void requestUserInfo();
    }

    interface View {

        void OnVersionUpdate(CheckUpdate checkUpdate);

        void OnVersionUpdateError();

        void OnUserInfoSuccess();

        void OnUserInfoError();
    }
}
