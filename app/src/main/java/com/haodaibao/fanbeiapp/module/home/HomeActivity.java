package com.haodaibao.fanbeiapp.module.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.base.BaseApplication;
import com.baseandroid.config.Constant;
import com.baseandroid.config.Global;
import com.baseandroid.jpush.JPushBizutils;
import com.baseandroid.utils.FileUtils;
import com.baseandroid.utils.StatusBarHelper;
import com.baseandroid.utils.ToastUtils;
import com.baseandroid.utils.runpermission.PermissionManger;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.event.HomeActivityEvent;
import com.haodaibao.fanbeiapp.event.LocationResultEvent;
import com.haodaibao.fanbeiapp.event.LocationSwitchEvent;
import com.haodaibao.fanbeiapp.event.LoginInvalideEvent;
import com.haodaibao.fanbeiapp.event.PersonalFragmentEvent;
import com.haodaibao.fanbeiapp.event.ShowFindEvent;
import com.haodaibao.fanbeiapp.module.Consent;
import com.haodaibao.fanbeiapp.module.login.LoginActivity;
import com.haodaibao.fanbeiapp.module.pay.PayActivity;
import com.haodaibao.fanbeiapp.module.personal.PersonalFragment;
import com.haodaibao.fanbeiapp.module.search.FindStoreFragment;
import com.haodaibao.fanbeiapp.pageadpter.FragmentPageAdpter;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.MyLocation;
import com.jaeger.library.StatusBarUtil;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;

import butterknife.BindView;
import hugo.weaving.DebugLog;

import static com.haodaibao.fanbeiapp.module.pay.PayActivity.OTHER;

@DebugLog
public class HomeActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.viewpager_id)
    ViewPager viewpagerId;
    @BindView(R.id.rg_bottom_id)
    RadioGroup rgBottomId;
    @BindView(R.id.rb_footer1)
    RadioButton rbFooter1;
    @BindView(R.id.rb_footer2)
    RadioButton rbFooter2;
    @BindView(R.id.rb_footer3)
    RadioButton rbFooter3;

    //    private MessageDialog mLocateMessageDialog;
    private int mCurrentpage = 0;


    private boolean homeLocation = true;

    public void isHomeLocation(boolean homeLocation) {
        this.homeLocation = homeLocation;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    public void showFind() {
        if (mCurrentpage != 1) {
            mCurrentpage = 1;
            viewpagerId.setCurrentItem(mCurrentpage);
            rgBottomId.check(R.id.rb_footer2);
        }
    }

    @Override
    protected void setupView() {
        StatusBarUtil.setTranslucentForImageViewInFragment(this, 0, null);
        HomeNewFragment homeNewFragment = initHomeFragment();
        setupViewPager(homeNewFragment);
        rbFooter1.setOnClickListener(this);
        rbFooter2.setOnClickListener(this);
        rbFooter3.setOnClickListener(this);

        if (Consent.versionUpdateType) {
            Consent.versionUpdateType = false;
            new VersionUpdateView(this).onCheckUpdate();
        }

        StatusBarHelper.translateStatusBar(this);
//        int supportWhiteStatusBar = StatusBarHelper.StatusBarLightMode(HomeActivity.this);
//        if (supportWhiteStatusBar > 0) {
//            StatusBarUtil.setColor(HomeActivity.this, ContextCompat.getColor(HomeActivity.this, R.color.white), 0);
//        } else {
//            StatusBarUtil.setColor(HomeActivity.this, ContextCompat.getColor(HomeActivity.this, android.R.color.black), 0);
//        }
    }

    private HomeNewFragment initHomeFragment() {
        HomeNewFragment homeNewFragment = new HomeNewFragment();
        homeNewFragment.onHomeClickListener(new HomeContract.HomeClickListener() {
            @Override
            public void onHomeClick() {
                rgBottomId.check(R.id.rb_footer2);
                if (mCurrentpage != 1) {
                    mCurrentpage = 1;
                    viewpagerId.setCurrentItem(mCurrentpage);
                }
            }
        });
        return homeNewFragment;
    }

    private void setupViewPager(HomeNewFragment homeNewFragment) {
        FragmentPageAdpter adapter = new FragmentPageAdpter(getSupportFragmentManager(), this);
        adapter.addFrag(homeNewFragment, "");
        adapter.addFrag(new FindStoreFragment(), "");
        adapter.addFrag(new PersonalFragment(), "");
        viewpagerId.setAdapter(adapter);
        viewpagerId.setOffscreenPageLimit(10);
        viewpagerId.setCurrentItem(mCurrentpage);
        viewpagerId.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (mCurrentpage != position) {
                    mCurrentpage = position;
                    if (mCurrentpage == 0) {
                        rgBottomId.check(R.id.rb_footer1);
                    } else if (mCurrentpage == 1) {
                        rgBottomId.check(R.id.rb_footer2);
                    } else if (mCurrentpage == 2) {
                        rgBottomId.check(R.id.rb_footer3);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        JPushBizutils.initJPush();
        PermissionManger.getInstance().startLocation(HomeActivity.this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rb_footer1:
                if (mCurrentpage != 0) {
                    mCurrentpage = 0;
                    viewpagerId.setCurrentItem(mCurrentpage);
                }
                break;

            case R.id.rb_footer2:
                if (mCurrentpage != 1) {
                    mCurrentpage = 1;
                    viewpagerId.setCurrentItem(mCurrentpage);
                }
                break;

            case R.id.rb_footer3:
                if (mCurrentpage != 2) {
                    mCurrentpage = 2;
                    viewpagerId.setCurrentItem(mCurrentpage);
                }
                break;

            default:

                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PersonalFragmentEvent event) {
        JPushBizutils.initJPush();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(String event) {
        switch (event){
            case "shopSelect" :
                rgBottomId.check(R.id.rb_footer2);
                if (mCurrentpage != 1) {
                    mCurrentpage = 1;
                    viewpagerId.setCurrentItem(mCurrentpage);
                }
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ShowFindEvent event) {
        showFind();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(LoginInvalideEvent event) {
        ToastUtils.showShortToast(R.string.login_expire_string);
        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        Global.loginOutclear();
        PersonalFragmentEvent e = new PersonalFragmentEvent();
        e.setLogOut(true);
        EventBus.getDefault().post(e);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(LocationResultEvent event) {
        final LocationSwitchEvent locationSwitchEvent = new LocationSwitchEvent();
        locationSwitchEvent.locationType = event.getLocationType();
        locationSwitchEvent.isHomeLocation = homeLocation;
        if (!event.isMbResult()) {
            if (TextUtils.isEmpty(Global.getSelectCity())) {
                MyLocation myLocation = Global.getMyLocation();
                if (myLocation.getLongitude() > 0) {
                } else {
                    myLocation.setLatitude(-180);
                    myLocation.setLongitude(-180);
                }
                myLocation.setCityCode(Constant.SELECTCITYCODE);
                myLocation.setCityName(Constant.SELECTCITY);
                Global.setMyLocation(myLocation);
                locationSwitchEvent.locationDialog = true;
                /*-----定位赋值默认值----*/
                Global.setLastSelectCity(Global.getSelectCity());
                Global.setSelectCity(Constant.SELECTCITY);
                Global.setSelectCityCode(Constant.SELECTCITYCODE);
                Global.setSelectCitySiteType(Constant.SELECTCITYSITETYPE);
            }
            EventBus.getDefault().post(locationSwitchEvent);
            locationSwitchEvent.cLocation = false;
        } else {
            if (!Global.getMyLocation().getAddress().contains(Global.getSelectCity())) {
                if (TextUtils.isEmpty(Global.getSelectCity())) {
                    locationSwitchEvent.locationDialog = true;
                    locationSwitchEvent.cLocation = true;
                    /*-----定位赋值默认值----*/
                    Global.setLastSelectCity(Global.getSelectCity());
                    Global.setSelectCity(Constant.SELECTCITY);
                    Global.setSelectCityCode(Constant.SELECTCITYCODE);
                    Global.setSelectCitySiteType(Constant.SELECTCITYSITETYPE);
                    EventBus.getDefault().post(locationSwitchEvent);
                } else {
                    Global.setSelectCityNameAndCodeByGaodeNameAdcode();
                    Global.setLastSelectCity(Global.getSelectCity());
                    Global.setSelectCity(Global.getMyLocation().getCityName());
                    locationSwitchEvent.locationDialog = true;
                    locationSwitchEvent.cLocation = true;
                    EventBus.getDefault().post(locationSwitchEvent);
                }
            } else {
                locationSwitchEvent.locationDialog = true;
                locationSwitchEvent.cLocation = true;
                Global.setSelectCityNameAndCodeByGaodeNameAdcode();
                EventBus.getDefault().post(locationSwitchEvent);
            }
        }
    }

    private static String FILEPATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            .getAbsolutePath();
    private static String FILENAME = "tempPhotoSuggest";
    private static String SUFFIX = ".png";

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(HomeActivityEvent event) {
        Glide.with(this)
                .load(Global.getLoginInfo().coupon.activityImageUrl)
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        if (resource == null) {
                            return;
                        }
                        File file = new File(FILEPATH + "/" + FILENAME + System.currentTimeMillis() + SUFFIX);
                        FileUtils.saveBitmapToFile(resource, file);
                        LifeCouponActivity.open(HomeActivity.this, file.toString());
                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        super.onLoadFailed(e, errorDrawable);
                    }
                });
    }

    private String mApkFilePath;
    public static int UNKNOWN_CODE = 0x0800;

    public void downloadCompleted(String apkpath) {
        mApkFilePath = apkpath;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            installApp(apkpath);
        } else {
            installApk(new File(apkpath));
            BaseApplication.exitApp();
        }
    }

    public void installApk(File apkFile) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            uri = FileProvider.getUriForFile(Global.getContext(), Global.getContext()
                    .getPackageName() + ".fileprovider", apkFile);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            uri = Uri.fromFile(apkFile);
        }
        intent.setDataAndType(uri, "application/vnd.android.package-archive");
        startActivity(intent);
    }

    public void installApp(final String apkpath) {
        @SuppressLint({"NewApi", "LocalSuppress"})
        boolean canbeInstall = getPackageManager()
                .canRequestPackageInstalls();
        if (canbeInstall) {
            installApk(new File(apkpath));
            BaseApplication.exitApp();
        } else {
            //请求安装未知应用来源的权限
            new RxPermissions(HomeActivity.this).request(Manifest.permission.REQUEST_INSTALL_PACKAGES)
                    .subscribe(new RxObserver<Boolean>() {
                        @Override
                        public void onNext(Boolean aBoolean) {
                            if (aBoolean) {
                                installApk(new File(apkpath));
                                BaseApplication.exitApp();
                            } else {
                                Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES, Uri
                                        .parse("package:" + getPackageName()));
                                startActivityForResult(intent, UNKNOWN_CODE);
                            }
                        }
                    });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == UNKNOWN_CODE) {
            installApp(mApkFilePath);
        } else if (resultCode == RESULT_CANCELED && requestCode == UNKNOWN_CODE) {
            BaseApplication.exitApp();
        }
        // 获取解析结果
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                //                Toast.makeText(getActivity(), "取消扫描", Toast.LENGTH_LONG).show();
            } else {
                dealResult(result.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void dealResult(String sao_result) {
        Uri uri = Uri.parse(sao_result);
        String path = uri.getQuery();
        if (!TextUtils.isEmpty(path)) {
            if (path.contains("s=") && path.contains("q=")) {// 收款码
                sao_result = uri.getQueryParameter("q");
            } else if (path.contains("s=") && path.contains("m=") && path.contains("c=")) {// 商户码
                sao_result = uri.getQueryParameter("m");
            }
            if (sao_result != null && sao_result.length() > 0) {// 商户码进在线支付
                PayActivity.openForResult(this, null, sao_result, OTHER, false);
            } else {
                ToastUtils.showShortToast("这个码我不认识哦~");
            }
        } else {
            ToastUtils.showShortToast("这个码我不认识哦~");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
