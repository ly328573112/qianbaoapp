package com.haodaibao.fanbeiapp.module.maphome.cluster.demo;


import com.amap.api.maps.model.LatLng;
import com.haodaibao.fanbeiapp.module.maphome.cluster.ClusterItem;

/**
 * Created by yiyi.qi on 16/10/10.
 */

public class RegionItem implements ClusterItem {
    private LatLng mLatLng;
    private MarkerInfo markerInfo;

    public RegionItem(LatLng latLng, MarkerInfo markerInfo) {
        mLatLng = latLng;
        this.markerInfo = markerInfo;
    }

    @Override
    public LatLng getPosition() {
        // TODO Auto-generated method stub
        return mLatLng;
    }

    @Override
    public MarkerInfo getMarkerInfo() {
        return markerInfo;
    }

}
