package com.haodaibao.fanbeiapp.module.start;

import com.baseandroid.config.Constant;
import com.baseandroid.config.Global;
import com.baseandroid.utils.RxUtils;
import com.haodaibao.fanbeiapp.repository.ConfigRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.UserInfoRepository;
import com.haodaibao.fanbeiapp.repository.json.CheckUpdate;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.UserDate;
import com.trello.rxlifecycle2.LifecycleProvider;

import java.util.HashMap;

import io.reactivex.annotations.NonNull;

public class StartPresenter implements StartContract.Presenter {

    private StartContract.View mView;
    private LifecycleProvider lifecycleProvider;

    public StartPresenter(StartContract.View view, LifecycleProvider lifecycleProvider) {
        this.mView = view;
        this.lifecycleProvider = lifecycleProvider;
    }

    public void checkUpdate() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("classcode", Constant.ANDROIDVERSION);

        ConfigRepository.getInstance()
                .checkUpdate(hashMap)
                .compose(RxUtils.<Data<CheckUpdate>>applySchedulersLifeCycle(lifecycleProvider))
                .subscribe(new RxObserver<Data<CheckUpdate>>() {

                    @Override
                    public void onNext(@NonNull Data<CheckUpdate> checkUpdateData) {
                        if (checkJsonCode(checkUpdateData, false)) {
                            mView.OnVersionUpdate(checkUpdateData.getResult());
                        } else {
                            mView.OnVersionUpdateError();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        mView.OnVersionUpdateError();
                    }

                });
    }

    @Override
    public void requestUserInfo() {
        UserInfoRepository.getInstance()
                .getUserinfo()
                .compose(RxUtils.<Data<UserDate>>applySchedulersLifeCycle(lifecycleProvider))
                .subscribe(new RxObserver<Data<UserDate>>() {

                    @Override
                    public void onNext(@NonNull Data<UserDate> userDateData) {
                        if (checkJsonCode(userDateData, false) && userDateData.getResult() != null) {
                            Global.setUserInfo(userDateData.getResult().getUser());
                            mView.OnUserInfoSuccess();
                        } else {
                            mView.OnUserInfoError();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        mView.OnUserInfoError();
                    }
                });
    }

}