package com.haodaibao.fanbeiapp.module.merchant;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.LayoutRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.widget.customscrollview.CustomScrollView;
import com.baseandroid.widget.customscrollview.ScrollListener;
import com.baseandroid.widget.customtext.richtext.RichTextView;
import com.haodaibao.fanbeiapp.R;

import butterknife.BindView;
import hugo.weaving.DebugLog;

@DebugLog
public abstract class CoorNestedScrollActivity extends BaseActivity {

    @BindView(R.id.custom_scrollview)
    protected CustomScrollView customScrollview;
    @BindView(R.id.mytoorbar)
    RelativeLayout mytoorbar;
    @BindView(R.id.icon_navigation)
    ImageView customIconNavigation;
    @BindView(R.id.rl_navigation)
    RelativeLayout rlNavigation;
    @BindView(R.id.tittle_linearlayout)
    RelativeLayout tittleLinearlayout;
    @BindView(R.id.title_tv)
    TextView customTitle;
    @BindView(R.id.right_rl)
    RelativeLayout rightRl;
    @BindView(R.id.right_icon)
    ImageView rightIcon;
    @BindView(R.id.right_more_rl)
    RelativeLayout rightMoreRl;
    @BindView(R.id.right_more_icon)
    ImageView rightMoreIcon;
    @BindView(R.id.rl_cb_focus)
    RelativeLayout rlCbFocus;
    @BindView(R.id.cb_focus)
    CheckBox cbFocus;
    @BindView(R.id.goto_buy)
    RichTextView gotoBuy;
    @BindView(R.id.line_title)
    View lineTitle;
    @BindView(R.id.root_relat)
    protected RelativeLayout rootRelat;
    private int statubarcolor;
    private int toobarcolor;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_coor_nested_scroll;
    }

    @Override
    protected void setupView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            postponeEnterTransition();
        }
        rootRelat.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        rootRelat.getViewTreeObserver().removeOnPreDrawListener(this);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            startPostponedEnterTransition();
                        }
                        return true;
                    }
                }
        );
        cbFocus.setClickable(false);
        cbFocus.setFocusable(false);
        rlNavigation.setOnClickListener(new CustomClick());
        rightRl.setOnClickListener(new CustomClick());
        rlCbFocus.setOnClickListener(new CustomClick());
        rightMoreRl.setOnClickListener(new CustomClick());
        initTitle();
        initappbar();
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        intitTop(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        destroy();
    }

    private void initappbar() {
        statubarcolor = ContextCompat.getColor(mytoorbar.getContext(), R.color.black1);
        toobarcolor = ContextCompat.getColor(mytoorbar.getContext(), R.color.c_ffffff);
    }

    protected abstract void initTitle();

    protected abstract void intitTop(Bundle savedInstanceState);

    protected abstract void buyChanged(int changed);

    protected abstract void onBaseScrolled(RecyclerView recyclerView, int dx, int dy);

    protected abstract void destroy();

    /*设置标题文字*/
    public void setmTittleText(String title) {
        customTitle.setText(title);
    }

    /*设置标题字体颜色*/
    public void setmTitleColor(@ColorInt int color) {
        customTitle.setTextColor(color);
    }

    class CustomClick implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.rl_navigation:
                    if (CommonUtils.isFastDoubleClick()) {
                        return;
                    }
                    setNavigationClick();
                    break;
                case R.id.right_rl:
                    if (CommonUtils.isFastDoubleClick()) {
                        return;
                    }
                    setmRightIconClick();
                    break;
                case R.id.rl_cb_focus:
                    if (CommonUtils.isFastDoubleClick()) {
                        return;
                    }
                    follow();
                    break;
                case R.id.right_more_rl:
                    if (CommonUtils.isFastDoubleClick()) {
                        return;
                    }
                    errorReport();
                    break;
                default:
                    break;
            }
        }
    }

    /*标题栏左图标点击方法*/
    public void setNavigationClick() {
        finish();
    }

    /*标题栏右图标点击方法*/
    public void setmRightIconClick() {

    }

    protected abstract void follow();

    protected abstract void errorReport();

    protected void addNestedScrollView(@LayoutRes int layout) {
        customScrollview.addView(LayoutInflater.from(this).inflate(layout, customScrollview, false));
        customScrollview.setScrollListener(new ScrollListener() {
            @Override
            public void onScrollChanged(int l, int t, int oldl, int oldt) {
                float alpha = customScrollview.doGinant(t);
                titleGginant(alpha);
                int changed = customScrollview.isChange(t);
                buyChanged(changed);
            }
        });
    }

    protected float mAlpha = 0;

    protected void titleGginant(float alpha) {
        this.mAlpha = alpha;
        setmTitleColor(Color.argb((int) alpha, Color.red(statubarcolor), Color.green(statubarcolor), Color.blue(statubarcolor)));
//        mytoorbar.setBackgroundColor(Color.argb((int) alpha, Color.red(toobarcolor), Color.green(toobarcolor), Color.blue(toobarcolor)));
        if (rightIcon == null) {
            rightIcon = findViewById(R.id.right_icon);
        }
//        if (alpha == 0) {
//            mytoorbar.setBackgroundResource(R.drawable.bg_merchant_title);
//        }
        if (alpha > 125) {
            customIconNavigation.setImageResource(R.drawable.icon_back_black);
            rightIcon.setImageResource(R.drawable.icon_share_black);
            rightMoreIcon.setImageResource(R.drawable.error_report_black_icon);
//            lineTitle.setVisibility(View.VISIBLE);
            if (cbFocus.isChecked()) {
                cbFocus.setBackgroundResource(R.drawable.icon_collection_red);
            } else {
                cbFocus.setBackgroundResource(R.drawable.icon_collection_black);
            }
        } else {
//            lineTitle.setVisibility(View.GONE);
//            customIconNavigation.setImageResource(R.drawable.icon_back_white);
            customIconNavigation.setImageResource(R.drawable.icon_back_black);
//            rightIcon.setImageResource(R.drawable.icon_share_white);
//            rightMoreIcon.setImageResource(R.drawable.error_report_icon);
            rightIcon.setImageResource(R.drawable.icon_share_black);
            rightMoreIcon.setImageResource(R.drawable.error_report_black_icon);
            if (cbFocus.isChecked()) {
                cbFocus.setBackgroundResource(R.drawable.icon_collection_red);
            } else {
//                cbFocus.setBackgroundResource(R.drawable.icon_collection_white);
                cbFocus.setBackgroundResource(R.drawable.icon_collection_black);
            }
        }
    }

}
