package com.haodaibao.fanbeiapp.module.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseFragment;
import com.baseandroid.base.BasePopupWindow;
import com.baseandroid.config.Constant;
import com.baseandroid.config.Global;
import com.baseandroid.utils.LogUtil;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.baseandroid.widget.recycleadpter.CustomPtrHeader;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.database.GreenDaoDbHelp;
import com.haodaibao.fanbeiapp.module.Consent;
import com.haodaibao.fanbeiapp.module.bizutils.CategoryAsc;
import com.haodaibao.fanbeiapp.module.bizutils.CategoryFenlei;
import com.haodaibao.fanbeiapp.module.home.HomeRefreshAdapter;
import com.haodaibao.fanbeiapp.module.merchant.MerchantDetailActivity;
import com.haodaibao.fanbeiapp.module.shangquantab.ViewShanghuFenlei;
import com.haodaibao.fanbeiapp.module.shangquantab.ViewShanghuPaixu;
import com.haodaibao.fanbeiapp.module.shangquantab.ViewShangquan;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoResp;
import com.haodaibao.fanbeiapp.repository.json.SearchCacheEntity;
import com.haodaibao.fanbeiapp.repository.json.ShanghufenleiInfo;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;

import static com.haodaibao.fanbeiapp.module.bizutils.CategoryAsc.AI;



public class NearbyFragment extends BaseFragment implements NearbyContract.View, BaseRecycleViewAdapter.OnItemClickListener, BaseRecycleViewAdapter.OnLoadMoreListener, View.OnClickListener {

    public final static int SEARCH_POSITION_TYPE = 00101; //位置搜素
    public final static int SEARCH_KEYWORD_TYPE = 00102;  //关键字搜素

    public final static int PAGE_SIZE = 20;
    public final static int PAGE_PER_SIZE = 5;


    @BindView(R.id.ll_seach_result_id)
    RelativeLayout llSeachResultId;
    @BindView(R.id.cb_catagory_id)
    CheckBox cbCatagoryId;
    @BindView(R.id.cb_scope_id)
    CheckBox cbScopeId;
    @BindView(R.id.cb_sortways_id)
    CheckBox cbSortwaysId;
    @BindView(R.id.rcv_list_id)
    RecyclerView rcvListId;
    @BindView(R.id.ptr_fresh_id)
    PtrClassicFrameLayout ptrFreshId;

    @BindView(R.id.right_back_tv)
    RelativeLayout rightBackRl;
    @BindView(R.id.not_search_rl)
    RelativeLayout notSearchRl;
    @BindView(R.id.history_rl)
    RelativeLayout history_rl;
    @BindView(R.id.delete_history)
    TextView delete_history;
    HomeRefreshAdapter mHomeRefreshAdapter;

    List<SearchCacheEntity> mSearchCacheEntities;

    NearbyPresenter mNearbyPresenter;
    int mCurentPage = 1;
    String headMerchant = "";
//    private boolean isFirst = true ;
    int mSearchType = SEARCH_POSITION_TYPE;

    BasePopupWindow mCatagoryPopupWindow;

    // request paramare
    String keyword = ""; // 商户名称
    String categoryno = "";// 分类编号
    String districtcode = "";// 区域
    String areacode = "";// 商圈
    String sort = "";// 排序条件
    String refundmode = "";// 1返现商户
    String isProduct = "";// 10优惠套餐
    String isVoucher = "";// 1 有优惠券
    private ViewShanghuFenlei viewShanghuFenlei;
    private ViewShangquan viewShangquan;
    private String addressName;
    private String longitude;
    private String latitude;
    private String cityCode;
    private ViewShanghuPaixu viewShanghuPaixu;
    private String sousuo;

    private boolean clickNearby = false;


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_nearby_main;
    }

    @Override
    protected void setupView() {
        mNearbyPresenter = new NearbyPresenter(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            keyword = bundle.getString("result");
            mSearchType = bundle.getInt("SEARCH_TYPE", SEARCH_POSITION_TYPE);
            longitude = bundle.getString("longitude");
            latitude = bundle.getString("latitude");
            cityCode = bundle.getString("cityCode");
            addressName = bundle.getString("addressName");
            sousuo = bundle.getString("sousuo");
            delete_history.setText(TextUtils.isEmpty(keyword) ? addressName : keyword);

        }

        if (Consent.currentLocation) {
            cbScopeId.setText("附近区域");
        } else {
            cbScopeId.setText("全城区域");
        }

        CustomPtrHeader customPtrHeader = new CustomPtrHeader(getActivity());
        ptrFreshId.setHeaderView(customPtrHeader);
        ptrFreshId.addPtrUIHandler(customPtrHeader);
        ptrFreshId.disableWhenHorizontalMove(true);
        ptrFreshId.setPtrHandler(new PtrDefaultHandler() {
            @Override
            public void onRefreshBegin(final PtrFrameLayout frame) {
                mCurentPage = 1;
                headMerchant = "";
                // when pull not loadmore
                mHomeRefreshAdapter.setEnableLoadMore(false);
//                isFirst = false ;
                if (!TextUtils.isEmpty(longitude)) {
                    sort = AI;
                    requestShanghuListInfo(cityCode, longitude, latitude);
                } else {
                    requestShanghuListInfo();
                }

            }
        });

        rcvListId.setLayoutManager(new LinearLayoutManager(getActivity()));
        mHomeRefreshAdapter = new HomeRefreshAdapter(getActivity(), this, cityCode);
        mHomeRefreshAdapter.setOnLoadMoreListener(this, rcvListId);
        rcvListId.setAdapter(mHomeRefreshAdapter);

        cbCatagoryId.setOnCheckedChangeListener(listener);
        cbScopeId.setOnCheckedChangeListener(listener);
        cbSortwaysId.setOnCheckedChangeListener(listener);

        rightBackRl.setOnClickListener(this);
        history_rl.setOnClickListener(this);
        delete_history.setOnClickListener(this);
        mSearchCacheEntities = new ArrayList<>();
        mSearchCacheEntities.addAll(GreenDaoDbHelp.loadallSearchInfo());

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.activity_search_footerview, null);
        view.findViewById(R.id.search_clean_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchCacheEntities.clear();
                GreenDaoDbHelp.deleteallSearchInfo();
            }
        });
    }

    CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (buttonView.getId()) {
                case R.id.cb_catagory_id:
                    Log.e("hpo", "onCheckedChanged: " + isChecked);
                    if (isChecked) {
                        if (Global.getCategorys() == null) {
                            return;
                        }
                        if (viewShanghuFenlei == null) {
                            viewShanghuFenlei = new ViewShanghuFenlei(getActivity());
                        } else if (clickNearby) {
                            viewShanghuFenlei.initData(mContext, Global.getCategorys());
                        }
                        viewShanghuFenlei.init(getActivity(), false);
                        viewShanghuFenlei.settBlockPosition(0);
                        viewShanghuFenlei.setOnSelectListener(shanghuFenleiSelectListener);
                        mCatagoryPopupWindow = BasePopupWindow.matchParentPopupWindowOnce(getActivity(), cbCatagoryId, viewShanghuFenlei, false, new BasePopupWindow.OnOutSideTouchListner() {
                            @Override
                            public void onOutSideTouch() {
                                cbCatagoryId.setChecked(false);
                            }
                        });
                        if (cbCatagoryId.isChecked()) {
                            if (!mCatagoryPopupWindow.isShowing()) {
                                mCatagoryPopupWindow.showMatchParentPopupDialogWithAlpha(mCatagoryPopupWindow);
                            }
                        }
                        cbScopeId.setChecked(false);
                        cbSortwaysId.setChecked(false);
                    }
                    break;

                case R.id.cb_scope_id:
                    if (isChecked) {
                        if (Global.getShangquans().size() <= 0) {
                            return;
                        }
                        if (viewShangquan == null) {
                            viewShangquan = new ViewShangquan(getActivity(), Global.getShangquans());
                        } else if (clickNearby) {
                            viewShangquan.init(mContext, Global.getShangquans());
                        }
                        viewShangquan.setOnSelectListener(shangquanSelectListener);
                        mCatagoryPopupWindow = BasePopupWindow.matchParentPopupWindowOnce(getActivity(), cbScopeId, viewShangquan, false, new BasePopupWindow.OnOutSideTouchListner() {
                            @Override
                            public void onOutSideTouch() {
                                cbScopeId.setChecked(false);
                            }
                        });
                        if (cbScopeId.isChecked()) {
                            if (!mCatagoryPopupWindow.isShowing()) {
                                mCatagoryPopupWindow.showMatchParentPopupDialogWithAlpha(mCatagoryPopupWindow);
                            }
                        }
                        cbCatagoryId.setChecked(false);
                        cbSortwaysId.setChecked(false);
                    }

                    break;

                case R.id.cb_sortways_id:
                    if (isChecked) {
                        viewShanghuPaixu = new ViewShanghuPaixu(getActivity());
                        //                    viewShanghuPaixu.changeCity();
                        viewShanghuPaixu.setOnSelectListener(paixuSelectListener);
                        mCatagoryPopupWindow = BasePopupWindow.matchParentPopupWindowOnce(getActivity(), cbSortwaysId, viewShanghuPaixu, false, new BasePopupWindow.OnOutSideTouchListner() {
                            @Override
                            public void onOutSideTouch() {
                                cbSortwaysId.setChecked(false);
                            }
                        });
                        if (cbSortwaysId.isChecked()) {
                            if (!mCatagoryPopupWindow.isShowing()) {
                                mCatagoryPopupWindow.showMatchParentPopupDialogWithAlpha(mCatagoryPopupWindow);
                            }
                        }
                        cbCatagoryId.setChecked(false);
                        cbScopeId.setChecked(false);
                    }
                    break;
            }
            if (!(cbCatagoryId.isChecked() || cbScopeId.isChecked() || cbSortwaysId.isChecked())) {
                if (mCatagoryPopupWindow.isShowing()) {
                    mCatagoryPopupWindow.disMissWithAlpha(mCatagoryPopupWindow);
                }
                cbCatagoryId.setEnabled(false);
                cbScopeId.setEnabled(false);
                cbSortwaysId.setEnabled(false);
                cbCatagoryId.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            cbCatagoryId.setEnabled(true);
                            cbScopeId.setEnabled(true);
                            cbSortwaysId.setEnabled(true);
                        } catch (Exception e) {

                        }

                    }
                }, 360);
            }
        }
    };

    @Override
    protected void setupData(Bundle savedInstanceState) {
        if (!TextUtils.isEmpty(longitude)) {
            sort = AI;
            requestShanghuListInfo(cityCode, longitude, latitude);
        } else {
            requestShanghuListInfo();
        }
    }

    @Override
    public void onLoadMore() {
        if (!TextUtils.isEmpty(longitude)) {
//            isFirst = true ;
            requestShanghuListInfo(cityCode, longitude, latitude);
        } else {
            requestShanghuListInfo();
        }

    }

    @Override
    public void onShanghuListInfo(MerchantInfoResp merchantInfoResp) {
        if (!TextUtils.isEmpty(longitude)) {
            llSeachResultId.setVisibility(View.VISIBLE);
        }
        if (mCurentPage == 1 && TextUtils.isEmpty(headMerchant)) {
            if (merchantInfoResp.getMerchantList().size() == 0) {
                notSearchRl.setVisibility(View.VISIBLE);
                ptrFreshId.setVisibility(View.INVISIBLE);
            } else {
                notSearchRl.setVisibility(View.INVISIBLE);
                ptrFreshId.setVisibility(View.VISIBLE);
            }
            mHomeRefreshAdapter.resetData(merchantInfoResp.getMerchantList());
            if (merchantInfoResp.getMerchantList().size() < PAGE_PER_SIZE) {
                mHomeRefreshAdapter.loadMoreEnd(true);
            }
            ptrFreshId.refreshComplete();
        } else {
            mHomeRefreshAdapter.addData(merchantInfoResp.getMerchantList());
            LogUtil.e("3 : " + mHomeRefreshAdapter.getData().size());
            if (merchantInfoResp.getMerchantList().size() < PAGE_SIZE) {
                LogUtil.e("2 : " + merchantInfoResp.getMerchantList().size());
                mHomeRefreshAdapter.setLoadEndContent("没有更多店铺了~");
                mHomeRefreshAdapter.loadMoreEnd(false);
                return;
            }
            mHomeRefreshAdapter.loadMoreComplete();
        }

        mCurentPage = Integer.parseInt(merchantInfoResp.getPage());
        headMerchant = merchantInfoResp.getHeadMerchant();
    }

    @Override
    public void onShanghuListInfoError() {
        if (mCurentPage == 1 && TextUtils.isEmpty(headMerchant)) {
            ptrFreshId.refreshComplete();
        } else {
            mHomeRefreshAdapter.loadMoreFail();
        }
    }

    @Override
    public <T> void onItemClick(View view, T t) {
        if (t == null) {
            return;
        }
        MerchantInfoEntiy infoEntiy = (MerchantInfoEntiy) t;
        Bundle bundle = new Bundle();
        bundle.putString("merchantno", infoEntiy.getMerchantno());
        ActivityJumpUtil.next(getActivity(), MerchantDetailActivity.class, bundle, 0);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.delete_history:
                getActivity().setResult(Constant.SEARCH_RESULT);
                getActivity().finish();
                break;
            case R.id.history_rl:
                getActivity().finish();
                break;
            case R.id.right_back_tv:
                getActivity().setResult(Constant.SEARCH_BACK);
                getActivity().finish();
                break;

            case R.id.search_iv:
                startActivity(new Intent(getActivity(), SearchActivity.class));
                break;


            default:
                break;
        }

        if (v.getId() != R.id.cb_catagory_id && v.getId() != R.id.cb_scope_id && v.getId() != R.id.cb_sortways_id) {
            cbCatagoryId.setChecked(false);
            cbScopeId.setChecked(false);
            cbSortwaysId.setChecked(false);
        }
    }

    /**
     * 商户分类item点击选择
     */
    private ViewShanghuFenlei.OnSelectListener shanghuFenleiSelectListener = new ViewShanghuFenlei.OnSelectListener() {

        @Override
        public void getValue(ShanghufenleiInfo info) {
            mCurentPage = 1;
            headMerchant = "";
            String showText = info.getName();
            String parentNo = info.getParentno();
            String categoryNo = info.getCategoryno();
            /* 点击二级分类中的全部且一级分类不选全部，查询一级中的父分类信息 */
            if (showText.equals("全部") && parentNo.equals("ROOT")) {
                categoryno = "";
                keyword = "";
            }
            /* 点击二级分类中的全部且一级分类选择全部，查询全部分类信息 */
            else if (showText.equals("全部") && !parentNo.equals("ROOT")) {
                categoryno = parentNo;
                keyword = "";
            } else {
                categoryno = categoryNo;
                //                keyword = "";
            }

            if (showText.equals("全部") && !parentNo.equals("")) {
                showText = CategoryFenlei.getCategoryParentName(info.getParentno());
            }

            cbCatagoryId.setText(showText);
            cbCatagoryId.setChecked(false);

            mCatagoryPopupWindow.dismiss();
//            isFirst = false ;
            if (!TextUtils.isEmpty(longitude)) {
                requestShanghuListInfo(cityCode, longitude, latitude);
            } else {
                requestShanghuListInfo();
            }
            clickNearby = false;
        }
    };

    /**
     * 商圈item点击选择
     */
    private ViewShangquan.OnSelectListener shangquanSelectListener = new ViewShangquan.OnSelectListener() {

        @Override
        public void getValue(String showText, String districtcode, String areacode) {
            mCurentPage = 1;
            headMerchant = "";
            NearbyFragment.this.districtcode = districtcode;
            NearbyFragment.this.areacode = areacode;
            cbScopeId.setText(showText);
            cbScopeId.setChecked(false);
            mCatagoryPopupWindow.dismiss();
//            isFirst = false ;
            if (!TextUtils.isEmpty(longitude)) {
                requestShanghuListInfo(cityCode, longitude, latitude);
            } else {
                requestShanghuListInfo();
            }
            clickNearby = false;
        }
    };

    /**
     * 排序item点击选择
     */
    private ViewShanghuPaixu.OnSelectListener paixuSelectListener = new ViewShanghuPaixu.OnSelectListener() {

        @Override
        public void getValue(String distance, String showText) {
            mCurentPage = 1;
            headMerchant = "";
            sort = CategoryAsc.getCategoryAscFromName(showText);
            cbSortwaysId.setText(showText);
            cbSortwaysId.setChecked(false);
            mCatagoryPopupWindow.dismiss();
//            isFirst = false ;
            if (!TextUtils.isEmpty(longitude)) {
                if (showText.equals("离我最近")) {
                    requestShanghuListInfo(cityCode, Global.getMyLocation()
                            .getLongitude() + "", Global.getMyLocation()
                            .getLatitude() + "");
                } else {
                    requestShanghuListInfo(cityCode, longitude, latitude);
                }

            } else {
                requestShanghuListInfo();
            }
            clickNearby = false;
        }
    };

    private void requestShanghuListInfo() {
        requestShanghuListInfo(Global.getSelectCityCode(), Global.getMyLocation().longitude + "", Global
                .getMyLocation().latitude + "");
    }

    private void requestShanghuListInfo(String cityCode, String longitude, String latitude) {
        mNearbyPresenter.getShanghuListInfo(/*isFirst*/keyword, categoryno, districtcode, areacode, sort, refundmode, isProduct, isVoucher, cityCode, mCurentPage, PAGE_SIZE, headMerchant, longitude, latitude);
        llSeachResultId.setVisibility(View.VISIBLE);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
