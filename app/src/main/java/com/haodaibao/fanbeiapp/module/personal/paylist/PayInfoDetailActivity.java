package com.haodaibao.fanbeiapp.module.personal.paylist;

import android.content.Intent;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.CountUtil;
import com.baseandroid.utils.FormatUtil;
import com.baseandroid.utils.StatusBarHelper;
import com.baseandroid.utils.TimeUtils;
import com.baseandroid.widget.animatorprovider.FadeAlphaAnimatorProvider;
import com.baseandroid.widget.customimageview.GlideCircleTransform;
import com.bumptech.glide.Glide;
import com.github.nukc.stateview.StateView;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.event.MakoutInvoiceEvent;
import com.haodaibao.fanbeiapp.event.PaylistEvent;
import com.haodaibao.fanbeiapp.module.combo.ComboDetailActivity;
import com.haodaibao.fanbeiapp.module.combo.ComboDetailContract;
import com.haodaibao.fanbeiapp.module.combo.ComboDetailPresenter;
import com.haodaibao.fanbeiapp.module.evaluate.EvaluateSubmitActivity;
import com.haodaibao.fanbeiapp.module.invoice.detail.InvoiceDetailActivity;
import com.haodaibao.fanbeiapp.module.invoice.makeout.MakeoutInvoiceActivity;
import com.haodaibao.fanbeiapp.module.merchant.MerchantDetailActivity;
import com.haodaibao.fanbeiapp.module.pay.PayActivity;
import com.haodaibao.fanbeiapp.module.pay.PayComboActivity;
import com.haodaibao.fanbeiapp.module.personal.balance.BalanceActivity;
import com.haodaibao.fanbeiapp.module.suggest.SuggestActivity;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;
import com.haodaibao.fanbeiapp.repository.json.PackageAllBean;
import com.haodaibao.fanbeiapp.repository.json.PackageAuthBean;
import com.haodaibao.fanbeiapp.repository.json.PayInfoDetailBean;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static com.baseandroid.config.Constant.PACKAGE_STATUS;
import static com.baseandroid.config.Constant.PACKAGE_STATUS01;

public class PayInfoDetailActivity extends BaseActivity implements PayInfoDetailContract.View, ComboDetailContract.View {

    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.tv_merchantname)
    TextView mTvMerchantname;
    @BindView(R.id.tv_order_time)
    TextView mTvOrderTime;
    @BindView(R.id.tv_order_no)
    TextView mTvOrderNo;
    @BindView(R.id.tv_pay_channel)
    TextView mTvPayChannel;
    @BindView(R.id.tv_order_money)
    TextView mTvOrderMoney;
    @BindView(R.id.tv_discount)
    TextView mTvDiscount;
    @BindView(R.id.tv_life_coupoun)
    TextView mTvLifeCoupoun;

    @BindView(R.id.tv_pay_money)
    TextView mTvPayMoney;
    @BindView(R.id.icon_status)
    ImageView icon_status;
    @BindView(R.id.info_discount)
    TextView info_discount;
    @BindView(R.id.goto_buy)
    TextView goto_buy;
    @BindView(R.id.goto_comment)
    TextView goto_comment;
    @BindView(R.id.combo_info_icon)
    ImageView combo_info_icon;
    @BindView(R.id.combo_info_name)
    TextView combo_info_name;
    @BindView(R.id.combo_info_price)
    TextView combo_info_price;
    @BindView(R.id.combo_info_price_yuan)
    TextView combo_info_price_yuan;
    @BindView(R.id.ll_discount)
    LinearLayout ll_discount;
    @BindView(R.id.ll_life_coupoun)
    LinearLayout ll_life_coupoun;
    @BindView(R.id.scroll_content)
    ScrollView scroll_content;
    @BindView(R.id.combo_info_ly)
    RelativeLayout combo_info_ly;
    @BindView(R.id.goto_feed)
    TextView goto_feed;

    @BindView(R.id.discount_rv)
    RecyclerView discount_rv;

    @BindView(R.id.tv_write_invoice)
    TextView tv_write_invoice;
    StateView stateView;
    private String mOrderNo;
    private PayInfoDetailPresenter mPresenter;
    private String merchantno;
    private PayInfoDetailBean payInfoDetailBean;
    private ComboDetailPresenter comboDetailPresenter;
    private MerchantInfoEntiy merchantInfoEntiy = new MerchantInfoEntiy();

    @Override
    protected int getLayoutId() {
        return R.layout.activity_pay_info_detail;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(PayInfoDetailActivity.this);
        mToolbarTitle.setText("交易详情");
        if (mPresenter == null) {
            mPresenter = new PayInfoDetailPresenter(this);
        }
        EventBus.getDefault().register(this);
        combo_info_price_yuan.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        mOrderNo = getIntent().getStringExtra("orderNo");
        merchantno = getIntent().getStringExtra("merchantno");
        initStateView();
        if (!TextUtils.isEmpty(mOrderNo)) {
            mPresenter.getPayInfoDetailData(mOrderNo);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PaylistEvent event) {
        if (event.isComment()) {
            goto_comment.setVisibility(View.GONE);
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MakoutInvoiceEvent event) {
        payInfoDetailBean.invoiceStatus = event.makeoutApplyBean.invoice.status;
        setInvoice(tv_write_invoice, payInfoDetailBean);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void initStateView() {
        stateView = StateView.inject(scroll_content);
        stateView.showLoading();
        stateView.setAnimatorProvider(new FadeAlphaAnimatorProvider());
        stateView.setOnRetryClickListener(new StateView.OnRetryClickListener() {
            @Override
            public void onRetryClick() {
                mPresenter.getPayInfoDetailData(mOrderNo);
            }
        });
    }

    @OnClick({R.id.toolbar_back, R.id.goto_buy, R.id.goto_comment, R.id.tv_merchantname, R.id.combo_info_ly, R.id.goto_feed, R.id.info_discount, R.id.tv_write_invoice})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.goto_buy:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                if (TextUtils.equals("1", payInfoDetailBean.relationType)) {
                    doComboClick(payInfoDetailBean);
                } else {
                    PayActivity.openForResult(PayInfoDetailActivity.this, null, payInfoDetailBean.getMerchantno(), PayActivity.OTHER, false);
                }
                break;
            case R.id.goto_comment:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                EvaluateSubmitActivity.open(PayInfoDetailActivity.this, payInfoDetailBean.getMerchantno(), mOrderNo);
                break;
            case R.id.tv_merchantname:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                //bundle.putString("merchantno", infoEntiy.getMerchantno());
                Intent intent = new Intent(PayInfoDetailActivity.this, MerchantDetailActivity.class);
                intent.putExtra("merchantno", payInfoDetailBean.getMerchantno());
                startActivity(intent);
                break;
            case R.id.combo_info_ly:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                if (TextUtils.equals("1", payInfoDetailBean.relationType)) {
                    if (comboDetailPresenter == null) {
                        comboDetailPresenter = new ComboDetailPresenter(this);
                    }
                    merchantInfoEntiy.setAddress(payInfoDetailBean.address);
                    merchantInfoEntiy.setCityCode(payInfoDetailBean.citycode);
                    merchantInfoEntiy.setMerchantname(payInfoDetailBean.getMerchantname());
                    merchantInfoEntiy.setMerchantno(payInfoDetailBean.getMerchantno());
                    comboDetailPresenter.checkMerchantPackageRule(payInfoDetailBean.getMerchantno(), payInfoDetailBean.packageNo);
                } else {
                    Intent intent1 = new Intent(PayInfoDetailActivity.this, MerchantDetailActivity.class);
                    intent1.putExtra("merchantno", payInfoDetailBean.getMerchantno());
                    startActivity(intent1);
                }
                break;
            case R.id.goto_feed:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                startActivity(new Intent(PayInfoDetailActivity.this, SuggestActivity.class));
                break;
            case R.id.info_discount:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                Intent balanceIntent = new Intent(PayInfoDetailActivity.this, BalanceActivity.class);
//                balanceIntent.putExtra("balance", mBalance);
                startActivity(balanceIntent);
            case R.id.tv_write_invoice:
                if (TextUtils.equals(payInfoDetailBean.invoiceStatus, "1")) {
                    InvoiceDetailActivity.start(this, payInfoDetailBean.invoiceId);
                } else {
                    MakeoutInvoiceActivity.start(this, mOrderNo, merchantno, payInfoDetailBean.invoiceId, false);
                }
                break;
            default:
                break;
        }

    }

    private void doComboClick(PayInfoDetailBean item) {
        if (comboDetailPresenter == null) {
            comboDetailPresenter = new ComboDetailPresenter(this);
        }
        merchantInfoEntiy.setAddress(item.address);
        merchantInfoEntiy.setCityCode(item.citycode);
        merchantInfoEntiy.setMerchantname(item.getMerchantname());
        merchantInfoEntiy.setMerchantno(item.getMerchantno());
        comboDetailPresenter.getComboDetail(item.getMerchantno(), item.packageNo);
    }

    //根据返回数据更新界面
    @Override
    public void setUpPayInfoData(PayInfoDetailBean result) {
        if (result == null) {
            return;
        }
        payInfoDetailBean = result;
        String url = result.getMerchantImageUrl();
        Glide.with(this).load(url).asBitmap()
                .placeholder(R.drawable.image_merchant_placehold).error(R.drawable.image_merchant_placehold)
                .transform(new GlideCircleTransform(this))
                .into(combo_info_icon);
        mTvMerchantname.setText(result.getMerchantname());
        mTvOrderTime.setText(TimeUtils.getOrderTime(result.getPaytime()));
        mTvOrderNo.setText(mOrderNo);
        mTvPayChannel.setText(getPayChannel(result.getPayflows()));
        mTvOrderMoney.setText(getString(R.string.rmb) + " " + result.getOrderamt());
        if (!TextUtils.isEmpty(result.getReduceamount()) && Double.parseDouble(result.getReduceamount()) > 0.0) {
            ll_discount.setVisibility(View.VISIBLE);
            mTvDiscount.setText("-" + getString(R.string.rmb) + " " + FormatUtil.formatAmount(2, result.getReduceamount()));
        } else {
            ll_discount.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(result.getCouponamt()) && Double.parseDouble(result.getCouponamt()) > 0.0) {
            ll_life_coupoun.setVisibility(View.VISIBLE);
            mTvLifeCoupoun.setText("-" + getString(R.string.rmb) + " " + FormatUtil.formatAmount(2, result.getCouponamt()));
        } else {
            ll_life_coupoun.setVisibility(View.GONE);
        }

        //2018.04.17添加优惠活动列表
        DiscountRvAdapter mAdapter = new DiscountRvAdapter(this);
        discount_rv.setLayoutManager(new LinearLayoutManager(this));
        discount_rv.setAdapter(mAdapter);
        mAdapter.resetData(result.getActivities());

        String cashamt = result.getCashamt();
        mTvPayMoney.setText(getString(R.string.rmb) + FormatUtil.formatAmount(2, cashamt));

        if (TextUtils.equals("1", result.relationType)) {
            combo_info_price.setVisibility(View.VISIBLE);
            combo_info_price_yuan.setVisibility(View.VISIBLE);
            combo_info_price_yuan.setText(getString(R.string.rmb) + " " + FormatUtil.formatAmount(2, result.originalPrice));
            combo_info_price.setText(getString(R.string.rmb) + " " + FormatUtil.formatAmount(2, result.preferentialPrice));
            if (TextUtils.equals("02", result.faceUserTypeCode)) {
                combo_info_name.setText("[新顾客专享]" + result.packageName);
            } else if (TextUtils.equals("03", result.faceUserTypeCode)) {
                combo_info_name.setText("[老顾客专享]" + result.packageName);
            } else {
                combo_info_name.setText(result.packageName);
            }
        } else {
            combo_info_price.setVisibility(View.GONE);
            combo_info_price_yuan.setVisibility(View.GONE);
            combo_info_name.setText("优惠买单，消费" + getString(R.string.rmb) + FormatUtil.formatAmount(2, cashamt));
        }

        String refundstatus = result.getRefundstatus();
        String status = result.getStatus();
        switch (refundstatus) {
            case "":
            case "01"://交易成功
                goto_buy.setVisibility(View.VISIBLE);
                goto_feed.setVisibility(View.GONE);
                if (TextUtils.equals("50", result.commentstatus)) {
                    goto_comment.setVisibility(View.VISIBLE);
                } else {
                    goto_comment.setVisibility(View.GONE);
                }
                mToolbarTitle.setText("交易详情");
                info_discount.setEnabled(false);
                if (TextUtils.isEmpty(status)) {
                    return;
                } else if (status.equals("20")) {// 20：交易成功
                    icon_status.setImageResource(R.drawable.icon_pay_success);
                    info_discount.setText(getPreferentialAmount(result.getCashamt(), result.getOrderamt()));

                } else {
                    icon_status.setImageResource(R.drawable.icon_pay_success);
                    info_discount.setText("消费多多，优惠多多~");
                }
                break;
            case "20"://退款成功
                goto_buy.setVisibility(View.GONE);
                goto_feed.setVisibility(View.VISIBLE);
                goto_comment.setVisibility(View.GONE);
                info_discount.setEnabled(true);
                mToolbarTitle.setText("退款详情");
                icon_status.setImageResource(R.drawable.image_refund_success);
                info_discount.setText("已退款至你的余额，请点击查看");
                Drawable rightDrawable = getResources().getDrawable(R.drawable.icon_right_white);
                rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());
                info_discount.setCompoundDrawables(null, null, rightDrawable, null);
                break;
            case "11"://退款中
                mToolbarTitle.setText("退款详情");
                icon_status.setImageResource(R.drawable.icon_refunding);
                info_discount.setVisibility(View.INVISIBLE);
                goto_feed.setVisibility(View.VISIBLE);
                goto_buy.setVisibility(View.GONE);
                goto_comment.setVisibility(View.GONE);
                break;

        }
        setInvoice(tv_write_invoice, payInfoDetailBean);
    }

    private void setInvoice(TextView tv_write_invoice, PayInfoDetailBean item) {
        if (TextUtils.equals(item.canInvoice, "0")) {
            tv_write_invoice.setVisibility(View.GONE);
        } else if (TextUtils.equals(item.canInvoice, "1")) {
            if (TextUtils.equals(item.invoiceStatus, "1")) {
                tv_write_invoice.setVisibility(View.VISIBLE);
                tv_write_invoice.setText("查看发票");
            } else if (TextUtils.equals(item.invoiceStatus, "2") || TextUtils.equals(item.invoiceStatus, "3")) {
                tv_write_invoice.setVisibility(View.VISIBLE);
                tv_write_invoice.setText("重开发票");
            } else {
                tv_write_invoice.setVisibility(View.VISIBLE);
                tv_write_invoice.setText("申请发票");
            }
        }
    }

    @Override
    public void onRetry() {
        stateView.showRetry();
    }

    @Override
    public void closeUI() {
        mToolbarTitle.postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 1000);
    }

    @Override
    public void onContent() {
        stateView.showContent();
    }

    //根据接口返回的支付信息获取支付方式
    private String getPayChannel(List<PayInfoDetailBean.PayflowsBean> payflows) {
        String result = "";
        for (PayInfoDetailBean.PayflowsBean item : payflows) {
            String paytype = item.getPaytype();
            if (!TextUtils.isEmpty(paytype)) {
                switch (paytype) {
                    case "01":
                        result = "现金账户";
                        break;
                    case "02":
                        result = "银行卡";
                        break;
                    case "03":
                        result = "微信";
                        break;
                    case "04":
                        result = "支付宝";
                        break;
                    case "05":
                        result = "微信";
                        break;
                    case "06":
                        result = "刷卡";
                        break;
                    case "07":
                        result = "支付宝";
                        break;
                    case "08":
                        result = "支付宝";
                        break;
                    case "11":
//                        result = "红包抵扣";
                        result = "";
                        break;
                    case "23":
                        result = "微信";
                        break;
                    case "24":
                        result = "支付宝";
                        break;
                    case "26":
                        result = "白花花";
                        break;
                }
            }
        }
        return result;
    }

    public String getPreferentialAmount(String cashamt, String orderamt) {
        double cashamtDou = Double.valueOf(FormatUtil.formatAmount(2, cashamt));
        double orderamtDou = Double.valueOf(FormatUtil.formatAmount(2, orderamt));
        double sub = CountUtil.sub(orderamtDou, cashamtDou);
        return sub > 0 ? "厉害啦~ 省了" + "¥" + FormatUtil.filterDecimalPoint(sub + "") + "哦!" : "消费多多，优惠多多~";
    }

    @Override
    public void updateUI(PackageAllBean packageAllBean) {
        if (TextUtils.equals(PACKAGE_STATUS, packageAllBean.packageAuth.packageStatus)) {
            PayComboActivity.openForResult(PayInfoDetailActivity.this, packageAllBean.packageDetail.merchantno, merchantInfoEntiy, packageAllBean.packageDetail);
        } else {
            PayActivity.openForResult(PayInfoDetailActivity.this, null, packageAllBean.packageDetail.merchantno, PayActivity.OTHER, false);
        }
    }

    @Override
    public void checkedUI(PackageAuthBean packageAuthBean) {
        if (TextUtils.equals(PACKAGE_STATUS01, packageAuthBean.packageStatus)) {
            showToast("该活动已结束");
        } else {
            ComboDetailActivity.start(PayInfoDetailActivity.this, payInfoDetailBean.packageNo, payInfoDetailBean.getMerchantno(), merchantInfoEntiy);
        }
    }

    @Override
    public void showRetry() {
        //do nothing
    }
}
