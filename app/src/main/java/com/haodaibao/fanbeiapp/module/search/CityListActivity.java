package com.haodaibao.fanbeiapp.module.search;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Global;
import com.baseandroid.utils.StatusBarHelper;
import com.baseandroid.widget.customtext.edittextview.EditTextWithDelNormal;
import com.github.promeg.pinyinhelper.Pinyin;
import com.github.promeg.tinypinyin.lexicons.android.cncity.CnCityDict;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.search.adpter.CityListadapter;
import com.haodaibao.fanbeiapp.module.search.adpter.SearchCityListAdapter;
import com.haodaibao.fanbeiapp.module.search.utils.PinyinComparator;
import com.haodaibao.fanbeiapp.module.search.view.StickyRecyclerHeadersDecoration;
import com.haodaibao.fanbeiapp.module.search.view.ZsideBar;
import com.haodaibao.fanbeiapp.repository.json.OpenCityBean;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;

import static com.haodaibao.fanbeiapp.database.GreenDaoDbHelp.loadallCityInfo;
import static com.haodaibao.fanbeiapp.database.GreenDaoDbHelp.queryCityInfoLike;

public class CityListActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.toolbar_back)
    View toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.icon_search)
    ImageView iconSearch;
    @BindView(R.id.city_searchtv)
    EditTextWithDelNormal citySearchtv;
    @BindView(R.id.city_searchlay)
    RelativeLayout citySearchlay;
    @BindView(R.id.city_recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.zsidebar)
    ZsideBar zsideBar;
    @BindView(R.id.city_seartch_recyclerview)
    RecyclerView searchRecyclerview;
    @BindView(R.id.activity_city_list)
    RelativeLayout activityCityList;

    private CityListadapter adapter;
    private SearchCityListAdapter searchAdapter;

    private Comparator<OpenCityBean> pinyinComparator;
    public static final int CITYLIST_RESULT_CODE = 1002;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_city_list;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(this);

        Pinyin.init(Pinyin.newConfig().with(CnCityDict.getInstance(CityListActivity.this)));

        pinyinComparator = new PinyinComparator();
        toolbarBack.setOnClickListener(this);
        toolbarTitle.setText("当前位置：" + Global.getSelectCity());

        searchAdapter = new SearchCityListAdapter(CityListActivity.this);
        LayoutManager layoutManager = new LinearLayoutManager(this);
        searchRecyclerview.setLayoutManager(layoutManager);
        searchRecyclerview.setAdapter(searchAdapter);

        citySearchtv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    zsideBar.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                    searchRecyclerview.setVisibility(View.VISIBLE);
                    List<OpenCityBean> citysResult = queryCityInfoLike(s.toString()).list();
                    searchAdapter.resetData(citysResult);
                    searchAdapter.notifyDataSetChanged();
                } else {
                    zsideBar.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.VISIBLE);
                    searchRecyclerview.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        List<OpenCityBean> citysList = loadallCityInfo();
        initdatas(citysList);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(CityListActivity.this);
        recyclerView.setLayoutManager(layoutManager1);
        recyclerView.setAdapter(adapter);

        recyclerView.addItemDecoration(new StickyRecyclerHeadersDecoration(adapter));
        zsideBar.setupWithRecycler(recyclerView);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
    }

    private void initdatas(List<OpenCityBean> citysList) {
        for (OpenCityBean e : citysList) {
            String pinyin = Pinyin.toPinyin(e.getName(), "");
            String sortString = pinyin.substring(0, 1).toUpperCase();

            if (sortString.matches("[A-Z]")) {
                e.setSortLetters(sortString.toUpperCase());
            } else {
                e.setSortLetters("#");
            }
        }
        Collections.sort(citysList, pinyinComparator);
        OpenCityBean openCityInfoEntity = new OpenCityBean();
        openCityInfoEntity.setSortLetters("$");
        openCityInfoEntity.setName(Global.getSelectCity());
        openCityInfoEntity.setCode(Global.getSelectCityCode());
        openCityInfoEntity.setType(Global.getSelectCitySiteType());
        citysList.add(0, openCityInfoEntity);
        adapter = new CityListadapter(this);
        adapter.resetData(citysList);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.toolbar_back:
                finish();
                break;

            default:

                break;
        }
    }
}
