package com.haodaibao.fanbeiapp.module.merchant.temporarydata;

import android.support.annotation.Keep;

import java.io.Serializable;

/**
 * 开发者：LuoYi
 * Time: 2017 10:48 2017/9/12 09
 */

@Keep
public interface MerchantTemporary extends Serializable{

    int getmType();

}
