package com.haodaibao.fanbeiapp.module.personal.message;

import android.os.Bundle;
import android.text.TextUtils;

import com.baseandroid.base.BaseFragment;
import com.baseandroid.config.Global;
import com.baseandroid.utils.RxUtils;
import com.haodaibao.fanbeiapp.module.home.HomeActivity;
import com.haodaibao.fanbeiapp.module.merchant.MerchantDetailActivity;
import com.haodaibao.fanbeiapp.module.personal.coupon.CouponActivity;
import com.haodaibao.fanbeiapp.module.personal.paylist.PayInfoDetailActivity;
import com.haodaibao.fanbeiapp.module.personal.paylist.PaylistActivity;
import com.haodaibao.fanbeiapp.module.search.SearchActivity;
import com.haodaibao.fanbeiapp.module.search.SearchDeliciousActivity;
import com.haodaibao.fanbeiapp.module.yaohe.YaoheActivity;
import com.haodaibao.fanbeiapp.repository.LifeRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.MessageListBean;
import com.haodaibao.fanbeiapp.webview.WebviewActivity;
import com.haodaibao.fanbeiapp.webview.WebviewTbsActivity;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;

import static com.baseandroid.base.BaseWebviewActivity.WEBVIEW_TITLE;
import static com.baseandroid.base.BaseWebviewActivity.WEBVIEW_URL;
import static com.haodaibao.fanbeiapp.webview.WebviewTbsActivity.MINITTITLE_KEY;
import static com.haodaibao.fanbeiapp.webview.WebviewTbsActivity.MINIURL_KEY;

/**
 * Created by Routee on 2017/9/21.
 * description: ${cusor}
 */

public class MessagePresenter implements MessageListContract.Presenter {
    private static final String CT_TEXT = "01";// 文本
    private static final String CT_URL = "02";// 地址URL
    private static final String CT_APP = "03";// APP界面
    private static final String CT_YAOHE = "04";// 吆喝页面

    private final MessageListContract.View mView;

    public MessagePresenter(MessageListContract.View view) {
        mView = view;
    }

    @Override
    public MessageJumpBean messageJump(MessageListBean.ResultBean.ItemsBean itemsBean) {
        MessageJumpBean jumpBean = new MessageJumpBean();
        Bundle bundle = new Bundle();
        String title = "消息";
        switch (itemsBean.getContenttype()) {
            case CT_TEXT:
                bundle.putString("messageNo", itemsBean.getMessageno());// 消息编号
                bundle.putString("title", itemsBean.getTitle());// 消息标题
                bundle.putString("msg_title", itemsBean.getTitle());// 消息内容标题
                bundle.putString("content", itemsBean.getContent());// 消息内容
                bundle.putString("effectivetime", itemsBean.getEffectivetime());// 消息时间
                jumpBean.setaBundle(bundle);
                jumpBean.setaClass(MessageDetailActivity.class);
                return jumpBean;

            case CT_URL:
                bundle.putString(MINIURL_KEY, title);
                bundle.putString(MINITTITLE_KEY, itemsBean.getContent());
                jumpBean.setaBundle(bundle);
                jumpBean.setaClass(WebviewTbsActivity.class);
                return jumpBean;

            case CT_APP:// 跳转app内部页面
                if (!TextUtils.isEmpty(itemsBean.getContent())) {
                    if (itemsBean.getContent().contains("?")) {
                        String[] to = itemsBean.getContent().split("\\?");
                        String toWhere = to[0];// merchant_detail
                        String param = to[1];// merchantNo=M20160101100009
                        jumpBean = getJumpBean(jumpBean, toWhere, param);
                    } else {
                        jumpBean = getJumpBean1(jumpBean, itemsBean.getContent());
                    }
                } else {
                    bundle.putString("messageNo", itemsBean.getMessageno());// 消息编号
                    bundle.putString("title", title);// 消息标题
                    bundle.putString("content", itemsBean.getContent());// 消息内容
                    bundle.putString("effectivetime", itemsBean.getEffectivetime());// 消息时间
                    jumpBean.setaBundle(bundle);
                    jumpBean.setaClass(MessageDetailActivity.class);
                }
                return jumpBean;

            case CT_YAOHE:
                bundle.putString("messageNo", itemsBean.getMessageno());
                jumpBean.setaBundle(bundle);
                jumpBean.setaClass(YaoheActivity.class);
                return jumpBean;

            default:
                return jumpBean;
        }
    }

    private MessageJumpBean getJumpBean(MessageJumpBean jumpBean, String toWhere, String param) {
        Bundle bundle = new Bundle();
        switch (toWhere) {
            case "merchant_list":// 打开商户列表
                if (!TextUtils.isEmpty(param) && param.contains("cityCode=")) {
                    String cityCode = param.split("cityCode=")[1];
                    bundle.putString("cityCode", cityCode);
                }
                bundle.putString("keyword", "ALL");
                jumpBean.setaBundle(bundle);
                jumpBean.setaClass(SearchActivity.class);
                return jumpBean;
            case "merchant_detail":// 打开商户详情
                if (!TextUtils.isEmpty(param) && param.contains("merchantNo=")) {
                    String merchantno = param.split("merchantNo=")[1];
                    bundle.putString("merchantno", merchantno);
                }
                jumpBean.setaBundle(bundle);
                jumpBean.setaClass(MerchantDetailActivity.class);
                return jumpBean;
            case "order_detail":// 打开“订单详情”
                if (!TextUtils.isEmpty(param) && param.contains("orderNo=")) {
                    String orderno = param.split("orderNo=")[1];
                    bundle.putString("orderNo", orderno);
                }
                jumpBean.setaBundle(bundle);
                jumpBean.setaClass(PayInfoDetailActivity.class);
                return jumpBean;
            case "product_detail":// 打开商品详情
                return jumpBean;
            default:
                return jumpBean;
        }
    }

    private MessageJumpBean getJumpBean1(MessageJumpBean jumpBean, String content) {
        Bundle bundle = new Bundle();
        switch (content) {
            case "my_index":// 打开“我的”
                jumpBean.closeActivity = true;
                jumpBean.setaBundle(bundle);
                jumpBean.setaClass(HomeActivity.class);
                return jumpBean;
            case "coupon_list":// 打开“红包列表”
                jumpBean.setaBundle(bundle);
                jumpBean.setaClass(CouponActivity.class);
                return jumpBean;
            case "voucher_list":// 打开“卡券列表”
                jumpBean.setaBundle(bundle);
                jumpBean.setaClass(CouponActivity.class);
                return jumpBean;
            case "order_list":// 打开“交易记录”
                jumpBean.setaBundle(bundle);
                jumpBean.setaClass(PaylistActivity.class);
                return jumpBean;
            case "index":// 打开“用户app首页”
                jumpBean.closeActivity = true;
                jumpBean.setaBundle(bundle);
                jumpBean.setaClass(HomeActivity.class);
                return jumpBean;
            default:
                return jumpBean;

        }
    }


    @Override
    public void getMessageList(String page, String type) {
        Map<String, String> params = new HashMap<>();
        params.put("page", page);
        params.put("limit", "20");
        params.put("userNo", Global.getUserInfo().getUserno() == null ? "" : Global.getUserInfo().getUserno());
        params.put("userChannel", "11");
        params.put("type", type);
        LifeRepository.getInstance().getMessageList(params).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<MessageListBean>>applySchedulersLifeCycle((BaseFragment) mView))
                .subscribe(new RxObserver<Data<MessageListBean>>() {
                    @Override
                    public void onNext(@NonNull Data<MessageListBean> data) {
                        if (checkJsonCode(data, false)) {
                            mView.setupMessageListData(data.getResult());
                        }
                    }
                });
    }

    @Override
    public void setMsgReaded(String msgNo) {
        Map<String, String> params = new HashMap<>();
        params.put("userNo", Global.getUserInfo().getUserno());
        params.put("messageNos", msgNo);
        LifeRepository.getInstance().setMsgReaded(params).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data>applySchedulersLifeCycle((BaseFragment) mView))
                .subscribe(new RxObserver<Data>() {
                    @Override
                    public void onNext(@NonNull Data data) {
                        if (checkJsonCode(data, false)) {
                            mView.setMsgReadedSuccess();
                        }
                    }
                });
    }

    @Override
    public void deleteMsg(String msgNo) {
        Map<String, String> params = new HashMap<>();
        params.put("userNo", Global.getUserInfo().getUserno());
        params.put("messageNos", msgNo);
        LifeRepository.getInstance().deletMsg(params).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data>applySchedulersLifeCycle((BaseFragment) mView))
                .subscribe(new RxObserver<Data>() {
                    @Override
                    public void onNext(@NonNull Data data) {
                        if (checkJsonCode(data, false)) {
                            mView.deleteMsgSuccess();
                        }
                    }
                });
    }
}
