package com.haodaibao.fanbeiapp.module.invoice.makeout;

import android.text.TextUtils;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.RxUtils;
import com.haodaibao.fanbeiapp.repository.LifeRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.MakeoutApplyBean;
import com.haodaibao.fanbeiapp.repository.json.MakeoutInvoiceBean;

import java.util.HashMap;
import java.util.Map;

/**
 * date: 2018/2/11.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class MakeoutInvoicePresenter implements MakeoutInvoiceContract.Presenter {
    private MakeoutInvoiceContract.View mView;

    public MakeoutInvoicePresenter(MakeoutInvoiceContract.View mView) {
        this.mView = mView;
    }

    @Override
    public void getApplyInfo(String orderNo, String merchantNo) {
        Map<String, String> map = new HashMap<>();
        map.put("orderNo", orderNo);
        map.put("merchantNo", merchantNo);
        LifeRepository.getInstance().getApplyInfo(map, true)
                .compose(RxUtils.<Data<MakeoutApplyBean>>applySchedulersLifeCycle((BaseActivity) mView))
                .subscribe(new RxObserver<Data<MakeoutApplyBean>>() {
                    @Override
                    public void onNext(Data<MakeoutApplyBean> makeoutApplyBeanData) {
                        if (checkJsonCode(makeoutApplyBeanData, true)) {
                            mView.updateUI(makeoutApplyBeanData.getResult().invoice);
                        }
                    }
                });
    }

    @Override
    public void applyInvoice(String id, String orderNo, String merchantNo, String invoiceTypeCode, String riseTypeCode, String riseNo, String riseName, String taxNo, String mobile, String email, String companyAddress, String companyMobile, String companyBankName, String companyBankNo, String invoiceProject, String invoiceAmount) {
        Map<String, String> map = new HashMap<>();
        if (!TextUtils.isEmpty(id)) {
            map.put("id", id);
        }
        map.put("orderNo", orderNo);
        map.put("merchantNo", merchantNo);
        map.put("invoiceTypeCode", "1");
        map.put("riseTypeCode", riseTypeCode);
        if (!TextUtils.isEmpty(riseNo)) {
            map.put("riseNo", riseNo);
        }
        map.put("riseName", riseName);
        map.put("taxNo", taxNo);
        map.put("mobile", mobile);
        map.put("email", email);
        map.put("companyAddress", companyAddress);
        map.put("companyMobile", companyMobile);
        map.put("companyBankName", companyBankName);
        map.put("companyBankNo", companyBankNo);
        map.put("invoiceProject", invoiceProject);
        map.put("invoiceAmount", invoiceAmount);
        LifeRepository.getInstance().applyInvoice(map, true)
                .compose(RxUtils.<Data<MakeoutInvoiceBean>>applySchedulersLifeCycle((BaseActivity) mView))
                .subscribe(new RxObserver<Data<MakeoutInvoiceBean>>() {
                    @Override
                    public void onNext(Data<MakeoutInvoiceBean> makeoutApplyBeanData) {
                        if (checkJsonCode(makeoutApplyBeanData, false)) {
                            mView.upDateStatus(true, makeoutApplyBeanData.getResult());
                        } else {
                            mView.upDateStatus(false, null);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        mView.upDateStatus(false, null);
                    }
                });
    }
}
