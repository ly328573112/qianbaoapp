package com.haodaibao.fanbeiapp.module.shangquantab;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.baseandroid.config.Global;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.Consent;
import com.haodaibao.fanbeiapp.module.bizutils.CategoryAsc;
import com.haodaibao.fanbeiapp.module.bizutils.CategoryAsc1;
import com.haodaibao.fanbeiapp.module.search.view.ViewBaseAction;
import com.haodaibao.fanbeiapp.module.shangquantab.adapter.TextStringAdapter;

import java.util.ArrayList;
import java.util.List;

public class ViewShanghuPaixu extends RelativeLayout implements ViewBaseAction {

    private RecyclerView mListView;
    private List<String> items = new ArrayList<>();
    private List<String> itemsVaule = new ArrayList<>();
    private OnSelectListener mOnSelectListener;
    private TextStringAdapter adapter;
    private String showText = CategoryAsc.names[0];

    public String getShowText() {
        return showText;
    }

    public void changeCity() {
        String[] itemsTemp = Global.getMyLocation()
                .getAddress()
                .contains(Global.getSelectCity()) ? CategoryAsc.names : CategoryAsc1.names;    //
        String[] itemsVauleTemp = Global.getMyLocation()
                .getAddress()
                .contains(Global.getSelectCity()) ? CategoryAsc.ids : CategoryAsc1.ids;    // 隐藏id{"1","2","3"}
        items.clear();
        itemsVaule.clear();
        for (int i = 0; i < itemsTemp.length; i++) {
            items.add(i, itemsTemp[i]);
            itemsVaule.add(i, itemsVauleTemp[i]);
        }
        adapter.resetData(items);
        adapter.notifyDataSetChanged();
    }

    public ViewShanghuPaixu(Context context) {
        super(context);
        init(context);
    }

    public ViewShanghuPaixu(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public ViewShanghuPaixu(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        String[] itemsTemp = Consent.currentLocation ? CategoryAsc.names : CategoryAsc1.names;    //
        String[] itemsVauleTemp = Consent.currentLocation ? CategoryAsc.ids : CategoryAsc1.ids;    // 隐藏id{"1","2","3"}
        items.clear();
        itemsVaule.clear();
        for (int i = 0; i < itemsTemp.length; i++) {
            items.add(i, itemsTemp[i]);
            itemsVaule.add(i, itemsVauleTemp[i]);
        }


        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_distance, this, true);
        mListView = (RecyclerView) findViewById(R.id.listView);
        adapter = new TextStringAdapter(context, R.color.c_F62241, R.drawable.choose_item_right, R.drawable.choose_eara_item_selector);
        mListView.setLayoutManager(new LinearLayoutManager(context));
        adapter.resetData(items);
        adapter.setTextSize(14);
        mListView.setAdapter(adapter);
        for (int i = 0; i < itemsVaule.size(); i++) {
            if (itemsVaule.get(i).equals(itemsVauleTemp[0])) {
                adapter.setSelectedPositionNoNotify(i, items.get(i));
                showText = items.get(i);
                break;
            }
        }
        adapter.setOnItemClickListener(new TextStringAdapter.OnPositionClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                if (mOnSelectListener != null) {
                    showText = items.get(position);
                    mOnSelectListener.getValue(itemsVaule.get(position), items.get(position));
                }
            }
        });
    }

    public void setOnSelectListener(OnSelectListener onSelectListener) {
        mOnSelectListener = onSelectListener;
    }

    public interface OnSelectListener {
        void getValue(String distance, String showText);
    }

    @Override
    public void hide() {

    }

    @Override
    public void show() {

    }

}
