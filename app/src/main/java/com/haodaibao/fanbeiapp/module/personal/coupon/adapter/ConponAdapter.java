package com.haodaibao.fanbeiapp.module.personal.coupon.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;

/**
 * create by 曹伟 on 2018/05/24
 * pro : 可用优惠券列表适配器
 * e-mail : caowei@qianbaocard.com
 */
public class ConponAdapter extends BaseRecycleViewAdapter {

    public ConponAdapter(Context context) {
        super(context);
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, Object item) {

    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }
}
