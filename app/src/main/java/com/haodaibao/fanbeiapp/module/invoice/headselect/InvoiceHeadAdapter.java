package com.haodaibao.fanbeiapp.module.invoice.headselect;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.InvoiceHeadBean;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * date: 2018/2/6.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class InvoiceHeadAdapter extends BaseRecycleViewAdapter<InvoiceHeadBean.InvoiceHeadItem> {
    private OnItemClickListener onItemClickListener;
    private boolean isSelect;
    private String riseId;
    public int position;

    public InvoiceHeadAdapter(Context context, OnItemClickListener onItemClickListener) {
        super(context);
        this.onItemClickListener = onItemClickListener;
        if (context instanceof InvoiceHeadSelectActivity) {
            isSelect = true;
            riseId = ((InvoiceHeadSelectActivity) context).getRiseId();
        } else {
            isSelect = false;
        }
    }

    @Override
    protected void onBindBaseViewHolder(final RecyclerView.ViewHolder holder, final InvoiceHeadBean.InvoiceHeadItem item) {
        InvoiceHeadHolder invoiceHeadHolder = (InvoiceHeadHolder) holder;
        invoiceHeadHolder.invoice_merchantname.setText(item.rise_name);
        if (TextUtils.equals(item.rise_type_code, "1")) {
            invoiceHeadHolder.invoice_tag.setText("公司");
            invoiceHeadHolder.invoice_taxnumber.setVisibility(View.VISIBLE);
            invoiceHeadHolder.invoice_taxnumber.setText("税号：" + item.tax_no);
        } else if (TextUtils.equals(item.rise_type_code, "2")) {
            invoiceHeadHolder.invoice_tag.setText("个人");
            invoiceHeadHolder.invoice_taxnumber.setVisibility(View.GONE);
        }
        if (isSelect) {
            invoiceHeadHolder.iv_arrow.setVisibility(View.GONE);
            invoiceHeadHolder.icon_select.setVisibility(View.VISIBLE);
        } else {
            invoiceHeadHolder.iv_arrow.setVisibility(View.VISIBLE);
            invoiceHeadHolder.icon_select.setVisibility(View.GONE);
        }
        if (TextUtils.equals(item.rise_id, riseId)) {
            invoiceHeadHolder.icon_select.setChecked(true);
        } else {
            invoiceHeadHolder.icon_select.setChecked(false);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position = holder.getAdapterPosition();
                onItemClickListener.onItemClick(holder.itemView, item);
            }
        });
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new InvoiceHeadHolder(LayoutInflater.from(mContext).inflate(R.layout.item_invoice_head, parent, false));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    public class InvoiceHeadHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.invoice_merchantname)
        TextView invoice_merchantname;
        @BindView(R.id.invoice_tag)
        TextView invoice_tag;
        @BindView(R.id.invoice_taxnumber)
        TextView invoice_taxnumber;
        @BindView(R.id.icon_select)
        CheckBox icon_select;
        @BindView(R.id.iv_arrow)
        ImageView iv_arrow;

        public InvoiceHeadHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
