package com.haodaibao.fanbeiapp.module.evaluate;

import com.baseandroid.base.IView;
import com.baseandroid.widget.RouteeFlowLayout;
import com.haodaibao.fanbeiapp.repository.json.CommentListBean;

import java.util.List;

/**
 * 开发者：LuoYi
 * Time: 2017 16:29 2017/9/12 09
 */

public interface EvaluateContract {

    interface Presenter {

        void getEvaluateData(String merchantno, String queryType, String ifcount, String page, String limit);

        void submintComment(String orderno, String merchantno, String star, String comment, String imgurls);

    }

    interface EvaluateView extends IView {

        void onEvaluateList(CommentListBean commentListBean);

        void onEvaluateListError();

        void onSubmint();

        void onSubmintError();
    }

    interface RouteeView {

        void setRouteeFlowView(RouteeFlowLayout tag_comment, List<CommentListBean.MyHeader> myHeader, String queryType);

    }



    interface ChangeMark {

        void changeMark(String type);

    }


}
