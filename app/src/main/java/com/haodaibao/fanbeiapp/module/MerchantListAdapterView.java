package com.haodaibao.fanbeiapp.module;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amap.api.maps.model.LatLng;
import com.baseandroid.config.Global;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.FormatUtil;
import com.baseandroid.utils.Util;
import com.baseandroid.widget.RouteeFlowLayout;
import com.bumptech.glide.Glide;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.database.GreenDaoDbHelp;
import com.haodaibao.fanbeiapp.repository.json.LableInfo;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;
import com.haodaibao.fanbeiapp.repository.json.OpenCityBean;
import com.haodaibao.fanbeiapp.repository.json.PackageListBean;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 开发者：LuoYi
 * Time: 2017 16:49 2017/10/10 10
 */

public class MerchantListAdapterView {

    private Context context;

    public MerchantListAdapterView(Context context) {
        this.context = context;
    }

    public static class MerchantViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.view_separator)
        View viewSeparator;
        @BindView(R.id.imageview1)
        RoundedImageView imageview1;
        @BindView(R.id.tv_discount_type)
        TextView tvDiscountType;
        @BindView(R.id.merchant_pictures_rl)
        RelativeLayout merchantPicturesRl;
        @BindView(R.id.tv_merchantname)
        TextView tvMerchantname;
        @BindView(R.id.tv_fenlei)
        TextView tvFenlei;
        @BindView(R.id.tv_average)
        TextView tvAverage;
        @BindView(R.id.tv_shangquan)
        TextView tvShangquan;
        @BindView(R.id.tv_fan)
        TextView tvFan;
        //        @BindView(R.id.img_preferential)
//        ImageView imgPreferential;
        @BindView(R.id.tv_preferential_yellow)
        TextView tvPreferentialYellow;
        @BindView(R.id.home_discount_rv)
        RecyclerView homeDiscountRv;
        @BindView(R.id.home_package_rfl)
        RouteeFlowLayout homePackageRfl;
        @BindView(R.id.iv_bhh)
        ImageView ivBhh;

        public MerchantViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setMerchantView(MerchantViewHolder holder, MerchantInfoEntiy merchantInfo) {
        holder.tvMerchantname.setText(merchantInfo.getMerchantname());
        /* 商户图片 */
        holder.imageview1.setScaleType(ImageView.ScaleType.FIT_XY);
        String imageurl = CommonUtils.getImageUrl(merchantInfo.getImageurl());
        Glide.with(context)
                .load(imageurl)
                .asBitmap()
                .placeholder(R.drawable.image_merchant_placehold)
                .error(R.drawable.image_merchant_placehold)
                .into(holder.imageview1);

        /* 人均消费，为0则不显示 */
        String personaverage = merchantInfo.getPersonaverage();
        String string = "";
        if (TextUtils.isEmpty(personaverage) || FormatUtil.formatAmount(0, personaverage).equals("0")) {
        } else {
            string = "人均 ¥" + FormatUtil.formatAmount(0, personaverage);
            holder.tvAverage.setText(string);
        }
        String shangquan = merchantInfo.getAreaname();
        if (TextUtils.isEmpty(shangquan)) {
            shangquan = merchantInfo.getPainame();
        }

        setShangquanText(holder.tvShangquan, shangquan, merchantInfo.getLongitude(), merchantInfo.getLatitude());

		/* 商户分类 */
        String fenlei = merchantInfo.getCategoryName();
        holder.tvFenlei.setText(fenlei);

        /* 优惠活动 */
//        List<LableInfo> lableInfoList = merchantInfo.getLabels();
//        if (lableInfoList == null) {
//            holder.imgPreferential.setVisibility(View.GONE);
//        } else if (lableInfoList.size() > 0) {
//            holder.imgPreferential.setVisibility(View.VISIBLE);
//        } else {
//            holder.imgPreferential.setVisibility(View.GONE);
//        }

        /* 返现规则 */
        // 返现模式0：非返现1：全额返2：满额返3：每满额返
        String returnBeginAmount = merchantInfo.getReturnbeginamount();
        String returnRate = merchantInfo.getReturnrate();
        String refundMode = merchantInfo.getRefundmode();

        if (!TextUtils.isEmpty(refundMode)) {
            if (!TextUtils.isEmpty(returnBeginAmount) && !TextUtils.isEmpty(returnRate)) {
                if (!refundMode.equals("0")) {
                    Spanned money = FormatUtil.setFanxian(refundMode, returnBeginAmount, returnRate);
                    holder.tvFan.setVisibility(View.VISIBLE);
                    holder.tvFan.setText(money);
                } else {
                    holder.tvFan.setVisibility(View.INVISIBLE);
                }
            } else {
                holder.tvFan.setVisibility(View.INVISIBLE);
            }
        } else {
            holder.tvFan.setVisibility(View.INVISIBLE);
        }

        if (merchantInfo.getPrieds() == null || merchantInfo.getPrieds().size() == 0) {
            holder.homeDiscountRv.setVisibility(View.GONE);
        } else {
            holder.homeDiscountRv.setVisibility(View.VISIBLE);
            boolean showNewView = merchantInfo.getPackageList() == null || merchantInfo.getPackageList().size() == 0;
            holder.homeDiscountRv.setLayoutManager(new LinearLayoutManager(context));
            MerchantDiscountListAdapter discountListAdapter = new MerchantDiscountListAdapter(context, showNewView);
            holder.homeDiscountRv.setAdapter(discountListAdapter);
            discountListAdapter.resetData(merchantInfo.getPrieds());
        }

        if (merchantInfo.getPackageList() == null || merchantInfo.getPackageList().size() == 0) {
            holder.tvPreferentialYellow.setVisibility(View.GONE);
        } else {
            holder.tvPreferentialYellow.setVisibility(View.VISIBLE);
            for (PackageListBean packageBean : merchantInfo.getPackageList()) {
                String faceUserTypeCode;
                if (TextUtils.isEmpty(packageBean.getFaceUserTypeCode())) {
                    faceUserTypeCode = "";
                } else if (TextUtils.equals(packageBean.getFaceUserTypeCode(), "02")) {
                    faceUserTypeCode = "[新顾客专享]";
                } else if (TextUtils.equals(packageBean.getFaceUserTypeCode(), "03")) {
                    faceUserTypeCode = "[老顾客专享]";
                } else {
                    faceUserTypeCode = "";
                }
                holder.tvPreferentialYellow.setText(faceUserTypeCode + packageBean.getPackageName());
//                holder.tvPreferentialYellow.setText(faceUserTypeCode + packageBean.getPackageName() + packageBean.getPreferentialPrice() + "元");
            }
        }

        if (TextUtils.isEmpty(merchantInfo.getBhhenable())) {
            holder.ivBhh.setVisibility(View.GONE);
        } else {
            holder.ivBhh.setVisibility(TextUtils.equals(merchantInfo.getBhhenable(), "1") ? View.VISIBLE : View.GONE);
        }

        if (merchantInfo.getLabels() != null && merchantInfo.getLabels().size() != 0) {
            holder.homePackageRfl.setVisibility(View.VISIBLE);
            setRouteeView(holder.homePackageRfl, merchantInfo.getLabels());
        } else {
            holder.homePackageRfl.setVisibility(View.GONE);
        }
    }

    private void setShangquanText(TextView shangquanText, String shangquan, String lo, String la) {
        if (Consent.currentLocation) {
            String distance = "";
            List<OpenCityBean> list = GreenDaoDbHelp.queryCityInfoByName(Global.getSelectCity()).list();
            if (list != null && list.size() != 0) {
                LatLng startLatLng = new LatLng(Global.getMyLocation().getLatitude(), Global.getMyLocation().getLongitude());
                LatLng endLatLng = new LatLng(Double.parseDouble(la), Double.parseDouble(lo));
                int distance1 = (int) Util.getDistance(startLatLng, endLatLng);
                distance = FormatUtil.formatDistance(distance1 + "");
            }
            shangquanText.setText(shangquan + " • " + distance);
        } else {
            shangquanText.setText(shangquan);
        }
    }

    private void setRouteeView(RouteeFlowLayout itemPackageRfl, List<LableInfo> lableList) {
        itemPackageRfl.removeAllViews();
        int labelTag = 1;
        for (LableInfo lableInfo : lableList) {
            labelTag++;
            TextView itemNewPackge = (TextView) LayoutInflater.from(context).inflate(R.layout.item_label_package, null);
            TextView labelTv = itemNewPackge.findViewById(R.id.item_label_tv);
            labelTv.setText(lableInfo.getLabelname());
            if (labelTag % 2 == 0) {
                labelTv.setBackgroundResource(R.drawable.label_red_btn_icon);
            } else {
                labelTv.setBackgroundResource(R.drawable.label_yellow_btn_icon);
            }
            itemPackageRfl.addView(itemNewPackge);
        }
    }

}
