package com.haodaibao.fanbeiapp.module.personal.help;

import android.text.TextUtils;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.RxUtils;
import com.haodaibao.fanbeiapp.repository.LifeRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.HelpBean;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Routee on 2017/9/26.
 * description: ${cusor}
 */

public class HelpPresenter implements HelpContract.Presenter {

    private final HelpContract.View mView;

    public HelpPresenter(HelpContract.View view) {
        mView = view;
    }

    @Override
    public void getHelpList(String uid) {
        Map<String, String> params = new HashMap();
        if (uid == null) {
            uid = "";
        }
        params.put("uid", uid);
        params.put("level", "0");
        Observable<Data<HelpBean>> helpObservable;
        if (TextUtils.isEmpty(uid)) {
            helpObservable = LifeRepository.getInstance().getHelpList(params);
        } else {
            helpObservable = LifeRepository.getInstance().getBhhHelpList(params);
        }
        helpObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<HelpBean>>applySchedulersLifeCycle((BaseActivity) mView))
                .subscribe(new RxObserver<Data<HelpBean>>() {
                    @Override
                    public void onNext(@NonNull Data<HelpBean> data) {
                        if (checkJsonCode(data, false)) {
                            mView.setUpHelpListData(data.getResult());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        mView.setEmptyView();
                    }
                });
    }
}
