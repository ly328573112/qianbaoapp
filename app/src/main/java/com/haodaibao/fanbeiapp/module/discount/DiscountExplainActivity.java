package com.haodaibao.fanbeiapp.module.discount;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.SpannableStringUtils;
import com.baseandroid.utils.StatusBarHelper;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.ActivityDetailBean;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import butterknife.BindView;

/**
 * 开发者：LuoYi
 * Time: 2017 17:27 2017/12/12 12
 */

public class DiscountExplainActivity extends BaseActivity implements View.OnClickListener, DiscountExplainContract.DiscountView {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.discount_no_enjoy_tv)
    TextView discountNoEnjoyTv;
    @BindView(R.id.discount_icon_iv)
    ImageView discountIconIv;
    @BindView(R.id.discount_enjoy_iv)
    ImageView discountEnjoyIv;
    @BindView(R.id.discount_number_tv)
    TextView discountNumberTv;
    @BindView(R.id.discount_amount_money_tv)
    TextView discountAmountMoneyTv;
    @BindView(R.id.discount_all_user_tv)
    TextView discountAllUserTv;
    @BindView(R.id.discount_use_time_tv)
    TextView discountUseTimeTv;
    @BindView(R.id.discount_available_date_tv)
    TextView discountAvailableDateTv;
    @BindView(R.id.discount_use_rules_tv)
    TextView discountUseRulesTv;
    @BindView(R.id.discount_activity_time_tv)
    TextView discountActivityTimeTv;
    private DiscountExplainPresenter explainPresenter;

    public static void start(Activity activity, String merchantno, String activityno) {
//        if (TextUtils.isEmpty(activityno)) {
//            return;
//        }
        Bundle bundle = new Bundle();
        bundle.putString("merchantno", merchantno);
        bundle.putString("activityno", activityno);
        ActivityJumpUtil.next(activity, DiscountExplainActivity.class, bundle, 0);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_discount_explain;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(this);
        toolbarBack.setOnClickListener(this);
        toolbarTitle.setText("优惠说明");
        discountNoEnjoyTv.setBackgroundColor(getResources().getColor(R.color.c_F62241));

        explainPresenter = new DiscountExplainPresenter(this);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        Intent intent = getIntent();
        String merchantno = intent.getStringExtra("merchantno");
        String activityno = intent.getStringExtra("activityno");
        explainPresenter.getActivityDetail(merchantno, activityno);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            default:

                break;
        }
    }

    @Override
    public void onDiscountData(ActivityDetailBean.ActivityBean activityBean, ActivityDetailBean.ActivityAuthBean authBean) {
        if (activityBean == null || authBean == null || TextUtils.isEmpty(authBean.activityStatus)) {
            return;
        }
        if (authBean.activityStatus.equals("00000000")) {// 可享
            discountNoEnjoyTv.setVisibility(View.GONE);
            discountIconIv.setVisibility(View.VISIBLE);
            discountEnjoyIv.setVisibility(View.VISIBLE);
        } else {//不可享
            discountNoEnjoyTv.setVisibility(View.VISIBLE);
            discountNoEnjoyTv.setText(authBean.activityMessage);
            discountIconIv.setVisibility(View.GONE);
            discountEnjoyIv.setVisibility(View.INVISIBLE);
        }

        discountNumberTv.setText(SpannableStringUtils.getBuilder(activityBean.discountTitle + "  ")
                .setForegroundColor(getResources().getColor(R.color.c_F62241))
                .append(activityBean.topAmountTitle)
                .setForegroundColor(getResources().getColor(R.color.c_3c3c3c))
                .create());
        discountAmountMoneyTv.setText(activityBean.topAmountTitle);
        discountAllUserTv.setText(activityBean.userTypeName);
        discountUseTimeTv.setText(activityBean.useTime);
        discountAvailableDateTv.setText(activityBean.notUseName);
        discountUseRulesTv.setText(activityBean.discountContent);
        discountActivityTimeTv.setText(activityBean.activityTime);
    }

    @Override
    public void onDiscoutError() {

    }
}
