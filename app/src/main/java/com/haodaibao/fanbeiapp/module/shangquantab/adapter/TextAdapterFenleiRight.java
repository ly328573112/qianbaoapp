package com.haodaibao.fanbeiapp.module.shangquantab.adapter;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.ShanghufenleiInfo;

import java.util.List;

public class TextAdapterFenleiRight extends ArrayAdapter<ShanghufenleiInfo> {

    private Context mContext;
    private List<ShanghufenleiInfo> mListData;
    private ShanghufenleiInfo[] mArrayData;
    private int selectedPos = -1;

    private int selectedTextColor = -1;
    private float textSize;
    private OnItemClickListener mOnItemClickListener;
    private int tLeft;

    private String selectedText = "";
    private String parentno = "";

    public TextAdapterFenleiRight(Context context, List<ShanghufenleiInfo> listData, int sIdText, int sId, int nId) {
        super(context, R.string.no_data, listData);
        mContext = context;
        mListData = listData;
        selectedTextColor = mContext.getResources().getColor(sIdText);

    }


    public TextAdapterFenleiRight(Context context, ShanghufenleiInfo[] arrayData, int sIdText, int sId, int nId) {
        super(context, R.string.no_data, arrayData);
        mContext = context;
        mArrayData = arrayData;
        selectedTextColor = mContext.getResources().getColor(sIdText);
    }

    public void refresh(List<ShanghufenleiInfo> mListData) {
        this.mListData = mListData;
        notifyDataSetChanged();
    }

    /**
     * 设置选中的position,并通知列表刷新
     */
    public void setSelectedPosition(int pos) {
        if (mListData != null && pos < mListData.size()) {
            selectedPos = pos;
            ShanghufenleiInfo info = mListData.get(pos);
            selectedText = info.getName();
            parentno = info.getParentno();
            notifyDataSetChanged();
        } else if (mArrayData != null && pos < mArrayData.length) {
            selectedPos = pos;
            ShanghufenleiInfo arrayDatum = mArrayData[pos];
            selectedText = arrayDatum.getName();
            parentno = arrayDatum.getParentno();
            notifyDataSetChanged();
        }

    }

    /**
     * 设置选中的position,但不通知刷新
     */
    public void setSelectedPositionNoNotify(int pos) {
        selectedPos = pos;
        if (mListData != null && pos < mListData.size()) {
            selectedText = mListData.get(pos).getName();
        } else if (mArrayData != null && pos < mArrayData.length) {
            selectedText = mArrayData[pos].getName();
        }
    }

    /**
     * 获取选中的position
     */
    public int getSelectedPosition() {
        if (mArrayData != null && selectedPos < mArrayData.length) {
            return selectedPos;
        }
        if (mListData != null && selectedPos < mListData.size()) {
            return selectedPos;
        }

        return -1;
    }

    public void cleanRightData() {
        selectedText = "";
        parentno = "";
    }

    /**
     * 设置列表字体大小
     */
    public void setTextSize(float tSize) {
        textSize = tSize;
    }

    /**
     * 设置列表文字距离
     */
    public void setTextPaddingLeft(int left) {
        tLeft = left;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        MyViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.choose_item1, parent, false);
            holder = new MyViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (MyViewHolder) convertView.getTag();
        }
        holder.view.setTag(position);
        String mString = "";
        String parentStr = "";
        if (mListData != null) {
            if (position < mListData.size()) {
                mString = mListData.get(position).getName();
                parentStr = mListData.get(position).getParentno();
            }
        } else if (mArrayData != null) {
            if (position < mArrayData.length) {
                mString = mArrayData[position].getName();
                parentStr = mArrayData[position].getParentno();
            }
        }
        if (mString.contains("全部")) {
            holder.view.setText("全部");
        } else {
            holder.view.setText(mString);
        }
        holder.view.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.rightIcon.getLayoutParams();
        params.width = getTextViewWidth(holder.view, mString);
        holder.rightIcon.setLayoutParams(params);

        if (!TextUtils.isEmpty(selectedText) && selectedText.equals(mString)) {
            if (!TextUtils.isEmpty(parentno) && parentStr.equals(parentno)) {
                holder.rightIcon.setVisibility(View.INVISIBLE);
                holder.view.setTextColor(selectedTextColor);// 设置选中的字体颜色
            } else {
                holder.rightIcon.setVisibility(View.INVISIBLE);
                holder.view.setTextColor(mContext.getResources().getColor(R.color.text_gray3));
            }
        } else {
            holder.rightIcon.setVisibility(View.INVISIBLE);
            holder.view.setTextColor(mContext.getResources().getColor(R.color.text_gray3));
        }
        convertView.setOnClickListener(new AdapterClick(position));
        return convertView;
    }

    class MyViewHolder {
        TextView view;
        ImageView rightIcon;

        MyViewHolder(View rootView) {
            view = (TextView) rootView.findViewById(R.id.content_text);
            rightIcon = (ImageView) rootView.findViewById(R.id.right_icon);
        }
    }

    private int getTextViewWidth(TextView textView, String text) {
        Rect rect = new Rect();
        textView.getPaint().getTextBounds(text, 0, text.length(), rect);
        int width = rect.width();
        return width;
    }

    public void setOnItemClickListener(OnItemClickListener l) {
        mOnItemClickListener = l;
    }

    /**
     * 重新定义菜单选项单击接口
     */
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    class AdapterClick implements View.OnClickListener {

        int position;

        AdapterClick(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            selectedPos = position;
            setSelectedPosition(selectedPos);
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(view, selectedPos);
            }
        }
    }

}
