package com.haodaibao.fanbeiapp.module.map;


import com.amap.api.maps.model.LatLng;

public class MapInfo {
    private String mapname;
    private String name;
    private String content;
    private LatLng latLng1;

    public String getName() {
        return name;
    }

    public String getContent() {
        return content;
    }

    public LatLng getLatLng1() {
        return latLng1;
    }

    public String getMapname() {
        return mapname;
    }

    public MapInfo() {
        super();
    }

    public MapInfo(String mapname, String name, String content, LatLng latLng1) {
        super();
        this.mapname = mapname;
        this.name = name;
        this.content = content;
        this.latLng1 = latLng1;
    }

}