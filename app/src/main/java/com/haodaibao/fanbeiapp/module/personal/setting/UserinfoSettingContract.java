package com.haodaibao.fanbeiapp.module.personal.setting;

import com.haodaibao.fanbeiapp.repository.json.UserDate;

import java.util.HashMap;

/**
 * Created by Routee on 2017/9/20.
 * description: ${cusor}
 */

public interface UserinfoSettingContract {
    interface View {
        void setupUserinfo(UserDate bean);

        void uploadHeadViewSuccess();

        void checkOutSuccess();
    }

    interface Presenter {
        void checkOut();

        void uploadHeadView(HashMap<String, String> params, String photoUri);
    }
}
