package com.haodaibao.fanbeiapp.module.personal.coupon;

import android.os.Bundle;

import com.baseandroid.base.BaseFragment;


/**
 * create by 曹伟 on 2018/05/24
 * pro : 优惠券历史记录页面
 * e-mail : caowei@qianbaocard.com
 */

public class CouponHistoryFragment extends BaseFragment {
    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    protected void setupView() {

    }

    @Override
    protected void setupData(Bundle savedInstanceState) {

    }
}
