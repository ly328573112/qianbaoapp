package com.haodaibao.fanbeiapp.module.maphome;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * date: 2017/11/16.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class MyBehavior extends CoordinatorLayout.Behavior<View> {
    public MyBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        String tag = (String) dependency.getTag();
        return TextUtils.equals(tag, "hp_parent");
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency) {
        int offset = dependency.getTop();
        Log.e("hpo", "onDependentViewChanged: "+dependency.getTop());
        child.offsetTopAndBottom(offset);
//        ViewCompat.offsetTopAndBottom(child, -offset);
        return true;
    }
}
