package com.haodaibao.fanbeiapp.module.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.base.BaseFragment;
import com.baseandroid.config.Global;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.DeviceUuid;
import com.baseandroid.widget.CustomBanner;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.baseandroid.widget.recycleadpter.CustomPtrHeader;
import com.google.zxing.integration.android.IntentIntegrator;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.database.GreenDaoDbHelp;
import com.haodaibao.fanbeiapp.event.LocationSwitchEvent;
import com.haodaibao.fanbeiapp.module.home.adapter.ClassificationOfShopAdapter;
import com.haodaibao.fanbeiapp.module.login.LoginActivity;
import com.haodaibao.fanbeiapp.module.merchant.MerchantDetailActivity;
import com.haodaibao.fanbeiapp.module.merchant.temporarydata.MerchantTemporary;
import com.haodaibao.fanbeiapp.module.pay.PayActivity;
import com.haodaibao.fanbeiapp.module.pay.PayComboActivity;
import com.haodaibao.fanbeiapp.module.personal.coupon.CouponActivity;
import com.haodaibao.fanbeiapp.module.search.CityListActivity;
import com.haodaibao.fanbeiapp.module.search.view.SpaceItemDecortion;
import com.haodaibao.fanbeiapp.module.shangquantab.NearbyTabView;
import com.haodaibao.fanbeiapp.module.zxing.ZXingScanActivity;
import com.haodaibao.fanbeiapp.repository.json.AdInfo;
import com.haodaibao.fanbeiapp.repository.json.LableInfo;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoResp;
import com.haodaibao.fanbeiapp.repository.json.NearbyRequest;
import com.haodaibao.fanbeiapp.repository.json.PackageAllBean;
import com.haodaibao.fanbeiapp.repository.json.PackageListBean;
import com.haodaibao.fanbeiapp.repository.json.ShopKindListBean;
import com.haodaibao.fanbeiapp.repository.json.YaoheListBean;
import com.haodaibao.fanbeiapp.webview.WebviewTbsActivity;
import com.qianbao.shiningwhitelibrary.BHHManager;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;
import com.tencent.stat.StatService;
import com.youth.banner.listener.OnBannerListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;

import static com.baseandroid.config.Global.getSelectCity;
import static com.haodaibao.fanbeiapp.module.pay.PayActivity.MERCHANT_DETAIL;

/**
 * 开发者：LuoYi
 * Time: 2018 15:14 2018/3/8 03
 */

public class HomeNewFragment extends BaseFragment implements HomeContract.HomeNewView
        , BaseRecycleViewAdapter.OnItemClickListener
        , HomeContract.OnBaseClickListener
        , View.OnClickListener
        , OnItemClickShopListener {

    public static final String AD1 = "20180414119141";// 1号广告位，滑动viewpager广告栏//20180414119141
    public static final String AD2 = "20180419119165";// 2号广告位，滑动viewpager广告栏//20180419119165

    @BindView(R.id.home_rl)
    RelativeLayout homeRl;
    @BindView(R.id.home_banner_id)
    CustomBanner homeBannerId;
    @BindView(R.id.home_saoma_rl)
    RelativeLayout homeSaomaRl;
    @BindView(R.id.home_bhh_rl)
    RelativeLayout homeBhhRl;
    @BindView(R.id.home_location_rl)
    RelativeLayout homeLocationRl;
    @BindView(R.id.home_location_tv)
    TextView homeLocationTv;
    @BindView(R.id.home_refresh_iv)
    ImageView homeRefreshIv;
    @BindView(R.id.home_merchant_rv)
    RecyclerView homeMerchantRv;
    @BindView(R.id.home_lookup_tv)
    TextView homeLookupTv;
    @BindView(R.id.home_ptrfl_fresh)
    PtrClassicFrameLayout homePtrflFresh;
    @BindView(R.id.home_banner_riv)
    ImageView homeBannerRiv;
    @BindView(R.id.home_lll)
    LinearLayout home_lll;
    @BindView(R.id.shop_city_rl)
    RecyclerView shop_city_rl ; //分类商铺列表

    private ShopKindListBean shopKindsBeans ;
    private GridLayoutManager mGridlayoutManager ;

    private HomeNewAdapter newAdapter;
    private HomePresenter homePresenter;
    private NearbyTabView homeNearbyView;
    private Animation operatingAnim;
    private List<MerchantTemporary> temporaries;
    private ClassificationOfShopAdapter classificationOfShopAdapter;    //首页分类适配器

    private HomeContract.HomeClickListener homeClickListener;
    private GlideImageLoader imageLoader;

    public void onHomeClickListener(HomeContract.HomeClickListener homeClickListener) {
        this.homeClickListener = homeClickListener;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.home_new_fragment_layout;
    }

    @Override
    protected void setupView() {
        initHomeBanner();
        imageLoader = new GlideImageLoader();
        temporaries = new ArrayList<>();
        setPtrFreshView();
        homeNearbyView = new NearbyTabView(getActivity(), this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        homeMerchantRv.setLayoutManager(linearLayoutManager);
        newAdapter = new HomeNewAdapter(getActivity(), this, this);
        homeMerchantRv.setAdapter(newAdapter);
        setPreloadingData();
        homeMerchantRv.setNestedScrollingEnabled(false);
        homeMerchantRv.setFocusable(false);

        homeBannerId.setOnClickListener(this);
        homeSaomaRl.setOnClickListener(this);
        homeBhhRl.setOnClickListener(this);
        homeLocationRl.setOnClickListener(this);
        homeLookupTv.setOnClickListener(this);

        mGridlayoutManager = new GridLayoutManager(mContext,2);
        mGridlayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        shop_city_rl.setLayoutManager(mGridlayoutManager);
        shop_city_rl.addItemDecoration(new SpaceItemDecortion(10));

        initLocationAnimation();
    }


    private void initHomeBanner() {
        homeBannerId.setPageTransformer(false, new CustomBanner.ScaleTransformer());
        homeBannerId.setImageLoader(new GlideImageLoader(5));
        List<Integer> arrayList = new ArrayList<>();
        arrayList.add(R.drawable.home_default_backgroud);
        homeBannerId.setImages(arrayList);
        homeBannerId.setOffscreenPageLimit(10);
        homeBannerId.start();
    }

    private void setPreloadingData() {
        if (temporaries != null) {
            temporaries.clear();
        }
        for (int i = 0; i < 2; i++) {
            temporaries.add(new MerchantTemporary() {
                @Override
                public int getmType() {
                    return 4;
                }
            });
        }
        newAdapter.resetData(temporaries);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        homePresenter = new HomePresenter(this);
        homePresenter.getOpenCitys();
    }

    @Override
    public <T> void onBaseClick(MerchantInfoEntiy merchantInfoEntiy, T t) {
        if (CommonUtils.isFastDoubleClick()) {
            return;
        }
        if (t == null) {
        } else if (t instanceof PackageListBean) {
            PackageListBean packageListBean = (PackageListBean) t;
            if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                ActivityJumpUtil.next(getActivity(), LoginActivity.class, new Bundle(), 0);
            } else {
                homePresenter.getComboDetail(merchantInfoEntiy, packageListBean.getPackageNo());
            }
        }
    }

    @Override
    public <T> void onItemClick(View view, T t) {
        if (t == null) {
        } else if (t instanceof MerchantInfoEntiy) {
            MerchantInfoEntiy infoEntiy = (MerchantInfoEntiy) t;
            if (view instanceof RelativeLayout) {
                Bundle bundle = new Bundle();
                bundle.putString("merchantno", infoEntiy.getMerchantno());
                ActivityJumpUtil.next(getActivity(), MerchantDetailActivity.class, bundle, 0);
            } else if (view instanceof TextView) {
                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                    ActivityJumpUtil.next(getActivity(), LoginActivity.class, new Bundle(), 0);
                } else {
                    PayActivity.openForResult(getActivity(), infoEntiy, infoEntiy.getMerchantno(), MERCHANT_DETAIL, false);
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.home_banner_id:
                break;
            case R.id.home_saoma_rl:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                    startActivity(new Intent(getContext(), LoginActivity.class));
                } else {
                    // ZXing Android Embedded支持Fragment
//                    IntentIntegrator intentIntegrator = IntentIntegrator.forSupportFragment(this);
                    IntentIntegrator intentIntegrator = new IntentIntegrator(getActivity());
                    // ZXing Android Embedded库提供的扫码Activity默认是横屏的,需要自定义
                    intentIntegrator.setCaptureActivity(ZXingScanActivity.class);
                    //该方法用于设置“被扫描的二维码图片”可以保存在本地,intent返回
                    //intentIntegrator.setBarcodeImageEnabled(false);
                    //该方法传0将会使用后置摄像头，传1将会使用前置摄像头
                    //intentIntegrator.setCameraId(0);
                    //如果设置可以使用QR_CODE_TYPES和ALL_CODE_TYPES
                    intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                    //功能是用来调整扫描界面方向的
                    intentIntegrator.setOrientationLocked(false);
                    //设置扫描界面的提示信息
                    //intentIntegrator.setPrompt("这里是二维码扫描界面");
                    intentIntegrator.initiateScan();
                }
                StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_scan", null);
                break;
            case R.id.home_bhh_rl:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    intent.putExtra("Bhh", "Bhh");
                    startActivity(intent);
                } else {
                    if (TextUtils.isEmpty(Global.getUserInfo().bhhAcessToken)) {
                    } else {
                        BHHManager.openViewForRelease(getActivity(), Global.getUserInfo().bhhAcessToken, Global.getUserInfo().getUserno(), false);
                    }
                }
                break;
            case R.id.home_location_rl:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                locationPositioning();
                break;
            case R.id.home_lookup_tv:
                if (CommonUtils.isFastDoubleClick() || homeClickListener == null) {
                    break;
                } else {
                    homeClickListener.onHomeClick();
                }
                break;
            default:
                break;
        }
    }

    private void locationPositioning() {
        homeLocationTv.setText("定位中...");
        homeRefreshIv.setAnimation(operatingAnim);
        homeLocationRl.setEnabled(false);
        if (operatingAnim != null) {
            operatingAnim.start();
        }
        homeNearbyView.onNearbyLocation("homeC");
    }

    private void initLocationAnimation() {
        operatingAnim = AnimationUtils.loadAnimation(mContext, R.anim.anim_location_home);
        LinearInterpolator lin = new LinearInterpolator();
        operatingAnim.setInterpolator(lin);
    }

    private void setPtrFreshView() {
        CustomPtrHeader customPtrHeader = new CustomPtrHeader(getActivity());
        homePtrflFresh.setHeaderView(customPtrHeader);
        homePtrflFresh.addPtrUIHandler(customPtrHeader);
        homePtrflFresh.disableWhenHorizontalMove(true);
        homePtrflFresh.setPtrHandler(ptrDefaultHandler);
    }

    PtrDefaultHandler ptrDefaultHandler = new PtrDefaultHandler() {
        @Override
        public void onRefreshBegin(final PtrFrameLayout frame) {
            //            List<OpenCityInfoEntity> openCityInfo = GreenDaoDbHelp.queryCityInfoByName(Global.getMyLocation().getCityName()).list();
            //            if (openCityInfo == null || openCityInfo.size() == 0) {
            //                return;
            //            }
            locationPositioning();
            //            homePresenter.requestHomeInfo(openCityInfo.get(0).getCode(), AD2, 1, 2, false);
        }

        @Override
        public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
            return PtrDefaultHandler.checkContentCanBePulledDown(frame, content, header);
        }
    };

    private void requestHomeData() {
        homePresenter.getClassification(Global.getSelectCitySiteType(), Global.getSelectCityCode(), Global.getSelectCityCode(), "");
        homePresenter.getShangquan(Global.getSelectCitySiteType(), Global.getSelectCityCode(), Global.getSelectCityCode());
        homePresenter.requestHomeInfo(Global.getSelectCitySiteType(), Global.getSelectCityCode(), Global.getSelectCityCode(), AD1, AD2, 1, 2, false);
    }

    @Override
    public void onAdListInfo(final List<AdInfo> adInfoList) {
        List<String> images = new ArrayList<>();
        for (AdInfo adInfo : adInfoList) {
            images.add(CommonUtils.getImageUrl(adInfo.getImageurl()));
        }
        homeBannerId.update(images);
//        homeBannerId.setImages(images);
        homeBannerId.setOnBannerListener(new OnBannerListener() {
            @Override
            public void OnBannerClick(int position) {
                AdInfo adInfo = adInfoList.get(position);
                String linkType = adInfo.getLinktype();
                String title = adInfo.getContentname();
                String content = adInfo.getContent();
                if (TextUtils.isEmpty(content)) {
                    return;
                }
                switch (linkType) {
                    case "04": //04：指定链接地址
                        WebviewTbsActivity.start(getActivity(), title, content);
//                        Intent intent = new Intent(getActivity(), WebviewActivity.class);
//                        intent.putExtra(WEBVIEW_URL, content);
//                        intent.putExtra(WEBVIEW_TITLE, title);
//                        startActivity(intent);
                        break;
                    case "06"://06:APP原生页面
                        if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                            Intent intent1 = new Intent(getContext(), LoginActivity.class);
                            intent1.putExtra("Bhh", "Bhh");
                            startActivity(intent1);
                        } else {
                            String urlResult = content.substring(content.lastIndexOf("/") + 1);
                            urlResult = urlResult.replace(".html", "");
                            /*---content包含bhh_index跳转到白花花首页,包含voucher_list跳转至优惠券列表页,需要修改---*/
                            if (TextUtils.equals(urlResult, "bhh_index")) {
                                if (TextUtils.isEmpty(Global.getUserInfo().bhhAcessToken)) {
                                } else {
                                    BHHManager.openViewForRelease(getActivity(), Global.getUserInfo().bhhAcessToken, Global.getUserInfo().getUserno(), false);
                                }
                            } else if (TextUtils.equals(urlResult, "voucher_list")) {
                                ActivityJumpUtil.next(getActivity(), CouponActivity.class);
                            }
                        }
                        break;
                    case "99"://99：无链接地址
                        break;
                    default:
                        break;
                }
                StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_banner", null);
            }
        });
    }

    @Override
    public void onAdListInfo2(final List<AdInfo> adInfoList) {
        if (adInfoList.size() > 0) {
            home_lll.setVisibility(View.VISIBLE);
//            homeBannerRiv.setCornerRadius(DisplayLess.$dp2px(5));
            imageLoader.setImagePath(getContext(), CommonUtils.getImageUrl(adInfoList.get(0).getImageurl()), homeBannerRiv);
            homeBannerRiv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    WebviewTbsActivity.start(getContext(), adInfoList.get(0).getContentname(), adInfoList.get(0).getContent());
//                    Intent intent = new Intent(getContext(), WebviewTbsActivity.class);
//                    intent.putExtra(WEBVIEW_URL, adInfoList.get(0).getContent());
//                    intent.putExtra(WEBVIEW_TITLE, adInfoList.get(0).getContentname());
//                    startActivity(intent);
                }
            });
        } else {
            home_lll.setVisibility(View.GONE);
        }
    }

    @Override
    public void onYaoheListInfo(List<YaoheListBean.YaoheBean> yaoheBeanList) {

    }

    @Override
    public void onShanghuListInfo(MerchantInfoResp merchantInfoResp) {
        if (merchantInfoResp == null || merchantInfoResp.getMerchantList().size() == 0) {
            return;
        }
        newAdapter.resetData(merchantInfoResp.getMerchantList());
    }

    @Override
    public void onShanghuListInfoError() {
        homeMerchantRv.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    newAdapter.loadMoreFail();
                }
            }
        }, 200);
    }

    @Override
    public void onHomeInfoRefresh() {
        newAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateCityShanghuList() {
        if (TextUtils.isEmpty(getSelectCity())) {
            return;
        }

        if (!TextUtils.isEmpty(Global.getMyLocation()
                .getAdCode())
                && GreenDaoDbHelp.loadallCityInfo().size() > 0
                && GreenDaoDbHelp.loadallDistrictsInfo().size() > 0
                && GreenDaoDbHelp.queryCityByGaodeNameAdCode().list().size() <= 0) {
            startActivity(new Intent(getActivity(), CityListActivity.class));
        }

        requestHomeData();
    }

    @Override
    public void onRetry() {
    }

    @Override
    public void onContent() {
        homePtrflFresh.refreshComplete();
    }

    @Override
    public void onNearbySelect(NearbyRequest nearbyRequest) {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(LocationSwitchEvent event) {
        if (event.locationType.equals("allC") || event.locationType.equals("homeC")) {
            onStopLocation();
        }
    }

    @Override
    public void onStopLocation() {
        homeLocationRl.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (homeRefreshIv == null || homeLocationRl == null || homeLocationTv == null) {
                    return;
                }
                if (operatingAnim != null && operatingAnim.isInitialized()) {
                    operatingAnim.cancel();
                }
                homeRefreshIv.clearAnimation();
                homeLocationRl.setEnabled(true);
                if (!Global.getMyLocation().isLocationSuccess()) {
                    homeLocationTv.setText(Html.fromHtml("<font color=\"#ff0000\">定位失败，点击重试</font>"));
                } else {
                    String address = "你当前在：" + Global.getMyLocation().getAddress();
                    homeLocationTv.setText(address);
                        homePresenter.getShopKind(Global.getSelectCitySiteType()
                                ,Global.getSelectCityCode()
                                ,Global.getMyLocation().getLatitude()+""
                                ,Global.getMyLocation().getLongitude()+""
                                ,Global.getHistoryLocation().getLatitude()+""
                                ,Global.getHistoryLocation().getLongitude()+""
                                , DeviceUuid.getDeviceId(Global.getContext()));
                }

                requestHomeData();
            }
        }, 1000);
    }

    @Override
    public void onShangquan() {

    }

    @Override
    public void onComboDetail(MerchantInfoEntiy merchantInfoEntiy, PackageAllBean packageAllBean) {
        PayComboActivity.openForResult((BaseActivity) getActivity(), merchantInfoEntiy.getMerchantno(), merchantInfoEntiy, packageAllBean.packageDetail);
    }

    @Override
    public void onLebelList(List<LableInfo> lableInfoList) {

    }

    /**
     * 商铺分类获取
     * @param result
     */
    @Override
    public void onShopKindsRefresh(ShopKindListBean result) {
        if(result != null && result.getList() != null && result.getList().size() > 0){
            shopKindsBeans = result ;
            if(classificationOfShopAdapter != null){
                classificationOfShopAdapter.setShopKindsBeans(result.getList());
                classificationOfShopAdapter.notifyDataSetChanged();
            }else{
                classificationOfShopAdapter = new ClassificationOfShopAdapter(mContext,shopKindsBeans.getList(),this);
                shop_city_rl.setAdapter(classificationOfShopAdapter);
            }
            classificationOfShopAdapter.setItemClickListener(this);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onItemClick(int position) {
        EventBus.getDefault().post("shopSelect");
        EventBus.getDefault().post(shopKindsBeans.getList().get(position));
    }
}
