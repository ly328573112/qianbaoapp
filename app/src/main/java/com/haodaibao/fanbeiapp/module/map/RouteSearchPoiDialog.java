package com.haodaibao.fanbeiapp.module.map;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

import com.amap.api.services.core.PoiItem;
import com.haodaibao.fanbeiapp.R;

import java.util.List;

public class RouteSearchPoiDialog extends Dialog implements OnItemClickListener, OnItemSelectedListener {

    private List<PoiItem> poiItems;
    private RouteSearchAdapter adapter;
    private OnListItemClick mOnClickListener;

    RouteSearchPoiDialog(Context context) {
        this(context, android.R.style.Theme_Dialog);
    }

    RouteSearchPoiDialog(Context context, int theme) {
        super(context, theme);
    }

    RouteSearchPoiDialog(Context context, List<PoiItem> poiItems) {
        this(context, android.R.style.Theme_Dialog);
        this.poiItems = poiItems;
        adapter = new RouteSearchAdapter(context);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.routesearch_list_poi);
        RecyclerView listView = (RecyclerView) findViewById(R.id.rv_nav_search_list_poi);
        listView.setLayoutManager(new LinearLayoutManager(getContext()));
        listView.setAdapter(adapter);
        adapter.addData(poiItems);
        adapter.setRouteItemClicklistener(new RouteSearchAdapter.RouteItemClick() {

            @Override
            public void onItemClick(PoiItem item) {
                dismiss();
                mOnClickListener.onListItemClick(RouteSearchPoiDialog.this, item);
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> view, View view1, int arg2, long arg3) {
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }

    public interface OnListItemClick {
        void onListItemClick(RouteSearchPoiDialog dialog, PoiItem item);
    }

    public void setOnListClickListener(OnListItemClick l) {
        mOnClickListener = l;
    }
}
