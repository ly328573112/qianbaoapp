package com.haodaibao.fanbeiapp.module.suggest;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.baseandroid.widget.recycleadpter.GridDivider;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.CommentImageAdapter;
import com.haodaibao.fanbeiapp.repository.json.FeedbackAttachmentFiles;
import com.haodaibao.fanbeiapp.repository.json.SuggestInfo;
import com.qianbao.shiningwhitelibrary.utils.UiUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * date: 2017/9/26.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class SuggestAdapter extends BaseRecycleViewAdapter<SuggestInfo> {
    public SuggestAdapter(Context context) {
        super(context);
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, SuggestInfo item) {
        SuggestHolder suggestHolder = (SuggestHolder) holder;
        /* 用户反馈信息 */
        String info = item.getContent();
        /* 用户反馈时间 */
        String date = item.getCreateTime();
        String date1 = date.substring(0, 4)// 年
                + "." + date.substring(4, 6)// 月
                + "." + date.substring(6, 8);// 日
        /* 客服回复信息 */
        String reply = item.getMsgContent();

        suggestHolder.tv_info.setText(info);
        suggestHolder.tv_date.setText(date1);
        if (TextUtils.isEmpty(reply)) {
            suggestHolder.tv_reply.setText("");
        } else {
            suggestHolder.tv_reply.setText("[客服反馈]:" + reply);
        }
        /* 加载反馈图片 */
        List<FeedbackAttachmentFiles> list = item.getAttFlList();
        ArrayList<String> imglist = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            imglist.add(list.get(i).getFileNm());
        }
        suggestHolder.ll_imgs.setAdapter(new CommentImageAdapter(imglist, mContext, null));
        if (imglist.size() == 0) {
            suggestHolder.ll_imgs.setVisibility(View.GONE);
        } else {
            suggestHolder.ll_imgs.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new SuggestHolder(LayoutInflater.from(mContext).inflate(R.layout.adapter_lv_suggest, parent, false));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class SuggestHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_date)
        TextView tv_date;
        @BindView(R.id.tv_reply)
        TextView tv_reply;
        @BindView(R.id.tv_info)
        TextView tv_info;
        @BindView(R.id.evaluate_gridview)
        RecyclerView ll_imgs;

        public SuggestHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 3);
            ll_imgs.setLayoutManager(gridLayoutManager);
            ll_imgs.addItemDecoration(new GridDivider(mContext, UiUtils.dp2px(mContext, 10), ContextCompat.getColor(mContext, R.color.bg_gray1)));
        }
    }
}
