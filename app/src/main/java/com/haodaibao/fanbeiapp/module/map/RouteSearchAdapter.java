package com.haodaibao.fanbeiapp.module.map;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amap.api.services.core.PoiItem;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RouteSearchAdapter extends BaseRecycleViewAdapter<PoiItem> {

    private RouteItemClick routeItemClick;

    RouteSearchAdapter(Context context) {
        super(context);
    }

    void setRouteItemClicklistener(RouteItemClick routeItemClick) {
        this.routeItemClick = routeItemClick;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, final PoiItem item) {
        RouteHolder routeHolder = (RouteHolder) holder;
        routeHolder.poiName.setText(item.getTitle());
        String address = null;
        if (item.getSnippet() != null) {
            address = item.getSnippet();
        } else {
            address = "中国";
        }
        routeHolder.poiAddress.setText("地址:" + address);
        routeHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                routeItemClick.onItemClick(item);
            }
        });
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new RouteHolder(View.inflate(mContext, R.layout.route_poi_result_item, null));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class RouteHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.poiName)
        TextView poiName;
        @BindView(R.id.poiAddress)
        TextView poiAddress;

        RouteHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    interface RouteItemClick {

        void onItemClick(PoiItem item);

    }
}
