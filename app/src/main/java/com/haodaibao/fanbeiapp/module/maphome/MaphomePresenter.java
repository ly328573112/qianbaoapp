package com.haodaibao.fanbeiapp.module.maphome;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.LocationSource;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MyLocationStyle;
import com.amap.api.services.core.LatLonPoint;
import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Constant;
import com.baseandroid.config.Global;
import com.baseandroid.gaode.GaoDeMap;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.FormatUtil;
import com.baseandroid.utils.LogUtil;
import com.baseandroid.utils.RxUtils;
import com.baseandroid.utils.runpermission.PermissionManger;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.database.GreenDaoDbHelp;
import com.haodaibao.fanbeiapp.event.LocationResultEvent;
import com.haodaibao.fanbeiapp.module.Consent;
import com.haodaibao.fanbeiapp.module.maphome.cluster.ClusterClickListener;
import com.haodaibao.fanbeiapp.module.maphome.cluster.ClusterItem;
import com.haodaibao.fanbeiapp.module.maphome.cluster.ClusterOverlay;
import com.haodaibao.fanbeiapp.module.maphome.cluster.ClusterRender;
import com.haodaibao.fanbeiapp.module.maphome.cluster.MoveFinishListener;
import com.haodaibao.fanbeiapp.module.maphome.cluster.demo.MarkerInfo;
import com.haodaibao.fanbeiapp.module.maphome.cluster.demo.RegionItem;
import com.haodaibao.fanbeiapp.module.search.SearchDeliciousActivity;
import com.haodaibao.fanbeiapp.repository.LifeRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.MerchantDetailResp;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoResp;
import com.haodaibao.fanbeiapp.repository.json.OpenCityBean;
import com.haodaibao.fanbeiapp.repository.json.OpenCitysInfoResp;
import com.haodaibao.fanbeiapp.repository.json.ProvinCityDistInfoResp;
import com.jayfeng.lesscode.core.DisplayLess;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

import static com.baseandroid.config.Constant.TAGNO;
import static com.haodaibao.fanbeiapp.database.GreenDaoDbHelp.queryCityInfoByName;
import static com.haodaibao.fanbeiapp.repository.MerchantRepository.getInstance;
import static java.lang.Double.parseDouble;

/**
 * date: 2017/11/2.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 * 20150429100067 丽人
 * 20150429100068 美食
 * 20150429100069 商超
 * 20150910100090 酒店
 * 20150910100091 运动健身
 * 20150910100092 购物
 * 20150910100093 生活
 * 20150910100094 休闲娱乐
 * 20160114100355 亲子
 * 20160202100509 电影院
 * 20160801105185 甜品
 * 20160808105190 烤肉烧烤
 * 20170814117436 摄影写真
 */

public class MaphomePresenter implements MaphomeContract.Presenter, AMap.OnMapLoadedListener, LocationSource, AMapLocationListener, ClusterClickListener, ClusterRender, MoveFinishListener {
    private final String TAG = "MaphomePresenter";
    private AMap aMap;
    private MaphomeContract.MaphomeView mView;
    private PublishSubject<HashMap<String, String>> mPublishSubject;
    private PublishSubject<HashMap<String, String>> mPublishSubjectMerchantInfo;
    public ClusterOverlay mClusterOverlay;
    private Map<Integer, Drawable> mBackDrawAbles = new HashMap<Integer, Drawable>();
    public static int[] leftBg = {R.drawable.icon_map_left_bg, R.drawable.icon_map_left_bg1, R.drawable.icon_map_left_bg2, R.drawable.icon_map_left_bg3, R.drawable.icon_map_left_bg4, R.drawable.icon_map_left_bg5, R.drawable.icon_map_left_bg6, R.drawable.icon_map_left_bg7, R.drawable.icon_map_left_bg8, R.drawable.icon_map_left_bg9, R.drawable.icon_map_left_bg10};
    public static int[] leftBg_noicon = {R.drawable.icon_map_left_bg_c, R.drawable.icon_map_left_bg_c1, R.drawable.icon_map_left_bg_c2, R.drawable.icon_map_left_bg_c3, R.drawable.icon_map_left_bg_c4, R.drawable.icon_map_left_bg_c5, R.drawable.icon_map_left_bg_c6, R.drawable.icon_map_left_bg_c7, R.drawable.icon_map_left_bg_c8, R.drawable.icon_map_left_bg_c9, R.drawable.icon_map_left_bg_c10};
    public static int[] left_icon = {R.drawable.icon_map_left, R.drawable.icon_map_left1, R.drawable.icon_map_left2, R.drawable.icon_map_left3, R.drawable.icon_map_left4, R.drawable.icon_map_left5, R.drawable.icon_map_left6, R.drawable.icon_map_left7, R.drawable.icon_map_left8, R.drawable.icon_map_left9, R.drawable.icon_map_left10};
    public static int[] text_colors = {R.color.map_text_color, R.color.map_text_color1, R.color.map_text_color2, R.color.map_text_color3, R.color.map_text_color4, R.color.map_text_color5, R.color.map_text_color6, R.color.map_text_color7, R.color.map_text_color8, R.color.map_text_color9, R.color.map_text_color10};
    public static String[] categoryNames = {"20150910100092", "20150429100068", "20150910100094", "20150429100067", "20150429100069", "20150910100090", "20150910100091", "20160114100355", "20170814117436", "20160202100509", "20150910100093"};
    public final int pageSie = 100000;
    public String cityCode;
    List<MerchantInfoEntiy> merchantInfoEntiyList;
    private boolean isLocation;
    private boolean isAutoMove;
    public double changeLongitude;
    public double changeLatitude;
    public boolean locationSuc;
    private RxObserver<Data<MerchantDetailResp>> merchantDetailRxObserver;
    private RxObserver<Data<MerchantInfoResp>> merchantInfoRxObserver;
    private boolean merchantDetailDisposable;
    private boolean merchantInfoDisposable;

    //categoryno
    public MaphomePresenter(MaphomeContract.MaphomeView view) {
        this.mView = view;
        mPublishSubject = PublishSubject.create();
        mPublishSubjectMerchantInfo = PublishSubject.create();
        merchantDetailRxObserver = new RxObserver<Data<MerchantDetailResp>>() {
            @Override
            public void onNext(@NonNull Data<MerchantDetailResp> merchantInfoRespData) {
                LogUtil.e("onNext");
                merchantDetailDisposable = false;
                if (RxObserver.checkJsonCode(merchantInfoRespData, true)) {
                    mView.updateMerchantInfo(merchantInfoRespData.getResult());
                }
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                LogUtil.e("onError");
            }
        };
        merchantInfoRxObserver = new RxObserver<Data<MerchantInfoResp>>() {
            @Override
            public void onSubscribe(Disposable d) {
                super.onSubscribe(d);
            }

            @Override
            public void onNext(@NonNull Data<MerchantInfoResp> merchantInfoRespData) {
                if (RxObserver.checkJsonCode(merchantInfoRespData, true) && merchantInfoRespData
                        .getResult() != null) {
                    merchantInfoDisposable = false;
                    addMarkersToMap2(merchantInfoRespData.getResult().getMerchantList());
                    merchantInfoEntiyList = merchantInfoRespData.getResult()
                            .getMerchantList();
                    LogUtil.e("merchantInfoDisposable=" + merchantInfoDisposable);
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                super.onError(e);
            }

        };
        initPublishSubjectMerchantInfo();
        initPublishSubject();
    }

    private void initPublishSubject() {
        mPublishSubject.debounce(300, TimeUnit.MILLISECONDS)
                .switchMap(new Function<HashMap<String, String>, ObservableSource<Data<MerchantInfoResp>>>() {
                    @Override
                    public ObservableSource<Data<MerchantInfoResp>> apply(HashMap<String, String> stringStringHashMap) throws
                            Exception {
                        LogUtil.e("showloading");
                        return LifeRepository.getInstance()
                                .getMapShanghuListInfo(stringStringHashMap, true);
                    }
                })
                .compose(RxUtils.<Data<MerchantInfoResp>>applySchedulersLifeCycle((BaseActivity) (mView)))
                .doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                        LogUtil.e("doFinally" + merchantInfoDisposable);
                        merchantInfoDisposable = true;
                        LogUtil.e("doFinally" + merchantInfoDisposable);
                    }
                })
                .subscribe(merchantInfoRxObserver);
    }

    //    }
    private void initPublishSubjectMerchantInfo() {

        mPublishSubjectMerchantInfo.debounce(300, TimeUnit.MILLISECONDS)
                .switchMap(new Function<HashMap<String, String>, ObservableSource<Data<MerchantDetailResp>>>() {
                    @Override
                    public ObservableSource<Data<MerchantDetailResp>> apply(HashMap<String, String> stringStringHashMap) throws
                            Exception {
                        LogUtil.e("switch");
                        return getInstance().getMerchantDetails(stringStringHashMap, true);
                    }
                })
                .compose(RxUtils.<Data<MerchantDetailResp>>applySchedulersLifeCycle((BaseActivity) (mView)))
                .doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                        LogUtil.e("doFinally" + merchantDetailDisposable);
                        merchantDetailDisposable = true;
                        LogUtil.e("doFinally" + merchantDetailDisposable);
                    }
                })
                .subscribe(merchantDetailRxObserver);
    }

    public void initMap() {
        if (aMap == null) {
            aMap = mView.getMapView().getMap();
        }
//        setMapCustomStyleFile((BaseActivity) mView);
        //		setMapCustomTextureFile(this);

        //开启自定义样式
        aMap.setMapCustomEnable(true);
        aMap.getUiSettings().setTiltGesturesEnabled(false);//禁用双手拖拽3D效果
        aMap.getUiSettings().setRotateGesturesEnabled(false);
        aMap.getUiSettings().setZoomControlsEnabled(false);
        aMap.getUiSettings().setLogoBottomMargin(-100);
        MyLocationStyle myLocationStyle = new MyLocationStyle();
        myLocationStyle.myLocationIcon(BitmapDescriptorFactory.fromResource(R.drawable.location_marker));// 设置小蓝点的图标
        aMap.setMyLocationStyle(myLocationStyle);
        if (Global.getMyLocation().getLatitude() == 0.0) {
            aMap.moveCamera(CameraUpdateFactory.zoomTo(16f));//设置地图缩放
        } else {
            aMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(new LatLng(Global
                    .getMyLocation()
                    .getLatitude(), Global.getMyLocation()
                    .getLongitude()), 16f, 0, 0)));//设置地图缩放
        }
        aMap.setOnMapLoadedListener(this);// 设置amap加载成功事件监听器
        aMap.setLocationSource(this);// 设置定位监听
        aMap.getUiSettings().setMyLocationButtonEnabled(false);// 设置默认定位按钮是否显示
        aMap.getUiSettings().setCompassEnabled(false);
        aMap.setMyLocationEnabled(true);// 设置为true表示显示定位层并可触发定位，false表示隐藏定位层并不可触发定位，默认是false
    }

    /**
     * 定位发生变化监听
     *
     * @param aMapLocation
     */
    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
    }

    /**
     * 地图加载成功监听
     */
    @Override
    public void onMapLoaded() {
        Log.e(TAG, "onMapLoaded: ");
        PermissionManger.getInstance().startLocation(((MaphomeActivity) mView));
        mClusterOverlay = new ClusterOverlay(aMap, DisplayLess.$dp2px(50), (BaseActivity) mView);
        mClusterOverlay.setClusterRenderer(this);
        mClusterOverlay.setOnClusterClickListener(this);
        mClusterOverlay.setMoveFinishListener(this);
    }

    @Override
    public void activate(OnLocationChangedListener onLocationChangedListener) {
        Log.e(TAG, "activate: ");
    }

    @Override
    public void deactivate() {
        /*GaoDeMap.getInstance().stopMap();
        GaoDeMap.getInstance().destroyMap();*/
    }


    @Override
    public void onCameraChangeFinish(CameraPosition cameraPosition) {
        changeLongitude = cameraPosition.target.longitude;
        changeLatitude = cameraPosition.target.latitude;
        Log.e(TAG, "onCameraChangeFinish: longitude=" + changeLongitude);
        Log.e(TAG, "onCameraChangeFinish: latitude=" + changeLatitude);
        if (isAutoMove) {
            Log.e(TAG, "onCameraChangeFinish: assignClusters");
            mClusterOverlay.assignClusters();
        } else {
            Log.e(TAG, "onCameraChangeFinish: getCityInfo");
            getCityInfo(changeLatitude, changeLongitude);
        }
        isAutoMove = false;
        //        LatLng latLng = new LatLng(changeLatitude, changeLongitude);
        //        aMap.clear();
        //        final Marker marker = aMap.addMarker(new MarkerOptions().position(latLng).title("北京").icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(((BaseActivity) mView).getResources(), R.drawable.icon_warn_red))));
    }

    public void getCityInfo(final double latitude, final double longitude) {
        List<OpenCityBean> citysList = Global.getOpenCitysResp().getSites();
        if (latitude == 0) {//定位失败
            getMerchanList(latitude, longitude, pageSie + "", Global.getmapSelectCityCode(), TAGNO);
            cityCode = Global.getmapSelectCityCode();//为判断是否显示距离所需要
            ((MaphomeActivity) mView).setNoopen(View.GONE);
        } else {//滑动地图定位都可以改变经纬度。以下条件均不可少
            GaoDeMap.getCityName(new LatLonPoint(latitude, longitude), new GaoDeMap.LocationResult() {
                @Override
                public void onLocationResult(String addressName) {
                    List<OpenCityBean> openCityInfoEntityList = queryCityInfoByName(addressName)
                            .list();
                    if (openCityInfoEntityList.size() > 0) {
                        ((MaphomeActivity) mView).setNoopen(View.GONE);
                        if (!TextUtils.equals(cityCode, openCityInfoEntityList.get(0)
                                .getCode())) {
                            cityCode = openCityInfoEntityList.get(0).getCode();
                        }
                        getMerchanList(latitude, longitude, pageSie + "", cityCode, "");
                    } else {
                        cityCode = "";
                        if (!TextUtils.equals(addressName, "")) {
                            aMap.clear();
                            mClusterOverlay.mClusters.clear();
                            mClusterOverlay.clearMarkers();
                            mClusterOverlay.mAddMarkers.clear();
                            ((MaphomeActivity) mView).setNoopen(View.VISIBLE);
                        } else {
                            ((MaphomeActivity) mView).setNoopen(View.GONE);
                        }
                    }
                    if (TextUtils.equals(addressName, Global.getMyLocation().cityName) && Global
                            .getMyLocation()
                            .isLocationSuccess()) {
                        Consent.currentLocation = true;
                    } else {
                        Consent.currentLocation = false;
                    }
                }
            });
        }
    }

    @Override
    public void onCameraChange() {
        //        if (((MaphomeActivity) mView).getMap_search_load().getVisibility() == View.VISIBLE) {
        //            ((MaphomeActivity) mView).getMap_search_load().setVisibility(View.GONE);
        //        }
    }

    public void getOpenCitys() {
        initMap();
        final Map<String, String> hasMap = new HashMap<>();
        hasMap.put("open", "1");
        Observable<Data<OpenCitysInfoResp>> openCitysObserver = LifeRepository.getInstance()
                .getPubCitysByOpen(hasMap)
                .map(new Function<Data<OpenCitysInfoResp>, Data<OpenCitysInfoResp>>() {
                    @Override
                    public Data<OpenCitysInfoResp> apply(Data<OpenCitysInfoResp> openCitysInfoRespData) throws Exception {
                        Log.e("++++++", "111 Thread = " + Thread.currentThread().getName());
                        if (RxObserver.checkJsonCode(openCitysInfoRespData, false)) {
                            if (!CommonUtils.isSameObject(Global.getOpenCitysResp(), openCitysInfoRespData.getResult())) {
                                GreenDaoDbHelp.insertallCityInfo(openCitysInfoRespData.getResult().getSites());
                            }
                        }
                        return openCitysInfoRespData;
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Consumer<Data<OpenCitysInfoResp>>() {
                    @Override
                    public void accept(Data<OpenCitysInfoResp> openCitysRespData) throws
                            Exception {
                        Log.e("++++++", "222 Thread = " + Thread.currentThread().getName());
                        if (RxObserver.checkJsonCode(openCitysRespData, false)) {
                            if (!CommonUtils.isSameObject(Global.getOpenCitysResp(), openCitysRespData.getResult())) {
                                Global.setOpenCitysResp(openCitysRespData.getResult());
                            }
                        }
                    }
                });

        Map<String, String> hasMap2 = new HashMap<>();
        hasMap2.put("version", Global.getProvCityDistResp()
                .getResult() == null ? "" : Global.getProvCityDistResp()
                .getResult()
                .getVersion());
        Observable<Data<ProvinCityDistInfoResp>> provinceCityDistrictObserver = LifeRepository
                .getInstance()
                .getProvinceCityDistrict(hasMap2)
                .map(new Function<Data<ProvinCityDistInfoResp>, Data<ProvinCityDistInfoResp>>() {
                    @Override
                    public Data<ProvinCityDistInfoResp> apply(Data<ProvinCityDistInfoResp> provinCityDistInfoRespData) throws
                            Exception {
                        Log.e("++++++", "333 Thread = " + Thread.currentThread().getName());
                        if (RxObserver.checkJsonCode(provinCityDistInfoRespData, true)) {
                            if (provinCityDistInfoRespData.getResult().getResult() != null && provinCityDistInfoRespData.getResult().getResult()
                                    .getDistricts() != null && !CommonUtils.isSameObject(Global.getProvCityDistResp(), provinCityDistInfoRespData.getResult())) {
                                GreenDaoDbHelp.insertallDistrictsInfo(provinCityDistInfoRespData.getResult().getResult().getDistricts());
                            }
                        }
                        return provinCityDistInfoRespData;
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Consumer<Data<ProvinCityDistInfoResp>>() {
                    @Override
                    public void accept(@NonNull Data<ProvinCityDistInfoResp> provinCityDistInfoRespData) throws
                            Exception {
                        Log.e("++++++", "444 Thread = " + Thread.currentThread().getName());
                        Global.setSelectCityNameAndCodeByGaodeNameAdcode();
                        if (RxObserver.checkJsonCode(provinCityDistInfoRespData, true)) {
                            if (provinCityDistInfoRespData.getResult().getResult() != null && provinCityDistInfoRespData.getResult()
                                    .getResult().getDistricts() != null && !CommonUtils.isSameObject(Global.getProvCityDistResp(), provinCityDistInfoRespData.getResult())) {
                                Global.setProvCityDistResp(provinCityDistInfoRespData.getResult());
                            }
                        }
                    }
                });

        Observable.concat(openCitysObserver, provinceCityDistrictObserver)
                .compose(RxUtils.<Data>applySchedulersLifeCycle((BaseActivity) (mView)))
                .subscribe(new RxObserver<Data>() {
                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                    }

                    @Override
                    public void onComplete() {
                        /*----定位成功刷新位置信息----*/
                        Global.setSelectCityNameAndCodeByGaodeNameAdcode();
                    }
                });
    }

    public void getMerchantInfo(String merchantno) {
        final HashMap<String, String> merchantMap = new HashMap<>();
        merchantMap.put("merchantno", merchantno);
        LogUtil.e("mPublishSubjectMerchantInfo" + merchantInfoDisposable);
        if (merchantDetailDisposable) {
            merchantDetailDisposable = false;
            initPublishSubjectMerchantInfo();
        }
        mPublishSubjectMerchantInfo.onNext(merchantMap);
    }


    @Override
    public void getMerchanList(Double latitude, Double longitude, String pageSizeStr, String citycode, String tagno) {
        final HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("page", "" + 1);
        hashMap.put("pageSize", "" + pageSizeStr);
        if (longitude == 0) {
            hashMap.put("longitude", "");
        } else {
            hashMap.put("longitude", "" + longitude);
        }
        if (latitude == 0) {
            hashMap.put("latitude", "");
        } else {
            hashMap.put("latitude", "" + latitude);
        }
        hashMap.put("arearange", "" + "1500");
        hashMap.put("citycode", citycode);
        hashMap.put("sort", "distance.asc");
        if (!TextUtils.isEmpty(tagno)) {
            hashMap.put("tagno", tagno);
        } else {
            hashMap.put("tagno", "");
        }
        //        ((MaphomeActivity) mView).getMap_search_load().setVisibility(View.VISIBLE);
        LogUtil.e("mPublishSubject" + merchantDetailDisposable);
        if (merchantInfoDisposable) {
            merchantInfoDisposable = false;
            initPublishSubject();
            mView.getMapView().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mPublishSubject.onNext(hashMap);
                }
            }, 200);
        } else {
            mPublishSubject.onNext(hashMap);
        }


    }


    /**
     * 在地图上添加marker,不删除原有点，只是更新
     */
    private void addMarkersToMap1(List<MerchantInfoEntiy> merchantInfoList) {
        LatLng ll = null;
        for (int i = 0; i < merchantInfoList.size(); i++) {
            String latitude = merchantInfoList.get(i).getLatitude();
            String longitude = merchantInfoList.get(i).getLongitude();
            String title = merchantInfoList.get(i).getMerchantname();
            String merchantno = merchantInfoList.get(i).getMerchantno();
            String categoryName = merchantInfoList.get(i).getCategoryName();
            boolean iscontains = false;

            for (int j = 0; j < mClusterOverlay.getmClusterItems().size(); j++) {
                if (TextUtils.equals(merchantno, mClusterOverlay.getmClusterItems()
                        .get(j)
                        .getMarkerInfo().merchantno)) {
                    iscontains = true;
                    break;
                }
            }
            if (!iscontains) {
                MarkerInfo markerInfo = new MarkerInfo();
                markerInfo.des = title;
                markerInfo.merchantno = merchantno;
                int bgInt = leftBg[i / 4];
                //                for (int j = 0; j < categoryNames.length; j++) {
                //                    if (TextUtils.equals(categoryNo, categoryNames[j])) {
                //                        bgInt = leftBg[i];
                //                        break;
                //                    }
                //                }

                markerInfo.categoryNo = bgInt;
                if (!TextUtils.isEmpty(latitude) && !TextUtils.isEmpty(longitude)) {
                    ll = new LatLng(parseDouble(latitude), parseDouble(longitude));
                    RegionItem regionItem = new RegionItem(ll, markerInfo);
                    mClusterOverlay.addClusterItem(regionItem);
                }

            }

        }
        //        aMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 100));//第二个参数为四周留空宽度
        //        aMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 15));
    }

    /**
     * 删除原有的点，更新最新的点
     *
     * @param merchantInfoList
     */
    private void addMarkersToMap2(final List<MerchantInfoEntiy> merchantInfoList) {
        mView.setLocatoEnable(false);
        LatLng ll = null;
        if (mClusterOverlay == null) {
            return;
        }
        mClusterOverlay.clearMarkers();
        ArrayList<RegionItem> regionItems = new ArrayList<>();
        for (int i = 0; i < merchantInfoList.size(); i++) {
            String latitude = merchantInfoList.get(i).getLatitude();
            String longitude = merchantInfoList.get(i).getLongitude();
            String title = merchantInfoList.get(i).getMerchantname();
            String merchantno = merchantInfoList.get(i).getMerchantno();
            String returnBeginAmount = merchantInfoList.get(i).getReturnbeginamount();
            String returnRate = merchantInfoList.get(i).getReturnrate();
            String refundMode = merchantInfoList.get(i).getRefundmode();
            MarkerInfo markerInfo = new MarkerInfo();
            markerInfo.des = title;
            markerInfo.merchantno = merchantno;
            if (!TextUtils.isEmpty(refundMode)) {
                markerInfo.discount = FormatUtil.setMapDiscount(refundMode, returnBeginAmount, returnRate);
            } else {
                markerInfo.discount = "";
            }
            int bgInt = 0;
            for (int j = 0; j < categoryNames.length; j++) {
                if (TextUtils.equals(categoryNames[j], merchantInfoList.get(i).categoryno)) {
                    bgInt = j;
                    break;
                }
            }
            markerInfo.categoryNo = bgInt;
            if (!TextUtils.isEmpty(latitude) && !TextUtils.isEmpty(longitude)) {
                ll = new LatLng(parseDouble(latitude), parseDouble(longitude));
                RegionItem regionItem = new RegionItem(ll, markerInfo);
                regionItems.add(regionItem);
            }
        }
        mClusterOverlay.addCluterItem(regionItems);
        mClusterOverlay.assignClusters();
        if (isLocation && merchantInfoList.size() > 0) {
            isAutoMove = true;
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            double merchantInfoLa = Double.parseDouble(merchantInfoList.get(merchantInfoList
                    .size() - 1).getLatitude());
            double merchantInfoLo = Double.parseDouble(merchantInfoList.get(merchantInfoList
                    .size() - 1).getLongitude());
            LatLng latLng = new LatLng(merchantInfoLa, merchantInfoLo);


            builder.include(latLng);
            builder.include(new LatLng(2 * Global.getMyLocation()
                    .getLatitude() - merchantInfoLa, 2 * Global.getMyLocation()
                    .getLongitude() - merchantInfoLo));
            builder.include(new LatLng(Global.getMyLocation()
                    .getLatitude(), Global.getMyLocation().getLongitude()));

            //            double lo = Util.getlo(new LatLng(Global.getMyLocation().getLatitude(), Global.getMyLocation().getLongitude()), latLng);
            //            builder.include(new LatLng(Global.getMyLocation().getLatitude(),Global.getMyLocation().getLongitude()-lo));
            //            builder.include(new LatLng(Global.getMyLocation().getLatitude(),Global.getMyLocation().getLongitude()+lo));
            aMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 200));//第二个参数为四周留空宽度
        }
        isLocation = false;
        mView.setLocatoEnable(true);
    }

    public void bindDestory() {
        /*GaoDeMap.getInstance().stopMap();
        GaoDeMap.getInstance().destroyMap();*/
    }

    public void onEvent(LocationResultEvent event) {
        Log.e(TAG, "onEvent: ");
        isLocation = true;
        LogUtil.e("la" + Global.getMyLocation().getLatitude());
        LogUtil.e("lo" + Global.getMyLocation().getLongitude());
        if (event.isMbResult()) {
            locationSuc = true;
            //            Global.setSelectCity(Global.getMyLocation().getCityName());
            List<OpenCityBean> openCityInfoEntityList = GreenDaoDbHelp.queryCityByGaodeNameAdCode()
                    .list();
            if (openCityInfoEntityList.size() > 0) {
                //                if (!TextUtils.equals(Global.getSelectCityCode(), openCityInfoEntityList.get(0).getCode())) {
                //                    getCateInfo(false);
                //                }
                Global.setmapSelectCityCode(openCityInfoEntityList.get(0).getCode());

            }
            getCityInfo(Global.getMyLocation().getLatitude(), Global.getMyLocation()
                    .getLongitude());
        } else {
            locationSuc = false;
            if (TextUtils.isEmpty(Global.getmapSelectCityCode())) {
                //                Global.setSelectCity(Constant.SELECTCITY);
                Global.setmapSelectCityCode(Constant.SELECTCITYCODE);
                Global.getMyLocation().setLatitude(Constant.LATITUDE);
                Global.getMyLocation().setLongitude(Constant.LONGITUDE);
            }
            getCityInfo(0, 0);
        }

        //        if (TextUtils.isEmpty(Global.getSelectCityCode())) {
        //            Global.setSelectCity(Constant.SELECTCITY);
        //            Global.getMyLocation().setLatitude(Constant.LATITUDE);
        //            Global.getMyLocation().setLongitude(Constant.LONGITUDE);
        //            Global.setMyLocation(Global.getMyLocation());
        //        }
        isAutoMove = true;
        aMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(new LatLng(Global
                .getMyLocation()
                .getLatitude(), Global.getMyLocation()
                .getLongitude()), 16f, 0, 0)));//设置地图缩放
        //        getCityInfo(Global.getMyLocation().getLatitude(), Global.getMyLocation().getLongitude());
    }

    @Override
    public Drawable getDrawAble(int clusterNum) {
        int radius = DisplayLess.$dp2px(40);
        if (clusterNum == 1) {
            Drawable bitmapDrawable = mBackDrawAbles.get(1);
            if (bitmapDrawable == null) {
                bitmapDrawable = ((BaseActivity) mView).getApplication()
                        .getResources()
                        .getDrawable(R.drawable.icon_openmap_mark);
                mBackDrawAbles.put(1, bitmapDrawable);
            }

            return bitmapDrawable;
        } else if (clusterNum < 5) {

            Drawable bitmapDrawable = mBackDrawAbles.get(2);
            if (bitmapDrawable == null) {
                bitmapDrawable = new BitmapDrawable(null, drawCircle(radius, Color.argb(159, 210, 154, 6)));
                mBackDrawAbles.put(2, bitmapDrawable);
            }

            return bitmapDrawable;
        } else if (clusterNum < 10) {
            Drawable bitmapDrawable = mBackDrawAbles.get(3);
            if (bitmapDrawable == null) {
                bitmapDrawable = new BitmapDrawable(null, drawCircle(radius, Color.argb(199, 217, 114, 0)));
                mBackDrawAbles.put(3, bitmapDrawable);
            }

            return bitmapDrawable;
        } else {
            Drawable bitmapDrawable = mBackDrawAbles.get(4);
            if (bitmapDrawable == null) {
                bitmapDrawable = new BitmapDrawable(null, drawCircle(radius, Color.argb(235, 215, 66, 2)));
                mBackDrawAbles.put(4, bitmapDrawable);
            }

            return bitmapDrawable;
        }
    }

    /**
     * - latitude31.258649
     * longitude121.339088
     *
     * @param marker       点击的聚合点
     * @param clusterItems
     */
    @Override
    public void onClick(Marker marker, List<ClusterItem> clusterItems) {
        if (CommonUtils.isFastDoubleClick()) {
            return;
        }
        if (clusterItems.size() > 1) {
            final double latitude = clusterItems.get(0).getPosition().latitude;
            final double longitude = clusterItems.get(0).getPosition().longitude;
            GaoDeMap.getAddressName(new LatLonPoint(latitude + 0.0024, longitude - 0.004328), new GaoDeMap.LocationResult() {
                @Override
                public void onLocationResult(String addressName) {
                    LogUtil.e(addressName + cityCode);
                    LogUtil.e("latitude" + latitude);
                    LogUtil.e("longitude" + longitude);
                    Intent intent = new Intent((BaseActivity) mView, SearchDeliciousActivity.class);
                    intent.putExtra("longitude", longitude + "");
                    intent.putExtra("latitude", latitude + "");
                    intent.putExtra("cityCode", cityCode);
                    intent.putExtra("addressName", addressName);
                    ((BaseActivity) mView).startActivity(intent);
                }
            });
        } else {
            mView.onMarkerClick(clusterItems.get(0), merchantInfoEntiyList);
        }

    }

    private Bitmap drawCircle(int radius, int color) {

        Bitmap bitmap = Bitmap.createBitmap(radius * 2, radius * 2, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        RectF rectF = new RectF(0, 0, radius * 2, radius * 2);
        paint.setColor(color);
        canvas.drawArc(rectF, 0, 360, true, paint);
        return bitmap;
    }

    public void onDestory() {
        if (mClusterOverlay != null) {
            mClusterOverlay.onDestroy();
        }
    }

    /**
     * 设置自定义样式文件，如果想要纹理生效，则必须设置自定义样式文件
     *
     * @param context
     */
    private void setMapCustomStyleFile(Context context) {
        //        String styleName = "style_json_texture.json";
        String styleName = "style.data";
        FileOutputStream outputStream = null;
        InputStream inputStream = null;
        String filePath = null;
        try {
            inputStream = context.getAssets().open(styleName);
            byte[] b = new byte[inputStream.available()];
            inputStream.read(b);

            filePath = context.getFilesDir().getAbsolutePath();
            File file = new File(filePath + "/" + styleName);
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            outputStream = new FileOutputStream(file);
            outputStream.write(b);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        aMap.setCustomMapStylePath(filePath + "/" + styleName);

    }
}
