package com.haodaibao.fanbeiapp.module.personal.paylist;

import com.haodaibao.fanbeiapp.repository.json.PayInfoDetailBean;

/**
 * Created by Routee on 2017/9/11.
 * description: ${cusor}
 */

public interface PayInfoDetailContract {
    interface View{
        void setUpPayInfoData(PayInfoDetailBean result);
        void onRetry();
        void closeUI();
        void onContent();
    }

    interface Presenter{
        void getPayInfoDetailData(String orderNo);

        void setMsgReaded(String msgNo);

    }
}
