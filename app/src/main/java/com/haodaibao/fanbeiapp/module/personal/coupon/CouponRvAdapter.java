package com.haodaibao.fanbeiapp.module.personal.coupon;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baseandroid.utils.FormatUtil;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.CouponBean;

import java.math.BigDecimal;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Routee on 2017/9/12.
 * description: ${cusor}
 */

public class CouponRvAdapter extends BaseRecycleViewAdapter<CouponBean.CouponListBean.ItemsBean> {

    public CouponRvAdapter(Context context) {
        super(context);
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, CouponBean.CouponListBean.ItemsBean item) {
        Hold hold = (Hold) holder;
        String amount = item.getAmount();
        String beginamt = item.getUsebeginamt();
        String amountnew = FormatUtil.filterDecimalPoint(new BigDecimal(amount).setScale(2, BigDecimal.ROUND_HALF_UP) + "");
        String beginamtnew = FormatUtil.filterDecimalPoint(new BigDecimal(beginamt).setScale(2, BigDecimal.ROUND_HALF_UP) + "");
        hold.mTvMoney.setText(amountnew);
        hold.mTvCondition.setText("满" + beginamtnew + "元可用");
        hold.mTvName.setText(item.getCouponName());
        hold.mTvSubName.setText(item.getCouponshortname());
        hold.mTvVld.setText(FormatUtil.changeTime(item.getEndtime(), "yyyy-MM-dd"));
        if (item.getStatus().equals("0")) {
            hold.mTvRmb.setSelected(true);
            hold.mTvMoney.setSelected(true);
            hold.mTvCondition.setSelected(true);
            hold.mTvName.setSelected(true);
            hold.mIvCouponTag.setVisibility(View.GONE);
            hold.mLlContent.setSelected(true);
        } else {
            hold.mTvRmb.setSelected(false);
            hold.mTvMoney.setSelected(false);
            hold.mTvCondition.setSelected(false);
            hold.mTvName.setSelected(false);
            hold.mIvCouponTag.setVisibility(View.VISIBLE);
            hold.mLlContent.setSelected(false);
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_coupon, parent, false);
        return new Hold(view);
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return position;
    }

    class Hold extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_rmb)
        TextView     mTvRmb;
        @BindView(R.id.tv_money)
        TextView     mTvMoney;
        @BindView(R.id.tv_condition)
        TextView     mTvCondition;
        @BindView(R.id.tv_name)
        TextView     mTvName;
        @BindView(R.id.tv_sub_name)
        TextView     mTvSubName;
        @BindView(R.id.tv_vld)
        TextView     mTvVld;
        @BindView(R.id.iv_coupon_tag)
        ImageView    mIvCouponTag;
        @BindView(R.id.ll_content)
        LinearLayout mLlContent;

        public Hold(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
