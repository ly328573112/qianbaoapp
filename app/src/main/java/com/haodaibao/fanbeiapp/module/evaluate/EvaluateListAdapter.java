package com.haodaibao.fanbeiapp.module.evaluate;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.DateUtil;
import com.baseandroid.utils.UiUtils;
import com.baseandroid.widget.customimageview.CircleImageViewWithStroke;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.baseandroid.widget.recycleadpter.GridDivider;
import com.bumptech.glide.Glide;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.CommentClickListener;
import com.haodaibao.fanbeiapp.module.CommentImageAdapter;
import com.haodaibao.fanbeiapp.repository.json.CommentListBean;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 开发者：LuoYi
 * Time: 2017 17:19 2017/9/12 09
 */

public class EvaluateListAdapter extends BaseRecycleViewAdapter<CommentListBean.CommentBean> {

    private CommentClickListener commentClickListener;

    EvaluateListAdapter(Context context, CommentClickListener commentClickListener) {
        super(context);
        this.commentClickListener = commentClickListener;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, CommentListBean.CommentBean commentBean) {
        EvaluateHolder evaHolder = (EvaluateHolder) holder;
        String url = CommonUtils.getImageUrl(commentBean.getFace());
        Glide.with(mContext).load(url).error(R.drawable.icon_default_head_info).into(evaHolder.userIcon);
        String name = TextUtils.isEmpty(commentBean.getAlias()) ? commentBean.getMobile() : commentBean.getAlias();
        evaHolder.userName.setText(name);
        String time = DateUtil.changeStringDate(commentBean.getTime());
        evaHolder.evaluateTime.setText(time);
        if (TextUtils.isEmpty(commentBean.getComment())) {
            evaHolder.evaluateContent.setVisibility(View.GONE);
        } else {
            evaHolder.evaluateContent.setVisibility(View.VISIBLE);
            evaHolder.evaluateContent.setText(commentBean.getComment());
        }
        if (TextUtils.equals(commentBean.getStar(), "5.00")) {
            evaHolder.evaluateSatisfied.setImageResource(R.drawable.text_satisfied);
        } else if (TextUtils.equals(commentBean.getStar(), "3.00")) {
            evaHolder.evaluateSatisfied.setImageResource(R.drawable.text_commonly);
        } else {
            evaHolder.evaluateSatisfied.setImageResource(R.drawable.text_dissatisfied);
        }
        if (commentBean.getImgs().size() == 0) {
            evaHolder.evaluateGridview.setVisibility(View.GONE);
        } else {
            evaHolder.evaluateGridview.setVisibility(View.VISIBLE);
            evaHolder.evaluateGridview.setAdapter(new CommentImageAdapter(commentBean.getImgs(), mContext, commentClickListener));
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new EvaluateHolder(View.inflate(mContext, R.layout.item_evaluate_layout, null));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    /**
     * 评价内容布局
     */
    class EvaluateHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_icon)
        CircleImageViewWithStroke userIcon;
        @BindView(R.id.user_name)
        TextView userName;
        @BindView(R.id.evaluate_content)
        TextView evaluateContent;
        @BindView(R.id.evaluate_time)
        TextView evaluateTime;
        @BindView(R.id.evaluate_satisfied)
        ImageView evaluateSatisfied;
        @BindView(R.id.evaluate_gridview)
        RecyclerView evaluateGridview;

        EvaluateHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            evaluateGridview.setLayoutManager(new GridLayoutManager(mContext, 3));
            evaluateGridview.addItemDecoration(new GridDivider(mContext, UiUtils.dp2px(mContext, 10), ContextCompat.getColor(mContext, R.color.white)));
        }
    }
}
