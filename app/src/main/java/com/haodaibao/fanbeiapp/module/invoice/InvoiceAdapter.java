package com.haodaibao.fanbeiapp.module.invoice;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.InvoiceBean;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * date: 2018/2/5.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class InvoiceAdapter extends BaseRecycleViewAdapter<InvoiceBean.InvoiceItem> {
    private OnItemClickListener onItemClickListener;
    public int pos;

    public InvoiceAdapter(Context context, OnItemClickListener onItemClickListener) {
        super(context);
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, final InvoiceBean.InvoiceItem item) {
        final InvoiceHolder invoiceHolder = (InvoiceHolder) holder;
        invoiceHolder.item_invoice_merchantname.setText(item.rise_name);
        invoiceHolder.item_invoice_cash.setText("开票金额：" + item.invoice_amount + "元");
        if (TextUtils.isEmpty(item.invoice_date)) {
            if (!TextUtils.isEmpty(item.create_time) && item.create_time.contains(" ")) {
                invoiceHolder.item_invoice_date.setText(item.create_time.substring(0, item.create_time.indexOf(" ")));
            } else {
                invoiceHolder.item_invoice_date.setText(item.create_time);
            }
        } else {
            if (item.invoice_date.contains(" ")) {
                invoiceHolder.item_invoice_date.setText(item.invoice_date.substring(0, item.invoice_date.indexOf(" ")));
            } else {
                invoiceHolder.item_invoice_date.setText(item.invoice_date);
            }
        }


        if (TextUtils.equals(item.status, "1")) {
            invoiceHolder.item_invoice_status.setText("开票成功");
            invoiceHolder.item_invoice_status.setTextColor(ContextCompat.getColor(mContext, R.color.invoice_text_green));
        } else if (TextUtils.equals(item.status, "2")) {
            invoiceHolder.item_invoice_status.setText("开票失败");
            invoiceHolder.item_invoice_status.setTextColor(ContextCompat.getColor(mContext, R.color.invoice_ffc100));
        } else if (TextUtils.equals(item.status, "3")) {
            invoiceHolder.item_invoice_status.setTextColor(ContextCompat.getColor(mContext, R.color.text_hint));
            invoiceHolder.item_invoice_status.setText("已作废");
        }
        invoiceHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = invoiceHolder.getAdapterPosition();
                onItemClickListener.onItemClick(invoiceHolder.itemView, item);
            }
        });
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new InvoiceHolder(LayoutInflater.from(mContext).inflate(R.layout.item_invoicelist, parent, false));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    public class InvoiceHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_invoice_merchantname)
        TextView item_invoice_merchantname;
        @BindView(R.id.item_invoice_status)
        TextView item_invoice_status;
        @BindView(R.id.item_invoice_cash)
        TextView item_invoice_cash;
        @BindView(R.id.item_invoice_date)
        TextView item_invoice_date;

        public InvoiceHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
