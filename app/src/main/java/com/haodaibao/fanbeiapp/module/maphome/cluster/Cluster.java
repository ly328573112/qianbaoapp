package com.haodaibao.fanbeiapp.module.maphome.cluster;


import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.haodaibao.fanbeiapp.module.maphome.cluster.demo.MarkerInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yiyi.qi on 16/10/10.
 */

public class Cluster {

    private MarkerInfo markerInfo;
    public LatLng mLatLng;
    private List<ClusterItem> mClusterItems;
    private Marker mMarker;

    Cluster(LatLng latLng, MarkerInfo markerInfo) {
        this.markerInfo = markerInfo;
        mLatLng = latLng;
        mClusterItems = new ArrayList<ClusterItem>();
    }

    void addClusterItem(ClusterItem clusterItem) {
        mClusterItems.add(clusterItem);
    }

    int getClusterCount() {
        return mClusterItems.size();
    }


    LatLng getCenterLatLng() {
        return mLatLng;
    }

    void setMarker(Marker marker) {
        mMarker = marker;
    }

    Marker getMarker() {
        return mMarker;
    }

    List<ClusterItem> getClusterItems() {
        return mClusterItems;
    }

    public MarkerInfo getMarkerInfo() {
        return markerInfo;
    }

    public void setMarkerInfo(MarkerInfo markerInfo) {
        this.markerInfo = markerInfo;
    }

    public String getKey() {
        return markerInfo.merchantno + mClusterItems.size();
    }
}
