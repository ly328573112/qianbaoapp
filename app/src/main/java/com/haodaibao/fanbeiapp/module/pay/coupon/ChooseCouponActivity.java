package com.haodaibao.fanbeiapp.module.pay.coupon;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.FormatUtil;
import com.baseandroid.utils.StatusBarHelper;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.CouponInfo;

import java.math.BigDecimal;
import java.util.ArrayList;

import butterknife.BindView;

import static com.baseandroid.config.Constant.CHOOSE_COUPON_RESULTCODE;
import static com.haodaibao.fanbeiapp.module.pay.PayPresenter.isCityOpen;

/**
 * date: 2017/9/15.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class ChooseCouponActivity extends BaseActivity {
    @BindView(R.id.rcv)
    protected RecyclerView rcv;
    private String userAmount, couponNo, cityCode;
    @BindView(R.id.toolbar_title)
    protected TextView toolbar_title;
    @BindView(R.id.coupon_select)
    protected Button coupon_select;
    @BindView(R.id.toolbar_back)
    protected RelativeLayout toolbar_back;
    @BindView(R.id.empty_hint_layout)
    LinearLayout emptyHintLayout;

    private ArrayList<CouponInfo> couponInfos = new ArrayList<>();
    private ChooseCouponAdapter adapter;

    public static void start(BaseActivity context, ArrayList<CouponInfo> couponInfos, String userAmount, String couponNo, String cityCode) {
        Intent b = new Intent(context, ChooseCouponActivity.class);
        b.putExtra("couponInfos", couponInfos);
        b.putExtra("userAmount", userAmount);
        b.putExtra("couponNo", couponNo);
        b.putExtra("cityCode", cityCode);
        context.startActivityForResult(b, 0);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_choose_coupon;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(this);
        toolbar_title.setText("选择优惠券");
        rcv.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ChooseCouponAdapter(this, couponInfos);
        rcv.setAdapter(adapter);
        coupon_select.setOnClickListener(l);
        toolbar_back.setOnClickListener(l);
    }

    private View.OnClickListener l = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.coupon_select:
                    if (CommonUtils.isFastDoubleClick()) {
                        return;
                    }
                    if (adapter.getCount() != 0) {
                        CouponInfo couponInfo = new CouponInfo();
                        Intent intent = new Intent();
                        intent.putExtra("couponInfo", couponInfo);
                        setResult(CHOOSE_COUPON_RESULTCODE, intent);
                    }
                    finish();
                    break;
                case R.id.toolbar_back:
                    if (CommonUtils.isFastDoubleClick()) {
                        return;
                    }
                    finish();
                    break;
            }
        }
    };

    @Override
    protected void setupData(Bundle savedInstanceState) {
        Intent intent = getIntent();
        couponInfos.clear();
        couponInfos.addAll((ArrayList<CouponInfo>) intent.getSerializableExtra("couponInfos"));
        userAmount = intent.getStringExtra("userAmount");
        couponNo = intent.getStringExtra("couponNo");
        cityCode = intent.getStringExtra("cityCode");
        doCouponInfos(couponInfos);
        adapter.notifyDataSetChanged();

    }

    public void doCouponInfos(ArrayList<CouponInfo> couponInfo) {
        if (couponInfo.size() == 0) {
            emptyHintLayout.setVisibility(View.VISIBLE);
            return;
        } else {
            emptyHintLayout.setVisibility(View.INVISIBLE);
        }
        for (int i = 0; i < couponInfo.size(); i++) {
            couponInfo.get(i).setUserAmount(userAmount);
            String openCity = isCityOpen(cityCode, couponInfo.get(i).getCitys());
            double amt = Double.parseDouble(couponInfo.get(i).getUsebeginamt()) - Double.parseDouble(userAmount);
            String amtnew = FormatUtil.filterDecimalPoint(new BigDecimal(amt).setScale(2, BigDecimal.ROUND_HALF_UP) + "");
            if (TextUtils.equals("01", couponInfo.get(i).getIsFailure())) {
                couponInfo.get(i).setCanUse("1");
                couponInfo.get(i).setUnReason(FormatUtil.changeTime(couponInfo.get(i).getStarttime(), "yyyy-MM-dd") + "起可用");
            } else if (!TextUtils.equals("", openCity)) {
                couponInfo.get(i).setCanUse("1");
                couponInfo.get(i).setUnReason("仅限" + openCity + "的商家使用");
            } else if (amt > 0) {
                couponInfo.get(i).setCanUse("1");
                couponInfo.get(i).setUnReason("消费需满" + FormatUtil.filterDecimalPoint(new BigDecimal(couponInfo.get(i).getUsebeginamt()).setScale(2, BigDecimal.ROUND_HALF_UP) + "") + "元，还差" + amtnew + "元可用");
            } else {
                couponInfo.get(i).setCanUse("0");
            }
            if (TextUtils.equals(couponInfo.get(i).getSerialno(), couponNo)) {
                couponInfo.get(i).setIsSelect("0");
            } else {
                couponInfo.get(i).setIsSelect("1");
            }
        }
    }
}
