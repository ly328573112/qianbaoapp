package com.haodaibao.fanbeiapp.module.personal;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.StatusBarHelper;
import com.haodaibao.fanbeiapp.BuildConfig;
import com.haodaibao.fanbeiapp.R;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import butterknife.BindView;
import butterknife.OnClick;

/**
 *                             _ooOoo_
 *                            o8888888o
 *                            88" . "88
 *                            (| -_- |)
 *                            O\  =  /O
 *                         ____/`---'\____
 *                       .'  \\|     |//  `.
 *                      /  \\|||  :  |||//  \
 *                     /  _||||| -:- |||||-  \
 *                     |   | \\\  -  /// |   |
 *                     | \_|  ''\---/''  |   |
 *                     \  .-\__  `-`  ___/-. /
 *                   ___`. .'  /--.--\  `. . __
 *                ."" '<  `.___\_<|>_/___.'  >'"".
 *               | | :  `- \`.;`\ _ /`;.`/ - ` : | |
 *               \  \ `-.   \_ __\ /__ _/   .-` /  /
 *          ======`-.____`-.___\_____/___.-`____.-'======
 *                             `=---='
 *          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *                     佛祖保佑        永无BUG
 *            佛曰:
 *                   写字楼里写字间，写字间里程序员；
 *                   程序人员写程序，又拿程序换酒钱。
 *                   酒醒只在网上坐，酒醉还来网下眠；
 *                   酒醉酒醒日复日，网上网下年复年。
 *                   但愿老死电脑间，不愿鞠躬老板前；
 *                   奔驰宝马贵者趣，公交自行程序员。
 *                   别人笑我忒疯癫，我笑自己命太贱；
 *                   不见满街漂亮妹，哪个归得程序员？
 */
public class MoreActivity extends BaseActivity {

    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.tv_version_name)
    TextView mTvVersionName;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_more;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(MoreActivity.this);
        mToolbarTitle.setText("关于");
        mTvVersionName.setText("当前版本:" + BuildConfig.VERSION_NAME);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {

    }

    @OnClick({R.id.toolbar_back})
    public void onViewClicked(View view) {
        if (CommonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.toolbar_back:
            default:
                ActivityJumpUtil.goBack(MoreActivity.this);
                break;
        }
    }


}
