package com.haodaibao.fanbeiapp.module.imgoriginal;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Build;
import android.support.v4.view.PagerAdapter;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.android.photocameralib.media.utils.AppOperator;
import com.android.photocameralib.media.widget.ImagePreviewView;
import com.android.photocameralib.media.widget.Loading;
import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.haodaibao.fanbeiapp.R;

import java.io.File;
import java.util.List;
import java.util.concurrent.Future;

/**
 * @author LuoYi
 *         Time: 2017 11:09 2017/10/30 10
 */

public class OriginalPagerAdapter extends PagerAdapter implements ImagePreviewView.OnReachBorderListener {

    private Activity activity;
    private Point mDisplayDimens;
    private List<String> mImageSources;
    private OriginalContract.PagerAdapterView pagerAdapter;

    OriginalPagerAdapter(Activity activity, List<String> mImageSources, OriginalContract.PagerAdapterView pagerAdapter) {
        this.activity = activity;
        this.mImageSources = mImageSources;
        this.pagerAdapter = pagerAdapter;
    }

    public void addData(List<String> mImageSources) {
        this.mImageSources = mImageSources;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mImageSources.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.lay_gallery_page_item_contener, container, false);
        ImagePreviewView previewView = (ImagePreviewView) view.findViewById(R.id.iv_preview);
        previewView.setOnReachBorderListener(this);
        Loading loading = (Loading) view.findViewById(R.id.loading);
        ImageView defaultView = (ImageView) view.findViewById(R.id.iv_default);

        loadImage(position, mImageSources.get(position), previewView, defaultView, loading);

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public void onReachBorder(boolean isReached) {
        pagerAdapter.onReached(isReached);
    }

    private <T> void loadImage(final int pos, final T urlOrPath, final ImageView previewView, final ImageView defaultView, final Loading loading) {

        loadImageDoDownAndGetOverrideSize(urlOrPath, new OriginalContract.DoOverrideSizeCallback() {
            @Override
            public void onDone(int overrideW, int overrideH, boolean isTrue) {
                DrawableRequestBuilder builder = Glide.with(activity)
                        .load(urlOrPath)
                        .listener(new RequestListener<T, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, T model, Target<GlideDrawable> target, boolean isFirstResource) {
                                if (e != null) {
                                    e.printStackTrace();
                                }
                                loading.stop();
                                loading.setVisibility(View.GONE);
                                defaultView.setVisibility(View.VISIBLE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, T model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                loading.stop();
                                loading.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE);

                // If download or get option error we not set override
                if (isTrue && overrideW > 0 && overrideH > 0) {
                    builder = builder.override(overrideW, overrideH).fitCenter();
                }

                builder.into(previewView);
            }
        });
    }

    private <T> void loadImageDoDownAndGetOverrideSize(final T urlOrPath, final OriginalContract.DoOverrideSizeCallback callback) {
        // In this save max image size is source
        final Future<File> future = Glide.with(activity)
                .load(urlOrPath)
                .downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);

        AppOperator.runOnThread(new Runnable() {
            @Override
            public void run() {
                try {
                    File sourceFile = future.get();

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    // First decode with inJustDecodeBounds=true to checkShare dimensions
                    options.inJustDecodeBounds = true;
                    // First decode with inJustDecodeBounds=true to checkShare dimensions
                    BitmapFactory.decodeFile(sourceFile.getAbsolutePath(), options);

                    int width = options.outWidth;
                    int height = options.outHeight;

                    if (width > 0 && height > 0) {
                        // Get Screen
                        final Point point = getDisplayDimens();

                        // This max size
                        final int maxLen = Math.min(Math.min(point.y, point.x) * 5, 1366 * 3);

                        // Init override size
                        final int overrideW, overrideH;

                        if ((width / (float) height) > (point.x / (float) point.y)) {
                            overrideH = Math.min(height, point.y);
                            overrideW = Math.min(width, maxLen);
                        } else {
                            overrideW = Math.min(width, point.x);
                            overrideH = Math.min(height, maxLen);
                        }

                        // Call back on main thread
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                callback.onDone(overrideW, overrideH, true);
                            }
                        });
                    } else {
                        // Call back on main thread
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                callback.onDone(0, 0, false);
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();

                    // Call back on main thread
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            callback.onDone(0, 0, false);
                        }
                    });
                }
            }
        });
    }

    private synchronized Point getDisplayDimens() {
        if (mDisplayDimens != null) {
            return mDisplayDimens;
        }
        Point displayDimens;
        WindowManager windowManager = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            displayDimens = new Point();
            display.getSize(displayDimens);
        } else {
            displayDimens = new Point(display.getWidth(), display.getHeight());
        }

        mDisplayDimens = displayDimens;
        return mDisplayDimens;
    }


}
