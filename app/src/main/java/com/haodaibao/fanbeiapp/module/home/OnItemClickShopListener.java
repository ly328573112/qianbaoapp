package com.haodaibao.fanbeiapp.module.home;


public interface OnItemClickShopListener {
    void onItemClick(int position);
}
