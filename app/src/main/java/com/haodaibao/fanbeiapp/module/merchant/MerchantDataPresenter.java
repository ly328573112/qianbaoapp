package com.haodaibao.fanbeiapp.module.merchant;

import android.text.TextUtils;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.FormatUtil;
import com.baseandroid.utils.LogUtil;
import com.baseandroid.utils.RxUtils;
import com.baseandroid.utils.ToastUtils;
import com.haodaibao.fanbeiapp.repository.LifeRepository;
import com.haodaibao.fanbeiapp.repository.MerchantRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.CommentListBean;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.ErrorRecoveryList;
import com.haodaibao.fanbeiapp.repository.json.MerchantDetailResp;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoResp;
import com.haodaibao.fanbeiapp.repository.json.PackageListBean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static com.haodaibao.fanbeiapp.repository.MerchantRepository.getInstance;

/**
 * @author LuoYi
 *         Time: 2017 15:52 2017/9/4 09
 */
class MerchantDataPresenter implements MerchantContract.Presenter {

    MerchantContract.MerchantView merchantView;

    MerchantDataPresenter(MerchantContract.MerchantView merchantView) {
        this.merchantView = merchantView;
    }


    @Override
    public Map<String, String> getRNamount(MerchantInfoEntiy merchantInfo) {
        Map<String, String> map = new HashMap<>(16);
        String refundmode = merchantInfo.getRefundmode();
        String returnbeginamount = merchantInfo.getReturnbeginamount();
        String returnrate = merchantInfo.getReturnrate();
        map.put("refundmode", FormatUtil.setdicountstyle(refundmode, returnbeginamount, returnrate));
        //商户优惠：折、减
        //满减1
        String fanxianNearBy1 = FormatUtil.setDiscountContent(refundmode, returnbeginamount, returnrate);
        //满减2
        String fanxianNearBy2 = FormatUtil.setDiscountContent(refundmode, merchantInfo.getReturnbegin2amount(), merchantInfo.getReturnrate2());
        //商户活动：惠
//        String activityInfo = FormatUtil.setActivityInfo(merchantInfo.getLabels());

        //页面设置优惠+活动信息
        /*设置第一种优惠*/
        StringBuilder sb = new StringBuilder();
        sb.append(fanxianNearBy1);
        //拼接第二种优惠
        if (!TextUtils.isEmpty(fanxianNearBy2)) {
            if (!TextUtils.isEmpty(fanxianNearBy1)) {
                sb.append("<br>");
            }
            sb.append(fanxianNearBy2);
        }
        /*拼接活动*/
//        if (!TextUtils.isEmpty(activityInfo)) {
//            if (!TextUtils.isEmpty(fanxianNearBy1)) {
//                sb.append("<br>");
//            }
//            sb.append(activityInfo);
//        }
        if (TextUtils.isEmpty(sb.toString())) {
            sb.append("去买单");
        }
        map.put("discount_title", sb.toString());
        return map;
    }

    @Override
    public void getMerchantDetail(String merchantno) {
        HashMap<String, String> merchantMap = new HashMap<>();
        merchantMap.put("merchantno", merchantno);
        merchantMap.put("citycode", "310000");
        getInstance().getMerchantDetails(merchantMap, true)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<MerchantDetailResp>>applySchedulersLifeCycle((BaseActivity) merchantView))
                .subscribe(new RxObserver<Data<MerchantDetailResp>>() {

                    @Override
                    public void onNext(@NonNull Data<MerchantDetailResp> merchantDetailRespData) {
                        super.onNext(merchantDetailRespData);
                        if (RxObserver.checkJsonCode(merchantDetailRespData, true) && merchantDetailRespData.getResult() != null) {
                            MerchantDetailResp detailResp = merchantDetailRespData.getResult();
                            List<PackageListBean> packageList = detailResp.getPackageLists();
                            merchantView.onMerchantDetail(detailResp.getAttention(), detailResp.getMerchant(), detailResp.getPeriods(), detailResp.getPriedList(), packageList);
                        } else {
                            merchantView.closeActivity();
                        }
//                        if (merchantDetailRespData.getResult() == null) {
//                            merchantView.closeActivity();
//                            return;
//                        }

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                        merchantView.onFail();
                        merchantView.onRetry();
                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();
                        merchantView.onContent();
                    }
                });
    }

    @Override
    public void followMerchant(String merchantno, boolean isChecked) {
        HashMap<String, String> merchantMap = new HashMap<>();
        merchantMap.put("interestno", merchantno);
        merchantMap.put("pmtype", "1");// 1商户，2商品
        merchantMap.put("attentiontype", isChecked ? "1" : "0");// 0添加关注，1取消关注
        getInstance().followMerchant(merchantMap)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data>applySchedulersLifeCycle((BaseActivity) merchantView))
                .subscribe(new RxObserver<Data>() {
                    @Override
                    public void onNext(@NonNull Data stringData) {
                        super.onNext(stringData);
                        merchantView.onFollow(stringData.getStatus());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                        merchantView.onFail();
                    }

                });
    }

    @Override
    public void getCommentNearbyList(String merchantno, String pageNumber, String longitude, String latitude, String cityCode) {
        HashMap<String, String> merchantMap = new HashMap<>(16);
        merchantMap.put("merchantno", merchantno);
        Observable<Data<CommentListBean>> commentObserver = MerchantRepository.getInstance().getCommentList(merchantMap, true)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Consumer<Data<CommentListBean>>() {
                    @Override
                    public void accept(@NonNull Data<CommentListBean> commentListBeanData) throws Exception {
                        if (RxObserver.checkJsonCode(commentListBeanData, true)) {
                            merchantView.onCommentList(commentListBeanData.getResult(), false);
                        }
                    }
                });

        HashMap<String, String> shanghuMap = new HashMap<>();
        shanghuMap.put("page", "1");
        shanghuMap.put("pageSize", pageNumber);
        shanghuMap.put("longitude", longitude);// 经度
        shanghuMap.put("latitude", latitude);// 纬度
        shanghuMap.put("sort", "distance.asc");// 排序条件
        if (!TextUtils.isEmpty(cityCode)) {// 城市编码
            shanghuMap.put("citycode", cityCode);// 城市编码
        }
        Observable<Data<MerchantInfoResp>> shanghuObserver = LifeRepository.getInstance()
                .getShanghuListInfo(shanghuMap, false)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Consumer<Data<MerchantInfoResp>>() {
                    @Override
                    public void accept(@NonNull Data<MerchantInfoResp> data) throws Exception {
                        if (RxObserver.checkJsonCode(data, true)) {
                            merchantView.onNearby(data.getResult());
                        }
                    }
                });

        Observable.concat(commentObserver, shanghuObserver)
                .subscribe(new RxObserver<Data>() {
                    @Override
                    public void onComplete() {
                        merchantView.onRefreshList();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                    }
                });
    }

    @Override
    public void getCommentL(String merchantno, final boolean isComment) {
        HashMap<String, String> merchantMap = new HashMap<>(16);
        merchantMap.put("merchantno", merchantno);
        getInstance().getCommentList(merchantMap, true)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<CommentListBean>>applySchedulersLifeCycle((BaseActivity) merchantView))
                .subscribe(new RxObserver<Data<CommentListBean>>() {
                    @Override
                    public void onNext(@NonNull Data<CommentListBean> commentListBeanData) {
                        super.onNext(commentListBeanData);
                        if (RxObserver.checkJsonCode(commentListBeanData, true)) {
                            merchantView.onCommentList(commentListBeanData.getResult(), isComment);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                        merchantView.onFail();
                    }

                });
    }

    @Override
    public void getErrorRecoveryList() {
        HashMap<String, String> merchantMap = new HashMap<>();
        getInstance().getErrorRecoveryList(merchantMap)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<ErrorRecoveryList>>applySchedulersLifeCycle((BaseActivity) merchantView))
                .subscribe(new RxObserver<Data<ErrorRecoveryList>>() {
                    @Override
                    public void onNext(@NonNull Data<ErrorRecoveryList> errorData) {
                        super.onNext(errorData);
                        if (RxObserver.checkJsonCode(errorData, true)) {
                            merchantView.onErrorRecovery(errorData.getResult());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                        LogUtil.e(e);
                    }

                });
    }

    @Override
    public void merchantErrorRecovery(String merchantno, String errorCode) {
        HashMap<String, String> merchantMap = new HashMap<>();
        merchantMap.put("merchantno", merchantno);
        merchantMap.put("errorno", errorCode);
        getInstance().merchantErrorRecovery(merchantMap)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data>applySchedulersLifeCycle((BaseActivity) merchantView))
                .subscribe(new RxObserver<Data>() {
                    @Override
                    public void onNext(@NonNull Data errorData) {
                        super.onNext(errorData);
                        if (RxObserver.checkJsonCode(errorData, true)) {
                            merchantView.onErrorCommint(errorData.getStatus());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                        LogUtil.e(e);
                        ToastUtils.showLongToast("提交失败");
                    }

                });
    }
}
