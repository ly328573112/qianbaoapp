//package com.haodaibao.fanbeiapp.module.bizutils;
//
//import android.text.Html;
//import android.text.Spanned;
//import android.text.TextUtils;
//
//import com.baseandroid.config.Global;
//import com.baseandroid.utils.CommonUtils;
//import com.baseandroid.utils.CountUtil;
//import com.baseandroid.utils.FormatUtil;
//import com.haodaibao.fanbeiapp.R;
//import com.haodaibao.fanbeiapp.repository.json.LableInfo;
//import com.haodaibao.fanbeiapp.repository.json.ShanghufenleiList;
//
//import java.math.BigDecimal;
//import java.text.DecimalFormat;
//import java.util.List;
//
//public class ShanghuInfoDic {
//    public static class Category {
//        public static String categoryno;// 商户编号
//        public static String categoryname;// 商户名称
//    }
//
//    /* 商户排序 */
//    public static class CategoryAsc {
//        public static String DISTANCEASC = "distance.asc";
//        public static String STARDESC = "star.desc";
//        public static String CREATETIMEDESC = "createtime.desc";
//        public static String ATTENTIONDESC = "attention.desc";
//        public static String PERONAVERAGEDESC = "personaverage.desc";
//        public static String PERSONAVERAGEASC = "personaverage.asc";
//
//        public static String CURRDISCOUNT = "currdiscount.desc";
//        public static String PERSONAVERAGE = "personaverage.desc";
//
//        public static String[] strings = {DISTANCEASC, CURRDISCOUNT, DISTANCEASC, PERSONAVERAGEASC, PERONAVERAGEDESC, CREATETIMEDESC};
//        // public static String[] names =
//        // {"离我最近","评价最高","最新发布","人气最高","价格最高","价格最低"};
//        // public static String[] ids = {"1","2","3","4","5","6"};
//        //        public static String[] names = {"离我最近", "人均最低", "最新发布"};
//        //        public static String[] ids = {"1", "2", "3"};
//        public static String[] names = {"智能排序", "优惠力度最大", "离我最近", "人均价格最低", "人均价格最高", "最新上线"};
//        public static String[] ids = {"1", "2", "3", "4", "5", "6"};
//
//        /**
//         * 商户排序条件
//         * @param name 中文名
//         * @return id
//         */
//        public static String getCategoryAscFromName(String name) {
//            String a = "";
//            for (int i = 0; i < names.length; i++) {
//                if (name.equals(names[i])) {
//                    a = strings[i];
//                }
//            }
//            return a;
//        }
//    }
//
//    /* 商户排序 去除离我最近*/
//    public static class CategoryAsc1 {
//        public static String DISTANCEASC = "distance.asc";
//        public static String STARDESC = "star.desc";
//        public static String CREATETIMEDESC = "createtime.desc";
//        public static String ATTENTIONDESC = "attention.desc";
//        public static String PERONAVERAGEDESC = "personaverage.desc";
//        public static String PERSONAVERAGEASC = "personaverage.asc";
//
//        public static String CURRDISCOUNT = "currdiscount.desc";
//        public static String PERSONAVERAGE = "personaverage.desc";
//
//        public static String[] strings = {DISTANCEASC, CURRDISCOUNT, PERSONAVERAGEASC, PERONAVERAGEDESC, CREATETIMEDESC};
//        // public static String[] names =
//        // {"离我最近","评价最高","最新发布","人气最高","价格最高","价格最低"};
//        // public static String[] ids = {"1","2","3","4","5","6"};
//        //        public static String[] names = {"离我最近", "人均最低", "最新发布"};
//        //        public static String[] ids = {"1", "2", "3"};
//        public static String[] names = {"智能排序", "优惠力度最大", "人均价格最低", "人均价格最高", "最新上线"};
//        public static String[] ids = {"1", "2", "3", "4", "5"};
//
//        /**
//         * 商户排序条件
//         *
//         * @param name 中文名
//         * @return id
//         */
//        public static String getCategoryAscFromName(String name) {
//            String a = "";
//            for (int i = 0; i < names.length; i++) {
//                if (name.equals(names[i])) {
//                    a = strings[i];
//                }
//            }
//            return a;
//        }
//    }
//
//    /* 商户筛选 */
//    public static class CategoryShaixuan {
//        public static String REFUNDMODE = "0";// 立减商户
//        public static String ISPRODUCT = "10";// 优惠套餐
//        public static String ISVOUCHER = "1";// 优惠券
//        public static String[] strings = {REFUNDMODE, ISPRODUCT, ISVOUCHER};
//        public static String[] names = {"优惠商户", "优惠套餐", "优惠券"};
//        public static String[] ids = {"1", "2", "3"};
//
//        /**
//         * 商户筛选条件
//         *
//         * @param name 中文名
//         * @return id
//         */
//        public static String getCategoryAscFromName(String name) {
//            String a = "";
//            for (int i = 0; i < names.length; i++) {
//                if (name.equals(names[i])) {
//                    a = strings[i];
//                }
//            }
//            return a;
//        }
//    }
//
//    /* 商户筛选 */
//    public static class CategoryFenlei {
//        private static String a = "";
//
//        // 获取商户筛选条件
//        public static String getCategoryParentName(String parentno) {
//            List<ShanghufenleiList> list = Global.getCategorys();
//            for (int i = 0; i < list.size(); i++) {
//                if (parentno.equals(list.get(i).getCategoryno())) {
//                    a = list.get(i).getName();
//                }
//            }
//            return a;
//        }
//    }
//
//    /**
//     * 商户返现模式
//     */
//    public static class RefundMode {
//        public static String NONE = "0";// 无优惠
//        public static String ALL = "1";// 立减N%
//        public static String FULL = "2";// 满M立减N
//        public static String PERFULL = "3";// 每满M立减N
//        private static RefundMode refundMode = null;
//
//        public static RefundMode getInstance() {
//            if (refundMode == null) {
//                refundMode = new RefundMode();
//            }
//            return refundMode;
//        }
//
//        /**
//         * 显示优惠信息
//         *
//         * @param refundMode
//         * @param returnBeginAmount
//         * @param returnRate
//         * @return if(refundMode = 1)立减returnRate% if(refundMode = 2)满returnBeginAmount立减returnRate
//         * if(refundMode = 3)每满returnBeginAmount立减returnRate
//         */
//        public Spanned setFanxian(String refundMode, String returnBeginAmount, String returnRate) {
//            String string = "";
//            if (TextUtils.isEmpty(refundMode) && TextUtils.isEmpty(returnBeginAmount) && TextUtils
//                    .isEmpty(returnRate)) {
//                return Html.fromHtml(string);
//            }
//            returnBeginAmount = FormatUtil.formatAmount(0, returnBeginAmount);
//            if (refundMode.equals(NONE)) {
//
//            } else if (refundMode.equals(ALL)) {
//                if (!TextUtils.isEmpty(returnRate)) {
//                    returnRate = FormatUtil.formatAmount(1, "" + (1 - Double.valueOf(returnRate)) * 10);
//                    if (0 < Double.parseDouble(returnRate)) {
//                        string = "买单<big><big><font color='#F62241'>" + returnRate + "</font></big></big>" + "折";
//                    }
//                }
//
//            } else if (refundMode.equals(FULL)) {
////                returnRate = FormatUtil.formatAmount(0, returnRate);
//                DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
//                returnRate = decimalFormat.format(new BigDecimal(returnRate));
//                string = "满" + returnBeginAmount + "减" + "<big><big><font color='#F62241'>" + returnRate + "</font></big></big>" + "元";
//            } else if (refundMode.equals(PERFULL)) {
//                DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
//                returnRate = decimalFormat.format(new BigDecimal(returnRate));
////                returnRate = FormatUtil.formatAmount(0, returnRate);
//                string = "每满" + returnBeginAmount + "减" + "<big><big><font color='#F62241'>" + returnRate + "</font></big></big>" + "元";
//            }
//            return Html.fromHtml(string);
//        }
//
//        /**
//         * 拼接优惠活动信息
//         */
//        public String setActivityInfo(List<LableInfo> lableList) {
//            StringBuilder builder = new StringBuilder();
//            for (LableInfo lableInfo : lableList) {
//                //图标
//                String lableIcon = CommonUtils.getImageUrl(lableInfo.getLabelicon());
//                String html_hui = "<img src='" + lableIcon + "'/>";
//                //活动名称
//                String labelRemark = lableInfo.getLabelremark();
//
//                String lableStr = html_hui + " " + labelRemark;
//                builder.append(lableStr);
//                builder.append("<br>");
//            }
//            String s = builder.toString();
//            if (s.contains("<br>")) {
//                return s.substring(0, s.lastIndexOf("<br>"));
//            } else {
//                return s;
//            }
//
//
//        }
//
//        /**
//         * 显示优惠信息,附近商户列表活动信息
//         *
//         * @param refundMode
//         * @param returnBeginAmount
//         * @param returnRate
//         * @return if(refundMode = 1)立减returnRate% if(refundMode = 2)满returnBeginAmount立减returnRate
//         * if(refundMode = 3)每满returnBeginAmount立减returnRate
//         */
//        public String setFanxianNearBy(String refundMode, String returnBeginAmount, String returnRate) {
//            String html_jian = "<img src='" + R.drawable.icon_jian + "'/>";
//            String html_zhe = "<img src='" + R.drawable.icon_zhe + "'/>";
//
//            String string = "";
//            if (TextUtils.isEmpty(returnBeginAmount) && TextUtils.isEmpty(returnRate)) {
//                return string;
//            }
//            returnBeginAmount = FormatUtil.formatAmount(0, returnBeginAmount);
//            if (refundMode.equals(NONE)) {
//
//            } else if (refundMode.equals(ALL)) {
//                if (!TextUtils.isEmpty(returnRate)) {
//                    returnRate = FormatUtil.formatAmount(1, "" + (1 - Double.valueOf(returnRate)) * 10);
//                    if (0 < Double.parseDouble(returnRate)) {
//                        string = html_zhe + " 在线支付优惠买单<big><font color='#f94e4e'>" + returnRate + "折</font></big>";
//                    }
//                }
//
//            } else if (refundMode.equals(FULL)) {
////                returnRate = FormatUtil.formatAmount(0, returnRate);
//                DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
//                returnRate = decimalFormat.format(new BigDecimal(returnRate));
//                string = html_jian + " 满" + returnBeginAmount + "减" + "<big><font color='#f94e4e'>" + returnRate + "元</font></big>";
//            } else if (refundMode.equals(PERFULL)) {
////                returnRate = FormatUtil.formatAmount(0, returnRate);
//                DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
//                returnRate = decimalFormat.format(new BigDecimal(returnRate));
//                string = html_jian + " 每满" + returnBeginAmount + "减" + "<big><font color='#f94e4e'>" + returnRate + "元</font></big>";
//            }
//            return string;
//        }
//
//        /**
//         * 显示优惠信息,附近商户列表活动信息
//         *
//         * @param refundMode
//         * @param returnBeginAmount
//         * @param returnRate
//         * @return if(refundMode = 1)立减returnRate% if(refundMode = 2)满returnBeginAmount立减returnRate
//         * if(refundMode = 3)每满returnBeginAmount立减returnRate
//         */
//        public String setDiscountContent(String refundMode, String returnBeginAmount, String returnRate) {
//
//            String string = "";
//            if (TextUtils.isEmpty(returnBeginAmount) && TextUtils.isEmpty(returnRate)) {
//                return string;
//            }
//            returnBeginAmount = FormatUtil.formatAmount(0, returnBeginAmount);
//            if (refundMode.equals(NONE)) {
//
//            } else if (refundMode.equals(ALL)) {
//                if (!TextUtils.isEmpty(returnRate)) {
//                    returnRate = FormatUtil.formatAmount(1, "" + (1 - Double.valueOf(returnRate)) * 10);
//                    if (0 < Double.parseDouble(returnRate)) {
//                        //                        string = "优惠买单<center><big><big>" + returnRate + "</big></big></center>折";
//                        string = "优惠买单<center><big><big><font color='#F62241'>" + returnRate + "</font></big></big></center>折";
//                    }
//                }
//
//            } else if (refundMode.equals(FULL)) {
////                returnRate = FormatUtil.formatAmount(0, returnRate);
//                DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
//                returnRate = decimalFormat.format(new BigDecimal(returnRate));
//                string = " 满" + returnBeginAmount + "减" + "<center><big><big><font color='#F62241'>" + returnRate + "</font></big></big></center>元";
//            } else if (refundMode.equals(PERFULL)) {
////                returnRate = FormatUtil.formatAmount(0, returnRate);
//                DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
//                returnRate = decimalFormat.format(new BigDecimal(returnRate));
//                string = "每满" + returnBeginAmount + "减" + "<center><big><big><font color='#F62241'>" + returnRate + "</font></big></big></center>元";
//            }
//            return string;
//        }
//
//        /**
//         * 显示优惠信息,全单打折或者全单满减
//         */
//        public String setdicountstyle(String refundMode, String returnBeginAmount, String returnRate) {
//            String string = "";
//            if (TextUtils.isEmpty(returnBeginAmount) && TextUtils.isEmpty(returnRate)) {
//                return string;
//            }
//            if (refundMode.equals(NONE)) {
//                string = "无优惠";
//            } else if (refundMode.equals(ALL)) {
//                string = "全单打折";
//            } else if (refundMode.equals(FULL)) {
//                string = "全单满减";
//            } else if (refundMode.equals(PERFULL)) {
//                string = "全单满减";
//            }
//            return string;
//        }
//
//        /**
//         * 显示优惠信息,不包含样式
//         */
//        public String setFanxianNoStyle(String refundMode, String returnBeginAmount, String returnRate) {
//            String string = "";
//            if (TextUtils.isEmpty(refundMode) && TextUtils.isEmpty(returnBeginAmount) && TextUtils
//                    .isEmpty(returnRate)) {
//                return string;
//            }
//
//            returnBeginAmount = FormatUtil.formatAmount(0, returnBeginAmount);
//            if (refundMode.equals(NONE)) {
//
//            } else if (refundMode.equals(ALL)) {
//                if (!TextUtils.isEmpty(returnRate)) {
//                    returnRate = FormatUtil.formatAmount(1, "" + (1 - Double.valueOf(returnRate)) * 10);
//                    if (0 < Double.parseDouble(returnRate)) {
//                        string = returnRate + "折";
//                    }
//                }
//
//
//            } else if (refundMode.equals(FULL)) {
//                returnRate = FormatUtil.formatAmount(0, returnRate);
//                string = "满" + returnBeginAmount + "减" + returnRate + "元";
//            } else if (refundMode.equals(PERFULL)) {
//                returnRate = FormatUtil.formatAmount(0, returnRate);
//                string = "每满" + returnBeginAmount + "减" + returnRate + "元";
//            }
//            return string;
//        }
//
//
//        /**
//         * 获取优惠金额
//         *
//         * @param refundMode        立减模式
//         * @param returnBeginAmount 立减起始金额
//         * @param returnRate        if(refundMode = 1)立减returnRate% if(refundMode =
//         *                          2)满returnBeginAmount立减returnRate if(refundMode = 3)每满returnBeginAmount立减returnRate
//         * @param topamount         最高优惠金额
//         * @param inReduceamount    参与优惠金额
//         * @return 参与优惠金额
//         */
//        public double getDiscountMoney(String refundMode, String returnBeginAmount, String returnRate, String returnBeginAmount2, String returnRate2, String topamount, double inReduceamount) {
//            double doubleNumber = Double.valueOf(FormatUtil.formatAmount(2, "0"));
//            double returnRateNum = Double.valueOf(FormatUtil.formatAmount(2, returnRate));
//            double topamountNum = Double.valueOf(FormatUtil.formatAmount(2, topamount));
//            double beginAmountNum = Double.valueOf(FormatUtil.formatAmount(2, returnBeginAmount));
//            //满减会出现以下两个变量
//            double returnRateNum2 = Double.valueOf(FormatUtil.formatAmount(2, returnRate2));
//            double beginAmountNum2 = Double.valueOf(FormatUtil.formatAmount(2, returnBeginAmount2));
//            //参与优惠金额为0
//            if (inReduceamount == 0) {
//                return doubleNumber;
//            }
//            //参与优惠金额不为0，按优惠模式计算
//            if (TextUtils.isEmpty(refundMode)) {
//
//            } else if (refundMode.equals(NONE)) {
//
//            } else if (refundMode.equals(ALL)) {
//                //满足优惠要求
//                if (inReduceamount >= beginAmountNum) {
//                    doubleNumber = CountUtil.mul(inReduceamount, returnRateNum);
//                }
//            } else if (refundMode.equals(FULL)) {//大于最大值取最大，位于之间取最小
//                if (beginAmountNum >= beginAmountNum2) {
//                    if (inReduceamount >= beginAmountNum) {
//                        doubleNumber = returnRateNum;
//                    } else if ((inReduceamount < beginAmountNum) && (inReduceamount >= beginAmountNum2)) {
//                        doubleNumber = returnRateNum2;
//                    }
//                } else {
//                    if (inReduceamount >= beginAmountNum2) {
//                        doubleNumber = returnRateNum2;
//                    } else if ((inReduceamount < beginAmountNum2) && (inReduceamount >= beginAmountNum)) {
//                        doubleNumber = returnRateNum;
//                    }
//                }
//                //满足优惠要求
//                if (inReduceamount >= beginAmountNum) {
//                    doubleNumber = returnRateNum;
//                }
//            } else if (refundMode.equals(PERFULL)) {
//                //（输入金额 / 每满金额 = 有几个每满金额） * 优惠金额
//                doubleNumber = ((int) (inReduceamount / beginAmountNum)) * returnRateNum;
//            }
//            //优惠金额>=最高优惠金额，取最高优惠金额
//            if (topamountNum > 0 && (doubleNumber >= topamountNum)) {
//                doubleNumber = topamountNum;
//            }
//            return doubleNumber;
//        }
//
//        /**
//         * @param topamount
//         * @param returnbeginamount
//         * @param returnmode
//         * @return
//         */
//        public String getDiscountData(String topamount, String returnbeginamount, String returnmode) {
//            String duscount = "";
//            DecimalFormat df = new DecimalFormat("0");
//            Double topamountDouble = Double.parseDouble(TextUtils.isEmpty(topamount) ? "0" : topamount);
//            Double returnbeginamountDouble = Double.parseDouble(TextUtils.isEmpty(returnbeginamount) ? "0" : returnbeginamount);
//            if (TextUtils.equals(returnmode, RefundMode.ALL) || TextUtils.equals(returnmode, RefundMode.FULL) || TextUtils
//                    .equals(returnmode, RefundMode.PERFULL)) {
//                if (!TextUtils.isEmpty(topamount) && topamountDouble != 0 && !TextUtils.isEmpty(returnbeginamount) && returnbeginamountDouble != 0 && topamountDouble != 99999) {
//                    duscount = "最低消费" + df.format(returnbeginamountDouble) + "元  " + "最高优惠" + df.format(topamountDouble) + "元";
//                } else if (!TextUtils.isEmpty(returnbeginamount) && returnbeginamountDouble != 0) {
//                    duscount = "最低消费" + df.format(returnbeginamountDouble) + "元";
//                } else if (!TextUtils.isEmpty(topamount) && topamountDouble != 0 && topamountDouble != 99999) {
//                    duscount = "最高优惠" + df.format(topamountDouble) + "元";
//                }
//            }
//            return duscount;
//        }
//
//        /**
//         * 优惠金额
//         *
//         * @param cashamt  付款金额
//         * @param orderamt 实际金额
//         * @return
//         */
//        public String getPreferentialAmount(String cashamt, String orderamt) {
//            double cashamtDou = Double.valueOf(FormatUtil.formatAmount(2, cashamt));
//            double orderamtDou = Double.valueOf(FormatUtil.formatAmount(2, orderamt));
//            double sub = CountUtil.sub(orderamtDou, cashamtDou);
//            return sub > 0 ? "厉害啦~省了<big><big>" + FormatUtil.filterDecimalPoint(sub + "") + "</big></big>元哦！" : "消费多多，优惠多多~";
//        }
//    }
//}