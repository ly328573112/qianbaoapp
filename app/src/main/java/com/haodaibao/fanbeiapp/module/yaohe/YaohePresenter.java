package com.haodaibao.fanbeiapp.module.yaohe;

import com.baseandroid.base.BaseActivity;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.YaoheRepository;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.YaoheDetailsBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by hdb on 2017/7/31.
 */

public class YaohePresenter implements YaoheContract.Presenter {
    private YaoheContract.IYaoheView iYaoheView;

    public YaohePresenter(YaoheContract.IYaoheView iYaoheView) {
        this.iYaoheView = iYaoheView;
    }

    @Override
    public void getYaoheDetails(String messageNo) {
        HashMap<String, String> map = new HashMap<>();
        map.put("messageNo", messageNo);
        YaoheRepository.getInstance().getYaoheDetails(map).map(new Function<Data<YaoheDetailsBean>, List<YaoheDetailsBean.MessageBody>>() {

            @Override
            public List<YaoheDetailsBean.MessageBody> apply(@NonNull Data<YaoheDetailsBean> yaoheDetailsBeanData) throws Exception {
                List<YaoheDetailsBean.MessageBody> list = new ArrayList<>();
                if (RxObserver.checkJsonCode(yaoheDetailsBeanData, true)) {
                    YaoheDetailsBean.MessageBody messageBody = new YaoheDetailsBean.MessageBody();
                    messageBody.setContent(yaoheDetailsBeanData.getResult().getTitle());
                    messageBody.setType("TITLE");
                    messageBody.setTime(yaoheDetailsBeanData.getResult().getSendendtime());
                    messageBody.setMerchantName(yaoheDetailsBeanData.getResult().getMerName());
                    messageBody.setMerNo(yaoheDetailsBeanData.getResult().getMerNo());
                    list.add(messageBody);
                    list.addAll(yaoheDetailsBeanData.getResult().getItems());
                }
                return list;
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(((BaseActivity) iYaoheView).<List<YaoheDetailsBean.MessageBody>>bindToLifecycle())
                .subscribe(new RxObserver<List<YaoheDetailsBean.MessageBody>>() {
                    @Override
                    public void onNext(@NonNull List<YaoheDetailsBean.MessageBody> messageBodies) {
                        super.onNext(messageBodies);
                        if (RxObserver.checkJsonCode(messageBodies, false)) {
                            return;
                        }
                        iYaoheView.updateDetails(messageBodies);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                        iYaoheView.onFail(e.getMessage());
                    }
                });
    }

}
