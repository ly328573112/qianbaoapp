package com.haodaibao.fanbeiapp.module.home.temporary;


import com.haodaibao.fanbeiapp.module.merchant.temporarydata.MerchantTemporary;
import com.haodaibao.fanbeiapp.repository.json.LableInfo;

import java.util.List;

/**
 * 开发者：LuoYi
 * Time: 2017 14:41 2017/9/12 09
 */

public class HomeTemporary implements MerchantTemporary {

    public String catagoryStr = "";
    public String scopeStr = "";
    public String sortwaysStr = "";
    public String locationStr = "";
    public List<LableInfo> lableList;

    @Override
    public int getmType() {
        return 0;
    }
}