package com.haodaibao.fanbeiapp.module.search.utils;

import com.haodaibao.fanbeiapp.repository.json.OpenCityBean;

import java.util.Comparator;

public class PinyinComparator implements Comparator<OpenCityBean> {

    public int compare(OpenCityBean o1, OpenCityBean o2) {
        if (o1.getSortLetters().equals("@") || o2.getSortLetters().equals("#")) {
            return -1;
        } else if (o1.getSortLetters().equals("#") || o2.getSortLetters().equals("@")) {
            return 1;
        } else {
            return o1.getSortLetters().compareTo(o2.getSortLetters());
        }
    }

}
