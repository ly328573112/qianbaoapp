package com.haodaibao.fanbeiapp.module.maphome;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.baseandroid.utils.CommonUtils;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.PriedListBean;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 开发者：LuoYi
 * Time: 2017 11:23 2017/10/10 10
 */

public class MapMerchantDiscountAdapter extends BaseRecycleViewAdapter<PriedListBean> {
    private OnItemClickListener onItemClickListener;
    private View view;

    public MapMerchantDiscountAdapter(Context context, OnItemClickListener onItemClickListener, View view) {
        super(context);
        this.onItemClickListener = onItemClickListener;
        this.view = view;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, final PriedListBean item) {
        DiscountHolder discountHolder = (DiscountHolder) holder;
        if (getItemCount() == 1) {
            discountHolder.view_discount.setVisibility(View.GONE);
        } else {
            discountHolder.view_discount.setVisibility(View.VISIBLE);
        }
//        holder.itemView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.bg_grayf9));
        discountHolder.tv_new_user_lable.setVisibility(TextUtils.equals(item.adaptType, "1") ? View.VISIBLE : View.GONE);
        discountHolder.tv_discount_type.setTextColor(mContext.getResources().getColor(R.color.c_252628));
        discountHolder.tv_new_user_lable.setTextColor(mContext.getResources().getColor(R.color.red));
        discountHolder.iv_discount_icon.setVisibility(View.VISIBLE);
        discountHolder.tv_discount_type.setText(item.refundContent);
        discountHolder.tv_discount_time.setText(item.dateStr);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                onItemClickListener.onItemClick(v, item);
            }
        });
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new DiscountHolder(LayoutInflater.from(mContext).inflate(R.layout.layout_maphome_merchant_discount, parent, false));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.bg_grayf9));
        return 0;
    }

    class DiscountHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_discount_icon)
        ImageView iv_discount_icon;
        @BindView(R.id.tv_new_user_lable)
        TextView tv_new_user_lable;
        @BindView(R.id.tv_discount_type)
        TextView tv_discount_type;
        @BindView(R.id.tv_discount_time)
        TextView tv_discount_time;
        @BindView(R.id.view_discount)
        View view_discount;

        public DiscountHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
