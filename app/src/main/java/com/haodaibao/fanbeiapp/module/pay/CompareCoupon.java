package com.haodaibao.fanbeiapp.module.pay;

import android.text.TextUtils;

import com.haodaibao.fanbeiapp.repository.json.CouponInfo;

import java.util.Comparator;

/**
 * Created by hdb on 2017/8/9.
 */

public class CompareCoupon implements Comparator<CouponInfo> {
    @Override
    public int compare(CouponInfo o1, CouponInfo o2) {
        double amt1 = Double.parseDouble(o1.getAmount());
        double amt2 = Double.parseDouble(o2.getAmount());
        if (TextUtils.equals("0", o1.getCanUse()) && TextUtils.equals("0", o2.getCanUse())) {
            if (amt1 > amt2) {
                return -1;
            } else if (amt1 == amt2) {
                long d = Long.parseLong(o1.getEndtime());
                long d1 = Long.parseLong(o2.getEndtime());
                Long dd = Long.valueOf(d);
                Long dd1 = Long.valueOf(d1);
                if (dd.longValue() < dd1.longValue()) {
                    return -1;
                } else if (dd.longValue() == dd1.longValue()) {
                    return 0;
                } else {
                    return 1;
                }
            } else {
                return 1;
            }
        } else if (TextUtils.equals("1", o1.getCanUse()) && TextUtils.equals("1", o2.getCanUse())) {
            if (Long.parseLong(o1.getEndtime()) <= Long.parseLong(o2.getEndtime())) {
                return -1;
            } else {
                return 1;
            }

        } else {
            if (TextUtils.equals("0", o1.getCanUse())) {
                return -1;
            } else {
                return 1;
            }
        }
    }
}
