package com.haodaibao.fanbeiapp.module.pay;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.StatusBarHelper;
import com.haodaibao.fanbeiapp.R;

import butterknife.BindView;

/**
 * date: 2017/9/20.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class NoReduceNoticeActivity extends BaseActivity {
    @BindView(R.id.toolbar_title)
    protected TextView toolbar_title;
    @BindView(R.id.toolbar_back)
    protected RelativeLayout toolbar_back;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_noreducenotice;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(NoReduceNoticeActivity.this);
        toolbar_title.setText("不参与优惠金额说明");
        toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                finish();
            }
        });
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {

    }
}
