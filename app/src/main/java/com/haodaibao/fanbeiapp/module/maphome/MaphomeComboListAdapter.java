package com.haodaibao.fanbeiapp.module.maphome;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.baseandroid.utils.CommonUtils;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.PackageListBean;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * date: 2017/10/17.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class MaphomeComboListAdapter extends BaseRecycleViewAdapter<PackageListBean> {
    private OnItemClickListener onItemClickListener;

    public MaphomeComboListAdapter(Context context, OnItemClickListener onItemClickListener) {
        super(context);
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, final PackageListBean item) {
        ComboHold comboHold = (ComboHold) holder;
        StringBuffer packageName = new StringBuffer();
        String faceUserTypeCode = item.getFaceUserTypeCode();
        if (TextUtils.equals("02", faceUserTypeCode)) {
            packageName = packageName.append("[新顾客专享]").append(item.getPackageName());
        } else if (TextUtils.equals("03", faceUserTypeCode)) {
            packageName = packageName.append("[老顾客专享]").append(item.getPackageName());
        } else {
            packageName = packageName.append(item.getPackageName());
        }
        comboHold.comboName.setText(packageName.toString());
        comboHold.comboAmount.setText("¥ " + item.getPreferentialPrice());
        comboHold.comboPrice.setText("¥ " + item.getOriginalprice());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                onItemClickListener.onItemClick(v, item);
            }
        });

    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new ComboHold(LayoutInflater.from(mContext).inflate(R.layout.item_maphome_combolist, parent, false));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class ComboHold extends DefaultViewHolder {
        @BindView(R.id.combo_name)
        TextView comboName;
        @BindView(R.id.combo_amount)
        TextView comboAmount;
        @BindView(R.id.combo_price)
        TextView comboPrice;
        @BindView(R.id.combo_line)
        View comboLine;

        ComboHold(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            comboPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            comboPrice.getPaint().setAntiAlias(true);// 抗锯齿
        }
    }
}
