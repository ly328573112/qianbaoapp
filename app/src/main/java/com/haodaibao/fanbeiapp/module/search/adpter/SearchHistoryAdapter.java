package com.haodaibao.fanbeiapp.module.search.adpter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.SearchCacheEntity;

import java.util.List;

public class SearchHistoryAdapter extends BaseAdapter {

    private Context ctx;
    private List<SearchCacheEntity> mList;
    private SearchHistoryClickIble searchHistoryClick;

    public SearchHistoryAdapter(Context context, List<SearchCacheEntity> list, SearchHistoryClickIble searchHistoryClick) {
        this.ctx = context;
        this.mList = list;
        this.searchHistoryClick = searchHistoryClick;
    }

    public void setSearchList(List<SearchCacheEntity> list, SearchHistoryClickIble searchHistoryClick) {
        this.mList = list;
        this.searchHistoryClick = searchHistoryClick;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mList != null ? mList.size() : 0;
    }

    @Override
    public Object getItem(int arg0) {
        return mList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int pos, View view, ViewGroup arg2) {
        view = LayoutInflater.from(ctx).inflate(R.layout.adapter_search_history, null);

        TextView searchTitleTv = ViewHolder.get(view, R.id.search_title_tv);
        LinearLayout searchContentll = ViewHolder.get(view, R.id.search_content_11);
        if (pos == 0) {
            searchTitleTv.setVisibility(View.VISIBLE);
        } else {
            searchTitleTv.setVisibility(View.GONE);
        }

        TextView searchContentTv = ViewHolder.get(view, R.id.search_content_tv);
        searchContentTv.setText(mList.get(pos).getSearchContent());
        searchContentll.setOnClickListener(new SearchHistoryClick(1, pos));
        return view;
    }

    class SearchHistoryClick implements View.OnClickListener {

        int number;
        int pos;

        public SearchHistoryClick(int number, int pos) {
            this.number = number;
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            switch (number) {
                case 1:
                    if (!isFastDoubleClick()) {
                        searchHistoryClick.clickJump(number, mList.get(pos)
                                .getSearchContent());
                    }
                    break;
                case 2:
                    searchHistoryClick.clickDelete(number, mList.get(pos).getTimeId());
                    break;

                default:
                    break;
            }
        }
    }

    public long lastClickTime = 0;

    public boolean isFastDoubleClick() {
        long time = System.currentTimeMillis();
        long timeD = time - lastClickTime;
        if (timeD >= 0 && timeD <= 300) {
            return true;
        } else {
            lastClickTime = time;
            return false;
        }
    }

    public interface SearchHistoryClickIble {
        // 点击跳转
        void clickJump(int number, String searchName);

        // 点击删除
        void clickDelete(int number, String timeId);
    }

}

