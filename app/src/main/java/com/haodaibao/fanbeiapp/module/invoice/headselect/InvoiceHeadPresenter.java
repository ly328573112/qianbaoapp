package com.haodaibao.fanbeiapp.module.invoice.headselect;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.RxUtils;
import com.haodaibao.fanbeiapp.repository.LifeRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.InvoiceHeadBean;

import java.util.HashMap;

/**
 * date: 2018/2/11.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class InvoiceHeadPresenter implements InvoiceHeadContract.Presenter {
    private InvoiceHeadContract.View mView;

    public InvoiceHeadPresenter(InvoiceHeadContract.View mView) {
        this.mView = mView;
    }

    @Override
    public void getRise(String page, String pageSize) {
        HashMap<String, String> map = new HashMap<>();
        map.put("page", page);
        map.put("pageSize", pageSize);
        LifeRepository.getInstance().getRise(map, true)
                .compose(RxUtils.<Data<InvoiceHeadBean>>applySchedulersLifeCycle((BaseActivity) mView))
                .subscribe(new RxObserver<Data<InvoiceHeadBean>>() {
                    @Override
                    public void onNext(Data<InvoiceHeadBean> invoiceHeadBeanData) {
                        if (checkJsonCode(invoiceHeadBeanData, true)) {
                            mView.updateUI(invoiceHeadBeanData.getResult().list.items);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        mView.updateFailed();
                    }
                });
    }

}
