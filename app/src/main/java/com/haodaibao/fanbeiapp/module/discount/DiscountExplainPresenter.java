package com.haodaibao.fanbeiapp.module.discount;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.RxUtils;
import com.haodaibao.fanbeiapp.repository.MerchantRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.ActivityDetailBean;
import com.haodaibao.fanbeiapp.repository.json.Data;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 开发者：LuoYi
 * Time: 2017 16:20 2017/12/14 12
 */

public class DiscountExplainPresenter implements DiscountExplainContract.Presenter {

    DiscountExplainContract.DiscountView discountView;

    DiscountExplainPresenter(DiscountExplainContract.DiscountView discountView) {
        this.discountView = discountView;
    }

    @Override
    public void getActivityDetail(String merchantno, String activityno) {

        Map<String, String> map = new HashMap<>(16);
        map.put("merchantno", merchantno);
        map.put("activityno", activityno);
        MerchantRepository.getInstance().getActivityDetail(map).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<ActivityDetailBean>>applySchedulersLifeCycle((BaseActivity) discountView))
                .subscribe(new RxObserver<Data<ActivityDetailBean>>() {

                    @Override
                    public void onNext(Data<ActivityDetailBean> activityDetailBeanData) {
                        super.onNext(activityDetailBeanData);
                        if (RxObserver.checkJsonCode(activityDetailBeanData, true)) {
                            ActivityDetailBean result = activityDetailBeanData.getResult();
                            discountView.onDiscountData(result.activity, result.activityAuth);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        discountView.onDiscoutError();
                    }

                });
    }
}
