package com.haodaibao.fanbeiapp.module.merchant;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.TextureMapView;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.amap.api.maps.model.MarkerOptions;
import com.haodaibao.fanbeiapp.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.haodaibao.fanbeiapp.R.id.mark_title;

/**
 * @author LuoYi
 * Time: 2017 17:16 2017/9/7 09
 */
class MerchantMapView implements MerchantContract.MapLocationView {

    private Context context;
    private AMap aMap;

    MerchantMapView(Context context) {
        this.context = context;
    }

    @Override
    public void initMapView(TextureMapView bmapView, String latitude, String longitude, String title) {
        aMap = bmapView.getMap();
        // 自定义系统定位小蓝点
//        setMapCustomStyleFile(context);
        //		setMapCustomTextureFile(this);

        //开启自定义样式
//        aMap.getUiSettings().setTiltGesturesEnabled(false);//禁用双手拖拽3D效果
//        aMap.getUiSettings().setRotateGesturesEnabled(false);
//        aMap.getUiSettings().setScrollGesturesEnabled(false);
        aMap.getUiSettings().setZoomControlsEnabled(false);
        aMap.getUiSettings().setAllGesturesEnabled(false);//禁用所有手势
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        if (!TextUtils.isEmpty(latitude) && !TextUtils.isEmpty(longitude)) {
            LatLng ll = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
            MarkerOptions mo = new MarkerOptions().title(title).position(ll).icon(BitmapDescriptorFactory.fromView(getView(title)));
            aMap.addMarker(mo);
            // 设置所有maker显示在当前可视区域地图中
            builder.include(ll);
            LatLng latLng = new LatLng(Double.parseDouble(latitude) + 0.001, Double.parseDouble(longitude));
            aMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        }
    }

    public View getView(String title) {
        View view = LayoutInflater.from(context).inflate(R.layout.map_infowindow, null);
        TextView markTitle = view.findViewById(mark_title);
        markTitle.setText(title);
        return view;
    }

    private void setMapCustomStyleFile(Context context) {
        //        String styleName = "style_json_texture.json";
        String styleName = "style.data";
        FileOutputStream outputStream = null;
        InputStream inputStream = null;
        String filePath = null;
        try {
            inputStream = context.getAssets().open(styleName);
            byte[] b = new byte[inputStream.available()];
            inputStream.read(b);

            filePath = context.getFilesDir().getAbsolutePath();
            File file = new File(filePath + "/" + styleName);
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            outputStream = new FileOutputStream(file);
            outputStream.write(b);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        aMap.setCustomMapStylePath(filePath + "/" + styleName);

    }
}
