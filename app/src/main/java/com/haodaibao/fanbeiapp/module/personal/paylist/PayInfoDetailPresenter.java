package com.haodaibao.fanbeiapp.module.personal.paylist;

import android.text.TextUtils;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Global;
import com.baseandroid.utils.RxUtils;
import com.haodaibao.fanbeiapp.repository.LifeRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.PayInfoDetailBean;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Routee on 2017/9/11.
 * description: ${cusor}
 */

public class PayInfoDetailPresenter implements PayInfoDetailContract.Presenter {
    private final PayInfoDetailContract.View mView;

    public PayInfoDetailPresenter(PayInfoDetailContract.View view) {
        mView = view;
    }

    @Override
    public void getPayInfoDetailData(String orderNo) {
        Map<String, String> params = new HashMap<>();
        params.put("orderno", orderNo);
        LifeRepository.getInstance().getPayInfoDetail(params).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<PayInfoDetailBean>>applySchedulersLifeCycle((BaseActivity) mView))
                .subscribe(new RxObserver<Data<PayInfoDetailBean>>() {
                    @Override
                    public void onNext(@NonNull Data<PayInfoDetailBean> data) {
                        if (checkJsonCode(data, true)) {
                            mView.setUpPayInfoData(data.getResult());
                        } else {
                            if (TextUtils.equals("MEM0000028", data.getStatus())) {
                                mView.closeUI();
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                        mView.onRetry();
                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();
                        mView.onContent();
                    }
                });
    }

    @Override
    public void setMsgReaded(String msgNo) {
        Map<String, String> params = new HashMap<>();
        params.put("userNo", Global.getUserInfo().getUserno());
        params.put("messageNos", msgNo);
        LifeRepository.getInstance().setMsgReaded(params).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }
}
