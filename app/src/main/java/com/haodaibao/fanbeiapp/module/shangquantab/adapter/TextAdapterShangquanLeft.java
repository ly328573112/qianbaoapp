package com.haodaibao.fanbeiapp.module.shangquantab.adapter;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.DistrictList;

import java.util.List;

public class TextAdapterShangquanLeft extends ArrayAdapter<DistrictList> {

    private Context mContext;
    private List<DistrictList> mListData;
    private DistrictList[] mArrayData;
    private int selectedPos = -1;
    private String selectedText = "";
    private int normalDrawbleId;
    private int selectedTextColor = -1;
    private Drawable selectedDrawble;
    private float textSize;
    private OnItemClickListener mOnItemClickListener;
    private int tLeft;

    public TextAdapterShangquanLeft(Context context, List<DistrictList> listData, int sIdText, int sId, int nId) {
        super(context, R.string.no_data, listData);
        mContext = context;
        mListData = listData;
        selectedTextColor = mContext.getResources().getColor(sIdText);
        selectedDrawble = mContext.getResources().getDrawable(sId);
        normalDrawbleId = nId;
    }

    public TextAdapterShangquanLeft(Context context, DistrictList[] arrayData, int sIdText, int sId, int nId) {
        super(context, R.string.no_data, arrayData);
        mContext = context;
        mArrayData = arrayData;
        selectedTextColor = mContext.getResources().getColor(sIdText);
        selectedDrawble = mContext.getResources().getDrawable(sId);
        normalDrawbleId = nId;
    }

    public void refresh(List<DistrictList> listData) {
        this.mListData = listData;
        notifyDataSetChanged();
    }

    /**
     * 设置选中的position,并通知列表刷新
     */
    public void setSelectedPosition(int pos) {
        if (mListData != null && pos < mListData.size()) {
            selectedPos = pos;
            selectedText = mListData.get(pos).getName();
            notifyDataSetChanged();
        } else if (mArrayData != null && pos < mArrayData.length) {
            selectedPos = pos;
            selectedText = mArrayData[pos].getName();
            notifyDataSetChanged();
        }

    }

    /**
     * 设置选中的position,但不通知刷新
     */
    public void setSelectedPositionNoNotify(int pos) {
        selectedPos = pos;
        if (mListData != null && pos < mListData.size()) {
            selectedText = mListData.get(pos).getName();
        } else if (mArrayData != null && pos < mArrayData.length) {
            selectedText = mArrayData[pos].getName();
        }
    }

    /**
     * 获取选中的position
     */
    public int getSelectedPosition() {
        if (mArrayData != null && selectedPos < mArrayData.length) {
            return selectedPos;
        }
        if (mListData != null && selectedPos < mListData.size()) {
            return selectedPos;
        }

        return -1;
    }

    /**
     * 设置列表字体大小
     */
    public void setTextSize(float tSize) {
        textSize = tSize;
    }

    /**
     * 设置列表文字距离
     */
    public void setTextPaddingLeft(int left) {
        tLeft = left;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ShangQuanHolder shangQuanHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.choose_item, parent, false);
            shangQuanHolder = new ShangQuanHolder(convertView);
            convertView.setTag(shangQuanHolder);
        } else {
            shangQuanHolder = (ShangQuanHolder) convertView.getTag();
        }
        shangQuanHolder.view.setTag(position);
        String mString = "";
        if (mListData != null) {
            if (position < mListData.size()) {
                mString = mListData.get(position).getName();
            }
        } else if (mArrayData != null) {
            if (position < mArrayData.length) {
                mString = mArrayData[position].getName();
            }
        }
        if (mString.contains("不限")) {
            shangQuanHolder.view.setText("不限");
        } else {
            shangQuanHolder.view.setText(mString);
        }

        shangQuanHolder.view.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) shangQuanHolder.rightIcon.getLayoutParams();
        params.width = getTextViewWidth(shangQuanHolder.view, mString);
        shangQuanHolder.rightIcon.setLayoutParams(params);

        if (selectedText != null && selectedText.equals(mString)) {
            shangQuanHolder.view.setBackgroundDrawable(selectedDrawble);// 设置选中的背景图片
            shangQuanHolder.view.setTextColor(selectedTextColor);// 设置选中的字体颜色
            shangQuanHolder.rightIcon.setVisibility(View.INVISIBLE);
        } else {
            shangQuanHolder.view.setBackgroundDrawable(mContext.getResources().getDrawable(normalDrawbleId));// 设置未选中状态背景图片
            shangQuanHolder.view.setTextColor(mContext.getResources().getColor(R.color.text_gray3));
            shangQuanHolder.rightIcon.setVisibility(View.INVISIBLE);
        }
//        view.setPadding(tLeft, 0, 0, 0);
        convertView.setOnClickListener(new AdapterClick(position));
        return convertView;
    }

    private int getTextViewWidth(TextView textView, String text) {
        Rect rect = new Rect();
        textView.getPaint().getTextBounds(text, 0, text.length(), rect);
        int width = rect.width();
        return width;
    }

    public void setOnItemClickListener(OnItemClickListener l) {
        mOnItemClickListener = l;
    }

    /**
     * 重新定义菜单选项单击接口
     */
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    class ShangQuanHolder {
        TextView view;
        ImageView rightIcon;

        ShangQuanHolder(View rootView) {
            view = (TextView) rootView.findViewById(R.id.choose_left_tv);
            rightIcon = (ImageView) rootView.findViewById(R.id.left_icon);
        }
    }

    class AdapterClick implements View.OnClickListener {

        int position;

        AdapterClick(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            selectedPos = position;
            setSelectedPosition(selectedPos);
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(view, selectedPos);
            }
        }
    }
}
