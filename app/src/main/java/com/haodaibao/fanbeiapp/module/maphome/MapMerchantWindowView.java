package com.haodaibao.fanbeiapp.module.maphome;

import android.content.Intent;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amap.api.maps.model.LatLng;
import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Global;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.FormatUtil;
import com.baseandroid.utils.Util;
import com.baseandroid.widget.BlurringView;
import com.baseandroid.widget.loopgallery.BaseLoopGallery;
import com.baseandroid.widget.loopgallery.BaseLoopGallery.BindDataListener;
import com.baseandroid.widget.loopgallery.ViewAnimationUtils;
import com.baseandroid.widget.loopgallery.ViewHolder;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.combo.ComboDetailActivity;
import com.haodaibao.fanbeiapp.module.login.LoginActivity;
import com.haodaibao.fanbeiapp.module.merchant.MerchantDetailActivity;
import com.haodaibao.fanbeiapp.module.pay.PayActivity;
import com.haodaibao.fanbeiapp.repository.json.MerchantDetailResp;
import com.haodaibao.fanbeiapp.repository.json.MerchantImageInfo;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;
import com.haodaibao.fanbeiapp.repository.json.PackageListBean;
import com.haodaibao.fanbeiapp.repository.json.PriedListBean;
import com.jayfeng.lesscode.core.DisplayLess;
import com.tencent.stat.StatService;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.support.v7.widget.RecyclerView.SCROLL_STATE_IDLE;

/**
 * 开发者：LuoYi
 * Time: 2018 11:26 2018/1/8 01
 */

public class MapMerchantWindowView implements MaphomeContract.HomeWindowView, View.OnClickListener {

    private BaseActivity activity;
    private MaphomeContract.MaphomeView mapView;

    @BindView(R.id.merchant_ly)
    FrameLayout merchantLy;
    @BindView(R.id.blurring_view)
    BlurringView blurringView;
    @BindView(R.id.maphome_icon_left)
    ImageView maphomeIconLeft;
    @BindView(R.id.map_home_loop_blg)
    BaseLoopGallery<MerchantInfoEntiy> mapHomeLoopBlg;
    @BindView(R.id.maphome_icon_right)
    ImageView maphomeIconRight;

    private boolean isCurrent;
    private List<MerchantInfoEntiy> merchantInfoEntiyList;

    MapMerchantWindowView(BaseActivity activity, MaphomeContract.MaphomeView mapView) {
        this.activity = activity;
        this.mapView = mapView;
        ButterKnife.bind(this, activity);
        merchantLy.setOnClickListener(this);
        maphomeIconLeft.setOnClickListener(this);
        maphomeIconRight.setOnClickListener(this);
    }

    @Override
    public void initBlurring(boolean isCurrent) {
        this.isCurrent = isCurrent;
        blurringView.setBlurredView(activity.findViewById(R.id.map_main_rl));
    }

    @Override
    public void initHomeLoopView(int position, List<MerchantInfoEntiy> merchantList) {
        merchantInfoEntiyList = merchantList;
        blurringView.invalidate();
        if (mapHomeLoopBlg.mAdapter == null) {
            mapHomeLoopBlg.setDatasAndLayoutId(merchantInfoEntiyList, R.layout.item_map_merchant, bindDataListener);
            mapHomeLoopBlg.addOnScrollListener(scrollListener);
        } else {
            mapHomeLoopBlg.mAdapter.setDatas(merchantInfoEntiyList);
        }
        mapHomeLoopBlg.scrollToPosition(position);
        if (position == 0) {
            maphomeIconLeft.setVisibility(View.INVISIBLE);
            maphomeIconRight.setVisibility(View.VISIBLE);
        } else if (position == mapHomeLoopBlg.mLinearLayoutManager.getItemCount() - 1) {
            maphomeIconLeft.setVisibility(View.VISIBLE);
            maphomeIconRight.setVisibility(View.INVISIBLE);
        } else {
            maphomeIconLeft.setVisibility(View.VISIBLE);
            maphomeIconRight.setVisibility(View.VISIBLE);
        }
        if (position == 0 && position == mapHomeLoopBlg.mLinearLayoutManager.getItemCount() - 1) {
            maphomeIconLeft.setVisibility(View.INVISIBLE);
            maphomeIconRight.setVisibility(View.INVISIBLE);
        }
        mapView.setVisitMerchant(merchantInfoEntiyList.get(position).getMerchantno());

        ViewAnimationUtils.startAnimation(activity, mapHomeLoopBlg, merchantLy, 1, null);
    }

    @Override
    public void updateWindowUI(MerchantDetailResp merchantDetailResp) {
        List<PriedListBean> priedList = merchantDetailResp.getPriedList();
        for (int i = 0; i < priedList.size(); i++) {
            if (TextUtils.equals(priedList.get(i).isShow, "2")) {
                priedList.remove(i);
            }
        }
        for (int i = 0; i < merchantInfoEntiyList.size(); i++) {
            if (TextUtils.equals(merchantDetailResp.getMerchant().getMerchantno(), merchantInfoEntiyList.get(i).getMerchantno())) {
                merchantInfoEntiyList.get(i).priedList = priedList;
                merchantInfoEntiyList.get(i).packageList = merchantDetailResp.getPackageLists();
                merchantInfoEntiyList.get(i).setMerchantImageList(merchantDetailResp.getMerchant().getMerchantImageList());
                mapHomeLoopBlg.mAdapter.notifyDataSetChanged();
                break;
            }
        }
    }

    private BindDataListener bindDataListener = new BindDataListener<MerchantInfoEntiy>() {
        @Override
        public void onBindData(final ViewHolder holder, final MerchantInfoEntiy data) {
            Banner viewpagerMer = holder.getView(R.id.merchant_banner);
            NestedScrollView mscroll = holder.getView(R.id.mscroll);
            TextView merchant_name = holder.getView(R.id.merchant_name);
            TextView merchant_categoryname = holder.getView(R.id.merchant_categoryname);
            TextView merchant_average = holder.getView(R.id.merchant_average);
            TextView merchant_dis = holder.getView(R.id.merchant_dis);
            RecyclerView item_disount = holder.getView(R.id.item_disount);
            RecyclerView item_combo = holder.getView(R.id.item_combo);
            ImageView dis_line = holder.getView(R.id.dis_line);
            final View map_merchant_list_bg = holder.getView(R.id.map_merchant_list_bg);

            MapMerchantDiscountAdapter discountAdapter = new MapMerchantDiscountAdapter(activity, new BaseRecycleViewAdapter.OnItemClickListener() {
                @Override
                public <T> void onItemClick(View view, T t) {
                    if (CommonUtils.isFastDoubleClick()) {
                        return;
                    }
                    if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                        activity.startActivity(new Intent(activity, LoginActivity.class));
                    } else {
                        PayActivity.openForResult(activity, null, data.getMerchantno(), "", false);
                    }
                    StatService.trackCustomKVEvent(activity, "qbsh_home_map_marker_popup_discountlist", null);
                }
            }, map_merchant_list_bg);
            MaphomeComboListAdapter comboListAdapter = new MaphomeComboListAdapter(activity, new BaseRecycleViewAdapter.OnItemClickListener() {
                @Override
                public <T> void onItemClick(View view, T t) {
                    PackageListBean bean = (PackageListBean) t;
                    ComboDetailActivity.start(activity, bean.getPackageNo(), data.getMerchantno(), data);
                    StatService.trackCustomKVEvent(activity, "qbsh_home_map_marker_popup_packagelist", null);
                }
            });
            item_disount.setLayoutManager(new LinearLayoutManager(activity));
            item_disount.setNestedScrollingEnabled(false);
            item_disount.setAdapter(discountAdapter);
            item_combo.setLayoutManager(new LinearLayoutManager(activity));
            item_combo.setNestedScrollingEnabled(false);
            item_combo.setAdapter(comboListAdapter);
            if (data.priedList != null) {
                for (PriedListBean priedListBean : data.priedList) {
                    if (!TextUtils.equals(priedListBean.isShow, "1")) {
                        data.priedList.remove(priedListBean);
                    }
                }
                discountAdapter.resetData(data.priedList);
            }
            if (data.packageList != null) {
                comboListAdapter.resetData(data.packageList);
            }
            setBannerUrl(data.getMerchantImageList(), viewpagerMer, mscroll);
            merchant_name.setText(data.getMerchantname());
            merchant_categoryname.setText(data.getCategoryName());

            merchant_average.setText(activity.getString(R.string.rmb_aveage) + FormatUtil.filterDecimalPoint(data.getPersonaverage()));
            String dis = "";
            if (isCurrent) {
                dis = "" + Util.getDistance(new LatLng(Global.getMyLocation().getLatitude(), Global.getMyLocation().getLongitude()), new LatLng(Double.parseDouble(data.getLatitude()), Double.parseDouble(data.getLongitude())));
            }

            if (TextUtils.isEmpty(dis)) {
                dis_line.setVisibility(View.GONE);
                merchant_dis.setVisibility(View.GONE);
            } else {
                dis_line.setVisibility(View.VISIBLE);
                merchant_dis.setVisibility(View.VISIBLE);
                dis = FormatUtil.formatDistance(dis);
                merchant_dis.setText(dis);
            }

            holder.getView(R.id.map_btn_merchant).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (CommonUtils.isFastDoubleClick()) {
                        return;
                    }
                    goToMerchantDetail(data, holder.itemView);
                    StatService.trackCustomKVEvent(activity, "qbsh_home_map_marker_popup_more", null);
                }
            });
            holder.getView(R.id.merchant_banner).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (CommonUtils.isFastDoubleClick()) {
                        return;
                    }
                    goToMerchantDetail(data, holder.itemView);
                    StatService.trackCustomKVEvent(activity, "qbsh_home_map_marker_popup_toppicture", null);
                }
            });
            holder.getView(R.id.merchant_name).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (CommonUtils.isFastDoubleClick()) {
                        return;
                    }
                    goToMerchantDetail(data, holder.itemView);
                    StatService.trackCustomKVEvent(activity, "qbsh_home_map_marker_popup_merchantname", null);
                }
            });
            holder.getView(R.id.goto_buy).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (CommonUtils.isFastDoubleClick()) {
                        return;
                    }
                    if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                        activity.startActivity(new Intent(activity, LoginActivity.class));
                    } else {
                        PayActivity.openForResult(activity, null, data.getMerchantno(), "", false);
                    }
                    StatService.trackCustomKVEvent(activity, "qbsh_home_map_marker_popup_payment", null);
                }
            });
        }
    };

    private void setBannerUrl(final List<MerchantImageInfo> imageList, Banner viewpagerMer, NestedScrollView mscroll) {
        List<Object> imgurlList = new ArrayList<>();
        if (imageList == null || imageList.size() == 0) {
            imgurlList.add(R.drawable.home_default_backgroud);
        } else {
            for (MerchantImageInfo merchantImageInfo : imageList) {
//                imgurlList.add(CommonUtils.getImageUrl(merchantImageInfo.getImageurl()) + "?imageView2/1/w/750/h/460");
                imgurlList.add(CommonUtils.getImageUrl(merchantImageInfo.getImageurl()));
            }
        }
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) viewpagerMer.getLayoutParams();
        layoutParams.height = (int) ((DisplayLess.$width(activity) - DisplayLess.$dp2px(40)) * 0.614925);
        ViewGroup.LayoutParams layoutParams1 = mscroll.getLayoutParams();
        layoutParams1.height = (int) ((DisplayLess.$width(activity) - DisplayLess.$dp2px(40)) * 0.614925) + DisplayLess.$dp2px(128 + 86);
        viewpagerMer.setImageLoader(new MapMerchantImageLoader());
        viewpagerMer.setImages(imgurlList);
        viewpagerMer.setBannerStyle(BannerConfig.NUM_INDICATOR);
        viewpagerMer.setIndicatorGravity(BannerConfig.RIGHT);

        viewpagerMer.start();
    }

    private void goToMerchantDetail(MerchantInfoEntiy merchantInfo, View shareView) {
        Intent intent = new Intent(activity, MerchantDetailActivity.class);
        intent.putExtra("merchantno", merchantInfo.getMerchantno());
        intent.putExtra("merchantInfo", merchantInfo);
        activity.startActivity(intent);
    }

    private RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (newState == SCROLL_STATE_IDLE) {
                int itemPosition = mapHomeLoopBlg.mLinearLayoutManager.findFirstVisibleItemPosition();
                if (itemPosition == 0) {
                    maphomeIconLeft.setVisibility(View.INVISIBLE);
                    maphomeIconRight.setVisibility(View.VISIBLE);
                } else if (itemPosition == mapHomeLoopBlg.mLinearLayoutManager.getItemCount() - 1) {
                    maphomeIconLeft.setVisibility(View.VISIBLE);
                    maphomeIconRight.setVisibility(View.INVISIBLE);
                } else {
                    maphomeIconLeft.setVisibility(View.VISIBLE);
                    maphomeIconRight.setVisibility(View.VISIBLE);
                }
                mapView.setVisitMerchant(merchantInfoEntiyList.get(itemPosition).getMerchantno());
            }
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.merchant_ly:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                ViewAnimationUtils.startAnimation(activity, mapHomeLoopBlg, merchantLy, 0, null);
                StatService.trackCustomKVEvent(activity, "qbsh_home_map_marker_popup_close", null);
                break;
            case R.id.maphome_icon_right:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                mapHomeLoopBlg.post(new Runnable() {
                    @Override
                    public void run() {
                        snapToTargetExistingView();
                    }

                    void snapToTargetExistingView() {
                        mapHomeLoopBlg.smoothScrollBy((int) (mapHomeLoopBlg.getWidth()), 0);
                    }
                });
                StatService.trackCustomKVEvent(activity, "qbsh_home_map_marker_popup_right", null);
                break;
            case R.id.maphome_icon_left:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                mapHomeLoopBlg.post(new Runnable() {
                    @Override
                    public void run() {
                        snapToTargetExistingView();
                    }

                    void snapToTargetExistingView() {
                        mapHomeLoopBlg.smoothScrollBy((int) (-mapHomeLoopBlg.getWidth()), 0);
                    }
                });
                StatService.trackCustomKVEvent(activity, "qbsh_home_map_marker_popup_left", null);
                break;
            default:
                break;
        }
    }
}
