package com.haodaibao.fanbeiapp.module.invoice.makeout;

import com.haodaibao.fanbeiapp.repository.json.MakeoutApplyBean;
import com.haodaibao.fanbeiapp.repository.json.MakeoutInvoiceBean;

/**
 * date: 2018/2/11.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public interface MakeoutInvoiceContract {
    interface Presenter {
        void getApplyInfo(String orderNo, String merchantNo);

        void applyInvoice(String id, String orderNo, String merchantNo, String invoiceTypeCode, String riseTypeCode, String riseNo, String riseName, String taxNo, String mobile, String email, String companyAddress, String companyMobile, String companyBankName, String companyBankNo, String invoiceProject, String invoiceAmount);
    }

    interface View {
        void updateUI(MakeoutApplyBean.ApplyInvoice applyInvoice);

        void upDateStatus(boolean success,MakeoutInvoiceBean makeoutApplyBean);
    }
}
