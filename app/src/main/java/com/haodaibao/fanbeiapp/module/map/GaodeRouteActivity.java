package com.haodaibao.fanbeiapp.module.map;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amap.api.maps.model.LatLng;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.route.BusRouteResult;
import com.amap.api.services.route.DriveRouteResult;
import com.amap.api.services.route.Path;
import com.amap.api.services.route.RideRouteResult;
import com.amap.api.services.route.RouteSearch;
import com.amap.api.services.route.WalkRouteResult;
import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Global;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.StatusBarHelper;
import com.haodaibao.fanbeiapp.R;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * 开发者：LuoYi
 * Time: 2017 14:26 2017/9/15 09
 */

public class GaodeRouteActivity extends BaseActivity implements GaodeMapContract.GaodeRouteView, View.OnClickListener, RouteSearch.OnRouteSearchListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.startTextView)
    EditText startTextView;
    @BindView(R.id.endTextView)
    EditText endTextView;
    @BindView(R.id.ll_img_change)
    LinearLayout llImgChange;
    @BindView(R.id.rg_fangshi)
    RadioGroup rgFangshi;
    @BindView(R.id.busButton)
    RadioButton busButton;
    @BindView(R.id.drivingButton)
    RadioButton drivingButton;
    @BindView(R.id.walkButton)
    RadioButton walkButton;
    @BindView(R.id.lv_route)
    RecyclerView lvRoute;
    @BindView(R.id.rl_no_bus)
    RelativeLayout rlNoBus;
    @BindView(R.id.tv_no_bus1)
    TextView tvNoBus1;
    @BindView(R.id.tv_no_bus2)
    TextView tvNoBus2;
    ProgressDialog progDialog;
    private String merchantlat;
    private String merchantlon;
    private String myaddress = "你当前在";
    private String merchantaddress;
    private RouteSearch routeSearch;

    private int busMode = RouteSearch.BusDefault;// 公交默认模式
    private int drivingMode = RouteSearch.DrivingDefault;// 驾车默认模式
    private int walkMode = RouteSearch.WalkDefault;// 步行默认模式
    private int routeType = 1;// 1代表公交模式，2代表驾车模式，3代表步行模式
    private double distance = 0;
    private MapRoutePresenter mapRouteView;
    private MyRoutePlanAdapter adapter;
    private boolean change = false;// 是否需要在切换搜索模式时重新确定起点和终点
    private LatLonPoint startPoint;
    private LatLonPoint endPoint;

    public static void open(Activity activity, String enLat, String enLon, String enaddr) {
        Intent intent = new Intent(activity, GaodeRouteActivity.class);
        intent.putExtra("enLat", enLat);
        intent.putExtra("enLon", enLon);
        intent.putExtra("enaddr", enaddr);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_gaode_route;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(GaodeRouteActivity.this);
        toolbarBack.setOnClickListener(this);
        llImgChange.setOnClickListener(this);
        tvNoBus2.setOnClickListener(this);
        busButton.setOnClickListener(this);
        drivingButton.setOnClickListener(this);
        walkButton.setOnClickListener(this);

        toolbarTitle.setText(R.string.title_searchroute);

        routeSearch = new RouteSearch(this);
        routeSearch.setRouteSearchListener(this);
        lvRoute.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MyRoutePlanAdapter(this);
        lvRoute.setAdapter(adapter);

    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            merchantlat = getIntent().getStringExtra("enLat");
            merchantlon = getIntent().getStringExtra("enLon");
            merchantaddress = getIntent().getStringExtra("enaddr");
        } else {
            merchantlat = savedInstanceState.getString("enLat");
            merchantlon = savedInstanceState.getString("enLon");
            myaddress = savedInstanceState.getString("staddr");
            merchantaddress = savedInstanceState.getString("enaddr");
        }

        startTextView.setText(myaddress);
        endTextView.setText(merchantaddress);
        endTextView.setEnabled(false);
        startTextView.setEnabled(false);
        LatLonPoint endPoint = new LatLonPoint(Double.parseDouble(merchantlat), Double.parseDouble(merchantlon));
        mapRouteView = new MapRoutePresenter(this, routeSearch, endPoint);
        busButton.performClick();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString("enLat", merchantlat);
        savedInstanceState.putString("enLon", merchantlon);
        savedInstanceState.putString("staddr", myaddress);
        savedInstanceState.putString("enaddr", merchantaddress);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.ll_img_change:
                String startText = startTextView.getText().toString();
                String endText = endTextView.getText().toString();
                startTextView.setText(endText);
                endTextView.setText(startText);
                break;
            case R.id.tv_no_bus2:
                if (distance < 1000) {
                    walkButton.performClick();
                } else {
                    drivingButton.performClick();
                }
                break;
            case R.id.busButton:
                rlNoBus.setVisibility(View.GONE);
                lvRoute.setVisibility(View.VISIBLE);
                mapRouteView.busRoute(startTextView, endTextView);
                break;
            case R.id.drivingButton:
                rlNoBus.setVisibility(View.GONE);
                lvRoute.setVisibility(View.VISIBLE);
                mapRouteView.drivingRoute(startTextView, endTextView);
                break;
            case R.id.walkButton:
                rlNoBus.setVisibility(View.GONE);
                lvRoute.setVisibility(View.VISIBLE);
                mapRouteView.walkRoute(startTextView, endTextView);
                break;
            default:
                break;
        }
    }

    @Override
    public void onBusRouteSearched(BusRouteResult busRouteResult, int i) {
        dissmissProgressDialog();
        if (i == 1000) {
            if (busRouteResult != null && busRouteResult.getPaths() != null && busRouteResult.getPaths().size() > 0) {
                GaodeMapContract.routeResult = busRouteResult;
                adapter.resetData(busRouteResult.getPaths());
            } else {
                LatLng start = new LatLng(Global.getMyLocation().getLatitude(), Global.getMyLocation().getLongitude());
                LatLng end = new LatLng(Double.parseDouble(merchantlat), Double.parseDouble(merchantlon));

                distance = CommonUtils.getDistance(start, end);
                distance = Math.abs(distance);
                rlNoBus.setVisibility(View.VISIBLE);
                lvRoute.setVisibility(View.GONE);
                if (distance > 1000) {
                    tvNoBus1.setText("该范围内暂无公交,");
                    tvNoBus2.setText("请选择其他路线");
                }
            }

        } else {
            adapter.resetData(new ArrayList<Path>());
            showToast("暂无结果");
        }
    }

    @Override
    public void onDriveRouteSearched(DriveRouteResult driveRouteResult, int i) {
        dissmissProgressDialog();
        if (i == 1000) {
            adapter.resetData(driveRouteResult.getPaths());
            GaodeMapContract.routeResult = driveRouteResult;
        } else {
            adapter.resetData(new ArrayList<Path>());
            showToast("暂无结果");
        }

    }

    @Override
    public void onWalkRouteSearched(WalkRouteResult walkRouteResult, int i) {
        dissmissProgressDialog();
        if (i == 1000) {
            adapter.resetData(walkRouteResult.getPaths());
            GaodeMapContract.routeResult = walkRouteResult;
        } else {
            adapter.resetData(new ArrayList<Path>());
            showToast("暂无结果");
        }

    }

    @Override
    public void onRideRouteSearched(RideRouteResult rideRouteResult, int i) {
    }

    @Override
    public void showProgressDialog() {
        if (progDialog == null) {
            progDialog = new ProgressDialog(this);
        }
        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDialog.setIndeterminate(false);
        progDialog.setCancelable(true);
        progDialog.setMessage("正在搜索");
        progDialog.show();
    }

    @Override
    public void dissmissProgressDialog() {
        if (progDialog != null) {
            progDialog.dismiss();
        }
    }
}
