package com.haodaibao.fanbeiapp.module.invoice;

import android.animation.ArgbEvaluator;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.LogUtil;
import com.baseandroid.utils.StatusBarHelper;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.event.InvoiceRiseChangeEvent;
import com.haodaibao.fanbeiapp.event.MakoutInvoiceEvent;
import com.haodaibao.fanbeiapp.module.invoice.detail.InvoiceDetailActivity;
import com.haodaibao.fanbeiapp.module.invoice.updaterise.UpdateInvoiceRiseActivity;
import com.haodaibao.fanbeiapp.repository.json.InvoiceBean;
import com.jayfeng.lesscode.core.DisplayLess;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * date: 2018/2/5.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class MyInvoiceActivity extends BaseActivity implements MyInvoiceContract.View, BaseRecycleViewAdapter.OnItemClickListener {
    @BindView(R.id.rcv)
    RecyclerView rcv;
    @BindView(R.id.mscroll)
    NestedScrollView mscroll;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @BindView(R.id.right_text)
    TextView right_text;
    @BindView(R.id.toolbar_back)
    View toolbar_back;
    @BindView(R.id.invoice_tag)
    TextView invoice_tag;
    @BindView(R.id.invoice_merchantname)
    TextView invoice_merchantname;
    @BindView(R.id.invoice_taxnumber)
    TextView invoice_taxnumber;
    @BindView(R.id.no_invoice_title_ly)
    RelativeLayout no_invoice_title_ly;
    @BindView(R.id.invoice_title_ly)
    LinearLayout invoice_title_ly;
    @BindView(R.id.iv_toolbar_back)
    ImageView iv_toolbar_back;
    @BindView(R.id.toolbar_line)
    View toolbar_line;
    @BindView(R.id.bg_invoice_yellow)
    View bg_invoice_yellow;

    private int page = 1;
    private int pageSize = 10;
    private int BG_COLOR_START;
    private int BG_COLOR_END;
    private int TEXT_COLOR_END;
    private MyInvoiceContract.Presenter presenter;
    private InvoiceAdapter adapter;

    public static void start(BaseActivity context) {
        Intent intent = new Intent(context, MyInvoiceActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_my_invoice;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(this);
        EventBus.getDefault().register(this);
        BG_COLOR_START = ContextCompat.getColor(this, R.color.invoice_yellow);
        BG_COLOR_END = ContextCompat.getColor(this, R.color.white);
        TEXT_COLOR_END = ContextCompat.getColor(this, R.color.black1);
        presenter = new MyInvoicePresenter(this);
        initTitle();
        initRecyclerView();
        final ArgbEvaluator argbEvaluator = new ArgbEvaluator();
        final ArgbEvaluator argbEvaluator1 = new ArgbEvaluator();
        rcv.setNestedScrollingEnabled(false);
        mscroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                float offset = (float) Math.min(scrollY, DisplayLess.$dp2px(72)) / (float) DisplayLess.$dp2px(72);
                LogUtil.e("scrollY=" + scrollY);
                LogUtil.e("offset=" + offset);
                int currentColor = (int) argbEvaluator.evaluate(offset, BG_COLOR_START, BG_COLOR_END);
                int currentColor1 = (int) argbEvaluator1.evaluate(offset * offset, BG_COLOR_END, TEXT_COLOR_END);
                toolbar.setBackgroundColor(currentColor);
                bg_invoice_yellow.setBackgroundColor(currentColor);
                right_text.setTextColor(currentColor1);
                toolbar_title.setTextColor(currentColor1);
                if (offset < 0.5) {
                    iv_toolbar_back.setImageResource(R.drawable.icon_back_white);
                    iv_toolbar_back.setAlpha(1 - offset);
                } else {
                    iv_toolbar_back.setImageResource(R.drawable.icon_back_black);
                    iv_toolbar_back.setAlpha(offset);
                }
                if (offset < 1) {
                    toolbar_line.setVisibility(View.GONE);
                } else {
                    toolbar_line.setVisibility(View.VISIBLE);
                }

            }
        });
    }

    private void initRecyclerView() {
        rcv.setLayoutManager(new LinearLayoutManager(this));
        adapter = new InvoiceAdapter(this, this);

        rcv.setAdapter(adapter);
    }

    private void initTitle() {
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.invoice_yellow));
        toolbar_title.setTextColor(ContextCompat.getColor(this, R.color.white));
        toolbar_title.setText(R.string.title_my_invoice);
        right_text.setTextColor(ContextCompat.getColor(this, R.color.white));
        right_text.setText("抬头设置");
        right_text.setVisibility(View.VISIBLE);
        iv_toolbar_back.setImageResource(R.drawable.icon_back_white);
        toolbar_line.setVisibility(View.GONE);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        presenter.getInvoiceList(page + "", pageSize + "");
    }

    @OnClick({R.id.toolbar_back, R.id.right_text, R.id.no_invoice_title_ly})
    public void onclick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.right_text:
                InvoiceRiseActivity.start(this);
                break;
            case R.id.no_invoice_title_ly:
                UpdateInvoiceRiseActivity.start(this, null, getString(R.string.add_invoice_rise_title));
                break;
        }
    }

    @Override
    public void updateUI(InvoiceBean invoiceBean) {
        if (page == 1) {
            if (invoiceBean.rise == null || TextUtils.isEmpty(invoiceBean.rise.rise_id)) {
                invoice_title_ly.setVisibility(View.GONE);
                no_invoice_title_ly.setVisibility(View.VISIBLE);
            } else {
                invoice_title_ly.setVisibility(View.VISIBLE);
                no_invoice_title_ly.setVisibility(View.GONE);
            }
            adapter.resetData(invoiceBean.list.items);
            invoice_merchantname.setText(invoiceBean.rise.rise_name);
            if (invoiceBean.list.items.size() == 0) {
                adapter.setEmptyView(R.layout.myinvoice_list_empty);
            }
            if (TextUtils.equals(invoiceBean.rise.rise_type_code, "1")) {
                invoice_tag.setText(R.string.makeout_compony_text);
                invoice_taxnumber.setVisibility(View.VISIBLE);
                invoice_taxnumber.setText("税号：" + invoiceBean.rise.tax_no);
            } else if (TextUtils.equals(invoiceBean.rise.rise_type_code, "2")) {
                invoice_tag.setText(R.string.makeout_person_text);
                invoice_taxnumber.setVisibility(View.GONE);
            }
        } else {
            adapter.loadMoreComplete();
            adapter.addData(invoiceBean.list.items);
            if (invoiceBean.list.items.size() < pageSize) {
                adapter.loadMoreEnd(false);
            }
        }
        pageSize++;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MakoutInvoiceEvent event) {
        InvoiceBean.InvoiceItem item = adapter.getData().get(adapter.pos);
        item.setData(event.makeoutApplyBean.invoice);
        adapter.notifyItemChanged(adapter.pos);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(InvoiceRiseChangeEvent invoiceRiseChangeEvent) {
        page = 1;
        presenter.getInvoiceList(page + "", pageSize + "");
    }

    @Override
    public void updateFailed() {
        if (page == 1) {

        } else {
            adapter.loadMoreFail();
        }
    }

    @Override
    public <T> void onItemClick(View view, T t) {
        InvoiceBean.InvoiceItem item = (InvoiceBean.InvoiceItem) t;
        InvoiceDetailActivity.start(this, item.id);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
