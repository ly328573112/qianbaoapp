package com.haodaibao.fanbeiapp.module.pay;

import com.baseandroid.base.IView;
import com.haodaibao.fanbeiapp.repository.json.LableInfo;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;
import com.haodaibao.fanbeiapp.repository.json.MyCouponsResp;
import com.haodaibao.fanbeiapp.repository.json.PackageAuthBean;
import com.haodaibao.fanbeiapp.repository.json.PayInfoDetailBean;

import java.util.List;

/**
 * Created by hdb on 2017/8/30.
 */

public interface PayContract {
    interface Presenter {
        void checkMerchantPackageRule(String merchantno, String packageno);

        void getMerchantInfo(String merchantNo, MerchantInfoEntiy merchantDetailResp);

        void createOrder(String merchantno, String orderAmt, String notrefundamt, String couponNo, String couponAmt, String packageno, List<LableInfo> lableInfoList);
    }

    interface View extends IView {
        void updateCouponList(MyCouponsResp myCouponsResp);

        void updateMerchantActivityList(List<LableInfo> activities);

        void updateMerchantInfo(MerchantInfoEntiy merchantInfoEntiy);

        void clolsePayUI();

        void zhiFuing(String msg);

        void zhiFuSuc(PayInfoDetailBean payInfoDetailBean, String orderno);

        void zhiFuFailed(String msg);

        void checkedUI(PackageAuthBean packageAuthBean);
    }
}
