package com.haodaibao.fanbeiapp.module.bizutils;

/**
 * 开发者：LuoYi
 * Time: 2017 15:27 2017/10/12 10
 */

    /* 商户排序 去除离我最近*/
public class CategoryAsc1 {
    public static String DISTANCEASC = "distance.asc";
    public static String STARDESC = "star.desc";
    public static String CREATETIMEDESC = "createtime.desc";
    public static String ATTENTIONDESC = "attention.desc";
    public static String PERONAVERAGEDESC = "personaverage.desc";
    public static String PERSONAVERAGEASC = "personaverage.asc";

    public static String CURRDISCOUNT = "currdiscount.desc";
    public static String PERSONAVERAGE = "personaverage.desc";

    public static String[] strings = {DISTANCEASC, CURRDISCOUNT, PERSONAVERAGEASC, PERONAVERAGEDESC, CREATETIMEDESC};
    // public static String[] names =
    // {"离我最近","评价最高","最新发布","人气最高","价格最高","价格最低"};
    // public static String[] ids = {"1","2","3","4","5","6"};
    //        public static String[] names = {"离我最近", "人均最低", "最新发布"};
    //        public static String[] ids = {"1", "2", "3"};
    public static String[] names = {"智能排序", "优惠力度最大", "人均价格最低", "人均价格最高", "最新上线"};
    public static String[] ids = {"1", "2", "3", "4", "5"};

    /**
     * 商户排序条件
     *
     * @param name 中文名
     * @return id
     */
    public static String getCategoryAscFromName(String name) {
        String a = "";
        for (int i = 0; i < names.length; i++) {
            if (name.equals(names[i])) {
                a = strings[i];
            }
        }
        return a;
    }
}