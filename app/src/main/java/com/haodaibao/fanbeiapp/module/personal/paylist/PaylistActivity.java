package com.haodaibao.fanbeiapp.module.personal.paylist;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.StatusBarHelper;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.baseandroid.widget.recycleadpter.CustomPtrHeader;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.event.MakoutInvoiceEvent;
import com.haodaibao.fanbeiapp.event.PaylistEvent;
import com.haodaibao.fanbeiapp.module.combo.ComboDetailContract;
import com.haodaibao.fanbeiapp.module.combo.ComboDetailPresenter;
import com.haodaibao.fanbeiapp.module.evaluate.EvaluateSubmitActivity;
import com.haodaibao.fanbeiapp.module.merchant.MerchantDetailActivity;
import com.haodaibao.fanbeiapp.module.pay.PayActivity;
import com.haodaibao.fanbeiapp.module.pay.PayComboActivity;
import com.haodaibao.fanbeiapp.module.suggest.SuggestActivity;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;
import com.haodaibao.fanbeiapp.repository.json.PackageAllBean;
import com.haodaibao.fanbeiapp.repository.json.PackageAuthBean;
import com.haodaibao.fanbeiapp.repository.json.PaylistItemBean;
import com.haodaibao.fanbeiapp.repository.json.PaylistTopBean;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import butterknife.BindView;
import butterknife.OnClick;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;

import static com.baseandroid.config.Constant.PACKAGE_STATUS;

public class PaylistActivity extends BaseActivity implements PaylistContract.View, BaseRecycleViewAdapter.OnLoadMoreListener, ComboDetailContract.View {

    @BindView(R.id.tv_money_cost)
    TextView mTvMoneyCost;
    @BindView(R.id.tv_money_discount)
    TextView mTvMoneyDiscount;
    @BindView(R.id.rv)
    RecyclerView mRv;
    @BindView(R.id.ptr)
    PtrClassicFrameLayout mPtr;
    @BindView(R.id.appbar)
    AppBarLayout mAppBar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.iv_back)
    ImageView mIvBack;
    @BindView(R.id.fl_top)
    FrameLayout mFlTop;
    @BindView(R.id.tv_banner_text)
    TextView tv_banner_text;

    MerchantInfoEntiy merchantInfoEntiy = new MerchantInfoEntiy();
    private PaylistRvAdapter mRvAdapter;
    private PaylistPresenter mPresenter;
    private int mCurrentPage = 1;
    private int mVerticalOffset;
    private ComboDetailPresenter comboDetailPresenter;
    AppBarLayout.OnOffsetChangedListener mOnOffsetChangedListener = new AppBarLayout.OnOffsetChangedListener() {
        @Override
        public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
            mVerticalOffset = verticalOffset;
            int topPercent = -mVerticalOffset * 100 / mAppBar.getHeight();
            float alpha = (topPercent - 50) / 10.f;
            mFlTop.setAlpha(alpha);
//            if (topPercent > 50) {
//                mIvBack.setImageResource(R.drawable.icon_back_black);
//                mToolbarTitle.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
//            } else {
//                mIvBack.setImageResource(R.drawable.icon_back_white);
//                mToolbarTitle.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
//            }
        }
    };

    @Override
    protected int getLayoutId() {
        return R.layout.activity_paylist;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(PaylistActivity.this);
        mToolbarTitle.setText("买单记录");
        tv_banner_text.setText(getHintSentence());
        EventBus.getDefault().register(this);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        mPtr.disableWhenHorizontalMove(true);
        mPtr.setPtrHandler(new PtrDefaultHandler() {
            @Override
            public void onRefreshBegin(final PtrFrameLayout frame) {
                mCurrentPage = 1;
                getRvData();
            }

            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return mVerticalOffset >= 0 && PtrDefaultHandler.checkContentCanBePulledDown(frame, content, header);
            }
        });
        mAppBar.addOnOffsetChangedListener(mOnOffsetChangedListener);
        CustomPtrHeader customPtrHeader = new CustomPtrHeader(this);
        mPtr.setHeaderView(customPtrHeader);
        mPtr.addPtrUIHandler(customPtrHeader);
        mRv.setLayoutManager(new LinearLayoutManager(this));
        if (mRvAdapter == null) {
            mRvAdapter = new PaylistRvAdapter(this);
        }
        mRv.setAdapter(mRvAdapter);
        mRvAdapter.setOnLoadMoreListener(this, mRv);
        mRvAdapter.setClickListener(new PaylistRvAdapter.OnClickListener() {
            @Override
            public void itemClick(PaylistItemBean.OrdersBean.ItemsBean item) {
                gotoPayinfoDetail(item);
            }

            @Override
            public void payAginClick(PaylistItemBean.OrdersBean.ItemsBean item) {
                //ToastUtils.showShortToast("点击了再买一单");
                android.util.Log.e("xxxxxx", "点击了再买一单");
                if (TextUtils.equals("1", item.relationType)) {
                    doComboClick(item);
                } else {
                    PayActivity.openForResult(PaylistActivity.this, null, item.getMerchantno(), PayActivity.OTHER, false);
                }
            }

            @Override
            public void feedbackClick(PaylistItemBean.OrdersBean.ItemsBean item) {
                startActivity(new Intent(PaylistActivity.this, SuggestActivity.class));
                android.util.Log.e("xxxxxx", "点击了问题反馈");
            }

            @Override
            public void commentClick(PaylistItemBean.OrdersBean.ItemsBean item) {
                EvaluateSubmitActivity.open(PaylistActivity.this, item.getMerchantno(), item.getOrderno());
                android.util.Log.e("xxxxxx", "点击了发表评价");
            }

            @Override
            public void nameClick(PaylistItemBean.OrdersBean.ItemsBean item) {
                gotoMerchantDetail(item.getMerchantno());
            }
        });
        getRvData();
        getTopData();
    }

    private String getHintSentence() {
        String[] stringArray = getResources().getStringArray(R.array.pay_sentence_list);
        int randomIndex = getRandomIndex(stringArray.length, 0);
        return stringArray[randomIndex];
    }

    /**
     * 取随机数
     *
     * @param max
     * @param min
     * @return
     */
    protected int getRandomIndex(int max, int min) {
        Random random = new Random();
        return random.nextInt(max) % (max - min + 1) + min;
    }

    //跳转至买单详情页面
    private void gotoPayinfoDetail(PaylistItemBean.OrdersBean.ItemsBean item) {
        Intent i = new Intent(this, PayInfoDetailActivity.class);
        i.putExtra("orderNo", item.getOrderno());
        i.putExtra("merchantno", item.getMerchantno());
        startActivity(i);
    }

    //跳转至商户详情页面
    private void gotoMerchantDetail(String merchantNo) {
        Intent i = new Intent(this, MerchantDetailActivity.class);
        i.putExtra("merchantno", merchantNo);
        startActivityForResult(i, 100);
    }

    @OnClick(R.id.toolbar_back)
    public void onViewClicked() {
        ActivityJumpUtil.goBack(PaylistActivity.this);
    }

    //获取消费信息网络数据成功回调
    @Override
    public void setResultTopView(PaylistTopBean bean) {
        if (bean == null) {
            return;
        }
        mTvMoneyCost.setText(bean.getPayAmt());
        mTvMoneyDiscount.setText(bean.getDiscountAmt());
    }

    //获取RV网络数据成功回调
    @Override
    public void setResultRvView(PaylistItemBean bean) {
        mPtr.refreshComplete();
        if (bean == null) {
            return;
        }
        List<PaylistItemBean.OrdersBean.ItemsBean> list = bean.getOrders().getItems();
        if (mCurrentPage <= 1) {
            try {
                mCurrentPage = Integer.parseInt(bean.getOrders().getPage());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            if (list.size() == 0) {
                mRvAdapter.setEmptyView(R.layout.layout_empty_paylsit);
                mRvAdapter.loadMoreEnd(false);
            } else if (list.size() < 5) {
                mRvAdapter.loadMoreEnd(false);
            }
            mRvAdapter.resetData(list);

        } else {
            try {
                mCurrentPage = Integer.parseInt(bean.getOrders().getPage());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            if (list.size() == 0) {
                mRvAdapter.loadMoreEnd(false);
                return;
            }
            if (list.size() < 5) {
                mRvAdapter.addData(list);
                mRvAdapter.loadMoreComplete();
                mRvAdapter.loadMoreEnd(false);
                return;
            }
            mRvAdapter.addData(list);
            mRvAdapter.loadMoreComplete();
        }
    }

    //请求列表数据
    public void getRvData() {
        if (mPresenter == null) {
            mPresenter = new PaylistPresenter(this);
        }
        Map<String, String> params = new HashMap<>();
        params.put("page", mCurrentPage + "");
        mPresenter.getRvData(params);
    }

    //请求消费信息数据
    public void getTopData() {
        if (mPresenter == null) {
            mPresenter = new PaylistPresenter(this);
        }
        mPresenter.getTopData();
    }

    @Override
    public void onLoadMore() {
        mCurrentPage += 1;
        getRvData();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PaylistEvent event) {
        if (event.isComment()) {
            mRvAdapter.getData().get(mRvAdapter.pos).setCommentstatus("");
            mRvAdapter.notifyItemChanged(mRvAdapter.pos);
        } else {
            mCurrentPage = 1;
            getTopData();
            getRvData();
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MakoutInvoiceEvent event) {
        mRvAdapter.getData().get(mRvAdapter.pos).invoiceStatus = event.makeoutApplyBean.invoice.status;
        mRvAdapter.notifyItemChanged(mRvAdapter.pos);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void doComboClick(PaylistItemBean.OrdersBean.ItemsBean item) {
        if (comboDetailPresenter == null) {
            comboDetailPresenter = new ComboDetailPresenter(this);
        }
        merchantInfoEntiy.setAddress(item.address);
        merchantInfoEntiy.setCityCode(item.citycode);
        merchantInfoEntiy.setMerchantname(item.getMerchantname());
        merchantInfoEntiy.setMerchantno(item.getMerchantno());
        comboDetailPresenter.getComboDetail(item.getMerchantno(), item.packageNo);
    }

    @Override
    public void updateUI(PackageAllBean packageAllBean) {
        if (TextUtils.equals(PACKAGE_STATUS, packageAllBean.packageAuth.packageStatus)) {

            PayComboActivity.openForResult(PaylistActivity.this, packageAllBean.packageDetail.merchantno, merchantInfoEntiy, packageAllBean.packageDetail);
        } else {
            PayActivity.openForResult(PaylistActivity.this, null, packageAllBean.packageDetail.merchantno, PayActivity.OTHER, false);
        }
    }

    @Override
    public void checkedUI(PackageAuthBean packageAuthBean) {
        //do noting
    }

    @Override
    public void showRetry() {
        //do noting
    }
}
