package com.haodaibao.fanbeiapp.module.personal.coupon;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.baseandroid.base.BaseFragment;
import com.baseandroid.config.Global;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.baseandroid.widget.recycleadpter.CustomPtrHeader;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.CouponBean;

import butterknife.BindView;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;

/**
 * Created by Routee on 2017/9/15.
 * description: ${cusor}
 */

public class CouponFragemnt extends BaseFragment implements BaseRecycleViewAdapter.OnLoadMoreListener, CouponContract.View {
    @BindView(R.id.rv)
    RecyclerView mRv;
    @BindView(R.id.ptr)
    PtrClassicFrameLayout mPtr;
    private CouponRvAdapter mAdapter;
    private boolean mTag;
    CouponPresenter mPresenter;
    private String mStatue = "0";
    private int mCurrentPage = 1;

    @Override
    protected int getLayoutId() {
        return R.layout.layout_ptr_rv;
    }

    @Override
    protected void setupView() {
        getBundleData();
        mPtr.disableWhenHorizontalMove(true);
        mPtr.setPtrHandler(new PtrDefaultHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                mCurrentPage = 1;
                getRvData();
            }
        });
        CustomPtrHeader ptrHeader = new CustomPtrHeader(mContext);
        mPtr.setHeaderView(ptrHeader);
        mPtr.addPtrUIHandler(ptrHeader);
        if (mAdapter == null) {
            mAdapter = new CouponRvAdapter(mContext);
        }
        mRv.setLayoutManager(new LinearLayoutManager(mContext));
        mRv.setAdapter(mAdapter);
        mAdapter.setOnLoadMoreListener(this, mRv);
    }

    //设置空页面（注意：哪里判断为空页面时，就在哪里调用，否则默认网络获取时显示空页面）
    private void initEmptyView() {
        View view = LayoutInflater.from(Global.getContext())
                .inflate(R.layout.layout_empty_coupon_list_personal, null, false);
        TextView tvMore = (TextView) view.findViewById(R.id.tv_more);
        mAdapter.setEmptyView(view);
        tvMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CouponActivity) mContext).showFragment(false);
            }
        });
        if (mTag) {
            tvMore.setVisibility(View.VISIBLE);
        } else {
            tvMore.setVisibility(View.GONE);
        }
    }

    private void initFootView() {
        mAdapter.removeAllFooterView();
        if (mTag) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.view_foot_rv_next, null, false);
            mAdapter.addFooterView(view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((CouponActivity) mContext).showFragment(false);
                }
            });
        }
    }

    void getBundleData() {
        Bundle b = getArguments();
        mTag = b.getBoolean("mTag");
        if (mTag) {
            mStatue = "0";
        } else {
            mStatue = "1";
        }
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        getRvData();
    }


    @Override
    public void onLoadMore() {
        getRvData();
    }

    @Override
    public void setUpCouponData(CouponBean bean) {
        initEmptyView();
        if (mCurrentPage == 1) {
            mAdapter.resetData(bean.getCouponList().getItems());
            mPtr.refreshComplete();
        } else {
            mAdapter.loadMoreComplete();
            if (bean.getCouponList().getItems().size() < 10) {
                mAdapter.loadMoreEnd(true);
            }
            mAdapter.addData(bean.getCouponList().getItems());
        }
        if (bean.getCouponList().getItems().size() < 10) {
            initFootView();
        }
        mCurrentPage++;
    }

    @Override
    public void onUpdateFailed() {
        if (mCurrentPage == 1) {
//            initEmptyView();
        } else {
            mAdapter.loadMoreFail();
        }
    }

    //获取网络数据
    public void getRvData() {
        if (mPresenter == null) {
            mPresenter = new CouponPresenter(this);
        }
        mPresenter.getCoupons(mStatue, mCurrentPage + "");
    }
}
