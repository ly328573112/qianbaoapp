package com.haodaibao.fanbeiapp.module.personal.paylist;

import com.haodaibao.fanbeiapp.repository.json.PaylistItemBean;
import com.haodaibao.fanbeiapp.repository.json.PaylistTopBean;

import java.util.Map;

/**
 * Created by Routee on 2017/9/4.
 * description: ${cusor}
 */

public interface PaylistContract {
    interface View {
        void setResultTopView(PaylistTopBean bean);
        void setResultRvView(PaylistItemBean bean);
    }

    interface Presenter {
        void getTopData();
        void getRvData(Map<String, String> params);
    }
}
