package com.haodaibao.fanbeiapp.module.imgoriginal;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.photocameralib.media.widget.PreviewerViewPager;
import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.StatusBarHelper;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.event.OriginalDeleteEvent;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * @author LuoYi
 *         Time: 2017 10:15 2017/10/30 10
 */

public class OriginalPagerActivity extends BaseActivity implements View.OnClickListener, ViewPager.OnPageChangeListener, OriginalContract.PagerAdapterView {

    public static final String KEY_IMAGE = "images";
    public static final String KEY_POSITION = "position";

    @BindView(R.id.pager_back_rl)
    RelativeLayout pagerBackRl;
    @BindView(R.id.pager_delete_rl)
    RelativeLayout pagerDeleteRl;
    @BindView(R.id.original_pager_pv)
    PreviewerViewPager originalPagerView;
    @BindView(R.id.original_index_tv)
    TextView originalIndexTv;

    private List<String> mImageSources;
    private int mCurPosition = 0;
    private OriginalPagerAdapter originalPagerAdapter;

    public static void start(Activity activity, ArrayList<String> imageList, int pos) {
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(KEY_IMAGE, imageList);
        bundle.putInt(KEY_POSITION, pos);
        ActivityJumpUtil.next(activity, OriginalPagerActivity.class, bundle, 0);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.original_pager_view;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(this);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

        pagerBackRl.setOnClickListener(this);
        pagerDeleteRl.setOnClickListener(this);
        originalPagerView.addOnPageChangeListener(this);
    }


    @Override
    protected void setupData(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        mImageSources = bundle.getStringArrayList(KEY_IMAGE);
        mCurPosition = bundle.getInt(KEY_POSITION, 0);

        originalPagerAdapter = new OriginalPagerAdapter(this, mImageSources, this);
        originalPagerView.setAdapter(originalPagerAdapter);
        originalPagerView.setCurrentItem(mCurPosition);
        onPageSelected(mCurPosition);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pager_back_rl:
                finish();
                break;

            case R.id.pager_delete_rl:
                mImageSources.remove(mCurPosition);

                OriginalDeleteEvent deleteEvent = new OriginalDeleteEvent();
                deleteEvent.curPosition = mCurPosition;
                EventBus.getDefault().post(deleteEvent);

                if (mImageSources.size() == 0) {
                    finish();
                    return;
                }

                originalPagerAdapter = new OriginalPagerAdapter(this, mImageSources, this);
                originalPagerView.setAdapter(originalPagerAdapter);
                originalPagerView.setCurrentItem(mCurPosition);
                break;

            default:
                break;
        }
    }

    private boolean first = true;

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (first && position == 0) {
            onPageSelected(0);
            first = false;
        } else {
            first = true;
        }
    }

    @Override
    public void onPageSelected(int position) {
        mCurPosition = position;
        originalIndexTv.setText(String.format("%s/%s", (position + 1), mImageSources.size()));
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onReached(boolean reached) {
        originalPagerView.isInterceptable(reached);
    }
}
