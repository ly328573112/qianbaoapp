package com.haodaibao.fanbeiapp.module.evaluate;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.baseandroid.widget.RouteeFlowLayout;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.CommentListBean;
import com.qianbao.shiningwhitelibrary.utils.UiUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 开发者：LuoYi
 * Time: 2017 19:57 2017/9/12 09
 */

public class RouteeFlowView implements EvaluateContract.RouteeView, View.OnClickListener {

    EvaluateContract.EvaluateView evaluateView;
    private EvaluateListActivity activity;
    private List<CheckBox> checkBoxes = new ArrayList<>();
    private EvaluateContract.ChangeMark changeMark;

    RouteeFlowView(EvaluateContract.EvaluateView evaluateView, EvaluateContract.ChangeMark changeMark) {
        this.evaluateView = evaluateView;
        this.changeMark = changeMark;
        activity = (EvaluateListActivity) evaluateView;
    }

    @Override
    public void setRouteeFlowView(RouteeFlowLayout tagComment, List<CommentListBean.MyHeader> myHeader, String queryType) {
        checkBoxes.clear();
        for (int i = 0; i < myHeader.size(); i++) {
            CheckBox checkBox = new CheckBox(activity);
            checkBox.setText(myHeader.get(i).getWord());
            checkBox.setTag(myHeader.get(i).getQueryType());
            checkBox.setOnClickListener(this);
            checkBox.setButtonDrawable(new ColorDrawable(Color.parseColor("#00000000")));
            checkBox.setTextSize(13);
            if (TextUtils.equals("0", myHeader.get(i).getCount())) {
                checkBox.setVisibility(View.GONE);
            } else {
                checkBox.setVisibility(View.VISIBLE);
                checkBox.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                checkBox.setPadding(UiUtils.dp2px(activity, 10), UiUtils.dp2px(activity, 6), UiUtils.dp2px(activity, 10), UiUtils.dp2px(activity, 6));
                checkBox.setGravity(Gravity.CENTER);
            }
            if (i == myHeader.size() - 1) {
                checkBox.setBackgroundResource(R.drawable.checkbox_bg_gray1);
                checkBox.setTextColor(ContextCompat.getColor(activity, R.color.text_hint));
            } else {
                checkBox.setBackgroundResource(R.drawable.checkbox_bg_gald);
                checkBox.setTextColor(ContextCompat.getColor(activity, R.color.c_666666));
            }
            if (TextUtils.equals((String) checkBox.getTag(), queryType)) {
                checkBox.setChecked(true);
                checkBox.setTextColor(ContextCompat.getColor(activity, R.color.white));
            }
            checkBoxes.add(checkBox);
            tagComment.addView(checkBox);
            tagComment.setHorizontalSpacing(UiUtils.dp2px(activity, 10));
        }
    }

    @Override
    public void onClick(View v) {
        for (int i = 0; i < checkBoxes.size(); i++) {
            if (checkBoxes.get(i).isChecked()) {
                checkBoxes.get(i).setChecked(false);
                if (i == checkBoxes.size() - 1) {
                    checkBoxes.get(i).setTextColor(ContextCompat.getColor(activity, R.color.text_hint));
                } else {
                    checkBoxes.get(i).setTextColor(ContextCompat.getColor(activity, R.color.c_666666));
                }
            }
        }
        ((CheckBox) v).setChecked(true);
        ((CheckBox) v).setTextColor(ContextCompat.getColor(activity, R.color.white));
        changeMark.changeMark((String) v.getTag());
    }
}
