package com.haodaibao.fanbeiapp.module.yaohe;

import com.baseandroid.base.IView;
import com.haodaibao.fanbeiapp.repository.json.YaoheDetailsBean;

import java.util.List;

public interface YaoheContract {
    interface Presenter {
        void getYaoheDetails(String messageNo);
    }

    interface IYaoheView extends IView {
        //数据处理移到子线程
        void updateDetails(List<YaoheDetailsBean.MessageBody> yaoheDetailsBean);

        void onFail(String msg);
    }
}