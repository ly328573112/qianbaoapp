package com.haodaibao.fanbeiapp.module.personal.collections;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Global;
import com.baseandroid.utils.RxUtils;
import com.baseandroid.utils.ToastUtils;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.LifeRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.CollectionsBean;
import com.haodaibao.fanbeiapp.repository.json.Data;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Routee on 2017/9/4.
 * description: ${cusor}
 */

class CollectionsPresenter implements CollectionsContract.Presenter{

    private CollectionsContract.View mView;

    public CollectionsPresenter(CollectionsContract.View view) {
        this.mView = view;
    }

    @Override
    public void getCollections() {
        Map<String, String> params = new HashMap<>();
        params.put("longitude", Global.getMyLocation().getLongitude() + "");
        params.put("latitude", Global.getMyLocation().getLatitude() + "");
        LifeRepository.getInstance().getCollections(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<CollectionsBean>>applySchedulersLifeCycle((BaseActivity) mView))
                .subscribe(new RxObserver<Data<CollectionsBean>>() {

                    @Override
                    public void onNext(@NonNull Data<CollectionsBean> data) {
                        if (checkJsonCode(data, false)) {
                            mView.setResultView(data.getResult());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                        ToastUtils.showShortToast(Global.getContext().getString(R.string
                                .toast_network_failure_default));
                    }
                });
    }
}
