package com.haodaibao.fanbeiapp.module.personal.setting;

import android.text.TextUtils;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Constant;
import com.baseandroid.utils.RxUtils;
import com.google.gson.Gson;
import com.haodaibao.fanbeiapp.repository.ConfigRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.UserInfoRepository;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.UploadResult;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

import static com.baseandroid.config.Constant.PACKAGE_STATUS;

/**
 * Created by Routee on 2017/9/20.
 * description: ${cusor}
 */

public class UserinfoSettingPresenter implements UserinfoSettingContract.Presenter {
    private final UserinfoSettingContract.View mView;

    public UserinfoSettingPresenter(UserinfoSettingContract.View view) {
        mView = view;
    }

    @Override
    public void checkOut() {
        UserInfoRepository.getInstance().checkOut().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.applySchedulersLifeCycle((BaseActivity) mView))
                .subscribe(new RxObserver<Data>() {
                    @Override
                    public void onNext(@NonNull Data data) {
                        if (checkJsonCode(data, false)) {
                            mView.checkOutSuccess();
                        }
                    }
                });
    }

    @Override
    public void uploadHeadView(HashMap<String, String> params, String photoUri) {
        ConfigRepository.getInstance().uploadFileWithPartMap(params, "file", photoUri)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<ResponseBody>applySchedulersLifeCycle((BaseActivity) mView))
                .subscribe(new RxObserver<ResponseBody>() {
                               @Override
                               public void onNext(@NonNull ResponseBody responseBody) {
                                   String respons = null;
                                   try {
                                       respons = responseBody.string();
                                       Gson gson = new Gson();
                                       UploadResult uploadResult = gson.fromJson(respons, UploadResult.class);
                                       UploadResult.UploadInfo uploadFile = uploadResult.getUploadFile();
                                       if (TextUtils.equals(uploadResult.getStatus(), Constant.SUCCESS_CODE) || TextUtils.equals(uploadResult.getResultCode(), PACKAGE_STATUS)) {
                                           // 文件上传成功
                                           if (!TextUtils.isEmpty(uploadFile.getName())) {
                                               updateHeadView(uploadFile.getName());
                                           }
                                       }
                                   } catch (IOException e) {
                                       e.printStackTrace();
                                   }
                               }

                               @Override
                               public void onError(Throwable e) {
                                   super.onError(e);
                                   ((BaseActivity) mView).closeUploading();
                               }

                               @Override
                               public void onComplete() {
                                   super.onComplete();

                               }
                           }
                );
    }

    public void updateHeadView(String url) {
        Map<String, String> params = new HashMap<>();
        params.put("imgurl", url);
        UserInfoRepository.getInstance().updateHeadView(params).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data>applySchedulersLifeCycle((BaseActivity) mView))
                .subscribe(new RxObserver<Data>() {
                    @Override
                    public void onComplete() {
                        super.onComplete();
                        mView.uploadHeadViewSuccess();
                    }
                });
    }
}
