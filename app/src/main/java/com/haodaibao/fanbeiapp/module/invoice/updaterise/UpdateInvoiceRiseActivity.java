package com.haodaibao.fanbeiapp.module.invoice.updaterise;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.ToastUtils;
import com.baseandroid.widget.customtext.edittextview.EditTextWithDelNormal;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.event.InvoiceRiseChangeEvent;
import com.haodaibao.fanbeiapp.module.dialog.MessageDialog;
import com.haodaibao.fanbeiapp.repository.json.InvoiceHeadBean;
import com.haodaibao.fanbeiapp.repository.json.InvoiceRiseDetailBean;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;

import static com.baseandroid.config.Constant.INVOICEHEADITEM;

/**
 * 修改发票抬头
 */
public class UpdateInvoiceRiseActivity extends BaseActivity implements View.OnClickListener, UpdateInvoiceRiseContract.View {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.right_text)
    TextView rightText;
    @BindView(R.id.invoice_select_company_rbtn)
    RadioButton selectCompanyRbtn;
    @BindView(R.id.invoice_select_personal_rbtn)
    RadioButton selectPersonalRbtn;
    @BindView(R.id.invoice_company_et)
    EditTextWithDelNormal companyEt;
    @BindView(R.id.invoice_duty_paragraph_et)
    EditTextWithDelNormal dutyParagraphEt;
    @BindView(R.id.invoice_company_address_et)
    EditTextWithDelNormal companyAddressEt;
    @BindView(R.id.invoice_company_phone_et)
    EditTextWithDelNormal companyPhoneEt;
    @BindView(R.id.invoice_bank_et)
    EditTextWithDelNormal bankEt;
    @BindView(R.id.invoice_bank_number_et)
    EditTextWithDelNormal bankNumberEt;
    @BindView(R.id.invoice__rise_type_rg)
    RadioGroup invoice__rise_type_rg;
    @BindView(R.id.invoice_tax_ly)
    LinearLayout invoice_tax_ly;

    private UpdateInvoiceRisePresenter presenter;
    private String id, riseTypeCode, riseName, taxNo, mobile, email, companyAddress, companyMobile, companyBankName, companyBankNo;

    private InvoiceHeadBean.InvoiceHeadItem invoiceHeadItem;
    private MessageDialog messageDialog;
    private String title;

    public static void start(BaseActivity context, InvoiceHeadBean.InvoiceHeadItem invoiceHeadItem,String title) {
        Intent intent = new Intent(context, UpdateInvoiceRiseActivity.class);
        intent.putExtra(INVOICEHEADITEM, invoiceHeadItem);
        intent.putExtra("title", title);
        context.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_update_invoice_rise;
    }

    @Override
    protected void setupView() {

        toolbarBack.setOnClickListener(this);
        rightText.setOnClickListener(this);
        rightText.setText("保存");
        rightText.setVisibility(View.VISIBLE);
        rightText.setTextColor(getResources().getColor(R.color.invoice_ffc100));
        invoice__rise_type_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.invoice_select_company_rbtn) {
                    invoice_tax_ly.setVisibility(View.VISIBLE);
                    riseTypeCode = 1 + "";
                } else if (checkedId == R.id.invoice_select_personal_rbtn) {
                    invoice_tax_ly.setVisibility(View.GONE);
                    riseTypeCode = 2 + "";
                }
            }
        });
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        presenter = new UpdateInvoiceRisePresenter(this);
        Intent intent = getIntent();
        title = intent.getStringExtra("title");
        toolbarTitle.setText(title);
//        toolbarTitle.setText("修改发票抬头");
        invoiceHeadItem = (InvoiceHeadBean.InvoiceHeadItem) intent.getSerializableExtra(INVOICEHEADITEM);
        setRiseData();
    }

    private void setRiseData() {
        if (invoiceHeadItem != null) {
            id = invoiceHeadItem.rise_id;
            riseTypeCode = invoiceHeadItem.rise_type_code;
            if (TextUtils.equals(invoiceHeadItem.rise_type_code, "1")) {
                selectCompanyRbtn.setChecked(true);
                dutyParagraphEt.setText(invoiceHeadItem.tax_no);
            } else if (TextUtils.equals(invoiceHeadItem.rise_type_code, "2")) {
                selectPersonalRbtn.setChecked(true);
                dutyParagraphEt.setText("");
            }
            companyEt.setText(invoiceHeadItem.rise_name);
            companyAddressEt.setText(invoiceHeadItem.company_address);
            companyPhoneEt.setText(invoiceHeadItem.company_mobile);
            bankEt.setText(invoiceHeadItem.company_bank_name);
            bankNumberEt.setText(invoiceHeadItem.company_bank_no);
        } else {
            id = "";
            riseTypeCode = 1 + "";
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                if (invoiceHeadItem != null) {
                    riseName = companyEt.getText().toString();
                    taxNo = dutyParagraphEt.getText().toString();
                    mobile = "";
                    email = "";
                    companyAddress = companyAddressEt.getText().toString();
                    companyMobile = companyPhoneEt.getText().toString();
                    companyBankName = bankEt.getText().toString();
                    companyBankNo = bankNumberEt.getText().toString();
                    if (TextUtils.equals(invoiceHeadItem.rise_name, riseName)
                            && TextUtils.equals(invoiceHeadItem.tax_no, taxNo)
                            && TextUtils.equals(invoiceHeadItem.company_address, companyAddress)
                            && TextUtils.equals(invoiceHeadItem.company_mobile, companyMobile)
                            && TextUtils.equals(invoiceHeadItem.company_bank_name, companyBankName)
                            && TextUtils.equals(invoiceHeadItem.company_bank_no, companyBankNo)
                            && TextUtils.equals(invoiceHeadItem.rise_type_code, riseTypeCode)) {
                        finish();
                    } else {
                        if (messageDialog == null) {
                            messageDialog = MessageDialog.newInstance();
                        }

                        if (!messageDialog.isVisible()) {
                            messageDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                @Override
                                public void onShow(DialogInterface dialog) {
                                    messageDialog.hideTitle();
                                    messageDialog.setContent("修改了的信息还未保存，现在返回吗?");
                                    messageDialog.setConfirmText("确定");
                                    messageDialog.setCancelText("取消");
                                    messageDialog.setConfirmOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            messageDialog.dismiss();
                                            finish();
                                        }
                                    });
                                    messageDialog.setCancelOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            messageDialog.dismiss();
                                        }
                                    });
                                }
                            });
                            if (!messageDialog.isAdded()) {
                                messageDialog.show(getSupportFragmentManager(), "MessageDialog");
                            }

                        }
                    }
                } else {
                    finish();
                }

                break;
            case R.id.right_text:
                riseName = companyEt.getText().toString();
                taxNo = dutyParagraphEt.getText().toString();
                mobile = "";
                email = "";
                companyAddress = companyAddressEt.getText().toString();
                companyMobile = companyPhoneEt.getText().toString();
                companyBankName = bankEt.getText().toString();
                companyBankNo = bankNumberEt.getText().toString();
                if (TextUtils.isEmpty(riseName) ) {
                    ToastUtils.showShortToast("发票抬头不能为空");
                    return;
                }
                if (TextUtils.isEmpty(taxNo) && TextUtils.equals(riseTypeCode, "1")) {
                    ToastUtils.showShortToast("税号不能为空");
                    return;
                }
                if ((taxNo.length() < 15 || taxNo.length() > 20) && TextUtils.equals(riseTypeCode, "1")) {
                    ToastUtils.showShortToast("公司税号为15-20位");
                    return;
                }
                presenter.addOrModify(id, riseTypeCode, riseName, taxNo, mobile, email, companyAddress, companyMobile, companyBankName, companyBankNo);
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if (invoiceHeadItem != null) {
                    riseName = companyEt.getText().toString();
                    taxNo = dutyParagraphEt.getText().toString();
                    mobile = "";
                    email = "";
                    companyAddress = companyAddressEt.getText().toString();
                    companyMobile = companyPhoneEt.getText().toString();
                    companyBankName = bankEt.getText().toString();
                    companyBankNo = bankNumberEt.getText().toString();
                    if (TextUtils.equals(invoiceHeadItem.rise_name, riseName)
                            && TextUtils.equals(invoiceHeadItem.tax_no, taxNo)
                            && TextUtils.equals(invoiceHeadItem.company_address, companyAddress)
                            && TextUtils.equals(invoiceHeadItem.company_mobile, companyMobile)
                            && TextUtils.equals(invoiceHeadItem.company_bank_name, companyBankName)
                            && TextUtils.equals(invoiceHeadItem.company_bank_no, companyBankNo)
                            && TextUtils.equals(invoiceHeadItem.rise_type_code, riseTypeCode)) {
                        finish();
                    } else {
                        if (messageDialog == null) {
                            messageDialog = MessageDialog.newInstance();
                        }

                        if (!messageDialog.isVisible()) {
                            messageDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                @Override
                                public void onShow(DialogInterface dialog) {
                                    messageDialog.hideTitle();
                                    messageDialog.setContent("修改了的信息还未保存，现在返回吗?");
                                    messageDialog.setConfirmText("确定");
                                    messageDialog.setCancelText("取消");
                                    messageDialog.setConfirmOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            messageDialog.dismiss();
                                            finish();
                                        }
                                    });
                                    messageDialog.setCancelOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            messageDialog.dismiss();
                                        }
                                    });
                                }
                            });
                            if (!messageDialog.isAdded()) {
                                messageDialog.show(getSupportFragmentManager(), "MessageDialog");
                            }

                        }
                    }
                } else {
                    finish();
                }
                break;

            default:
                break;
        }
        return true;
    }

    @Override
    public void addSucc(InvoiceRiseDetailBean invoiceRiseDetailBean) {
        InvoiceRiseChangeEvent invoiceRiseChangeEvent = new InvoiceRiseChangeEvent();
        invoiceRiseChangeEvent.invoiceRiseDetailBean = invoiceRiseDetailBean;
        EventBus.getDefault().post(invoiceRiseChangeEvent);
        finish();
//        ToastUtils.showShortToast("保存成功");
    }
}
