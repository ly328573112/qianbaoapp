package com.haodaibao.fanbeiapp.module.maphome.cluster;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amap.api.maps.AMap;
import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.model.BitmapDescriptor;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.model.animation.Animation;
import com.amap.api.maps.model.animation.ScaleAnimation;
import com.baseandroid.utils.LogUtil;
import com.baseandroid.widget.customtext.richtext.RichTextView;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.maphome.cluster.demo.MarkerInfo;
import com.haodaibao.fanbeiapp.module.maphome.cluster.demo.RegionItem;
import com.jayfeng.lesscode.core.DisplayLess;

import java.util.ArrayList;
import java.util.List;

import static com.haodaibao.fanbeiapp.module.maphome.MaphomePresenter.left_icon;


/**
 * Created by yiyi.qi on 16/10/10.
 * 整体设计采用了两个线程,一个线程用于计算组织聚合数据,一个线程负责处理Marker相关操作
 */
public class ClusterOverlay implements AMap.OnCameraChangeListener,
        AMap.OnMarkerClickListener {
    private AMap mAMap;
    private Context mContext;
    public List<ClusterItem> mClusterItems;
    public List<Cluster> mClusters;
    public int mClusterSize;
    private ClusterClickListener mClusterClickListener;
    private MoveFinishListener moveFinishListener;
    private ClusterRender mClusterRender;
    public List<Marker> mAddMarkers = new ArrayList<Marker>();
    private double mClusterDistance;
    private LruCache<String, BitmapDescriptor> mLruCache;
    private HandlerThread mMarkerHandlerThread = new HandlerThread("addMarker");
    private HandlerThread mSignClusterThread = new HandlerThread("calculateCluster");
    private Handler mMarkerhandler;
    private Handler mSignClusterHandler;
    private float mPXInMeters;
    private boolean mIsCanceled = false;
    public List<Cluster> mClustersAnimation;

    /**
     * 构造函数
     *
     * @param amap
     * @param clusterSize 聚合范围的大小（指点像素单位距离内的点会聚合到一个点显示）
     * @param context
     */
    public ClusterOverlay(AMap amap, int clusterSize, Context context) {
        this(amap, null, clusterSize, context);
    }

    /**
     * 构造函数,批量添加聚合元素时,调用此构造函数
     *
     * @param amap
     * @param clusterItems 聚合元素
     * @param clusterSize
     * @param context
     */
    public ClusterOverlay(AMap amap, List<ClusterItem> clusterItems,
                          int clusterSize, Context context) {
//        默认最多会缓存80张图片作为聚合显示元素图片, 根据自己显示需求和app使用内存情况, 可以修改数量
        mLruCache = new LruCache<String, BitmapDescriptor>(80) {
            protected void entryRemoved(boolean evicted, String key, BitmapDescriptor oldValue, BitmapDescriptor newValue) {
                try {
                    oldValue.getBitmap().recycle();
                } catch (Exception e) {
                    LogUtil.e(e.getMessage());
                }

            }
        };
        if (clusterItems != null) {
            mClusterItems = clusterItems;
        } else {
            mClusterItems = new ArrayList<ClusterItem>();
        }
        mContext = context;
        mClusters = new ArrayList<Cluster>();
        this.mAMap = amap;
        mClusterSize = clusterSize;
        mPXInMeters = mAMap.getScalePerPixel();
        mClusterDistance = mPXInMeters * mClusterSize;
        amap.setOnCameraChangeListener(this);
        amap.setOnMarkerClickListener(this);
        initThreadHandler();
        assignClusters();
    }

    /**
     * 设置聚合点的点击事件
     *
     * @param clusterClickListener
     */
    public void setOnClusterClickListener(
            ClusterClickListener clusterClickListener) {
        mClusterClickListener = clusterClickListener;
    }

    public void setMoveFinishListener(MoveFinishListener moveFinishListener) {
        this.moveFinishListener = moveFinishListener;
    }

    /**
     * 添加一个聚合点
     *
     * @param item
     */
    public void addClusterItem(ClusterItem item) {
        Message message = Message.obtain();
        message.what = SignClusterHandler.CALCULATE_SINGLE_CLUSTER;
        message.obj = item;
        mSignClusterHandler.sendMessage(message);
    }

    /**
     * 设置聚合元素的渲染样式，不设置则默认为气泡加数字形式进行渲染
     *
     * @param render
     */
    public void setClusterRenderer(ClusterRender render) {
        mClusterRender = render;
    }

    public void onDestroy() {
        mIsCanceled = true;
        mSignClusterHandler.removeCallbacksAndMessages(null);
        mMarkerhandler.removeCallbacksAndMessages(null);
        mSignClusterThread.quit();
        mMarkerHandlerThread.quit();
        for (Marker marker : mAddMarkers) {
            marker.remove();
        }
        mClusterItems.clear();
        mAddMarkers.clear();
//        mLruCache.evictAll();
    }

    //初始化Handler
    private void initThreadHandler() {
        mMarkerHandlerThread.start();
        mSignClusterThread.start();
        mMarkerhandler = new MarkerHandler(mMarkerHandlerThread.getLooper());
        mSignClusterHandler = new SignClusterHandler(mSignClusterThread.getLooper());
    }

    @Override
    public void onCameraChange(CameraPosition arg0) {
        moveFinishListener.onCameraChange();

    }

    @Override
    public void onCameraChangeFinish(CameraPosition arg0) {
        mPXInMeters = mAMap.getScalePerPixel();
        mClusterDistance = mPXInMeters * mClusterSize;
//        assignClusters();
        moveFinishListener.onCameraChangeFinish(arg0);
    }

    //点击事件
    @Override
    public boolean onMarkerClick(Marker arg0) {
        if (mClusterClickListener == null) {
            return true;
        }
        Cluster cluster = (Cluster) arg0.getObject();
        if (cluster != null) {
            mClusterClickListener.onClick(arg0, cluster.getClusterItems());
            return true;
        }
        return false;
    }

    Handler handler = new Handler();
    int i = 0;
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (mClustersAnimation.size() == 0 || i >= mClustersAnimation.size()) {
                handler.removeCallbacks(this);
            } else {
                addSingleClusterToMap(mClustersAnimation.get(i));
                i++;
            }


            if (i >= mClustersAnimation.size()) {
                handler.removeCallbacks(this);
            } else {
                if (mClustersAnimation.get(i).getMarker() == null) {
                    handler.postDelayed(this, 20);
                } else {
                    handler.post(this);
                }

            }

        }
    };

    /**
     * 将聚合元素添加至地图上
     */
    private void addClusterToMap(List<Cluster> clusters) {
        Log.e("hpo", "addClusterToMap: ");
        ArrayList<Marker> removeMarkers = new ArrayList<>();
        removeMarkers.addAll(mAddMarkers);
//        ScaleAnimation scaleAnimation = new ScaleAnimation(1, 0, 1, 0);
//        scaleAnimation.setDuration(300);
////        scaleAnimation.setInterpolator(new OvershootInterpolator());
//        MyAnimationListener myAnimationListener = new MyAnimationListener(removeMarkers);
//        for (Marker marker : removeMarkers) {
//            marker.setAnimation(scaleAnimation);
//            marker.setAnimationListener(myAnimationListener);
//            marker.startAnimation();
//        }
        for (Marker marker : removeMarkers) {
            marker.remove();
        }
        removeMarkers.clear();
        mAddMarkers.clear();

        i = 0;
        handler.removeCallbacks(runnable);
        handler.post(runnable);
//        for (Cluster cluster : clusters) {
//            addSingleClusterToMap(cluster);
//        }
    }

    //    private AlphaAnimation mADDAnimation = new AlphaAnimation(0, 1);
//    private ScaleAnimation mADDAnimation = new ScaleAnimation(0, 1, 0, 1);

    /**
     * 将单个聚合元素添加至地图显示
     *
     * @param cluster
     */
    private void addSingleClusterToMap(Cluster cluster) {
        Log.e("hpo", "addSingleClusterToMap: ");
        if (cluster.getMarker() == null) {

            LatLng latlng = cluster.getCenterLatLng();
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.anchor(0.5f, 0.5f)
                    .icon(getBitmapDes(cluster)).position(latlng);
            Marker marker = mAMap.addMarker(markerOptions);
            ScaleAnimation mADDAnimation = new ScaleAnimation(0, 1, 0, 1);
//            AlphaAnimation mADDAnimation = new AlphaAnimation(0, 1);
            mADDAnimation.setInterpolator(new OvershootInterpolator());
            mADDAnimation.setDuration(300);
            marker.setAnimation(mADDAnimation);
            marker.setObject(cluster);

            marker.startAnimation();
            cluster.setMarker(marker);
            mAddMarkers.add(marker);
        } else {
            mAddMarkers.add(cluster.getMarker());
        }


    }


    private void calculateClusters() {
        Log.e("hpo", "calculateClusters: ");
        mIsCanceled = false;
        mClusters.clear();
        LatLngBounds visibleBounds = mAMap.getProjection().getVisibleRegion().latLngBounds;
        ArrayList<ClusterItem> clusterItems = new ArrayList<>();
        clusterItems.addAll(mClusterItems);
        for (ClusterItem clusterItem : clusterItems) {
            if (mIsCanceled) {
                return;
            }
            LatLng latlng = clusterItem.getPosition();
            if (visibleBounds.contains(latlng)) {
                Cluster cluster = getCluster(latlng, clusterItem.getMarkerInfo(), mClusters);
                if (cluster != null) {
                    cluster.addClusterItem(clusterItem);
                } else {
                    cluster = new Cluster(latlng, clusterItem.getMarkerInfo());
                    mClusters.add(cluster);
                    cluster.addClusterItem(clusterItem);
                }

            }
        }

        //复制一份数据，规避同步
//        List<Cluster> clusters = new ArrayList<Cluster>();
        if (mClustersAnimation == null) {
            mClustersAnimation = new ArrayList<>();
        } else {
            mClustersAnimation.clear();
        }
        mClustersAnimation.addAll(mClusters);
        for (int i = 0; i < mClustersAnimation.size(); i++) {
            for (int j = 0; j < mAddMarkers.size(); j++) {
                Cluster cluster = (Cluster) mAddMarkers.get(j).getObject();
                if (TextUtils.equals(mClustersAnimation.get(i).getKey(), cluster.getKey())) {
//                    mAddMarkers.get(j).setObject(mClustersAnimation.get(i));
                    mClustersAnimation.get(i).setMarker(mAddMarkers.get(j));
                    mAddMarkers.remove(j);
                }
            }
        }
        Message message = Message.obtain();
        message.what = MarkerHandler.ADD_CLUSTER_LIST;
        message.obj = mClustersAnimation;
        if (mIsCanceled) {
            return;
        }
        mMarkerhandler.sendMessage(message);
    }

    /**
     * 对点进行聚合
     */
    public void assignClusters() {
        mIsCanceled = true;
        mSignClusterHandler.removeMessages(SignClusterHandler.CALCULATE_CLUSTER);
        mSignClusterHandler.sendEmptyMessage(SignClusterHandler.CALCULATE_CLUSTER);
    }

    /**
     * 在已有的聚合基础上，对添加的单个元素进行聚合
     *
     * @param clusterItem
     */
    private void calculateSingleCluster(ClusterItem clusterItem) {
        LatLngBounds visibleBounds = mAMap.getProjection().getVisibleRegion().latLngBounds;
        LatLng latlng = clusterItem.getPosition();
        if (!visibleBounds.contains(latlng)) {
            return;
        }
        Cluster cluster = getCluster(latlng, clusterItem.getMarkerInfo(), mClusters);
        if (cluster != null) {
            cluster.addClusterItem(clusterItem);
            Message message = Message.obtain();
            message.what = MarkerHandler.UPDATE_SINGLE_CLUSTER;
            message.obj = cluster;
            mMarkerhandler.removeMessages(MarkerHandler.UPDATE_SINGLE_CLUSTER);
            mMarkerhandler.sendMessageDelayed(message, 5);
        } else {
            cluster = new Cluster(latlng, clusterItem.getMarkerInfo());
            mClusters.add(cluster);
            cluster.addClusterItem(clusterItem);
            Message message = Message.obtain();
            message.what = MarkerHandler.ADD_SINGLE_CLUSTER;
            message.obj = cluster;
            mMarkerhandler.sendMessage(message);

        }
    }

    /**
     * 根据一个点获取是否可以依附的聚合点，没有则返回null
     *
     * @param latLng
     * @return
     */
    private Cluster getCluster(LatLng latLng, MarkerInfo markerInfo, List<Cluster> clusters) {
        for (Cluster cluster : clusters) {
            LatLng clusterCenterPoint = cluster.getCenterLatLng();
            double distance = AMapUtils.calculateLineDistance(latLng, clusterCenterPoint);
            if (distance < mClusterDistance && mAMap.getCameraPosition().zoom <= 19) {
                return cluster;
            }
        }

        return null;
    }


    /**
     * 获取每个聚合点的绘制样式
     */
    private BitmapDescriptor getBitmapDes(Cluster cluster) {

        float corner[] = {DisplayLess.$dp2px(0), DisplayLess.$dp2px(0), DisplayLess.$dp2px(2), DisplayLess.$dp2px(2), DisplayLess.$dp2px(2), DisplayLess.$dp2px(2), DisplayLess.$dp2px(0), DisplayLess.$dp2px(0)};
        int num = cluster.getClusterCount();
        int categoryNo = cluster.getMarkerInfo().categoryNo;
        String merchantno = cluster.getMarkerInfo().merchantno;
        List<Integer> cateNos = getCateNos(cluster);
        String key;
        if (num == 1) {
            key = merchantno;
        } else {
            String s = "";
            for (int i = 0; i < cateNos.size(); i++) {
                s = s + cateNos.get(i);
            }
            key = merchantno + num + s;
        }
        BitmapDescriptor bitmapDescriptor = mLruCache.get(key);
//        BitmapDescriptor bitmapDescriptor = null;
        if (bitmapDescriptor == null) {
//            TextView textView = new TextView(mContext);
            View view = LayoutInflater.from(mContext).inflate(R.layout.map_marker_ly, null);
            RelativeLayout left_bg = (RelativeLayout) view.findViewById(R.id.left_bg);
            RelativeLayout text_wai = (RelativeLayout) view.findViewById(R.id.text_wai);
            TextView left_text = (TextView) view.findViewById(R.id.left_text);
            TextView des = (TextView) view.findViewById(R.id.des);
//            RelativeLayout marker_categroy = (RelativeLayout) view.findViewById(R.id.marker_categroy);
            LinearLayout marker_discount_ly = (LinearLayout) view.findViewById(R.id.marker_discount_ly);
            ImageView right = (ImageView) view.findViewById(R.id.right);
            ImageView center = (ImageView) view.findViewById(R.id.center);
            ImageView left = (ImageView) view.findViewById(R.id.left);
            RichTextView discount = (RichTextView) view.findViewById(R.id.discount);
            TextView text_deng = (TextView) view.findViewById(R.id.text_deng);
            LinearLayout des_ly = (LinearLayout) view.findViewById(R.id.des_ly);
            FrameLayout discount_fr = (FrameLayout) view.findViewById(R.id.discount_fr);
            ImageView map_icon_left = (ImageView) view.findViewById(R.id.map_icon_left);
            TextView left_text_jia = (TextView) view.findViewById(R.id.left_text_jia);
//            TextView marker_discount_zhe = (TextView) view.findViewById(R.id.marker_discount_zhe);

            if (TextUtils.isEmpty(cluster.getMarkerInfo().discount)) {
                discount_fr.setVisibility(View.GONE);
            } else {
                discount_fr.setVisibility(View.VISIBLE);
            }
            if (num > 1) {
                int colors[] = {0xfff62241, 0xfff62261, 0xfff6227b};
                GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, colors);
                gd.setCornerRadii(corner);
                discount_fr.setBackground(gd);
                map_icon_left.setVisibility(View.GONE);
                text_wai.setVisibility(View.VISIBLE);
//                marker_categroy.setVisibility(View.VISIBLE);
//                marker_discount_ly.setVisibility(View.GONE);
                String tile = String.valueOf(num < 99 ? num : 99);
                left_text.setTextColor(ContextCompat.getColor(mContext, R.color.red));
                left_text_jia.setTextColor(ContextCompat.getColor(mContext, R.color.red));
                left_text.setText(tile);
                des.setText(cluster.getMarkerInfo().des);
                text_deng.setVisibility(View.VISIBLE);
//                if (cateNos.size() == 1) {
//                    left.setVisibility(View.GONE);
//                    center.setVisibility(View.GONE);
//                    right.setBackgroundColor(ContextCompat.getColor(mContext, text_colors[cateNos.get(0)]));
//                    right.setImageResource(left_icon[cateNos.get(0)]);
//                } else if (cateNos.size() == 2) {
//                    left.setVisibility(View.GONE);
//                    center.setVisibility(View.VISIBLE);
//                    center.setBackgroundColor(ContextCompat.getColor(mContext, text_colors[cateNos.get(0)]));
//                    center.setImageResource(left_icon[cateNos.get(0)]);
//                    right.setBackgroundColor(ContextCompat.getColor(mContext, text_colors[cateNos.get(1)]));
//                    right.setImageResource(left_icon[cateNos.get(1)]);
//                } else {
//                    left.setVisibility(View.VISIBLE);
//                    center.setVisibility(View.VISIBLE);
//                    left.setBackgroundColor(ContextCompat.getColor(mContext, text_colors[cateNos.get(0)]));
//                    center.setBackgroundColor(ContextCompat.getColor(mContext, text_colors[cateNos.get(1)]));
//                    right.setBackgroundColor(ContextCompat.getColor(mContext, text_colors[cateNos.get(2)]));
//                    left.setImageResource(left_icon[cateNos.get(0)]);
//                    center.setImageResource(left_icon[cateNos.get(1)]);
//                    right.setImageResource(left_icon[cateNos.get(2)]);
//                }

//                left_bg.setBackgroundResource(leftBg_noicon[cluster.getClusterItems().get(0).getMarkerInfo().categoryNo]);
                discount.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                discount.setRichText(cluster.getMarkerInfo().discount + "起");
            } else {
                GradientDrawable gd = new GradientDrawable();
                gd.setCornerRadii(corner);
//                gd.setColor(ContextCompat.getColor(mContext, text_colors[cluster.getMarkerInfo().categoryNo]));
                gd.setColor(ContextCompat.getColor(mContext, R.color.c_FD392E));
                discount_fr.setBackground(gd);
                map_icon_left.setVisibility(View.VISIBLE);
                text_wai.setVisibility(View.GONE);
                map_icon_left.setImageResource(left_icon[cluster.getMarkerInfo().categoryNo]);
//                marker_categroy.setVisibility(View.GONE);
//                marker_discount_ly.setVisibility(View.VISIBLE);
//                discount.setTextColor(ContextCompat.getColor(mContext, text_colors[cluster.getMarkerInfo().categoryNo]));

//                marker_discount_zhe.setTextColor(ContextCompat.getColor(mContext, text_colors[cluster.getMarkerInfo().categoryNo]));
//                left_bg.setBackgroundResource(leftBg[cluster.getMarkerInfo().categoryNo]);
                des.setText(cluster.getMarkerInfo().des);
                text_deng.setVisibility(View.GONE);
                discount.setTextColor(ContextCompat.getColor(mContext, R.color.text_chunjie));
                discount.setRichText(cluster.getMarkerInfo().discount);
            }
            bitmapDescriptor = BitmapDescriptorFactory.fromView(view);
            mLruCache.put(key, bitmapDescriptor);

        }
        return bitmapDescriptor;
    }

    public List<Integer> getCateNos(Cluster cluster) {
        ArrayList<Integer> list = new ArrayList<>();
        for (ClusterItem clusterItem : cluster.getClusterItems()) {
            if (list.size() == 3) {
                return list;
            }
            if (!list.contains(clusterItem.getMarkerInfo().categoryNo)) {
                list.add(clusterItem.getMarkerInfo().categoryNo);
            }
        }
        return list;
    }

    /**
     * 更新已加入地图聚合点的样式
     */
    private void updateCluster(Cluster cluster) {

        Marker marker = cluster.getMarker();
        marker.setIcon(getBitmapDes(cluster));

    }


//-----------------------辅助内部类用---------------------------------------------

    /**
     * marker渐变动画，动画结束后将Marker删除
     */
    class MyAnimationListener implements Animation.AnimationListener {
        private List<Marker> mRemoveMarkers;

        MyAnimationListener(List<Marker> removeMarkers) {
            mRemoveMarkers = removeMarkers;
        }

        @Override
        public void onAnimationStart() {

        }

        @Override
        public void onAnimationEnd() {
            for (Marker marker : mRemoveMarkers) {
                marker.remove();
            }
            mRemoveMarkers.clear();
        }
    }

    /**
     * 处理market添加，更新等操作
     */
    class MarkerHandler extends Handler {

        static final int ADD_CLUSTER_LIST = 0;

        static final int ADD_SINGLE_CLUSTER = 1;

        static final int UPDATE_SINGLE_CLUSTER = 2;

        MarkerHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {

            switch (message.what) {
                case ADD_CLUSTER_LIST:
                    List<Cluster> clusters = (List<Cluster>) message.obj;
                    addClusterToMap(clusters);
                    break;
                case ADD_SINGLE_CLUSTER:
                    Cluster cluster = (Cluster) message.obj;
                    addSingleClusterToMap(cluster);
                    break;
                case UPDATE_SINGLE_CLUSTER:
                    Cluster updateCluster = (Cluster) message.obj;
                    updateCluster(updateCluster);
                    break;
            }
        }
    }

    /**
     * 处理聚合点算法线程
     */
    class SignClusterHandler extends Handler {
        static final int CALCULATE_CLUSTER = 0;
        static final int CALCULATE_SINGLE_CLUSTER = 1;

        SignClusterHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case CALCULATE_CLUSTER:
                    calculateClusters();
                    break;
                case CALCULATE_SINGLE_CLUSTER:
                    ClusterItem item = (ClusterItem) message.obj;
                    mClusterItems.add(item);
                    Log.i("yiyi.qi", "calculate single cluster");
                    calculateSingleCluster(item);
                    break;
            }
        }
    }

    public List<ClusterItem> getmClusterItems() {
        return mClusterItems;
    }

    public void addCluterItem(ArrayList<RegionItem> list) {
        mClusterItems.addAll(list);
    }

    public void clearMarkers() {
//        for (Marker marker : mAddMarkers) {
//            marker.remove();
//        }
        mClusterItems.clear();
//        mAddMarkers.clear();
//        mLruCache.evictAll();
    }
}