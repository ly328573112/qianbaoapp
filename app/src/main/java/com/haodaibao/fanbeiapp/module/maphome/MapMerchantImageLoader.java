package com.haodaibao.fanbeiapp.module.maphome;


import android.content.Context;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.haodaibao.fanbeiapp.R;
import com.youth.banner.loader.ImageLoaderInterface;

public class MapMerchantImageLoader implements ImageLoaderInterface<FrameLayout> {

    @Override
    public void displayImage(Context context, Object path, FrameLayout frameLayout) {
        Glide.with(context)
                .load(path)
                .asBitmap()
                .placeholder(R.drawable.home_default_backgroud)
                .error(R.drawable.home_default_backgroud)
                .centerCrop()
                .into((ImageView) frameLayout.findViewById(R.id.iv_banner));
    }

    @Override
    public FrameLayout createImageView(Context context) {
        FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(context)
                .inflate(R.layout.mapmerchant_banner_image_layout, null);
        return frameLayout;
    }
}