package com.haodaibao.fanbeiapp.module.pay.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.baseandroid.utils.FormatUtil;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.LableInfo;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 开发者：LuoYi
 * Time: 2018 17:44 2018/4/13 04
 */

public class PayActivityAdapter extends BaseRecycleViewAdapter<LableInfo> {

    private OnItemClickListener onItemClickListener;
    private int payIndex = 1;

    public PayActivityAdapter(Context context, OnItemClickListener onItemClickListener) {
        super(context);
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, final LableInfo item) {
        if (holder instanceof PayActivityHolder) {
            final PayActivityHolder payholder = (PayActivityHolder) holder;
            payholder.payItemTitieTv.setText(item.getLabelname());
            if (TextUtils.equals(item.getIsEnabled(), "0")) { //是可用，0：不可用 1：可用
                payholder.payItemCheckbox.setVisibility(View.INVISIBLE);
                payholder.payItemHintTv.setVisibility(View.VISIBLE);
                payholder.payItemHintTv.setText(item.getReason());
                payholder.payItemTitieTv.setBackgroundResource(R.drawable.bg_pay_activity);
            } else if (TextUtils.equals(item.getIsEnabled(), "1")) {
                payIndex++;
                payholder.payItemCheckbox.setVisibility(View.VISIBLE);
                payholder.payItemHintTv.setVisibility(View.INVISIBLE);
                if (payIndex % 2 == 0) {
                    payholder.payItemTitieTv.setBackgroundResource(R.drawable.label_red_btn_icon);
                } else {
                    payholder.payItemTitieTv.setBackgroundResource(R.drawable.label_yellow_btn_icon);
                }

                payholder.payItemCheckbox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (onItemClickListener != null) {
                            item.setLabelSelected(payholder.payItemCheckbox.isChecked());
                            if (item.isLabelSelected()) {
                                payholder.payItemAmountTv.setVisibility(View.VISIBLE);
                                String formatAmount = FormatUtil.formatAmount(2, (Double.parseDouble(item.getBenefitAmount()) / 100) + "");
                                payholder.payItemAmountTv.setText("-¥ " + formatAmount);
                            } else {
                                payholder.payItemAmountTv.setVisibility(View.INVISIBLE);
                            }
                            onItemClickListener.onItemClick(view, item);
                        }
                    }
                });
            }
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_pay_activity, parent, false);
        return new PayActivityHolder(view);
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class PayActivityHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.pay_item_titie_tv)
        TextView payItemTitieTv;
        @BindView(R.id.pay_item_hint_tv)
        TextView payItemHintTv;
        @BindView(R.id.pay_item_amount_tv)
        TextView payItemAmountTv;
        @BindView(R.id.pay_item_checkbox)
        CheckBox payItemCheckbox;

        public PayActivityHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
