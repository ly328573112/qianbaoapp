package com.haodaibao.fanbeiapp.module.merchant;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amap.api.maps.TextureMapView;
import com.android.photocameralib.media.ImageGalleryActivity;
import com.baseandroid.config.Api;
import com.baseandroid.config.Global;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.FormatUtil;
import com.baseandroid.utils.LogUtil;
import com.baseandroid.utils.ToastUtils;
import com.baseandroid.widget.RouteeFlowLayout;
import com.baseandroid.widget.animatorprovider.FadeAlphaAnimatorProvider;
import com.baseandroid.widget.customlistview.CustomLinearLayoutManager;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.github.nukc.stateview.StateView;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.event.MerchantEvent;
import com.haodaibao.fanbeiapp.event.PaylistEvent;
import com.haodaibao.fanbeiapp.module.CommentClickListener;
import com.haodaibao.fanbeiapp.module.combo.ComboDetailActivity;
import com.haodaibao.fanbeiapp.module.discount.DiscountExplainActivity;
import com.haodaibao.fanbeiapp.module.evaluate.EvaluateListActivity;
import com.haodaibao.fanbeiapp.module.home.GlideImageLoader;
import com.haodaibao.fanbeiapp.module.login.LoginActivity;
import com.haodaibao.fanbeiapp.module.map.GaodeMapActivity;
import com.haodaibao.fanbeiapp.module.merchant.adapter.ComboListAdapter;
import com.haodaibao.fanbeiapp.module.merchant.adapter.ConsumeHintAdapter;
import com.haodaibao.fanbeiapp.module.merchant.adapter.MerchantDetailAdapter;
import com.haodaibao.fanbeiapp.module.merchant.adapter.MerchantDiscountAdapter;
import com.haodaibao.fanbeiapp.module.merchant.temporarydata.CommentTemporary;
import com.haodaibao.fanbeiapp.module.merchant.temporarydata.PreferredTemporary;
import com.haodaibao.fanbeiapp.module.pay.PayActivity;
import com.haodaibao.fanbeiapp.repository.json.CommentListBean;
import com.haodaibao.fanbeiapp.repository.json.ErrorRecoveryList;
import com.haodaibao.fanbeiapp.repository.json.LableInfo;
import com.haodaibao.fanbeiapp.repository.json.MerchantImageInfo;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoResp;
import com.haodaibao.fanbeiapp.repository.json.PackageListBean;
import com.haodaibao.fanbeiapp.repository.json.PeriodsInfo;
import com.haodaibao.fanbeiapp.repository.json.PriedListBean;
import com.jayfeng.lesscode.core.DisplayLess;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.listener.OnBannerListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hugo.weaving.DebugLog;

import static com.haodaibao.fanbeiapp.module.pay.PayActivity.MERCHANT_DETAIL;

@DebugLog
public class MerchantDetailActivity extends CoorNestedScrollActivity implements MerchantContract.MerchantView, View.OnClickListener, CommentClickListener {

    RelativeLayout rlyViewpager;
    Banner viewpagerMer;
    TextView merchantName;
    TextView merchantClassification;
    TextView merchantAverage;
    LinearLayout merchantDiscountTitleLy;
    TextView merchantRule;
    TextView merchantOhter;
    ImageView merchantIconPhone;
    TextView merchantTime;
    TextView merchantAddr;
    TextureMapView bmapView;
    View mapviewLayout;
    View viewDiscountLine;

    RecyclerView consumeHintListView;
    RecyclerView mRcy;
    RecyclerView rvDiscountList;
    RecyclerView comboList;

    /**
     * 套餐布局
     */
    RelativeLayout comboLinear;
    private String merchantno = "";

    private Map<String, String> calculationMap = new HashMap<>();
    private MerchantDataPresenter merchantDataPresenter;
    private MerchantDialogView merchantDialogView;
    private MerchantDetailAdapter detailAdapter;
    private ComboListAdapter comboListAdapter;

    private MerchantInfoEntiy merchantInfo;
    private LinearLayoutManager linearLayout;
    private MerchantDiscountAdapter discountAdapter;

    private MerchantErrorView merchantErrorView;
    private List<ErrorRecoveryList.ErrorResult> errorResult;

    private StateView stateView;
    private View commentInflate;
    private TextView commentCount;
    private RouteeFlowLayout merchantLabelRfl;
    private LinearLayout titleLl;

    private LinearLayout bhh_support_ll;


    @Override
    protected void initTitle() {
        EventBus.getDefault().register(this);
        addNestedScrollView(R.layout.activity_merchant_detail);
        initMerchantView();
        initListener();
        merchantDataPresenter = new MerchantDataPresenter(this);
        merchantDialogView = new MerchantDialogView(this);
        merchantErrorView = new MerchantErrorView(this, merchantDataPresenter);
        gotoBuy.setOnClickListener(this);
    }


    private void initStateView(MerchantInfoEntiy merchantInfo) {
        stateView = StateView.inject(rootRelat);
        stateView.setEmptyResource(R.layout.base_empty_title);
        stateView.setLoadingResource(R.layout.base_loading_title);
        stateView.setRetryResource(R.layout.base_retry_title);
        if (merchantInfo == null) {
            stateView.showLoading();
            stateView.setAnimatorProvider(new FadeAlphaAnimatorProvider());
        }
        stateView.setOnRetryClickListener(new StateView.OnRetryClickListener() {
            @Override
            public void onRetryClick() {
                getNetworkData();
            }
        });
        stateView.setOnInflateListener(new StateView.OnInflateListener() {
            @Override
            public void onInflate(int viewType, View view) {
                ImageView iconNavigation = (ImageView) view.findViewById(R.id.icon_navigation);
                iconNavigation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
            }
        });
    }

    private void initMerchantView() {
        merchantDiscountTitleLy = findViewById(R.id.merchant_discount_title_ly);
        merchantName = findViewById(R.id.merchant_name);

        merchantClassification = findViewById(R.id.merchant_classification);
        merchantAverage = findViewById(R.id.merchant_average);

        bmapView = findViewById(R.id.bmapView);
        findViewById(R.id.merchant_address_ll).setOnClickListener(this);
        mapviewLayout = findViewById(R.id.mapview_layout);
        viewDiscountLine = findViewById(R.id.view_discount_line);

        merchantOhter = findViewById(R.id.merchant_ohter);
//        discount_layout = (LinearLayout) findViewById(R.id.discount_layout);
        merchantRule = findViewById(R.id.merchant_rule);
        merchantIconPhone = findViewById(R.id.merchant_icon_phone);
        merchantTime = findViewById(R.id.merchant_time);
        merchantAddr = findViewById(R.id.merchant_addr);

        rlyViewpager = findViewById(R.id.rly_viewpager);
        viewpagerMer = findViewById(R.id.viewpager_mer);


        comboLinear = findViewById(R.id.combo_linear);
        comboLinear.setVisibility(View.GONE);

        titleLl = findViewById(R.id.title_ll);
        merchantLabelRfl = findViewById(R.id.merchant_label_rfl);

        comboList = findViewById(R.id.combo_list);
        mRcy = findViewById(R.id.rcy);
        rvDiscountList = findViewById(R.id.rv_discount_list);
        consumeHintListView = findViewById(R.id.consume_hint_ListView);

        bhh_support_ll = findViewById(R.id.bhh_support_ll);
    }

    private void setRecyclerViewAttribute(RecyclerView recyclerView) {
        recyclerView.setFocusable(false);
        customScrollview.fullScroll(NestedScrollView.FOCUS_UP);
        customScrollview.setOverScrollMode(View.OVER_SCROLL_NEVER);
    }

    private void initListener() {
        merchantIconPhone.setOnClickListener(this);
        customScrollview.setGiantView(rlyViewpager);
        customScrollview.setChangeView(merchantDiscountTitleLy);
        linearLayout = new LinearLayoutManager(this);
        linearLayout.setSmoothScrollbarEnabled(true);
        linearLayout.setAutoMeasureEnabled(true);
        detailAdapter = new MerchantDetailAdapter(this, this);
        mRcy.setLayoutManager(linearLayout);
        mRcy.setAdapter(detailAdapter);
        setRecyclerViewAttribute(mRcy);

        setRecycleListView();
    }

    private void setRecycleListView() {
        linearLayout = new LinearLayoutManager(this) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };

        discountAdapter = new MerchantDiscountAdapter(this, onItemClickListener);
        rvDiscountList.setLayoutManager(linearLayout);
        setRecyclerViewAttribute(rvDiscountList);
        rvDiscountList.setAdapter(discountAdapter);

        linearLayout = new LinearLayoutManager(this) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        comboListAdapter = new ComboListAdapter(this, onItemClickListener);
        comboList.setLayoutManager(linearLayout);
        comboList.setAdapter(comboListAdapter);
        setRecyclerViewAttribute(comboList);
    }

    BaseRecycleViewAdapter.OnItemClickListener onItemClickListener = new BaseRecycleViewAdapter.OnItemClickListener() {

        @Override
        public <T> void onItemClick(View view, T t) {
            if (t instanceof PackageListBean) {
                // TODO: 2017/10/17 跳转套餐详情页
                PackageListBean bean = (PackageListBean) t;
                ComboDetailActivity.start(MerchantDetailActivity.this, bean.getPackageNo(), merchantno, merchantInfo);
            } else if (t instanceof PriedListBean) {
                PriedListBean listBean = (PriedListBean) t;
                if (!TextUtils.isEmpty(listBean.isShow) && listBean.isShow.equals("1")) {
                    goBuy();
                } else if (!TextUtils.isEmpty(listBean.isShow) && listBean.isShow.equals("2")) {
                    DiscountExplainActivity.start(MerchantDetailActivity.this, merchantno, listBean.activityno);
                }
            }
        }
    };

    @DebugLog
    @Override
    protected void intitTop(Bundle savedInstanceState) {
        Intent intent = getIntent();
        merchantno = intent.getStringExtra("merchantno");
        merchantInfo = (MerchantInfoEntiy) intent.getSerializableExtra("merchantInfo");
        if (merchantInfo != null) {
            updateUI(null, merchantInfo, null);
            List<PackageListBean> packageLists = merchantInfo.packageList;
            List<PriedListBean> priedList = merchantInfo.priedList;
            setDiscountAdapter(priedList);
            if (packageLists == null || packageLists.size() == 0) {
                comboLinear.setVisibility(View.GONE);
            } else {
                comboLinear.setVisibility(View.VISIBLE);
                comboListAdapter.resetData(packageLists);
            }
        }
        bmapView.onCreate(savedInstanceState);
        initStateView(merchantInfo);
        getNetworkData();
    }

    @DebugLog
    private void getNetworkData() {
        merchantDataPresenter.getMerchantDetail(merchantno);
        merchantDataPresenter.getErrorRecoveryList();
    }

    @Override
    protected void onBaseScrolled(RecyclerView recyclerView, int dx, int dy) {
//        gotoBuy.setText(getDiscountTitle(calculationMap.get("discount_title")) + ", 去买单");
//        if (linearLayout.findViewByPosition(1) == null) {
//            gotoBuy.setRichText(calculationMap.get("discount_title"));
//        } else {
//            gotoBuy.setText("去买单");
//        }
    }

    @Override
    protected void destroy() {
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void buyChanged(int changed) {
//        if (changed == 0) {
//            gotoBuy.setText("去买单");
//        } else {
//        }
    }

    @Override
    public void goToAllComment() {
        EvaluateListActivity.open(this, merchantno);
    }

    @Override
    public void goToEnlarge(List<String> imglists, int pos) {
        String[] strings = imglists.toArray(new String[imglists.size()]);
        ImageGalleryActivity.show(MerchantDetailActivity.this, strings, pos, false);
    }

    @Override
    public void goToMerchantDetail(String merchantno) {
        Bundle bundle = new Bundle();
        bundle.putString("merchantno", merchantno);
        ActivityJumpUtil.next(MerchantDetailActivity.this, MerchantDetailActivity.class, bundle, 0);
    }

    @Override
    public void setmRightIconClick() {
        String imgurl = CommonUtils.getImageUrl(merchantInfo.getImageurl());
        String link = Api.MER_SHARE_LINK + "?" + "merchantno=" + merchantno;
        String[] c = new String[]{imgurl, link};
        Bundle bundle = new Bundle();
        bundle.putString("title", merchantInfo.getMerchantname());
        bundle.putString("circle", getDiscountTitle(calculationMap.get("discount_title")));
        bundle.putString("desc", "用【钱包生活】买单立享优惠哦！地址：" + merchantInfo.getAddress());
        bundle.putStringArray("content", c);
        merchantDialogView.showShareDialog(bundle);
        super.setmRightIconClick();
    }

    private String getDiscountTitle(String discountTitle) {
        if (!TextUtils.isEmpty(discountTitle)) {
            discountTitle = discountTitle.replace("<center>", "");
            discountTitle = discountTitle.replace("<big>", "");
            discountTitle = discountTitle.replace("</big>", "");
            discountTitle = discountTitle.replace("</center>", "");
            discountTitle = discountTitle.replace("<font color='#F62241'>", "");
            discountTitle = discountTitle.replace("</font>", "");
        }
        return discountTitle;
    }

    @Override
    protected void follow() {
        if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
            ActivityJumpUtil.next(MerchantDetailActivity.this, LoginActivity.class, new Bundle(), 0);
        } else {
            merchantDataPresenter.followMerchant(merchantno, cbFocus.isChecked());
        }
    }

    @Override
    protected void errorReport() {
        if (errorResult == null || errorResult.size() == 0) {
            return;
        }
        for (ErrorRecoveryList.ErrorResult result : errorResult) {
            if (TextUtils.equals(result.code, "0")) {
                errorResult.remove(result);
            }
        }
        ErrorRecoveryList.ErrorResult cancelResult = new ErrorRecoveryList.ErrorResult();
        cancelResult.code = "0";
        cancelResult.name = "取消";
        cancelResult.desc = "取消";
        errorResult.add(cancelResult);
        merchantErrorView.initErrorDialog(this.errorResult, merchantno);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.merchant_icon_phone:
                merchantDialogView.callDialog(merchantInfo.getPhone());
                break;
            case R.id.goto_buy:
                goBuy();
                break;
            case R.id.merchant_address_ll:
            case R.id.mapview_layout:
                GaodeMapActivity.open(this, merchantInfo.getLongitude(), merchantInfo.getLatitude(), merchantInfo.getImageurl(), merchantInfo
                        .getMerchantname(), merchantInfo.getMerchantno(), merchantInfo.getPhone(), merchantInfo
                        .getAddress());
                break;
            case R.id.title_ll:
                if (merchantInfo.getLabels() != null && merchantInfo.getLabels().size() > 0) {
                    merchantDialogView.discountDialog(merchantInfo.getLabels());
                }
                break;
            default:
                break;
        }
    }

    private void goBuy() {
        if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
            ActivityJumpUtil.next(MerchantDetailActivity.this, LoginActivity.class, new Bundle(), 0);
        } else {
            PayActivity.openForResult(MerchantDetailActivity.this, merchantInfo, merchantno, MERCHANT_DETAIL, false);
        }
    }

    private void updateUI(Map<String, String> calculationMap, MerchantInfoEntiy merchantInfo, List<PeriodsInfo> perList) {
        setBannerUrl(merchantInfo.getMerchantImageList());
        merchantName.setText(merchantInfo.getMerchantname());
        if (!TextUtils.isEmpty(merchantInfo.getSubcategoryname())) {
            merchantClassification.setText(merchantInfo.getSubcategoryname());
            merchantClassification.setVisibility(View.VISIBLE);
        } else if (!TextUtils.isEmpty(merchantInfo.getCategoryName())) {
            merchantClassification.setText(merchantInfo.getCategoryName());
            merchantClassification.setVisibility(View.VISIBLE);
        } else {
            merchantClassification.setVisibility(View.GONE);
        }
        String filterDecimalPoint = FormatUtil.filterDecimalPoint(merchantInfo.getPersonaverage());
        merchantAverage.setText(getString(R.string.rmb_aveage) + filterDecimalPoint);

        if (merchantInfo.getOpentimeext().length() < 2) {
            merchantTime.setText(merchantInfo.getOpentime().replace(",", "-"));
        } else {
            merchantTime.setText(merchantInfo.getOpentime()
                    .replace(",", "-") + "，" + merchantInfo.getOpentimeext()
                    .replace(",", "-"));
        }
        merchantAddr.setText(merchantInfo.getAddress());

        new MerchantMapView(this).initMapView(bmapView, merchantInfo.getLatitude(), merchantInfo.getLongitude(), merchantInfo.getMerchantname());
        mapviewLayout.setOnClickListener(this);

        if (TextUtils.isEmpty(merchantInfo.getRefundnote()) && (perList == null || perList.size() <= 1)) {
            merchantOhter.setVisibility(View.GONE);
            consumeHintListView.setVisibility(View.GONE);
            merchantRule.setVisibility(View.GONE);
        } else {
            merchantOhter.setVisibility(View.VISIBLE);
            consumeHintListView.setVisibility(View.VISIBLE);
            ConsumeHintAdapter newhintAdapter = new ConsumeHintAdapter(this);
            newhintAdapter.setRefundmode(merchantInfo.getRefundmode());
            CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(this);
            consumeHintListView.setLayoutManager(linearLayoutManager);
            consumeHintListView.setAdapter(newhintAdapter);
            setRecyclerViewAttribute(rvDiscountList);
        }
        if (TextUtils.isEmpty(merchantInfo.getRefundnote())) {
            merchantRule.setVisibility(View.GONE);
        } else {
            merchantRule.setVisibility(View.VISIBLE);
            merchantRule.setText("使用规则：" + merchantInfo.getRefundnote().replace("<br />", "\n"));
        }
        if (TextUtils.isEmpty(merchantInfo.getPhone())) {
            merchantIconPhone.setVisibility(View.GONE);
        } else {
            merchantIconPhone.setVisibility(View.VISIBLE);
        }

        try {
            if (TextUtils.isEmpty(Global.getUserInfo().getUserno()) || TextUtils.equals(getDiscountTitle(calculationMap.get("discount_title")), "去买单") ||
                    TextUtils.isEmpty(merchantInfo.getRefundmode()) || TextUtils.equals(merchantInfo.getRefundmode(), "0")) {
                gotoBuy.setText("去买单");
            } else {
                gotoBuy.setText(getDiscountTitle(calculationMap.get("discount_title")) + ", 去买单");
            }
        } catch (Exception e) {
            gotoBuy.setText("去买单");
        }

        if (merchantInfo.getLabels() != null && merchantInfo.getLabels().size() > 0) {
            titleLl.setVisibility(View.VISIBLE);
            titleLl.setOnClickListener(this);
            setRouteeView(merchantLabelRfl, merchantInfo.getLabels());
        } else {
            titleLl.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(merchantInfo.getBhhenable()) || merchantInfo.getBhhenable().equals("0")) {
            bhh_support_ll.setVisibility(View.GONE);
        } else if (merchantInfo.getBhhenable().equals("1")) {
            bhh_support_ll.setVisibility(View.VISIBLE);
        }
    }

    private void setRouteeView(RouteeFlowLayout itemPackageRfl, List<LableInfo> lableList) {
        itemPackageRfl.removeAllViews();
        int labelTag = 1;
        for (LableInfo lableInfo : lableList) {
            labelTag++;
            TextView itemNewPackge = (TextView) LayoutInflater.from(MerchantDetailActivity.this).inflate(R.layout.item_label_package, null);
            TextView labelTv = itemNewPackge.findViewById(R.id.item_label_tv);
            labelTv.setText(lableInfo.getLabelname());
            if (labelTag % 2 == 0) {
                labelTv.setBackgroundResource(R.drawable.label_red_btn_icon);
            } else {
                labelTv.setBackgroundResource(R.drawable.label_yellow_btn_icon);
            }
            itemPackageRfl.addView(itemNewPackge);
        }
    }

    private void setDiscountAdapter(List<PriedListBean> priedList) {
        if (priedList == null || priedList.size() == 0) {
            merchantDiscountTitleLy.setVisibility(View.GONE);
            viewDiscountLine.setVisibility(View.GONE);
            return;
        }
        merchantDiscountTitleLy.setVisibility(View.VISIBLE);
        viewDiscountLine.setVisibility(View.VISIBLE);
        discountAdapter.resetData(priedList);
    }

    private void setBannerUrl(final List<MerchantImageInfo> imageList) {
        List<Object> imgurlList = new ArrayList<>();
        if (imageList == null || imageList.size() == 0) {
            imgurlList.add(R.drawable.home_default_backgroud);
        } else {
            for (MerchantImageInfo merchantImageInfo : imageList) {
                imgurlList.add(CommonUtils.getImageUrl(merchantImageInfo.getImageurl()));
            }
        }
        viewpagerMer.setImages(imgurlList);
        viewpagerMer.setBannerStyle(BannerConfig.NUM_INDICATOR);
        viewpagerMer.setIndicatorGravity(BannerConfig.RIGHT);
        viewpagerMer.setImageLoader(new GlideImageLoader());
        viewpagerMer.setOnBannerListener(new OnBannerListener() {
            @Override
            public void OnBannerClick(int position) {
                if (Integer.parseInt(merchantInfo.getAlbum()) == 0) {
                    return;
                }
                MerchantImageInfo merchantImage = imageList.get(position);
                Bundle b = new Bundle();
                b.putString("merchantno", merchantImage.getMerchantno());
//                ActivityJumpUtil.next(MerchantDetailActivity.this, MerchantDetailActivity.class, bundle, 0);
            }
        });
        viewpagerMer.start();
    }

    @Override
    public void onMerchantDetail(String attention, MerchantInfoEntiy merchantInfo, List<PeriodsInfo> perList, List<PriedListBean> priedList, List<PackageListBean> packageLists) {
        this.merchantInfo = merchantInfo;
        setmTittleText(merchantInfo.getMerchantname());
        // 该套餐是否被关注
        boolean atten = Boolean.parseBoolean(attention);
        cbFocus.setChecked(atten);
        if (cbFocus.isChecked()) {
            cbFocus.setBackgroundResource(R.drawable.icon_collection_red);
        } else {
            cbFocus.setBackgroundResource(R.drawable.icon_collection_black);
        }
        calculationMap = merchantDataPresenter.getRNamount(merchantInfo);
        updateUI(calculationMap, merchantInfo, perList);
        setDiscountAdapter(priedList);
        merchantDataPresenter.getCommentNearbyList(merchantno, "6", merchantInfo.getLongitude(), merchantInfo.getLatitude(), merchantInfo.getCityCode());
        if (packageLists == null || packageLists.size() == 0) {
            comboLinear.setVisibility(View.GONE);
        } else {
            comboLinear.setVisibility(View.VISIBLE);
            comboListAdapter.resetData(packageLists);
        }

    }

    @Override
    public void setNavigationClick() {
        super.setNavigationClick();
    }

    @Override
    public void onFollow(String status) {
        if (TextUtils.isEmpty(status) || TextUtils.equals(status, "404")) {
            ToastUtils.showLongToast("收藏失败");
            return;
        }
        cbFocus.setChecked(!cbFocus.isChecked());
        if (mAlpha > 125) {
            if (cbFocus.isChecked()) {
                cbFocus.setBackgroundResource(R.drawable.icon_collection_red);
            } else {
                cbFocus.setBackgroundResource(R.drawable.icon_collection_black);
            }
        } else {
            if (cbFocus.isChecked()) {
                cbFocus.setBackgroundResource(R.drawable.icon_collection_red);
            } else {
                cbFocus.setBackgroundResource(R.drawable.icon_collection_black);
            }
        }
        if (cbFocus.isChecked()) {
            ToastUtils.showLongToast("收藏成功");
        } else {
            ToastUtils.showLongToast("取消收藏");
        }
    }

    @Override
    public void onCommentList(CommentListBean commentListBean, boolean isComment) {
        LogUtil.e("评价列表");
        List<CommentListBean.CommentBean> commentBeanList = commentListBean.getList().getItems();
        if (commentBeanList.size() == 0) {
            return;
        }

        if (commentInflate == null) {
            commentInflate = LayoutInflater.from(this).inflate(R.layout.comment_header_ly, null);
            commentCount = (TextView) commentInflate.findViewById(R.id.comment_count);
            commentCount.setText(commentListBean.getCount() + "条评价");
            commentInflate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToAllComment();
                }
            });
            detailAdapter.addHeaderView(commentInflate);
        } else {
            commentCount.setText(commentListBean.getCount() + "条评价");
            detailAdapter.notifyDataSetChanged();
        }

        detailAdapter.resetData(commentListBean.getList().getItems());
        detailAdapter.addData(new CommentTemporary());

        if (isComment) {
//        customScrollview.scrollTo(0, mRcy.getTop() - inflate.getHeight());
            customScrollview.postDelayed(new Runnable() {
                @Override
                public void run() {
                    customScrollview.scrollTo(0, mRcy.getTop() - (commentInflate.getHeight() - DisplayLess.$dp2px(30)));
                }
            }, 500);
        }
    }

    @Override
    public void onNearby(MerchantInfoResp merchantInfoResp) {
        LogUtil.e("附近商户列表");
        detailAdapter.addData(new PreferredTemporary());
        List<MerchantInfoEntiy> merchantList = merchantInfoResp.getMerchantList();
        for (MerchantInfoEntiy merchantInfo : merchantList) {
            if (merchantInfo.getMerchantno().equals(merchantno)) {
                merchantList.remove(merchantInfo);
                break;
            }
        }
        detailAdapter.addData(merchantList);
        View view1 = LayoutInflater.from(this).inflate(R.layout.item_merchant_wenan, null);
        detailAdapter.setFooterView(view1);
    }

    @Override
    public void onRefreshList() {
        LogUtil.e("数据请求完成~~~~");
        detailAdapter.notifyDataSetChanged();
    }

    @Override
    public void onFail() {

    }

    @Override
    public void onDismiss() {

    }

    @Override
    public void closeActivity() {
//        ToastUtils.showShortToast("商户信息异动，无法显示");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                MerchantDetailActivity.this.finish();
            }
        }, 3000);
    }

    @Override
    public void onRetry() {
        stateView.postDelayed(new Runnable() {
            @Override
            public void run() {
                stateView.showRetry();
            }
        }, 200);

    }

    @Override
    public void onContent() {
        stateView.showContent();
    }

    @Override
    public void onErrorRecovery(ErrorRecoveryList errorRecoveryList) {
        errorResult = errorRecoveryList.result;
    }

    @Override
    public void onErrorCommint(String status) {
        if (status.startsWith("2")) {
            ToastUtils.showLongToast("感谢你的反馈，我们会尽快处理", 3000);
        } else {
            ToastUtils.showLongToast("提交失败");
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MerchantEvent event) {
        getNetworkData();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PaylistEvent event) {
        getNetworkData();
        merchantDataPresenter.getCommentL(merchantno, event.isComment());
    }

    @Override
    protected void onResume() {
        super.onResume();
        bmapView.onResume();
    }

    @Override
    protected void onPause() {
        bmapView.onPause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        bmapView.onDestroy();
        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }
}
