package com.haodaibao.fanbeiapp.module.bizutils;

import com.baseandroid.config.Global;
import com.haodaibao.fanbeiapp.repository.json.ShanghufenleiList;

import java.util.List;

/**
 * 开发者：LuoYi
 * Time: 2017 15:30 2017/10/12 10
 */

    /* 商户筛选 */
public class CategoryFenlei {
    private static String a = "";

    // 获取商户筛选条件
    public static String getCategoryParentName(String parentno) {
        List<ShanghufenleiList> list = Global.getCategorys();
        for (int i = 0; i < list.size(); i++) {
            if (parentno.equals(list.get(i).getCategoryno())) {
                a = list.get(i).getName();
            }
        }
        return a;
    }
}
