package com.haodaibao.fanbeiapp.module.personal.message;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.baseandroid.base.BaseFragment;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.baseandroid.widget.recycleadpter.CustomPtrHeader;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.event.MessageEvent;
import com.haodaibao.fanbeiapp.repository.json.MessageListBean;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;

/**
 * Created by Routee on 2017/9/21.
 * description: ${cusor}
 */

public class MessageFragment extends BaseFragment implements MessageListContract.View, BaseRecycleViewAdapter.OnLoadMoreListener {
    @BindView(R.id.rv)
    RecyclerView mRv;
    @BindView(R.id.ptr)
    PtrClassicFrameLayout mPtr;
    private int mCurrentPage = 1;
    private String mType = "ALL";
    MessagePresenter mPresenter;
    MessageRvAdapter mRvAdapter;
    private MessageListActivity mActivity;
    private boolean isEdit;
    private String mMsgNo;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_messagelist;
    }

    @Override
    protected void setupView() {
        EventBus.getDefault().register(this);
        mPtr.disableWhenHorizontalMove(true);
        mPtr.setPtrHandler(new PtrDefaultHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                mCurrentPage = 1;
                getRvData();
            }
        });
        if (mRvAdapter == null) {
            mRvAdapter = new MessageRvAdapter(getContext(), isEdit);
            mRvAdapter.setLoadEndContent("没有更多消息了~");
        }
        mRvAdapter.setOnClickListen(new MessageRvAdapter.Onclick() {
            @Override
            public void itemClick(MessageListBean.ResultBean.ItemsBean bean) {
                setMsgReaded();
                bean.setReadstatus("1");
                mRvAdapter.notifyDataSetChanged();
                // TODO: 2017/9/22 要跳转至不同的页面
                if (bean.getReadstatus().equals("0")) {
                    MessageEvent event = new MessageEvent();
                    event.setDelete(false);
                    event.setMsgNo(bean.getMessageno());
                    event.setType(mType);
                    EventBus.getDefault().post(event);
                }
                MessageJumpBean jumpBean = mPresenter.messageJump(bean);
                if (jumpBean.getaClass() != null) {
                    ActivityJumpUtil.next(mActivity, jumpBean.getaClass(), jumpBean.getaBundle(), 0);
                }
                if (jumpBean.closeActivity) {
                    mActivity.finish();
                }

            }
        });
        mRv.setAdapter(mRvAdapter);
        mRv.setLayoutManager(new LinearLayoutManager(getContext()));
        CustomPtrHeader customPtrHeader = new CustomPtrHeader(getContext());
        mPtr.setHeaderView(customPtrHeader);
        mPtr.addPtrUIHandler(customPtrHeader);
        mRvAdapter.setOnLoadMoreListener(this, mRv);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        Bundle b = getArguments();
        mType = b.getString("type");
        getRvData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity = (MessageListActivity) getActivity();
        isEdit = mActivity.isEdit();
    }

    //获取网络数据
    public void getRvData() {
        if (mPresenter == null) {
            mPresenter = new MessagePresenter(this);
        }
        mPresenter.getMessageList(mCurrentPage + "", mType);
    }

    //获取数据MessageList成功
    @Override
    public void setupMessageListData(MessageListBean bean) {
        //不能在此处调用。服务器返回的消息第二页为0时bean.getResult.getPage()为1
        //try {
        //    mCurrentPage = Integer.parseInt(bean.getResult().getPage());
        //} catch (NumberFormatException e) {
        //    e.printStackTrace();
        //}
        if (mRvAdapter.getEmptyView() == null) {
            mRvAdapter.setEmptyView(R.layout.layout_empty_message_list);
        }
        List<MessageListBean.ResultBean.ItemsBean> list = bean.getResult().getItems();
        if (mCurrentPage <= 1) {
            try {
                mCurrentPage = Integer.parseInt(bean.getResult().getPage());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            if (list.size() < 20) {
                mRvAdapter.loadMoreEnd(false);
            }
            mRvAdapter.resetData(list);
            mPtr.refreshComplete();
        } else {
            try {
                mCurrentPage = Integer.parseInt(bean.getResult().getPage());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            if (list.size() == 0) {
                mRvAdapter.loadMoreEnd(false);
                ((MessageListActivity) getActivity()).calEditable(this);
                return;
            }
            if (list.size() < 20) {
                mRvAdapter.addData(list);
                mRvAdapter.loadMoreComplete();
                mRvAdapter.loadMoreEnd(false);
                ((MessageListActivity) getActivity()).calEditable(this);
                return;
            }
            mRvAdapter.addData(list);
            mRvAdapter.loadMoreComplete();
        }
        ((MessageListActivity) getActivity()).calEditable(this);
    }

    //设置选中消息为已读请求接口
    public void setMsgReaded() {
        mMsgNo = mRvAdapter.getCheckedMsg();
        mPresenter.setMsgReaded(mMsgNo);
    }

    //设置选中消息已读成功回调
    @Override
    public void setMsgReadedSuccess() {
        mRvAdapter.setReaded();
        MessageEvent event = new MessageEvent();
        event.setDelete(false);
        event.setType(mType);
        event.setMsgNo(mMsgNo);
        EventBus.getDefault().post(event);
    }

    //删除消息
    public void deleteMsg() {
        mMsgNo = mRvAdapter.getCheckedMsg();
        mPresenter.deleteMsg(mMsgNo);
    }

    //消息删除成功
    @Override
    public void deleteMsgSuccess() {
        mRvAdapter.deleteMsg();
        ((MessageListActivity) getActivity()).calEditable(this);
        MessageEvent event = new MessageEvent();
        event.setType(mType);
        event.setDelete(true);
        event.setMsgNo(mMsgNo);
        EventBus.getDefault().post(event);
    }

    //通知可编辑
    public void notifyEditable(boolean editable) {
        isEdit = editable;
        if (mRvAdapter != null) {
            mRvAdapter.setEditable(editable);
            mRvAdapter.notifyDataSetChanged();
        }
    }

    //加载更多
    @Override
    public void onLoadMore() {
        mPresenter.getMessageList(++mCurrentPage + "", mType);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent event) {
        if (event.getType().equals(mType)) {
            return;
        }
        if (event.getType().equals("ALL") || mType.equals("ALL")) {
            if (event.isDelete()) {
                mRvAdapter.removeData(event.getMsgNo());
            } else {
                mRvAdapter.readData(event.getMsgNo());
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    //检查消息列表页是否可编辑
    public boolean checkEditable() {
        if (mRvAdapter == null) {
            return false;
        } else if (mRvAdapter.getData() == null || mRvAdapter.getData().size() == 0) {
            return false;
        }
        return true;
    }

    public String getType() {
        return mType;
    }
}
