package com.haodaibao.fanbeiapp.module;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.LogUtil;
import com.baseandroid.widget.customimageview.CropSquareTransformation;
import com.bumptech.glide.Glide;
import com.haodaibao.fanbeiapp.R;

import java.util.ArrayList;
import java.util.List;


public class CommentImageAdapter extends RecyclerView.Adapter<CommentImageAdapter.CommentImageHolder> {
    private List<String> imgList;
    private Context mContext;
    private CommentClickListener commentClickListener;

    public CommentImageAdapter(List<String> imgList, Context mContext, CommentClickListener commentClickListener) {
        this.imgList = imgList;
        this.mContext = mContext;
        this.commentClickListener = commentClickListener;
    }

    @Override
    public CommentImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ImageView iv = new ImageView(mContext);
        iv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        iv.setScaleType(ImageView.ScaleType.FIT_XY);
        iv.setAdjustViewBounds(true);
        return new CommentImageHolder(iv);
    }

    @Override
    public void onBindViewHolder(final CommentImageHolder holder, final int position) {
        if (imgList.size() == 0) {
            return;
        }
        final String imageurl = CommonUtils.getImageUrl(imgList.get(position));
        LogUtil.e("imageurl------" + imageurl);
        Glide.with(mContext).load(imageurl).placeholder(R.drawable.ic_split_graph).error(R.drawable.ic_split_graph).bitmapTransform(new CropSquareTransformation(mContext)).into(holder.iv);
        holder.iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (commentClickListener == null) {
                    return;
                }
                ArrayList<String> lists = new ArrayList<>();
                for (int i = 0; i < imgList.size(); i++) {
                    lists.add(CommonUtils.getImageUrl(imgList.get(i)));
                }
                commentClickListener.goToEnlarge(lists, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imgList == null ? 0 : imgList.size();
    }

    class CommentImageHolder extends RecyclerView.ViewHolder {
        ImageView iv;

        public CommentImageHolder(View itemView) {
            super(itemView);
            iv = (ImageView) itemView;
        }
    }

//    public void updateUrls(List<String> urls) {
//
//        if (imgList == null) {
//            imgList = new ArrayList<>();
//        } else {
//            imgList.clear();
//        }
//        if (urls != null) {
//            imgList.addAll(urls);
//        }
//
//        notifyDataSetChanged();
//    }
}