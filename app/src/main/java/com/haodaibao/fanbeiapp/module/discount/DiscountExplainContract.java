package com.haodaibao.fanbeiapp.module.discount;

import com.baseandroid.base.IView;
import com.haodaibao.fanbeiapp.repository.json.ActivityDetailBean.ActivityBean;
import com.haodaibao.fanbeiapp.repository.json.ActivityDetailBean.ActivityAuthBean;

;

/**
 * 开发者：LuoYi
 * Time: 2017 16:23 2017/12/14 12
 */

public interface DiscountExplainContract {

    interface Presenter {

        void getActivityDetail(String merchantno, String activityno);

    }

    interface DiscountView extends IView {

        void onDiscountData(ActivityBean activityBean, ActivityAuthBean authBean);

        void onDiscoutError();

    }

}
