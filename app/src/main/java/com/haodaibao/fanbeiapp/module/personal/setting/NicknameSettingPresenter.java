package com.haodaibao.fanbeiapp.module.personal.setting;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.RxUtils;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.UserInfoRepository;
import com.haodaibao.fanbeiapp.repository.json.Data;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Routee on 2017/9/20.
 * description: ${cusor}
 */

public class NicknameSettingPresenter implements NicknameSettingContract.Presenter {
    private final NicknameSettingContract.View mView;

    public NicknameSettingPresenter(NicknameSettingContract.View view) {
        mView = view;
    }

    @Override
    public void updateNickname(String nickname) {
        Map<String, String> params = new HashMap<>();
        params.put("alias", nickname);
        UserInfoRepository.getInstance().updateNickname(params).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data>applySchedulersLifeCycle((BaseActivity) mView))
                .subscribe(new RxObserver<Data>() {
                    @Override
                    public void onNext(@NonNull Data data) {
                        if (checkJsonCode(data, false)) {
                            mView.updateNicknameSuccess();
                        }
                    }
                });
    }
}
