package com.haodaibao.fanbeiapp.module.dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.baseandroid.base.BaseDialog;
import com.haodaibao.fanbeiapp.R;

public class LoadingDialog extends BaseDialog {

    @Override
    protected int getLayoutId() {
        return R.layout.waitdialoglayout;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.LoadingDialog);
    }

    @Override
    protected void setupView() {
        getDialog().setCanceledOnTouchOutside(false);
        setCancelable(false);
    }
}
