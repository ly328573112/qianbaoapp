package com.haodaibao.fanbeiapp.module.invoice.updaterise;

import android.text.TextUtils;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.RxUtils;
import com.haodaibao.fanbeiapp.repository.LifeRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.InvoiceRiseDetailBean;

import java.util.HashMap;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

/**
 * date: 2018/2/24.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class UpdateInvoiceRisePresenter implements UpdateInvoiceRiseContract.Presenter {
    private UpdateInvoiceRiseContract.View mView;

    public UpdateInvoiceRisePresenter(UpdateInvoiceRiseContract.View mView) {
        this.mView = mView;
    }

    @Override
    public void addOrModify(final String id, String riseTypeCode, String riseName, String taxNo, String mobile, String email, String companyAddress, String companyMobile, String companyBankName, String companyBankNo) {
        HashMap<String, String> map = new HashMap<>();
        map.put("id", id);
        map.put("riseTypeCode", riseTypeCode);
        map.put("riseName", riseName);
        map.put("taxNo", taxNo);
        map.put("mobile", mobile);
        map.put("email", email);
        map.put("companyAddress", companyAddress);
        map.put("companyMobile", companyMobile);
        map.put("companyBankName", companyBankName);
        map.put("companyBankNo", companyBankNo);
        LifeRepository.getInstance().addOrModify(map)
                .flatMap(new Function<Data, ObservableSource<Data<InvoiceRiseDetailBean>>>() {
                    @Override
                    public ObservableSource<Data<InvoiceRiseDetailBean>> apply(Data data) throws Exception {
                        if (TextUtils.isEmpty(id)) {
                            Observable<Data<InvoiceRiseDetailBean>> observable = Observable.create(new ObservableOnSubscribe<Data<InvoiceRiseDetailBean>>() {

                                @Override
                                public void subscribe(ObservableEmitter<Data<InvoiceRiseDetailBean>> e) throws Exception {
                                    Data<InvoiceRiseDetailBean> d = new Data<InvoiceRiseDetailBean>();
                                    d.setStatus("20000151");
                                    e.onNext(d);
                                }
                            });
                            return observable;
                        } else {
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("id", id);
                            return LifeRepository.getInstance().getInvoiceRiseDetail(hashMap);
                        }

                    }
                })
                .compose(RxUtils.<Data<InvoiceRiseDetailBean>>applySchedulersLifeCycle((BaseActivity) mView))
                .subscribe(new RxObserver<Data<InvoiceRiseDetailBean>>() {
                    @Override
                    public void onNext(Data<InvoiceRiseDetailBean> data) {
                        if (checkJsonCode(data, true)) {
                            mView.addSucc(data.getResult());
                        }

                    }
                });
    }
}
