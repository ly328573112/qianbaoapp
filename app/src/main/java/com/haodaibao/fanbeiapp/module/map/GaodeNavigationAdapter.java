package com.haodaibao.fanbeiapp.module.map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.amap.api.services.route.DriveStep;
import com.amap.api.services.route.WalkStep;
import com.baseandroid.utils.CommonUtils;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.search.adpter.ViewHolder;

import java.util.List;

/**
 * 开发者：LuoYi
 * Time: 2017 19:26 2017/9/27 09
 */

public class GaodeNavigationAdapter extends BaseExpandableListAdapter {

    private Context context;
    private String flag;
    private List step;
    private String startTitle;
    private float routeDuration;
    private float routeDistance;

    GaodeNavigationAdapter(Context context) {
        this.context = context;
    }

    void setNavigationData(String flag, List step, String startTitle, float routeDuration, float routeDistance) {
        this.flag = flag;
        this.step = step;
        this.startTitle = startTitle;
        this.routeDuration = routeDuration;
        this.routeDistance = routeDistance;
    }


    @Override
    public int getGroupCount() {
        return 1;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return step.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View groupView, ViewGroup parent) {
        groupView = LayoutInflater.from(context).inflate(R.layout.adapter_routeplan_group, null);
            /* 路线详情名称 */
        TextView tvName = ViewHolder.get(groupView, R.id.tv_name);
            /* 路线详情时长 */
        TextView tvDur = ViewHolder.get(groupView, R.id.tv_dur);
            /* 路线详情距离 */
        TextView tvDis = ViewHolder.get(groupView, R.id.tv_dis);
        tvName.setText(startTitle);
        tvDur.setText(CommonUtils.formatTime("" + routeDuration));
        tvDis.setText(CommonUtils.formatDistance("" + routeDistance));
        return groupView;
    }

    @Override
    public View getChildView(int groupPosition, int childId, boolean isLastChild, View childView, ViewGroup parent) {
        childView = LayoutInflater.from(context).inflate(R.layout.adapter_routeplan_child, null);
        TextView tvRoutestep = ViewHolder.get(childView, R.id.tv_routestep);
        // step = step.get(pos);
        if (flag.equals("drive")) {
            if (step != null) {
                DriveStep driveStep = (DriveStep) step.get(childId);
                tvRoutestep.setText(driveStep.getInstruction());
            }
        } else if (flag.equals("bus")) {
            if (step != null) {
                // BusStep busStep = (BusStep) step.get(childId);
                // RouteBusLineItem busline = busStep.getBusLine();
                // RouteBusWalkItem walkline = busStep.getWalk();
                // busline.getArrivalBusStation();
                // walkline.get
                // tv_routestep.setText(busStep.getBusLine().getBusLineName());
            }
        } else if (flag.equals("walk")) {
            if (step != null) {
                WalkStep walkStep = (WalkStep) step.get(childId);
                tvRoutestep.setText(walkStep.getInstruction());
            }
        }
        return childView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
