package com.haodaibao.fanbeiapp.module.search.adpter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Global;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.database.GreenDaoDbHelp;
import com.haodaibao.fanbeiapp.event.CitySelectChangEvent;
import com.haodaibao.fanbeiapp.repository.json.OpenCityBean;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * @author LuoYi
 */
public class SearchCityListAdapter extends BaseRecycleViewAdapter<OpenCityBean> {

    public SearchCityListAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, final OpenCityBean item) {
        ((CityHolder) holder).cityName.setText(item.getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*-----城市切换选择-----*/
                Global.setLastSelectCity(Global.getSelectCity());
                Global.setSelectCity(item.getName());
                Global.setSelectCityCode(item.getCode());
                Global.setSelectCitySiteType(item.getType());
                ((BaseActivity) mContext).finish();
                CitySelectChangEvent changEvent = new CitySelectChangEvent();
                List<OpenCityBean> openCityBeans = GreenDaoDbHelp.queryCityByGaodeNameAdCode().list();
                if (openCityBeans.size() > 0) {
                    if (openCityBeans.get(0).getCode().equals(item.getCode())) {
                        changEvent.currentLocation = true;
                    }
                } else {
                    changEvent.currentLocation = false;
                }
                EventBus.getDefault().post(changEvent);
            }
        });
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new CityHolder(LayoutInflater.from(mContext).inflate(R.layout.item_searchcity, parent, false));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class CityHolder extends RecyclerView.ViewHolder {
        TextView cityName;

        public CityHolder(View itemView) {
            super(itemView);
            cityName = (TextView) itemView.findViewById(R.id.search_city_name);
        }
    }

}
