package com.haodaibao.fanbeiapp.module.personal;

import com.baseandroid.config.Global;
import com.baseandroid.utils.RxUtils;
import com.haodaibao.fanbeiapp.repository.LifeRepository;
import com.haodaibao.fanbeiapp.repository.MerchantRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.UserInfoRepository;
import com.haodaibao.fanbeiapp.repository.json.BalanceBean;
import com.haodaibao.fanbeiapp.repository.json.BhhInfoBean;
import com.haodaibao.fanbeiapp.repository.json.BhhStatueBean;
import com.haodaibao.fanbeiapp.repository.json.CouponBean;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.UserDate;
import com.trello.rxlifecycle2.LifecycleProvider;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Routee on 2017/9/19.
 * description: ${cusor}
 */

public class PersonalFragmentPresenter implements PersonalFragmentContract.Presenter {

    PersonalFragmentContract.View mView;
    LifecycleProvider lifecycleProvider;

    PersonalFragmentPresenter(PersonalFragmentContract.View view, LifecycleProvider lifecycleProvider) {
        this.mView = view;
        this.lifecycleProvider = lifecycleProvider;
    }

    @Override
    public void getBhhStatue() {
        Map<String, String> params = new HashMap<>(16);
        params.put("classcode", "BHH_CONTROL");
        LifeRepository.getInstance().getBhhStatue(params).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<BhhStatueBean>>applySchedulersLifeCycle(lifecycleProvider))
                .subscribe(new RxObserver<Data<BhhStatueBean>>() {
                    @Override
                    public void onNext(@NonNull Data<BhhStatueBean> data) {
                        if (checkJsonCode(data, false)) {
                            mView.setupBhhStatueData(data.getResult());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                        mView.getBhhStatueFailed();
                    }
                });
    }

    @Override
    public void getBalance() {
        LifeRepository.getInstance().getBalance().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<BalanceBean>>applySchedulersLifeCycle(lifecycleProvider))
                .subscribe(new RxObserver<Data<BalanceBean>>() {
                    @Override
                    public void onNext(@NonNull Data<BalanceBean> data) {
                        if (checkJsonCode(data, false)) {
                            mView.setupBalanceData(data.getResult());
                        }
                    }
                });
    }

    @Override
    public void getUserInfo() {
        UserInfoRepository.getInstance().getUserinfo().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<UserDate>>bindToLifecycle(lifecycleProvider))
                .subscribe(new RxObserver<Data<UserDate>>() {
                    @Override
                    public void onNext(@NonNull Data<UserDate> data) {
                        if (checkJsonCode(data, false)) {
                            mView.setupUserInfoData(data.getResult());
                        }
                    }
                });
    }

    @Override
    public void getCouponsCount() {
        Map<String, String> params = new HashMap<>(16);
        params.put("status", "0");
        params.put("merchantno", "");
        params.put("orderamt ", "");
        params.put("page", "" + "1");
        params.put("limit", "1000");
        params.put("isPay", "");
        LifeRepository.getInstance().getCouponsList(params).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<CouponBean>>applySchedulersLifeCycle(lifecycleProvider))
                .subscribe(new RxObserver<Data<CouponBean>>() {
                    @Override
                    public void onNext(@NonNull Data<CouponBean> data) {
                        if (checkJsonCode(data, false)) {
                            mView.setupCouponCountData(data.getResult());
                        }
                    }
                });
    }

    @Override
    public void getUnReadMsgsNum() {
        Map<String, String> params = new HashMap<>(16);
        params.put("userNo", Global.getUserInfo().getUserno());// 用户号
        params.put("userChannel", "11");// 接受者渠道 11钱包生活
        params.put("type", "ALL");// 消息类型
        // ALL：所有类型，01:提醒消息，02:优惠消息，03:系统消息
        MerchantRepository.getInstance()
                .unReadMsgsNum(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<Object>>applySchedulersLifeCycle(lifecycleProvider))
                .subscribe(new RxObserver<Data<Object>>() {
                    @Override
                    public void onNext(Data data) {
                        super.onNext(data);
                        if (data.getResult() != null) {
                            String result = data.getResult().toString();
                            result = result.replace("{result=", "");//{result=1}
                            result = result.replace("}", "");
                            mView.setUnReadMsgView(result);
                        } else {
                            mView.setUnReadMsgView(null);
                        }
                    }
                });
    }

    @Override
    public void getMyBhhInfo(String token) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("token", token == null ? "" : token);
        UserInfoRepository.getInstance().getBhhInfo(hashMap)
                .compose(RxUtils.<Data<BhhInfoBean>>applySchedulersLifeCycle(lifecycleProvider))
                .subscribe(new RxObserver<Data<BhhInfoBean>>() {
                    @Override
                    public void onNext(Data<BhhInfoBean> bhhInfoBeanData) {
                        if (checkJsonCode(bhhInfoBeanData, true)) {
                            mView.updateBhhInfo(bhhInfoBeanData.getResult());
                        }
//                        else {
//                            mView.getBhhInfoFailed();
//                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        mView.getBhhInfoFailed();
                    }
                });
    }


}
