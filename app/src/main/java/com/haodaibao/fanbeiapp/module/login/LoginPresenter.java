package com.haodaibao.fanbeiapp.module.login;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Constant;
import com.baseandroid.config.Global;
import com.baseandroid.utils.DeviceUuid;
import com.baseandroid.utils.RxUtils;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.UserInfoRepository;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.LoginInfo;
import com.haodaibao.fanbeiapp.repository.json.UserDate;
import com.jayfeng.lesscode.core.EncodeLess;
import com.trello.rxlifecycle2.android.ActivityEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

import static com.haodaibao.fanbeiapp.repository.RxObserver.checkJsonCode;

public class LoginPresenter implements LoginContract.Presenter {

    private LoginContract.View mView;

    LoginPresenter(LoginContract.View view) {
        this.mView = view;
    }

    @Override
    public void requestSmsCode(String phonenum) {
        mView.disableValidateCodeButton();
        Map<String, String> params = new HashMap<>();
        params.put("username", Constant.ORGNO + "#" + phonenum);
        UserInfoRepository.getInstance()
                .getSmsCode(params)
                .compose(RxUtils.<Data>applySchedulersLifeCycle((BaseActivity) mView))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        if (checkJsonCode(data, true)) {
                            countValidateCode();
                        } else {
                            mView.enableValidateCodeButton();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                        mView.enableValidateCodeButton();
                    }

                });
    }

    /**
     * 发送语音验证码请求
     *
     * @param phoneNum 手机号
     */
    @Override
    public void requestVmsCode(String phoneNum) {
        Map<String, String> params = new HashMap<>();
        params.put("username", Constant.ORGNO + "#" + phoneNum);
        UserInfoRepository.getInstance()
                .getVmsCode(params)
                .compose(RxUtils.<Data>applySchedulersLifeCycle((BaseActivity) mView))
                .subscribe(new RxObserver<Data>());
    }

    private void countValidateCode() {
        Observable.interval(0, 1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(((BaseActivity) mView).<Long>bindUntilEvent(ActivityEvent.DESTROY))
                .subscribe(new RxObserver<Long>() {

                    Disposable mDisposable;

                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        mDisposable = d;
                    }

                    @Override
                    public void onNext(Long count) {
                        if (count < Constant.VALIDETE_CODE_WATING_TIME_MAX) {
                            mView.countValidateCodeButton(Constant.VALIDETE_CODE_WATING_TIME_MAX - count);
                            if (count == Constant.VALIDETE_CODE_WATING_TIME_MAX / 2) {
                                mView.showVoiceAuthTips();
                            }
                        } else {
                            if (mDisposable != null) {
                                mDisposable.dispose();
                            }
                            mView.resendValidateCodeButton();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.resendValidateCodeButton();
                    }

                });
    }

    @Override
    public void requestLogin(String siteType, String siteCode, final String phoneNum, String verifiCode) {
        Map<String, String> params = new HashMap<>();
        params.put("siteType", siteType);
        params.put("siteCode", siteCode);
        params.put("username", Constant.ORGNO + "#" + phoneNum);
        params.put("password", verifiCode);
        params.put("normal", "no");
        params.put("audience", EncodeLess.$md5(DeviceUuid.getDeviceId(Global.getContext())));
        params.put("cityCode", Global.getSelectCityCode());
        params.put("lng", "" + Global.getMyLocation().getLongitude());
        params.put("lat", "" + Global.getMyLocation().getLatitude());

        UserInfoRepository.getInstance()
                .requestLogin(params)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        mView.showLoadingDialog();
                    }
                })
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Consumer<Data<LoginInfo>>() {
                    @Override
                    public void accept(@NonNull Data<LoginInfo> loginInfoData) throws
                            Exception {
                        if (checkJsonCode(loginInfoData, true)) {
                            Global.setLoginInfo(loginInfoData.getResult());
                        }
                    }
                })
                .observeOn(Schedulers.newThread())
                .flatMap(new Function<Data<LoginInfo>, Observable<Data<UserDate>>>() {
                    @Override
                    public Observable<Data<UserDate>> apply(@NonNull Data<LoginInfo> loginInfoBeanData) throws
                            Exception {
                        if (checkJsonCode(loginInfoBeanData, false)) {
                            return UserInfoRepository.getInstance().getUserinfo();
                        }
                        return null;
                    }
                })
                .compose(RxUtils.<Data<UserDate>>applySchedulersLifeCycle(((BaseActivity) mView)))
                .subscribe(new RxObserver<Data<UserDate>>() {

                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        super.onSubscribe(d);
                    }

                    @Override
                    public void onNext(@NonNull Data<UserDate> userDateData) {
                        if (checkJsonCode(userDateData, true)) {
                            if (userDateData.getResult().getUser() != null) {
                                Global.setUserInfo(userDateData.getResult().getUser());
                                Global.setUsrePhotoNumber(phoneNum);
                                mView.OnLoginInfo();
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                        mView.closeLoadingDialog();
                    }

                    @Override
                    public void onComplete() {
                        mView.closeLoadingDialog();
                    }
                });
    }

}