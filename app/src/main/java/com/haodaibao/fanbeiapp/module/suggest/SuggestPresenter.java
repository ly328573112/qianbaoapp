package com.haodaibao.fanbeiapp.module.suggest;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.RxUtils;
import com.haodaibao.fanbeiapp.repository.LifeRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.SuggestBean;
import com.haodaibao.fanbeiapp.repository.json.SuggestResp;

import java.util.HashMap;

import io.reactivex.annotations.NonNull;

/**
 * date: 2017/9/26.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class SuggestPresenter implements SuggestContract.Presenter {
    private SuggestContract.View mView;

    public SuggestPresenter(SuggestContract.View mView) {
        this.mView = mView;
    }

    @Override
    public void submitSuggest(String _userNo, String content, String attFlList) {
        HashMap<String, String> map = new HashMap<>();
        map.put("_userNo", _userNo == null ? "" : _userNo);
        map.put("content", content);
        map.put("attFlList", attFlList);
        LifeRepository.getInstance().submitSuggest(map)
                .compose(RxUtils.<Data<SuggestBean>>applySchedulersLifeCycle((BaseActivity) mView))
                .subscribe(new RxObserver<Data<SuggestBean>>() {
                    @Override
                    public void onNext(@NonNull Data<SuggestBean> suggestBeanData) {
                        if (checkJsonCode(suggestBeanData, true)) {
                            mView.subSucc(suggestBeanData.getResult());
                        }

                    }
                });
    }

    @Override
    public void getSuggestList(String _userNo) {
        HashMap<String, String> map = new HashMap<>();
        map.put("_userNo", _userNo == null ? "" : _userNo);
        LifeRepository.getInstance().getSubmitList(map)
                .compose(RxUtils.<Data<SuggestResp>>applySchedulersLifeCycle((BaseActivity) mView))
                .subscribe(new RxObserver<Data<SuggestResp>>() {
                    @Override
                    public void onNext(@NonNull Data<SuggestResp> suggestRespData) {
                        if (checkJsonCode(suggestRespData, true)) {
                            mView.updateList(suggestRespData.getResult().getList());
                        }
                    }
                });
    }
}
