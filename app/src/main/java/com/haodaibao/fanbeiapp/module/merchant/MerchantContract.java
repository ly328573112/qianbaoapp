package com.haodaibao.fanbeiapp.module.merchant;

import android.os.Bundle;

import com.amap.api.maps.TextureMapView;
import com.baseandroid.base.IView;
import com.haodaibao.fanbeiapp.repository.json.CommentListBean;
import com.haodaibao.fanbeiapp.repository.json.ErrorRecoveryList;
import com.haodaibao.fanbeiapp.repository.json.LableInfo;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoResp;
import com.haodaibao.fanbeiapp.repository.json.PackageListBean;
import com.haodaibao.fanbeiapp.repository.json.PeriodsInfo;
import com.haodaibao.fanbeiapp.repository.json.PriedListBean;

import java.util.List;
import java.util.Map;

/**
 * 开发者：LuoYi
 * Time: 2017 15:52 2017/9/4 09
 *
 * @author
 */
public interface MerchantContract {

    interface Presenter {

        Map<String, String> getRNamount(MerchantInfoEntiy merchantInfo);

        void getMerchantDetail(String merchantno);

        void followMerchant(String merchantno, boolean isChecked);

        void getCommentNearbyList(String merchantno, String pageNumber, String longitude, String latitude, String cityCode);

        void getCommentL(String merchantno, boolean isComment);

        void getErrorRecoveryList();

        void merchantErrorRecovery(String merchantno, String errorCode);
    }

    interface MerchantView extends IView {

        void onMerchantDetail(String attention, MerchantInfoEntiy merchantInfo, List<PeriodsInfo> perList, List<PriedListBean> priedList, List<PackageListBean> packageLists);

        void onFollow(String status);

        void onFail();

        void onCommentList(CommentListBean commentListBean, boolean isComment);

        void onNearby(MerchantInfoResp merchantInfoResp);

        void onRefreshList();

        void onDismiss();

        void closeActivity();

        void onRetry();

        void onContent();

        void onErrorRecovery(ErrorRecoveryList errorRecoveryList);

        void onErrorCommint(String status);
    }

    interface ShareDialog {
        void showShareDialog(Bundle bundle);

        void callDialog(String phone);

        void discountDialog(List<LableInfo> lableInfos);
    }

    interface MapLocationView {

        void initMapView(TextureMapView bmapView, String latitude, String longitude, String title);

    }

    interface MerchantError {

        void initErrorDialog(List<ErrorRecoveryList.ErrorResult> errorRecoveryList, String merchantno);

    }


}
