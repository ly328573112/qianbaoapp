package com.haodaibao.fanbeiapp.module.login;

import com.baseandroid.base.IView;

public interface LoginContract {

    interface Presenter {
        void requestSmsCode(String phonenum);

        /**
         * 获取语音验证码
         *
         * @param phoneNum 手机号
         */
        void requestVmsCode(String phoneNum);

        void requestLogin(String siteType, String siteCode, String phoneNum, String verifiCode);
    }

    interface View extends IView {

        void enableValidateCodeButton();

        void disableValidateCodeButton();

        void showVoiceAuthTips();

        void hiddenVoiceAuthTips();

        void countValidateCodeButton(long number);

        void resendValidateCodeButton();

        void OnLoginInfo();


    }
}
