package com.haodaibao.fanbeiapp.module.personal.coupon;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.StatusBarHelper;
import com.haodaibao.fanbeiapp.R;

import butterknife.BindView;

/**
 * create by 曹伟 on 2018/05/24
 * pro 查看优惠券列表
 * e-mail : caowei@qianbaocard.com
 */

public class CouponListActivity extends BaseActivity {

    @BindView(R.id.toolbar_title)
    TextView    mToolbarTitle;  //标题栏

//    @BindView(R.id.use_tv)
//    TextView use_tv ;   //可用优惠券数量可用优惠券列表
//
//    @BindView(R.id.use_coupon_list_rcy)
//    RecyclerView use_coupon_list_rcy ;  //可用优惠券数量列表
//
//    @BindView(R.id.unuser_tv)
//    TextView unuser_tv ;    //不可用优惠券数量

    @BindView(R.id.unuse_coupon_list_rcy)
    RecyclerView unuse_coupon_list_rcy ;    //不可用优惠券列表

    private LinearLayoutManager mLinearLayoutManager ;

    @Override
    protected int getLayoutId(){
        return R.layout.activity_coupon_list;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(CouponListActivity.this);
        mToolbarTitle.setText("生活优惠券");

        mLinearLayoutManager = new LinearLayoutManager(mContext);
    }


    @Override
    protected void setupData(Bundle savedInstanceState) {

    }
}
