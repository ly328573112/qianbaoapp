package com.haodaibao.fanbeiapp.module.map;

import android.view.View;

import com.amap.api.maps.AMap;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;


/**
 * 开发者：LuoYi
 * Time: 2017 17:06 2017/9/26 09
 */

public class GaodeNavigationPresenter implements GaodeMapContract.NavigationPresenter, AMap.OnMarkerClickListener, AMap.OnMapClickListener, AMap.OnInfoWindowClickListener, AMap.InfoWindowAdapter {

    private final GaodeNavigationActivity mapActivity;
    private AMap aMap;

    public GaodeNavigationPresenter(GaodeMapContract.GaodeMapView gaodeMapView) {
        mapActivity = (GaodeNavigationActivity) gaodeMapView;
    }

    @Override
    public AMap setMap(AMap aMap) {
        this.aMap = aMap;
        /**
         * 注册监听
         */
        aMap.setOnMapClickListener(this);
        aMap.setOnMarkerClickListener(this);
        aMap.setOnInfoWindowClickListener(this);
        aMap.setInfoWindowAdapter(this);
        return aMap;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker.isInfoWindowShown()) {
            marker.hideInfoWindow();
        } else {
            marker.showInfoWindow();
        }
        return false;
    }


}
