package com.haodaibao.fanbeiapp.module.start;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseDialog;
import com.baseandroid.utils.Util;
import com.baseandroid.utils.VersionUtil;
import com.haodaibao.fanbeiapp.BuildConfig;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.CheckUpdate;
import com.jayfeng.lesscode.core.DisplayLess;

import butterknife.BindView;

public class UpdateDialog extends BaseDialog {

    @BindView(R.id.bt_ok)
    protected Button confirmButton;
    @BindView(R.id.bt_cancel)
    protected Button cancelButton;
    @BindView(R.id.tv_content)
    protected TextView contentView;
    @BindView(R.id.ll_title)
    LinearLayout llTitle;

    private String curVersion = BuildConfig.VERSION_NAME;

    protected OnSelectClickListener mOnSelectClickListener;
    private String latestVersion;
    private String forceVersion;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Full Screen Area
        getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        getDialog().getWindow().getDecorView().setBackgroundColor(Color.TRANSPARENT);
        WindowManager.LayoutParams lp = getDialog().getWindow().getAttributes();
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes(lp);
        if (mOnShowListener != null) {
            getDialog().setOnShowListener(mOnShowListener);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_update;
    }

    @Override
    protected void setupView() {
        Util.setMargins(llTitle, DisplayLess.$dp2px(25), 0, DisplayLess.$dp2px(25), 0);

        Bundle args = getArguments();
        if (args != null) {
            String versionDesc = args.getString("versionDesc");
            latestVersion = args.getString("latestVersion");
            forceVersion = args.getString("forceVersion");
            contentView.setText(versionDesc.replace("#", "\n"));
        } else {
            contentView.setText("");
        }
        confirmButton.setText("立即更新");

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnSelectClickListener != null) {
                    mOnSelectClickListener.confirmOnClickListener(getVersionUpdateStatus());
                }
                dismiss();
            }
        });

        if (getVersionUpdateStatus()) {
            cancelButton.setVisibility(View.GONE);
            setButtonWidth(confirmButton);
        } else {
            cancelButton.setVisibility(View.VISIBLE);
            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnSelectClickListener != null) {
                        mOnSelectClickListener.cancelOnClickListener(getVersionUpdateStatus());
                    }
                    dismiss();
                }
            });
        }

        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN && event.getRepeatCount() == 0) {
                    mOnSelectClickListener.onKeyListener(keyCode, event, getVersionUpdateStatus());
                    dismiss();
                    return true;
                } else {
                    return false;
                }
            }
        });

        getDialog().setCancelable(false);
        getDialog().setCanceledOnTouchOutside(false);

    }

    private void setButtonWidth(Button buttonWidth) {
        RelativeLayout.LayoutParams linearParams = (RelativeLayout.LayoutParams) buttonWidth.getLayoutParams(); //取控件textView当前的布局参数
        linearParams.height = DisplayLess.$dp2px(40);// 控件的高强制设成20
        linearParams.width = DisplayLess.$dp2px(120);// 控件的宽强制设成30
        buttonWidth.setLayoutParams(linearParams); //使设置好的布局参数应用到控件
    }

    private boolean getVersionUpdateStatus() {
        //是否有更新
        boolean update = VersionUtil.needUpdate(curVersion, latestVersion, false);
        boolean force = VersionUtil.needUpdate(curVersion, forceVersion, true);
        if (update && !force) {
            // APK可选更新
            return false;
        } else if (update && force) {
            // APK强制更新
            return true;
        } else {
            // 不需要更新
            return false;
        }
    }

    public interface OnSelectClickListener {
        void confirmOnClickListener(boolean updateStatus);

        void cancelOnClickListener(boolean updateStatus);

        void onKeyListener(int keyCode, KeyEvent event, boolean updateStatus);
    }

    public OnSelectClickListener getOnSelectClickListener() {
        return mOnSelectClickListener;
    }

    public void setOnSelectClickListener(OnSelectClickListener onSelectClickListener) {
        mOnSelectClickListener = onSelectClickListener;
    }

    public static UpdateDialog newInstance(CheckUpdate checkUpdate, OnSelectClickListener onSelectClickListener) {
        UpdateDialog updateDialog = new UpdateDialog();
        Bundle args = new Bundle();
        args.putString("param", "param");
        args.putString("versionDesc", checkUpdate.getVersion_desc());
        args.putString("latestVersion", checkUpdate.getLatest_version());
        args.putString("forceVersion", checkUpdate.getForce_update_version());
        updateDialog.setArguments(args);
        updateDialog.setOnSelectClickListener(onSelectClickListener);
        return updateDialog;
    }

}
