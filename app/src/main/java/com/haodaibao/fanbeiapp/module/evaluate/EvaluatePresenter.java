package com.haodaibao.fanbeiapp.module.evaluate;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.LogUtil;
import com.baseandroid.utils.RxUtils;
import com.haodaibao.fanbeiapp.repository.MerchantRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.CommentListBean;
import com.haodaibao.fanbeiapp.repository.json.Data;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;

/**
 * 开发者：LuoYi
 * Time: 2017 16:31 2017/9/12 09
 */

public class EvaluatePresenter implements EvaluateContract.Presenter {

    private EvaluateContract.EvaluateView evaluateView;

    public EvaluatePresenter(EvaluateContract.EvaluateView evaluateView) {
        this.evaluateView = evaluateView;
    }

    @Override
    public void getEvaluateData(String merchantno, String queryType, String ifcount, String page, String limit) {
        HashMap<String, String> merchantMap = new HashMap<>();
        merchantMap.put("merchantno", merchantno);
        merchantMap.put("queryType", queryType);
        merchantMap.put("ifcount", ifcount);
        merchantMap.put("page", page);
        merchantMap.put("limit", limit);
        MerchantRepository.getInstance().getCommentListMoreServer(merchantMap, true)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<CommentListBean>>applySchedulersLifeCycle((BaseActivity) evaluateView))
                .subscribe(new RxObserver<Data<CommentListBean>>() {
                    @Override
                    public void onNext(@NonNull Data<CommentListBean> commentListBeanData) {
                        super.onNext(commentListBeanData);
                        if (RxObserver.checkJsonCode(commentListBeanData, true)) {
                            evaluateView.onEvaluateList(commentListBeanData.getResult());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                        evaluateView.onEvaluateListError();
                    }
                });
    }

    @Override
    public void submintComment(String orderno, String merchantno, String star, String comment, String imgurls) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("orderno", orderno);
        hashMap.put("merchantno", merchantno);
        hashMap.put("star", star);
        hashMap.put("comment", comment);
        hashMap.put("imgurls", imgurls);
        MerchantRepository.getInstance().submitComment(hashMap, false)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<Object>>applySchedulersLifeCycle((BaseActivity) evaluateView))
                .subscribe(new RxObserver<Data<Object>>() {
                    @Override
                    public void onNext(@NonNull Data<Object> objectData) {
                        super.onNext(objectData);
                        if (RxObserver.checkJsonCode(objectData, true)) {
                            evaluateView.onSubmint();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                        evaluateView.onSubmintError();
                        LogUtil.showNDebug((BaseActivity) evaluateView, "提交失败,请检查网络是否连接!");
                    }
                });
    }
}
