package com.haodaibao.fanbeiapp.module.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.FormatUtil;
import com.baseandroid.widget.RouteeFlowLayout;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.bumptech.glide.Glide;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.merchant.temporarydata.MerchantTemporary;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;
import com.haodaibao.fanbeiapp.repository.json.PackageListBean;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 开发者：LuoYi
 * Time: 2018 11:27 2018/3/14 03
 */

public class HomeNewAdapter extends BaseRecycleViewAdapter<MerchantTemporary> {

    private OnItemClickListener itemClickListener;
    private HomeContract.OnBaseClickListener clickListener;

    public HomeNewAdapter(Context context, OnItemClickListener itemClickListener, HomeContract.OnBaseClickListener clickListener) {
        super(context);
        this.itemClickListener = itemClickListener;
        this.clickListener = clickListener;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, MerchantTemporary temporary) {
        if (holder instanceof HomeNewHolder) {
            HomeNewHolder newHolder = (HomeNewHolder) holder;
            final MerchantInfoEntiy item = (MerchantInfoEntiy) temporary;
            /* 商户图片 */
            newHolder.itemMerchantImageIv.setScaleType(ImageView.ScaleType.FIT_XY);
            String imageurl = CommonUtils.getImageUrl(item.getImageurl());
            Glide.with(mContext)
                    .load(imageurl)
                    .asBitmap()
                    .placeholder(R.drawable.image_merchant_placehold)
                    .error(R.drawable.image_merchant_placehold)
                    .into(newHolder.itemMerchantImageIv);

            newHolder.itemMerchantNameTv.setText(item.getMerchantname());

            /* 返现规则 */
            // 返现模式0：非返现1：全额返2：满额返3：每满额返
            String returnBeginAmount = item.getReturnbeginamount();
            String returnRate = item.getReturnrate();
            String refundMode = item.getRefundmode();

            if (!TextUtils.isEmpty(refundMode)) {
                if (!TextUtils.isEmpty(returnBeginAmount) && !TextUtils.isEmpty(returnRate)) {
                    if (!refundMode.equals("0")) {
//                        newHolder.itemDisountRl.setBackgroundColor(mContext.getResources().getColor(R.color.c_FFF4F5));
                        Spanned money;
                        try {
                            money = FormatUtil.setFanxian1(refundMode, returnBeginAmount, returnRate);
                        } catch (Exception e) {
                            money = Html.fromHtml("");
                        }
                        newHolder.itemDiscountTv.setText(money);
                        newHolder.itemFenleiLl.setVisibility(View.INVISIBLE);
                        newHolder.itemDiscountTv.setVisibility(View.VISIBLE);
                    } else {
                        newHolder.itemDisountRl.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                        newHolder.itemFenleiLl.setVisibility(View.VISIBLE);
                        newHolder.itemDiscountTv.setVisibility(View.INVISIBLE);
                        newHolder.itemFenleiTv.setText(item.getCategoryName());
                        newHolder.itemShangquanTv.setText("人均 ¥" + FormatUtil.filterDecimalPoint(item.getPersonaverage()));
                    }
                } else {
                    newHolder.itemDisountRl.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                    newHolder.itemFenleiLl.setVisibility(View.VISIBLE);
                    newHolder.itemDiscountTv.setVisibility(View.INVISIBLE);
                    newHolder.itemFenleiTv.setText(item.getCategoryName());
                    newHolder.itemShangquanTv.setText("人均 ¥" + FormatUtil.filterDecimalPoint(item.getPersonaverage()));
                }
            } else {
                newHolder.itemFenleiLl.setVisibility(View.INVISIBLE);
                newHolder.itemDiscountTv.setVisibility(View.INVISIBLE);
            }

            setRouteeView(newHolder.itemPackageRfl, item, item.getPackageList());

            newHolder.itemGoPayTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (itemClickListener != null) {
                        itemClickListener.onItemClick(view, item);
                    }
                }
            });
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.onItemClick(v, item);
                    }
                }
            });
        } else if (holder instanceof HomeEmptyHodler) {

        }

    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1:
                return new HomeNewHolder(View.inflate(mContext, R.layout.item_new_merchant, null));
            case 4:
                return new HomeEmptyHodler(View.inflate(mContext, R.layout.item_empty_merchant, null));
            default:
                return new HomeNewHolder(View.inflate(mContext, R.layout.item_new_merchant, null));
        }
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return getData().get(position).getmType();
    }

    private void setRouteeView(RouteeFlowLayout itemPackageRfl, final MerchantInfoEntiy merchantInfoEntiy, List<PackageListBean> packageList) {
        itemPackageRfl.removeAllViews();
        itemPackageRfl.setVerticalSpacing(14);
        for (final PackageListBean packageBean : packageList) {
            TextView itemNewPackge = (TextView) LayoutInflater.from(mContext).inflate(R.layout.item_new_package, null);
            itemNewPackge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clickListener != null) {
                        clickListener.onBaseClick(merchantInfoEntiy, packageBean);
                    }
                }
            });
            TextView packageNameTv = itemNewPackge.findViewById(R.id.item_package_name_tv);
            packageNameTv.setText(packageBean.getPackageName());
            itemPackageRfl.addView(itemNewPackge);
        }
    }

    class HomeNewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_merchant_rl)
        RelativeLayout itemMerchantRl;
        @BindView(R.id.item_merchant_image_iv)
        RoundedImageView itemMerchantImageIv;
        @BindView(R.id.item_merchant_name_tv)
        TextView itemMerchantNameTv;
        @BindView(R.id.item_discount_tv)
        TextView itemDiscountTv;
        @BindView(R.id.item_package_rfl)
        RouteeFlowLayout itemPackageRfl;
        @BindView(R.id.item_go_pay_tv)
        TextView itemGoPayTv;
        @BindView(R.id.item_disount_rl)
        RelativeLayout itemDisountRl;
        @BindView(R.id.item_fenlei_ll)
        LinearLayout itemFenleiLl;
        @BindView(R.id.item_fenlei_tv)
        TextView itemFenleiTv;
        @BindView(R.id.item_shangquan_tv)
        TextView itemShangquanTv;
//        @BindView(R.id.item_empty_iv)
//        ImageView itemEmptyIv;
//        @BindView(R.id.item_package_merchant_rl)
//        RelativeLayout itemPackageMerchantRl;

        public HomeNewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class HomeEmptyHodler extends RecyclerView.ViewHolder {

        public HomeEmptyHodler(View itemView) {
            super(itemView);
        }
    }
}
