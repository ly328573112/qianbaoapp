package com.haodaibao.fanbeiapp.module.manage;

import android.os.Bundle;

import com.baseandroid.base.BaseFragment;
import com.haodaibao.fanbeiapp.R;

public class ManageFragment extends BaseFragment implements ManageContract.View {

    @Override
    protected int getLayoutId() {
        return R.layout.home_manage_layout;
    }

    @Override
    protected void setupView() {

    }

    @Override
    protected void setupData(Bundle savedInstanceState) {

    }

    @Override
    public void showLoadingDialog() {

    }

    @Override
    public void closeLoadingDialog() {

    }

    @Override
    public void showUploading() {

    }

    @Override
    public void processUploading(String processText) {

    }

    @Override
    public void closeAnimatorUploading() {

    }

    @Override
    public void closeUploading() {

    }


}
