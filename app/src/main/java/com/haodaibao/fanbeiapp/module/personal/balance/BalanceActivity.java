package com.haodaibao.fanbeiapp.module.personal.balance;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.RxUtils;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.LifeRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.BalanceBean;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import butterknife.BindView;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;

public class BalanceActivity extends BaseActivity implements BaseRecycleViewAdapter.OnLoadMoreListener, View.OnClickListener {

    @BindView(R.id.tv_balance_title)
    TextView mTvBalanceTitle;
    @BindView(R.id.rv)
    RecyclerView mRv;
    @BindView(R.id.ptr)
    PtrClassicFrameLayout mPtr;
    @BindView(R.id.iv_back)
    ImageView mIvBack;
    @BindView(R.id.appbar)
    AppBarLayout mAppbar;
    @BindView(R.id.fl_top)
    FrameLayout mFlTop;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;

    private int mVerticalOffset;
    private BalanceRvAdapter mRvAdapter;
    private AppBarLayout.OnOffsetChangedListener mOnOffsetChangedListener
            = new AppBarLayout.OnOffsetChangedListener() {
        @Override
        public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
            android.util.Log.e("xxxxxx", "verticalOffSet = " + verticalOffset);
            mVerticalOffset = verticalOffset;
            int topPercent = -mVerticalOffset * 100 / mAppbar.getHeight();
            float alpha = (topPercent - 50) / 10.f;
            mFlTop.setAlpha(alpha);
            if (topPercent > 50) {
                mIvBack.setImageResource(R.drawable.icon_back_black);
                mToolbarTitle.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
            } else {
                mIvBack.setImageResource(R.drawable.icon_back_white);
                mToolbarTitle.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
            }
        }
    };
    private String mBalance = "";
    private boolean isPersonal = false;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_balance;
    }

    @Override
    protected void setupView() {
        mToolbarTitle.setText("余额");
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        initBundle();
        if (isPersonal == false) {
            LifeRepository.getInstance().getBalance().subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .compose(RxUtils.<Data<BalanceBean>>applySchedulersLifeCycle(BalanceActivity.this))
                    .subscribe(new RxObserver<Data<BalanceBean>>() {
                        @Override
                        public void onNext(@NonNull Data<BalanceBean> data) {
                            if (checkJsonCode(data, false)) {
                                mTvBalanceTitle.setText(data.getResult().getBalanceAvailable());
                            }
                        }
                    });

        }

        mAppbar.addOnOffsetChangedListener(mOnOffsetChangedListener);
        mPtr.disableWhenHorizontalMove(true);
        mPtr.setPullToRefresh(false);
        mPtr.setPtrHandler(new PtrDefaultHandler() {
            @Override
            public void onRefreshBegin(final PtrFrameLayout frame) {

            }

            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return false;
            }
        });
        toolbarBack.setOnClickListener(this);
    }

    private void initBundle() {
        mBalance = getIntent().getStringExtra("balance");
        isPersonal = getIntent().getBooleanExtra("isPersonal", false);
        mTvBalanceTitle.setText(TextUtils.isEmpty(mBalance) ? "" : mBalance);
    }

//    @OnClick(R.id.toolbar_back)
//    public void onViewClicked() {
//
//    }

    @Override
    public void onLoadMore() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back:
                ActivityJumpUtil.goBack(BalanceActivity.this);
                break;
            default:
                break;
        }
    }
}
