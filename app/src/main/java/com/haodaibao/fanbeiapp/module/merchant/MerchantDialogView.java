package com.haodaibao.fanbeiapp.module.merchant;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.ShareUtil;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.merchant.adapter.DiscountDialogAdapter;
import com.haodaibao.fanbeiapp.repository.json.LableInfo;
import com.tencent.connect.share.QQShare;
import com.tencent.connect.share.QzoneShare;

import java.util.List;

import static com.haodaibao.fanbeiapp.R.id.btn_pick_photo;
import static com.haodaibao.fanbeiapp.R.id.btn_take_photo;
import static com.haodaibao.fanbeiapp.R.id.ib_weixin;
import static com.haodaibao.fanbeiapp.R.id.ib_weixin_pengyouquan;


/**
 * 开发者：LuoYi
 * Time: 2017 20:05 2017/9/5 09
 */

public class MerchantDialogView implements MerchantContract.ShareDialog {

    MerchantContract.MerchantView merchantView;
    private MerchantDetailActivity activity;
    private ShareUtil share_util;

    private int contentType;
    private String title;
    private String message;
    private String circle;
    private String[] content;

    MerchantDialogView(MerchantContract.MerchantView merchantView) {
        this.merchantView = merchantView;
        activity = (MerchantDetailActivity) this.merchantView;
    }

    @Override
    public void showShareDialog(Bundle bundle) {
        Dialog shareDialog = new Dialog(activity, R.style.dialog_fullscreen);
        View view = activity.getLayoutInflater().inflate(R.layout.dialog_merchant_share, null);
        initDialogView(view, bundle, shareDialog);
        shareDialog.setContentView(view);
        shareDialog.show();
    }

    private void initDialogView(View view, Bundle bundle, final Dialog shareDialog) {
        /* 初始化分享工具类 */
        share_util = ShareUtil.getInstance(activity);

		/* 取得分享内容 */
        if (bundle != null) {
            contentType = bundle.getInt("contentType");
            title = bundle.getString("title");
            message = bundle.getString("desc");
            circle = bundle.getString("circle");
            content = bundle.getStringArray("content");
        }

        TextView pengyouquanTv = view.findViewById(ib_weixin_pengyouquan);
        pengyouquanTv.setOnClickListener(new ShareDialogOnClick(1, shareDialog));
        TextView weixinTv = view.findViewById(ib_weixin);
        weixinTv.setOnClickListener(new ShareDialogOnClick(2, shareDialog));
        TextView cancelTv = view.findViewById(R.id.tv_cancel);
        cancelTv.setOnClickListener(new ShareDialogOnClick(7, shareDialog));
    }

    class ShareDialogOnClick implements View.OnClickListener {

        int number;
        Dialog shareDialog;

        public ShareDialogOnClick(int number, Dialog shareDialog) {
            this.number = number;
            this.shareDialog = shareDialog;
        }

        @Override
        public void onClick(View v) {
            switch (number) {
                case 1://朋友圈
                    if (CommonUtils.isFastDoubleClick()) {
                        return;
                    } else {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                contentType = ShareUtil.TYPE_WEBPAGE;
                                circle = circle.contains("优惠买单") ? "用【钱包生活】" + circle : "用【钱包生活】优惠买单" + circle;
                                share_util.sendToWeixin("wxfriends", contentType, title + "，" + circle, circle, content);
                            }
                        }).start();
                    }
                    break;
                case 2://微信好友
                    if (CommonUtils.isFastDoubleClick()) {
                        return;
                    } else {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                contentType = ShareUtil.TYPE_WEBPAGE;
                                circle = circle.contains("优惠买单") ? circle.substring(4, circle.length()) : circle;
                                share_util.sendToWeixin("wxchat", contentType, title + " " + circle, message, content);
                            }
                        }).start();
                    }
                    break;
                case 3://QQ空间
                    int contentType1 = QzoneShare.SHARE_TO_QZONE_TYPE_IMAGE_TEXT;
                    share_util.sendToQQZone(contentType1, title, message, content);
                    break;
                case 4://QQ好友
                    int contentType = QQShare.SHARE_TO_QQ_TYPE_DEFAULT;
                    share_util.sendToQQ(contentType, title, message, content);
                    break;
                case 6://短信
                    share_util.sendToSMS(title + "\n" + message + "\n" + content[1]);
                    break;

                default:
                    break;
            }
            shareDialog.dismiss();
        }
    }

    @Override
    public void callDialog(String phone) {
        View view = LayoutInflater.from(activity).inflate(R.layout.dialog_select, null);
        Dialog popupDialog1 = new Dialog(activity, R.style.dialog_transparentscreen);
        popupDialog1.setContentView(view);
        Window dialogWindow = popupDialog1.getWindow();
        WindowManager.LayoutParams attributes = dialogWindow.getAttributes();
        dialogWindow.setGravity(Gravity.BOTTOM);
        dialogWindow.setWindowAnimations(R.style.PopupWindowAnimation1);
        attributes.width = ViewGroup.LayoutParams.MATCH_PARENT;
        attributes.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        // lp.alpha=0.3f;//设置背景透明度
        dialogWindow.setAttributes(attributes);
        popupDialog1.show();

        Button btnTakePhoto = view.findViewById(btn_take_photo);
        btnTakePhoto.setClickable(false);
        btnTakePhoto.setText("联系店铺客服人员");
        btnTakePhoto.setTextColor(activity.getResources().getColor(R.color.text_gray));
        Button btnPickPhoto = view.findViewById(btn_pick_photo);
        btnPickPhoto.setText(phone);
        btnPickPhoto.setTextColor(Color.parseColor("#4381BC"));
        btnPickPhoto.setOnClickListener(new CallClick(popupDialog1, phone));
        Button btnCancel = view.findViewById(R.id.btn_cancel);
        btnCancel.setText("取消");
        btnCancel.setTextColor(Color.parseColor("#4381BC"));
        btnCancel.setOnClickListener(new CallClick(popupDialog1));
    }

    private class CallClick implements View.OnClickListener {

        String phone;
        Dialog popupDialog1;

        CallClick(Dialog popupDialog1) {
            this.popupDialog1 = popupDialog1;
        }

        CallClick(Dialog popupDialog1, String phone) {
            this.popupDialog1 = popupDialog1;
            this.phone = phone;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case btn_pick_photo:
                    if (CommonUtils.isFastDoubleClick()) {
                        return;
                    }
                    dialCall(activity, phone);
                    if (popupDialog1 != null && popupDialog1.isShowing()) {
                        popupDialog1.cancel();
                    }
                    break;

                case R.id.btn_cancel:
                    if (CommonUtils.isFastDoubleClick()) {
                        return;
                    }
                    if (popupDialog1 != null && popupDialog1.isShowing()) {
                        popupDialog1.cancel();
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 调用系统拨打电话
     */
    private void dialCall(Context context, String phoneno) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + phoneno));
//        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//            return;
//        }
        context.startActivity(intent);
    }

    @Override
    public void discountDialog(List<LableInfo> lableInfos) {
        Dialog discountDialog = new Dialog(activity, R.style.dialog_fullscreen);
        View view = activity.getLayoutInflater().inflate(R.layout.dialog_merchant_discount, null);
        initDiscountView(view, lableInfos, discountDialog);
        discountDialog.setContentView(view);
        discountDialog.show();
    }

    private void initDiscountView(View view, List<LableInfo> lableInfos, final Dialog discountDialog) {
        LinearLayout dialogDiscountLl = view.findViewById(R.id.dialog_discount_ll);
        dialogDiscountLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                discountDialog.dismiss();
            }
        });
        RecyclerView dialogDiscountRv = view.findViewById(R.id.dialog_discount_rv);
        dialogDiscountRv.setLayoutManager(new LinearLayoutManager(activity));
        DiscountDialogAdapter dialogAdapter = new DiscountDialogAdapter(activity);
        dialogDiscountRv.setAdapter(dialogAdapter);
        dialogAdapter.resetData(lableInfos);
    }

}
