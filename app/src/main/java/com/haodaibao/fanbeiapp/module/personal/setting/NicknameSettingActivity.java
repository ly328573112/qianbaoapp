package com.haodaibao.fanbeiapp.module.personal.setting;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.StatusBarHelper;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.event.PersonalFragmentEvent;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;

public class NicknameSettingActivity extends BaseActivity implements NicknameSettingContract.View {

    private static final int UPDATE_NICK_NAME_SUCCESS = 300;
    private boolean isRequest;
    @BindView(R.id.toolbar_back)
    RelativeLayout mToolbarBack;
    @BindView(R.id.et_nickname)
    EditText mEtNickname;
    private String mPreNickname;
    private NicknameSettingPresenter mPresenter;
    private String mNickname;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_nickname_setting;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(NicknameSettingActivity.this);
        mPreNickname = getIntent().getStringExtra("nickname");
        if (!TextUtils.isEmpty(mPreNickname)) {
            mEtNickname.setText(mPreNickname);
            mEtNickname.setSelection(mPreNickname.length());
        }
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        if (mPresenter == null) {
            mPresenter = new NicknameSettingPresenter(this);
        }
    }

    @OnClick({R.id.toolbar_back, R.id.tv_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.tv_save:
                updateNikeName();
                break;
        }
    }

    private void updateNikeName() {
        if (!isRequest) {
            mNickname = mEtNickname.getText().toString();
            mPresenter.updateNickname(mNickname);
            isRequest = true;
        }
    }

    @Override
    public void updateNicknameSuccess() {
        isRequest = false;
        Intent intent = new Intent();
        intent.putExtra("nickname", mNickname);
        android.util.Log.e("xxxxxx", "nickname = " + mNickname);
        setResult(UPDATE_NICK_NAME_SUCCESS, intent);
        PersonalFragmentEvent event = new PersonalFragmentEvent();
        event.setRefreshUserInfo(true);
        EventBus.getDefault().post(event);
        finish();
    }
}
