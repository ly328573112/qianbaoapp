package com.haodaibao.fanbeiapp.module;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.PriedsBean;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 开发者：LuoYi
 * Time: 2017 17:20 2017/12/19 12
 */

public class MerchantDiscountListAdapter extends BaseRecycleViewAdapter<PriedsBean> {

    private boolean showNewView;

    public MerchantDiscountListAdapter(Context context, boolean showNewView) {
        super(context);
        this.showNewView = showNewView;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, PriedsBean item) {
        DiscountHolder discountHolder = (DiscountHolder) holder;
        if ((mData.size() - 1) == holder.getLayoutPosition()) {
            discountHolder.newView.setVisibility(View.GONE);
        } else {
            discountHolder.newView.setVisibility(View.VISIBLE);
        }
        if (TextUtils.equals(item.getAdaptType(), "1")) {
            discountHolder.newUserIconIv.setImageResource(R.drawable.new_user_discount_icon);
        } else {
            discountHolder.newUserIconIv.setImageResource(R.drawable.discount_red_icon);
        }
        discountHolder.newUserDiscountIv.setText(item.getContent());
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new DiscountHolder(View.inflate(mContext, R.layout.item_merchant_discount, null));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class DiscountHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.new_user_icon_iv)
        ImageView newUserIconIv;
        @BindView(R.id.tv_new_user_discount)
        TextView newUserDiscountIv;
        @BindView(R.id.new_view)
        View newView;

        public DiscountHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
