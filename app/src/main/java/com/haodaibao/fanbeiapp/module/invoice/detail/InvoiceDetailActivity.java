package com.haodaibao.fanbeiapp.module.invoice.detail;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.invoice.makeout.MakeoutInvoiceActivity;
import com.haodaibao.fanbeiapp.repository.json.InvoiceDetailBean;

import butterknife.BindView;
import butterknife.OnClick;

import static com.baseandroid.config.Constant.INVOICEID;
import static com.baseandroid.config.Constant.INVOICE_DETAIL_RESULTCODE;

/**
 * date: 2018/2/11.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class InvoiceDetailActivity extends BaseActivity implements InvoiceDetailContract.View {
    @BindView(R.id.invoice_status)
    TextView invoice_status;
    @BindView(R.id.invoice_detail_top)
    TextView invoice_detail_top;
    @BindView(R.id.invoice_detail_merchant)
    TextView invoice_detail_merchant;
    @BindView(R.id.invoice_detail_time)
    TextView invoice_detail_time;
    @BindView(R.id.invoice_detail_money)
    TextView invoice_detail_money;
    @BindView(R.id.invoice_detail_order)
    TextView invoice_detail_order;
    @BindView(R.id.invoice_detail_phone)
    TextView invoice_detail_phone;
    @BindView(R.id.invoice_detail_email)
    TextView invoice_detail_email;
    @BindView(R.id.toolbar)
    View toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @BindView(R.id.toolbar_back)
    View toolbar_back;
    @BindView(R.id.makeout_again)
    TextView makeout_again;
    @BindView(R.id.iv_toolbar_back)
    ImageView iv_toolbar_back;
    @BindView(R.id.toolbar_line)
    View toolbar_line;
    @BindView(R.id.invoice_status_msg)
    TextView invoice_status_msg;

    private InvoicePresenter presenter;
    private String id;
    private InvoiceDetailBean.Invoice invoiceBean;

    public static void start(BaseActivity context, String id) {
        Intent intent = new Intent(context, InvoiceDetailActivity.class);
        intent.putExtra(INVOICEID, id);
        context.startActivityForResult(intent, 0);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_invoice_detail;
    }

    @Override
    protected void setupView() {
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.no_color));
        toolbar_title.setText("");
        iv_toolbar_back.setImageResource(R.drawable.icon_back_white);
        toolbar_line.setVisibility(View.GONE);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        Intent intent = getIntent();
        id = intent.getStringExtra(INVOICEID);
        presenter = new InvoicePresenter(this);
        presenter.getInvoiceDetail(id);
    }

    @OnClick({R.id.toolbar_back, R.id.makeout_again})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                setResult(INVOICE_DETAIL_RESULTCODE);
                finish();
                break;
            case R.id.makeout_again:
                MakeoutInvoiceActivity.start(this, invoiceBean.order_no, invoiceBean.merchant_no, invoiceBean.invoice_id, true);
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                setResult(INVOICE_DETAIL_RESULTCODE);
                finish();
                break;

            default:
                break;
        }
        return true;
    }

    @Override
    public void updateUI(InvoiceDetailBean.Invoice invoice) {
        invoiceBean = invoice;
        id = invoiceBean.invoice_id;
        invoice_detail_top.setText(invoice.rise_name);
        invoice_detail_merchant.setText(invoice.merchant_register_name);
        if (TextUtils.isEmpty(invoice.invoice_date)) {
            if (!TextUtils.isEmpty(invoice.create_time) && invoice.create_time.contains(" ")) {
                invoice_detail_time.setText(invoice.create_time.substring(0, invoice.create_time.indexOf(" ")));
            } else {
                invoice_detail_time.setText(invoice.create_time);
            }
        } else {
            if (invoice.invoice_date.contains(" ")) {
                invoice_detail_time.setText(invoice.invoice_date.substring(0, invoice.invoice_date.indexOf(" ")));
            } else {
                invoice_detail_time.setText(invoice.invoice_date);
            }
        }
        invoice_detail_money.setText(invoice.invoice_amount);
        invoice_detail_order.setText(invoice.order_no);
        invoice_detail_phone.setText(invoice.mobile.substring(0, 3) + "****" + invoice.mobile.substring(7));
        invoice_detail_email.setText(invoice.email);
        if (TextUtils.equals(invoice.status, "1")) {
            makeout_again.setVisibility(View.GONE);
            invoice_status_msg.setVisibility(View.GONE);
            invoice_status.setText(R.string.text_makeout_success);
            Drawable rightDrawable = getResources().getDrawable(R.drawable.icon_invoice_success);
            rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());
            invoice_status.setCompoundDrawables(rightDrawable, null, null, null);
        } else if (TextUtils.equals(invoice.status, "2")) {
            invoice_status.setText(R.string.text_makeout_failed);
            invoice_status_msg.setVisibility(View.VISIBLE);
            invoice_status_msg.setText(R.string.invoice_failed_tips);
            makeout_again.setVisibility(View.VISIBLE);
            Drawable rightDrawable = getResources().getDrawable(R.drawable.icon_invoice_failed);
            rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());
            invoice_status.setCompoundDrawables(rightDrawable, null, null, null);
        } else if (TextUtils.equals(invoice.status, "3")) {
            makeout_again.setVisibility(View.VISIBLE);
            invoice_status_msg.setVisibility(View.VISIBLE);
            invoice_status.setText(R.string.invoice_makeout_nouse);
            invoice_status_msg.setText(R.string.invoice_nouse_tips);
            Drawable rightDrawable = getResources().getDrawable(R.drawable.icon_invoice_nouse);
            rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());
            invoice_status.setCompoundDrawables(rightDrawable, null, null, null);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.getInvoiceDetail(id);
    }
}
