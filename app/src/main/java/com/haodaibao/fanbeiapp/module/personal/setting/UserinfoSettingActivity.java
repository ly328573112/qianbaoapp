package com.haodaibao.fanbeiapp.module.personal.setting;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.camera.CameraUtil;
import com.baseandroid.config.Global;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.StatusBarHelper;
import com.bumptech.glide.Glide;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.event.PersonalFragmentEvent;
import com.haodaibao.fanbeiapp.imageadapter.CameraSelectView;
import com.haodaibao.fanbeiapp.repository.json.PhotoInfo;
import com.haodaibao.fanbeiapp.repository.json.UserDate;
import com.makeramen.roundedimageview.RoundedImageView;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class UserinfoSettingActivity extends BaseActivity implements UserinfoSettingContract.View, CameraSelectView.CameraPotoListener {
    private static final int SET_NICKNAME_REQUEST = 100;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.riv_head_userinfo)
    RoundedImageView mRivHeadUserinfo;
    @BindView(R.id.tv_nickname)
    TextView mTvNickname;
    private String mUserName;
    private String mHeadIcon;
    private UserinfoSettingPresenter mPresenter;
    private CameraSelectView mCameraSelectView;
    private CameraUtil mCameraUtil;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_userinfo_setting;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(this);
        mToolbarTitle.setText("设置");
        mUserName = Global.getUserInfo().getAlias();
        if (TextUtils.isEmpty(mUserName)) {
            mTvNickname.setText("添加昵称");
        } else {
            mTvNickname.setText(mUserName);
        }
        mHeadIcon = Global.getUserInfo().getFace();
        Glide.with(Global.getContext()).load(CommonUtils.getImageUrl(mHeadIcon)).asBitmap()
                .error(R.drawable.icon_default_head_info).placeholder(R.drawable.icon_default_head_info)
                .centerCrop()
                .into(mRivHeadUserinfo);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        if (mPresenter == null) {
            mPresenter = new UserinfoSettingPresenter(this);
        }
    }

    @OnClick({R.id.toolbar_back, R.id.ll_head, R.id.ll_nickname})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                ActivityJumpUtil.goBack(UserinfoSettingActivity.this);
                break;
            case R.id.ll_head:
                setUserHeadIcon();
                break;
            case R.id.ll_nickname:
                gotoNicknameSetting();
                break;
        }
    }

    private void setUserHeadIcon() {
        if (mCameraSelectView == null) {
            mCameraSelectView = new CameraSelectView(this);
        }
        if (mCameraUtil == null) {
            mCameraUtil = new CameraUtil(this);
        }
        mCameraSelectView.showCameraDialog(mCameraUtil, 1);
        mCameraSelectView.setPictureCut(true);
        mCameraSelectView.setCameraPotoListener(this);
    }

    //修改用户名
    private void gotoNicknameSetting() {
        Intent intent = new Intent(this, NicknameSettingActivity.class);
        intent.putExtra("nickname", mUserName);
        startActivityForResult(intent, SET_NICKNAME_REQUEST);
    }

    //退出登录
//    private void checkOut() {
//        if (mPresenter == null) {
//            mPresenter = new UserinfoSettingPresenter(this);
//        }
//        mPresenter.checkOut();
//    }

    @Override
    public void setupUserinfo(UserDate bean) {
        Global.setUserInfo(bean.getUser());
        setupView();
    }

    @Override
    public void uploadHeadViewSuccess() {
        closeUploading();
        PersonalFragmentEvent event = new PersonalFragmentEvent();
        event.setRefreshUserInfo(true);
        EventBus.getDefault().post(event);
    }

    //登出请求成功回调
    @Override
    public void checkOutSuccess() {
//        Global.loginOutclear();
//        PersonalFragmentEvent event = new PersonalFragmentEvent();
//        event.setLogOut(true);
//        EventBus.getDefault().post(event);
//        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (SET_NICKNAME_REQUEST == requestCode) {
            if (300 == resultCode) {
                String nickname = data.getStringExtra("nickname");
                Global.getUserInfo().setAlias(nickname);
                android.util.Log.e("xxxxxx", "nickname = " + nickname);
                setupView();
            }
        } else {
            mCameraUtil.onHandleActivityResult(requestCode, resultCode, data);
        }
    }

    //图片选择回调
    @Override
    public void onResultPotoList(List<PhotoInfo> list) {
        PhotoInfo info = list.get(0);
        Glide.with(Global.getContext()).load(info.getPhotoUri()).asBitmap()
                .error(R.drawable.icon_default_head_info).placeholder(R.drawable.icon_default_head_info)
                .centerCrop()
                .into(mRivHeadUserinfo);
        showUploading();
        HashMap<String, String> params = new HashMap<>();
        mPresenter.uploadHeadView(params, info.getPhotoUri());
    }
}
