package com.haodaibao.fanbeiapp.module.merchant.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.PeriodsInfo;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("InflateParams")
public class ConsumeHintAdapter extends BaseRecycleViewAdapter<PeriodsInfo> {

    private final LayoutInflater inflater;
    private String refundmode = "";

    public ConsumeHintAdapter(Context context) {
        super(context);
        inflater = LayoutInflater.from(mContext);
    }

    public void setRefundmode(String refundmode) {
        this.refundmode = refundmode;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, PeriodsInfo item) {
        ConsumeHolder consumeHolder = (ConsumeHolder) holder;
        // 优惠日期
        consumeHolder.tv_consume_hint_day.setText(item.getDaystring());
        // 优惠时间
        if (item.getTimestring() != null) {
            consumeHolder.tv_consume_hint_time.setText(item.getTimestring().replace(",", "-"));
        }

        String refundMode2 = getRefundMode(refundmode, item.getReturnrate(), item.getReturnbeginamount(), item.getReturnrate2(), item.getReturnbegin2amount());
        consumeHolder.tv_consume_hint_lijian.setText(refundMode2);
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.adapter_consume_hint, null);
        return new ConsumeHolder(view);
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class ConsumeHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_consume_hint_day)
        TextView tv_consume_hint_day;
        @BindView(R.id.tv_consume_hint_time)
        TextView tv_consume_hint_time;
        @BindView(R.id.tv_consume_hint_lijian)
        TextView tv_consume_hint_lijian;

        public ConsumeHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private String getRefundMode(String refundmode, String returnrate, String returnbeginamount, String returnrate2, String returnbegi2namount) {
        String refund = "";
        if (TextUtils.isEmpty(refundmode)) return refund;

        boolean isReturnrate = !TextUtils.isEmpty(returnrate);
        if (refundmode.equals("1") && isReturnrate) {
            DecimalFormat df = new DecimalFormat("0.0");
            refund = df.format((1 - Double.parseDouble(returnrate)) * 10) + "折";
        } else if (refundmode.equals("2") && isReturnrate) {
            DecimalFormat df1 = new DecimalFormat("0");
            boolean isReturnbeginamount = !TextUtils.isEmpty(returnbeginamount);
            if (isReturnbeginamount && !TextUtils.isEmpty(returnbegi2namount)) {
                refund = "满" + getFormant(df1, returnbeginamount) + "减" + getFormant(df1, returnrate) + "元\n满" + getFormant(df1, returnbegi2namount) + "减" + getFormant(df1, returnrate2) + "元";
            } else if (isReturnbeginamount) {
                refund = "满" + getFormant(df1, returnbeginamount) + "减" + getFormant(df1, returnrate) + "元";
            }
        } else if (refundmode.equals("3") && isReturnrate) {
            DecimalFormat df2 = new DecimalFormat("0");
            refund = "每满" + getFormant(df2, returnbeginamount) + "减" + getFormant(df2, returnrate) + "元";
        }
        return refund;
    }

    private String getFormant(DecimalFormat df, String retuStr) {
        return df.format(Double.parseDouble(retuStr));
    }

}
