package com.haodaibao.fanbeiapp.module.personal.coupon;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.StatusBarHelper;
import com.haodaibao.fanbeiapp.R;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import butterknife.BindView;
import butterknife.OnClick;

public class CouponActivity extends BaseActivity {

    @BindView(R.id.toolbar_title)
    TextView    mToolbarTitle;
    @BindView(R.id.fl)
    FrameLayout mFl;
    private CouponFragemnt      mUsefulFragment;
    private CouponFragemnt      mUselessFragment;
    private CouponHistoryFragment mHistoryFragment ;
    private FragmentManager     mFragmentManager;
    private FragmentTransaction mTransaction;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_coupon;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(CouponActivity.this);
        mToolbarTitle.setText("生活优惠券");
        showFragment(true);
    }

    public void showFragment(boolean b) {
        mFragmentManager = getSupportFragmentManager();
        mTransaction = mFragmentManager.beginTransaction();
        if (mUsefulFragment == null) {
            mUsefulFragment = new CouponFragemnt();
            Bundle usefulBundle = new Bundle();
            usefulBundle.putBoolean("mTag", true);
            mUsefulFragment.setArguments(usefulBundle);
        }
        if (mUselessFragment == null) {
            mUselessFragment = new CouponFragemnt();
            Bundle usefulBundle = new Bundle();
            usefulBundle.putBoolean("mTag", false);
            mUselessFragment.setArguments(usefulBundle);
        }
        if (b) {
            if (mUselessFragment.isVisible()) {
                mTransaction.hide(mUselessFragment);
            }
            if (mUsefulFragment.isAdded()) {
                mTransaction.show(mUsefulFragment);
            } else {
                mTransaction.add(R.id.fl, mUsefulFragment);
            }
        } else {
            if (mUsefulFragment.isVisible()) {
                mTransaction.hide(mUsefulFragment);
            }
            if (mUselessFragment.isAdded()) {
                mTransaction.show(mUselessFragment);
            } else {
                mTransaction.add(R.id.fl, mUselessFragment);
            }
        }
        mTransaction.commit();
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {

    }

    @OnClick(R.id.toolbar_back)
    public void onViewClicked() {
        if (mUselessFragment != null && mUselessFragment.isVisible()) {
            showFragment(true);
        } else {
            ActivityJumpUtil.goBack(CouponActivity.this);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if (mUselessFragment != null && mUselessFragment.isVisible()) {
                    showFragment(true);
                    return false;
                }
                break;

            default:
                break;
        }
        return super.onKeyDown(keyCode, event);
    }
}
