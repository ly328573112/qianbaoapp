package com.haodaibao.fanbeiapp.module.personal.collections;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.MerchantListAdapterView;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;

/**
 * Created by Routee on 2017/9/4.
 * description: ${cusor}
 */

class CollectionsRvAdapter extends BaseRecycleViewAdapter<MerchantInfoEntiy> {

    private OnClickListener mListener;
    private final MerchantListAdapterView merchantListAdapterView;

    public CollectionsRvAdapter(Context context) {
        super(context);
        merchantListAdapterView = new MerchantListAdapterView(mContext);
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, MerchantInfoEntiy item) {
        MerchantListAdapterView.MerchantViewHolder hold = (MerchantListAdapterView.MerchantViewHolder) holder;
        merchantListAdapterView.setMerchantView(hold, item);
        final String merchantno = item.getMerchantno();
        hold.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener == null) {
                    return;
                }
                mListener.click(merchantno);
            }
        });
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(mContext, R.layout.adapter_shanghusearchresult, null);
        return new MerchantListAdapterView.MerchantViewHolder(view);
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    public interface OnClickListener {
        void click(String str);
    }

    public void setClickListener(OnClickListener listener) {
        this.mListener = listener;
    }
}
