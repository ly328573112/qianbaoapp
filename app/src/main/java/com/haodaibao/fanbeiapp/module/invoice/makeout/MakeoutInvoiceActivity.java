package com.haodaibao.fanbeiapp.module.invoice.makeout;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.ToastUtils;
import com.baseandroid.widget.customtext.edittextview.EditTextWithDelNormal;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.event.MakoutInvoiceEvent;
import com.haodaibao.fanbeiapp.module.dialog.InvoiceDialog;
import com.haodaibao.fanbeiapp.module.invoice.detail.InvoiceDetailActivity;
import com.haodaibao.fanbeiapp.module.invoice.headselect.InvoiceHeadSelectActivity;
import com.haodaibao.fanbeiapp.repository.json.InvoiceHeadBean;
import com.haodaibao.fanbeiapp.repository.json.MakeoutApplyBean;
import com.haodaibao.fanbeiapp.repository.json.MakeoutInvoiceBean;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;

import static com.baseandroid.config.Constant.HEADER_SELECT_RESULTCODE;
import static com.baseandroid.config.Constant.INVOICEHEADITEM;
import static com.baseandroid.config.Constant.INVOICE_DETAIL_RESULTCODE;
import static com.qianbao.shiningwhitelibrary.utils.StringUtil.isEmail;

/**
 * date: 2018/2/6.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class MakeoutInvoiceActivity extends BaseActivity implements MakeoutInvoiceContract.View {
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @BindView(R.id.toolbar_back)
    RelativeLayout toolbar_back;
    @BindView(R.id.makeout_orderno)
    TextView makeout_orderno;
    @BindView(R.id.makeout_top_select)
    TextView makeout_top_select;
    @BindView(R.id.makeout_pro)
    TextView makeout_pro;
    @BindView(R.id.makeout_cashier)
    TextView makeout_cashier;
    @BindView(R.id.makeout_ragroup)
    RadioGroup makeout_ragroup;
    @BindView(R.id.makeout_taxno_text)
    TextView makeout_taxno_text;
    @BindView(R.id.makeout_taxno)
    EditTextWithDelNormal makeout_taxno;
    @BindView(R.id.makeout_top_name)
    EditTextWithDelNormal makeout_top_name;
    @BindView(R.id.makeout_phone)
    EditTextWithDelNormal makeout_phone;
    @BindView(R.id.makeout_email)
    EditTextWithDelNormal makeout_email;
    @BindView(R.id.makeout_address)
    EditTextWithDelNormal makeout_address;
    @BindView(R.id.makeout_comphone)
    EditTextWithDelNormal makeout_comphone;
    @BindView(R.id.makeout_band)
    EditTextWithDelNormal makeout_band;
    @BindView(R.id.makeout_bandnum)
    EditTextWithDelNormal makeout_bandnum;

    @BindView(R.id.more_ly)
    RelativeLayout more_ly;
    @BindView(R.id.makeout_more)
    TextView makeout_more;
    @BindView(R.id.makeout_invoice)
    TextView makeout_invoice;
    @BindView(R.id.makeout_rb_compony)
    RadioButton makeout_rb_compony;
    @BindView(R.id.makeout_rb_person)
    RadioButton makeout_rb_person;
    @BindView(R.id.makeout_error_tips)
    TextView makeout_error_tips;
    @BindView(R.id.line4)
    View line4;
    @BindView(R.id.iv_scale)
    ImageView iv_scale;

    private MakeoutApplyBean.ApplyInvoice invoiceData;
    private InvoiceDialog invoiceDialog;
    private String orderNo;
    private String merchantNo;
    private MakeoutInvoicePresenter presenter;
    private InvoiceHeadBean.InvoiceHeadItem invoiceHeadItem;
    private String rise_type_code, riseNo, riseName, taxNo, mobile, email, companyAddress, companyMobile, companyBankName, companyBankNo;
    private String riseNamePer, mobilePer, emailPer;
    private String id, riseId;
    private boolean isFromInvoiceDetail;

    public static void start(BaseActivity context, String orderNo, String merchantNo, String id, boolean isFromInvoiceDetail) {
        Intent intent = new Intent(context, MakeoutInvoiceActivity.class);
        intent.putExtra("orderNo", orderNo);
        intent.putExtra("merchantNo", merchantNo);
        intent.putExtra("id", id);
        intent.putExtra("isFromInvoiceDetail", isFromInvoiceDetail);
        context.startActivityForResult(intent, 0);
    }

    @Override
    protected int getLayoutId() {
        Intent intent = getIntent();
        orderNo = intent.getStringExtra("orderNo");
        merchantNo = intent.getStringExtra("merchantNo");
        isFromInvoiceDetail = intent.getBooleanExtra("isFromInvoiceDetail", false);
        id = intent.getStringExtra("id");
        return R.layout.activity_makeout_invoice;
    }

    @Override
    protected void setupView() {
//        int supportWhiteStatusBar = StatusBarHelper.StatusBarLightMode(this);
//        if (supportWhiteStatusBar > 0) {
//            StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.white), 0);
//        } else {
//            StatusBarUtil.setColor(this, ContextCompat.getColor(this, android.R.color.black), 0);
//        }
        toolbar_title.setText("开票申请");
        makeout_ragroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.makeout_rb_compony) {
                    setTaxShow(View.VISIBLE);
                    rise_type_code = "1";
                    makeout_more.setVisibility(View.VISIBLE);
//                    iv_scale.setRotation(180);
                    iv_scale.setVisibility(View.VISIBLE);
//                    more_ly.setVisibility(View.VISIBLE);
                    riseNamePer = makeout_top_name.getText().toString();
                    mobilePer = makeout_phone.getText().toString();
                    emailPer = makeout_email.getText().toString();
                    makeout_top_name.setText(riseName);
                    makeout_phone.setText(mobile);
                    makeout_email.setText(email);
                    makeout_top_name.setHint(getString(R.string.invoice_rise_hint));
                } else if (checkedId == R.id.makeout_rb_person) {
                    setTaxShow(View.GONE);
                    rise_type_code = "2";
                    makeout_more.setVisibility(View.GONE);
                    more_ly.setVisibility(View.GONE);
//                    iv_scale.setRotation(0);
                    iv_scale.setVisibility(View.GONE);
                    riseName = makeout_top_name.getText().toString();
                    mobile = makeout_phone.getText().toString();
                    email = makeout_email.getText().toString();
                    makeout_top_name.setText(riseNamePer);
                    makeout_phone.setText(mobilePer);
                    makeout_email.setText(emailPer);
                    makeout_top_name.setHint(getString(R.string.invoice_rise_person_hint));
                }
            }
        });
    }

    public void setTaxShow(int show) {
        makeout_taxno_text.setVisibility(show);
        makeout_taxno.setVisibility(show);
        line4.setVisibility(show);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        presenter = new MakeoutInvoicePresenter(this);
        presenter.getApplyInfo(orderNo, merchantNo);
    }

    @OnClick({R.id.toolbar_back, R.id.makeout_top_select, R.id.makeout_more, R.id.makeout_invoice})
    public void onclick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                finish();
                break;
            case R.id.makeout_top_select:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
//                makeout_rb_compony.setChecked(false);
//                makeout_rb_person.setChecked(false);

                InvoiceHeadSelectActivity.startForResult(this, riseId);
                break;
            case R.id.makeout_more:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                if (more_ly.getVisibility() == View.VISIBLE) {
                    more_ly.setVisibility(View.GONE);
                    iv_scale.setRotation(180);
                } else {
                    more_ly.setVisibility(View.VISIBLE);
                    iv_scale.setRotation(0);
                }
                break;
            case R.id.makeout_invoice:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                applyInvoice();
//
                break;
        }
    }

    private void applyInvoice() {
        if (invoiceData != null) {
            if (TextUtils.isEmpty(makeout_top_name.getText().toString())) {
                ToastUtils.showShortToast("发票抬头不能为空");
                return;
            }
            if (TextUtils.isEmpty(makeout_taxno.getText().toString()) && TextUtils.equals(rise_type_code, "1")) {
                ToastUtils.showShortToast("税号不能为空");
                return;
            }
            if (TextUtils.isEmpty(makeout_phone.getText().toString())) {
                ToastUtils.showShortToast("手机号不能为空");
                return;
            }
            if (TextUtils.isEmpty(makeout_email.getText().toString())) {
                ToastUtils.showShortToast("邮箱不能为空");
                return;
            }
            if ((makeout_taxno.getText().toString().length() < 15 || makeout_taxno.getText().toString().length() > 20) && TextUtils.equals(rise_type_code, "1")) {
                ToastUtils.showShortToast("公司税号为15-20位");
                return;
            }
            if (!CommonUtils.isMobile(makeout_phone.getText().toString())) {
                ToastUtils.showShortToast("手机号格式有误");
                return;
            }
            if (!isEmail(makeout_email.getText().toString())) {
                ToastUtils.showShortToast("邮箱格式有误");
                return;
            }
            if (TextUtils.equals(rise_type_code, "1")) {
                presenter.applyInvoice(id, invoiceData.order_no, merchantNo, invoiceData.invoice_type_code, rise_type_code, riseNo, makeout_top_name.getText().toString(), makeout_taxno.getText().toString(), makeout_phone.getText().toString(), makeout_email.getText().toString(), makeout_address.getText().toString(), makeout_comphone.getText().toString(), makeout_band.getText().toString(), makeout_bandnum.getText().toString(), invoiceData.invoice_project, invoiceData.invoice_amount);
            } else if (TextUtils.equals(rise_type_code, "2")) {
                presenter.applyInvoice(id, invoiceData.order_no, merchantNo, invoiceData.invoice_type_code, rise_type_code, riseNo, makeout_top_name.getText().toString(), "", makeout_phone.getText().toString(), makeout_email.getText().toString(), "", "", "", "", invoiceData.invoice_project, invoiceData.invoice_amount);
            }
//            presenter.applyInvoice(invoiceData.invoice_id, invoiceData.order_no, invoiceData.merchant_no, invoiceData.invoice_type_code, rise_type_code, riseNo, riseName, taxNo, mobile, email, companyAddress, companyMobile, companyBankName, companyBankNo, invoiceData.invoice_project, invoiceData.invoice_amount);

            if (invoiceDialog == null) {
                invoiceDialog = InvoiceDialog.newInstance();
            }
            if (!invoiceDialog.isVisible()) {
                invoiceDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        invoiceDialog.setIv_top(R.drawable.bg_invoice_dialog_making);
                        invoiceDialog.setInvoice_make_statue_text(getString(R.string.text_makeouting));
                        invoiceDialog.setInvoice_make_statue_message(getString(R.string.text_makeouting_message));
                        invoiceDialog.setInvoice_make_btn(View.GONE);
                    }
                });
            }
            if (!invoiceDialog.isAdded()) {
                invoiceDialog.show(getSupportFragmentManager(), "invoiceDialog");
            }
        }
    }

    @Override
    public void updateUI(MakeoutApplyBean.ApplyInvoice applyInvoice) {
        invoiceData = applyInvoice;
        riseNo = invoiceData.rise_no;
        if (TextUtils.equals(invoiceData.rise_type_code, "1")) {
            makeout_rb_compony.setChecked(true);
            riseName = invoiceData.rise_name;
            mobile = invoiceData.mobile;
            email = invoiceData.email;
            companyAddress = invoiceData.company_address;
            companyBankName = invoiceData.company_bank_name;
            companyBankNo = invoiceData.company_bank_no;
            companyMobile = invoiceData.company_mobile;
            taxNo = invoiceData.tax_no;
            makeout_taxno.setText(taxNo);
            makeout_top_name.setText(riseName);
            makeout_phone.setText(mobile);
            makeout_email.setText(email);
            makeout_address.setText(invoiceData.company_address);
            makeout_comphone.setText(invoiceData.company_mobile);
            makeout_band.setText(invoiceData.company_bank_name);
            makeout_bandnum.setText(invoiceData.company_bank_no);
        } else if (TextUtils.equals(invoiceData.rise_type_code, "2")) {
            makeout_rb_person.setChecked(true);
            riseNamePer = invoiceData.rise_name;
            mobilePer = invoiceData.mobile;
            emailPer = invoiceData.email;
            makeout_top_name.setText(riseNamePer);
            makeout_phone.setText(mobilePer);
            makeout_email.setText(emailPer);
        } else {
            makeout_rb_compony.setChecked(true);
        }
        makeout_orderno.setText(applyInvoice.order_no);
        if (TextUtils.equals(applyInvoice.show_rise_button, "1")) {
            makeout_top_select.setVisibility(View.VISIBLE);
        } else {
            makeout_top_select.setVisibility(View.GONE);
        }
        makeout_pro.setText(applyInvoice.invoice_project);
        makeout_cashier.setText(getString(R.string.rmb) + applyInvoice.invoice_amount);

    }

    @Override
    public void upDateStatus(final boolean success, final MakeoutInvoiceBean makeoutApplyBean) {
        if (invoiceDialog == null) {
            return;
        }
        toolbar_title.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (invoiceDialog == null || invoiceDialog.invoice_make_btn == null) {
                    return;
                }
                invoiceDialog.setInvoice_make_btn(View.VISIBLE);

                if (success) {
                    invoiceDialog.invoice_make_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (isFromInvoiceDetail) {
                                finish();
                            } else {
                                InvoiceDetailActivity.start(MakeoutInvoiceActivity.this, makeoutApplyBean.invoice.invoice_id);
                            }
                        }
                    });
                    MakoutInvoiceEvent event = new MakoutInvoiceEvent();
                    event.makeoutApplyBean = makeoutApplyBean;
                    EventBus.getDefault().post(event);
                    if (TextUtils.equals(makeoutApplyBean.invoice.status, "1")) {
                        invoiceDialog.setIv_top(R.drawable.bg_invoice_dialog_success);
                        invoiceDialog.setInvoice_make_statue_text(getString(R.string.text_makeout_success));
                        invoiceDialog.setInvoice_make_statue_message(getString(R.string.text_makeouting_message_success));
                    } else {
                        invoiceDialog.setIv_top(R.drawable.bg_invoice_dialog_failed);
                        invoiceDialog.setInvoice_make_statue_text(getString(R.string.text_makeout_failed));
                        invoiceDialog.setInvoice_make_statue_message(getString(R.string.text_makeouting_message_again));
                    }

                } else {
                    invoiceDialog.dismiss();
                }
            }
        }, 300);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == HEADER_SELECT_RESULTCODE) {
            invoiceHeadItem = (InvoiceHeadBean.InvoiceHeadItem) data.getSerializableExtra(INVOICEHEADITEM);
            setHeadData(invoiceHeadItem);
        } else if (resultCode == INVOICE_DETAIL_RESULTCODE) {
            finish();
        }
    }

    private void setHeadData(InvoiceHeadBean.InvoiceHeadItem invoiceHeadItem) {
        if (invoiceHeadItem != null) {
            riseNo = invoiceHeadItem.rise_no;
            riseId = invoiceHeadItem.rise_id;
//            makeout_email.setText(invoiceHeadItem.email);
//            makeout_phone.setText(invoiceHeadItem.mobile);

//            makeout_top_name.setText(invoiceHeadItem.rise_name);
//            makeout_ragroup.clearCheck();
            if (TextUtils.equals(invoiceHeadItem.rise_type_code, "1")) {
                makeout_rb_compony.setChecked(true);
                taxNo = invoiceHeadItem.tax_no;

                riseName = invoiceHeadItem.rise_name;
                mobile = invoiceHeadItem.mobile;
                email = invoiceHeadItem.email;
                companyAddress = invoiceHeadItem.company_address;
                companyBankName = invoiceHeadItem.company_bank_name;
                companyBankNo = invoiceHeadItem.company_bank_no;
                companyMobile = invoiceHeadItem.company_mobile;
                setTaxShow(View.VISIBLE);
                makeout_more.setVisibility(View.VISIBLE);
//                    more_ly.setVisibility(View.VISIBLE);
                riseNamePer = makeout_top_name.getText().toString();
                mobilePer = makeout_phone.getText().toString();
                emailPer = makeout_email.getText().toString();
                makeout_top_name.setText(riseName);
                makeout_phone.setText(mobile);
                makeout_email.setText(email);
                makeout_taxno.setText(invoiceHeadItem.tax_no);
                makeout_address.setText(invoiceHeadItem.company_address);
                makeout_comphone.setText(invoiceHeadItem.company_mobile);
                makeout_band.setText(invoiceHeadItem.company_bank_name);
                makeout_bandnum.setText(invoiceHeadItem.company_bank_no);
            } else if (TextUtils.equals(invoiceHeadItem.rise_type_code, "2")) {
                makeout_rb_person.setChecked(true);
                riseNamePer = invoiceHeadItem.rise_name;
                mobilePer = invoiceHeadItem.mobile;
                emailPer = invoiceHeadItem.email;
                setTaxShow(View.GONE);
                makeout_more.setVisibility(View.GONE);
                more_ly.setVisibility(View.GONE);
                riseName = makeout_top_name.getText().toString();
                mobile = makeout_phone.getText().toString();
                email = makeout_email.getText().toString();
                makeout_top_name.setText(riseNamePer);
                makeout_phone.setText(mobilePer);
                makeout_email.setText(emailPer);
            }
        }
    }
}
