package com.haodaibao.fanbeiapp.module.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.widget.RouteeFlowLayout;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.MerchantListAdapterView;
import com.haodaibao.fanbeiapp.module.home.temporary.HomeTemporary;
import com.haodaibao.fanbeiapp.module.merchant.temporarydata.MerchantTemporary;
import com.haodaibao.fanbeiapp.repository.json.LableInfo;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.haodaibao.fanbeiapp.R.id.cb_scope_id;

/**
 * @author LuoYi
 */
public class HomeRefreshAdapter extends BaseRecycleViewAdapter<MerchantTemporary> {
    public static final String HOME_CATAGORY = "catagory";
    public static final String HOME_SCOPE = "scope";
    public static final String HOME_SORTWAYS = "sortways";

    public static boolean hideLocationRl = false;

    private Context mContext;
    private OnItemClickListener itemClickListener;
    private final MerchantListAdapterView merchantListAdapterView;

    private Animation operatingAnim;
    private ImageView refreshIv;
    private TextView locationTv;
    private RelativeLayout homeLocationRl;

    private String mCityCode;

    public HomeRefreshAdapter(Context context, OnItemClickListener itemClickListener) {
        super(context);
        mContext = context;
        this.itemClickListener = itemClickListener;
        merchantListAdapterView = new MerchantListAdapterView(mContext);

        operatingAnim = AnimationUtils.loadAnimation(mContext, R.anim.anim_location_home);
        LinearInterpolator lin = new LinearInterpolator();
        operatingAnim.setInterpolator(lin);
    }

    public HomeRefreshAdapter(Context context, OnItemClickListener itemClickListener, String mCityCode) {
        super(context);
        mContext = context;
        this.itemClickListener = itemClickListener;
        this.mCityCode = mCityCode;
        merchantListAdapterView = new MerchantListAdapterView(mContext);

        operatingAnim = AnimationUtils.loadAnimation(mContext, R.anim.anim_location_home);
        LinearInterpolator lin = new LinearInterpolator();
        operatingAnim.setInterpolator(lin);
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, final MerchantTemporary item) {
        if (holder instanceof MerchantListAdapterView.MerchantViewHolder) {
            MerchantListAdapterView.MerchantViewHolder hold = (MerchantListAdapterView.MerchantViewHolder) holder;
            final MerchantInfoEntiy infoEntiy = (MerchantInfoEntiy) item;
            merchantListAdapterView.setMerchantView(hold, infoEntiy);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.onItemClick(v, infoEntiy);
                    }
                }
            });
        } else if (holder instanceof HomeNearbyHolder) {
            final HomeNearbyHolder nearbyHolder = (HomeNearbyHolder) holder;
            HomeTemporary homeTemporary = (HomeTemporary) item;
            homeLocationRl = nearbyHolder.homeLocationRl;
            refreshIv = nearbyHolder.homeRefreshIv;
            locationTv = nearbyHolder.homeLocationTv;
            nearbyHolder.cbCatagoryId.setOnClickListener(new HomeNearbyClick(nearbyHolder, HOME_CATAGORY));
            nearbyHolder.cbScopeId.setOnClickListener(new HomeNearbyClick(nearbyHolder, HOME_SCOPE));
            nearbyHolder.cbSortwaysId.setOnClickListener(new HomeNearbyClick(nearbyHolder, HOME_SORTWAYS));
            nearbyHolder.cbCatagoryId.setText(homeTemporary.catagoryStr);
            nearbyHolder.cbScopeId.setText(homeTemporary.scopeStr);
            nearbyHolder.cbSortwaysId.setText(homeTemporary.sortwaysStr);
            nearbyHolder.homeLocationTv.setText(homeTemporary.locationStr);
            nearbyHolder.homeLocationRl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loactionPositioning();
                }
            });

            if (homeTemporary.lableList != null && homeTemporary.lableList.size() > 0) {
                nearbyHolder.homeLabelLl.setVisibility(View.VISIBLE);
                setRouteeView(nearbyHolder.homeLabelRfl, homeTemporary.lableList);
            } else {
                nearbyHolder.homeLabelLl.setVisibility(View.GONE);
            }
        }
    }


    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0:
                return new HomeNearbyHolder(View.inflate(mContext, R.layout.home_nearby_item, null));

            case 1:
                View view = View.inflate(mContext, R.layout.adapter_shanghusearchresult, null);
                return new MerchantListAdapterView.MerchantViewHolder(view);
            default:
                return new FooterHolder(LayoutInflater.from(mContext).inflate(R.layout.home_footer, parent, false));
        }
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return getData().get(position).getmType();
    }

    public void loactionPositioning() {
        locationTv.setText("定位中...");
        refreshIv.setAnimation(operatingAnim);
        homeLocationRl.setEnabled(false);
        if (operatingAnim != null) {
            operatingAnim.start();
        }
        if (itemClickListener != null) {
            itemClickListener.onItemClick(null, true);
        }
    }

    public void resetLocationList(boolean hideLocationRl) {
        this.hideLocationRl = hideLocationRl;
        if (homeLocationRl == null) {
            return;
        }
        if (hideLocationRl) {
            //如果是当前城市就显示定位View
            homeLocationRl.setVisibility(View.VISIBLE);
            if (operatingAnim != null && operatingAnim.isInitialized()) {
                homeLocationRl.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        operatingAnim.cancel();
                        refreshIv.clearAnimation();
                        homeLocationRl.setEnabled(true);
                    }
                }, 1000);
            }
        } else {
            homeLocationRl.setVisibility(View.GONE);
        }
        notifyDataSetChanged();
    }

    private void setRouteeView(RouteeFlowLayout itemPackageRfl, List<LableInfo> lableList) {
        itemPackageRfl.removeAllViews();
        for (final LableInfo lableInfo : lableList) {
            FrameLayout itemNewPackge = (FrameLayout) LayoutInflater.from(mContext).inflate(R.layout.item_label_checkbox, null);
            final CheckBox labelTv = itemNewPackge.findViewById(R.id.item_label_cb);
            final ImageView labelIv = itemNewPackge.findViewById(R.id.item_label_iv);
            labelTv.setText(lableInfo.getLabelname());
            labelTv.setChecked(lableInfo.isLabelSelected());
            labelIv.setVisibility(lableInfo.isLabelSelected() ? View.VISIBLE : View.INVISIBLE);
            labelTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (itemClickListener != null) {
                        lableInfo.setLabelSelected(labelTv.isChecked());
                        labelIv.setVisibility(lableInfo.isLabelSelected() ? View.VISIBLE : View.INVISIBLE);
                        itemClickListener.onItemClick(null, lableInfo);
                    }
                }
            });
            itemPackageRfl.addView(itemNewPackge);
        }
    }

    private class HomeNearbyClick implements View.OnClickListener {
        HomeNearbyHolder nearbyHolder;
        String nearbyStr;

        HomeNearbyClick(HomeNearbyHolder nearbyHolder, String nearbyStr) {
            this.nearbyHolder = nearbyHolder;
            this.nearbyStr = nearbyStr;
        }

        @Override
        public void onClick(View v) {
            nearbyHolder.cbCatagoryId.setChecked(false);
            nearbyHolder.cbScopeId.setChecked(false);
            nearbyHolder.cbSortwaysId.setChecked(false);
            if (itemClickListener != null) {
                itemClickListener.onItemClick(v, nearbyStr);
            }
        }
    }

    class HomeNearbyHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cb_catagory_id)
        CheckBox cbCatagoryId;
        @BindView(cb_scope_id)
        CheckBox cbScopeId;
        @BindView(R.id.cb_sortways_id)
        CheckBox cbSortwaysId;

        @BindView(R.id.home_location_rl)
        RelativeLayout homeLocationRl;
        @BindView(R.id.home_location_tv)
        TextView homeLocationTv;
        @BindView(R.id.home_refresh_iv)
        ImageView homeRefreshIv;

        @BindView(R.id.home_label_ll)
        LinearLayout homeLabelLl;
        @BindView(R.id.home_label_rfl)
        RouteeFlowLayout homeLabelRfl;

        HomeNearbyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class FooterHolder extends RecyclerView.ViewHolder {

        FooterHolder(View itemView) {
            super(itemView);
        }
    }
}
