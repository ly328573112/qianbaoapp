package com.haodaibao.fanbeiapp.module.map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.amap.api.maps.AMap;
import com.amap.api.maps.MapView;
import com.amap.api.services.route.BusPath;
import com.amap.api.services.route.BusRouteResult;
import com.amap.api.services.route.DrivePath;
import com.amap.api.services.route.DriveRouteResult;
import com.amap.api.services.route.WalkPath;
import com.amap.api.services.route.WalkRouteResult;
import com.amap.api.services.share.ShareSearch;
import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.StatusBarHelper;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.map.overlay.BusRouteOverlay;
import com.haodaibao.fanbeiapp.module.map.overlay.DrivingRouteOverlay;
import com.haodaibao.fanbeiapp.module.map.overlay.WalkRouteOverlay;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import java.util.List;

import butterknife.BindView;

/**
 * 开发者：LuoYi
 * Time: 2017 16:11 2017/9/15 09
 */

public class GaodeNavigationActivity extends BaseActivity implements GaodeMapContract.GaodeMapView, View.OnClickListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.map)
    MapView mapView;
    @BindView(R.id.expandableListView1)
    ExpandableListView expandableListView1;
    private String startTitle;
    private float routeDuration;
    private float routeDistance;
    private String flag;

    private AMap aMap;
    private GaodeNavigationPresenter navigationPresenter;
    private GaodeNavigationAdapter navigationAdapter;
    private Object routePath;

    public static void open(Activity activity, Object routePath, String startTitle, float routeDuration, float routeDistance, String flag) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("routePath", (Parcelable) routePath);
        bundle.putString("startTitle", startTitle);
        bundle.putFloat("routeDuration", routeDuration);
        bundle.putFloat("routeDistance", routeDistance);
        bundle.putString("flag", flag);
        ActivityJumpUtil.next(activity, GaodeNavigationActivity.class, bundle, 0);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_gaode_navigation;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(GaodeNavigationActivity.this);
        toolbarBack.setOnClickListener(this);
        toolbarTitle.setText(R.string.title_searchroute);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            Intent intent = getIntent();
            routePath = intent.getParcelableExtra("routePath");
            startTitle = intent.getStringExtra("startTitle");
            routeDuration = intent.getFloatExtra("routeDuration", 0);
            routeDistance = intent.getFloatExtra("routeDistance", 0);
            flag = intent.getStringExtra("flag");
        } else {
            routePath = savedInstanceState.getParcelable("routePath");
            startTitle = savedInstanceState.getString("startTitle");
            routeDuration = savedInstanceState.getFloat("routeDuration", 0);
            routeDistance = savedInstanceState.getFloat("routeDistance", 0);
            flag = savedInstanceState.getString("flag");
        }
        mapView.onCreate(savedInstanceState);
        navigationPresenter = new GaodeNavigationPresenter(this);
        aMap = navigationPresenter.setMap(mapView.getMap());

        List navigationMode = getNavigationMode();

        navigationAdapter = new GaodeNavigationAdapter(this);
        navigationAdapter.setNavigationData(flag, navigationMode, startTitle, routeDuration, routeDistance);
        expandableListView1.setAdapter(navigationAdapter);
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString("startTitle", startTitle);
        savedInstanceState.putFloat("routeDuration", routeDuration);
        savedInstanceState.putFloat("routeDistance", routeDistance);
        savedInstanceState.putString("flag", flag);
        mapView.onSaveInstanceState(savedInstanceState);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            default:
                break;
        }
    }

    private List getNavigationMode() {
        try {
            if (flag.equals("drive")) {// 驾车模式
                DrivePath drivePath = (DrivePath) routePath;
                DriveRouteResult driveRouteResult = (DriveRouteResult) GaodeMapContract.routeResult;// 驾车模式查询结果
                DrivingRouteOverlay drivingRouteOverlay = new DrivingRouteOverlay(this, aMap, drivePath, driveRouteResult.getStartPos(), driveRouteResult.getTargetPos(),null);
                drivingRouteOverlay.removeFromMap();
                drivingRouteOverlay.addToMap();
                drivingRouteOverlay.zoomToSpan();
                if (drivePath.getSteps() != null) {
                    return drivePath.getSteps();
                }
            } else if (flag.equals("bus")) {// 公交模式
                BusPath busPath = (BusPath) routePath;
                BusRouteResult busRouteResult = (BusRouteResult) GaodeMapContract.routeResult;// 公交模式查询结果
                BusRouteOverlay busRouteOverlay = new BusRouteOverlay(this, aMap, busPath, busRouteResult.getStartPos(), busRouteResult.getTargetPos());
                busRouteOverlay.removeFromMap();
                busRouteOverlay.addToMap();
                busRouteOverlay.zoomToSpan();
                if (busPath.getSteps() != null) {
                    return busPath.getSteps();
                }
            } else if (flag.equals("walk")) {// 步行模式
                WalkPath walkPath = (WalkPath) routePath;
                WalkRouteResult walkRouteResult = (WalkRouteResult) GaodeMapContract.routeResult;// 步行模式查询结果
                WalkRouteOverlay walkRouteOverlay = new WalkRouteOverlay(this, aMap, walkPath, walkRouteResult.getStartPos(), walkRouteResult.getTargetPos());
                walkRouteOverlay.removeFromMap();
                walkRouteOverlay.addToMap();
                walkRouteOverlay.zoomToSpan();
                if (walkPath.getSteps() != null) {
                    return walkPath.getSteps();
                }
            }
        } catch (NullPointerException e) {
            return null;
        }
        return null;
    }
}
