package com.haodaibao.fanbeiapp.module.map;

import android.widget.EditText;

import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.route.RouteSearch;
import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Global;

/**
 * 开发者：LuoYi
 * Time: 2017 14:09 2017/9/20 09
 */

public class MapRoutePresenter implements GaodeMapContract.GaodeRoutePresenter {

    private GaodeRouteActivity activity;
    private RouteSearch routeSearch;
    private LatLonPoint endPoint;
    private LatLonPoint startPoint;

    MapRoutePresenter(BaseActivity activity, RouteSearch routeSearch, LatLonPoint endPoint) {
        this.activity = (GaodeRouteActivity) activity;
        this.routeSearch = routeSearch;
        this.endPoint = endPoint;
    }

    @Override
    public void busRoute(EditText startTextView, EditText endTextView) {
        RouteSearch.FromAndTo fromAndTo = searchRoute();
        activity.showProgressDialog();
        RouteSearch.BusRouteQuery query = new RouteSearch.BusRouteQuery(fromAndTo, RouteSearch.BusDefault, Global.getMyLocation().cityName, 0);// 第一个参数表示路径规划的起点和终点，第二个参数表示公交查询模式，第三个参数表示公交查询城市区号，第四个参数表示是否计算夜班车，0表示不计算
        routeSearch.calculateBusRouteAsyn(query);// 异步路径规划公交模式查询
    }

    @Override
    public void drivingRoute(EditText startTextView, EditText endTextView) {
        RouteSearch.FromAndTo fromAndTo = searchRoute();
        activity.showProgressDialog();
        RouteSearch.DriveRouteQuery query = new RouteSearch.DriveRouteQuery(fromAndTo, RouteSearch.DrivingDefault, null, null, "");// 第一个参数表示路径规划的起点和终点，第二个参数表示驾车模式，第三个参数表示途经点，第四个参数表示避让区域，第五个参数表示避让道路
        routeSearch.calculateDriveRouteAsyn(query);// 异步路径规划驾车模式查询
    }

    @Override
    public void walkRoute(EditText startTextView, EditText endTextView) {
        RouteSearch.FromAndTo fromAndTo = searchRoute();
        activity.showProgressDialog();
        RouteSearch.WalkRouteQuery query = new RouteSearch.WalkRouteQuery(fromAndTo, RouteSearch.WalkMultipath);
        routeSearch.calculateWalkRouteAsyn(query);// 异步路径规划步行模式查询
    }

    /**
     * Route搜索
     */
    RouteSearch.FromAndTo searchRoute() {
        startPoint = new LatLonPoint(Global.getMyLocation().getLatitude(), Global.getMyLocation().getLongitude());
        return new RouteSearch.FromAndTo(startPoint, endPoint);
    }
}
