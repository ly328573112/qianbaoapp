package com.haodaibao.fanbeiapp.module.personal.message;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.haodaibao.fanbeiapp.R;

import butterknife.BindView;

public class MessageDetailActivity extends BaseActivity {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbar_back;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @BindView(R.id.tv_msg_title)
    TextView tv_msg_title;
    @BindView(R.id.tv_time)
    TextView tv_time;
    @BindView(R.id.tv_content)
    TextView tv_content;

    private String title;
    private String content;
    private String effectivetime;
    private String msg_title;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_message_detail;
    }

    @Override
    protected void setupView() {
        toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        Intent intent = getIntent();
        if (savedInstanceState == null) {
            /* 消息标题 */
            title = intent.getStringExtra("title");
            /* 消息内容标题 */
            msg_title = intent.getStringExtra("msg_title");
            /* 消息内容 */
            content = intent.getStringExtra("content");
            /* 消息生效时间 */
            effectivetime = getIntent().getStringExtra("effectivetime");
        } else {
            title = savedInstanceState.getString("title");
            msg_title = savedInstanceState.getString("msg_title");
            content = savedInstanceState.getString("content");
            effectivetime = savedInstanceState.getString("effectivetime");
        }

        toolbar_title.setText(title);
        tv_msg_title.setText(msg_title);
        /* 消息发布时间 */
        if (!TextUtils.isEmpty(effectivetime)) {
            String date1 = effectivetime.substring(0, 4)// 年
                    + "." + effectivetime.substring(4, 6)// 月
                    + "." + effectivetime.substring(6, 8);// 日
            String time1 = effectivetime.substring(8, 10)// 时
                    + ":" + effectivetime.substring(10, 12)// 分
                    + ":" + effectivetime.substring(12, 14);// 秒
            effectivetime = date1 + " " + time1;
            tv_time.setText(effectivetime);
        }

		/* 消息内容 */
        tv_content.setText(content);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString("title", title);
        savedInstanceState.putString("msg_title", msg_title);
        savedInstanceState.putString("content", content);
        savedInstanceState.putString("effectivetime", effectivetime);
        super.onSaveInstanceState(savedInstanceState);
    }
}
