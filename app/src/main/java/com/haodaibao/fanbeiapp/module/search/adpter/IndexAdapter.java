package com.haodaibao.fanbeiapp.module.search.adpter;

public interface IndexAdapter {
    Indexable getItem(int i);
}
