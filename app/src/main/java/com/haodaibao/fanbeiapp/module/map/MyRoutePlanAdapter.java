package com.haodaibao.fanbeiapp.module.map;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amap.api.services.route.BusPath;
import com.amap.api.services.route.BusStep;
import com.amap.api.services.route.DrivePath;
import com.amap.api.services.route.DriveStep;
import com.amap.api.services.route.Path;
import com.amap.api.services.route.RouteBusLineItem;
import com.amap.api.services.route.WalkPath;
import com.amap.api.services.route.WalkStep;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * date: 2017/9/28.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class MyRoutePlanAdapter extends BaseRecycleViewAdapter<Path> {
    //    private String startTitle = "";
//    private float distance;
//    private long duration;
    private String flag;

    MyRoutePlanAdapter(Context context) {
        super(context);
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, final Path item) {
        RouteHolder routeHolder = (RouteHolder) holder;
//        GaodeMapContract.routePath = item;
//        distance = item.getDistance();
//        duration = item.getDuration();
        List steps;
//        startTitle = "";
        String startTitle = "";
        if (item instanceof DrivePath) {
            flag = "drive";
            steps = ((DrivePath) item).getSteps();
            startTitle = "途径";
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < steps.size(); i++) {
                DriveStep driveStep = (DriveStep) steps.get(i);
                if (driveStep != null) {
                    if (!TextUtils.isEmpty(driveStep.getRoad())) {
                        s.append(driveStep.getRoad() + ",");
                    }
                }
            }
            if (s.length() > 0) {
                s.delete(s.length() - 1, s.length());
            }
            startTitle = startTitle + ":" + s.toString();
            routeHolder.tvWalkdis.setVisibility(View.GONE);
            routeHolder.tvName.setText(startTitle);
        } else if (item instanceof WalkPath) {
            flag = "walk";
            steps = ((WalkPath) item).getSteps();
            startTitle = "途径";
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < steps.size(); i++) {
                WalkStep driveStep = (WalkStep) steps.get(i);
                if (driveStep != null) {
                    if (!TextUtils.isEmpty(driveStep.getRoad())) {
                        s.append(driveStep.getRoad() + ",");
                    }
                }
            }
            if (s.length() > 0) {
                s.delete(s.length() - 1, s.length());
            }
            startTitle = startTitle + ":" + s.toString();
            routeHolder.tvWalkdis.setVisibility(View.GONE);
            routeHolder.tvName.setText(startTitle);
        } else if (item instanceof BusPath) {
            flag = "bus";
            steps = ((BusPath) item).getSteps();
            if (steps.size() > 0) {
                for (int i = 0; i < steps.size(); i++) {
                    BusStep busStep = (BusStep) steps.get(i);
                    RouteBusLineItem routeBusLineItem = busStep.getBusLine();
                    if (routeBusLineItem != null) {
                        String a = routeBusLineItem.getBusLineName();
                        startTitle += a + " —";
                    }

                }
                startTitle = startTitle.substring(0, startTitle.length() - 2);
            }
            routeHolder.tvWalkdis.setVisibility(View.VISIBLE);
            routeHolder.tvWalkdis.setText("步行" + CommonUtils.formatDistance("" + item.getDistance()));
            routeHolder.tvId.setText(CommonUtils.formatNum(holder.getLayoutPosition() - getHeaderLayoutCount() + 1));
            routeHolder.tvName.setText(startTitle);
        }
        routeHolder.tvDur.setText(CommonUtils.formatTime("" + item.getDuration()));
        routeHolder.tvDis.setText(CommonUtils.formatDistance("" + item.getDistance()));
        final String title = startTitle;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GaodeNavigationActivity.open((Activity) mContext, item, title, item.getDuration(), item.getDistance(), flag);
            }
        });
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new RouteHolder(LayoutInflater.from(mContext).inflate(R.layout.adapter_routeplanlist, parent, false));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class RouteHolder extends DefaultViewHolder {
        @BindView(R.id.tv_id)
        TextView tvId;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_dur)
        TextView tvDur;
        @BindView(R.id.tv_dis)
        TextView tvDis;
        @BindView(R.id.tv_walkdis)
        TextView tvWalkdis;

        RouteHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
