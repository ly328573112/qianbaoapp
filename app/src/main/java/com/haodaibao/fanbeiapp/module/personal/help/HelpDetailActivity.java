package com.haodaibao.fanbeiapp.module.personal.help;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.haodaibao.fanbeiapp.R;

import butterknife.BindView;
import butterknife.OnClick;

public class HelpDetailActivity extends BaseActivity {
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar  mToolbar;
    @BindView(R.id.tv_title_content)
    TextView mTvTitleContent;
    @BindView(R.id.tv_content)
    TextView mTvContent;
    @BindView(R.id.wv)
    WebView  mWv;
    private String mTitle;
    private String mContent;
    private String mContenttype;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_help_detail;
    }

    @Override
    protected void setupView() {
        initIntent();
    }

    private void initIntent() {
        mTitle = getIntent().getStringExtra("title");
        mContent = getIntent().getStringExtra("content");
        mContenttype = getIntent().getStringExtra("contenttype");
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        if (mContenttype.equals("02")) {
            mWv.setVisibility(View.VISIBLE);
            mTvContent.setVisibility(View.GONE);
            mTvTitleContent.setVisibility(View.GONE);
            initWebView(mContent);
        } else {
            mWv.setVisibility(View.GONE);
            mTvContent.setVisibility(View.VISIBLE);
            mTvTitleContent.setVisibility(View.VISIBLE);
            initView();
        }
    }

    private void initView() {
        mToolbarTitle.setText("详情");
        mTvTitleContent.setText(mTitle);
        mTvContent.setText(mContent);
        ViewTreeObserver observer = mTvTitleContent.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mTvTitleContent.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                if (mTvTitleContent.getLineCount() > 1) {
                    mTvTitleContent.setGravity(Gravity.LEFT);
                } else {
                    mTvTitleContent.setGravity(Gravity.CENTER);
                }
            }
        });
    }

    private void initWebView(String url) {
        // 设置缓存
        mWv.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE); // 设置
        // 缓存模式
        // 开启 DOM storage API 功能
        mWv.getSettings().setDomStorageEnabled(true);
        // 开启 database storage API 功能
        mWv.getSettings().setDatabaseEnabled(true);
        String cacheDirPath = this.getFilesDir().getAbsolutePath() + "/webViewCache";
        // 设置数据库缓存路径
        mWv.getSettings().setDatabasePath(cacheDirPath);
        // 设置 Application Caches 缓存目录
        mWv.getSettings().setAppCachePath(cacheDirPath);
        // 开启 Application Caches 功能
        mWv.getSettings().setAppCacheEnabled(true);
        // 设置监听事件
        String ua = mWv.getSettings().getUserAgentString();
        mWv.getSettings().setUserAgentString(ua + " webview");
        // 设置可以支持缩放
        mWv.getSettings().setSupportZoom(true);
        // 设置出现缩放工具
        mWv.getSettings().setBuiltInZoomControls(true);
        //扩大比例的缩放
        mWv.getSettings().setUseWideViewPort(true);
        //自适应屏幕
        mWv.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        mWv.getSettings().setLoadWithOverviewMode(true);
        mWv.getSettings().setTextSize(WebSettings.TextSize.NORMAL);
        // 初始化加载页面
        mWv.loadUrl(url);
    }

    @OnClick(R.id.toolbar_back)
    public void onViewClicked() {
        finish();
    }
}
