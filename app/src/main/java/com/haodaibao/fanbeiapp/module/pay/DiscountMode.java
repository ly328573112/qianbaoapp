package com.haodaibao.fanbeiapp.module.pay;

import android.text.TextUtils;

import com.baseandroid.utils.CountUtil;
import com.baseandroid.utils.FormatUtil;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Created by hdb on 2017/9/1.
 */

public class DiscountMode {
    public static String NONE = "0";// 无优惠
    public static String ALL = "1";// 立减N%
    public static String FULL = "2";// 满M立减N
    public static String PERFULL = "3";// 每满M立减N

    /**
     * 显示优惠信息,不包含样式
     */
    public static String setFanxianNoStyle(String refundMode, String returnBeginAmount, String returnRate) {
        String string = "";
        if (TextUtils.isEmpty(refundMode) || TextUtils.isEmpty(returnBeginAmount) || TextUtils.isEmpty(returnRate)) {
            return string;
        }

        DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
        returnBeginAmount = decimalFormat.format(new BigDecimal(returnBeginAmount));
//        returnBeginAmount = FormatUtil.formatAmount(0, returnBeginAmount);
        if (refundMode.equals(NONE)) {

        } else if (refundMode.equals(ALL)) {
            if (!TextUtils.isEmpty(returnRate)) {
                returnRate = FormatUtil.formatAmount(1, "" + (1 - Double.valueOf(returnRate)) * 10);
                if (0 < Double.parseDouble(returnRate)) {
                    string = returnRate + "折";
                }
            }


        } else if (refundMode.equals(FULL)) {
//            returnRate = FormatUtil.formatAmount(0, returnRate);
            DecimalFormat decimalFormat1 = new DecimalFormat("###################.###########");
            returnRate = decimalFormat1.format(new BigDecimal(returnRate));
            string = "满" + returnBeginAmount + "减" + returnRate + "元";
        } else if (refundMode.equals(PERFULL)) {
//            returnRate = FormatUtil.formatAmount(0, returnRate);
            DecimalFormat decimalFormat2 = new DecimalFormat("###################.###########");
            returnRate = decimalFormat2.format(new BigDecimal(returnRate));
            string = "每满" + returnBeginAmount + "减" + returnRate + "元";
        }
        return string;
    }

    public static String getDiscountData(String topamount, String returnbeginamount, String returnmode) {
        String duscount = "";
        DecimalFormat df = new DecimalFormat("0");
        Double topamountDouble = Double.parseDouble(TextUtils.isEmpty(topamount) ? "0" : topamount);
        Double returnbeginamountDouble = Double.parseDouble(TextUtils.isEmpty(returnbeginamount) ? "0" : returnbeginamount);
        if (TextUtils.equals(returnmode, ALL) || TextUtils.equals(returnmode, FULL) || TextUtils.equals(returnmode, PERFULL)) {
            if (!TextUtils.isEmpty(topamount) && topamountDouble != 0 && !TextUtils.isEmpty(returnbeginamount) && returnbeginamountDouble != 0 && topamountDouble < 99999) {
                duscount = "最低消费" + df.format(returnbeginamountDouble) + "元  " + "最高优惠" + df.format(topamountDouble) + "元";
            } else if (!TextUtils.isEmpty(returnbeginamount) && returnbeginamountDouble != 0) {
                duscount = "最低消费" + df.format(returnbeginamountDouble) + "元";
            } else if (!TextUtils.isEmpty(topamount) && topamountDouble != 0 && topamountDouble < 99999) {
                duscount = "最高优惠" + df.format(topamountDouble) + "元";
            }
        }
        return duscount;
    }

    /**
     * 获取优惠金额
     *
     * @param refundMode        立减模式
     * @param returnBeginAmount 立减起始金额
     * @param returnRate        if(refundMode = 1)立减returnRate% if(refundMode =
     *                          2)满returnBeginAmount立减returnRate if(refundMode =
     *                          3)每满returnBeginAmount立减returnRate
     * @param topamount         最高优惠金额
     * @param inReduceamount    参与优惠金额
     * @return 参与优惠金额
     */
    public static double getDiscountMoney(String refundMode, String returnBeginAmount, String returnRate, String returnBeginAmount2, String returnRate2, String topamount, double inReduceamount) {
        double doubleNumber = Double.valueOf(FormatUtil.formatAmount(2, "0"));
        double returnRateNum = Double.valueOf(FormatUtil.formatAmount(2, returnRate));
        double topamountNum = Double.valueOf(FormatUtil.formatAmount(2, topamount));
        double beginAmountNum = Double.valueOf(FormatUtil.formatAmount(2, returnBeginAmount));
        //满减会出现以下两个变量
        double returnRateNum2 = Double.valueOf(FormatUtil.formatAmount(2, returnRate2));
        double beginAmountNum2 = Double.valueOf(FormatUtil.formatAmount(2, returnBeginAmount2));
        //参与优惠金额为0
        if (inReduceamount == 0) {
            return doubleNumber;
        }
        //参与优惠金额不为0，按优惠模式计算
        if (TextUtils.isEmpty(refundMode)) {

        } else if (refundMode.equals(NONE)) {

        } else if (refundMode.equals(ALL)) {
            //满足优惠要求
            if (inReduceamount >= beginAmountNum) {
                doubleNumber = CountUtil.mul(inReduceamount, returnRateNum);
            }
        } else if (refundMode.equals(FULL)) {//大于最大值取最大，位于之间取最小
            if (beginAmountNum >= beginAmountNum2) {
                if (inReduceamount >= beginAmountNum) {
                    doubleNumber = returnRateNum;
                } else if ((inReduceamount < beginAmountNum) && (inReduceamount >= beginAmountNum2)) {
                    doubleNumber = returnRateNum2;
                }
            } else {
                if (inReduceamount >= beginAmountNum2) {
                    doubleNumber = returnRateNum2;
                } else if ((inReduceamount < beginAmountNum2) && (inReduceamount >= beginAmountNum)) {
                    doubleNumber = returnRateNum;
                }
            }
            //满足优惠要求
            if (inReduceamount >= beginAmountNum) {
                doubleNumber = returnRateNum;
            }
        } else if (refundMode.equals(PERFULL)) {
            //（输入金额 / 每满金额 = 有几个每满金额） * 优惠金额
            doubleNumber = ((int) (inReduceamount / beginAmountNum)) * returnRateNum;
        }
        //优惠金额>=最高优惠金额，取最高优惠金额
        if (topamountNum > 0 && (doubleNumber >= topamountNum)) {
            doubleNumber = topamountNum;
        }
        return doubleNumber;
    }
}
