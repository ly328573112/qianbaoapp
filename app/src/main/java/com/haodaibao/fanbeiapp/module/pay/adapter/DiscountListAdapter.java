package com.haodaibao.fanbeiapp.module.pay.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baseandroid.utils.StringUtils;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.PayInfoDetailBean;

import java.util.zip.Inflater;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Description：支付成功页面优惠明细recycleView适配器
 * <p>
 * Created by Flower.G on 2018/4/17.
 */
public class DiscountListAdapter extends BaseRecycleViewAdapter<PayInfoDetailBean.ActivitiesBean> {

    public DiscountListAdapter(Context context) {
        super(context);
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, PayInfoDetailBean.ActivitiesBean item) {
        mViewHolder mViewHolder = (mViewHolder) holder;
        if (item == null || StringUtils.isEmpty(item.getLabelName()) || StringUtils.equals("0", item.getBenefitAmount())) {
            mViewHolder.setVisibility(false);
        } else {
            mViewHolder.setVisibility(true);
            mViewHolder.discount_label.setText(item.getLabelName());
            mViewHolder.discount_amount.setText("-" + mContext.getString(R.string.rmb) + " " + (Double.parseDouble(item.getBenefitAmount()) / 100));
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_pay_succ_discount, parent, false);
        return new mViewHolder(view);
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class mViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.discount_label)
        TextView discount_label;
        @BindView(R.id.discount_amount)
        TextView discount_amount;


        public mViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setVisibility(boolean isVisible) {
            RecyclerView.LayoutParams param = (RecyclerView.LayoutParams) itemView.getLayoutParams();
            if (isVisible) {
                param.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                param.width = LinearLayout.LayoutParams.MATCH_PARENT;
                itemView.setVisibility(View.VISIBLE);
            } else {
                itemView.setVisibility(View.GONE);
                param.height = 0;
                param.width = 0;
            }
            itemView.setLayoutParams(param);
        }
    }
}
