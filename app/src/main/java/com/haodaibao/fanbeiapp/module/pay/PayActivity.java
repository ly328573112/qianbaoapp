package com.haodaibao.fanbeiapp.module.pay;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.AbsoluteSizeSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Constant;
import com.baseandroid.gaode.GaoDeMap;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.CountUtil;
import com.baseandroid.utils.FormatUtil;
import com.baseandroid.utils.StatusBarHelper;
import com.baseandroid.widget.keyview.InputNumberKeyBoard;
import com.baseandroid.widget.keyview.KeyBoard;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.discount.DiscountExplainActivity;
import com.haodaibao.fanbeiapp.module.pay.adapter.PayActivityAdapter;
import com.haodaibao.fanbeiapp.module.personal.paylist.PaylistActivity;
import com.haodaibao.fanbeiapp.repository.json.CouponInfo;
import com.haodaibao.fanbeiapp.repository.json.LableInfo;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;
import com.haodaibao.fanbeiapp.repository.json.MyCouponsResp;
import com.haodaibao.fanbeiapp.repository.json.PackageAuthBean;
import com.haodaibao.fanbeiapp.repository.json.PayInfoDetailBean;
import com.qianbao.mobilecashier.QBPaySDK;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

import static com.baseandroid.config.Constant.CHOOSE_COUPON_RESULTCODE;
import static com.baseandroid.config.Constant.FROM;
import static com.baseandroid.config.Constant.PAYSUCC_COMMENT_RESULTCODE;
import static com.haodaibao.fanbeiapp.module.pay.DiscountMode.getDiscountMoney;


public class PayActivity extends BaseActivity implements View.OnClickListener, PayContract.View {
    @BindView(R.id.et_ordersum)
    protected EditText ordersumEt;
    @BindView(R.id.et_noreducesum)
    protected EditText noreducesumEt;
    @BindView(R.id.keyboard)
    protected InputNumberKeyBoard keyboard;
    @BindView(R.id.discount_tv)
    protected TextView discountTv;//立减比例
    @BindView(R.id.rl_discount)
    protected RelativeLayout rl_discount;//立减比例
    @BindView(R.id.discount_minimum_payment_tv)
    protected TextView discountMinimumTv;//至少消费金额
    @BindView(R.id.discount_payment_money_tv)
    protected TextView discountPaymentMoneyTv;
    @BindView(R.id.pay_first_loaction)
    protected TextView pay_first_loaction;
    @BindView(R.id.tv_title)
    protected TextView tv_title;
    @BindView(R.id.iv_back)
    protected LinearLayout backLl;
    @BindView(R.id.explain_tv)
    protected TextView explainTv;

    @BindView(R.id.ll_refundnote)
    protected LinearLayout ll_refundnote;
    @BindView(R.id.tv_refundnote)
    protected TextView tv_refundnote;
    @BindView(R.id.tv_refundnote_more)
    protected TextView tv_refundnote_more;
    @BindView(R.id.iv_refundnote)
    protected ImageView iv_refundnote;

    @BindView(R.id.bt_next)
    protected TextView btNext;
    @BindView(R.id.pay_confirm_check_tv)
    protected TextView payConfirmCheckTv;
    @BindView(R.id.pay_hint_content_tv)
    protected TextView payHintContentTv;
    @BindView(R.id.pay_check_rl)
    protected RelativeLayout payCheckRl;
    @BindView(R.id.pay_sv)
    protected LinearLayout PaySv;
    @BindView(R.id.pay_view2)
    View pay_view2;
    @BindView(R.id.pay_view3)
    View pay_view3;
    @BindView(R.id.pay_activity_rv)
    RecyclerView payActivityRv;
    @BindView(R.id.pay_sl)
    ScrollView pay_sl;
    @BindView(R.id.pay_participate_rl)
    RelativeLayout payParticipateRl;
    @BindView(R.id.pay_view1)
    View payView1;

    //商户相关信息
    private String cityCode, merchantno, returnmode, returnrate, returnrate2, returnbeginamount, returnbegin2amount, topamount, merchantname, merchantaddress, orderno;
    //当前商户全部可用个红包列表
    private ArrayList<CouponInfo> couponsList = null;
    //匹配出来的红包
    private CouponInfo matchCouponInfo;

    //activity跳转传值key
    public static final String MERCHANT = "merchant";

    public static final String MERCHANT_NO = "merchantNo";

    // 消费总额
    private double ordersum = 0;
    // 不参与优惠金额
    private double noreducesum = 0;
    // 优惠金额
    private double reduceamount = 0;
    // 实付金额
    private double actualPayNum = 0;
    //红包金额
    private double couponAmount;
    //红包编号,应取Serialno
    private String couponNo;

    private MerchantInfoEntiy merchantDetailResp;
    private String from;

    //标记支付页面从哪个页面跳转过来的
    public static final String MERCHANT_DETAIL = "merchantDetail";//从商家详情页跳转过来
    public static final String OTHER = "other";//从其他页跳转过来

    private PayPresenter payPresenter;
    private String activityno;
    private List<LableInfo> lableInfoListSelect = new ArrayList<>();
    private PayActivityAdapter activityAdapter;

    public static void openForResult(Activity activity, MerchantInfoEntiy merchantDetailResp, String merchantNo, String from, boolean isFinish) {
        Intent intent = new Intent(activity, PayActivity.class);
        intent.putExtra(MERCHANT, merchantDetailResp);
        intent.putExtra(FROM, from);
        intent.putExtra(MERCHANT_NO, merchantNo);
        activity.startActivityForResult(intent, 0);
        if (isFinish) {
            activity.finish();
        }
    }

    private void payLocation() {
        new RxPermissions(PayActivity.this).request(Manifest.permission.ACCESS_FINE_LOCATION)
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull final Boolean aBoolean) {
                        if (aBoolean) {
                            GaoDeMap.getInstance().startMap("payC");
                        } else {
                            CommonUtils.goSetPremission(PayActivity.this);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        CommonUtils.goSetPremission(PayActivity.this);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_pay;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(PayActivity.this);
        payLocation();
        viewListener();
        QBPaySDK.getInstance().initSDK(this);
        payPresenter = new PayPresenter(this);
        keyboard.setListener(keyboardListener);
        ordersumEt.setFocusable(true);
        ordersumEt.setFocusableInTouchMode(true);
        ordersumEt.requestFocus();
        SpannableString ss = new SpannableString("询问服务员后输入");//定义hint的值
        AbsoluteSizeSpan ass = new AbsoluteSizeSpan(14, true);//设置字体大小 true表示单位是sp
        ss.setSpan(ass, 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ordersumEt.setHint(new SpannedString(ss));
        ordersumEt.setOnFocusChangeListener(onlinePayFirstFocusChange);
        ordersumEt.addTextChangedListener(new OnlinePayFirstTextWatcher(0));
        ordersumEt.setOnEditorActionListener(onlinePayFirstEditorAction);
        disableShowSoftInput(ordersumEt);

        noreducesumEt.setOnFocusChangeListener(onlinePayFirstFocusChange);
        noreducesumEt.addTextChangedListener(new OnlinePayFirstTextWatcher(1));
        noreducesumEt.setOnEditorActionListener(onlinePayFirstEditorAction);
        disableShowSoftInput(noreducesumEt);

        if (keyboard != null && keyboard.getVisibility() == View.VISIBLE) {
            adjustView();
        }

    }

    private void goBottom() {
        if (pay_sl != null) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    pay_sl.fullScroll(ScrollView.FOCUS_DOWN);
                }
            });
        }
    }

    //调整scrollView布局
    private void adjustView() {

        ViewTreeObserver vto = payCheckRl.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                payCheckRl.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) pay_sl.getLayoutParams();
                if (payCheckRl.getVisibility() == View.GONE) {
                    lp.setMargins(0, 0, 0, 0);
                } else {
                    lp.setMargins(0, 0, 0, payCheckRl.getHeight());
                }
                pay_sl.setLayoutParams(lp);
//                goBottom();
            }
        });
    }

    private void viewListener() {
        ordersumEt.setOnClickListener(this);
        noreducesumEt.setOnClickListener(this);
        backLl.setOnClickListener(this);
//        actualCouponTv.setOnClickListener(this);
        btNext.setOnClickListener(this);
        payConfirmCheckTv.setOnClickListener(this);
        PaySv.setOnClickListener(this);
        explainTv.setOnClickListener(this);
//        actualCouponRl.setOnClickListener(this);
        iv_refundnote.setOnClickListener(this);
    }

    private BaseRecycleViewAdapter.OnItemClickListener onItemClickListener = new BaseRecycleViewAdapter.OnItemClickListener() {

        @Override
        public <T> void onItemClick(View view, T t) {
            if (t instanceof LableInfo) {
                LableInfo lableInfo = (LableInfo) t;
                if (lableInfo.isLabelSelected()) {
                    lableInfoListSelect.add(lableInfo);
                } else {
                    for (LableInfo lableInfo1 : lableInfoListSelect) {
                        if (TextUtils.equals(lableInfo1.getActivityNo(), lableInfo.getActivityNo())) {
                            lableInfoListSelect.remove(lableInfo);
                            break;
                        }
                    }
                }
                calculation(false);
            }
        }
    };

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                finish();
                break;
            case R.id.pay_sv:
                HintCursor(false);
                break;
            case R.id.explain_tv:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                DiscountExplainActivity.start(PayActivity.this, merchantno, activityno);
                HintCursor(true);
                break;
//            case R.id.actual_coupon_rl:
//            case R.id.actual_coupon_tv:
//                if (CommonUtils.isFastDoubleClick()) {
//                    return;
//                }
//                ChooseCouponActivity.start(PayActivity.this, couponsList, ordersum - noreducesum + "", couponNo, cityCode);
//                HintCursor(true);
//                break;
            case R.id.et_ordersum:
                ordersumEt.setFocusable(true);
                ordersumEt.setFocusableInTouchMode(true);
                ordersumEt.requestFocus();
                break;
            case R.id.et_noreducesum:
                noreducesumEt.setFocusable(true);
                noreducesumEt.setFocusableInTouchMode(true);
                noreducesumEt.requestFocus();
                break;
            case R.id.pay_confirm_check_tv:
            case R.id.bt_next:
                doNext();
                break;
            case R.id.iv_refundnote:
                iv_refundnote.setVisibility(View.GONE);
                tv_refundnote.setVisibility(View.GONE);
                tv_refundnote_more.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }

    private void HintCursor(boolean delayed) {
        if (delayed) {
//            actualCouponTv.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    ordersumEt.clearFocus();
//                    ordersumEt.setFocusable(false);
//
//                    noreducesumEt.clearFocus();
//                    noreducesumEt.setFocusable(false);
//                }
//            }, 500);
        } else {
            ordersumEt.clearFocus();
            ordersumEt.setFocusable(false);

            noreducesumEt.clearFocus();
            noreducesumEt.setFocusable(false);
        }

        if (TextUtils.isEmpty(ordersumEt.getText().toString().trim())) {
            btNext.setEnabled(false);
            payConfirmCheckTv.setEnabled(false);
            payConfirmCheckTv.setBackgroundColor(getResources().getColor(R.color.red_hint));
        } else {
            btNext.setEnabled(true);
            payConfirmCheckTv.setEnabled(true);
            payConfirmCheckTv.setBackgroundColor(getResources().getColor(R.color.c_F62241));
        }
    }

    /**
     * 处理支付按钮逻辑
     */
    private void doNext() {
        if (CommonUtils.isFastDoubleClick()) {
            return;
        }
        btNext.setEnabled(false);
        btNext.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (btNext != null) {
                    btNext.setEnabled(true);
                }
            }
        }, 3000);
        payConfirmCheckTv.setEnabled(false);
        payConfirmCheckTv.setBackgroundColor(getResources().getColor(R.color.red_hint));
        payConfirmCheckTv.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (payConfirmCheckTv != null) {
                    payConfirmCheckTv.setEnabled(true);
                    payConfirmCheckTv.setBackgroundColor(getResources().getColor(R.color.c_F62241));
                }

            }
        }, 3000);

        /* 不参与优惠金额不得大于消费总额 */
        if (noreducesum >= ordersum) {
            noreducesum = Double.valueOf(FormatUtil.formatAmount(2, ordersumEt.getText().toString()));
        } else {
            noreducesum = Double.valueOf(FormatUtil.formatAmount(2, noreducesumEt.getText().toString()));
        }
        if (ordersum == 0) {
            showToast("消费总额不能为0");
            return;
        }
        payPresenter.createOrder(merchantno, ordersum + "", noreducesum + "", couponNo, couponAmount + "", null, lableInfoListSelect);
    }

    @Override
    protected void onResume() {
        super.onResume();
        QBPaySDK.getInstance().onResume();
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            Intent intent = getIntent();
            merchantDetailResp = (MerchantInfoEntiy) intent.getSerializableExtra(MERCHANT);
            from = intent.getStringExtra(FROM);
        } else {
            merchantDetailResp = (MerchantInfoEntiy) savedInstanceState.getSerializable(MERCHANT);
            from = savedInstanceState.getString(FROM);
        }
        initInfo();
        getMerchantDetail();
    }

    private void getMerchantDetail() {
        payPresenter.getMerchantInfo(merchantno, merchantDetailResp);
    }

    private void setConfirmStatus(int visibi, int visibili) {
        btNext.setVisibility(visibi);
//        payHintContentTv.setVisibility(visibi);
        payCheckRl.setVisibility(visibili);
        adjustView();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        outState.putSerializable(MERCHANT, merchantDetailResp);
        outState.putString(FROM, from);
        super.onSaveInstanceState(outState, outPersistentState);
    }

    KeyBoard.KeyBoradListener keyboardListener = new KeyBoard.KeyBoradListener() {
        @Override
        public void onKeyBoardClick(String inputStr, String resultString) {

            if (TextUtils.isEmpty(inputStr)) {
                if (ordersumEt.isFocused()) {
                    ordersumEt.setText("");
                    ordersumEt.setSelection(ordersumEt.getText().length());//将光标移至文字末尾
                } else if (noreducesumEt.isFocused()) {
                    noreducesumEt.setText("");
                    noreducesumEt.setSelection(ordersumEt.getText().length());//将光标移至文字末尾
                }
            } else {
                if (ordersumEt.isFocused()) {
                    int index = ordersumEt.getSelectionStart();
                    Editable editable = ordersumEt.getText();
                    keyboard.reset();
                    if (resultString.equals("#") && ordersumEt.getText()
                            .toString()
                            .length() > 0 && index > 0) {
                        ordersumEt.setText(ordersumEt.getText().toString().substring(0, index - 1) + ordersumEt.getText()
                                .toString().substring(index, ordersumEt.getText().toString().length()));
                        int index2 = index - 1;
                        ordersumEt.setSelection(index2);//将光标移至文字末尾
                    } else {
                        editable.insert(index, resultString);
                        int index2 = ordersumEt.getSelectionStart();
                        ordersumEt.setSelection(index2);//将光标移至文字末尾
                    }

                } else if (noreducesumEt.isFocused()) {
                    int index = noreducesumEt.getSelectionStart();
                    Editable editable = noreducesumEt.getText();
                    keyboard.reset();
                    if (resultString.equals("#") && noreducesumEt.getText().toString().length() > 0 && index > 0) {
                        noreducesumEt.setText(noreducesumEt.getText()
                                .toString()
                                .substring(0, index - 1) + noreducesumEt.getText()
                                .toString()
                                .substring(index, noreducesumEt.getText()
                                        .toString()
                                        .length()));
                        int index2 = index - 1;
                        noreducesumEt.setSelection(index2);//将光标移至文字末尾
                    } else {
                        editable.insert(index, resultString);
                        int index2 = noreducesumEt.getSelectionStart();
                        noreducesumEt.setSelection(index2);//将光标移至文字末尾
                    }
                }
            }
        }

        @Override
        public void ok(Object o) {

        }

        @Override
        public void cancel() {

        }
    };

    /**
     * 禁止Edittext弹出软件盘，光标依然正常显示。
     */
    public void disableShowSoftInput(EditText editText) {
        if (android.os.Build.VERSION.SDK_INT <= 10) {
            editText.setInputType(InputType.TYPE_NULL);
        } else {
            Class<EditText> cls = EditText.class;
            Method method;
            try {
                method = cls.getMethod("setShowSoftInputOnFocus", boolean.class);
                method.setAccessible(true);
                method.invoke(editText, false);
            } catch (Exception e) {
                // TODO: handle exception
            }

            try {
                method = cls.getMethod("setSoftInputShownOnFocus", boolean.class);
                method.setAccessible(true);
                method.invoke(editText, false);
            } catch (Exception e) {
                // TODO: handle exception
            }
        }
    }

    private View.OnFocusChangeListener onlinePayFirstFocusChange = new View.OnFocusChangeListener() {

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                // 此处为得到焦点时的处理内容
                keyboard.reset();
            } else {
                if (!TextUtils.isEmpty(ordersumEt.getText().toString()) || !TextUtils.isEmpty(noreducesumEt.getText().toString())) {
                    getCoupon();
                }
            }
            if (v.getId() == R.id.et_ordersum) {
                if (hasFocus) {
                    setConfirmStatus(View.GONE, View.VISIBLE);
                } else {
                    setConfirmStatus(View.VISIBLE, View.GONE);
                    if ((!TextUtils.isEmpty(ordersumEt.getText()
                            .toString())) && Double.parseDouble(ordersumEt.getText()
                            .toString()) == 0) {
                        showToast("消费金额必须大于0元");
                    }
                }
            } else if (v.getId() == R.id.et_noreducesum) {
                if (hasFocus) {
                    setConfirmStatus(View.GONE, View.VISIBLE);
                } else {
                    setConfirmStatus(View.VISIBLE, View.GONE);
                }
            }
        }
    };

    @Override
    public void updateCouponList(MyCouponsResp myCouponsResp) {
        couponsList = myCouponsResp.getCouponList().getItems();
    }

    @Override
    public void updateMerchantActivityList(List<LableInfo> activities) {
        if (activityAdapter != null) {
            activityAdapter = null;
        }
        if (lableInfoListSelect != null && lableInfoListSelect.size() > 0) {
            lableInfoListSelect.clear();
        }
        calculation(false);
        if (activities == null || activities.size() == 0) {
            pay_view3.setVisibility(View.GONE);
            payActivityRv.setVisibility(View.GONE);
        } else {
            setPayActivityAdapter();
            pay_view3.setVisibility(View.VISIBLE);
            payActivityRv.setVisibility(View.VISIBLE);
            activityAdapter.resetData(activities);
        }
    }

    private void setPayActivityAdapter() {
        activityAdapter = new PayActivityAdapter(PayActivity.this, onItemClickListener);
        payActivityRv.setLayoutManager(new LinearLayoutManager(PayActivity.this));
        payActivityRv.setNestedScrollingEnabled(false);
        payActivityRv.setAdapter(activityAdapter);
    }

    @Override
    public void updateMerchantInfo(MerchantInfoEntiy merchant) {
        String merchantname = merchant.getMerchantname();
        if (TextUtils.isEmpty(merchantname)) {
            //            showErrorDialog();
            return;
        }
        merchantno = merchant.getMerchantno();
        tv_title.setText(merchantname);
        pay_first_loaction.setText(merchant.getAddress());
        returnmode = merchant.getRefundmode();
        returnrate = merchant.getReturnrate();
        returnbeginamount = merchant.getReturnbeginamount();
        topamount = merchant.getTopamount();
        cityCode = merchant.getCityCode();
        activityno = merchant.getActivityno();

        initDiscountInfo(merchant);
    }

    @Override
    public void clolsePayUI() {
        tv_title.postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 1000);
    }


    @Override
    public void zhiFuing(String msg) {
        showToast(msg);
        startActivity(new Intent(this, PaylistActivity.class));
    }

    @Override
    public void zhiFuSuc(PayInfoDetailBean payInfoDetailBean, String orderno) {
        PaySuccessActivity.start(this, payInfoDetailBean, orderno, from);
    }

    @Override
    public void zhiFuFailed(String msg) {
        showToast("请重新支付");
    }

    @Override
    public void checkedUI(PackageAuthBean packageAuthBean) {

    }


    class OnlinePayFirstTextWatcher implements TextWatcher {

        int number;
        String text;

        /**
         * @param number 0:：消费总额 1：不参与优惠金额
         */
        public OnlinePayFirstTextWatcher(int number) {
            this.number = number;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            switch (number) {
                case 0:
                    break;
            }
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            text = s.toString();
            switch (number) {
                case 0:
                    btNext.setEnabled(false);
                    payConfirmCheckTv.setEnabled(false);
                    payConfirmCheckTv.setBackgroundColor(getResources().getColor(R.color.red_hint));
                    if (!text.equals(".")) {
                        if (text.contains(".")) {
                            int index = text.indexOf(".");
                            if (index + 3 < text.length()) {
                                text = text.substring(0, index + 3);
                                ordersumEt.setText(text);
                                ordersumEt.setSelection(text.length());
                            }
                        } else {// 不包含小数点只能输入到99999
                            if (text.length() > 5) {
                                text = text.substring(0, 5);
                                ordersumEt.setText(text);
                                ordersumEt.setSelection(text.length());
                            }
                        }

                        if (TextUtils.isEmpty(ordersumEt.getText().toString().trim())) {
                            ordersum = 0.00;
                            btNext.setEnabled(false);
                            payConfirmCheckTv.setEnabled(false);
                            payConfirmCheckTv.setBackgroundColor(getResources().getColor(R.color.red_hint));
                        } else {
                            ordersum = Double.valueOf(FormatUtil.formatAmount(2, ordersumEt
                                    .getText()
                                    .toString()));
                            btNext.setEnabled(true);
                            payConfirmCheckTv.setEnabled(true);
                            payConfirmCheckTv.setBackgroundColor(getResources().getColor(R.color.c_F62241));
                        }
                    } else {
                        ordersumEt.setText("");
                    }

                    //没有输入消费总额
                    if (TextUtils.isEmpty(text)) {
                        discountPaymentMoneyTv.setText("");
                    }
                    break;

                case 1:
                    if (!text.equals(".")) {
                        if (text.contains(".")) {
                            int index = text.indexOf(".");
                            if (index + 3 < text.length()) {
                                text = text.substring(0, index + 3);
                                noreducesumEt.setText(text);
                                noreducesumEt.setSelection(text.length());
                            }
                        } else {// 不包含小数点只能输入到99999
                            if (text.length() > 5) {
                                text = text.substring(0, 5);
                                noreducesumEt.setText(text);
                                noreducesumEt.setSelection(text.length());
                            }
                        }

                        if (TextUtils.isEmpty(noreducesumEt.getText().toString())) {
                            noreducesum = 0;
                        } else {
                            noreducesum = Double.valueOf(FormatUtil.formatAmount(2, noreducesumEt.getText().toString()));
                        }
                    } else {
                        noreducesumEt.setText("");
                    }
                    break;
            }

            /* 不参与优惠金额不得大于消费总额 */
            if (noreducesum >= ordersum) {
                noreducesum = Double.valueOf(FormatUtil.formatAmount(2, ordersumEt.getText().toString()));
            } else {
                noreducesum = Double.valueOf(FormatUtil.formatAmount(2, noreducesumEt.getText().toString()));
            }
            calculation(false);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }


    private TextView.OnEditorActionListener onlinePayFirstEditorAction = new TextView.OnEditorActionListener() {

        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(0, InputMethodManager.RESULT_HIDDEN);
            return false;
        }
    };

    /**
     * 设置优惠信息
     */
    private void initInfo() {
        if (merchantDetailResp == null) {
            merchantno = getIntent().getStringExtra(MERCHANT_NO);
            //            merchantno = "M20170421335977";
            return;
        }
        returnmode = merchantDetailResp.getRefundmode();
        returnbeginamount = merchantDetailResp.getReturnbeginamount();
        returnrate = merchantDetailResp.getReturnrate();
        merchantno = merchantDetailResp.getMerchantno();
        merchantname = merchantDetailResp.getMerchantname();
        returnrate2 = merchantDetailResp.getReturnrate2();
        returnbegin2amount = merchantDetailResp.getReturnbegin2amount();
        topamount = merchantDetailResp.getTopamount();
        merchantaddress = merchantDetailResp.getAddress();
        cityCode = merchantDetailResp.getCityCode();
        activityno = merchantDetailResp.getActivityno();
        if (!TextUtils.isEmpty(merchantname)) {
            tv_title.setText(merchantname);
            pay_first_loaction.setText(merchantaddress);
        }

        initDiscountInfo(merchantDetailResp);
    }

    /**
     * 计算优惠
     *
     * @param isUserChoose 用来判断自动匹配红包还是用户手选红包
     */
    private void calculation(boolean isUserChoose) {
        /* 显示计算所得优惠后金额 */
        //参与优惠金额 = 订单总额 - 不参与优惠金额
        double inReduceamount = ordersum - noreducesum;
        if (ordersumEt.getText().toString().length() == 0) {
            discountPaymentMoneyTv.setTextColor(ContextCompat.getColor(this, R.color.text_hint));
            discountPaymentMoneyTv.setText("输入金额后自动计算");
//            actualCouponTv.setTextColor(ContextCompat.getColor(this, R.color.text_hint));
//            actualCouponTv.setText("输入金额后自动匹配");
            btNext.setText("确认支付");
            payConfirmCheckTv.setText("确认支付");
            return;
        }
        reduceamount = getDiscountMoney(returnmode, returnbeginamount, returnrate, returnbegin2amount, returnrate2, topamount, inReduceamount);
        reduceamount = CountUtil.round(reduceamount + "", 2, BigDecimal.ROUND_HALF_UP);
        if (reduceamount == 0) {
            discountPaymentMoneyTv.setTextColor(ContextCompat.getColor(this, R.color.text_hint));
            discountPaymentMoneyTv.setText("不可用");
        } else {
            discountPaymentMoneyTv.setTextColor(ContextCompat.getColor(this, R.color.title_color));
            discountPaymentMoneyTv.setText("-" + FormatUtil.formatAmount(2, reduceamount + "") + "   元");
        }

        //优惠后金额 = 订单金额 - 优惠金额
        double afterReduceamount = ordersum - reduceamount;
        /* 计算红包优惠金额 */
        if (!isUserChoose) {
            matchCouponInfo = payPresenter.newmatchCoupon(inReduceamount, couponsList, cityCode);
        }
        if (matchCouponInfo != null) {
            couponAmount = Double.valueOf(FormatUtil.formatAmount(2, matchCouponInfo.getAmount()));
            couponNo = matchCouponInfo.getSerialno();
        } else {
            couponAmount = 0;
            couponNo = "";
        }

//        if (couponAmount == 0) {
//            if (isUserChoose && matchCouponInfo != null) {
//                actualCouponTv.setTextColor(ContextCompat.getColor(this, R.color.text_hint));
//                actualCouponTv.setText("请选择优惠券");
//            } else {
//                actualCouponTv.setTextColor(ContextCompat.getColor(this, R.color.text_hint));
//                actualCouponTv.setText("无可用优惠券");
//            }
//        } else {
//            actualCouponTv.setTextColor(ContextCompat.getColor(this, R.color.text_gald));
//            actualCouponTv.setText("-" + FormatUtil.formatAmount(2, "" + couponAmount) + "   元");
//        }

        /* 计算实付金额 */
        //实付金额 = 优惠后金额 - 红包金额
        actualPayNum = Double.valueOf(afterReduceamount - couponAmount);
        try {
            for (int i = 0; i < lableInfoListSelect.size(); i++) {
                double benefitAmount = Double.parseDouble(lableInfoListSelect.get(i).benefitAmount) / 100.00;
                actualPayNum = actualPayNum - benefitAmount;
            }
        } catch (Exception e) {

        }


        if (actualPayNum <= 0.0) {
            actualPayNum = 0.01;
        }
//        if (actualPayNum == 0.0) {
//            btNext.setEnabled(false);
//            payConfirmCheckTv.setEnabled(false);
//            payConfirmCheckTv.setBackgroundColor(getResources().getColor(R.color.red_hint));
//        } else {
//            btNext.setEnabled(true);
//            payConfirmCheckTv.setEnabled(true);
//            payConfirmCheckTv.setBackgroundColor(getResources().getColor(R.color.c_F62241));
//        }
        btNext.setEnabled(true);
        payConfirmCheckTv.setEnabled(true);
        payConfirmCheckTv.setBackgroundColor(getResources().getColor(R.color.c_F62241));
        btNext.setText(actualPayNum == 0.0 ? "确认支付" : FormatUtil.subStrAmount(actualPayNum) + "元  " + "确认买单");
        payConfirmCheckTv.setText(actualPayNum == 0.0 ? "确认支付" : FormatUtil.subStrAmount(actualPayNum) + "元  " + "确认买单");
    }


    /**
     * 判断不参与优惠金额不得大于消费总额
     */
    private void getCoupon() {
        if (noreducesum >= ordersum) {
            noreducesum = Double.valueOf(FormatUtil.formatAmount(2, ordersumEt.getText().toString()));
        } else {
            noreducesum = Double.valueOf(FormatUtil.formatAmount(2, noreducesumEt.getText().toString()));
        }
    }

    /**
     * 设置优惠信息
     */
    private void initDiscountInfo(final MerchantInfoEntiy merchant) {
        /* 显示优惠信息 */
        String refundInfo = DiscountMode.setFanxianNoStyle(returnmode, returnbeginamount, returnrate);
        discountTv.setText(refundInfo);
        //不优惠商户不显示此行
        if (TextUtils.isEmpty(refundInfo)) {
            rl_discount.setVisibility(View.GONE);
            pay_view2.setVisibility(View.GONE);
            payParticipateRl.setVisibility(View.GONE);
            ll_refundnote.setVisibility(View.GONE);
            payView1.setVisibility(View.GONE);
            explainTv.setVisibility(View.GONE);
        } else {
            rl_discount.setVisibility(View.VISIBLE);
            pay_view2.setVisibility(View.VISIBLE);
            payParticipateRl.setVisibility(View.VISIBLE);
//            ll_refundnote.setVisibility(View.VISIBLE);
            payView1.setVisibility(View.VISIBLE);
            explainTv.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(merchant.getRefundnote())) {
                ll_refundnote.setVisibility(View.VISIBLE);
                tv_refundnote.setText(merchant.getRefundnote());
                tv_refundnote_more.setText(merchant.getRefundnote());
                tv_refundnote.post(new Runnable() {
                    @Override
                    public void run() {
                        int maxindex = getStringIndexByMaxPix(merchant.getRefundnote(), tv_refundnote.getWidth(), tv_refundnote.getPaint());
                        if (maxindex < merchant.getRefundnote().length() - 1) {
                            iv_refundnote.setVisibility(View.VISIBLE);
                        } else {
                            iv_refundnote.setVisibility(View.GONE);
                        }
                    }
                });
            } else {
                ll_refundnote.setVisibility(View.GONE);
            }
        }

        /* 优惠信息下显示优惠限制*/
        String discountData = DiscountMode.getDiscountData(topamount, returnbeginamount, returnmode);
        if (!TextUtils.isEmpty(discountData)) {
            discountMinimumTv.setVisibility(View.VISIBLE);
            discountMinimumTv.setText(discountData);
        } else {
            discountMinimumTv.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == CHOOSE_COUPON_RESULTCODE) {
            if (data == null) {
                matchCouponInfo = null;
            } else {
                matchCouponInfo = (CouponInfo) data.getSerializableExtra("couponInfo");
            }
            calculation(true);
        }
        if (resultCode == Constant.PAYSUCC_PRESS_BACK_RESULTCODE) {
            setResult(Constant.PAYSUCC_PRESS_BACK_RESULTCODE);
            finish();
        }
        if (resultCode == PAYSUCC_COMMENT_RESULTCODE) {
            setResult(Constant.PAYSUCC_COMMENT_RESULTCODE);
            finish();
        }
    }

    private static int getStringIndexByMaxPix(String str, int maxPix, TextPaint paint) {
        if (TextUtils.isEmpty(str)) {
            return 0;
        }
        int currentIndex = 0;
        for (int i = 0; i < str.length(); i++) {
            String temp = str.substring(0, i + 1);
            float valueLength = paint.measureText(temp);
            if (valueLength > maxPix) {
                currentIndex = i - 1;
                break;
            } else if (valueLength == maxPix) {
                currentIndex = i;
                break;
            }
        }

        if (currentIndex == 0) {
            currentIndex = str.length() - 1;
        }
        return currentIndex;
    }

    //    /**
    //     * 计算用户手动选择红后优惠金额
    //     */
    //    private void initData2() {
    //        /* 显示计算所得优惠后金额 */
    //        //参与优惠金额 = 订单总额 - 不参与优惠金额
    //        double inReduceamount = ordersum - noreducesum;
    //
    //        reduceamount = DiscountMode.getDiscountMoney(returnmode, returnbeginamount, returnrate, returnbegin2amount, returnrate2, topamount, inReduceamount);
    //        reduceamount = CountUtil.round(reduceamount + "", 2, BigDecimal.ROUND_HALF_UP);
    //        if (reduceamount == 0) {
    //            discountPaymentMoneyTv.setTextColor(ContextCompat.getColor(this, R.color.text_hint));
    //            discountPaymentMoneyTv.setText("不可用");
    //        } else {
    //            discountPaymentMoneyTv.setTextColor(ContextCompat.getColor(this, R.color.shining_white_333333));
    //            discountPaymentMoneyTv.setText("-" + FormatUtil.formatAmount(2, reduceamount + "") + "   元");
    //
    //        }
    //
    //        //优惠后金额 = 订单金额 - 优惠金额
    //        double afterReduceamount = ordersum - reduceamount;
    //        if (matchCouponInfo != null) {
    //            couponAmount = Double.valueOf(FormatUtil.formatAmount(2, matchCouponInfo.getAmount()));
    //            couponNo = matchCouponInfo.getSerialno();
    //        } else {
    //            couponAmount = 0;
    //            couponNo = "";
    //        }
    //
    //        if (couponAmount == 0) {
    //            actualCouponTv.setTextColor(ContextCompat.getColor(this, R.color.font_hint));
    //            actualCouponTv.setText("请选择优惠券");
    //        } else {
    //            actualCouponTv.setTextColor(ContextCompat.getColor(this, R.color.text_gald));
    //            actualCouponTv.setText("-¥" + FormatUtil.formatAmount(2, "" + couponAmount) + "   元");
    //        }
    //
    //		/* 计算实付金额 */
    //        //实付金额 = 优惠后金额 - 红包金额
    //        actualPayNum = Double.valueOf(afterReduceamount - couponAmount);
    //        if (actualPayNum < 0.0) {
    //            actualPayNum = 0.0;
    //        }
    //        if (actualPayNum == 0.0) {
    //            btNext.setEnabled(false);
    //    payConfirmCheckTv.setEnabled(false);
    //     payConfirmCheckTv.setBackgroundColor(getResources().getColor(R.color.red_hint));
    //        } else {
    //            btNext.setEnabled(true);
    //    payConfirmCheckTv.setEnabled(true);
    //     payConfirmCheckTv.setBackgroundColor(getResources().getColor(R.color.c_F62241));
    //        }
    //        btNext.setText(actualPayNum == 0.0 ? "确认支付" : "确认买单" + FormatUtil.subStrAmount(actualPayNum) + "元");
    //   payConfirmCheckTv.setText(actualPayNum == 0.0 ? "确认支付" : "确认买单" + FormatUtil.subStrAmount(actualPayNum) + "元");
    //    }
}
