package com.haodaibao.fanbeiapp.module.maphome;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.amap.api.maps.MapView;
import com.baseandroid.base.BaseActivity;
import com.baseandroid.gaode.GaoDeMap;
import com.baseandroid.jpush.JPushBizutils;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.runpermission.PermissionManger;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.event.LocationResultEvent;
import com.haodaibao.fanbeiapp.event.MerchantEvent;
import com.haodaibao.fanbeiapp.event.PersonalFragmentEvent;
import com.haodaibao.fanbeiapp.module.Consent;
import com.haodaibao.fanbeiapp.module.home.VersionUpdateView;
import com.haodaibao.fanbeiapp.module.maphome.cluster.ClusterItem;
import com.haodaibao.fanbeiapp.module.search.SearchActivity;
import com.haodaibao.fanbeiapp.module.search.SearchDeliciousActivity;
import com.haodaibao.fanbeiapp.repository.json.MerchantDetailResp;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;
import com.tencent.stat.StatService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * date: 2017/11/2.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class MaphomeActivity extends BaseActivity implements MaphomeContract.MaphomeView {
    @BindView(R.id.home_map)
    protected MapView home_map;
    private MaphomePresenter presenter;
    @BindView(R.id.map_dingwei)
    ImageView map_dingwei;
    @BindView(R.id.map_icon_noopen)
    ImageView map_icon_noopen;
    @BindView(R.id.iv_map_back)
    ImageView iv_map_back;
    @BindView(R.id.tv_map_search)
    TextView tv_map_search;

    private String itemMerchantNo;
    private MapMerchantWindowView merchantWindowView;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_maphome;
    }

    @Override
    protected void setupView() {
        EventBus.getDefault().register(this);

        GaoDeMap.getInstance().startMap();
        presenter = new MaphomePresenter(this);
        merchantWindowView = new MapMerchantWindowView(this, this);
        merchantWindowView.initBlurring(presenter.locationSuc && Consent.currentLocation);
        if (Consent.versionUpdateType) {
            Consent.versionUpdateType = false;
            new VersionUpdateView(this).onCheckUpdate();
        }
    }

    @OnClick({R.id.map_dingwei, R.id.merchant_ly, R.id.tv_map_search, R.id.iv_map_back})
    public void onclick(View view) {
        switch (view.getId()) {
            case R.id.map_dingwei:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                PermissionManger.getInstance().startLocation(MaphomeActivity.this);
                StatService.trackCustomKVEvent(MaphomeActivity.this, "qbsh_home_map_location", null);
                break;
            case R.id.tv_map_search:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                Intent intent = new Intent(this, SearchActivity.class);
                startActivity(intent);
                break;
            case R.id.iv_map_back:
                finish();
                break;
            default:
                break;
        }
    }


    @Override
    protected void setupData(Bundle savedInstanceState) {
        home_map.onCreate(savedInstanceState);
        presenter.getOpenCitys();
        JPushBizutils.initJPush();
    }

    @Override
    public MapView getMapView() {
        return home_map;
    }

    @Override
    public void onMarkerClick(final ClusterItem clusterItem, List<MerchantInfoEntiy> list) {
        int position = 0;
        if (list.size() == 0) {
            return;
        }
        for (int i = 0; i < list.size(); i++) {
            if (TextUtils.equals(list.get(i).getMerchantno(), clusterItem.getMarkerInfo().merchantno)) {
                position = i;
                break;
            }
        }
        merchantWindowView.initHomeLoopView(position, list);
        StatService.trackCustomKVEvent(MaphomeActivity.this, "qbsh_home_map_marker", null);
    }


    @Override
    public void updateMerchantInfo(MerchantDetailResp merchantDetailResp) {
        if (merchantDetailResp == null || merchantDetailResp.getPriedList() == null) {
            return;
        }
        merchantWindowView.updateWindowUI(merchantDetailResp);
    }

    @Override
    protected void onDestroy() {
        presenter.deactivate();
        presenter.bindDestory();
        home_map.onDestroy();
        EventBus.getDefault().unregister(this);
        presenter.onDestory();
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //在activity执行onResume时执行mMapView.onResume ()，重新绘制加载地图
        home_map.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //在activity执行onPause时执行mMapView.onPause ()，暂停地图的绘制
        home_map.onPause();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MerchantEvent event) {
        presenter.getMerchantInfo(itemMerchantNo);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(LocationResultEvent event) {
        presenter.onEvent(event);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PersonalFragmentEvent event) {
        JPushBizutils.initJPush();
    }


    public void setNoopen(int isvisble) {
        map_icon_noopen.setVisibility(isvisble);
    }


    @Override
    public void setLocatoEnable(boolean isEnable) {
    }

    @Override
    public void setVisitMerchant(String merchantNo) {
        this.itemMerchantNo = merchantNo;
        presenter.getMerchantInfo(itemMerchantNo);
    }

}
