package com.haodaibao.fanbeiapp.module.maphome;

import com.amap.api.maps.MapView;
import com.baseandroid.base.IView;
import com.haodaibao.fanbeiapp.module.maphome.cluster.ClusterItem;
import com.haodaibao.fanbeiapp.repository.json.MerchantDetailResp;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;

import java.util.List;

/**
 * date: 2017/11/2.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public interface MaphomeContract {
    interface Presenter {
        void getMerchanList(Double longitude, Double latitude, String pageSize, String citycode, String tagno);

    }

    interface MaphomeView extends IView {
        MapView getMapView();

        void onMarkerClick(ClusterItem clusterItem, List<MerchantInfoEntiy> merchantInfoEntiyList);




        void updateMerchantInfo(MerchantDetailResp merchantDetailResp);

        void setLocatoEnable(boolean isEnable);

        void setVisitMerchant(String merchantNo);
    }

    interface HomeWindowView {

        void initBlurring(boolean isCurrent);

        void initHomeLoopView(int position, List<MerchantInfoEntiy> merchantList);

        void updateWindowUI(MerchantDetailResp merchantDetailResp);
    }
}
