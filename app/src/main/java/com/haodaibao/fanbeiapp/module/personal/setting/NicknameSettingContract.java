package com.haodaibao.fanbeiapp.module.personal.setting;

/**
 * Created by Routee on 2017/9/20.
 * description: ${cusor}
 */

public class NicknameSettingContract {
    interface View {
        void updateNicknameSuccess();
    }

    interface Presenter {
        void updateNickname(String nickname);
    }
}
