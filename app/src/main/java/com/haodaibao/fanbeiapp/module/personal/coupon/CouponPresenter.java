package com.haodaibao.fanbeiapp.module.personal.coupon;

import com.baseandroid.base.BaseFragment;
import com.baseandroid.utils.RxUtils;
import com.haodaibao.fanbeiapp.repository.LifeRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.CouponBean;
import com.haodaibao.fanbeiapp.repository.json.Data;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Routee on 2017/9/12.
 * description: ${cusor}
 */

public class CouponPresenter implements CouponContract.Presenter {

    private CouponContract.View mView;

    public CouponPresenter(CouponContract.View view) {
        mView = view;
    }

    @Override
    public void getCoupons(String statue, String page) {
        Map<String, String> params = new HashMap<>();
        params.put("status", statue);
        params.put("page", page);
        params.put("limit", "10");
        LifeRepository.getInstance().getCouponsList(params).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<CouponBean>>applySchedulersLifeCycle((BaseFragment) mView))
                .subscribe(new RxObserver<Data<CouponBean>>(){
                    @Override
                    public void onNext(@NonNull Data<CouponBean> data) {
                        if (checkJsonCode(data, false)) {
                            mView.setUpCouponData(data.getResult());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        mView.onUpdateFailed();
                    }
                });
    }
}
