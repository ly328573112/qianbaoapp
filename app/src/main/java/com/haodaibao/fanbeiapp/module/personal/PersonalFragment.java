package com.haodaibao.fanbeiapp.module.personal;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.base.BaseFragment;
import com.baseandroid.config.BizUtils;
import com.baseandroid.config.Global;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.FormatUtil;
import com.baseandroid.widget.customimageview.GlideCircleTransform;
import com.bumptech.glide.Glide;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.event.MessageEvent;
import com.haodaibao.fanbeiapp.event.PersonalFragmentEvent;
import com.haodaibao.fanbeiapp.module.invoice.MyInvoiceActivity;
import com.haodaibao.fanbeiapp.module.login.LoginActivity;
import com.haodaibao.fanbeiapp.module.personal.balance.BalanceActivity;
import com.haodaibao.fanbeiapp.module.personal.collections.CollectionActivity;
import com.haodaibao.fanbeiapp.module.personal.coupon.CouponListActivity;
import com.haodaibao.fanbeiapp.module.personal.message.MessageListActivity;
import com.haodaibao.fanbeiapp.module.personal.paylist.PaylistActivity;
import com.haodaibao.fanbeiapp.module.personal.setting.SettingActivity;
import com.haodaibao.fanbeiapp.module.personal.setting.UserinfoSettingActivity;
import com.haodaibao.fanbeiapp.repository.json.BalanceBean;
import com.haodaibao.fanbeiapp.repository.json.BhhInfoBean;
import com.haodaibao.fanbeiapp.repository.json.BhhStatueBean;
import com.haodaibao.fanbeiapp.repository.json.CouponBean;
import com.haodaibao.fanbeiapp.repository.json.UserDate;
import com.qianbao.shiningwhitelibrary.BHHManager;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;

import butterknife.BindView;

/**
 * Created by Routee on 2017/8/8
 * description: 首页展示的PersonalFragment
 */

public class PersonalFragment extends BaseFragment implements PersonalFragmentContract.View, View.OnClickListener {

    @BindView(R.id.menu_head_user_iv)
    ImageView menuHeadUserIv;
    @BindView(R.id.menu_name_tv)
    TextView menuNameTv;
    @BindView(R.id.menu_login_tv)
    TextView menuLoginTv;
    @BindView(R.id.menu_tel_tv)
    TextView menuTelTv;
    @BindView(R.id.menu_user_personal_ll)
    LinearLayout menuUserPersonalLl;
    @BindView(R.id.menu_message_rl)
    LinearLayout menuMessageIv;
    @BindView(R.id.balance_content_tv)
    TextView balanceContentTv;
    @BindView(R.id.menu_balance_ll)
    RelativeLayout menuBalanceLl;
    @BindView(R.id.menu_bhh_ll)
    RelativeLayout menuBhhLl;
    @BindView(R.id.coupon_content_tv)
    TextView couponContentTv;
    @BindView(R.id.menu_coupon_ll)
    RelativeLayout menuCouponLl;
    @BindView(R.id.menu_record_ll)
    RelativeLayout menuRecordLl;
    @BindView(R.id.menu_collection_ll)
    RelativeLayout menuCollectionLl;
    @BindView(R.id.menu_message_icon_iv)
    ImageView menuMessageIconIv;
    @BindView(R.id.invoice_ll)
    RelativeLayout invoice_ll;
    @BindView(R.id.view_line_two)
    View viewLineTwo;
    @BindView(R.id.menu_setting_rl)
    LinearLayout menuSettingRl;

    @BindView(R.id.bhh_yue_hint_tv)
    TextView bhhYueHintTv;
    @BindView(R.id.bhh_yue_tv)
    TextView bhhYueTv;
    @BindView(R.id.bhh_content_ll)
    LinearLayout bhhContentLl;
    @BindView(R.id.bhh_content_tv)
    TextView bhhContentTv;
    @BindView(R.id.bhh_hint_copy_tv1)
    TextView bhhHintCopyTv1;
    @BindView(R.id.bhh_hint_copy_tv)
    TextView bhhHintCopyTv;

    private String mBalance = "";        //余额
    private PersonalFragmentPresenter mPresenter;
    private String imageUrl;
//    private boolean isRefresh;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_personal;
    }

    @Override
    protected void setupView() {
        menuBalanceLl.setOnClickListener(this);
        menuCouponLl.setOnClickListener(this);
        menuBhhLl.setOnClickListener(this);
        menuRecordLl.setOnClickListener(this);
        menuCollectionLl.setOnClickListener(this);
        menuMessageIv.setOnClickListener(this);
        menuUserPersonalLl.setOnClickListener(this);
        menuHeadUserIv.setOnClickListener(this);
        invoice_ll.setOnClickListener(this);
        menuSettingRl.setOnClickListener(this);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (isRefresh) {
//            isRefresh = false;
        if (!TextUtils.isEmpty(Global.getUserInfo().bhhAcessToken)) {
            if (mPresenter != null) {
                mPresenter.getMyBhhInfo(Global.getUserInfo().bhhAcessToken);
            }
        }

//        }
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        requestNetData();
    }

    private void requestNetData() {
        if (mPresenter == null) {
            mPresenter = new PersonalFragmentPresenter(this, this);
        }
//        mPresenter.getBhhStatue();
        if (!TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
            UserDate bean = new UserDate();
            bean.setUser(Global.getUserInfo());
            setupUserInfoData(bean);
            mPresenter.getBalance();
            mPresenter.getCouponsCount();
            mPresenter.getUnReadMsgsNum();
        } else {
            menuLoginTv.setVisibility(View.VISIBLE);
            menuNameTv.setVisibility(View.GONE);
            menuTelTv.setVisibility(View.GONE);
            couponContentTv.setVisibility(View.INVISIBLE);
            menuBalanceLl.setVisibility(View.GONE);
            viewLineTwo.setVisibility(View.GONE);

            Glide.with(Global.getContext())
                    .load(R.drawable.icon_default_head_info)
                    .error(R.drawable.icon_default_head_info)
                    .transform(new GlideCircleTransform(mContext))
                    .into(menuHeadUserIv);
            bhhContentTv.setText("想花就花");
            setBhhShowText1(false);
            menuMessageIconIv.setImageResource(R.drawable.mine_msg_normal_icon);
        }
    }

    @Override
    public void onClick(View view) {
        if (CommonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.menu_head_user_iv:
            case R.id.menu_user_personal_ll:
                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                    ActivityJumpUtil.next(getActivity(), LoginActivity.class);
                } else {
                    ActivityJumpUtil.next(getActivity(), UserinfoSettingActivity.class);
                }
                break;
            //消息列表
            case R.id.menu_message_rl:
                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                    ActivityJumpUtil.next(getActivity(), LoginActivity.class);
                } else {
                    ActivityJumpUtil.next(getActivity(), MessageListActivity.class);
                }
                break;
            //余额
            case R.id.menu_balance_ll:
                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                    ActivityJumpUtil.next(getActivity(), LoginActivity.class);
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString("balance", mBalance);
                    bundle.putBoolean("isPersonal", true);
                    ActivityJumpUtil.next(getActivity(), BalanceActivity.class, bundle, 0);
                }
                break;
            //白花花
            case R.id.menu_bhh_ll:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    intent.putExtra("Bhh", "Bhh");
                    startActivity(intent);
                } else {
                    if (TextUtils.isEmpty(Global.getUserInfo().bhhAcessToken)) {
                        mPresenter.getUserInfo();
                    } else {
//                        isRefresh = true;
                        BHHManager.openViewForRelease(getActivity(), Global.getUserInfo().bhhAcessToken, Global.getUserInfo().getUserno(), false);
                    }
                }
                break;
            case R.id.menu_setting_rl:
                ActivityJumpUtil.next(getActivity(), SettingActivity.class);
                break;
            //生活优惠券
            case R.id.menu_coupon_ll:
                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                    ActivityJumpUtil.next(getActivity(), LoginActivity.class);
                } else {
                    ActivityJumpUtil.next(getActivity(), CouponListActivity.class);
                }
                break;
            //买单记录
            case R.id.menu_record_ll:
                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                    ActivityJumpUtil.next(getActivity(), LoginActivity.class);
                } else {
                    ActivityJumpUtil.next(getActivity(), PaylistActivity.class);
                }
                break;
            //收藏的店
            case R.id.menu_collection_ll:
                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                    ActivityJumpUtil.next(getActivity(), LoginActivity.class);
                } else {
                    ActivityJumpUtil.next(getActivity(), CollectionActivity.class);
                }
                break;
            //发票
            case R.id.invoice_ll:
                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                    ActivityJumpUtil.next(getActivity(), LoginActivity.class);
                } else {
                    MyInvoiceActivity.start((BaseActivity) getActivity());
                }
                break;
            default:
                break;
        }
    }


    //获取白花花状态成功
    @Override
    public void setupBhhStatueData(BhhStatueBean bean) {
        String show = bean.getSHOWBHH();
        if (show.equals("1")) {
            menuBhhLl.setVisibility(View.VISIBLE);
        } else {
            menuBhhLl.setVisibility(View.GONE);
        }
    }

    //获取白花花状态失败
    @Override
    public void getBhhStatueFailed() {
        menuBhhLl.setVisibility(View.GONE);
    }

    //设置用户信息
    @Override
    public void setupUserInfoData(UserDate bean) {
        String imgHeadUrl = bean.getUser().getFace();
        imageUrl = CommonUtils.getImageUrl(imgHeadUrl);
        menuLoginTv.setVisibility(View.GONE);
        Glide.with(Global.getContext()).load(imageUrl).asBitmap()
                .error(R.drawable.icon_default_head_info).centerCrop()
                .placeholder(R.drawable.icon_default_head_info)
                .transform(new GlideCircleTransform(mContext))
                .into(menuHeadUserIv);
        if (TextUtils.isEmpty(bean.getUser().getAlias())) {
            menuNameTv.setText("生活达人 来个昵称吧~");
        } else {
            menuNameTv.setText(bean.getUser().getAlias());
        }
        menuNameTv.setVisibility(View.VISIBLE);
        menuTelTv.setText(BizUtils.hintPwd(bean.getUser().getMobile()));
        menuTelTv.setVisibility(View.VISIBLE);
        Global.setUserInfo(bean.getUser());
        mPresenter.getMyBhhInfo(Global.getUserInfo().bhhAcessToken);
    }

    //设置余额
    @Override
    public void setupBalanceData(BalanceBean bean) {
        if (bean == null) {
            menuBalanceLl.setVisibility(View.GONE);
            viewLineTwo.setVisibility(View.GONE);
            return;
        }
        mBalance = bean.getBalanceAvailable();
        if (Float.parseFloat(mBalance) <= 0.f) {
            menuBalanceLl.setVisibility(View.GONE);
            viewLineTwo.setVisibility(View.GONE);
        } else {
            menuBalanceLl.setVisibility(View.VISIBLE);
            viewLineTwo.setVisibility(View.VISIBLE);
            balanceContentTv.setVisibility(View.VISIBLE);
            balanceContentTv.setText(FormatUtil.formatAmount(2, mBalance));
        }
    }

    //设置生活优惠券
    @Override
    public void setupCouponCountData(CouponBean bean) {
        int size = 0;
        try {
            size = bean.getCouponList().getItems().size();
        } catch (Exception e) {
            e.printStackTrace();
        }
        couponContentTv.setText(size + "张");
        if (size == 0) {
            couponContentTv.setVisibility(View.INVISIBLE);
        } else {
            couponContentTv.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setUnReadMsgView(String unReadNumber) {
        if (TextUtils.isEmpty(unReadNumber) || TextUtils.equals(unReadNumber, "0")) {
            menuMessageIconIv.setImageResource(R.drawable.mine_msg_normal_icon);
        } else {
            menuMessageIconIv.setImageResource(R.drawable.mine_msg_new_icon);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void updateBhhInfo(BhhInfoBean bhhInfoBean) {
        try {
            String baiHuaHua;
            if (TextUtils.equals(bhhInfoBean.openBhh, "1")) {
                double available = Double.parseDouble(bhhInfoBean.availableLimit) / 100.00;
                BigDecimal bd = new BigDecimal(available);
                bd = bd.setScale(2, BigDecimal.ROUND_HALF_DOWN);
                bhhYueTv.setText(bd + "");
                setBhhShowText();
            } else {
                if (TextUtils.isEmpty(Global.getUserInfo().bhhAcessToken)) {
                    bhhContentTv.setText("想花就花");

                } else {
                    baiHuaHua = "想花就花";
                    bhhContentTv.setText(baiHuaHua);
                }
                setBhhShowText1(true);
            }
        } catch (Exception e) {
            if (TextUtils.isEmpty(Global.getUserInfo().bhhAcessToken)) {
                bhhContentTv.setText("想花就花");
                setBhhShowText1(true);
            }
        }
    }

    private void setBhhShowText() {
        bhhYueHintTv.setVisibility(View.VISIBLE);
        bhhYueTv.setVisibility(View.VISIBLE);
        bhhContentLl.setVisibility(View.INVISIBLE);
        bhhHintCopyTv1.setVisibility(View.INVISIBLE);
        bhhHintCopyTv.setVisibility(View.INVISIBLE);
    }

    private void setBhhShowText1(boolean showCopy1) {
        bhhYueHintTv.setVisibility(View.INVISIBLE);
        bhhYueTv.setVisibility(View.INVISIBLE);
        bhhContentLl.setVisibility(View.VISIBLE);
        if (showCopy1) {
            bhhHintCopyTv1.setVisibility(View.INVISIBLE);
            bhhHintCopyTv.setVisibility(View.VISIBLE);
        } else {
            bhhHintCopyTv1.setVisibility(View.VISIBLE);
            bhhHintCopyTv.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void getBhhInfoFailed() {
        if (TextUtils.isEmpty(Global.getUserInfo().bhhAcessToken)) {
            bhhContentTv.setText("想花就花");
            setBhhShowText1(true);
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PersonalFragmentEvent event) {
        if (event.isRefreshUserInfo()) {
            mPresenter.getUserInfo();
            mPresenter.getBalance();
            mPresenter.getUnReadMsgsNum();
        }
        if (event.isLogOut()) {
            requestNetData();
        }
        if (event.refreshCouponCount) {
            mPresenter.getCouponsCount();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent event) {
        mPresenter.getUnReadMsgsNum();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
