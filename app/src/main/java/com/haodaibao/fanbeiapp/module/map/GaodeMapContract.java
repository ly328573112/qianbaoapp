package com.haodaibao.fanbeiapp.module.map;

import android.widget.EditText;

import com.amap.api.maps.AMap;
import com.amap.api.services.route.RouteResult;

import java.util.List;

/**
 * 开发者：LuoYi
 * Time: 2017 13:51 2017/9/13 09
 */

public class GaodeMapContract {

    interface Presenter {

        AMap initMapView(AMap aMap);

        List<MapInfo> checkAppInstall(String latitude, String longitude, String merchantaddress, String merchantname);

        void onDestroyMap();
    }

    interface GaodeMapView {

    }

    interface GaodeRoutePresenter {

        void busRoute(EditText startTextView, EditText endTextView);

        void drivingRoute(EditText startTextView, EditText endTextView);

        void walkRoute(EditText startTextView, EditText endTextView);
    }

    interface GaodeRouteView {

        void showProgressDialog();

        void dissmissProgressDialog();

    }

    interface SelectMapAppView {

        void selectMapApp(List<MapInfo> list);

    }

    interface NavigationPresenter {

        AMap setMap(AMap aMap);

    }

//    public static Object routePath;
    public static RouteResult routeResult;
}
