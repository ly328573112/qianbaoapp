package com.haodaibao.fanbeiapp.module.shangquantab;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;

import com.baseandroid.base.BasePopupWindow;
import com.baseandroid.config.Global;
import com.baseandroid.gaode.GaoDeMap;
import com.baseandroid.utils.CommonUtils;
import com.haodaibao.fanbeiapp.module.bizutils.CategoryAsc;
import com.haodaibao.fanbeiapp.module.bizutils.CategoryFenlei;
import com.haodaibao.fanbeiapp.module.home.HomeContract;
import com.haodaibao.fanbeiapp.repository.json.NearbyRequest;
import com.haodaibao.fanbeiapp.repository.json.ShanghufenleiInfo;
import com.tbruyelle.rxpermissions2.RxPermissions;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * 开发者：LuoYi
 * Time: 2017 17:26 2017/10/19 10
 */

public class NearbyTabView implements NearbyContract.NearbyView {

    private Context context;
    private HomeContract.View mView;

    private BasePopupWindow mCatagoryPopupWindow;

    private NearbyRequest nearbyRequest;
    private ViewShanghuFenlei viewShanghuFenlei;
    private ViewShangquan viewShangquan;
    private ViewShanghuPaixu viewShanghuPaixu;

    public NearbyTabView(Context context, HomeContract.View view) {
        this.context = context;
        this.mView = view;
        nearbyRequest = new NearbyRequest();
    }

    @Override
    public void onNearbyLocation(final String locationType) {
        new RxPermissions((Activity) context).request(Manifest.permission.ACCESS_FINE_LOCATION)
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull final Boolean aBoolean) {
                        if (aBoolean) {
                            GaoDeMap.getInstance().startMap(locationType);
                        } else {
                            mView.onStopLocation();
                            CommonUtils.goSetPremission(context);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        mView.onStopLocation();
                        CommonUtils.goSetPremission(context);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void flushPop() {
        viewShanghuFenlei = null;
        viewShangquan = null;
        viewShanghuPaixu = null;
    }

    public void disMiss() {
        try {
            if (mCatagoryPopupWindow.isShowing()) {
                mCatagoryPopupWindow.disMissWithAlpha(mCatagoryPopupWindow);
            }
        } catch (Exception e) {

        }

    }

    @Override
    public void onCatagoryView(final CheckBox cbCatagoryId, View view) {
        Log.e("hpo", "onCatagoryView: ");
        if (Global.getCategorys().size() <= 0) {
            return;
        }
        if (viewShanghuFenlei == null) {
            viewShanghuFenlei = new ViewShanghuFenlei(context);
            viewShanghuFenlei.init(context, false);
            viewShanghuFenlei.settBlockPosition(0);
            viewShanghuFenlei.setOnSelectListener(shanghuFenleiSelectListener);
        }
        mCatagoryPopupWindow = BasePopupWindow.matchParentPopupWindowOnce((Activity) context, view, viewShanghuFenlei, false, new BasePopupWindow.OnOutSideTouchListner() {
            @Override
            public void onOutSideTouch() {
                cbCatagoryId.setChecked(false);
            }
        });
        if (cbCatagoryId.isChecked()) {
            if (!mCatagoryPopupWindow.isShowing()) {
                mCatagoryPopupWindow.showMatchParentPopupDialogWithAlpha(mCatagoryPopupWindow);
            }
        }
    }

    @Override
    public void onScopeView(final CheckBox cbScopeId, View view) {
        if (Global.getShangquans().size() <= 0) {
            return;
        }
        if (viewShangquan == null) {
            viewShangquan = new ViewShangquan(context, Global.getShangquans());
            viewShangquan.setOnSelectListener(shangquanSelectListener);
        }
        mCatagoryPopupWindow = BasePopupWindow.matchParentPopupWindowOnce((Activity) context, view, viewShangquan, false, new BasePopupWindow.OnOutSideTouchListner() {
            @Override
            public void onOutSideTouch() {
                cbScopeId.setChecked(false);
            }
        });
        if (cbScopeId.isChecked()) {
            if (!mCatagoryPopupWindow.isShowing()) {
                mCatagoryPopupWindow.showMatchParentPopupDialogWithAlpha(mCatagoryPopupWindow);
            }
        }
    }

    @Override
    public void onSortwaysView(final CheckBox cbSortwaysId, View view) {
        if (viewShanghuPaixu == null) {
            viewShanghuPaixu = new ViewShanghuPaixu(context);
            viewShanghuPaixu.changeCity();
            viewShanghuPaixu.setOnSelectListener(paixuSelectListener);
        }
        mCatagoryPopupWindow = BasePopupWindow.matchParentPopupWindowOnce((Activity) context, view, viewShanghuPaixu, false, new BasePopupWindow.OnOutSideTouchListner() {
            @Override
            public void onOutSideTouch() {
                cbSortwaysId.setChecked(false);
            }
        });
        if (cbSortwaysId.isChecked()) {
            if (!mCatagoryPopupWindow.isShowing()) {
                mCatagoryPopupWindow.showMatchParentPopupDialogWithAlpha(mCatagoryPopupWindow);
            }
        }

    }

    @Override
    public void onReductionTabSelect() {
        if (mCatagoryPopupWindow != null) {

            mCatagoryPopupWindow.dismiss();
            mCatagoryPopupWindow = null;
        }
    }


    /**
     * 商户分类item点击选择
     */
    private ViewShanghuFenlei.OnSelectListener shanghuFenleiSelectListener = new ViewShanghuFenlei.OnSelectListener() {

        @Override
        public void getValue(ShanghufenleiInfo info) {
            nearbyRequest.setNearbyType(0);
            String showText = info.getName();
            String parentNo = info.getParentno();
            String categoryNo = info.getCategoryno();
            /* 点击二级分类中的全部且一级分类不选全部，查询一级中的父分类信息 */
            if (showText.equals("全部类型") && parentNo.equals("ROOT")) {
                nearbyRequest.setCategoryno("");
                nearbyRequest.setKeyword("");
            }
            /* 点击二级分类中的全部且一级分类选择全部，查询全部分类信息 */
            else if (showText.equals("全部类型") && !parentNo.equals("ROOT")) {
                nearbyRequest.setCategoryno(parentNo);
                nearbyRequest.setKeyword("");
            } else {
                nearbyRequest.setCategoryno(categoryNo);
                nearbyRequest.setKeyword("");
            }

            if (showText.equals("全部类型") && !parentNo.equals("")) {
                showText = CategoryFenlei.getCategoryParentName(info.getParentno());
            }
            nearbyRequest.setShowText(showText);
            mView.onNearbySelect(nearbyRequest);
            mCatagoryPopupWindow.dismiss();
        }
    };

    /**
     * 商圈item点击选择
     */
    private ViewShangquan.OnSelectListener shangquanSelectListener = new ViewShangquan.OnSelectListener() {

        @Override
        public void getValue(String showText, String districtcode, String areacode) {
            nearbyRequest.setNearbyType(1);
            nearbyRequest.setDistrictcode(districtcode);
            nearbyRequest.setAreacode(areacode);
            nearbyRequest.setShowText1(showText);
            mView.onNearbySelect(nearbyRequest);
            mCatagoryPopupWindow.dismiss();
        }
    };

    /**
     * 排序item点击选择
     */
    private ViewShanghuPaixu.OnSelectListener paixuSelectListener = new ViewShanghuPaixu.OnSelectListener() {

        @Override
        public void getValue(String distance, String showText) {
            nearbyRequest.setNearbyType(2);
            nearbyRequest.setSort(CategoryAsc.getCategoryAscFromName(showText));
            nearbyRequest.setShowText2(showText);
            mView.onNearbySelect(nearbyRequest);
            mCatagoryPopupWindow.dismiss();
        }
    };

}
