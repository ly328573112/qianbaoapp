//package com.haodaibao.fanbeiapp.module.home;
//
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.graphics.drawable.Drawable;
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.design.widget.AppBarLayout;
//import android.support.design.widget.CollapsingToolbarLayout;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.text.TextUtils;
//import android.transition.Transition;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.WindowManager;
//import android.widget.CheckBox;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.baseandroid.base.BaseFragment;
//import com.baseandroid.config.Global;
//import com.baseandroid.utils.CommonUtils;
//import com.baseandroid.utils.LogUtil;
//import com.baseandroid.utils.ToastUtils;
//import com.baseandroid.utils.UiUtils;
//import com.baseandroid.widget.CustomBanner;
//import com.baseandroid.widget.MarqueeView;
//import com.baseandroid.widget.animatorprovider.FadeAlphaAnimatorProvider;
//import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
//import com.baseandroid.widget.recycleadpter.CustomPtrHeader;
//import com.bumptech.glide.Glide;
//import com.bumptech.glide.request.animation.GlideAnimation;
//import com.bumptech.glide.request.target.SimpleTarget;
//import com.github.nukc.stateview.StateView;
//import com.google.zxing.integration.android.IntentIntegrator;
//import com.google.zxing.integration.android.IntentResult;
//import com.haodaibao.fanbeiapp.R;
//import com.haodaibao.fanbeiapp.event.CitySelectChangEvent;
//import com.haodaibao.fanbeiapp.event.LocationSwitchEvent;
//import com.haodaibao.fanbeiapp.event.OpencitysRequestEvent;
//import com.haodaibao.fanbeiapp.event.ShanghufenleiRequestEvent;
//import com.haodaibao.fanbeiapp.event.ShangquanRequestEvent;
//import com.haodaibao.fanbeiapp.module.Consent;
//import com.haodaibao.fanbeiapp.module.home.temporary.HomeTemporary;
//import com.haodaibao.fanbeiapp.module.login.LoginActivity;
//import com.haodaibao.fanbeiapp.module.maphome.MaphomeActivity;
//import com.haodaibao.fanbeiapp.module.merchant.MerchantDetailActivity;
//import com.haodaibao.fanbeiapp.module.merchant.temporarydata.MerchantTemporary;
//import com.haodaibao.fanbeiapp.module.pay.PayActivity;
//import com.haodaibao.fanbeiapp.module.search.CityListActivity;
//import com.haodaibao.fanbeiapp.module.search.SearchDeliciousActivity;
//import com.haodaibao.fanbeiapp.module.shangquantab.NearbyTabView;
//import com.haodaibao.fanbeiapp.module.yaohe.YaoheActivity;
//import com.haodaibao.fanbeiapp.module.zxing.ZXingScanActivity;
//import com.haodaibao.fanbeiapp.repository.json.AdInfo;
//import com.haodaibao.fanbeiapp.repository.json.LableInfo;
//import com.haodaibao.fanbeiapp.repository.json.MarqueeBean;
//import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;
//import com.haodaibao.fanbeiapp.repository.json.MerchantInfoResp;
//import com.haodaibao.fanbeiapp.repository.json.MyLocation;
//import com.haodaibao.fanbeiapp.repository.json.NearbyRequest;
//import com.haodaibao.fanbeiapp.repository.json.PackageAllBean;
//import com.haodaibao.fanbeiapp.repository.json.YaoheListBean;
//import com.haodaibao.fanbeiapp.webview.WebviewActivity;
//import com.jayfeng.lesscode.core.DisplayLess;
//import com.qianbao.shiningwhitelibrary.BHHManager;
//import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;
//import com.tencent.stat.StatService;
//import com.youth.banner.listener.OnBannerListener;
//
//import org.greenrobot.eventbus.EventBus;
//import org.greenrobot.eventbus.Subscribe;
//import org.greenrobot.eventbus.ThreadMode;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import butterknife.BindView;
//import in.srain.cube.views.ptr.PtrClassicFrameLayout;
//import in.srain.cube.views.ptr.PtrDefaultHandler;
//import in.srain.cube.views.ptr.PtrFrameLayout;
//
//import static com.baseandroid.base.BaseWebviewActivity.WEBVIEW_TITLE;
//import static com.baseandroid.base.BaseWebviewActivity.WEBVIEW_URL;
//import static com.baseandroid.config.Global.getSelectCity;
//import static com.haodaibao.fanbeiapp.module.home.HomeRefreshAdapter.HOME_CATAGORY;
//import static com.haodaibao.fanbeiapp.module.home.HomeRefreshAdapter.HOME_SCOPE;
//import static com.haodaibao.fanbeiapp.module.home.HomeRefreshAdapter.HOME_SORTWAYS;
//import static com.haodaibao.fanbeiapp.module.pay.PayActivity.OTHER;
//
///**
// * @author LuoYi -------not used-----
// */
//@Deprecated
//public class HomeFragment extends BaseFragment implements BaseRecycleViewAdapter.OnItemClickListener, BaseRecycleViewAdapter.OnLoadMoreListener, HomeContract.View, View.OnClickListener {
//
//    private static final String AD1 = "20150828100171";// 1号广告位，分类
//    public static final String AD2 = "20170527115979";// 2号广告位，滑动viewpager广告栏//20170527115979
//    private static final String AD3 = "20160809105197";// 3号广告位，商户广告位
//
//    public final static int PAGE_SIZE = 20;
//    public final static int PAGE_PER_SIZE = 5;
//
//    @BindView(R.id.search_rl)
//    RelativeLayout searchRl;
//    @BindView(R.id.bt_left)
//    TextView btLeft;
//    @BindView(R.id.home_scan_tv)
//    TextView homeScanTv;
//    @BindView(R.id.home_pay_record_tv)
//    TextView homePayRecordTv;
//    @BindView(R.id.rcv_list_id)
//    RecyclerView listIdRcv;
//    @BindView(R.id.ptr_fresh_id)
//    PtrClassicFrameLayout freshIdPtr;
//    StateView stateView;
//    @BindView(R.id.test_fram)
//    FrameLayout testFram;
//    @BindView(R.id.home_appbar)
//    AppBarLayout homeAppbar;
//    @BindView(R.id.home_coll_layout)
//    CollapsingToolbarLayout homeCollLayout;
//
//    @BindView(R.id.rl_title_open)
//    RelativeLayout titleOpenRl;
//    @BindView(R.id.rl_title_close)
//    RelativeLayout titleCloseRl;
//    @BindView(R.id.home_scan_rl)
//    RelativeLayout homeScanRl;
//    @BindView(R.id.home_pay_record_rl)
//    RelativeLayout homePayRecordRl;
//    @BindView(R.id.home_search_rl)
//    RelativeLayout homeSearchRl;
//    @BindView(R.id.ll_nearby_select)
//    LinearLayout nearbySelectLl;
//    @BindView(R.id.cb_catagory_id)
//    CheckBox cbCatagoryId;
//    @BindView(R.id.cb_scope_id)
//    CheckBox cbScopeId;
//    @BindView(R.id.saoyisao)
//    ImageView saoyisao;
//    @BindView(R.id.title_ly)
//    RelativeLayout titleLy;
//    @BindView(R.id.maidan)
//    ImageView maidan;
//    @BindView(R.id.cb_sortways_id)
//    CheckBox cbSortwaysId;
//    @BindView(R.id.icon_search)
//    ImageView iconSearch;
//    @BindView(R.id.home_map_rl)
//    RelativeLayout homeMapRl;
//
//    int mCurentPage = 1;
//    String headMerchant = "";
//    private int mVerticalOffset;
//    private boolean move = false;
//    private int screenHight;
//    private HomeRefreshAdapter mHomeRefreshAdapter;
//    private HomePresenter mHomePresenter;
//    private LinearLayoutManager linearLayoutManager;
//    private NearbyTabView homeNearbyView;
//    private NearbyRequest nearbyR;
//    private HomeTemporary homeTemporary;
//
//    private Transition mTransition;
//
//    private LinearLayout mYaoHeNearbyLayout;
//    private LinearLayout mBannerLayout;
//
//    @Override
//    protected int getLayoutId() {
//        return R.layout.home_fragment_layout;
//    }
//
//    @Override
//    protected void setupView() {
//        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
//
//        screenHight = wm.getDefaultDisplay().getHeight();
//        EventBus.getDefault().register(this);
//        mHomePresenter = new HomePresenter(this);
//        homeNearbyView = new NearbyTabView(getActivity(), this);
//        nearbyR = new NearbyRequest();
//
//        homeTemporary = new HomeTemporary();
//        setNavicationProject();
//
//        nearbySelectLl.setVisibility(View.INVISIBLE);
//
//        CustomPtrHeader customPtrHeader = new CustomPtrHeader(getActivity());
//        freshIdPtr.setHeaderView(customPtrHeader);
//        freshIdPtr.addPtrUIHandler(customPtrHeader);
//        freshIdPtr.disableWhenHorizontalMove(true);
//        freshIdPtr.setPtrHandler(ptrDefaultHandler);
//
//        linearLayoutManager = new LinearLayoutManager(getActivity());
//        listIdRcv.setLayoutManager(linearLayoutManager);
//        mHomeRefreshAdapter = new HomeRefreshAdapter(getActivity(), this);
//        mHomeRefreshAdapter.addData(homeTemporary);
//        mHomeRefreshAdapter.setOnLoadMoreListener(this, listIdRcv);
//        listIdRcv.setAdapter(mHomeRefreshAdapter);
//
//        mYaoHeNearbyLayout = (LinearLayout) LayoutInflater.from(getActivity())
//                .inflate(R.layout.yaohe_layout, null);
//        mBannerLayout = (LinearLayout) LayoutInflater.from(getActivity())
//                .inflate(R.layout.banner_layout, null);
//
//        initStateView();
//        initListener();
//
//    }
//
//    private void initStateView() {
//        stateView = StateView.inject(testFram);
//        //stateView.showLoading();
//        stateView.showContent();
//        stateView.setAnimatorProvider(new FadeAlphaAnimatorProvider());
//        stateView.setOnRetryClickListener(new StateView.OnRetryClickListener() {
//            @Override
//            public void onRetryClick() {
//                mHomePresenter.getOpenCitys();
//            }
//        });
//    }
//
//    private void initListener() {
//        searchRl.setOnClickListener(this);
//        btLeft.setOnClickListener(this);
//        homeScanTv.setOnClickListener(this);
//        homePayRecordTv.setOnClickListener(this);
//        homeScanRl.setOnClickListener(this);
//        homePayRecordRl.setOnClickListener(this);
//        homeSearchRl.setOnClickListener(this);
//        homeMapRl.setOnClickListener(this);
//
//        cbCatagoryId.setOnClickListener(this);
//        cbScopeId.setOnClickListener(this);
//        cbSortwaysId.setOnClickListener(this);
//
//        homeAppbar.addOnOffsetChangedListener(offsetChangedListener);
//        listIdRcv.addOnScrollListener(onScrollListener);
//    }
//
//    PtrDefaultHandler ptrDefaultHandler = new PtrDefaultHandler() {
//        @Override
//        public void onRefreshBegin(final PtrFrameLayout frame) {
//            mCurentPage = 1;
//            headMerchant = "";
//            // all request form network
////            refreshHomeData();
//        }
//
//        @Override
//        public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
//            return mVerticalOffset >= 0 && PtrDefaultHandler.checkContentCanBePulledDown(frame, content, header);
//        }
//    };
//
//    AppBarLayout.OnOffsetChangedListener offsetChangedListener = new AppBarLayout.OnOffsetChangedListener() {
//        @Override
//        public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
//            mVerticalOffset = verticalOffset;
//            int topPercent = Math.min(-mVerticalOffset * 255 / (homeAppbar.getHeight() - titleLy
//                    .getHeight()), 255);
//            int topPercent_bg = Math.min(-mVerticalOffset * 255 * 5 / (homeAppbar.getHeight() - titleLy
//                    .getHeight()), 255);
//            LogUtil.e("onOffsetChanged: " + topPercent + "====" + verticalOffset + "====" + homeAppbar
//                    .getHeight());
//            titleOpenRl.setVisibility(topPercent_bg >= 255 ? View.GONE : View.VISIBLE);
//            titleCloseRl.setVisibility(topPercent > 0 ? View.VISIBLE : View.GONE);
//            titleCloseRl.getBackground().setAlpha(topPercent_bg);
//            saoyisao.setImageAlpha(topPercent_bg < 255 ? 0 : topPercent);
//            maidan.setImageAlpha(topPercent_bg < 255 ? 0 : topPercent);
//            iconSearch.setImageAlpha(topPercent_bg < 255 ? 0 : topPercent);
//        }
//    };
//
//
//    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
//
//        @Override
//        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//            super.onScrolled(recyclerView, dx, dy);
//            LogUtil.e("onScrolled:recyclerView==== " + recyclerView.getHeight());
//            if (0 != linearLayoutManager.findFirstVisibleItemPosition()) {
//                nearbySelectLl.setVisibility(View.VISIBLE);
//            } else {
//                nearbySelectLl.setVisibility(View.INVISIBLE);
//            }
//
//            if (move) {
//                if (nearbySelectLl.getId() == R.id.ll_nearby_select) {
//                    move = false;
//                    int n = linearLayoutManager.findFirstVisibleItemPosition();
//                    if (0 <= n && n < listIdRcv.getChildCount()) {
//                        int top = listIdRcv.getChildAt(n).getTop();
//                        listIdRcv.scrollBy(0, top);
//                    }
//                }
//            }
//        }
//
//        @Override
//        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//            super.onScrollStateChanged(recyclerView, newState);
//            //解决华为手机滑动时，图片不收缩问题，替换原来的在xml文件中设置behavior方法
//            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
//                int visiblePosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
//                if (visiblePosition == 0) {
//                    homeAppbar.setExpanded(true, true);
//                }
//            }
//
//        }
//    };
//
//
//    @Override
//    protected void setupData(Bundle savedInstanceState) {
//        mHomePresenter.getOpenCitys();
//    }
//
//    @Override
//    public void onLoadMore() {
//        getMerchantList();
//    }
//
//    private void setupYaoHeNearbyLayout(List<MarqueeBean> marqueeBeanList) {
//        LinearLayout yaoheLy = (LinearLayout) mYaoHeNearbyLayout.findViewById(R.id.yaohe_ly);
//        MarqueeView marqueeview = (MarqueeView) mYaoHeNearbyLayout.findViewById(R.id.marqueeview);
//
//        if (marqueeBeanList.size() == 0) {
//            yaoheLy.setVisibility(View.GONE);
//        } else {
//            yaoheLy.setVisibility(View.VISIBLE);
//            marqueeview.startWithList(marqueeBeanList);
//        }
//
//        marqueeview.setOnItemClickListener(new MarqueeView.OnItemClickListener() {
//            @Override
//            public void onItemClick(String position, View view) {
//                YaoheActivity.open(getActivity(), position);
//                StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_cry", null);
//            }
//        });
//    }
//
//    @Override
//    public void onYaoheListInfo(List<YaoheListBean.YaoheBean> yaoheBeanList) {
//        List<MarqueeBean> marqueeBeanList = new ArrayList<>();
//        for (int i = 0; i < yaoheBeanList.size(); i++) {
//            MarqueeBean marqueeBean = new MarqueeBean();
//            marqueeBean.imageUrl = yaoheBeanList.get(i).getMerImg();
//            marqueeBean.titleText = yaoheBeanList.get(i).getMerchantName();
//            marqueeBean.contentText = yaoheBeanList.get(i).getTitle();
//            marqueeBean.messageNo = yaoheBeanList.get(i).getMessageNo();
//            marqueeBeanList.add(marqueeBean);
//        }
//
//        setupYaoHeNearbyLayout(marqueeBeanList);
//    }
//
//    @Override
//    public void onAdListInfo(final List<AdInfo> adInfoList) {
//        final CustomBanner bannerId = mBannerLayout.findViewById(R.id.banner_id);
//        bannerId.setPageTransformer(false, new CustomBanner.ScaleTransformer());
//
//        bannerId.setImageLoader(new GlideImageLoader());
//        List<String> images = new ArrayList<>();
//        for (AdInfo adInfo : adInfoList) {
//            images.add(CommonUtils.getImageUrl(adInfo.getImageurl()));
//        }
////        for (AdInfo adInfo : adInfoList) {
////            images.add(CommonUtils.getImageUrl(adInfo.getImageurl()));
////        }
////        for (AdInfo adInfo : adInfoList) {
////            images.add(CommonUtils.getImageUrl(adInfo.getImageurl()));
////        }
//        bannerId.setImages(images);
//        bannerId.setOnBannerListener(new OnBannerListener() {
//            @Override
//            public void OnBannerClick(int position) {
//                AdInfo adInfo = adInfoList.get(position);
//                String linkType = adInfo.getLinktype();
//                String title = adInfo.getContentname();
//                String content = adInfo.getContent();
//                if (TextUtils.isEmpty(content)) {
//                    return;
//                }
//                String urlResult = content.substring(content.lastIndexOf("/") + 1);
//                urlResult = urlResult.replace(".html", "");
//                if (TextUtils.equals(urlResult, "bhhsdk")) {
//                    if (!TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
//                        if (TextUtils.isEmpty(Global.getUserInfo().bhhAcessToken)) {
//                        } else {
//                            BHHManager.openViewForRelease(getActivity(), Global.getUserInfo().bhhAcessToken);
//                        }
//                    } else {
//                        Intent intent = new Intent(getContext(), LoginActivity.class);
//                        intent.putExtra("Bhh", "Bhh");
//                        startActivity(intent);
////                        startActivity(new Intent(getContext(), LoginActivity.class));
//                    }
//                } else {
//                    if (linkType.equals("04")) {
//                        Intent intent = new Intent(getActivity(), WebviewActivity.class);
//                        intent.putExtra(WEBVIEW_URL, content);
//                        intent.putExtra(WEBVIEW_TITLE, title);
//                        startActivity(intent);
//                    }
//                }
//                StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_banner", null);
//            }
//        });
//        bannerId.setOffscreenPageLimit(10);
////        bannerId.setOnPageChangeListener();
//        Glide.with(getActivity())
//                .load(images.get(0))
//                .asBitmap()
//                .into(new SimpleTarget<Bitmap>() {
//                    @Override
//                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
//                        if (!resource.isRecycled()) {
//                            bannerId.stopAutoPlay();
//                            if (bannerId.getHeight() <= 0) {
//                                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) bannerId
//                                        .getLayoutParams();
//                                layoutParams.height = DisplayLess.$width(getActivity()) * resource
//                                        .getHeight() / resource.getWidth();
//                            }
//                            mHomeRefreshAdapter.removeAllHeaderView();
//                            mHomeRefreshAdapter.addHeaderView(mBannerLayout);
//                            mHomeRefreshAdapter.addHeaderView(mYaoHeNearbyLayout);
//                            mHomeRefreshAdapter.notifyDataSetChanged();
//                            getMerchantList();
//                            bannerId.start();
//                        }
//                    }
//
//                    @Override
//                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
//                        super.onLoadFailed(e, errorDrawable);
//                        mHomeRefreshAdapter.resetData(new ArrayList<MerchantTemporary>());
//                        mHomeRefreshAdapter.removeAllHeaderView();
//                        mHomeRefreshAdapter.addHeaderView(mYaoHeNearbyLayout);
//                        mHomeRefreshAdapter.notifyDataSetChanged();
//                        getMerchantList();
//                    }
//                });
//    }
//
//    @Override
//    public void onAdListInfo2(List<AdInfo> adInfoList) {
//
//    }
//
//    @Override
//    public void onShanghuListInfo(MerchantInfoResp merchantInfoResp) {
//        if (mCurentPage == 1 && TextUtils.isEmpty(headMerchant)) {
//            ArrayList<MerchantTemporary> arrayList = new ArrayList<>();
//            arrayList.add(homeTemporary);
//            arrayList.addAll(merchantInfoResp.getMerchantList());
//
//            if (merchantInfoResp.getMerchantList().size() < PAGE_PER_SIZE) {
//                for (int i = 0; i < 1 + (screenHight - UiUtils.dp2px(getContext(), 174) - (UiUtils
//                        .dp2px(getContext(), 100) * merchantInfoResp.getMerchantList()
//                        .size())) / UiUtils.dp2px(getContext(), 100); i++) {
//                    arrayList.add(new MerchantTemporary() {
//                        @Override
//                        public int getmType() {
//                            return 2;
//                        }
//                    });
//                }
//                mHomeRefreshAdapter.resetData(arrayList);
//                freshIdPtr.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        mHomeRefreshAdapter.loadMoreEnd(true);
//                    }
//                }, 100);
//
//            } else {
//                mHomeRefreshAdapter.resetData(arrayList);
//            }
//
//            freshIdPtr.refreshComplete();
//        } else {
//            mHomeRefreshAdapter.addData(merchantInfoResp.getMerchantList());
//            if (merchantInfoResp.getMerchantList().size() < PAGE_PER_SIZE) {
//                mHomeRefreshAdapter.loadMoreEnd(true);
//                return;
//            } else if (merchantInfoResp.getMerchantList().size() < PAGE_SIZE) {
//                mHomeRefreshAdapter.setLoadEndContent("没有更多店铺了~");
//                mHomeRefreshAdapter.loadMoreEnd(false);
//                return;
//            }
//            mHomeRefreshAdapter.loadMoreComplete();
//        }
//        mCurentPage = Integer.parseInt(merchantInfoResp.getPage());
//        headMerchant = merchantInfoResp.getHeadMerchant();
//    }
//
//    @Override
//    public void onShanghuListInfoError() {
//        if (mCurentPage == 1 && TextUtils.isEmpty(headMerchant)) {
//            ArrayList<MerchantTemporary> arrayList = new ArrayList<>();
//            arrayList.add(new HomeTemporary());
//            mHomeRefreshAdapter.resetData(arrayList);
//            freshIdPtr.refreshComplete();
//        } else {
//            listIdRcv.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    if (!getActivity().isFinishing()) {
//                        mHomeRefreshAdapter.loadMoreFail();
//                    }
//                }
//            }, 200);
//        }
//    }
//
//    @Override
//    public void onHomeInfoRefresh() {
//        if (mCurentPage == 1 && TextUtils.isEmpty(headMerchant)) {
//            freshIdPtr.refreshComplete();
//        }
//        mHomeRefreshAdapter.notifyDataSetChanged();
//    }
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.home_search_rl:
//                Intent intent = new Intent(getActivity(), MaphomeActivity.class);
//                startActivity(intent);
//                break;
//            case R.id.search_rl:
//                startActivity(new Intent(getActivity(), SearchDeliciousActivity.class));
//                StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_search", null);
//                break;
//
//            case R.id.bt_left:
//                startActivity(new Intent(getActivity(), CityListActivity.class));
//                StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_citylist", null);
//                break;
//
//            case R.id.home_map_rl:
////            case R.id.home_map_rl1:
//                startActivity(new Intent(getActivity(), MaphomeActivity.class));
//                break;
//
//            case R.id.home_scan_rl:
//            case R.id.home_scan_tv:
//                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
//                    startActivity(new Intent(getContext(), LoginActivity.class));
//                } else {
//                    // ZXing Android Embedded支持Fragment
//                    IntentIntegrator intentIntegrator = IntentIntegrator.forSupportFragment(this);
//                    // ZXing Android Embedded库提供的扫码Activity默认是横屏的,需要自定义
//                    intentIntegrator.setCaptureActivity(ZXingScanActivity.class);
//                    //该方法用于设置“被扫描的二维码图片”可以保存在本地,intent返回
//                    //intentIntegrator.setBarcodeImageEnabled(false);
//                    //该方法传0将会使用后置摄像头，传1将会使用前置摄像头
//                    //intentIntegrator.setCameraId(0);
//                    //如果设置可以使用QR_CODE_TYPES和ALL_CODE_TYPES
//                    intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
//                    //功能是用来调整扫描界面方向的
//                    intentIntegrator.setOrientationLocked(false);
//                    //设置扫描界面的提示信息
//                    //intentIntegrator.setPrompt("这里是二维码扫描界面");
//                    intentIntegrator.initiateScan();
//                }
//                StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_scan", null);
//                break;
//
//            case R.id.home_pay_record_rl:
//            case R.id.home_pay_record_tv:
//                if (!TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
//                    if (TextUtils.isEmpty(Global.getUserInfo().bhhAcessToken)) {
//                    } else {
//                        BHHManager.openViewForRelease(getActivity(), Global.getUserInfo().bhhAcessToken);
//                    }
////                    startActivity(new Intent(getContext(), PaylistActivity.class));
//                } else {
////                    Intent intent = new Intent(getContext(), LoginActivity.class);
////                    intent.putExtra("Bhh", "Bhh");
////                    startActivity(intent);
//                }
//                StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_payrecord", null);
//                break;
//
//            case R.id.cb_catagory_id:
//                cbCatagoryId.setChecked(true);
//                cbScopeId.setChecked(false);
//                cbSortwaysId.setChecked(false);
//                moveToPosition(1);
//                homeNearbyView.onCatagoryView(cbCatagoryId);
//                StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_all", null);
//                break;
//
//            case R.id.cb_scope_id:
//                cbCatagoryId.setChecked(false);
//                cbScopeId.setChecked(true);
//                cbSortwaysId.setChecked(false);
//                moveToPosition(1);
//                homeNearbyView.onScopeView(cbScopeId);
//                StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_nearby", null);
//                break;
//
//            case R.id.cb_sortways_id:
//                cbCatagoryId.setChecked(false);
//                cbScopeId.setChecked(false);
//                cbSortwaysId.setChecked(true);
//                moveToPosition(1);
//                homeNearbyView.onSortwaysView(cbSortwaysId);
//                StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_intelligence", null);
//                break;
//
//            default:
//                break;
//        }
//    }
//
//    @Override
//    public <T> void onItemClick(View view, T t) {
//        if (t == null) {
//            return;
//        }
//        if (t instanceof MerchantInfoEntiy) {
//            MerchantInfoEntiy infoEntiy = (MerchantInfoEntiy) t;
//            Bundle bundle = new Bundle();
//            bundle.putString("merchantno", infoEntiy.getMerchantno());
//            ActivityJumpUtil.next(getActivity(), MerchantDetailActivity.class, bundle, 0);
//            StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_item", null);
//        } else if (t instanceof String) {
//            String homeNearby = (String) t;
//            switch (homeNearby) {
//                case HOME_CATAGORY:
//                default:
//                    cbCatagoryId.setChecked(true);
//                    cbScopeId.setChecked(false);
//                    cbSortwaysId.setChecked(false);
//                    homeAppbar.setExpanded(false);
//                    cbCatagoryId.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            homeNearbyView.onCatagoryView(cbCatagoryId);
//                        }
//                    }, 350);
//                    StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_all", null);
//                    break;
//                case HOME_SCOPE:
//                    cbCatagoryId.setChecked(false);
//                    cbScopeId.setChecked(true);
//                    cbSortwaysId.setChecked(false);
//                    homeAppbar.setExpanded(false);
//                    cbScopeId.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            homeNearbyView.onScopeView(cbScopeId);
//                        }
//                    }, 350);
//                    StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_nearby", null);
//                    break;
//                case HOME_SORTWAYS:
//                    cbCatagoryId.setChecked(false);
//                    cbScopeId.setChecked(false);
//                    cbSortwaysId.setChecked(true);
//                    homeAppbar.setExpanded(false);
//                    cbSortwaysId.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            homeNearbyView.onSortwaysView(cbSortwaysId);
//                        }
//                    }, 350);
//                    StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_intelligence", null);
//                    break;
//            }
//
//            moveToPosition(1);
//        } else if (t instanceof Boolean) {
//            homeNearbyView.onNearbyLocation("homeC");
//            StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_location", null);
//        }
//    }
//
//    private void moveToPosition(int n) {
//
//        int firstItem = linearLayoutManager.findFirstVisibleItemPosition();
//        int lastItem = linearLayoutManager.findLastVisibleItemPosition();
//
//        if (firstItem > 1) {
//            return;
//        }
//
//        if (n <= firstItem) {
//            listIdRcv.scrollToPosition(n);
//        } else if (n <= lastItem) {
//            int top = listIdRcv.getChildAt(n - firstItem).getTop();
//            listIdRcv.scrollBy(0, top);
//        } else {
//            listIdRcv.scrollToPosition(n);
//            move = true;
//        }
//
//    }
//
//    @Override
//    public void updateCityShanghuList() {
//
//        if (TextUtils.isEmpty(getSelectCity())) {
//            return;
//        }
//        if (Global.getOpenCitysResp()
//                .getSites() != null && Global.getMyLocation().cityName != null && Global.getOpenCitysResp()
//                .getSites()
//                .contains(Global.getMyLocation().cityName)) {
//            startActivity(new Intent(getActivity(), CityListActivity.class));
//        }
//        btLeft.setText(getSelectCity());
//        mCurentPage = 1;
//        headMerchant = "";
//
////        requestHomeData();
//    }
//
//    @Override
//    public void onRetry() {
//        stateView.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                stateView.showRetry();
//            }
//        }, 200);
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//    }
//
//    @Override
//    public void onContent() {
//        stateView.showContent();
//    }
//
//    @Override
//    public void onNearbySelect(NearbyRequest nearbyRequest) {
//        mCurentPage = 1;
//        headMerchant = "";
//        if (nearbyRequest == null) {
//            return;
//        }
//        nearbyR = nearbyRequest;
//        switch (nearbyRequest.getNearbyType()) {
//            case 0:
//            default:
//                cbCatagoryId.setChecked(false);
//                cbCatagoryId.setText(nearbyRequest.getShowText());
//                homeTemporary.catagoryStr = nearbyRequest.getShowText();
//                break;
//            case 1:
//                cbScopeId.setChecked(false);
//                cbScopeId.setText(nearbyRequest.getShowText1());
//                homeTemporary.scopeStr = nearbyRequest.getShowText1();
//                break;
//            case 2:
//                cbSortwaysId.setChecked(false);
//                cbSortwaysId.setText(nearbyRequest.getShowText2());
//                homeTemporary.sortwaysStr = nearbyRequest.getShowText2();
//                break;
//        }
//        mHomeRefreshAdapter.notifyItemRangeChanged(1, 1, "Neraby");
//        getMerchantList();
//    }
//
//    @Override
//    public void onStopLocation() {
//        mHomeRefreshAdapter.resetLocationList(true);
//    }
//
//    @Override
//    public void onShangquan() {
//
//    }
//
//    @Override
//    public void onComboDetail(MerchantInfoEntiy merchantInfoEntiy, PackageAllBean packageAllBean) {
//
//    }
//
//    @Override
//    public void onLebelList(List<LableInfo> lableInfoList) {
//
//    }
//
//    private void getMerchantList() {
////        Global.getSelectCitySiteType();
////        mHomePresenter.getShanghuListInfo(false, nearbyR.getKeyword(),//
////                nearbyR.getCategoryno(),//
////                nearbyR.getDistrictcode(),//
////                nearbyR.getAreacode(),//
////                nearbyR.getSort(),//
////                nearbyR.getRefundmode(),//
////                nearbyR.getIsProduct(),//
////                nearbyR.getIsVoucher(),//
////                Global.getSelectCityCode(),//
////                mCurentPage, PAGE_SIZE, headMerchant, null);
//////        mHomePresenter.getShanghuListInfo(Global.getSelectCitySiteType(), Global.getSelectCityCode(), nearbyR
//////                        .getKeyword(),//
//    }
//
////    private void requestHomeData() {
////        mHomePresenter.getClassification(Global.getSelectCitySiteType(), Global.getSelectCityCode(), Global
////                .getSelectCityCode(), "");
////        mHomePresenter.getShangquan(Global.getSelectCitySiteType(), Global.getSelectCityCode(), Global
////                .getSelectCityCode());
////        mHomePresenter.requestHomeInfo(Global.getSelectCitySiteType(), Global.getSelectCityCode(), Global
////                .getSelectCityCode(), AD2, mCurentPage, PAGE_SIZE, false);
////    }
////
////    private void refreshHomeData() {
////        mHomePresenter.getClassification(Global.getSelectCitySiteType(), Global.getSelectCityCode(), Global
////                .getSelectCityCode(), "");
////        mHomePresenter.getShangquan(Global.getSelectCitySiteType(), Global.getSelectCityCode(), Global
////                .getSelectCityCode());
////        mHomePresenter.requestHomeInfo(Global.getSelectCitySiteType(), Global.getSelectCityCode(), Global
////                .getSelectCityCode(), AD2, mCurentPage, PAGE_SIZE, true);
////    }
//
//
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onEvent(OpencitysRequestEvent event) {
//        mHomePresenter.getOpenCitys();
//    }
//
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onEvent(ShanghufenleiRequestEvent event) {
//        mHomePresenter.getClassification(Global.getSelectCitySiteType(), Global.getSelectCityCode(), Global
//                .getSelectCityCode(), "");
//    }
//
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onEvent(ShangquanRequestEvent event) {
//        mHomePresenter.getShangquan(Global.getSelectCitySiteType(), Global.getSelectCityCode(), Global
//                .getSelectCityCode());
//    }
//
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onEvent(LocationSwitchEvent event) {
//        Consent.currentLocation = event.cLocation;
//        if (event.locationDialog) {
//            if (nearbyR != null) {
//                nearbyR.cleanNearby();
//            }
//            setNavicationTab();
//            updateCityShanghuList();
//        }
//        mHomeRefreshAdapter.resetLocationList(event.cLocation);
//    }
//
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onEvent(CitySelectChangEvent event) {
//        mCurentPage = 1;
//        headMerchant = "";
//        Consent.currentLocation = event.currentLocation;
//        setNavicationProject();
//        homeNearbyView.onReductionTabSelect();
//        mHomeRefreshAdapter.resetLocationList(event.currentLocation);
//        if (nearbyR != null) {
//            nearbyR.cleanNearby();
//        }
//        updateCityShanghuList();
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        EventBus.getDefault().unregister(this);
//    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        // 获取解析结果
//        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
//        if (result != null) {
//            if (result.getContents() == null) {
////                Toast.makeText(getActivity(), "取消扫描", Toast.LENGTH_LONG).show();
//            } else {
//                dealResult(result.getContents());
//            }
//        } else {
//            super.onActivityResult(requestCode, resultCode, data);
//        }
//    }
//
//    private void dealResult(String sao_result) {
//        Uri uri = Uri.parse(sao_result);
//        String path = uri.getQuery();
//        if (!TextUtils.isEmpty(path)) {
//            if (path.contains("s=") && path.contains("q=")) {// 收款码
//                sao_result = uri.getQueryParameter("q");
//            } else if (path.contains("s=") && path.contains("m=") && path.contains("c=")) {// 商户码
//                sao_result = uri.getQueryParameter("m");
//            }
//            if (sao_result != null && sao_result.length() > 0) {// 商户码进在线支付
//                PayActivity.openForResult(getActivity(), null, sao_result, OTHER, false);
//            } else {
//                ToastUtils.showShortToast("这个码我不认识哦~");
//            }
//        } else {
//            ToastUtils.showShortToast("这个码我不认识哦~");
//        }
//    }
//
//    private void setNavicationTab() {
//        MyLocation location = Global.getMyLocation();
//        if (TextUtils.isEmpty(location.getCityName())) {
//            homeTemporary.locationStr = "你当前在：" + Global.getSelectCity();
//            homeTemporary.scopeStr = "全城区域";
//        } else {
//            if (Global.getSelectCity().startsWith(location.getCityName())) {
//                homeTemporary.locationStr = "你当前在：" + location.getAddress();
//                homeTemporary.scopeStr = "附近区域";
//            } else {
//                homeTemporary.locationStr = "你当前在：" + Global.getSelectCity();
//                homeTemporary.scopeStr = "全城区域";
//            }
//        }
//        cbCatagoryId.setText("全部类型");
//        cbScopeId.setText(homeTemporary.scopeStr);
//        cbSortwaysId.setText("智能排序");
//        homeTemporary.catagoryStr = "全部类型";
//        homeTemporary.sortwaysStr = "智能排序";
//        btLeft.setText(getSelectCity());
//
//    }
//
//    private void setNavicationProject() {
//        homeTemporary.catagoryStr = "全部类型";
//        if (TextUtils.isEmpty(Global.getMyLocation().getCityName())) {
//            homeTemporary.scopeStr = "全城区域";
//        } else {
//            if (Global.getSelectCity().startsWith(Global.getMyLocation().getCityName())) {
//                homeTemporary.scopeStr = "附近区域";
//            } else {
//                homeTemporary.scopeStr = "全城区域";
//            }
//        }
//        homeTemporary.locationStr = "你当前在：" + Global.getMyLocation().getAddress();
//        homeTemporary.sortwaysStr = "智能排序";
//
//        cbCatagoryId.setText(homeTemporary.catagoryStr);
//        cbScopeId.setText(homeTemporary.scopeStr);
//        cbSortwaysId.setText(homeTemporary.sortwaysStr);
//        btLeft.setText(getSelectCity());
//    }
//
//
//}
