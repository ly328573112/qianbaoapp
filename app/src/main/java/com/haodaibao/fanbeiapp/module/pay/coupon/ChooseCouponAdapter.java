package com.haodaibao.fanbeiapp.module.pay.coupon;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baseandroid.utils.FormatUtil;
import com.baseandroid.widget.customtext.richtext.RichTextView;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.CouponInfo;

import java.math.BigDecimal;
import java.util.ArrayList;

import static com.baseandroid.config.Constant.CHOOSE_COUPON_RESULTCODE;

/**
 * Created by hdb on 2017/8/8.
 */

public class ChooseCouponAdapter extends RecyclerView.Adapter {
    private LayoutInflater inflater;
    private ArrayList<CouponInfo> list;
    private int count;//可用数量
    private Context mContext;

    public ChooseCouponAdapter(Context mContext, ArrayList<CouponInfo> list) {
        this.mContext = mContext;
        inflater = LayoutInflater.from(mContext);
        this.list = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CouponHolder(inflater.inflate(R.layout.item_choose_coupon, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        CouponHolder couponHolder = (CouponHolder) holder;
        couponHolder.canuse_ly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("couponInfo", list.get(position));
                ((ChooseCouponActivity) mContext).setResult(CHOOSE_COUPON_RESULTCODE, intent);
                ((ChooseCouponActivity) mContext).finish();
            }
        });
        couponHolder.cannot_use_ly.setEnabled(false);
        if (position == 0) {
            couponHolder.coupon_total.setVisibility(View.VISIBLE);
            couponHolder.coupon_total.setText(count == 0 ? "没有优惠券可用" : Html.fromHtml("有" + "<font color='#C59D5F'>" + count + "</font>" + "张优惠券可用"));
        } else {
            couponHolder.coupon_total.setVisibility(View.GONE);
        }
        CouponInfo info = list.get(position);
        String amount = info.getAmount();
        String beginamt = info.getUsebeginamt();
        String amountnew = FormatUtil.filterDecimalPoint(new BigDecimal(amount).setScale(2, BigDecimal.ROUND_HALF_UP) + "");
        String beginamtnew = FormatUtil.filterDecimalPoint(new BigDecimal(beginamt).setScale(2, BigDecimal.ROUND_HALF_UP) + "");
        if (TextUtils.equals("0", info.getCanUse())) {
            couponHolder.canuse_ly.setVisibility(View.VISIBLE);
            couponHolder.cannot_use_ly.setVisibility(View.GONE);
            couponHolder.coupon_yuan.setText(amountnew);
            couponHolder.coupon_fange.setText(info.getCouponName());
            couponHolder.coupon_limit.setText(info.getRole());
            couponHolder.coupon_expire.setText(FormatUtil.changeTime(info.getEndtime(), "yyyy-MM-dd") + "到期");
            couponHolder.coupon_des.setText("满¥" + beginamtnew + "元可用");
            if (TextUtils.equals("0", info.getIsSelect())) {
                couponHolder.coupon_select.setBackgroundResource(R.drawable.icon_coupon_right_sec);
            } else if (TextUtils.equals("1", info.getIsSelect())) {
                couponHolder.coupon_select.setBackgroundResource(R.drawable.icon_coupon_right_nor);
            }
        } else {
            couponHolder.canuse_ly.setVisibility(View.GONE);
            couponHolder.cannot_use_ly.setVisibility(View.VISIBLE);
            couponHolder.coupon_yuan1.setText(amountnew);
            couponHolder.coupon_fange1.setText(info.getCouponName());
            couponHolder.coupon_limit1.setText(info.getRole());
            couponHolder.coupon_expire1.setText(FormatUtil.changeTime(info.getEndtime(), "yyyy-MM-dd") + "到期");
            couponHolder.coupon_des1.setText("满¥" + beginamtnew + "元可用");
            couponHolder.donuse_reason.setText(info.getUnReason());
        }
        if (position == getItemCount() - 1) {
            couponHolder.bg_bottom.setVisibility(View.VISIBLE);
        } else {
            couponHolder.bg_bottom.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        count = 0;
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                if (TextUtils.equals("0", list.get(i).getCanUse())) {
                    count++;
                }
            }
        }

        return list == null ? 0 : list.size();
    }


    //R.layout.item_choose_coupon
    class CouponHolder extends RecyclerView.ViewHolder {
        RichTextView coupon_total;
        LinearLayout cannot_use_ly;
        LinearLayout canuse_ly;
        TextView coupon_yuan, coupon_des, coupon_fange, coupon_limit, coupon_expire;
        ImageView coupon_select;
        TextView coupon_yuan1, coupon_des1, coupon_fange1, coupon_limit1, coupon_expire1, donuse_reason;
        View bg_bottom;

        public CouponHolder(View itemView) {
            super(itemView);
            coupon_total = (RichTextView) itemView.findViewById(R.id.coupon_total);
            cannot_use_ly = (LinearLayout) itemView.findViewById(R.id.cannot_use_ly);
            canuse_ly = (LinearLayout) itemView.findViewById(R.id.canuse_ly);
            coupon_yuan = (TextView) itemView.findViewById(R.id.coupon_yuan);
            coupon_yuan1 = (TextView) itemView.findViewById(R.id.coupon_yuan1);
            coupon_des = (TextView) itemView.findViewById(R.id.coupon_des);
            coupon_des1 = (TextView) itemView.findViewById(R.id.coupon_des1);
            coupon_fange = (TextView) itemView.findViewById(R.id.coupon_fange);
            coupon_fange1 = (TextView) itemView.findViewById(R.id.coupon_fange1);
            coupon_limit = (TextView) itemView.findViewById(R.id.coupon_limit);
            coupon_limit1 = (TextView) itemView.findViewById(R.id.coupon_limit1);
            coupon_expire = (TextView) itemView.findViewById(R.id.coupon_expire);
            coupon_expire1 = (TextView) itemView.findViewById(R.id.coupon_expire1);
            coupon_select = (ImageView) itemView.findViewById(R.id.coupon_select);
            bg_bottom = itemView.findViewById(R.id.bg_bottom);
            donuse_reason = (TextView) itemView.findViewById(R.id.donuse_reason);
        }
    }

    public int getCount() {
        return count;
    }
}
