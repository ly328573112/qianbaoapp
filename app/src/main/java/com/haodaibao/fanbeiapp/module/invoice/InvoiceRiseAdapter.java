package com.haodaibao.fanbeiapp.module.invoice;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 开发者：LuoYi
 * Time: 2018 16:52 2018/2/6 02
 */

public class InvoiceRiseAdapter extends BaseRecycleViewAdapter<String> {

    public InvoiceRiseAdapter(Context context) {
        super(context);
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, String item) {
        if (holder instanceof RiseHolder) {
            RiseHolder riseHolder = (RiseHolder) holder;
            riseHolder.typeTv.setText(item);
            riseHolder.companyTitleTv.setText(item);
            riseHolder.dutyParagraphTv.setText(item);
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        View layoutInflater = LayoutInflater.from(mContext).inflate(R.layout.item_invoice_rise, parent, false);
        return new RiseHolder(layoutInflater);
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class RiseHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.invoice_type_tv)
        TextView typeTv;
        @BindView(R.id.invoice_company_title_tv)
        TextView companyTitleTv;
        @BindView(R.id.invoice_duty_paragraph_tv)
        TextView dutyParagraphTv;

        RiseHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
