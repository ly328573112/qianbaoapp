package com.haodaibao.fanbeiapp.module.home;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.os.Environment;
import android.view.View;
import android.widget.Toast;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.base.BaseDoubleClickExitHelper;
import com.baseandroid.config.Api;
import com.baseandroid.config.Global;
import com.baseandroid.download.DownloadException;
import com.baseandroid.download.DownloadManager;
import com.baseandroid.download.downinterfaceimpl.SimpleDownLoadCallBack;
import com.baseandroid.utils.ToastUtils;
import com.baseandroid.utils.VersionUtil;
import com.haodaibao.fanbeiapp.module.start.StartContract;
import com.haodaibao.fanbeiapp.module.start.StartPresenter;
import com.haodaibao.fanbeiapp.module.versionupdate.UpdateMessageDialog;
import com.haodaibao.fanbeiapp.module.versionupdate.UpdateProgressDialog;
import com.haodaibao.fanbeiapp.repository.json.CheckUpdate;
import com.jayfeng.lesscode.core.AppLess;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.File;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * 开发者：LuoYi
 * Time: 2017 11:03 2017/12/20 12
 */

public class VersionUpdateView implements HomeContract.VersionUpdateView, StartContract.View {

    private BaseActivity activity;
    private final StartPresenter startPresenter;
    protected BaseDoubleClickExitHelper mDoubleClickExitHelper;

    private UpdateMessageDialog mUpdataDialog;
    private UpdateProgressDialog mUpdateProgressDialog;
    private File mDownLoadApkFolde;
    private String mDownLoadApkFileName;

    public VersionUpdateView(BaseActivity activity) {
        this.activity = activity;
        startPresenter = new StartPresenter(this, activity);
        mDoubleClickExitHelper = new BaseDoubleClickExitHelper(activity);
//        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
        mDownLoadApkFolde = new File(Environment.getExternalStorageDirectory().getPath() + "/" + Global.getContext().getPackageName() + "/download");
        if (!mDownLoadApkFolde.exists()) {
            mDownLoadApkFolde.mkdirs();
        }
        mDownLoadApkFileName = activity.getPackageName() + "." + AppLess.$vername() + ".apk";
    }

    @Override
    public void onCheckUpdate() {
        startPresenter.checkUpdate();
    }

    @Override
    public void OnVersionUpdate(CheckUpdate checkUpdate) {
        checkUpdate(checkUpdate);
    }

    @Override
    public void OnVersionUpdateError() {

    }

    private void checkUpdate(final CheckUpdate checkUpdate) {
        //是否有更新
        final boolean update = VersionUtil.needUpdate(AppLess.$vername(), checkUpdate.getLatest_version(), false);
        final boolean force = VersionUtil.needUpdate(AppLess.$vername(), checkUpdate.getForce_update_version(), true);
        if (update || force) {
            mUpdataDialog = new UpdateMessageDialog();
            mUpdataDialog.setCancelable(false);
            mUpdataDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    //是否有更新
                    mUpdataDialog.setContent(checkUpdate.getVersion_desc()
                            .replaceAll("#", "\r\n"));
                    if (update && force) {
                        // APK强制更新
                        mUpdataDialog.hideCancelButton();
                    }

                    mUpdataDialog.setConfirmOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mUpdataDialog.dismiss();
                            new RxPermissions((Activity) activity).request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    .subscribe(new Observer<Boolean>() {
                                        @Override
                                        public void onSubscribe(Disposable d) {

                                        }

                                        @Override
                                        public void onNext(Boolean aBoolean) {
                                            if (aBoolean) {
                                                mUpdateProgressDialog = new UpdateProgressDialog();
                                                mUpdateProgressDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                                    @Override
                                                    public void onShow(DialogInterface dialog) {
                                                        DownloadManager.getInstance()
                                                                .download(Api.UpdateUrl, mDownLoadApkFolde, mDownLoadApkFileName, false, new SimpleDownLoadCallBack() {

                                                                    @Override
                                                                    public void onProgress(long finished, long total, int progress) {
                                                                        if (mUpdateProgressDialog != null) {
                                                                            mUpdateProgressDialog
                                                                                    .setProgress(progress);
                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onCompleted(File downloadfile) {
                                                                        if (mUpdateProgressDialog != null && mUpdateProgressDialog
                                                                                .isVisible()) {
                                                                            mUpdateProgressDialog
                                                                                    .dismiss();
                                                                        }
                                                                        ((HomeActivity) activity)
                                                                                .downloadCompleted(new File(mDownLoadApkFolde, mDownLoadApkFileName)
                                                                                        .getAbsolutePath());
                                                                    }

                                                                    @Override
                                                                    public void onFailed(DownloadException e) {
                                                                        if (mUpdateProgressDialog != null && mUpdateProgressDialog
                                                                                .isVisible()) {
                                                                            mUpdateProgressDialog
                                                                                    .dismiss();
                                                                        }
                                                                        ToastUtils.showShortToast(e
                                                                                .getErrorMessage());
                                                                    }
                                                                });

                                                    }
                                                });
                                                mUpdateProgressDialog.show(activity.getSupportFragmentManager(), "updateprogressdialog");
                                            } else {
                                                ToastUtils.showShortToast("检查到新版本，但您的权限被拒绝，无法使用存储空间，无法下载最新版本。", Toast.LENGTH_SHORT);
                                            }
                                        }

                                        @Override
                                        public void onError(Throwable e) {

                                        }

                                        @Override
                                        public void onComplete() {

                                        }
                                    });

                        }
                    });

                    mUpdataDialog.setCancelOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mUpdataDialog.dismiss();
                        }
                    });
                }
            });
            mUpdataDialog.show(activity.getSupportFragmentManager(), "updatadialog");
        }
    }

    @Override
    public void OnUserInfoSuccess() {

    }

    @Override
    public void OnUserInfoError() {

    }

}
