package com.haodaibao.fanbeiapp.module.merchant.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.DateUtil;
import com.baseandroid.utils.UiUtils;
import com.baseandroid.widget.customimageview.CircleImageViewWithStroke;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.baseandroid.widget.recycleadpter.GridDivider;
import com.bumptech.glide.Glide;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.CommentClickListener;
import com.haodaibao.fanbeiapp.module.CommentImageAdapter;
import com.haodaibao.fanbeiapp.module.MerchantListAdapterView;
import com.haodaibao.fanbeiapp.module.merchant.temporarydata.MerchantTemporary;
import com.haodaibao.fanbeiapp.repository.json.CommentListBean;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 开发者：LuoYi
 * Time: 2017 14:49 2017/9/6 09
 */

public class MerchantDetailAdapter extends BaseRecycleViewAdapter<MerchantTemporary> {

    private Context mContext;
    private CommentClickListener clickListener;
    private final MerchantListAdapterView merchantListAdapterView;

    public MerchantDetailAdapter(Context context, CommentClickListener clickListener) {
        super(context);
        this.mContext = context;
        this.clickListener = clickListener;
        merchantListAdapterView = new MerchantListAdapterView(mContext);
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return getData().get(position).getmType();
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0:
                return new EvaluateHolder(View.inflate(mContext, R.layout.item_evaluate_layout, null));
            case 1:
                return new MerchantListAdapterView.MerchantViewHolder(View.inflate(mContext, R.layout.adapter_shanghusearchresult, null));
            case 2:
                return new CommentFootHolder(LayoutInflater.from(mContext).inflate(R.layout.comment_footer_ly, parent, false));
            case 3:
                return new PreferredHolder(LayoutInflater.from(mContext).inflate(R.layout.preferred_ly, parent, false));
            default:
                return null;
        }
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, MerchantTemporary bean) {
        if (holder instanceof EvaluateHolder) {
            EvaluateHolder evaluateHolder = (EvaluateHolder) holder;
            CommentListBean.CommentBean commentBean = (CommentListBean.CommentBean) bean;
            setEvaluateList(evaluateHolder, commentBean);
        } else if (holder instanceof MerchantListAdapterView.MerchantViewHolder) {
            MerchantListAdapterView.MerchantViewHolder merchantListHolder = (MerchantListAdapterView.MerchantViewHolder) holder;
            final MerchantInfoEntiy infoEntiy = (MerchantInfoEntiy) bean;
            merchantListAdapterView.setMerchantView(merchantListHolder, infoEntiy);
            merchantListHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.goToMerchantDetail(infoEntiy.getMerchantno());
                }
            });
        } else if (holder instanceof CommentFootHolder) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.goToAllComment();
                }
            });
        }
    }

    /**
     * 评价
     */
    private void setEvaluateList(EvaluateHolder evaluateHolder, CommentListBean.CommentBean commentBean) {
        String url = CommonUtils.getImageUrl(commentBean.getFace());
        Glide.with(mContext).load(url).error(R.drawable.icon_default_head_info).into(evaluateHolder.user_icon);
        String name = TextUtils.isEmpty(commentBean.getAlias()) ? commentBean.getMobile() : commentBean.getAlias();
        evaluateHolder.user_name.setText(name);
        String time = DateUtil.changeStringDate(commentBean.getTime());
        evaluateHolder.evaluate_time.setText(time);
        if (TextUtils.isEmpty(commentBean.getComment())) {
            evaluateHolder.evaluate_content.setVisibility(View.GONE);
        } else {
            evaluateHolder.evaluate_content.setVisibility(View.VISIBLE);
            evaluateHolder.evaluate_content.setText(commentBean.getComment());
        }
        if (TextUtils.equals(commentBean.getStar(), "5.00")) {
            evaluateHolder.evaluate_satisfied.setImageResource(R.drawable.text_satisfied);
        } else if (TextUtils.equals(commentBean.getStar(), "3.00")) {
            evaluateHolder.evaluate_satisfied.setImageResource(R.drawable.text_commonly);
        } else {
            evaluateHolder.evaluate_satisfied.setImageResource(R.drawable.text_dissatisfied);
        }
        ArrayList urls = new ArrayList<>();
        urls.addAll(commentBean.getImgs());
        CommentImageAdapter adapter = new CommentImageAdapter(urls, mContext, clickListener);
        evaluateHolder.evaluate_gridview.setAdapter(adapter);
        if (commentBean.getImgs().size() == 0) {
            evaluateHolder.evaluate_gridview.setVisibility(View.GONE);
        } else {
            evaluateHolder.evaluate_gridview.setVisibility(View.VISIBLE);
        }
    }


    /**
     * 评价内容布局
     */
    class EvaluateHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_icon)
        CircleImageViewWithStroke user_icon;
        @BindView(R.id.user_name)
        TextView user_name;
        @BindView(R.id.evaluate_content)
        TextView evaluate_content;
        @BindView(R.id.evaluate_time)
        TextView evaluate_time;
        @BindView(R.id.evaluate_satisfied)
        ImageView evaluate_satisfied;
        @BindView(R.id.evaluate_gridview)
        RecyclerView evaluate_gridview;

        public EvaluateHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            evaluate_gridview.setLayoutManager(new GridLayoutManager(mContext, 3));
            evaluate_gridview.addItemDecoration(new GridDivider(mContext, UiUtils.dp2px(mContext, 10), ContextCompat.getColor(mContext, R.color.white)));
        }
    }

    /**
     * 评价尾部文案
     */
    class CommentFootHolder extends RecyclerView.ViewHolder {
        public CommentFootHolder(View itemView) {
            super(itemView);
        }
    }

    /**
     * 优选文案
     */
    class PreferredHolder extends RecyclerView.ViewHolder {

        public PreferredHolder(View itemView) {
            super(itemView);
        }
    }

}
