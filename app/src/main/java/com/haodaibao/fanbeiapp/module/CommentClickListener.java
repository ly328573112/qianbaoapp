package com.haodaibao.fanbeiapp.module;

import java.util.List;

public interface CommentClickListener {
    void goToAllComment();

    void goToEnlarge(List<String> imglists, int pos);

    void goToMerchantDetail(String merchantno);
}