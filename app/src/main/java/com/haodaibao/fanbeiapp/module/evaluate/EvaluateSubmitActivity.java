package com.haodaibao.fanbeiapp.module.evaluate;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.camera.CameraUtil;
import com.baseandroid.utils.EmojiUtil;
import com.baseandroid.utils.StatusBarHelper;
import com.baseandroid.utils.ToastUtils;
import com.baseandroid.widget.CustomTextWatcher;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.event.OriginalDeleteEvent;
import com.haodaibao.fanbeiapp.event.PaylistEvent;
import com.haodaibao.fanbeiapp.fileuploading.FileUploaderManager;
import com.haodaibao.fanbeiapp.imageadapter.CameraSelectView;
import com.haodaibao.fanbeiapp.imageadapter.ImageClickListener;
import com.haodaibao.fanbeiapp.imageadapter.PhotoImageAdapter;
import com.haodaibao.fanbeiapp.module.dialog.MessageDialog;
import com.haodaibao.fanbeiapp.module.imgoriginal.OriginalPagerActivity;
import com.haodaibao.fanbeiapp.module.merchant.MerchantDetailActivity;
import com.haodaibao.fanbeiapp.repository.json.CommentListBean;
import com.haodaibao.fanbeiapp.repository.json.PhotoInfo;
import com.haodaibao.fanbeiapp.repository.json.UploadResult;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

import static com.baseandroid.config.Constant.PAYSUCC_COMMENT_RESULTCODE;


/**
 * 开发者：LuoYi
 * Time: 2017 16:24 2017/9/20 09
 */

public class EvaluateSubmitActivity extends BaseActivity implements View.OnClickListener, EvaluateContract.EvaluateView, ImageClickListener, CameraSelectView.CameraPotoListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.right_text)
    TextView rightText;
    @BindView(R.id.satis_ragp)
    RadioGroup satisRagp;
    @BindView(R.id.comment_content)
    EditText commentContent;
    @BindView(R.id.comment_gridview)
    RecyclerView commentGridview;


    private CameraUtil cameraUtil;
    private String orderno;
    private String merchantno;

    private List<PhotoInfo> photoList = new ArrayList<>();// 照片uri列表
    private int currentItemId = 1;// 加号
    private int COLUMN = 9;//图片可选数量
    private int imageColumn = 1;//图片集合是否带有加号
    private PhotoImageAdapter imageAdapter;
    private EvaluatePresenter evaluatePresenter;
    private String star = "5";
    private CameraSelectView selectView;
    private FileUploaderManager uploaderManager;
    private MessageDialog messageDialog;

    public static void open(Activity activity, String merchantno, String orderno) {
        Bundle bundle = new Bundle();
        bundle.putString("merchantno", merchantno);
        bundle.putString("orderno", orderno);
        ActivityJumpUtil.next(activity, EvaluateSubmitActivity.class, bundle, 0);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_evaluate_submit;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(EvaluateSubmitActivity.this);
        toolbarBack.setOnClickListener(this);
        rightText.setVisibility(View.VISIBLE);
        rightText.setOnClickListener(this);
        toolbarTitle.setText("发表评价");

        evaluatePresenter = new EvaluatePresenter(this);
        selectView = new CameraSelectView(this);
        selectView.setPictureCut(false);
        cameraUtil = new CameraUtil(this);
        photoList.clear();
        PhotoInfo info = new PhotoInfo(currentItemId, null, false);
        photoList.add(info);
        initListView();
        satisRagp.setOnCheckedChangeListener(checkedChangeListener);
        commentContent.addTextChangedListener(new CustomTextWatcher(this, commentContent, 500));
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        Intent intent = getIntent();
        orderno = intent.getExtras().getString("orderno");
        merchantno = intent.getExtras().getString("merchantno");

        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.right_text:
                String text = commentContent.getText().toString();
                text = EmojiUtil.replaceEmoji(text);
                rightText.setEnabled(false);
                showUploading();
                if (photoList.size() > 1) {
                    if (uploaderManager == null) {
                        uploaderManager = FileUploaderManager.getInstance();
                    }
                    uploaderManager.updateImage(photoList, uploaderResult);
                } else {
                    // 拼接上传图片名称路径
                    StringBuffer imgUriBuffer = new StringBuffer();
                    String s = imgUriBuffer.toString();
                    processUploading("提交中");
                    evaluatePresenter.submintComment(orderno, merchantno, star, text, s);
                }
                break;
            default:
                break;
        }
    }

    private RadioGroup.OnCheckedChangeListener checkedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
            switch (checkedId) {
                case R.id.satisfied_btn:
                default:
                    star = "5";
                    break;
                case R.id.commonly_btn:
                    star = "3";
                    break;
                case R.id.dissatisfied_btn:
                    star = "1";
                    break;
            }
        }
    };

    private void initListView() {
        GridLayoutManager layoutManager = new GridLayoutManager(this, 4, LinearLayoutManager.VERTICAL, false);
        commentGridview.setLayoutManager(layoutManager);
        commentGridview.setHasFixedSize(true);
        imageAdapter = new PhotoImageAdapter(this);
        commentGridview.setAdapter(imageAdapter);
        commentGridview.setItemAnimator(new DefaultItemAnimator());
        imageAdapter.setImageClickListener(this);
        imageAdapter.addData(photoList);
    }

    @Override
    public void openCamera(int position) {
        int photoNumber = photoList.get(position).getPhotoNumber();
        if (photoNumber == currentItemId) {
            int optionalNumber = COLUMN - (photoList.size() - imageColumn);
            selectView.showCameraDialog(cameraUtil, optionalNumber);
            selectView.setCameraPotoListener(this);
        } else if (photoNumber == 0) {
            ArrayList<String> arrayList = new ArrayList<>();
//            String[] strings = new String[photoList.size() - imageColumn];
            for (int i = 0; i < photoList.size() - imageColumn; i++) {
                arrayList.add(photoList.get(i).getPhotoUri());
            }
            OriginalPagerActivity.start(this, arrayList, position);
        }
    }

    @Override
    public void onDeleteImage(int position) {
        photoList.remove(position);
        confineListSize();
        imageAdapter.resetData(photoList);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(OriginalDeleteEvent event) {
        onDeleteImage(event.curPosition);
    }

    /**
     * 加号始终放在List最后
     */
    private void confineListSize() {
        for (PhotoInfo photoInfo : photoList) {
            if (photoInfo.getPhotoNumber() == currentItemId) {
                photoList.remove(photoInfo);
                break;
            }
        }
        if (photoList.size() < COLUMN) {
            PhotoInfo info = new PhotoInfo(currentItemId, null, false);
            photoList.add(info);
            imageColumn = 1;
        } else {
            imageColumn = 0;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        cameraUtil.onHandleActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onEvaluateList(CommentListBean commentListBean) {

    }

    @Override
    public void onEvaluateListError() {

    }

    @Override
    public void onSubmint() {
        rightText.setEnabled(true);
        closeUploading();
        setResult(PAYSUCC_COMMENT_RESULTCODE);
        Bundle b = new Bundle();
        b.putString("merchantno", merchantno);
        ActivityJumpUtil.next(this, MerchantDetailActivity.class, b, 0);


        new Handler().postDelayed(new Runnable() {
            public void run() {
                PaylistEvent paylistEvent = new PaylistEvent();
                paylistEvent.setComment(true);
                EventBus.getDefault().post(paylistEvent);
                finish();
            }
        }, 500);
    }

    @Override
    public void onSubmintError() {
        rightText.setEnabled(true);
        closeUploading();
    }


    @Override
    public void onResultPotoList(List<PhotoInfo> imgList) {
        this.photoList.addAll(imgList);
        confineListSize();
        imageAdapter.resetData(photoList);
    }

    private FileUploaderManager.UploaderResultListener uploaderResult = new FileUploaderManager.UploaderResultListener() {

        @Override
        public void onNetworkType(boolean cleanList) {
            closeUploading();
            if (cleanList) {
                uploaderManager.cleanFileList();
            }
            rightText.setEnabled(true);
            ToastUtils.showShortToastSafe("网络访问失败", 1000);
        }

        @Override
        public void onUploadProcessNumber(int successNumber, int fileListSize) {
            processUploading((successNumber + 1) + "/" + fileListSize);
        }

        @Override
        public void onUploaderFail(final List<String> failUrl, final int successNumber) {
            closeUploading();
            if (failUrl == null && successNumber == 0) {
                ToastUtils.showShortToastSafe("图片上传终止,请检查网络是否连接!", 1000);
                return;
            }
            if (messageDialog == null) {
                messageDialog = MessageDialog.newInstance();
            }
            if (!messageDialog.isVisible()) {
                messageDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        messageDialog.hideTitle();
                        messageDialog.setContent("有" + failUrl.size() + "张图片上传失败,请点击继续上传！");
                        messageDialog.setConfirmText("继续上传");
                        messageDialog.setConfirmOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showUploading();
                                uploaderManager.uploaderStringFile(failUrl, successNumber);
                                messageDialog.dismiss();
                            }
                        });
                        messageDialog.setCancelOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                rightText.setEnabled(true);
                                uploaderManager.cleanFileList();
                                messageDialog.dismiss();
                            }
                        });
                    }
                });

                messageDialog.show(getSupportFragmentManager(), "MessageDialog");
            }
        }

        @Override
        public void onUploaderSuccess(List<UploadResult.UploadInfo> uploadFileList) {
            closeAnimatorUploading();
            // 拼接上传图片名称路径
            StringBuffer imgUriBuffer = new StringBuffer();
            for (UploadResult.UploadInfo uploadInfo : uploadFileList) {
                imgUriBuffer.append(uploadInfo.getName());
                imgUriBuffer.append(",");
            }
            String s = imgUriBuffer.toString();
            String text = commentContent.getText().toString();
            text = EmojiUtil.replaceEmoji(text);
            evaluatePresenter.submintComment(orderno, merchantno, star, text, s);
        }
    };
}
