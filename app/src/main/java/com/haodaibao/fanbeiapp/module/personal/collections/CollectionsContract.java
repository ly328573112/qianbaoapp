package com.haodaibao.fanbeiapp.module.personal.collections;

import com.haodaibao.fanbeiapp.repository.json.CollectionsBean;

/**
 * Created by Routee on 2017/9/4.
 * description: ${cusor}
 */

public interface CollectionsContract {
    interface View {
        void setResultView(CollectionsBean bean);
    }

    interface Presenter {
        void getCollections();
    }
}
