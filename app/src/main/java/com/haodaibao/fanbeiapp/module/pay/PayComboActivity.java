package com.haodaibao.fanbeiapp.module.pay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Constant;
import com.baseandroid.utils.FormatUtil;
import com.baseandroid.utils.StatusBarHelper;
import com.baseandroid.widget.customtext.richtext.RichTextView;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.pay.coupon.ChooseCouponActivity;
import com.haodaibao.fanbeiapp.module.personal.paylist.PaylistActivity;
import com.haodaibao.fanbeiapp.repository.json.CouponInfo;
import com.haodaibao.fanbeiapp.repository.json.LableInfo;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;
import com.haodaibao.fanbeiapp.repository.json.MyCouponsResp;
import com.haodaibao.fanbeiapp.repository.json.PackageAuthBean;
import com.haodaibao.fanbeiapp.repository.json.PackageDetailBean;
import com.haodaibao.fanbeiapp.repository.json.PayInfoDetailBean;
import com.qianbao.mobilecashier.QBPaySDK;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static com.baseandroid.config.Constant.CHOOSE_COUPON_RESULTCODE;
import static com.baseandroid.config.Constant.PACKAGE_STATUS;
import static com.baseandroid.config.Constant.PAYSUCC_COMMENT_RESULTCODE;


/**
 * date: 2017/10/13.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class PayComboActivity extends BaseActivity implements PayContract.View {
    @BindView(R.id.goto_buy)
    protected TextView goto_buy;
    @BindView(R.id.toolbar_title)
    protected TextView toolbar_title;
    @BindView(R.id.combo_coupon)
    protected TextView combo_coupon;
    @BindView(R.id.toolbar_back)
    protected RelativeLayout toolbar_back;
    @BindView(R.id.merchant_location)
    protected TextView merchant_location;
    @BindView(R.id.combo_total)
    protected RichTextView combo_total;
    @BindView(R.id.combo_title)
    TextView combo_title;
    @BindView(R.id.combo_amount)
    TextView combo_amount;
    @BindView(R.id.pay_combo_hint_tv)
    TextView payComboHintTv;

    private PayPresenter payPresenter;
    private MerchantInfoEntiy merchantDetailResp;
    private String merchantNo;
    public static final String PAY_COMBO = "payCombo";
    public static final String MERCHANT_NO = "merchantNo";
    public static final String PACKAGE_DETAIL = "packageDetailBean";
    private ArrayList<CouponInfo> couponsList;
    private double comboReduce = 0.0;//套餐金额
    //匹配出来的红包
    private CouponInfo matchCouponInfo;
    private String cityCode;
    private Double couponAmount = 0.0;
    private String couponNo;
    private Double actualPayNum = 0.0;
    private PackageDetailBean packageDetailBean;
//    private MessageDialog messageDialog;

    public static void openForResult(BaseActivity activity, String merchantNo, MerchantInfoEntiy merchantDetailResp, PackageDetailBean packageDetailBean) {
        Intent intent = new Intent(activity, PayComboActivity.class);
        intent.putExtra(PAY_COMBO, merchantDetailResp);
        intent.putExtra(MERCHANT_NO, merchantNo);
        intent.putExtra(PACKAGE_DETAIL, packageDetailBean);
        activity.startActivityForResult(intent, 0);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_paycombo;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(PayComboActivity.this);
        QBPaySDK.getInstance().initSDK(this);
        payPresenter = new PayPresenter(this);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        merchantDetailResp = (MerchantInfoEntiy) getIntent().getSerializableExtra(PAY_COMBO);
        merchantNo = getIntent().getStringExtra(MERCHANT_NO);
        packageDetailBean = (PackageDetailBean) getIntent().getSerializableExtra(PACKAGE_DETAIL);
        comboReduce = Double.valueOf(FormatUtil.formatAmount(2, packageDetailBean.preferentialprice));
        updateMerchantUI(merchantDetailResp);
        payPresenter.getMerchantInfo(merchantNo, merchantDetailResp);
    }

    private void updateMerchantUI(MerchantInfoEntiy merchantDetailResp) {
        if (merchantDetailResp == null) {
            return;
        }
        merchantNo = merchantDetailResp.getMerchantno();
        cityCode = merchantDetailResp.getCityCode();
        toolbar_title.setText(merchantDetailResp.getMerchantname());
        merchant_location.setText(merchantDetailResp.getAddress());
        combo_title.setText(packageDetailBean.packagename);
        combo_amount.setText("¥ " + packageDetailBean.preferentialprice);
    }

    @OnClick({R.id.goto_buy, R.id.combo_coupon, R.id.toolbar_back})
    public void onclick(View view) {
        switch (view.getId()) {
            case R.id.goto_buy:
                goto_buy.setEnabled(false);
//                goto_buy.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        goto_buy.setEnabled(true);
//                    }
//                }, 3000);
                payPresenter.checkMerchantPackageRule(merchantNo, packageDetailBean.packageno);
                break;
            case R.id.combo_coupon:
//                if(goto_buy.isEnabled()){
//                    goto_buy.setEnabled(false);
//                }else {
//                    goto_buy.setEnabled(true);
//                }
                ChooseCouponActivity.start(this, couponsList, comboReduce + "", couponNo, cityCode);
                break;
            case R.id.toolbar_back:
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void updateCouponList(MyCouponsResp myCouponsResp) {
        couponsList = myCouponsResp.getCouponList().getItems();
        calculation(false);
    }

    @Override
    public void updateMerchantActivityList(List<LableInfo> activities) {

    }

    @Override
    public void updateMerchantInfo(MerchantInfoEntiy merchantInfoEntiy) {
        updateMerchantUI(merchantInfoEntiy);
    }

    @Override
    public void clolsePayUI() {
        toolbar_title.postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 1000);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == CHOOSE_COUPON_RESULTCODE) {
            if (data == null) {
                matchCouponInfo = null;
            } else {
                matchCouponInfo = (CouponInfo) data.getSerializableExtra("couponInfo");
            }
            calculation(true);
        } else if (resultCode == Constant.PAYSUCC_PRESS_BACK_RESULTCODE) {
            setResult(Constant.PAYSUCC_PRESS_BACK_RESULTCODE);
            finish();
        } else if (resultCode == PAYSUCC_COMMENT_RESULTCODE) {
            setResult(Constant.PAYSUCC_COMMENT_RESULTCODE);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        QBPaySDK.getInstance().onResume();
    }

    @Override
    public void zhiFuing(String msg) {
        showToast(msg);
        startActivity(new Intent(this, PaylistActivity.class));
    }

    @Override
    public void zhiFuSuc(PayInfoDetailBean payInfoDetailBean, String orderno) {
        PaySuccessActivity.start(this, payInfoDetailBean, orderno, "");
    }

    @Override
    public void zhiFuFailed(String msg) {
        showToast("请重新支付");
    }

    @Override
    public void checkedUI(final PackageAuthBean packageAuth) {
        if (TextUtils.equals(PACKAGE_STATUS, packageAuth.packageStatus)) {
            payPresenter.createOrder(merchantNo, comboReduce + "", 0 + "", couponNo, couponAmount + "", packageDetailBean.packageno, null);
            goto_buy.postDelayed(new Runnable() {
                @Override
                public void run() {
                    goto_buy.setEnabled(true);
                }
            }, 1000);
        } else {
//            goto_buy.setEnabled(false);
            payComboHintTv.setVisibility(View.VISIBLE);
            payComboHintTv.setText(packageAuth.packageMessage);
//            if (messageDialog == null) {
//                messageDialog = MessageDialog.newInstance();
//            }
//            if (!messageDialog.isVisible()) {
//                messageDialog.setOnShowListener(new DialogInterface.OnShowListener() {
//                    @Override
//                    public void onShow(DialogInterface dialog) {
//                        messageDialog.hideTitle();
//                        messageDialog.hideCancelButton();
//                        messageDialog.setContent(packageAuth.packageMessage);
//                        messageDialog.setConfirmText("我知道了");
//                        messageDialog.setConfirmOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                goto_buy.postDelayed(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        goto_buy.setEnabled(true);
//                                    }
//                                }, 1000);
//                                messageDialog.dismiss();
//                            }
//                        });
//                    }
//                });
//                if (!messageDialog.isAdded()) {
//                    messageDialog.show(getSupportFragmentManager(), "messageDialog");
//                }
//            }
        }
    }

    private void calculation(boolean isUserChoose) {
        if (!isUserChoose) {
            matchCouponInfo = payPresenter.newmatchCoupon(comboReduce, couponsList, cityCode);
        }
        if (matchCouponInfo != null) {
            couponAmount = Double.valueOf(FormatUtil.formatAmount(2, matchCouponInfo.getAmount()));
            couponNo = matchCouponInfo.getSerialno();
        } else {
            couponAmount = 0.0;
            couponNo = "";
        }
        if (couponAmount == 0) {
            if (isUserChoose && matchCouponInfo != null) {
                combo_coupon.setTextColor(ContextCompat.getColor(this, R.color.text_hint));
                combo_coupon.setText("请选择优惠券");
            } else {
                combo_coupon.setTextColor(ContextCompat.getColor(this, R.color.text_hint));
                combo_coupon.setText("无可用优惠券");
            }
        } else {
            combo_coupon.setTextColor(ContextCompat.getColor(this, R.color.red));
            combo_coupon.setText("-" + "¥ " + FormatUtil.formatAmount(2, "" + couponAmount));
        }
        /* 计算实付金额 */
        //实付金额 = 优惠后金额 - 红包金额
        actualPayNum = Double.valueOf(comboReduce - couponAmount);
        if (actualPayNum < 0.0) {
            actualPayNum = 0.0;
        }
        combo_total.setText(Html.fromHtml("实付金额：" + "<big><big><font color='#F62241'>" + "¥ " + FormatUtil.subStrAmount(actualPayNum) + "</font></big></big>"));
        if (actualPayNum == 0.0) {
            goto_buy.setEnabled(false);
        } else {
            goto_buy.setEnabled(true);
        }
    }

}
