package com.haodaibao.fanbeiapp.module.combo;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.net.http.SslError;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Constant;
import com.baseandroid.config.Global;
import com.baseandroid.utils.FormatUtil;
import com.baseandroid.utils.RxUtils;
import com.baseandroid.utils.StatusBarHelper;
import com.baseandroid.widget.customtext.richtext.RichTextView;
import com.baseandroid.widget.loopgallery.ViewAnimationUtils;
import com.github.nukc.stateview.StateView;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.dialog.MessageDialog;
import com.haodaibao.fanbeiapp.module.login.LoginActivity;
import com.haodaibao.fanbeiapp.module.pay.PayComboActivity;
import com.haodaibao.fanbeiapp.repository.ConfigRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;
import com.haodaibao.fanbeiapp.repository.json.PackageAllBean;
import com.haodaibao.fanbeiapp.repository.json.PackageAuthBean;
import com.haodaibao.fanbeiapp.repository.json.PackageDetailBean;
import com.qianbao.shiningwhitelibrary.utils.UiUtils;

import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.OnClick;

import static com.baseandroid.config.Constant.PACKAGE_STATUS;
import static com.baseandroid.config.Constant.PAYSUCC_COMMENT_RESULTCODE;

/**
 * date: 2017/10/18.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class ComboDetailActivity extends BaseActivity implements ComboDetailContract.View {
    @BindView(R.id.toolbar_title)
    protected TextView toolbarTitle;
    @BindView(R.id.combo_next)
    protected TextView combo_next;
    @BindView(R.id.webview)
    WebView webview;
    @BindView(R.id.combo_name)
    TextView combo_name;
    @BindView(R.id.combo_time)
    RichTextView combo_time;
    @BindView(R.id.combo_user)
    RichTextView combo_user;
    @BindView(R.id.combo_rule_content)
    TextView combo_rule_content;
    @BindView(R.id.combo_price)
    TextView combo_price;
    @BindView(R.id.combo_price_yuan)
    TextView combo_price_yuan;
    @BindView(R.id.toolbar_back)
    RelativeLayout toolbar_back;
    @BindView(R.id.root_ly)
    LinearLayout root_ly;
    @BindView(R.id.combo_cannot_use_ly)
    LinearLayout combo_cannot_use_ly;
    @BindView(R.id.packagemessage)
    TextView packagemessage;
    @BindView(R.id.close_tips)
    ImageView close_tips;
    private StateView stateView;
    private ComboDetailPresenter comboDetailPresenter;
    private String merchantno, packageno;
    private MerchantInfoEntiy merchantDetailResp;
    private PackageDetailBean packageDetail;
    private MessageDialog messageDialog;

    public static void start(Activity activity, String packageno, String merchantno, MerchantInfoEntiy merchantDetailResp) {
        Intent intent = new Intent(activity, ComboDetailActivity.class);
        intent.putExtra("merchantDetailResp", merchantDetailResp);
        intent.putExtra("packageno", packageno);
        intent.putExtra("merchantno", merchantno);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_combodetail;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(ComboDetailActivity.this);
        comboDetailPresenter = new ComboDetailPresenter(this);
        stateView = StateView.inject(root_ly);

        stateView.setOnRetryClickListener(new StateView.OnRetryClickListener() {
            @Override
            public void onRetryClick() {
                comboDetailPresenter.getComboDetail(merchantno, packageno);
            }
        });
        combo_price_yuan.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        combo_price_yuan.getPaint().setAntiAlias(true);// 抗锯齿
        webview.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
            }
        });
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
                handler.proceed();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                stateView.showContent();
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);

            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
            }
        });

    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        Intent intent = getIntent();
        merchantDetailResp = (MerchantInfoEntiy) intent.getSerializableExtra("merchantDetailResp");
        merchantno = intent.getStringExtra("merchantno");
        packageno = intent.getStringExtra("packageno");
        toolbarTitle.setText("套餐详情");
        stateView.showLoading();
        comboDetailPresenter.getComboDetail(merchantno, packageno);
    }

    @OnClick({R.id.toolbar_back, R.id.combo_next, R.id.close_tips})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                if (UiUtils.isFastDoubleClick()) {
                    return;
                }
                finish();
                break;
            case R.id.close_tips:
                if (UiUtils.isFastDoubleClick()) {
                    return;
                }
                ViewAnimationUtils.startAnimation(ComboDetailActivity.this, combo_cannot_use_ly, 0, null);
//                combo_cannot_use_ly.setVisibility(View.GONE);
                break;
            case R.id.combo_next:
                if (UiUtils.isFastDoubleClick()) {
                    return;
                }
                combo_next.setEnabled(false);
                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                    startActivity(new Intent(ComboDetailActivity.this, LoginActivity.class));
                    combo_next.setEnabled(true);
                } else {
                    showLoadingDialog();
                    comboDetailPresenter.checkMerchantPackageRule(merchantno, packageno);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void updateUI(PackageAllBean packageAllBean) {
        packageDetail = packageAllBean.packageDetail;
//        ArrayList<PackageDetailBean> list = new ArrayList<>();
//        for (int i = 0; i < 6; i++) {
//            PackageDetailBean bean = new PackageDetailBean();
//            bean.
//            list.add();
//        }
        if (TextUtils.isEmpty(packageAllBean.packageDetail.packagedesciptionurl)) {
            stateView.showContent();
            webview.setVisibility(View.GONE);
        } else {
            webview.setVisibility(View.VISIBLE);
//            webview.loadUrl(packageAllBean.packageDetail.packagedesciptionurl);
//            webview.loadUrl("http://img9.qianbaocard.com/public/qianbaolife/32f109c8ba4bb7a8a13983c17f476f46.html");
//            webview.loadUrl("https://www.baidu.com/");
            ConfigRepository.getInstance().getHtmltext(packageDetail.packagedesciptionurl)
                    .compose(RxUtils.<String>applySchedulersLifeCycle(ComboDetailActivity.this))
                    .subscribe(new RxObserver<String>() {
                        @Override
                        public void onNext(String s) {
                            super.onNext(s);
                            webview.loadDataWithBaseURL(null, makeRitchEditorHtml(ComboDetailActivity.this, "richeditor.css", s), "text/html", "utf-8", null);
                        }
                    });
        }
        combo_name.setText(packageAllBean.packageDetail.packagename);
        /**
         * Html.fromHtml("实付金额：" + "<font color='#C59D5F'>" + "¥" + actualPayNum + "</font>")
         */
        String time = FormatUtil.changeTime(packageAllBean.packageDetail.begindate + "000000", "yyyy/MM/dd") + "-" + FormatUtil.changeTime(packageAllBean.packageDetail.enddate + "000000", "yyyy/MM/dd");
        combo_time.setRichText("<font color='#333333'>" + "活动时间：" + "</font>" + "<font color='#666666'>" + time + "</font>");
        String faceUser = "";
        if (TextUtils.equals("02", packageAllBean.packageDetail.faceusertypecode)) {
            faceUser = "新顾客";
        } else if (TextUtils.equals("03", packageAllBean.packageDetail.faceusertypecode)) {
            faceUser = "老顾客";
        } else {
            faceUser = "所有顾客";
        }
        combo_user.setRichText("<font color='#333333'>" + "面向用户：" + "</font>" + "<font color='#666666'>" + faceUser + "</font>");
        combo_rule_content.setText(packageAllBean.packageDetail.packageRule);
        combo_price.setText("¥" + packageAllBean.packageDetail.preferentialprice);
        combo_price_yuan.setText("¥" + packageAllBean.packageDetail.originalprice);
        if (TextUtils.equals(PACKAGE_STATUS, packageAllBean.packageAuth.packageStatus)) {
            combo_cannot_use_ly.setVisibility(View.GONE);
            combo_next.setEnabled(true);
        } else {
            combo_next.setEnabled(false);
            if (TextUtils.equals("RULE500001", packageAllBean.packageAuth.packageStatus)) {
                combo_next.setText("已抢光");
            } else {
                combo_next.setText("立即付款");
            }
            combo_cannot_use_ly.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ViewAnimationUtils.startAnimation(ComboDetailActivity.this, combo_cannot_use_ly, 1, null);
                }
            }, 200);
            packagemessage.setText(packageAllBean.packageAuth.packageMessage);
        }
    }

    @Override
    public void checkedUI(final PackageAuthBean packageAuthBean) {
        closeLoadingDialog();
        if (TextUtils.equals(PACKAGE_STATUS, packageAuthBean.packageStatus)) {
            PayComboActivity.openForResult(ComboDetailActivity.this, merchantno, merchantDetailResp, packageDetail);
            combo_next.setEnabled(true);
        } else {
            combo_next.setEnabled(false);
            if (messageDialog == null) {
                messageDialog = MessageDialog.newInstance();
            }
            if (!messageDialog.isVisible()) {
                messageDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        messageDialog.hideTitle();
                        messageDialog.hideCancelButton();
                        messageDialog.setContent(packageAuthBean.packageMessage);
                        messageDialog.setConfirmText("我知道了");
                        messageDialog.setConfirmOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                messageDialog.dismiss();
                            }
                        });
                    }
                });
                if (!messageDialog.isAdded()) {
                    messageDialog.show(getSupportFragmentManager(), "messageDialog");
                }
            }
            packagemessage.setText(packageAuthBean.packageMessage);
            if (TextUtils.equals("RULE500001", packageAuthBean.packageStatus)) {
                combo_next.setText("已抢光");
            } else {
                combo_next.setText("立即付款");
            }
        }
    }

    @Override
    public void showRetry() {
        stateView.showRetry();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Constant.PAYSUCC_PRESS_BACK_RESULTCODE) {
            setResult(Constant.PAYSUCC_PRESS_BACK_RESULTCODE);
            finish();
        } else if (resultCode == PAYSUCC_COMMENT_RESULTCODE) {
            setResult(Constant.PAYSUCC_COMMENT_RESULTCODE);
            finish();
        }
    }

    public static String makeRitchEditorHtml(Context context, String assetname, String htmlcontent) {
        StringBuilder stringBuilder = new StringBuilder();
        //read head
        stringBuilder.append("<!DOCTYPE html>\n" + "<html lang=\"en\">\n" + "<head>\n" + "    <meta charset=\"UTF-8\">\n" + "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" + "    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n");
        try {
            //read css
            stringBuilder.append("    <style type=\"text/css\">\n");
            InputStream inputStream = context.getAssets().open(assetname);
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            stringBuilder.append(new String(buffer, "UTF-8"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        stringBuilder.append("    </style>\n" + "</head>\n");
        // readbody
        stringBuilder.append("<body>\n" + "    <div class=\"content\">\n" + "        <header>\n" + "            <input hidden='content' type=\"text\" placeholder=\"请输入标题\" class=\"title\" id=\"title\">\n" + "        </header>\n" + "        <div hidden='content' class=\"line\"></div>\n" + "        <div id=\"editor\" placeholder=\"请输入正文\">\n");
        stringBuilder.append(htmlcontent).append("\n");
        stringBuilder.append("    </div>\n" + "</div>\n" + "</body>\n" + "</html>");
        return stringBuilder.toString();
    }
}
