package com.haodaibao.fanbeiapp.module.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.widget.RelativeLayout;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Constant;
import com.baseandroid.utils.StatusBarHelper;
import com.haodaibao.fanbeiapp.R;

import butterknife.BindView;

import static com.haodaibao.fanbeiapp.module.search.NearbyFragment.SEARCH_KEYWORD_TYPE;

public class SearchDeliciousActivity extends BaseActivity {

    @BindView(R.id.rl_fragment_id)
    RelativeLayout rl_fragment_id;

    NearbyFragment mNearbyFragment;
    private String result;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_search_delicious;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(SearchDeliciousActivity.this);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        Intent intent = getIntent();
        result = intent.getStringExtra("result");
        String longitude = intent.getStringExtra("longitude");
        String latitude = intent.getStringExtra("latitude");
        String cityCode = intent.getStringExtra("cityCode");
        String addressName = intent.getStringExtra("addressName");
        String sousuo = intent.getStringExtra("sousuo");
        mNearbyFragment = new NearbyFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("SEARCH_TYPE", SEARCH_KEYWORD_TYPE);
        bundle.putString("longitude", longitude);
        bundle.putString("latitude", latitude);
        bundle.putString("cityCode", cityCode);
        bundle.putString("addressName", addressName);
        bundle.putString("sousuo", sousuo);
        bundle.putString("result", result);
        mNearbyFragment.setArguments(bundle);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.rl_fragment_id, mNearbyFragment, "NearbyFragment").commitAllowingStateLoss();

    }

    @Override
    public void onBackPressed() {
        setResult(Constant.SEARCH_BACK);
        super.onBackPressed();
    }
}
