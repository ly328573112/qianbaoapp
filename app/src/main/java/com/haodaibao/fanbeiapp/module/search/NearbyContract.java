package com.haodaibao.fanbeiapp.module.search;

import com.haodaibao.fanbeiapp.repository.json.MerchantInfoResp;

public class NearbyContract {
    interface Presenter {
        void getShanghuListInfo(/*boolean isFirst,*/String merchantname, String categoryno, String districtcode, String areacode, String sort, String refundmode, String isProduct, String isVoucher, String cityCode, int pagerNumber, int pagerSize, String headMerchant, String longitude, String latitude);
    }

    interface View {
        void onShanghuListInfo(MerchantInfoResp merchantInfoResp);

        void onShanghuListInfoError();
    }
}
