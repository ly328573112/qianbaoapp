package com.haodaibao.fanbeiapp.module.merchant.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.ErrorRecoveryList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 开发者：LuoYi
 * Time: 2017 17:00 2017/11/28 11
 */

public class MerchantErrorAdapter extends BaseRecycleViewAdapter<ErrorRecoveryList.ErrorResult> {

    private OnItemClickListener itemClickListener;

    public MerchantErrorAdapter(Context context, OnItemClickListener itemClickListener) {
        super(context);
        this.itemClickListener = itemClickListener;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, final ErrorRecoveryList.ErrorResult item) {
        if (holder instanceof ErrorHolder) {
            ErrorHolder errorHolder = (ErrorHolder) holder;
            if (item.name.equals("取消")) {
                errorHolder.errorReportRl.setVisibility(View.GONE);
                errorHolder.errorCancelTv.setVisibility(View.VISIBLE);
                errorHolder.errorCancelTv.setText(item.name);
            } else {
                errorHolder.errorReportRl.setVisibility(View.VISIBLE);
                errorHolder.errorCancelTv.setVisibility(View.GONE);
                errorHolder.errorNameTv.setText(item.name);
                errorHolder.errorDescTv.setText(item.desc);
            }
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClick(v, item);
            }
        });
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new ErrorHolder(LayoutInflater.from(mContext).inflate(R.layout.item_merchant_error, null));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class ErrorHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.error_report_rl)
        LinearLayout errorReportRl;
        @BindView(R.id.error_name_tv)
        TextView errorNameTv;
        @BindView(R.id.error_desc_tv)
        TextView errorDescTv;
        @BindView(R.id.error_cancel_tv)
        TextView errorCancelTv;

        ErrorHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
