package com.haodaibao.fanbeiapp.module.pay;

import android.text.TextUtils;
import android.widget.Toast;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Constant;
import com.baseandroid.config.Global;
import com.baseandroid.utils.DeviceUuid;
import com.baseandroid.utils.LogUtil;
import com.baseandroid.utils.RxUtils;
import com.baseandroid.utils.ToastUtils;
import com.haodaibao.fanbeiapp.event.PersonalFragmentEvent;
import com.haodaibao.fanbeiapp.repository.LifeRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.CouponInfo;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.LabelListResp;
import com.haodaibao.fanbeiapp.repository.json.LableInfo;
import com.haodaibao.fanbeiapp.repository.json.MerchantDetailResp;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;
import com.haodaibao.fanbeiapp.repository.json.MyCouponsResp;
import com.haodaibao.fanbeiapp.repository.json.PackageAllBean;
import com.haodaibao.fanbeiapp.repository.json.PayInfo;
import com.haodaibao.fanbeiapp.repository.json.PayInfoDetailBean;
import com.haodaibao.fanbeiapp.repository.json.PayProcessResp;
import com.qianbao.mobilecashier.PayResultCallBack;
import com.qianbao.mobilecashier.QBPaySDK;
import com.qianbao.mobilecashier.utils.PayCode;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

import static com.baseandroid.config.Constant.JIAOYICHENGGONG;
import static com.baseandroid.config.Constant.WEIZHIFU;
import static com.haodaibao.fanbeiapp.repository.RxObserver.checkJsonCode;

/**
 * Created by hdb on 2017/8/30.
 */

public class PayPresenter implements PayContract.Presenter {


    private PayContract.View view;
    private String orderno;
    private long num = 0;

    public PayPresenter(PayContract.View view) {
        this.view = view;
    }

    @Override
    public void checkMerchantPackageRule(String merchantno, String packageno) {
        HashMap<String, String> map = new HashMap<>();
        map.put("merchantno", merchantno);
        map.put("packageno", packageno);
        LifeRepository.getInstance().checkMerchantPackageRule(map)
                .compose(RxUtils.<Data<PackageAllBean>>applySchedulersLifeCycle((BaseActivity) view))
                .subscribe(new RxObserver<Data<PackageAllBean>>() {
                    @Override
                    public void onNext(Data<PackageAllBean> packageAllBeanData) {
                        if (checkJsonCode(packageAllBeanData, true)) {
                            view.checkedUI(packageAllBeanData.getResult().packageAuth);
                        }
                    }
                });
    }

    @Override
    public void getMerchantInfo(String merchantNo, final MerchantInfoEntiy merchantDetailResp) {
        view.showLoadingDialog();
        final HashMap<String, String> map = new HashMap<>();
        map.put("merchantno", merchantNo);
        Observable<Data<MyCouponsResp>> myCoupons = LifeRepository.getInstance().getCoupons(map)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Consumer<Data<MyCouponsResp>>() {
                    @Override
                    public void accept(@NonNull Data<MyCouponsResp> myCouponsRespData) throws Exception {
                        if (checkJsonCode(myCouponsRespData, true)) {
                            view.updateCouponList(myCouponsRespData.getResult());
                        }

                    }
                });
        Observable<Data<MerchantDetailResp>> merchant = LifeRepository.getInstance().getMerchantDetails(map, true)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Consumer<Data<MerchantDetailResp>>() {
                    @Override
                    public void accept(@NonNull Data<MerchantDetailResp> merchantDetailRespData) throws Exception {
                        if (checkJsonCode(merchantDetailRespData, true)) {
                            view.updateMerchantInfo(merchantDetailRespData.getResult().getMerchant());
                        } else {
                            if (TextUtils.equals("TRD90000", merchantDetailRespData.getStatus())) {
                                view.clolsePayUI();
                            }
                        }
                    }
                });
//        Observable<Data<MerchantDetailResp>> merchant = Observable.create(new ObservableOnSubscribe<Boolean>() {
//            @Override
//            public void subscribe(@NonNull ObservableEmitter<Boolean> e) throws Exception {
//                if (merchantDetailResp == null || TextUtils.isEmpty(merchantDetailResp.getActivityno())) {
//                    e.onNext(false);//发射商家详情
//                } else {
//                    e.onNext(true);//发射商家详情
//                }
//                e.onComplete();
//            }
//        }).filter(new Predicate<Boolean>() {
//            @Override
//            public boolean test(@NonNull Boolean e) throws Exception {//过滤商家详情，若为空，则发射。
//                return e;
//            }
//        }).flatMap(new Function<Boolean, ObservableSource<Data<MerchantDetailResp>>>() {
//            @Override
//            public ObservableSource<Data<MerchantDetailResp>> apply(@NonNull final Boolean e) throws Exception {//转换是个幌子，只是在这里进行网络访问操作
//                return LifeRepository.getInstance().getMerchantDetails(map, true)
//                        .subscribeOn(Schedulers.newThread())
//                        .observeOn(AndroidSchedulers.mainThread())
//                        .doOnNext(new Consumer<Data<MerchantDetailResp>>() {
//                            @Override
//                            public void accept(@NonNull Data<MerchantDetailResp> merchantDetailRespData) throws Exception {
//                                if (checkJsonCode(merchantDetailRespData, true)) {
//                                    view.updateMerchantInfo(merchantDetailRespData.getResult().getMerchant());
//                                } else {
//                                    if (TextUtils.equals("TRD90000", merchantDetailRespData.getStatus())) {
//                                        view.clolsePayUI();
//                                    }
//
//                                }
//
//                            }
//                        });
//            }
//        });

        HashMap<String, String> params = new HashMap<>();
        params.put("merchantNo", merchantNo);
        params.put("bhhAcessToken", Global.getUserInfo().bhhAcessToken);
        params.put("deviceCode", DeviceUuid.getDeviceId(Global.getContext()));
        Observable<Data<LabelListResp>> labelList = LifeRepository.getInstance().getMerchantActivityList(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Consumer<Data<LabelListResp>>() {
                    @Override
                    public void accept(@NonNull Data<LabelListResp> labelListResp) throws Exception {
                        if (checkJsonCode(labelListResp, true)) {
                            view.updateMerchantActivityList(labelListResp.getResult().getActivities());
                        }
                    }
                });

        Observable.merge(merchant, labelList, myCoupons)//两个请求并联，并不产生任何关系
                .subscribe(new RxObserver<Data<? extends Serializable>>() {
                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                        view.closeLoadingDialog();
                    }

                    @Override
                    public void onComplete() {
                        view.closeLoadingDialog();
                    }
                });
//
    }

    @Override
    public void createOrder(final String merchantno, String orderAmt, String notrefundamt, String couponNo, String couponAmt, String packageno, List<LableInfo> lableInfoList) {
        HashMap<String, String> map = new HashMap<>();
        map.put("merchantNo", merchantno);
        map.put("orderAmt", orderAmt);
        map.put("userNo", Global.getUserInfo().getUserno() == null ? "" : Global.getUserInfo().getUserno());
        map.put("notrefundamt", notrefundamt);
        if (!TextUtils.isEmpty(couponNo)) {
            map.put("couponParams[0].serialno", couponNo);
            map.put("couponParams[0].componAmt", couponAmt);
        }
        if (!TextUtils.isEmpty(packageno)) {
            map.put("packageno", packageno);
        }
        try {
            for (int i = 0; i < lableInfoList.size(); i++) {
                map.put("activityParams" + "[" + i + "]" + ".activityNo", lableInfoList.get(i).getActivityNo());
                map.put("activityParams" + "[" + i + "]" + ".labelName", lableInfoList.get(i).getLabelname());
            }
        } catch (Exception e) {

        }
        map.put("devicecode", DeviceUuid.getDeviceId(Global.getContext()));
        map.put("longitude", Global.getMyLocation().longitude + "");
        map.put("latitude", Global.getMyLocation().latitude + "");
        map.put("address", Global.getMyLocation().address + "");
        map.put("bhhAcessToken", Global.getUserInfo().bhhAcessToken);
        view.showLoadingDialog();
        LifeRepository.getInstance().createOrder(map)
                .compose(RxUtils.<Data<PayProcessResp>>applySchedulersLifeCycle((BaseActivity) view))
                .subscribe(new RxObserver<Data<PayProcessResp>>() {
                    @Override
                    public void onNext(@NonNull Data<PayProcessResp> payProcessRespData) {
                        view.closeLoadingDialog();
                        if (checkJsonCode(payProcessRespData, true)) {
                            PayProcessResp payProcessResp = payProcessRespData.getResult();
                            if (payProcessResp == null) {
                                ToastUtils.showShortToast("支付订单创建异常");
                                return;
                            }
                            PayInfo bhh = payProcessResp.getBhh();
                            if (bhh != null) {
                                String cashierId = payProcessResp.getBhh().getCashierId();
                                String cashierToken = payProcessResp.getBhh().getCashierToken();
                                String cashierEnv = payProcessResp.getBhh().getCashierEnv();
                                orderno = payProcessResp.getBhh().getOrderno();
                                PersonalFragmentEvent personalFragmentEvent = new PersonalFragmentEvent();
                                personalFragmentEvent.refreshCouponCount = true;
                                EventBus.getDefault().post(personalFragmentEvent);
                                /**
                                 * 调起收银台支付
                                 * */
                                showPaySdk(cashierId, cashierToken, cashierEnv);
                            } else if (payProcessResp.getActivityError() != null) {
                                PayProcessResp.ActivityError activityError = payProcessResp.getActivityError();
                                if (TextUtils.equals(activityError.getErrorCode(), "ACT500001")) {
                                    getMerchantActivity(merchantno);
                                    ToastUtils.showShortToast(activityError.getReason());
                                } else {
                                    ToastUtils.showShortToast("支付订单创建异常");
                                }
                            } else {
                                ToastUtils.showShortToast("支付订单创建异常");
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                        view.closeLoadingDialog();
                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();
                        view.closeLoadingDialog();
                    }
                });
    }

    /**
     * 轮询查询支付结果状态
     */
    private void checkPayResult() {
        final HashMap<String, String> map = new HashMap<>();
        map.put("orderno", orderno);
        view.showLoadingDialog();
        Observable.interval(0, 3, TimeUnit.SECONDS)
                .takeUntil(new Predicate<Long>() {
                    @Override
                    public boolean test(@NonNull Long aLong) throws Exception {
                        LogUtil.e("along=" + aLong);
                        return aLong >= 2;
                    }
                })
                .flatMap(new Function<Long, ObservableSource<Data<PayInfoDetailBean>>>() {
                    @Override
                    public ObservableSource<Data<PayInfoDetailBean>> apply(@NonNull Long aLong) throws Exception {
                        return LifeRepository.getInstance().getPayInfoDetail(map);
                    }
                })
                .takeUntil(new Predicate<Data<PayInfoDetailBean>>() {
                    @Override
                    public boolean test(@NonNull Data<PayInfoDetailBean> payInfoDetailBeanData) throws Exception {
                        LogUtil.e("getStatus=" + payInfoDetailBeanData.getResult().getStatus());
                        if (checkJsonCode(payInfoDetailBeanData, false)) {//ZHIFUCHENGGONG
                            return TextUtils.equals(Constant.ZHIFUCHENGGONG, payInfoDetailBeanData.getResult().getStatus()) || TextUtils.equals(WEIZHIFU, payInfoDetailBeanData.getResult().getStatus()) || TextUtils.equals(JIAOYICHENGGONG, payInfoDetailBeanData.getResult().getStatus());
                        } else {
                            return false;
                        }
                    }
                })
                .compose(RxUtils.<Data<PayInfoDetailBean>>applySchedulersLifeCycle((BaseActivity) view))
                .subscribe(new RxObserver<Data<PayInfoDetailBean>>() {
                    @Override
                    public void onNext(@NonNull Data<PayInfoDetailBean> payInfoDetailBeanData) {
                        if (checkJsonCode(payInfoDetailBeanData, true)) {
                            String statue = payInfoDetailBeanData.getResult().getStatus();
                            if (TextUtils.equals(statue, Constant.ZHIFUCHENGGONG) || TextUtils.equals(statue, Constant.JIAOYICHENGGONG)) {
                                view.zhiFuSuc(payInfoDetailBeanData.getResult(), orderno);
                            } else if (TextUtils.equals(statue, WEIZHIFU)) {
                                String msg = TextUtils.equals("", payInfoDetailBeanData.getResult().getHostreturnmessage()) ? "请重新支付" : payInfoDetailBeanData.getResult().getHostreturnmessage() + ",请重新支付";
                                view.zhiFuFailed(msg);
                            } else {
                                if (num == 2) {
                                    String msg = TextUtils.equals("", payInfoDetailBeanData.getResult().getHostreturnmessage()) ? "请稍后查询" : payInfoDetailBeanData.getResult().getHostreturnmessage() + "，请稍后查询";
                                    view.zhiFuing(msg);
                                    num = 0;
                                } else {
                                    num++;
                                }
                            }
                        }

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                        view.closeLoadingDialog();
                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();
                        view.closeLoadingDialog();
                    }
                });

    }

    private void cancelPayment(String orderno) {
        HashMap<String, String> map = new HashMap<>();
        map.put("orderno", orderno);
        LifeRepository.getInstance().cancelPayment(map)
                .compose(RxUtils.<Data<Object>>applySchedulersLifeCycle((BaseActivity) view))
                .subscribe(new RxObserver<Data<Object>>() {
                    @Override
                    public void onNext(Data<Object> objectData) {
                        super.onNext(objectData);
                        PersonalFragmentEvent personalFragmentEvent = new PersonalFragmentEvent();
                        personalFragmentEvent.refreshCouponCount = true;
                        EventBus.getDefault().post(personalFragmentEvent);
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                    }
                });
    }

    private void showPaySdk(String cashierId, String cashierToken, String cashierEnv) {
        LogUtil.i("QianBao+下单@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        //调起收银台
//			cashierId = "MjAxNzAzMjAwMDYwMTIwMDAwMDAzMzM1NnwxNDg5OTk5NDA0ODMw";
//			cashierToken = "8E9D7D515BE03C9A609C4F5410E51BAF";
        QBPaySDK.getInstance().callPaySDK(cashierId, cashierToken, cashierEnv, new PayResultCallBack() {

            @Override
            public void success(String code, String status) {
                LogUtil.i("支付结果：成功=================================");
                ToastUtils.showShortToast("支付成功");
                //查询订单支付状态
                checkPayResult();
            }

            @Override
            public void paying(String code, String status) {
                LogUtil.i("支付结果：支付中=================================");
                ToastUtils.showShortToast("支付中...");
                //查询订单支付状态
                checkPayResult();
            }

            @Override
            public void failed(String code, String message) {
                LogUtil.i("支付结果：失败=========错误码：" + code + "==========错误描述：" + message);
                if (TextUtils.equals(code, PayCode.NETERROR)) {
                    ToastUtils.showShortToast("网络超时");
                } else if (TextUtils.equals(code, PayCode.PAY_OUT)) {
                    //取消订单
                    cancelPayment(orderno);
                } else if (TextUtils.equals(code, PayCode.WEIXIN_ING)
                        || TextUtils.equals(code, PayCode.AILPAY_ING)
                        || TextUtils.equals(code, PayCode.BHH_ING)) {//处理中
                    //查询订单支付状态
                    checkPayResult();
                } else if (TextUtils.equals(code, PayCode.WEIXIN_ERROR)
                        || TextUtils.equals(code, PayCode.AILPAY_ERROR)
                        || TextUtils.equals(code, PayCode.BHH_ERROR)) {//支付失败
                    ToastUtils.showShortToast("支付失败", Toast.LENGTH_SHORT);
                    cancelPayment(orderno);
                } else if (TextUtils.equals(code, PayCode.PAY_NULL)) {//未开通支付工具

                } else {
                    cancelPayment(orderno);
                }

            }

            @Override
            public void cancel(String s) {
                cancelPayment(orderno);
            }


        });
    }

    /*
    * 判断集合是否存在某个字符串工具
    * */
    public static String isCityOpen(String cityCode, List<CouponInfo.CityInfo> cityInfos) {
        String string = "";
        if (cityInfos == null || cityInfos.size() == 0) {
            return "";
        }
        if (cityCode == null) {//扫一扫进入时，可能商户暂停合作未登录等异常拿不到citycode;
            cityCode = "";
        }
        for (int i = 0; i < cityInfos.size(); i++) {
            if (cityCode.startsWith(cityInfos.get(i).getCode())) {
                return "";
            }
            string = string + cityInfos.get(i).getName() + ((i == cityInfos.size() - 1) ? "" : "、");
        }
        return string;
    }

    /**
     * 按规则匹配当前支付金额可用的红包
     *
     * @param cashAmt 支付金额
     * @param cList   全部红包列表
     */
    public CouponInfo newmatchCoupon(double cashAmt, List<CouponInfo> cList, String cityCode) {
        if (cList == null || cList.size() == 0) {
            return null;
        }
        for (int i = 0; i < cList.size(); i++) {
            cList.get(i).setUserAmount(cashAmt + "");
            if (Double.parseDouble(cList.get(i).getUsebeginamt()) > cashAmt || TextUtils.equals("01", cList.get(i).getIsFailure()) || !TextUtils.equals("", isCityOpen(cityCode, cList.get(i).getCitys()))) {
                cList.get(i).setCanUse("1");
            } else {
                cList.get(i).setCanUse("0");
            }
        }
        Collections.sort(cList, new CompareCoupon());
        for (int i = 0; i < cList.size(); i++) {
            if (i == 0 && TextUtils.equals("0", cList.get(i).getCanUse())) {
                cList.get(i).setIsSelect("0");
            } else {
                cList.get(i).setIsSelect("1");
            }
        }
        if (TextUtils.equals("0", cList.get(0).getCanUse())) {
            return cList.get(0);
        }
        return null;
    }

    private void getMerchantActivity(String merchantNo) {
        HashMap<String, String> params = new HashMap<>();
        params.put("merchantNo", merchantNo);
        params.put("bhhAcessToken", Global.getUserInfo().bhhAcessToken);
        params.put("deviceCode", DeviceUuid.getDeviceId(Global.getContext()));
        LifeRepository.getInstance().getMerchantActivityList(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<LabelListResp>>applySchedulersLifeCycle((BaseActivity) view))
                .subscribe(new RxObserver<Data<LabelListResp>>() {
                    @Override
                    public void onNext(Data<LabelListResp> labelListResp) {
                        if (checkJsonCode(labelListResp, true)) {
                            view.updateMerchantActivityList(labelListResp.getResult().getActivities());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                    }
                });
    }
}
