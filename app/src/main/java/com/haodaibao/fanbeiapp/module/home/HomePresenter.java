package com.haodaibao.fanbeiapp.module.home;

import android.text.TextUtils;
import android.util.Log;

import com.baseandroid.base.BaseFragment;
import com.baseandroid.config.Constant;
import com.baseandroid.config.Global;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.LogUtil;
import com.baseandroid.utils.RxUtils;
import com.haodaibao.fanbeiapp.database.GreenDaoDbHelp;
import com.haodaibao.fanbeiapp.module.bizutils.CategoryAsc;
import com.haodaibao.fanbeiapp.repository.LifeRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.AdInfoResp;
import com.haodaibao.fanbeiapp.repository.json.AreaList;
import com.haodaibao.fanbeiapp.repository.json.CdaList;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.DistrictList;
import com.haodaibao.fanbeiapp.repository.json.LabelListResp;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoResp;
import com.haodaibao.fanbeiapp.repository.json.OpenCitysInfoResp;
import com.haodaibao.fanbeiapp.repository.json.PackageAllBean;
import com.haodaibao.fanbeiapp.repository.json.ProvinCityDistInfoResp;
import com.haodaibao.fanbeiapp.repository.json.ShanghufenleiInfo;
import com.haodaibao.fanbeiapp.repository.json.ShanghufenleiList;
import com.haodaibao.fanbeiapp.repository.json.ShanghufenleiResp;
import com.haodaibao.fanbeiapp.repository.json.ShangquanResp;
import com.haodaibao.fanbeiapp.repository.json.ShopKindListBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


public class HomePresenter implements HomeContract.Presenter {

    private HomeContract.View mView;

    public HomePresenter(HomeContract.View view) {
        this.mView = view;
    }

    @Override
    public void getOpenCitys() {
        final Map<String, String> hasMap = new HashMap<>();
        hasMap.put("open", "1");
        Observable<Data<OpenCitysInfoResp>> openCitysObserver = LifeRepository.getInstance()
                .getPubCitysByOpen(hasMap)
                .map(new Function<Data<OpenCitysInfoResp>, Data<OpenCitysInfoResp>>() {
                    @Override
                    public Data<OpenCitysInfoResp> apply(Data<OpenCitysInfoResp> openCitysInfoRespData) throws Exception {
                        Log.e("++++++", "111 Thread = " + Thread.currentThread().getName());
                        if (RxObserver.checkJsonCode(openCitysInfoRespData, false)) {
                            if (!CommonUtils.isSameObject(Global.getOpenCitysResp(), openCitysInfoRespData.getResult())) {
                                GreenDaoDbHelp.insertallCityInfo(openCitysInfoRespData.getResult().getSites());
                            }
                        }
                        return openCitysInfoRespData;
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Consumer<Data<OpenCitysInfoResp>>() {
                    @Override
                    public void accept(Data<OpenCitysInfoResp> openCitysRespData) throws
                            Exception {
                        Log.e("++++++", "222 Thread = " + Thread.currentThread().getName());
                        if (RxObserver.checkJsonCode(openCitysRespData, false)) {
                            if (!CommonUtils.isSameObject(Global.getOpenCitysResp(), openCitysRespData.getResult())) {
                                Global.setOpenCitysResp(openCitysRespData.getResult());
                            }
                        }
                    }
                });

        Map<String, String> hasMap2 = new HashMap<>();
        hasMap2.put("version", Global.getProvCityDistResp()
                .getResult() == null ? "" : Global.getProvCityDistResp()
                .getResult()
                .getVersion());
        Observable<Data<ProvinCityDistInfoResp>> provinceCityDistrictObserver = LifeRepository
                .getInstance()
                .getProvinceCityDistrict(hasMap2)
                .map(new Function<Data<ProvinCityDistInfoResp>, Data<ProvinCityDistInfoResp>>() {
                    @Override
                    public Data<ProvinCityDistInfoResp> apply(Data<ProvinCityDistInfoResp> provinCityDistInfoRespData) throws
                            Exception {
                        Log.e("++++++", "333 Thread = " + Thread.currentThread().getName());
                        if (RxObserver.checkJsonCode(provinCityDistInfoRespData, true)) {
                            if (provinCityDistInfoRespData.getResult().getResult() != null && provinCityDistInfoRespData.getResult().getResult()
                                    .getDistricts() != null && !CommonUtils.isSameObject(Global.getProvCityDistResp(), provinCityDistInfoRespData.getResult())) {
                                GreenDaoDbHelp.insertallDistrictsInfo(provinCityDistInfoRespData.getResult().getResult().getDistricts());
                            }
                        }
                        return provinCityDistInfoRespData;
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Consumer<Data<ProvinCityDistInfoResp>>() {
                    @Override
                    public void accept(@NonNull Data<ProvinCityDistInfoResp> provinCityDistInfoRespData) throws
                            Exception {
                        Log.e("++++++", "444 Thread = " + Thread.currentThread().getName());
                        Global.setSelectCityNameAndCodeByGaodeNameAdcode();
                        if (RxObserver.checkJsonCode(provinCityDistInfoRespData, true)) {
                            if (provinCityDistInfoRespData.getResult().getResult() != null && provinCityDistInfoRespData.getResult()
                                    .getResult().getDistricts() != null && !CommonUtils.isSameObject(Global.getProvCityDistResp(), provinCityDistInfoRespData.getResult())) {
                                Global.setProvCityDistResp(provinCityDistInfoRespData.getResult());
                            }
                        }
                    }
                });

        Observable.concat(openCitysObserver, provinceCityDistrictObserver)
                .compose(RxUtils.<Data>applySchedulersLifeCycle((BaseFragment) (mView)))
                .subscribe(new RxObserver<Data>() {
                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                        mView.onRetry();
                    }

                    @Override
                    public void onComplete() {
                        Global.setSelectCityNameAndCodeByGaodeNameAdcode();
                        mView.updateCityShanghuList();
                    }
                });
    }

    @Override
    public void getClassification(String siteType, String siteCode, String cityCode, String keyWord) {
        Map<String, String> hasMap = new HashMap<>();
        hasMap.put("cityCode", cityCode);
        hasMap.put("siteType", siteType);
        hasMap.put("siteCode", cityCode);
        hasMap.put("keyWord", keyWord);
        LifeRepository.getInstance()
                .getClassification(hasMap)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.newThread())
                .compose(RxUtils.<Data<ShanghufenleiResp>>bindToLifecycle((BaseFragment) (mView)))
                .subscribe(new RxObserver<Data<ShanghufenleiResp>>() {

                    @Override
                    public void onNext(@NonNull Data<ShanghufenleiResp> shanghufenleiRespData) {
                        if (checkJsonCode(shanghufenleiRespData, false)) {
                            parseCatagoryList(shanghufenleiRespData);
                        }
                    }
                });
    }

    @Override
    public void getShangquan(String siteType, String siteCode, String cityCode) {
        Map<String, String> hasMap = new HashMap<>();
        hasMap.put("citycode", cityCode);
        hasMap.put("siteType", siteType);
        hasMap.put("siteCode", cityCode);
        LifeRepository.getInstance()
                .getShangquan(hasMap)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.newThread())
                .compose(RxUtils.<Data<ShangquanResp>>bindToLifecycle((BaseFragment) (mView)))
                .subscribe(new RxObserver<Data<ShangquanResp>>() {

                    @Override
                    public void onNext(@NonNull Data<ShangquanResp> shangquanRespData) {
                        if (checkJsonCode(shangquanRespData, false) && shangquanRespData.getResult().getCdaList() != null) {
                            List<CdaList> cdaList = shangquanRespData.getResult().getCdaList();
                            if (cdaList.size() > 0) {
                                List<DistrictList> districtListOne = cdaList.get(0).getDistrictList();
                                if (Global.getMyLocation().getAddress().contains(Global.getSelectCity())) {
                                    List<AreaList> areaLists = new ArrayList<>();
                                    areaLists.add(new AreaList("00001", "附近（智能范围）", "0000", ""));
                                    areaLists.add(new AreaList("00002", "1km", "0000", ""));
                                    areaLists.add(new AreaList("00003", "2km", "0000", ""));
                                    areaLists.add(new AreaList("00004", "5km", "0000", ""));
                                    DistrictList districtList = new DistrictList("0000", "附近", Global.getSelectCityCode(), areaLists);
                                    districtListOne.add(0, districtList);
                                }
                                Global.setShangquans(districtListOne);
                                mView.onShangquan();
                            }
                        }
                    }
                });
    }

    @Override
    public void getShanghuListInfo(String cityCode, int pagerNumber, int pagerSize) {
        Map<String, String> hasMap = new HashMap<>();
        hasMap.put("page", "" + pagerNumber);
        hasMap.put("pageSize", "" + pagerSize);
        hasMap.put("longitude", "" + Global.getMyLocation().getLongitude());
        hasMap.put("latitude", "" + Global.getMyLocation().getLatitude());
        hasMap.put("citycode", cityCode);
        hasMap.put("sort", "distance.asc");
        LifeRepository.getInstance()
                .getShanghuListInfo(hasMap, false)
                .compose(RxUtils.<Data<MerchantInfoResp>>applySchedulersLifeCycle((BaseFragment) (mView)))
                .subscribe(new RxObserver<Data<MerchantInfoResp>>() {

                    @Override
                    public void onNext(@NonNull Data<MerchantInfoResp> merchantInfoRespData) {
                        if (RxObserver.checkJsonCode(merchantInfoRespData, true) && merchantInfoRespData
                                .getResult() != null) {
                            mView.onShanghuListInfo(merchantInfoRespData.getResult());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                        mView.onShanghuListInfoError();
                    }
                });
    }

    @Override
    public void getShanghuListInfo(boolean forcenetwork, String merchantname, String categoryno, String districtcode, String areacode, String sort, String refundmode, String isProduct, String isVoucher, String siteType, String siteCode, String cityCode, int pagerNumber, int pagerSize, String headMerchant, String tagNo) {
        Map<String, String> hasMap = new HashMap<>();
        hasMap.put("page", "" + pagerNumber);
        hasMap.put("pageSize", "" + pagerSize);
        String arearange = "";
        if (TextUtils.isEmpty(sort)) {
            sort = CategoryAsc.AI;
        }
        if (TextUtils.equals(districtcode, "0000")) {
            if (TextUtils.equals(areacode, "00001")) {
                arearange = "";
            } else if (TextUtils.equals(areacode, "00002")) {
                arearange = "1000";
            } else if (TextUtils.equals(areacode, "00003")) {
                arearange = "2000";
            } else {
                arearange = "5000";
            }
            hasMap.put("arearange", arearange);
            districtcode = "";
            areacode = "";
        }
        if (!TextUtils.isEmpty(merchantname)) {
            hasMap.put("merchantname", merchantname);// 商户名称
        }
        if (!TextUtils.isEmpty(categoryno)) {
            hasMap.put("categoryno", categoryno);// 商户类别
        }
        if (!TextUtils.isEmpty(districtcode)) {
            hasMap.put("districtcode", districtcode);// 区域
        }
        if (!TextUtils.isEmpty(areacode)) {
            hasMap.put("areacode", areacode);// 商圈
        }
        if (!TextUtils.isEmpty(sort)) {
            hasMap.put("sort", sort);// 排序条件
        }
        if (!TextUtils.isEmpty(refundmode)) {
            hasMap.put("refundmode", refundmode);// 返现商户
        }
        if (!TextUtils.isEmpty(isProduct)) {
            hasMap.put("discountProduct", isProduct);// 优惠套餐
        }
        if (!TextUtils.isEmpty(isVoucher)) {
            hasMap.put("voucher", isVoucher);// 优惠套餐
        }

        hasMap.put("siteType", siteType);
        hasMap.put("siteCode", siteCode);
        hasMap.put("citycode", cityCode);
        hasMap.put("longitude", "" + Global.getMyLocation().getLongitude());
        hasMap.put("latitude", "" + Global.getMyLocation().getLatitude());
        hasMap.put("headMerchant", headMerchant);
        if (TextUtils.equals(sort, CategoryAsc.AI) && !Global.getMyLocation()
                .isLocationSuccess()) {
            hasMap.put("tagno", Constant.TAGNO);
        }
        if (!TextUtils.isEmpty(tagNo)) {
            hasMap.put("tagno", tagNo);
        }

        LifeRepository.getInstance()
                .getShanghuListInfo(hasMap, forcenetwork)
                .compose(RxUtils.<Data<MerchantInfoResp>>applySchedulersLifeCycle((BaseFragment) (mView)))
                .subscribe(new RxObserver<Data<MerchantInfoResp>>() {

                    @Override
                    public void onNext(@NonNull Data<MerchantInfoResp> merchantInfoRespData) {
                        if (RxObserver.checkJsonCode(merchantInfoRespData, true) && merchantInfoRespData.getResult() != null) {
                            mView.onShanghuListInfo(merchantInfoRespData.getResult());
                            mView.onContent();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                        mView.onRetry();
                        mView.onShanghuListInfoError();
                    }
                });
    }

    @Override
    public void requestHomeInfo(String siteType, String siteCode, String cityCode, String adid, String adid2, int pagerNumber, int pagerSize, boolean forcenetwork) {
        Map<String, String> hasMap1 = new HashMap<>();
        hasMap1.put("cityCode", cityCode);
        hasMap1.put("siteType", siteType);
        hasMap1.put("siteCode", cityCode);
        hasMap1.put("organizationno", Constant.ORGNO);// 机构号
        hasMap1.put("positionid", adid);// 广告位id
        Observable<Data<AdInfoResp>> advertObserver1 = LifeRepository.getInstance()
                .getAdvertInfos(hasMap1)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Consumer<Data<AdInfoResp>>() {
                    @Override
                    public void accept(@NonNull Data<AdInfoResp> data) throws Exception {
                        if (RxObserver.checkJsonCode(data, true)) {
                            mView.onAdListInfo(data.getResult().getAdverts());
                        }
                    }
                });

        Map<String, String> hasMap2 = new HashMap<>();
        hasMap2.put("cityCode", cityCode);
        hasMap2.put("siteType", siteType);
        hasMap2.put("siteCode", cityCode);
        hasMap2.put("organizationno", Constant.ORGNO);// 机构号
        hasMap2.put("positionid", adid2);// 广告位id
        Observable<Data<AdInfoResp>> advertObserver2 = LifeRepository.getInstance()
                .getAdvertInfos(hasMap2)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Consumer<Data<AdInfoResp>>() {
                    @Override
                    public void accept(@NonNull Data<AdInfoResp> data) throws Exception {
                        if (RxObserver.checkJsonCode(data, true)) {
                            mView.onAdListInfo2(data.getResult().getAdverts());
                        }
                    }
                });

        Map<String, String> hasMap3 = new HashMap<>();
        hasMap3.put("page", "" + pagerNumber);
        hasMap3.put("pageSize", "" + pagerSize);
        hasMap3.put("longitude", "" + Global.getMyLocation().getLongitude());
        hasMap3.put("latitude", "" + Global.getMyLocation().getLatitude());
        hasMap3.put("citycode", cityCode);
        hasMap3.put("siteType", siteType);
        hasMap3.put("siteCode", cityCode);
        if (Global.getMyLocation().getLongitude() > 0) {
            hasMap3.put("sort", "distance.asc");
        } else if (!Global.getMyLocation().isLocationSuccess() && Global.getMyLocation()
                .getLongitude() < 0) {
            hasMap3.put("sort", "ai.asc");
            hasMap3.put("tagno", Constant.TAGNO);
        }
        hasMap3.put("allpackage", "1");
        Observable<Data<MerchantInfoResp>> shanghuObserver = LifeRepository.getInstance()
//        LifeRepository.getInstance()
                .getShanghuList(hasMap3)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
//                .compose(RxUtils.<Data<MerchantInfoResp>>applySchedulersLifeCycle((BaseFragment) (mView)))
                .doOnNext(new Consumer<Data<MerchantInfoResp>>() {
                    @Override
                    public void accept(@NonNull Data<MerchantInfoResp> data) throws Exception {
                        if (RxObserver.checkJsonCode(data, true)) {
                            mView.onShanghuListInfo(data.getResult());
                        }
                    }
                });
//                .subscribe(new RxObserver<Data<MerchantInfoResp>>() {
//                    @Override
//                    public void onError(@NonNull Throwable e) {
//                        super.onError(e);
//                        mView.onContent();
//                        mView.onHomeInfoRefresh();
//                    }
//
//                    @Override
//                    public void onComplete() {
//                        mView.onContent();
//                        mView.onHomeInfoRefresh();
//                    }
//                }
//                );

        Observable.concat(advertObserver1, advertObserver2, shanghuObserver)
                .compose(RxUtils.<Data>applySchedulersLifeCycle((BaseFragment) (mView)))
                .subscribe(new RxObserver<Data>() {
                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                        mView.onContent();
                        mView.onHomeInfoRefresh();
                    }

                    @Override
                    public void onComplete() {
                        mView.onContent();
                        mView.onHomeInfoRefresh();
                    }
                });

    }

    private void parseCatagoryList(@NonNull Data<ShanghufenleiResp> shanghufenleiRespData) {
        List<ShanghufenleiInfo> shanghufenleiList = shanghufenleiRespData.getResult()
                .getCategoryList();
        List<ShanghufenleiList> group = new ArrayList<>();
        if (shanghufenleiList.size() > 0) {
            // 取出全部父类
            for (int i = 0; i < shanghufenleiList.size(); i++) {
                String parentno1 = shanghufenleiList.get(i).getParentno();
                String categoryno1 = shanghufenleiList.get(i).getCategoryno();
                String name1 = shanghufenleiList.get(i).getName();
                String hot1 = shanghufenleiList.get(i).getHot();
                String status1 = shanghufenleiList.get(i).getStatus();
                if (parentno1.equals("ROOT")) {
                    List<ShanghufenleiInfo> infoList = new ArrayList<>();
                    ShanghufenleiList parentInfo = new ShanghufenleiList(categoryno1, name1, parentno1, hot1, status1, infoList);
                    group.add(parentInfo);
                }
            }
            for (int i = 0; i < group.size(); i++) {
                String parentCategoryno = group.get(i).getCategoryno();
                ShanghufenleiList info = group.get(i);
                List<ShanghufenleiInfo> infoList = info.getList();
                for (int j = 0; j < shanghufenleiList.size(); j++) {
                    // 如果当前父类编号为已存父类编号中的一个，则将此子分类加入该父类list中
                    if (parentCategoryno.equals(shanghufenleiList.get(j).getParentno())) {
                        infoList.add(shanghufenleiList.get(j));
                    }
                }
                info.setList(infoList);
                group.set(i, info);
            }
        }
        Global.setCategorys(group);
    }

    @Override
    public void getComboDetail(final MerchantInfoEntiy merchantInfoEntiy, String packageno) {
        HashMap<String, String> map = new HashMap<>();
        map.put("merchantno", merchantInfoEntiy.getMerchantno());
        map.put("packageno", packageno);
        LifeRepository.getInstance()
                .getPackageDetail(map)
                .compose(RxUtils.<Data<PackageAllBean>>applySchedulersLifeCycle((BaseFragment) (mView)))
                .subscribe(new RxObserver<Data<PackageAllBean>>() {
                    @Override
                    public void onNext(Data<PackageAllBean> packageAllBeanData) {
                        if (checkJsonCode(packageAllBeanData, true)) {
                            mView.onComboDetail(merchantInfoEntiy, packageAllBeanData.getResult());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        mView.onRetry();
                    }
                });
    }

    @Override
    public void getTopLabelList() {
        LifeRepository.getInstance()
                .getLebelList()
                .compose(RxUtils.<Data<LabelListResp>>applySchedulersLifeCycle((BaseFragment) (mView)))
                .subscribe(new RxObserver<Data<LabelListResp>>() {

                    @Override
                    public void onNext(Data<LabelListResp> lableInfoData) {
                        if (RxObserver.checkJsonCode(lableInfoData, true)) {
                            mView.onLebelList(lableInfoData.getResult().getLableInfo());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
//                        mView.onShanghuListInfoError();
                    }
                });
    }

    /**
     * 根据用户当前的位置获取商铺分类
     *
     * @param siteType      站点类型
     * @param siteCode      站点code
     * @param latitude      经度
     * @param longitude     维度
     * @param lastlatitude  历史经度
     * @param lastlongitude 历史维度
     * @param devicecode    iemi码
     */
    public void getShopKind(String siteType, String siteCode, String latitude
            , String longitude, String lastlatitude, String lastlongitude, String devicecode) {
        HashMap<String, String> parms = new HashMap<>();
        parms.put("siteType", siteType);
        parms.put("siteCode", siteCode);
        parms.put("latitude", latitude);
        parms.put("longitude", longitude);
        parms.put("lastlatitude", lastlatitude);
        parms.put("lastlongitude", lastlongitude);
        parms.put("devicecode", devicecode);

        LifeRepository.getInstance()
                .getShopKind(parms)
                .compose(RxUtils.<Data<ShopKindListBean>>applySchedulersLifeCycle((BaseFragment) (mView)))
                .subscribe(new RxObserver<Data<ShopKindListBean>>() {
                    @Override
                    public void onNext(Data<ShopKindListBean> packageAllBeanData) {
                        if (checkJsonCode(packageAllBeanData, true)) {
                            LogUtil.e(packageAllBeanData.getResult().toString());
                            mView.onShopKindsRefresh(packageAllBeanData.getResult());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        mView.onRetry();
                    }
                });
    }
}