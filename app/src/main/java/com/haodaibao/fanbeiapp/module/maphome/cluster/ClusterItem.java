package com.haodaibao.fanbeiapp.module.maphome.cluster;


import com.amap.api.maps.model.LatLng;
import com.haodaibao.fanbeiapp.module.maphome.cluster.demo.MarkerInfo;

/**
 * Created by yiyi.qi on 16/10/10.
 */

public interface ClusterItem {
    /**
     * 返回聚合元素的地理位置
     *
     * @return
     */
    LatLng getPosition();

    MarkerInfo getMarkerInfo();
}
