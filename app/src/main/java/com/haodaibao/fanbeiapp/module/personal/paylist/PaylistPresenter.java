package com.haodaibao.fanbeiapp.module.personal.paylist;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.RxUtils;
import com.haodaibao.fanbeiapp.repository.LifeRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.PaylistItemBean;
import com.haodaibao.fanbeiapp.repository.json.PaylistTopBean;

import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Routee on 2017/9/4.
 * description: ${cusor}
 */

class PaylistPresenter implements PaylistContract.Presenter {

    private PaylistContract.View mView;

    public PaylistPresenter(PaylistContract.View view) {
        this.mView = view;
    }

    @Override
    public void getTopData() {
        LifeRepository.getInstance().getPaylistTopData().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<PaylistTopBean>>applySchedulersLifeCycle((BaseActivity) mView))
                .subscribe(new RxObserver<Data<PaylistTopBean>>() {
                    @Override
                    public void onNext(@NonNull Data<PaylistTopBean> data) {
                        if (checkJsonCode(data, false)) {
                            mView.setResultTopView(data.getResult());
                        }
                    }
                });
    }

    @Override
    public void getRvData(Map<String, String> params) {
        params.put("pageSize", "5");
        params.put("bsncode", "1001");
        params.put("orderstatus", "12,20");
        LifeRepository.getInstance().getPaylistRvData(params).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<PaylistItemBean>>applySchedulersLifeCycle((BaseActivity) mView))
                .subscribe(new RxObserver<Data<PaylistItemBean>>() {
                    @Override
                    public void onNext(@NonNull Data<PaylistItemBean> data) {
                        if (checkJsonCode(data, false)) {
                            mView.setResultRvView(data.getResult());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                    }
                });
    }
}
