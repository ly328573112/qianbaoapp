package com.haodaibao.fanbeiapp.module.invoice.detail;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.utils.RxUtils;
import com.haodaibao.fanbeiapp.repository.LifeRepository;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.InvoiceDetailBean;

import java.util.HashMap;

/**
 * date: 2018/2/24.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class InvoicePresenter implements InvoiceDetailContract.Presenter {
    private InvoiceDetailContract.View mView;

    public InvoicePresenter(InvoiceDetailContract.View mView) {
        this.mView = mView;
    }

    @Override
    public void getInvoiceDetail(String id) {
        HashMap<String, String> map = new HashMap<>();
        map.put("id", id);
        LifeRepository.getInstance().getInvoiceDetail(map)
                .compose(RxUtils.<Data<InvoiceDetailBean>>applySchedulersLifeCycle((BaseActivity) mView))
                .subscribe(new RxObserver<Data<InvoiceDetailBean>>() {
                    @Override
                    public void onNext(Data<InvoiceDetailBean> invoiceDetailBeanData) {
                        super.onNext(invoiceDetailBeanData);
                        mView.updateUI(invoiceDetailBeanData.getResult().invoice);
                    }
                });

    }
}
