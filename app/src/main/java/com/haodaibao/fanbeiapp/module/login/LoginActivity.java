package com.haodaibao.fanbeiapp.module.login;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Global;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.FormatUtil;
import com.baseandroid.utils.SpannableStringUtils;
import com.baseandroid.utils.StatusBarHelper;
import com.baseandroid.widget.customtext.edittextview.EditTextWithDelNormal;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.event.HomeActivityEvent;
import com.haodaibao.fanbeiapp.event.MerchantEvent;
import com.haodaibao.fanbeiapp.event.PersonalFragmentEvent;
import com.haodaibao.fanbeiapp.module.dialog.MessageDialog;
import com.haodaibao.fanbeiapp.webview.WebviewActivity;
import com.qianbao.shiningwhitelibrary.BHHManager;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import hugo.weaving.DebugLog;

import static com.baseandroid.base.BaseWebviewActivity.WEBVIEW_TITLE;
import static com.baseandroid.base.BaseWebviewActivity.WEBVIEW_URL;

@DebugLog
public class LoginActivity extends BaseActivity implements View.OnClickListener, LoginContract.View {

    private final static String Tag = LoginActivity.class.getSimpleName();

    private LoginPresenter mLoginPresenter;

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar_back)
    View toolbarBack;
    @BindView(R.id.et_phone)
    EditTextWithDelNormal etPhone;
    @BindView(R.id.et_auth_code)
    EditTextWithDelNormal etAuthCode;
    @BindView(R.id.bt_auth_code)
    TextView btAuthCode;
    @BindView(R.id.bt_login_num)
    TextView btLoginNum;
    @BindView(R.id.div_line)
    ImageView div_line;
    @BindView(R.id.btn_voice_auth)
    TextView btn_voice_auth;
    @BindView(R.id.voice_auth_ll)
    LinearLayout voice_auth_ll;

    @BindView(R.id.tv_cut)
    CheckBox tvCut;
    @BindView(R.id.tv_cut1)
    TextView tvCut1;
    private String bhh;

    private MessageDialog voiceTipsDialog;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(LoginActivity.this);
        mLoginPresenter = new LoginPresenter(this);

        toolbarTitle.setText(R.string.titile_login);

        toolbarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goneInputMethodManager();
                toolbarBack.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ActivityJumpUtil.goBack(LoginActivity.this);
                    }
                }, 100);
            }
        });

        etPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (TextUtils.isEmpty(s.toString())) {
                    return;
                }
                FormatUtil.addEditSpace(s, start, before, etPhone);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etAuthCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                btLoginNum.setEnabled(s.toString()
//                        .length() > 3 && tvCut.isChecked());
                if (s.toString().length() > 3 && tvCut.isChecked()) {
                    btLoginNum.setEnabled(true);
                    login();
                } else {
                    btLoginNum.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        tvCut.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String s = etAuthCode.getText().toString();
                if (isChecked) {
                    btLoginNum.setEnabled((s.length() > 3));
                } else {
                    btLoginNum.setEnabled(false);
                }
            }
        });

        btAuthCode.setOnClickListener(this);
        btLoginNum.setOnClickListener(this);
        tvCut1.setOnClickListener(this);
        btn_voice_auth.setOnClickListener(this);

        etPhone.setText(Global.getUsrePhotoNumber());

        hiddenVoiceAuthTips();

        etPhone.postDelayed(new Runnable() {
            @Override
            public void run() {
                forceShowSoftInput(etPhone);
            }
        }, 100);

//        SpannableString str = new SpannableString(tvCut1.getText());
//        tvCut1.setMovementMethod(LinkMovementMethod.getInstance());
//
//        str.setSpan(new Clickable(clickListener),0,10, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//        str.setSpan(new Clickable(click) ,11,str.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//        tvCut1.setText(str);
    }

    private void goneInputMethodManager() {
        try {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(LoginActivity.this
                    .getCurrentFocus()
                    .getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        Intent intent = getIntent();
        bhh = intent.getStringExtra("Bhh");
    }

    private void showVoiceDialog(final String phoneNum) {
        if (voiceTipsDialog == null) {
            voiceTipsDialog = MessageDialog.newInstance();
        }
        if (!voiceTipsDialog.isVisible()) {
            voiceTipsDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    voiceTipsDialog.hideTitle();
                    voiceTipsDialog.setContent(getString(R.string.voice_auth_call_1) +
                            getString(R.string.voice_auth_phone_num) + getString(R.string.voice_auth_call_2));
                    voiceTipsDialog.setConfirmText(getString(R.string.voice_auth_confirm));
                    voiceTipsDialog.setConfirmOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mLoginPresenter.requestVmsCode(phoneNum);
                            voiceTipsDialog.dismiss();
                        }
                    });
                    voiceTipsDialog.setCancelOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            voiceTipsDialog.dismiss();
                        }
                    });
                }
            });

            voiceTipsDialog.show(getSupportFragmentManager(), "voiceTipsDialog");
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.bt_auth_code:
                if (CommonUtils.isMobile(etPhone.getText()
                        .toString()
                        .replaceAll(" ", ""))) {
                    String phoneNum = etPhone.getText().toString().replaceAll(" ", "");
                    mLoginPresenter.requestSmsCode(phoneNum);
                    etAuthCode.setFocusable(true);
                    etAuthCode.setFocusableInTouchMode(true);
                    etAuthCode.requestFocus();
                    forceShowSoftInput(etAuthCode);
                } else {
                    showToast(getString(R.string.login_input_mobile_tip));
                }
                break;

            case R.id.tv_cut1:
//                SpannableString str = new SpannableString(tvCut1.getText());
//                tvCut1.setMovementMethod(LinkMovementMethod.getInstance());
//
//                str.setSpan(new Clickable(clickListener),0,10, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//                str.setSpan(new Clickable(click) ,10,str.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//                tvCut1.setText(str);
                Intent intent = new Intent(LoginActivity.this, WebviewActivity.class);
                intent.putExtra(WEBVIEW_URL, "file:///android_asset/register_agreement.html");
                intent.putExtra(WEBVIEW_TITLE, "钱包生活用户协议");
                startActivity(intent);
                break;

            case R.id.bt_login_num:
                login();
//                goneInputMethodManager();
//                String phoneNum = etPhone.getText().toString().replaceAll(" ", "");
//                String verifiCode = etAuthCode.getText().toString();
//                mLoginPresenter.requestLogin(Global.getSelectCitySiteType(), Global.getSelectCityCode(), phoneNum, verifiCode);
                break;
            case R.id.btn_voice_auth:
                if (CommonUtils.isMobile(etPhone.getText()
                        .toString()
                        .replaceAll(" ", ""))) {
                    String phone = etPhone.getText().toString().replaceAll(" ", "");
//                    mLoginPresenter.requestVmsCode(phone);
                    showVoiceDialog(phone);

                } else {
                    showToast(getString(R.string.login_input_mobile_tip));
                }
                break;

            default:
                break;
        }
    }

    private View.OnClickListener click = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(LoginActivity.this, WebviewActivity.class);
            intent.putExtra(WEBVIEW_URL, "file:///android_asset/member_agreement.html");
            intent.putExtra(WEBVIEW_TITLE, "钱包会员服务协议");
            startActivity(intent);
        }
    };
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(LoginActivity.this, WebviewActivity.class);
            intent.putExtra(WEBVIEW_URL, "file:///android_asset/register_agreement.html");
            intent.putExtra(WEBVIEW_TITLE, "钱包生活用户协议");
            startActivity(intent);
        }
    };

    /**
     * 强制显示软键盘
     *
     * @param view 接受软键盘输入的视图
     */
    private void forceShowSoftInput(View view) {
        InputMethodManager imm = ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE));
        if (imm != null && view != null) {
            imm.showSoftInput(view, InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    /**
     * 登录
     */
    private void login() {
        goneInputMethodManager();
        String phoneNum = etPhone.getText().toString().replaceAll(" ", "");
        String verifiCode = etAuthCode.getText().toString();
        mLoginPresenter.requestLogin(Global.getSelectCitySiteType(), Global.getSelectCityCode(), phoneNum, verifiCode);

    }

    @Override
    public void enableValidateCodeButton() {
        btAuthCode.setEnabled(true);
        btAuthCode.setTextColor(ContextCompat.getColor(this, R.color.color_red));
        div_line.setBackgroundColor(ContextCompat.getColor(this, R.color.color_red));
    }

    @Override
    public void disableValidateCodeButton() {
        btAuthCode.setTextColor(ContextCompat.getColor(this, R.color.c_cccccc));
        btAuthCode.setEnabled(false);
        div_line.setBackgroundColor(ContextCompat.getColor(this, R.color.c_eaeaea));

    }

    /**
     * 显示语音验证提示
     */
    @Override
    public void showVoiceAuthTips() {
        voice_auth_ll.setVisibility(View.VISIBLE);
    }

    /**
     * 隐藏语音验证提示
     */
    @Override
    public void hiddenVoiceAuthTips() {
        voice_auth_ll.setVisibility(View.GONE);
    }

    @Override
    public void countValidateCodeButton(long number) {
        btAuthCode.setTextColor(ContextCompat.getColor(this, R.color.c_cccccc));
        div_line.setBackgroundColor(ContextCompat.getColor(this, R.color.c_eaeaea));
        btAuthCode.setText("重新发送(" + number + ")");
    }

    @Override
    public void resendValidateCodeButton() {
        btAuthCode.setEnabled(true);
        btAuthCode.setTextColor(ContextCompat.getColor(this, R.color.color_red));
        div_line.setBackgroundColor(ContextCompat.getColor(this, R.color.color_red));
        btAuthCode.setText("重新发送");
    }

    @Override
    public void OnLoginInfo() {
        PersonalFragmentEvent event = new PersonalFragmentEvent();
        event.setRefreshUserInfo(true);
        event.setRefreshBhhItem(true);
        EventBus.getDefault().post(event);
        if (!TextUtils.isEmpty(Global.getLoginInfo().coupon.activityImageUrl)) {
            HomeActivityEvent homeEvent = new HomeActivityEvent();
            EventBus.getDefault().post(homeEvent);
        }
        EventBus.getDefault().post(new MerchantEvent());
        if (TextUtils.equals(bhh, "Bhh")) {
            BHHManager.openViewForRelease(LoginActivity.this, Global.getUserInfo().bhhAcessToken, Global.getUserInfo().getUserno(), false);
            btAuthCode.postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            }, 100);
        } else {
            finish();
        }
    }

    class Clickable extends ClickableSpan {
        private final View.OnClickListener mListener;

        public Clickable(View.OnClickListener l) {
            mListener = l;
        }
        /**
         * 重写父类点击事件
         */
        @Override
        public void onClick(View v) {
            mListener.onClick(v);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);// 设置文字下划线不显示
            ds.setColor(getResources().getColor(R.color.map_text_color6));// 设置字体颜色
        }
    }
}
