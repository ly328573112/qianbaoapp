package com.haodaibao.fanbeiapp.module.search.adpter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Constant;
import com.baseandroid.config.Global;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.database.GreenDaoDbHelp;
import com.haodaibao.fanbeiapp.event.CitySelectChangEvent;
import com.haodaibao.fanbeiapp.module.search.view.StickyRecyclerHeadersAdapter;
import com.haodaibao.fanbeiapp.repository.json.OpenCityBean;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class CityListadapter extends BaseRecycleViewAdapter<OpenCityBean> implements StickyRecyclerHeadersAdapter<RecyclerView.ViewHolder>, IndexAdapter {

    public CityListadapter(Context mContext) {
        super(mContext);
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, final OpenCityBean item) {
        if (holder instanceof LocationCityHolder) {
            LocationCityHolder locationCityHolder = (LocationCityHolder) holder;
            if (GreenDaoDbHelp.loadallCityInfo().size() > 0 && GreenDaoDbHelp.loadallDistrictsInfo().size() > 0 &&
                    GreenDaoDbHelp.queryCityByGaodeNameAdCode().list().size() > 0) {
                List<OpenCityBean> openCityBeans = GreenDaoDbHelp.queryCityByGaodeNameAdCode().list();
                if (openCityBeans.size() > 0) {
                    locationCityHolder.cityName.setText(openCityBeans.get(0).getName());
                    item.setName(openCityBeans.get(0).getName());
                    item.setCode(openCityBeans.get(0).getCode());
                    item.setType(openCityBeans.get(0).getType());
                }
                locationCityHolder.tipsLy.setVisibility(View.GONE);
                locationCityHolder.itemView.setEnabled(true);
            } else {
                if (TextUtils.isEmpty(Global.getMyLocation().getCityName())) {
                    locationCityHolder.cityName.setText(Constant.SELECTCITY);
                    locationCityHolder.tipsLy.setVisibility(View.GONE);
                } else {
                    locationCityHolder.cityName.setText(Global.getMyLocation().getCityName());
                    if (Global.getMyLocation().getCityName().equals(Constant.SELECTCITY)){
                        locationCityHolder.tipsLy.setVisibility(View.GONE);
                    }else {
                        locationCityHolder.tipsLy.setVisibility(View.VISIBLE);
                    }
                }
                locationCityHolder.itemView.setEnabled(false);
            }

            locationCityHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                     /*--城市切换选择---*/
                    Global.setLastSelectCity(Global.getSelectCity());
                    Global.setSelectCity(item.getName());
                    Global.setSelectCityCode(item.getCode());
                    Global.setSelectCitySiteType(item.getType());
                    CitySelectChangEvent changEvent = new CitySelectChangEvent();
                    List<OpenCityBean> openCityBeans = GreenDaoDbHelp.queryCityByGaodeNameAdCode().list();
                    if (openCityBeans.size() > 0) {
                        if (openCityBeans.get(0).getCode().equals(item.getCode())) {
                            changEvent.currentLocation = true;
                        }
                    } else {
                        changEvent.currentLocation = false;
                    }
                    EventBus.getDefault().post(changEvent);
                    ((BaseActivity) mContext).finish();
                }
            });

        } else if (holder instanceof CityHolder) {
            CityHolder cityHolder = (CityHolder) holder;
            cityHolder.cityName.setText(item.getName());
            cityHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*--城市切换选择---*/
                    Global.setLastSelectCity(Global.getSelectCity());
                    Global.setSelectCity(item.getName());
                    Global.setSelectCityCode(item.getCode());
                    Global.setSelectCitySiteType(item.getType());
                    CitySelectChangEvent changEvent = new CitySelectChangEvent();
                    List<OpenCityBean> openCityBeans = GreenDaoDbHelp.queryCityByGaodeNameAdCode().list();
                    if (openCityBeans.size() > 0) {
                        if (openCityBeans.get(0).getCode().equals(item.getCode())) {
                            changEvent.currentLocation = true;
                        }
                    } else {
                        changEvent.currentLocation = false;
                    }
                    EventBus.getDefault().post(changEvent);
                    ((BaseActivity) mContext).finish();
                }
            });
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0:
            default:
                View view = LayoutInflater.from(mContext).inflate(R.layout.item_location_citylist, parent, false);
                return new LocationCityHolder(view);
            case 1:
                View view2 = LayoutInflater.from(mContext).inflate(R.layout.item_citylist, parent, false);
                return new CityHolder(view2);
        }
    }

    @Override
    protected int getBaseItemViewType(int position) {
        if (position == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public long getHeaderId(int position) {
        if (position == 0) {
            return 0;
        }
        return getItem(position).getSortLetters().charAt(0);
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.city_header, parent, false);
        return new HeaderHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
        TextView textView = (TextView) holder.itemView;
        if (position == 0) {
            textView.setText(R.string.city_location_name);
        } else {
            textView.setText(getItem(position).getSortLetters());
        }
    }

    class HeaderHolder extends RecyclerView.ViewHolder {
        HeaderHolder(View itemView) {
            super(itemView);
        }
    }

    class LocationCityHolder extends RecyclerView.ViewHolder {
        TextView cityName;
        LinearLayout tipsLy;

        LocationCityHolder(View itemView) {
            super(itemView);
            cityName = itemView.findViewById(R.id.location_city_name);
            tipsLy = itemView.findViewById(R.id.tips_ly);
        }
    }

    class CityHolder extends RecyclerView.ViewHolder {
        TextView cityName;

        CityHolder(View itemView) {
            super(itemView);
            cityName = itemView.findViewById(R.id.city_name);
        }
    }

    @Override
    public OpenCityBean getItem(int position) {
        return getData().get(position);
    }
}
