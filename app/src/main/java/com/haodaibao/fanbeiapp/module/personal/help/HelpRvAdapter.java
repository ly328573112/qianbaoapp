package com.haodaibao.fanbeiapp.module.personal.help;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.HelpBean;
import com.haodaibao.fanbeiapp.repository.json.HelpInterface;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Routee on 2017/9/26.
 * description: ${cusor}
 */

public class HelpRvAdapter extends BaseRecycleViewAdapter<HelpInterface> {
    public HelpRvAdapter(Context context) {
        super(context);
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, HelpInterface item) {
        VH vh = (VH) holder;
        if (item instanceof HelpBean.HelpListBean) {
            HelpBean.HelpListBean bean = (HelpBean.HelpListBean) item;
            vh.mLlHelpTitle.setVisibility(View.VISIBLE);
            vh.mLlHelpContent.setVisibility(View.GONE);
            vh.mTvHelpTitle.setText(bean.getTopicName());
            vh.mTvHelpContent.setText("");
        } else if (item instanceof HelpBean.HelpListBean.HelpContentListBean) {
            final HelpBean.HelpListBean.HelpContentListBean bean = (HelpBean.HelpListBean.HelpContentListBean) item;
            vh.mLlHelpTitle.setVisibility(View.GONE);
            vh.mLlHelpContent.setVisibility(View.VISIBLE);
            vh.mTvHelpTitle.setText("");
            vh.mTvHelpContent.setText(bean.getTitle());
            vh.mLlHelpContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, HelpDetailActivity.class);
                    i.putExtra("title", bean.getTitle());
                    i.putExtra("content", bean.getContent());
                    i.putExtra("contenttype", bean.getContenttype());
                    mContext.startActivity(i);
                }
            });
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_item_help, parent, false);
        return new VH(view);
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class VH extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_help_title)
        TextView     mTvHelpTitle;
        @BindView(R.id.ll_help_title)
        LinearLayout mLlHelpTitle;
        @BindView(R.id.tv_help_content)
        TextView     mTvHelpContent;
        @BindView(R.id.ll_help_content)
        LinearLayout mLlHelpContent;

        public VH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
