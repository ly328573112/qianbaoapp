package com.haodaibao.fanbeiapp.module.personal.message;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.utils.FormatUtil;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.MessageListBean;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Routee on 2017/9/21.
 * description: ${cusor}
 */

public class MessageRvAdapter extends BaseRecycleViewAdapter<MessageListBean.ResultBean.ItemsBean> {

    private boolean isEdit;
    private Onclick mListen;

    public MessageRvAdapter(Context context) {
        super(context);
    }

    public MessageRvAdapter(Context context, boolean isEdit) {
        this(context);
        this.isEdit = isEdit;
    }

    @Override
    protected void onBindBaseViewHolder(final RecyclerView.ViewHolder holder, MessageListBean.ResultBean.ItemsBean item) {
        VH hold = (VH) holder;
        hold.setItme(item);
    }

    private String getTypeName(String type) {
        if (TextUtils.isEmpty(type)) {
            return "消息";
        }
        //消息类型, ALL：所有类型，01:提醒消息，02:优惠消息，03:系统消息
        switch (type) {
            case "01":
                return "提醒消息";
            case "02":
                return "优惠消息";
            case "03":
                return "系统消息";
            default:
                return "消息";
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_message, parent, false);
        return new VH(view);
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    public void setEditable(boolean editable) {
        isEdit = editable;
    }

    //获取选中消息msgNo
    public String getCheckedMsg() {
        StringBuilder sb = new StringBuilder();
        for (MessageListBean.ResultBean.ItemsBean bean : mData) {
            if (bean.isChecked()) {
                sb.append(bean.getMessageno() + ",");
            }
        }
        String s = sb.toString();
        if (s.endsWith(",")) {
            s = s.substring(0, s.length());
        }
        return s;
    }

    //设为已读
    public void setReaded() {
        for (MessageListBean.ResultBean.ItemsBean bean : mData) {
            if (bean.isChecked()) {
                bean.setReadstatus("1");
                bean.setChecked(false);
            }
        }
        notifyDataSetChanged();
    }

    //删除消息
    public void deleteMsg() {
        for (int i = 0; i < mData.size(); i++) {
            if (mData.get(i).isChecked()) {
                mData.remove(i--);
            }
        }
        notifyDataSetChanged();
    }

    //删除已被删除消息
    public void removeData(String msgNo) {
        for (int i = 0; i < mData.size(); i++) {
            if (msgNo.contains(mData.get(i).getMessageno())) {
                mData.remove(i--);
            }
        }
        notifyDataSetChanged();
    }

    //取消标记已标记为已读的消息
    public void readData(String msgNo) {
        for (int i = 0; i < mData.size(); i++) {
            if (msgNo.contains(mData.get(i).getMessageno())) {
                mData.get(i).setReadstatus("1");
            }
        }
        notifyDataSetChanged();
    }

    class VH extends RecyclerView.ViewHolder {
        @BindView(R.id.cb_message_select)
        CheckBox       mCbMessageSelect;
        @BindView(R.id.rl_message_select)
        RelativeLayout mRlMessageSelect;
        @BindView(R.id.tv_title)
        TextView       mTvTitle;
        @BindView(R.id.iv_notify_point)
        ImageView      mIvNotifyPoint;
        @BindView(R.id.tv_content)
        TextView       mTvContent;
        @BindView(R.id.tv_time)
        TextView       mTvTime;
        @BindView(R.id.rl_item)
        RelativeLayout mRlItem;
        private MessageListBean.ResultBean.ItemsBean mItme;

        public VH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mCbMessageSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (mItme != null) {
                        mItme.setChecked(isChecked);
                    }
                }
            });
            mRlItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mItme == null) {
                        return;
                    } else {
                        if (mListen == null) {
                            return;
                        }
                        mListen.itemClick(mItme);
                    }
                }
            });
        }

        public void setItme(MessageListBean.ResultBean.ItemsBean item) {
            mItme = item;
            mTvTitle.setText(getTypeName(item.getType()));
            mTvContent.setText(item.getTitle());
            mTvTime.setText(FormatUtil.getCurrentDate(item.getEffectivetime()));
            mIvNotifyPoint.setVisibility(item.getReadstatus().equals("0") ? View.VISIBLE : View.GONE);
            if (isEdit) {
                mRlMessageSelect.setVisibility(View.VISIBLE);
            } else {
                mRlMessageSelect.setVisibility(View.GONE);
            }
            if (item.isChecked()) {
                mCbMessageSelect.setChecked(true);
            } else {
                mCbMessageSelect.setChecked(false);
            }
        }
    }

    interface Onclick {
        void itemClick(MessageListBean.ResultBean.ItemsBean bean);
    }

    public void setOnClickListen(Onclick listen) {
        this.mListen = listen;
    }
}
