package com.haodaibao.fanbeiapp.module.merchant;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.baseandroid.config.Global;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.login.LoginActivity;
import com.haodaibao.fanbeiapp.module.merchant.adapter.MerchantErrorAdapter;
import com.haodaibao.fanbeiapp.repository.json.ErrorRecoveryList;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import java.util.List;

/**
 * 开发者：LuoYi
 * Time: 2017 16:38 2017/11/28 11
 */

public class MerchantErrorView implements MerchantContract.MerchantError, BaseRecycleViewAdapter.OnItemClickListener {

    private Context context;
    private BottomSheetDialog sheetDialog;
    private String merchantno;
    private MerchantDataPresenter merchantDataPresenter;

    MerchantErrorView(Context context, MerchantDataPresenter merchantDataPresenter) {
        this.context = context;
        this.merchantDataPresenter = merchantDataPresenter;
    }

    @Override
    public void initErrorDialog(List<ErrorRecoveryList.ErrorResult> errorRecoveryList, String merchantno) {
        this.merchantno = merchantno;
        RelativeLayout rl = new RelativeLayout(context);
        rl.setBackgroundColor(setErrorColor(R.color.c_F1F1F1));
        RecyclerView recyclerView = new RecyclerView(context);
        RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        MerchantErrorAdapter merchantErrorAdapter = new MerchantErrorAdapter(context, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(merchantErrorAdapter);
        rl.addView(recyclerView, relativeParams);

        merchantErrorAdapter.addData(errorRecoveryList);

        sheetDialog = new BottomSheetDialog(context);
        sheetDialog.setContentView(rl);
        sheetDialog.show();
    }

    private int setErrorColor(int color) {
        return context.getResources().getColor(color);
    }

    @Override
    public <T> void onItemClick(View view, T t) {
        ErrorRecoveryList.ErrorResult errorResult = (ErrorRecoveryList.ErrorResult) t;
        if (TextUtils.equals("0", errorResult.code)) {
            sheetDialog.dismiss();
        } else {
            if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                ActivityJumpUtil.next((Activity) context, LoginActivity.class, new Bundle(), 0);
            } else {
                merchantDataPresenter.merchantErrorRecovery(merchantno, errorResult.code);
            }
            sheetDialog.dismiss();
        }
    }
}
