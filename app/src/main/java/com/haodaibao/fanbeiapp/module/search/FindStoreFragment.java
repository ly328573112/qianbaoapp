package com.haodaibao.fanbeiapp.module.search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseFragment;
import com.baseandroid.base.BasePopupWindow;
import com.baseandroid.config.Global;
import com.baseandroid.utils.StringUtils;
import com.baseandroid.utils.UiUtils;
import com.baseandroid.widget.animatorprovider.FadeAlphaAnimatorProvider;
import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.baseandroid.widget.recycleadpter.CustomPtrHeader;
import com.github.nukc.stateview.StateView;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.database.GreenDaoDbHelp;
import com.haodaibao.fanbeiapp.event.CitySelectChangEvent;
import com.haodaibao.fanbeiapp.event.LocationSwitchEvent;
import com.haodaibao.fanbeiapp.event.OpencitysRequestEvent;
import com.haodaibao.fanbeiapp.event.ShanghufenleiRequestEvent;
import com.haodaibao.fanbeiapp.event.ShangquanRequestEvent;
import com.haodaibao.fanbeiapp.module.Consent;
import com.haodaibao.fanbeiapp.module.home.HomeContract;
import com.haodaibao.fanbeiapp.module.home.HomePresenter;
import com.haodaibao.fanbeiapp.module.home.HomeRefreshAdapter;
import com.haodaibao.fanbeiapp.module.home.adapter.ClassificationOfShopAdapter;
import com.haodaibao.fanbeiapp.module.home.temporary.HomeTemporary;
import com.haodaibao.fanbeiapp.module.maphome.MaphomeActivity;
import com.haodaibao.fanbeiapp.module.merchant.MerchantDetailActivity;
import com.haodaibao.fanbeiapp.module.merchant.temporarydata.MerchantTemporary;
import com.haodaibao.fanbeiapp.module.shangquantab.NearbyTabView;
import com.haodaibao.fanbeiapp.repository.json.AdInfo;
import com.haodaibao.fanbeiapp.repository.json.LableInfo;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoEntiy;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoResp;
import com.haodaibao.fanbeiapp.repository.json.MyLocation;
import com.haodaibao.fanbeiapp.repository.json.NearbyRequest;
import com.haodaibao.fanbeiapp.repository.json.OpenCityBean;
import com.haodaibao.fanbeiapp.repository.json.PackageAllBean;
import com.haodaibao.fanbeiapp.repository.json.ShopKindListBean;
import com.haodaibao.fanbeiapp.repository.json.ShopKindsBean;
import com.haodaibao.fanbeiapp.repository.json.YaoheListBean;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;
import com.tencent.stat.StatService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;


/**
 * 开发者：LuoYi
 * Time: 2018 13:59 2018/3/6 03
 */

public class FindStoreFragment extends BaseFragment implements BaseRecycleViewAdapter.OnItemClickListener, BaseRecycleViewAdapter.OnLoadMoreListener, HomeContract.View, View.OnClickListener {

    public final static int PAGE_SIZE = 20;
    public final static int PAGE_PER_SIZE = 5;

    @BindView(R.id.search_rl)
    RelativeLayout searchRl;
    @BindView(R.id.bt_left)
    TextView btLeft;
    @BindView(R.id.rcv_list_id)
    RecyclerView listIdRcv;
    @BindView(R.id.ptr_fresh_id)
    PtrClassicFrameLayout freshIdPtr;
    StateView stateView;
    @BindView(R.id.test_fram)
    FrameLayout testFram;
    @BindView(R.id.home_appbar)
    AppBarLayout homeAppbar;
    @BindView(R.id.home_coll_layout)
    CollapsingToolbarLayout homeCollLayout;

    @BindView(R.id.rl_title_close)
    RelativeLayout titleCloseRl;
    @BindView(R.id.home_scan_rl)
    RelativeLayout homeScanRl;
    @BindView(R.id.home_search_rl)
    RelativeLayout homeSearchRl;
    @BindView(R.id.ll_nearby_select)
    LinearLayout nearbySelectLl;
    @BindView(R.id.cb_catagory_id)
    CheckBox cbCatagoryId;
    @BindView(R.id.cb_scope_id)
    CheckBox cbScopeId;
    @BindView(R.id.saoyisao)
    ImageView saoyisao;
    @BindView(R.id.title_ly)
    RelativeLayout titleLy;
    @BindView(R.id.home_map_rl)
    RelativeLayout homeMapRl;
    @BindView(R.id.cb_sortways_id)
    CheckBox cbSortwaysId;
    @BindView(R.id.icon_search)
    ImageView iconSearch;
    @BindView(R.id.pop_parent)
    View pop_parent;
    int mCurentPage = 1;
    String headMerchant = "";
    private int mVerticalOffset;
    private boolean move = false;
    private int screenHight;
    private HomeRefreshAdapter mHomeRefreshAdapter;
    private HomePresenter mHomePresenter;
    private LinearLayoutManager linearLayoutManager;
    private NearbyTabView homeNearbyView;
    private NearbyRequest nearbyR;
    private HomeTemporary homeTemporary;

    private LinearLayout mYaoHeNearbyLayout;

    private String cityName = "";
    private String cityCode = "";
    private String citySiteType = "";

    ArrayList<MerchantTemporary> arrayList;
    private List<String> tagNoList;
    private String tagNo;
    private boolean isSkip = false ;

    @Override
    protected int getLayoutId() {
        return R.layout.home_find_store_layout;
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        arrayList = new ArrayList<>();
        tagNoList = new ArrayList<>();
        mHomePresenter.getOpenCitys();
    }

    @Override
    protected void setupView() {
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);

        screenHight = wm.getDefaultDisplay().getHeight();
        EventBus.getDefault().register(this);
        mHomePresenter = new HomePresenter(this);
        mHomePresenter.getTopLabelList();
        homeNearbyView = new NearbyTabView(getActivity(), this);
        nearbyR = new NearbyRequest();

        homeTemporary = new HomeTemporary();
        setNavicationProject();

        nearbySelectLl.setVisibility(View.GONE);

        CustomPtrHeader customPtrHeader = new CustomPtrHeader(getActivity());
        freshIdPtr.setHeaderView(customPtrHeader);
        freshIdPtr.addPtrUIHandler(customPtrHeader);
        freshIdPtr.disableWhenHorizontalMove(true);
        freshIdPtr.setPtrHandler(ptrDefaultHandler);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        listIdRcv.setLayoutManager(linearLayoutManager);
        mHomeRefreshAdapter = new HomeRefreshAdapter(getActivity(), this);
        mHomeRefreshAdapter.addData(homeTemporary);
        mHomeRefreshAdapter.setOnLoadMoreListener(this, listIdRcv);
        listIdRcv.setAdapter(mHomeRefreshAdapter);

        mYaoHeNearbyLayout = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.item_linshi, null);

        initStateView();
        initListener();
        cbCatagoryId.setOnCheckedChangeListener(listener);
        cbScopeId.setOnCheckedChangeListener(listener);
        cbSortwaysId.setOnCheckedChangeListener(listener);
    }

    CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (buttonView.getId()) {
                case R.id.cb_catagory_id:
                    if (isChecked) {
                        cbScopeId.setChecked(false);
                        cbSortwaysId.setChecked(false);
                        moveToPosition(1);
                        homeNearbyView.onCatagoryView(cbCatagoryId, pop_parent);
                        StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_all", null);
                    }
                    break;

                case R.id.cb_scope_id:
                    if (isChecked) {
                        cbCatagoryId.setChecked(false);
                        cbSortwaysId.setChecked(false);
                        moveToPosition(1);
                        homeNearbyView.onScopeView(cbScopeId, pop_parent);
                        StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_nearby", null);
                    }
                    break;

                case R.id.cb_sortways_id:
                    if (isChecked) {
                        cbScopeId.setChecked(false);
                        cbCatagoryId.setChecked(false);
                        moveToPosition(1);
                        homeNearbyView.onSortwaysView(cbSortwaysId, pop_parent);
                        StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_intelligence", null);
                    }
                    break;
            }
            if (!(cbCatagoryId.isChecked() || cbScopeId.isChecked() || cbSortwaysId.isChecked())) {
                homeNearbyView.disMiss();
                cbCatagoryId.setEnabled(false);
                cbScopeId.setEnabled(false);
                cbSortwaysId.setEnabled(false);
                cbCatagoryId.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            cbCatagoryId.setEnabled(true);
                            cbScopeId.setEnabled(true);
                            cbSortwaysId.setEnabled(true);
                        } catch (Exception e) {

                        }
                    }
                }, 360);
            }
        }
    };

    private void initStateView() {
        stateView = StateView.inject(testFram);
        stateView.setRetryResource(R.layout.base_retry);
        stateView.showContent();
        stateView.setAnimatorProvider(new FadeAlphaAnimatorProvider());
        stateView.setOnRetryClickListener(new StateView.OnRetryClickListener() {
            @Override
            public void onRetryClick() {
                if (Consent.currentLocation) {
                    mHomeRefreshAdapter.loactionPositioning();
                } else {
                    mHomePresenter.getShanghuListInfo(true, nearbyR.getKeyword(),//
                            nearbyR.getCategoryno(),//
                            nearbyR.getDistrictcode(),//
                            nearbyR.getAreacode(),//
                            nearbyR.getSort(),//
                            nearbyR.getRefundmode(),//
                            nearbyR.getIsProduct(),//
                            nearbyR.getIsVoucher(),//
                            citySiteType,//
                            cityCode,//
                            cityCode,//
                            mCurentPage, PAGE_SIZE, headMerchant, tagNo);
                }
            }
        });
    }

    private void initListener() {
        searchRl.setOnClickListener(this);
        btLeft.setOnClickListener(this);
        homeMapRl.setOnClickListener(this);
        homeScanRl.setOnClickListener(this);
        homeSearchRl.setOnClickListener(this);
        cbCatagoryId.setOnClickListener(this);
        cbScopeId.setOnClickListener(this);
        cbSortwaysId.setOnClickListener(this);

        homeAppbar.addOnOffsetChangedListener(offsetChangedListener);
        listIdRcv.addOnScrollListener(onScrollListener);
    }

    PtrDefaultHandler ptrDefaultHandler = new PtrDefaultHandler() {
        @Override
        public void onRefreshBegin(final PtrFrameLayout frame) {
            mCurentPage = 1;
            headMerchant = "";
            if (tagNoList != null) {
                tagNoList.clear();
            }
            tagNo = "";
            mHomePresenter.getTopLabelList();
            List<OpenCityBean> openCityBeans = GreenDaoDbHelp.queryCityByGaodeNameAdCode().list();
            if (openCityBeans.size() > 0 && openCityBeans.get(0).getCode().equals(cityCode)) {
                Consent.currentLocation = true;
            } else {
                Consent.currentLocation = false;
                mHomeRefreshAdapter.resetLocationList(false);
            }
            if (Consent.currentLocation) {
                mHomeRefreshAdapter.loactionPositioning();
            } else {
                mHomePresenter.getShanghuListInfo(true, nearbyR.getKeyword(),//
                        nearbyR.getCategoryno(),//
                        nearbyR.getDistrictcode(),//
                        nearbyR.getAreacode(),//
                        nearbyR.getSort(),//
                        nearbyR.getRefundmode(),//
                        nearbyR.getIsProduct(),//
                        nearbyR.getIsVoucher(),//
                        citySiteType,//
                        cityCode,//
                        cityCode,//
                        mCurentPage, PAGE_SIZE, headMerchant, tagNo);
            }
        }

        @Override
        public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
            return mVerticalOffset >= 0 && PtrDefaultHandler.checkContentCanBePulledDown(frame, content, header);
        }
    };

    AppBarLayout.OnOffsetChangedListener offsetChangedListener = new AppBarLayout.OnOffsetChangedListener() {
        @Override
        public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
            mVerticalOffset = verticalOffset;
        }
    };


    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (0 != linearLayoutManager.findFirstVisibleItemPosition()) {
                nearbySelectLl.setVisibility(View.VISIBLE);
            } else {
                nearbySelectLl.setVisibility(View.GONE);
            }

            if (move) {
                if (nearbySelectLl.getId() == R.id.ll_nearby_select) {
                    move = false;
                    int n = linearLayoutManager.findFirstVisibleItemPosition();
                    if (0 <= n && n < listIdRcv.getChildCount()) {
                        int top = listIdRcv.getChildAt(n).getTop();
                        listIdRcv.scrollBy(0, top);
                    }
                }
            }
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            //解决华为手机滑动时，图片不收缩问题，替换原来的在xml文件中设置behavior方法
            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                int visiblePosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                if (visiblePosition == 0) {
                    homeAppbar.setExpanded(true, true);
                }
            }
        }
    };


    @Override
    public void onLoadMore() {
        getMerchantList();
    }

    @Override
    public void onYaoheListInfo(List<YaoheListBean.YaoheBean> yaoheBeanList) {
    }

    @Override
    public void onAdListInfo(final List<AdInfo> adInfoList) {
    }

    @Override
    public void onAdListInfo2(List<AdInfo> adInfoList) {

    }

    @Override
    public void onShanghuListInfo(MerchantInfoResp merchantInfoResp) {
        if (mCurentPage == 1 && TextUtils.isEmpty(headMerchant)) {
            if (arrayList != null) {
                arrayList.clear();
            }
            arrayList.add(homeTemporary);
            arrayList.addAll(merchantInfoResp.getMerchantList());

            if (merchantInfoResp.getMerchantList().size() < PAGE_PER_SIZE) {
                for (int i = 0; i < 1 + (screenHight - UiUtils.dp2px(getContext(), 174) - (UiUtils
                        .dp2px(getContext(), 100) * merchantInfoResp.getMerchantList()
                        .size())) / UiUtils.dp2px(getContext(), 100); i++) {
                    arrayList.add(new MerchantTemporary() {
                        @Override
                        public int getmType() {
                            return 2;
                        }
                    });
                }
                mHomeRefreshAdapter.resetData(arrayList);
                freshIdPtr.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mHomeRefreshAdapter.loadMoreEnd(true);
                    }
                }, 100);

            } else {
                mHomeRefreshAdapter.resetData(arrayList);
            }
            mHomeRefreshAdapter.removeAllHeaderView();
            mHomeRefreshAdapter.addHeaderView(mYaoHeNearbyLayout);
            mHomeRefreshAdapter.notifyDataSetChanged();

            freshIdPtr.refreshComplete();
        } else {
            mHomeRefreshAdapter.addData(merchantInfoResp.getMerchantList());
            if (merchantInfoResp.getMerchantList().size() < PAGE_PER_SIZE) {
                mHomeRefreshAdapter.loadMoreEnd(true);
                return;
            } else if (merchantInfoResp.getMerchantList().size() < PAGE_SIZE) {
                mHomeRefreshAdapter.setLoadEndContent("没有更多店铺了~");
                mHomeRefreshAdapter.loadMoreEnd(false);
                return;
            }
            mHomeRefreshAdapter.loadMoreComplete();
        }
        mCurentPage = Integer.parseInt(merchantInfoResp.getPage());
        headMerchant = merchantInfoResp.getHeadMerchant();
    }

    @Override
    public void onShanghuListInfoError() {
        if (mCurentPage == 1 && TextUtils.isEmpty(headMerchant)) {
            ArrayList<MerchantTemporary> arrayList = new ArrayList<>();
            arrayList.add(new HomeTemporary());
            mHomeRefreshAdapter.resetData(arrayList);
            freshIdPtr.refreshComplete();
        } else {
            listIdRcv.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!getActivity().isFinishing()) {
                        mHomeRefreshAdapter.loadMoreFail();
                    }
                }
            }, 200);
        }
    }

    @Override
    public void onHomeInfoRefresh() {
        if (mCurentPage == 1 && TextUtils.isEmpty(headMerchant)) {
            freshIdPtr.refreshComplete();
        }
        mHomeRefreshAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.home_search_rl:
                Intent intent = new Intent(getActivity(), MaphomeActivity.class);
                startActivity(intent);
                break;
            case R.id.search_rl:
                Global.setSelectCityCode(cityCode);
                startActivity(new Intent(getActivity(), SearchActivity.class));
                StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_search", null);
                break;

            case R.id.bt_left:
                startActivity(new Intent(getActivity(), CityListActivity.class));
                StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_citylist", null);
                break;

            case R.id.home_map_rl:
                startActivity(new Intent(getActivity(), MaphomeActivity.class));
                break;
            default:
                break;
        }
    }

    @Override
    public <T> void onItemClick(View view, T t) {
        if (t == null) {
            return;
        }
        if (t instanceof MerchantInfoEntiy) {
            MerchantInfoEntiy infoEntiy = (MerchantInfoEntiy) t;
            Bundle bundle = new Bundle();
            bundle.putString("merchantno", infoEntiy.getMerchantno());
            ActivityJumpUtil.next(getActivity(), MerchantDetailActivity.class, bundle, 0);
            StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_item", null);
        } else if (t instanceof String) {
            String homeNearby = (String) t;
            switch (homeNearby) {
                case HomeRefreshAdapter.HOME_CATAGORY:
                default:
                    cbCatagoryId.setChecked(true);
                    cbScopeId.setChecked(false);
                    cbSortwaysId.setChecked(false);
                    homeAppbar.setExpanded(false);
                    StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_all", null);
                    break;
                case HomeRefreshAdapter.HOME_SCOPE:
                    cbCatagoryId.setChecked(false);
                    cbScopeId.setChecked(true);
                    cbSortwaysId.setChecked(false);
                    homeAppbar.setExpanded(false);
                    StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_nearby", null);
                    break;
                case HomeRefreshAdapter.HOME_SORTWAYS:
                    cbCatagoryId.setChecked(false);
                    cbScopeId.setChecked(false);
                    cbSortwaysId.setChecked(true);
                    homeAppbar.setExpanded(false);
                    StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_intelligence", null);
                    break;
            }

            moveToPosition(1);
        } else if (t instanceof Boolean) {
            homeNearbyView.onNearbyLocation("findC");
            StatService.trackCustomKVEvent(getActivity(), "qbsh_home_list_location", null);
        } else if (t instanceof LableInfo) {
            LableInfo lableInfo = (LableInfo) t;
            if (lableInfo.isLabelSelected()) {
                tagNoList.add(lableInfo.getLabelno());
            } else if (tagNoList.size() > 0) {
                for (String tagno : tagNoList) {
                    if (TextUtils.equals(tagno, lableInfo.getLabelno())) {
                        tagNoList.remove(lableInfo.getLabelno());
                        break;
                    }
                }
            }
            tagNo = StringUtils.ListToString(tagNoList);
            if (!TextUtils.isEmpty(tagNo)) {
                tagNo = tagNo.substring(0, tagNo.length() - 1);
            }
            mCurentPage = 1;
            headMerchant = "";
            mHomeRefreshAdapter.notifyItemRangeChanged(2, 2, "Neraby");
            getMerchantList();
        }
    }

    private void moveToPosition(int n) {
        int firstItem = linearLayoutManager.findFirstVisibleItemPosition();
        int lastItem = linearLayoutManager.findLastVisibleItemPosition();
        if (firstItem > 1) {
            return;
        }
        if (n <= firstItem) {
            listIdRcv.scrollToPosition(n);
        } else if (n <= lastItem) {
            int top = listIdRcv.getChildAt(n - firstItem).getTop();
            listIdRcv.scrollBy(0, top);
        } else {
            listIdRcv.scrollToPosition(n);
            move = true;
        }
    }

    @Override
    public void updateCityShanghuList() {
        if (TextUtils.isEmpty(Global.getSelectCity())) {
            return;
        }

        btLeft.setText(Global.getSelectCity());
        mCurentPage = 1;
        headMerchant = "";

        requestHomeData();
    }

    @Override
    public void onRetry() {
        stateView.postDelayed(new Runnable() {
            @Override
            public void run() {
                stateView.showRetry();
            }
        }, 200);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onContent() {
        stateView.showContent();
    }

    @Override
    public void onNearbySelect(NearbyRequest nearbyRequest) {
        mCurentPage = 1;
        headMerchant = "";
        if (nearbyRequest == null) {
            return;
        }
        nearbyR = nearbyRequest;
        switch (nearbyRequest.getNearbyType()) {
            case 0:
            default:
                cbCatagoryId.setChecked(false);
                cbCatagoryId.setText(nearbyRequest.getShowText());
                homeTemporary.catagoryStr = nearbyRequest.getShowText();
                break;
            case 1:
                cbScopeId.setChecked(false);
                cbScopeId.setText(nearbyRequest.getShowText1());
                homeTemporary.scopeStr = nearbyRequest.getShowText1();
                break;
            case 2:
                cbSortwaysId.setChecked(false);
                cbSortwaysId.setText(nearbyRequest.getShowText2());
                homeTemporary.sortwaysStr = nearbyRequest.getShowText2();
                break;
        }
        mHomeRefreshAdapter.notifyItemRangeChanged(2, 2, "Neraby");
        getMerchantList();
    }

    @Override
    public void onStopLocation() {
        mHomeRefreshAdapter.resetLocationList(true);
    }

    @Override
    public void onShangquan() {

    }

    @Override
    public void onComboDetail(MerchantInfoEntiy merchantInfoEntiy, PackageAllBean packageAllBean) {

    }

    @Override
    public void onLebelList(List<LableInfo> lableInfoList) {
        homeTemporary.lableList = lableInfoList;
//        mHomeRefreshAdapter.notifyDataSetChanged();
    }

    @Override
    public void onShopKindsRefresh(ShopKindListBean result) {

    }

    private void getMerchantList() {
        mHomePresenter.getShanghuListInfo(false,
                nearbyR.getKeyword(),//
                nearbyR.getCategoryno(),//
                nearbyR.getDistrictcode(),//
                nearbyR.getAreacode(),//
                nearbyR.getSort(),//
                nearbyR.getRefundmode(),//
                nearbyR.getIsProduct(),//
                nearbyR.getIsVoucher(),//
                citySiteType, //
                cityCode, //
                cityCode,//
                mCurentPage, PAGE_SIZE, headMerchant, tagNo);
    }

    private void requestHomeData() {
        mHomePresenter.getClassification(Global.getSelectCitySiteType(), Global.getSelectCityCode(), Global.getSelectCityCode(), "");
        mHomePresenter.getShangquan(Global.getSelectCitySiteType(), Global.getSelectCityCode(), Global.getSelectCityCode());
        getMerchantList();
    }

    private void refreshHomeData() {
        mHomePresenter.getClassification(Global.getSelectCitySiteType(), Global.getSelectCityCode(), Global.getSelectCityCode(), "");
        mHomePresenter.getShangquan(Global.getSelectCitySiteType(), Global.getSelectCityCode(), Global.getSelectCityCode());
        getMerchantList();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(OpencitysRequestEvent event) {
        mHomePresenter.getOpenCitys();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ShanghufenleiRequestEvent event) {
        mHomePresenter.getClassification(Global.getSelectCitySiteType(), Global.getSelectCityCode(), Global.getSelectCityCode(), "");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ShangquanRequestEvent event) {
        mHomePresenter.getShangquan(Global.getSelectCitySiteType(), Global.getSelectCityCode(), Global.getSelectCityCode());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(LocationSwitchEvent event) {
        if (homeNearbyView != null) {
            homeNearbyView.flushPop();
        }
        if (event.locationType.equals("allC") || event.locationType.equals("findC")) {
            Consent.currentLocation = event.cLocation;
            cityName = Global.getSelectCity();
            cityCode = Global.getSelectCityCode();
            citySiteType = Global.getSelectCitySiteType();
            if (event.locationDialog) {
                if (!isSkip && nearbyR != null) {
                    nearbyR.cleanNearby();
                }
                isSkip = false ;
                setNavicationTab();
                updateCityShanghuList();
            } else {
                freshIdPtr.refreshComplete();
            }
            mHomeRefreshAdapter.resetLocationList(event.cLocation);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ShopKindsBean event) {
        mHomeRefreshAdapter.loactionPositioning();
        isSkip = true ;
        if (GreenDaoDbHelp.loadallCityInfo().size() > 0 && GreenDaoDbHelp.loadallDistrictsInfo().size() > 0 &&
                GreenDaoDbHelp.queryCityByGaodeNameAdCode().list().size() > 0) {
            List<OpenCityBean> openCityBeans = GreenDaoDbHelp.queryCityByGaodeNameAdCode().list();
            if (openCityBeans.size() > 0) {
                cityName = openCityBeans.get(0).getName() ;
                citySiteType = openCityBeans.get(0).getType();
                cityCode = openCityBeans.get(0).getCode() ;
                Global.setSelectCity(cityName);
                Global.setSelectCityCode(cityCode);
                Global.setSelectCitySiteType(citySiteType);
            }
        }
        if (homeNearbyView != null) {
            homeNearbyView.flushPop();
        }
        if (tagNoList != null) {
            tagNoList.clear();
        }
        tagNo = "";
        mCurentPage = 1;
        headMerchant = "";
        homeNearbyView.onReductionTabSelect();

        if (nearbyR != null) {
            nearbyR.cleanNearby();
        }
        nearbyR.setCategoryno("1".equals(event.getCategorytype())
                ? event.getCategoryno() : event.getSubcategoryno());
        nearbyR.setNearbyType(Integer.valueOf(event.getCategorytype()));
        nearbyR.setShowText(event.getName());
        btLeft.setText(cityName);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CitySelectChangEvent event) {
        if (homeNearbyView != null) {
            homeNearbyView.flushPop();
        }
        if (tagNoList != null) {
            tagNoList.clear();
        }
        tagNo = "";
        mCurentPage = 1;
        headMerchant = "";
        homeNearbyView.onReductionTabSelect();
        Consent.currentLocation = event.currentLocation;
        cityName = Global.getSelectCity();
        cityCode = Global.getSelectCityCode();
        citySiteType = Global.getSelectCitySiteType();
        setNavicationProject();
        mHomeRefreshAdapter.resetLocationList(event.currentLocation);
        if (nearbyR != null) {
            nearbyR.cleanNearby();
        }
        mHomePresenter.getTopLabelList();
        updateCityShanghuList();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        BasePopupWindow.destroy();
    }

    private void setNavicationTab() {
        MyLocation location = Global.getMyLocation();
        if (TextUtils.isEmpty(location.getCityName())) {
            homeTemporary.scopeStr = "全城区域";
        } else {
            if (location.getAddress().contains(Global.getSelectCity())) {
                homeTemporary.scopeStr = "附近区域";
            } else {
                homeTemporary.scopeStr = "全城区域";
            }
        }
        homeTemporary.locationStr = "你当前在：" + location.getAddress();
        cbCatagoryId.setText("全部类型");
        cbScopeId.setText(homeTemporary.scopeStr);
        cbSortwaysId.setText("智能排序");
        homeTemporary.catagoryStr = "全部类型";
        homeTemporary.sortwaysStr = "智能排序";
        btLeft.setText(Global.getSelectCity());

    }

    private void setNavicationProject() {
        homeTemporary.catagoryStr = "全部类型";
        if (TextUtils.isEmpty(Global.getMyLocation().getCityName())) {
            homeTemporary.scopeStr = "全城区域";
        } else {
            if (Global.getMyLocation().getAddress().contains(Global.getSelectCity())) {
                homeTemporary.scopeStr = "附近区域";
            } else {
                homeTemporary.scopeStr = "全城区域";
            }
        }
        homeTemporary.locationStr = "你当前在：" + Global.getMyLocation().getAddress();
        homeTemporary.sortwaysStr = "智能排序";

        cbCatagoryId.setText(homeTemporary.catagoryStr);
        cbScopeId.setText(homeTemporary.scopeStr);
        cbSortwaysId.setText(homeTemporary.sortwaysStr);
        btLeft.setText(Global.getSelectCity());
    }

    @Override
    public void onPause() {
        cbCatagoryId.setChecked(false);
        cbScopeId.setChecked(false);
        cbSortwaysId.setChecked(false);
        homeNearbyView.onReductionTabSelect();
        super.onPause();
    }
}
