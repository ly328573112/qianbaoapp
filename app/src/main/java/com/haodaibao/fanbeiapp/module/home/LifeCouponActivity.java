package com.haodaibao.fanbeiapp.module.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Global;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.personal.coupon.CouponActivity;

import butterknife.BindView;

/**
 * 开发者：LuoYi
 * Time: 2017 10:25 2017/8/14 08
 */

public class LifeCouponActivity extends BaseActivity {

    @BindView(R.id.iv_life_coupon)
    ImageView lifeCouponIv;
    @BindView(R.id.iv_life_vertical)
    ImageView lifeVerticalIv;
    @BindView(R.id.iv_life_cancle)
    ImageView lifeCancleIv;
    private String couponImgUrlFile;

    public static void open(Activity activity, String couponImgUrlFile) {
        Bundle b = new Bundle();
        b.putString("couponImgUrlFile", couponImgUrlFile);
        Intent i = new Intent(Global.getContext(), LifeCouponActivity.class);
        i.putExtras(b);
        activity.startActivity(i);
    }

    @Override
    protected int getLayoutId() {
        Window window = getWindow();
        WindowManager.LayoutParams layoutParams = window.getAttributes();
        //设置窗口的大小及透明度
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
//        layoutParams.alpha = 0.5f;
        window.setAttributes(layoutParams);

        return R.layout.dialog_life_coupon;
    }

    @Override
    protected void setupView() {
        lifeCouponIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Global.getContext(), CouponActivity.class);
                LifeCouponActivity.this.startActivity(i);
                finish();
            }
        });
        lifeCancleIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        Intent intent = getIntent();
        couponImgUrlFile = intent.getStringExtra("couponImgUrlFile");
        Bitmap bitmap = BitmapFactory.decodeFile(couponImgUrlFile);
        if (bitmap == null) {
            finish();
            return;
        }
        lifeCouponIv.setImageBitmap(bitmap);
        matchAll(lifeCouponIv);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void matchAll(ImageView imageView) {
        int width, height;//ImageView调整后的宽高
        //获取屏幕宽高
        WindowManager manager = (WindowManager) LifeCouponActivity.this.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(metrics);
        int sWidth = metrics.widthPixels;
        int sHeight = metrics.heightPixels;
        //获取图片宽高
        Drawable drawable = imageView.getDrawable();
        int dWidth = drawable.getIntrinsicWidth();
        int dHeight = drawable.getIntrinsicHeight();

        //屏幕宽高比,一定要先把其中一个转为float
        float sScale = (float) sWidth / sHeight;
        //图片宽高比
        float dScale = (float) dWidth / dHeight;
        /*
        缩放比
        如果sScale>dScale，表示在高相等的情况下，控屏幕比较宽，这时候要适应高度，缩放比就是两则的高之比，图片宽度用缩放比计算
        如果sScale<dScale，表示在高相等的情况下，图片比较宽，这时候要适应宽度，缩放比就是两则的宽之比，图片高度用缩放比计算
         */
        float scale = 1.0f;
        if (sScale > dScale) {
            scale = (float) dHeight / sHeight;
            height = sHeight;//图片高度就是屏幕高度
            width = (int) (dWidth * scale);//按照缩放比算出图片缩放后的宽度
        } else if (sScale < dScale) {
            scale = (float) dWidth / sWidth;
            width = sWidth;
            height = (int) (dHeight / scale);//这里用除
        } else {
            //最后两者刚好比例相同，其实可以不用写，刚好充满
            width = sWidth;
            height = sHeight;
        }
        //重设ImageView宽高
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, height);
        imageView.setLayoutParams(params);
        //这样就获得了一个既适应屏幕有适应内部图片的ImageView，不用再纠结该给ImageView设定什么尺寸合适了
    }
}
