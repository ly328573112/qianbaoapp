package com.haodaibao.fanbeiapp.module.personal;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Global;
import com.baseandroid.retrofit.OkHttpClientManager;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.FormatUtil;
import com.baseandroid.widget.customimageview.GlideCircleTransform;
import com.baseandroid.widget.menu.view.QianBaoContentLayout;
import com.baseandroid.widget.menu.view.QianBaoMenuLayout;
import com.bumptech.glide.Glide;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.event.PersonalFragmentEvent;
import com.haodaibao.fanbeiapp.module.invoice.MyInvoiceActivity;
import com.haodaibao.fanbeiapp.module.login.LoginActivity;
import com.haodaibao.fanbeiapp.module.personal.balance.BalanceActivity;
import com.haodaibao.fanbeiapp.module.personal.collections.CollectionActivity;
import com.haodaibao.fanbeiapp.module.personal.coupon.CouponActivity;
import com.haodaibao.fanbeiapp.module.personal.message.MessageListActivity;
import com.haodaibao.fanbeiapp.module.personal.paylist.PaylistActivity;
import com.haodaibao.fanbeiapp.module.personal.setting.UserinfoSettingActivity;
import com.haodaibao.fanbeiapp.module.suggest.SuggestActivity;
import com.haodaibao.fanbeiapp.repository.json.BalanceBean;
import com.haodaibao.fanbeiapp.repository.json.BhhInfoBean;
import com.haodaibao.fanbeiapp.repository.json.BhhStatueBean;
import com.haodaibao.fanbeiapp.repository.json.CouponBean;
import com.haodaibao.fanbeiapp.repository.json.UserDate;
import com.qianbao.shiningwhitelibrary.BHHManager;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Cookie;

/**
 * @author LuoYi
 * Time: 2017 16:47 2017/11/20 11
 */

public class PersonalMenuView implements PersonalFragmentContract.MenuView, PersonalFragmentContract.View, View.OnClickListener {

    private BaseActivity activity;

    @BindView(R.id.qianbao_menu)
    QianBaoMenuLayout qianbaoMenu;
    @BindView(R.id.menu_content_fl)
    FrameLayout menuContentFl;
    @BindView(R.id.menu_content_layout)
    QianBaoContentLayout menuContentLayout;
    @BindView(R.id.menu_balance_ll)
    LinearLayout menuBalanceLl;
    @BindView(R.id.menu_bhh_ll)
    RelativeLayout menuBhhLl;
    @BindView(R.id.menu_coupon_ll)
    LinearLayout menuCouponLl;
    @BindView(R.id.menu_record_ll)
    LinearLayout menuRecordLl;
    @BindView(R.id.menu_collection_ll)
    LinearLayout menuCollectionLl;
    @BindView(R.id.menu_feedback_ll)
    LinearLayout menuFeedbackLl;
    @BindView(R.id.menu_more_ll)
    LinearLayout menuMoreLl;
    @BindView(R.id.balance_content_tv)
    TextView balanceContentTv;
    @BindView(R.id.bhh_content_tv)
    TextView bhhContentTv;
    @BindView(R.id.coupon_content_tv)
    TextView couponContentTv;
    @BindView(R.id.close_menu_iv)
    ImageView closeMenuIv;
    @BindView(R.id.menu_message_rl)
    LinearLayout menuMessageIv;
    @BindView(R.id.menu_user_personal_ll)
    LinearLayout menuUserPersonalLl;
    @BindView(R.id.menu_head_user_iv)
    ImageView menuHeadUserIv;
    @BindView(R.id.menu_login_tv)
    TextView menuLoginTv;
    @BindView(R.id.menu_name_tv)
    TextView menuNameTv;
    @BindView(R.id.menu_tel_tv)
    TextView menuTelTv;
    @BindView(R.id.menu_customer_service_iv)
    ImageView menuCustomerServiceIv;
    @BindView(R.id.menu_message_icon_iv)
    ImageView menuMessageIconIv;
    @BindView(R.id.invoice_ll)
    View invoice_ll;

    private String mBalance = "";        //余额
    // private SWhiteManager swIManager;
    private boolean mOpenStatus;

    private PersonalFragmentPresenter mPresenter;

    public PersonalMenuView(BaseActivity activity) {
        this.activity = activity;
        ButterKnife.bind(this, activity);
        menuCustomerServiceIv.setVisibility(View.GONE);
    }

    @Override
    public void setMenuLayout() {
        qianbaoMenu.setQianBaoMenuStatusListener(menuStatusListener);
        qianbaoMenu.setQianBaoContentLayout(menuContentLayout);

        menuBalanceLl.setOnClickListener(this);
        menuCouponLl.setOnClickListener(this);
        menuBhhLl.setOnClickListener(this);
        menuRecordLl.setOnClickListener(this);
        menuCollectionLl.setOnClickListener(this);
        menuFeedbackLl.setOnClickListener(this);
        menuMoreLl.setOnClickListener(this);
        closeMenuIv.setOnClickListener(this);
        menuMessageIv.setOnClickListener(this);
        menuUserPersonalLl.setOnClickListener(this);
        menuHeadUserIv.setOnClickListener(this);
        menuCustomerServiceIv.setOnClickListener(this);
        invoice_ll.setOnClickListener(this);
    }

    @Override
    public void initMenuData() {
        if (mPresenter == null) {
            mPresenter = new PersonalFragmentPresenter(this, activity);
        }
        // swIManager = SWhiteManager.getInstance();
        requestNetData();
    }

    @Override
    public void onOpenMenu() {

        qianbaoMenu.setVisibility(View.VISIBLE);
        qianbaoMenu.open();
    }

    @Override
    public void onCloseMenu() {
        qianbaoMenu.close();
    }

    @Override
    public void onLoginEvent(PersonalFragmentEvent event) {
        if (event.isRefreshBhhItem()) {
            getSWhiteInfo();
        }
        if (event.isRefreshUserInfo()) {
            mPresenter.getUserInfo();
            mPresenter.getBalance();
            mPresenter.getUnReadMsgsNum();
        }
        if (event.isLogOut()) {
            requestNetData();
        }
        if (event.refreshCouponCount) {
            mPresenter.getCouponsCount();
        }
    }

    @Override
    public void onUnRadMsg() {
        mPresenter.getUnReadMsgsNum();
    }

    @Override
    public void backPressed() {
        if (qianbaoMenu.isOpen()) {
            qianbaoMenu.close();
            return;
        }
    }

    @Override
    public boolean keyDown() {
        if (qianbaoMenu.isOpen()) {
            qianbaoMenu.close();
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void destroy() {
        // swIManager = null;
    }

    QianBaoMenuLayout.QianBaoMenuStatusListener menuStatusListener = new QianBaoMenuLayout.QianBaoMenuStatusListener() {
        @Override
        public void onOpen() {

        }

        @Override
        public void onClose() {
            qianbaoMenu.setVisibility(View.GONE);
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.close_menu_iv:
//                StatusBarUtil.setTranslucentForImageViewInFragment(activity, 0, null);
                qianbaoMenu.close();
                break;
            case R.id.menu_balance_ll://跳转至余额
                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                    ActivityJumpUtil.next(activity, LoginActivity.class);
                    return;
                }
                Bundle bundle = new Bundle();
                bundle.putString("balance", mBalance);
                bundle.putBoolean("isPersonal", true);
                ActivityJumpUtil.next(activity, BalanceActivity.class, bundle, 0);
                break;
            case R.id.menu_coupon_ll://跳转至生活优惠券
                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                    ActivityJumpUtil.next(activity, LoginActivity.class);
                    return;
                }
                ActivityJumpUtil.next(activity, CouponActivity.class);
                break;
            case R.id.menu_bhh_ll: //跳转至白花花页
                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                    Intent intent = new Intent(activity, LoginActivity.class);
                    intent.putExtra("Bhh", "Bhh");
                    activity.startActivity(intent);
//                    ActivityJumpUtil.next(activity, LoginActivity.class, null, 1000);
                } else {
                    // swIManager.jumpToSWhite(getActivity(), mOpenStatus);
                    if (TextUtils.isEmpty(Global.getUserInfo().bhhAcessToken)) {
                        mPresenter.getUserInfo();
                    } else {
                        BHHManager.openViewForRelease(activity, Global.getUserInfo().bhhAcessToken, Global.getUserInfo().getUserno(), false);
                    }

                }
                break;
            case R.id.menu_record_ll: //跳转至买单记录页面
                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                    ActivityJumpUtil.next(activity, LoginActivity.class);
                    return;
                }
                ActivityJumpUtil.next(activity, PaylistActivity.class);
                break;
            case R.id.menu_collection_ll: //跳转至收藏页面
                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                    ActivityJumpUtil.next(activity, LoginActivity.class);
                    return;
                }
                ActivityJumpUtil.next(activity, CollectionActivity.class);
                break;
            case R.id.menu_feedback_ll:
                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                    ActivityJumpUtil.next(activity, LoginActivity.class);
                    return;
                }
                ActivityJumpUtil.next(activity, SuggestActivity.class);
                break;
            case R.id.menu_more_ll: //跳转至更多页面
                ActivityJumpUtil.next(activity, MoreActivity.class);
                break;
            case R.id.menu_message_rl: //跳转至消息列表页
                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                    ActivityJumpUtil.next(activity, LoginActivity.class);
                    return;
                }
                ActivityJumpUtil.next(activity, MessageListActivity.class);
                break;
            case R.id.menu_head_user_iv:
            case R.id.menu_user_personal_ll://跳转至详情设置页
                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                    ActivityJumpUtil.next(activity, LoginActivity.class);
                    return;
                }
                ActivityJumpUtil.next(activity, UserinfoSettingActivity.class);
                break;
            case R.id.menu_customer_service_iv:
                dial();
                break;
            case R.id.invoice_ll:
                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                    ActivityJumpUtil.next(activity, LoginActivity.class);
                } else {
                    MyInvoiceActivity.start(activity);
                }

                break;
            default:

                break;
        }
    }

    private void dial() {
        Intent dial = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + activity.getString(R.string.Tel)));
        dial.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(dial);
    }

    @Override
    public void setupBhhStatueData(BhhStatueBean bean) {
        String show = bean.getSHOWBHH();
        if (show.equals("1")) {
            menuBhhLl.setVisibility(View.VISIBLE);
        } else {
            menuBhhLl.setVisibility(View.GONE);
        }
    }

    @Override
    public void getBhhStatueFailed() {
        menuBhhLl.setVisibility(View.GONE);
    }

    @Override
    public void setupUserInfoData(UserDate bean) {
        String imgHeadUrl = bean.getUser().getFace();
        String imageUrl = CommonUtils.getImageUrl(imgHeadUrl);
        menuLoginTv.setVisibility(View.GONE);
        Glide.with(Global.getContext()).load(imageUrl).asBitmap().error(R.drawable.icon_default_head_info).centerCrop()
                .placeholder(R.drawable.icon_default_head_info)
                .transform(new GlideCircleTransform(Global.getContext()))
                //                .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(menuHeadUserIv);
        if (TextUtils.isEmpty(bean.getUser().getAlias())) {
            menuNameTv.setText("生活达人 来个昵称吧~");
        } else {
            menuNameTv.setText(bean.getUser().getAlias());
        }
        menuNameTv.setVisibility(View.VISIBLE);
        menuTelTv.setText(bean.getUser().getMobile());
        menuTelTv.setVisibility(View.VISIBLE);
        Global.setUserInfo(bean.getUser());
        mPresenter.getMyBhhInfo(Global.getUserInfo().bhhAcessToken);
    }

    @Override
    public void setupBalanceData(BalanceBean bean) {
        if (bean == null) {
            menuBalanceLl.setVisibility(View.GONE);
            return;
        }
        mBalance = bean.getBalanceAvailable();
        if (Float.parseFloat(mBalance) <= 0.f) {
            menuBalanceLl.setVisibility(View.GONE);
        } else {
            menuBalanceLl.setVisibility(View.VISIBLE);
            balanceContentTv.setVisibility(View.VISIBLE);
            balanceContentTv.setText(FormatUtil.formatAmount(2, mBalance));
        }
    }

    @Override
    public void setupCouponCountData(CouponBean bean) {
        int size = 0;
        try {
            size = bean.getCouponList().getItems().size();
        } catch (Exception e) {
            e.printStackTrace();
        }
        couponContentTv.setText(size + "张");
        if (size == 0) {
            couponContentTv.setVisibility(View.INVISIBLE);
        } else {
            couponContentTv.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setUnReadMsgView(String unReadNumber) {
        if (TextUtils.isEmpty(unReadNumber) || TextUtils.equals(unReadNumber, "0")) {
            menuMessageIconIv.setImageResource(R.drawable.mine_msg_normal_icon);
        } else {
            menuMessageIconIv.setImageResource(R.drawable.mine_msg_new_icon);
        }
    }


    @Override
    public void updateBhhInfo(BhhInfoBean bhhInfoBean) {
        try {
            String baiHuaHua;
            if (TextUtils.equals(bhhInfoBean.openBhh, "1")) {
                double available = Double.parseDouble(bhhInfoBean.availableLimit) / 100.0;
                baiHuaHua = "可用额度  ¥" + FormatUtil.formatAmount(2, "" + available);
                bhhContentTv.setText(baiHuaHua);
            } else if (TextUtils.equals(bhhInfoBean.openBhh, "2")) {
                baiHuaHua = "额度审批中，请稍候";
                bhhContentTv.setText(baiHuaHua);
            } else if (TextUtils.equals(bhhInfoBean.openBhh, "3")) {
                baiHuaHua = "拒绝期限内";
                bhhContentTv.setText(baiHuaHua);
            } else if (TextUtils.equals(bhhInfoBean.openBhh, "4")) {
                baiHuaHua = "最长免息60天，马上开通";
                bhhContentTv.setText(baiHuaHua);
            } else if (TextUtils.equals(bhhInfoBean.openBhh, "5")) {
                baiHuaHua = "未知";
                bhhContentTv.setText(baiHuaHua);
            } else {
                if (TextUtils.isEmpty(Global.getUserInfo().bhhAcessToken)) {
                    bhhContentTv.setText("点击重试");
                }
            }
        } catch (NullPointerException e) {
            if (TextUtils.isEmpty(Global.getUserInfo().bhhAcessToken)) {
                bhhContentTv.setText("点击重试");
            }

        }

    }

    @Override
    public void getBhhInfoFailed() {
        if (TextUtils.isEmpty(Global.getUserInfo().bhhAcessToken)) {
            bhhContentTv.setText("点击重试");
        }

    }

    //获取白花花信息
    private void getSWhiteInfo() {
        if (!TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
            List<Cookie> cookieList = OkHttpClientManager.getHttpCookieList();
            if (cookieList != null && cookieList.size() > 0) {
                /*swIManager.getStatus(cookieList, new ISdkManager.StatusCallback() {
                    @Override
                    public void callback(ISdkManager.SWhite sWhite) {
                        if (activity == null || activity.isFinishing()) {
                            return;
                        }
                        String baiHuaHua;
                        if (sWhite.success) {
                            if (sWhite.status == 1) {
                                mOpenStatus = true;
                                baiHuaHua = "可用额度  ¥" + FormatUtil.formatAmount(2, "" + sWhite.available);
                                bhhContentTv.setText(baiHuaHua);
                            } else {
                                mOpenStatus = false;
                                baiHuaHua = "最长免息60天，马上开通";
                                bhhContentTv.setText(baiHuaHua);
                            }
                        } else {
                            LogUtil.e(sWhite.message);
                        }
                    }
                });*/
            }

        } else {
        }
    }

    private void requestNetData() {
        mPresenter.getBhhStatue();
        if (!TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
//            getSWhiteInfo();
            UserDate bean = new UserDate();
            bean.setUser(Global.getUserInfo());
            setupUserInfoData(bean);
            mPresenter.getBalance();
            mPresenter.getCouponsCount();
            mPresenter.getUnReadMsgsNum();
        } else {
            mOpenStatus = false;
            menuLoginTv.setVisibility(View.VISIBLE);
            menuNameTv.setVisibility(View.GONE);
            menuTelTv.setVisibility(View.GONE);
            menuBalanceLl.setVisibility(View.GONE);
            couponContentTv.setVisibility(View.INVISIBLE);
            Glide.with(Global.getContext()).load(R.drawable.icon_default_head_info).asBitmap()
                    .transform(new GlideCircleTransform(Global.getContext())).into(menuHeadUserIv);
            bhhContentTv.setText("最长免息60天，马上开通");
        }
    }
}
