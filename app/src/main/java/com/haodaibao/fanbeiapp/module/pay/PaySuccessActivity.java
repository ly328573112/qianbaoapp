package com.haodaibao.fanbeiapp.module.pay;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.camera.CameraUtil;
import com.baseandroid.config.Constant;
import com.baseandroid.utils.CommonUtils;
import com.baseandroid.utils.EmojiUtil;
import com.baseandroid.utils.FormatUtil;
import com.baseandroid.utils.LogUtil;
import com.baseandroid.utils.StringUtils;
import com.baseandroid.utils.ToastUtils;
import com.baseandroid.widget.customscrollview.QBScrollview;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.event.MakoutInvoiceEvent;
import com.haodaibao.fanbeiapp.event.MerchantEvent;
import com.haodaibao.fanbeiapp.event.OriginalDeleteEvent;
import com.haodaibao.fanbeiapp.event.PaylistEvent;
import com.haodaibao.fanbeiapp.fileuploading.FileUploaderManager;
import com.haodaibao.fanbeiapp.imageadapter.CameraSelectView;
import com.haodaibao.fanbeiapp.imageadapter.ImageClickListener;
import com.haodaibao.fanbeiapp.imageadapter.PhotoImageAdapter;
import com.haodaibao.fanbeiapp.module.dialog.MessageDialog;
import com.haodaibao.fanbeiapp.module.evaluate.EvaluateContract;
import com.haodaibao.fanbeiapp.module.evaluate.EvaluatePresenter;
import com.haodaibao.fanbeiapp.module.imgoriginal.OriginalPagerActivity;
import com.haodaibao.fanbeiapp.module.invoice.makeout.MakeoutInvoiceActivity;
import com.haodaibao.fanbeiapp.module.merchant.MerchantDetailActivity;
import com.haodaibao.fanbeiapp.module.pay.adapter.DiscountListAdapter;
import com.haodaibao.fanbeiapp.repository.json.CommentListBean;
import com.haodaibao.fanbeiapp.repository.json.PayInfoDetailBean;
import com.haodaibao.fanbeiapp.repository.json.PhotoInfo;
import com.haodaibao.fanbeiapp.repository.json.UploadResult;
import com.jayfeng.lesscode.core.DisplayLess;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

import static com.baseandroid.config.Constant.FROM;
import static com.baseandroid.config.Constant.PAYSUCC_COMMENT_RESULTCODE;

/**
 * date: 2017/9/21.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class PaySuccessActivity extends BaseActivity implements View.OnClickListener, ImageClickListener, EvaluateContract.EvaluateView, CameraSelectView.CameraPotoListener {
    @BindView(R.id.icon_close_btn)
    protected ImageView icon_close_btn;
    @BindView(R.id.paysuccess_mask)
    protected View paysuccess_mask;
    @BindView(R.id.edit_scroll)
    protected QBScrollview edit_scroll;
    @BindView(R.id.evaluate_ok)
    protected TextView evaluate_ok;
    @BindView(R.id.satisfied_btn)
    protected Button satisfied_btn;
    @BindView(R.id.commonly_btn)
    protected Button commonly_btn;
    @BindView(R.id.dissatisfied_btn)
    protected Button dissatisfied_btn;
    @BindView(R.id.comment_sheet)
    protected LinearLayout comment_sheet;
    @BindView(R.id.toolbar_back)
    protected RelativeLayout toolbar_back;
    @BindView(R.id.toolbar_title)
    protected TextView tvTitle;
    @BindView(R.id.tv_order_money)
    protected TextView mOrderMoney;
    @BindView(R.id.tv_pay_way)
    protected TextView mPayWay;
    @BindView(R.id.rl_discout_detail)
    protected RelativeLayout rl_discout_detail;
    @BindView(R.id.ll_total_mpney)
    protected LinearLayout ll_total_mpney;
    @BindView(R.id.tv_merchant_discount_name)
    protected TextView tv_merchant_discount_name;
    @BindView(R.id.ll_merchat_discount)
    protected LinearLayout ll_merchat_discount;
    @BindView(R.id.tv_merchat_discount)
    protected TextView tv_merchat_discount;
    @BindView(R.id.ll_life_coupon)
    protected LinearLayout ll_life_layout;
    @BindView(R.id.ll_life_layout)
    protected LinearLayout ll_life_coupon;
    @BindView(R.id.tv_life_coupon)
    protected TextView tv_life_coupon;

    @BindView(R.id.discount_rv)
    protected RecyclerView discount_rv;
    @BindView(R.id.pay_succ_sv)
    protected ScrollView pay_succ_sv;
    @BindView(R.id.content_fl)
    protected FrameLayout content_fl;

    @BindView(R.id.tv_total_money)
    protected TextView mTotalMoney;
    @BindView(R.id.commment_edit)
    protected EditText commment_edit;
    @BindView(R.id.goto_invoice)
    LinearLayout goto_invoice;
    @BindView(R.id.toolbar)
    View toolbar;

    BottomSheetBehavior bottomSheetBehavior;
    private String star = "5";
    private int COLUMN = 9;//图片可选数量
    private CameraUtil cameraUtil;
    private int imageColumn = 1;//图片集合是否带有加号
    private List<PhotoInfo> photoList = new ArrayList<>();// 照片uri列表
    private int currentItemId = 1;// 加号
    private PhotoImageAdapter imageAdapter;
    private CameraSelectView selectView;
    private EvaluatePresenter evaluatePresenter;
    private int oldState = BottomSheetBehavior.STATE_COLLAPSED;
    public static final String PAYINFODETAIL = "payinfoDetail";
    private static final String ORDERNO = "orderno";
    private String orderno;//订单号
    private String from;//支付流程进入页面（扫码，商户详情，再买一单）
    private String merchantno;
    private Serializable r;
    private FileUploaderManager uploaderManager;
    private MessageDialog messageDialog;
    private int state = BottomSheetBehavior.STATE_COLLAPSED;

    public static void start(BaseActivity activity, PayInfoDetailBean payInfoDetailBean, String orderno, String from) {
        Intent intent = new Intent(activity, PaySuccessActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(PAYINFODETAIL, payInfoDetailBean);
        bundle.putString(ORDERNO, orderno);
        bundle.putString(FROM, from);
        intent.putExtras(bundle);
        activity.startActivityForResult(intent, 0);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_pay_success;
    }

    @Override
    protected void setupView() {
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.c_F1F1F1));
        selectView = new CameraSelectView(this);
        selectView.setPictureCut(false);
        cameraUtil = new CameraUtil(this);
        evaluatePresenter = new EvaluatePresenter(this);
        doEvaluate();
        setSvHeight();
        toolbar_back.setOnClickListener(this);
        goto_invoice.setOnClickListener(this);
        commment_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() > 500) {
                    showToast("最多输入500字！");
                    int selEndIndex = Selection.getSelectionEnd(s);
                    commment_edit.setText(s.toString().substring(0, 500));
                    Editable editable = commment_edit.getText();

                    //新字符串的长度
                    int newLen = editable.toString().length();
                    //旧光标位置超过字符串长度
                    if (selEndIndex > newLen) {
                        selEndIndex = editable.length();
                    }
                    //设置新光标所在的位置
                    Selection.setSelection(editable, selEndIndex);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


    }

    //计算设置scrollView高度
    private void setSvHeight() {
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) pay_succ_sv.getLayoutParams();
        ViewTreeObserver vto = content_fl.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                content_fl.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) pay_succ_sv.getLayoutParams();
                lp.height = content_fl.getHeight() - toolbar.getHeight() - bottomSheetBehavior.getPeekHeight();
                pay_succ_sv.setLayoutParams(lp);
            }
        });
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        initData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void initData() {
        orderno = getIntent().getStringExtra(ORDERNO);
        r = getIntent().getSerializableExtra(PAYINFODETAIL);
        from = getIntent().getStringExtra("from");
        getMypayDetails(r);
    }

    /* 获取交易详情 */
    private void getMypayDetails(Serializable r) {
        if (r != null && (r instanceof PayInfoDetailBean)) {// 支付成功
            PayInfoDetailBean resp = (PayInfoDetailBean) r;

            tvTitle.setText(resp.getMerchantname());
            merchantno = resp.getMerchantno();
            mOrderMoney.setText("¥ " + FormatUtil.formatAmount(2, resp.getOrderamt()));
            mPayWay.setText(getPaymentMethod(resp.getPayflows()));
            double cashamtDou = Double.valueOf(FormatUtil.formatAmount(2, resp.getCashamt()));
            double orderamtDou = Double.valueOf(FormatUtil.formatAmount(2, resp.getOrderamt()));
//            double sub = CountUtil.sub(orderamtDou, cashamtDou);
            if (cashamtDou == orderamtDou) {
                rl_discout_detail.setVisibility(View.GONE);
                ll_total_mpney.setVisibility(View.GONE);
//                ll_redict_money.setVisibility(View.GONE);
            } else {
                rl_discout_detail.setVisibility(View.VISIBLE);
                ll_total_mpney.setVisibility(View.VISIBLE);
//                ll_redict_money.setVisibility(View.VISIBLE);

                if (TextUtils.isEmpty(resp.getReduceamount()) && TextUtils.isEmpty(resp.getCouponamt())) {
                    rl_discout_detail.setVisibility(View.GONE);
                    ll_total_mpney.setVisibility(View.GONE);
                } else {
                    rl_discout_detail.setVisibility(View.VISIBLE);
                    ll_total_mpney.setVisibility(View.VISIBLE);
                    if (!TextUtils.isEmpty(resp.getRefundmode()) && resp.getRefundmode().equals("1")) {
                        tv_merchant_discount_name.setText("商家折扣");
                    } else if (!TextUtils.isEmpty(resp.getRefundmode()) && resp.getRefundmode().equals("3")) {
                        tv_merchant_discount_name.setText("商家满减");
                    } else {
                        tv_merchant_discount_name.setText("商家折扣");
                    }
                    String reduceamount = FormatUtil.formatAmount(2, resp.getReduceamount());
                    if (reduceamount.equals("0")) {
                        ll_merchat_discount.setVisibility(View.GONE);
                    } else {
                        ll_merchat_discount.setVisibility(View.VISIBLE);
                        tv_merchat_discount.setText("-¥ " + reduceamount);
                    }
                    String couponamt = FormatUtil.formatAmount(2, resp.getCouponamt());
                    if (couponamt.equals("0")) {
                        ll_life_coupon.setVisibility(View.GONE);

                    } else {
                        ll_life_coupon.setVisibility(View.VISIBLE);
                        tv_life_coupon.setText("-¥ " + couponamt);
                    }
                    if (ll_merchat_discount.getVisibility() == View.VISIBLE && ll_life_coupon.getVisibility() == View.VISIBLE) {
                        ll_life_layout.setVisibility(View.VISIBLE);
                    } else {
                        ll_life_layout.setVisibility(View.GONE);
                    }

                    //2018.04.17添加优惠活动列表
                    DiscountListAdapter mAdapter = new DiscountListAdapter(this);
                    discount_rv.setLayoutManager(new LinearLayoutManager(this));
                    discount_rv.setAdapter(mAdapter);
                    mAdapter.resetData(resp.getActivities());

                    mTotalMoney.setText("¥ " + FormatUtil.formatAmount(2, resp.getCashamt()));

                }


//                if (FormatUtil.formatAmount(2, resp.getReduceamount()).equals("0")) {
//                    ll_redict_money.setVisibility(View.GONE);
//                }
//                mReditMoney.setText("-¥ " + FormatUtil.formatAmount(2, resp.getReduceamount()));

//                if(FormatUtil.formatAmount(2, resp.getCouponamt()).equals("0")){
//                    ll_red_packet.setVisibility(View.GONE);
//                }
//
//                mRedPacket.setText("-¥ " + FormatUtil.formatAmount(2, resp.getCouponamt()));
//                mRedictTotle.setText("¥ " + FormatUtil.formatAmount(2, sub + ""));
            }
//            getSWhiteInfo();
            setInvoice(resp);
        }

    }

    private void setInvoice(PayInfoDetailBean resp) {
        if (TextUtils.equals(resp.canInvoice, "1")) {
            goto_invoice.setVisibility(View.VISIBLE);
        } else {
            goto_invoice.setVisibility(View.GONE);
        }
    }

    /* 支付信息 */
    private String getPaymentMethod(List<PayInfoDetailBean.PayflowsBean> mypayDetailsFlow) {
        String result = "";
        /* 支付方式 */
        for (PayInfoDetailBean.PayflowsBean payDetailsFlow : mypayDetailsFlow) {

            String pt = payDetailsFlow.getPaytype();
            if (!TextUtils.isEmpty(pt)) {
                if (pt.equals(Constant.XIANJIN)) {// 余额支付
                    result = "余额";
                } else if (pt.equals(Constant.KUAIJIE)) {// 银行卡支付
//                    String bankname = payDetailsFlow.getBankname();
//                    String payno = payDetailsFlow.getPayno();
//                    payno = payno.substring(payno.length() - 4, payno.length());
//                    result = bankname + "(" + payno + ")";
                    result = "银行卡";
                } else if (pt.equals(Constant.WEIXIN)) {// 微信支付
                    result = "微信";
                } else if (pt.equals(Constant.SHUAKA)) {// 刷卡支付
                    result = "刷卡";
                } else if (pt.equals(Constant.ALIPAY_4)) {// 支付宝支付
                    result = "支付宝";
                } else if (pt.equals(Constant.ALIPAY_7)) {// 支付宝支付
                    result = "支付宝";
                } else if (pt.equals(Constant.ALIPAY_8)) {// 支付宝支付
                    result = "支付宝";
                } else if (pt.equals(Constant.GONGZHONGHAO)) {// 微信公众号支付
                    result = "微信";
                } else if (pt.equals(Constant.POS)) {// POS刷卡支付
                    result = "POS刷卡";
                } else if (pt.equals(Constant.PAYSDK_WEIXIN)) {// 收银台-微信app支付
                    result = "微信";
                } else if (pt.equals(Constant.PAYSDK_ALIPAY)) {// 收银台-支付宝app支付
                    result = "支付宝";
                } else if (pt.equals(Constant.PAYSDK_SWHITE)) {// 收银台-白花花支付
                    result = "白花花";
                } else if (pt.equals(Constant.HONGBAO)) {// 红包抵扣金额
//                    result= "红包抵扣";
                }
            }
        }
        return result;
    }

    private void doEvaluate() {

        bottomSheetBehavior = BottomSheetBehavior.from(comment_sheet);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    bottomSheetBehavior.setState(state);
                }
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    setPaySuccessBtnBG(satisfied_btn, R.color.shining_white_999999, R.drawable.pay_success_false_btn, R.drawable.icon_satisfied_normal);
                    setPaySuccessBtnBG(commonly_btn, R.color.shining_white_999999, R.drawable.pay_success_false_btn, R.drawable.icon_commonly_normal);
                    setPaySuccessBtnBG(dissatisfied_btn, R.color.shining_white_999999, R.drawable.pay_success_false_btn, R.drawable.icon_dissatisfied_normal);
                    icon_close_btn.setVisibility(View.GONE);
                    paysuccess_mask.setVisibility(View.GONE);

                    if (getCurrentFocus() != null && getCurrentFocus().getWindowToken() != null) {
                        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    }
                } else if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    icon_close_btn.setVisibility(View.VISIBLE);
                    paysuccess_mask.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
        dissatisfied_btn.setOnClickListener(goneListener);
        satisfied_btn.setOnClickListener(goneListener);
        commonly_btn.setOnClickListener(goneListener);
        icon_close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                state = BottomSheetBehavior.STATE_COLLAPSED;
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                icon_close_btn.setVisibility(View.GONE);
                paysuccess_mask.setVisibility(View.GONE);
                setPaySuccessBtnBG(satisfied_btn, R.color.shining_white_999999, R.drawable.pay_success_false_btn, R.drawable.icon_satisfied_normal);
                setPaySuccessBtnBG(commonly_btn, R.color.shining_white_999999, R.drawable.pay_success_false_btn, R.drawable.icon_commonly_normal);
                setPaySuccessBtnBG(dissatisfied_btn, R.color.shining_white_999999, R.drawable.pay_success_false_btn, R.drawable.icon_dissatisfied_normal);
            }
        });

        evaluate_ok.setOnClickListener(this);
        RecyclerView pay_success_gridview = (RecyclerView) findViewById(R.id.pay_success_gridview);
        photoList.clear();
        PhotoInfo info = new PhotoInfo(currentItemId, null, false);
        photoList.add(info);
        initListView(pay_success_gridview);
    }

    private void initListView(RecyclerView pay_success_gridview) {
        CustomGridLayoutManager layoutManager = new CustomGridLayoutManager(this, 4, GridLayoutManager.VERTICAL, false);
        pay_success_gridview.setLayoutManager(layoutManager);
        pay_success_gridview.setHasFixedSize(true);
        imageAdapter = new PhotoImageAdapter(this);
        pay_success_gridview.setAdapter(imageAdapter);
        pay_success_gridview.setItemAnimator(new DefaultItemAnimator());
        imageAdapter.setImageClickListener(this);
        imageAdapter.addData(photoList);
    }

    private void setPaySuccessBtnBG(Button btnBG, int colorText, int backgroudBtn, int drawableBtn) {
        btnBG.setTextColor(getResources().getColor(colorText));
        btnBG.setBackgroundResource(backgroudBtn);
        Drawable drawable = getResources().getDrawable(drawableBtn);
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
        btnBG.setCompoundDrawables(drawable, null, null, null);
    }

    View.OnClickListener goneListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
//            if (edit_scroll.getVisibility() == View.GONE) {
//                edit_scroll.setVisibility(View.VISIBLE);
//                evaluate_ok.setVisibility(View.VISIBLE);
//                icon_close_btn.setVisibility(View.VISIBLE);
//                paysuccess_mask.setVisibility(View.VISIBLE);
//            }
//            if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
            state = BottomSheetBehavior.STATE_EXPANDED;
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            icon_close_btn.setVisibility(View.VISIBLE);
            paysuccess_mask.setVisibility(View.VISIBLE);
//            }

            switch (v.getId()) {
                case R.id.satisfied_btn:
                    star = "5";
                    setPaySuccessBtnBG(satisfied_btn, R.color.shining_white_ffffff, R.drawable.pay_success_true_btn, R.drawable.icon_satisfied_selected);
                    setPaySuccessBtnBG(commonly_btn, R.color.shining_white_999999, R.drawable.pay_success_false_btn, R.drawable.icon_commonly_normal);
                    setPaySuccessBtnBG(dissatisfied_btn, R.color.shining_white_999999, R.drawable.pay_success_false_btn, R.drawable.icon_dissatisfied_normal);
                    break;
                case R.id.commonly_btn:
                    star = "3";
                    setPaySuccessBtnBG(satisfied_btn, R.color.shining_white_999999, R.drawable.pay_success_false_btn, R.drawable.icon_satisfied_normal);
                    setPaySuccessBtnBG(commonly_btn, R.color.shining_white_ffffff, R.drawable.pay_success_true_btn, R.drawable.icon_commonly_selected);
                    setPaySuccessBtnBG(dissatisfied_btn, R.color.shining_white_999999, R.drawable.pay_success_false_btn, R.drawable.icon_dissatisfied_normal);
                    break;
                case R.id.dissatisfied_btn:
                    star = "1";
                    setPaySuccessBtnBG(satisfied_btn, R.color.shining_white_999999, R.drawable.pay_success_false_btn, R.drawable.icon_satisfied_normal);
                    setPaySuccessBtnBG(commonly_btn, R.color.shining_white_999999, R.drawable.pay_success_false_btn, R.drawable.icon_commonly_normal);
                    setPaySuccessBtnBG(dissatisfied_btn, R.color.shining_white_ffffff, R.drawable.pay_success_true_btn, R.drawable.icon_dissatisfied_selected);
                    break;
            }
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back:
                if (CommonUtils.isFastDoubleClick()) {
                    return;
                }
                setResult(Constant.PAYSUCC_PRESS_BACK_RESULTCODE);
                EventBus.getDefault().post(new MerchantEvent());
                EventBus.getDefault().post(new PaylistEvent());
                finish();
                break;
            case R.id.evaluate_ok:
                String text = commment_edit.getText().toString();
                text = EmojiUtil.replaceEmoji(text);
                evaluate_ok.setEnabled(false);
                showUploading();
                if (photoList.size() > 1) {
                    if (uploaderManager == null) {
                        uploaderManager = FileUploaderManager.getInstance();
                    }
                    uploaderManager.updateImage(photoList, uploaderResult);
                } else {
                    StringBuffer imgUriBuffer = new StringBuffer();// 拼接上传图片名称路径
                    String s = imgUriBuffer.toString();
                    processUploading("提交中");
                    evaluatePresenter.submintComment(orderno, merchantno, star, text, s);
                }
                break;
            case R.id.goto_invoice:
                MakeoutInvoiceActivity.start(this, orderno, merchantno, "", false);
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MakoutInvoiceEvent event) {
        goto_invoice.setVisibility(View.GONE);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            setResult(Constant.PAYSUCC_PRESS_BACK_RESULTCODE);
            EventBus.getDefault().post(new PaylistEvent());
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void openCamera(int position) {
        int photoNumber = photoList.get(position).getPhotoNumber();
        if (photoNumber == currentItemId) {
            int optionalNumber = COLUMN - (photoList.size() - imageColumn);
            selectView.showCameraDialog(cameraUtil, optionalNumber);
            selectView.setCameraPotoListener(this);
        } else if (photoNumber == 0) {
            ArrayList<String> arrayList = new ArrayList<>();
//            String[] strings = new String[photoList.size() - imageColumn];
//            ImageGalleryActivity.show(this, strings, position, false);
            for (int i = 0; i < photoList.size() - imageColumn; i++) {
                arrayList.add(photoList.get(i).getPhotoUri());
            }
            OriginalPagerActivity.start(this, arrayList, position);
        }
    }

    @Override
    public void onDeleteImage(int position) {
        photoList.remove(position);
        confineListSize();
        imageAdapter.resetData(photoList);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(OriginalDeleteEvent event) {
        onDeleteImage(event.curPosition);
    }


    @Override
    public void onEvaluateList(CommentListBean commentListBean) {
        //此页面不需要显示评论列表
    }

    @Override
    public void onEvaluateListError() {

    }

    @Override
    public void onSubmint() {
        closeUploading();
        evaluate_ok.setEnabled(true);
        showToast("评论成功");
        setResult(PAYSUCC_COMMENT_RESULTCODE);

        EventBus.getDefault().post(new MerchantEvent());
        final PaylistEvent paylistEvent = new PaylistEvent();
        paylistEvent.setComment(true);
        if (!TextUtils.isEmpty(from) && !from.equals("merchantDetail")) {
            Bundle b = new Bundle();
            b.putString("merchantno", merchantno);
            ActivityJumpUtil.next(this, MerchantDetailActivity.class, b, 0);

            new Handler().postDelayed(new Runnable() {
                public void run() {
                    EventBus.getDefault().post(paylistEvent);
                    finish();
                }
            }, 500);
        } else {
            EventBus.getDefault().post(paylistEvent);
            finish();
        }
    }

    @Override
    public void onSubmintError() {
        closeUploading();
        evaluate_ok.setEnabled(true);
    }

    @Override
    public void onResultPotoList(List<PhotoInfo> imgList) {
        this.photoList.addAll(imgList);
        confineListSize();
        imageAdapter.resetData(photoList);
    }

    /**
     * 加号始终放在List最后
     */
    private void confineListSize() {
        for (PhotoInfo photoInfo : photoList) {
            if (photoInfo.getPhotoNumber() == currentItemId) {
                photoList.remove(photoInfo);
                break;
            }
        }
        if (photoList.size() < COLUMN) {
            PhotoInfo info = new PhotoInfo(currentItemId, null, false);
            photoList.add(info);
            imageColumn = 1;
        } else {
            imageColumn = 0;
        }
    }

    private FileUploaderManager.UploaderResultListener uploaderResult = new FileUploaderManager.UploaderResultListener() {

        @Override
        public void onNetworkType(boolean cleanList) {
            closeUploading();
            if (cleanList) {
                uploaderManager.cleanFileList();
            }
            evaluate_ok.setEnabled(true);
            ToastUtils.showShortToastSafe("网络访问失败", 1000);
        }

        @Override
        public void onUploadProcessNumber(int successNumber, int fileListSize) {
            processUploading((successNumber + 1) + "/" + fileListSize);
        }

        @Override
        public void onUploaderFail(final List<String> failUrl, final int successNumber) {
            closeUploading();
            if (failUrl == null && successNumber == 0) {
                ToastUtils.showShortToastSafe("图片上传终止,请检查网络是否连接!", 1000);
                return;
            }
            if (messageDialog == null) {
                messageDialog = MessageDialog.newInstance();
            }
            if (!messageDialog.isVisible()) {
                messageDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        messageDialog.hideTitle();
                        messageDialog.setContent("有" + failUrl.size() + "张图片上传失败,请点击继续上传！");
                        messageDialog.setConfirmText("继续上传");
                        messageDialog.setConfirmOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showUploading();
                                uploaderManager.uploaderStringFile(failUrl, successNumber);
                                messageDialog.dismiss();
                            }
                        });
                        messageDialog.setCancelOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                evaluate_ok.setEnabled(true);
                                uploaderManager.cleanFileList();
                                messageDialog.dismiss();
                            }
                        });
                    }
                });

                messageDialog.show(getSupportFragmentManager(), "MessageDialog");
            }
        }

        @Override
        public void onUploaderSuccess(List<UploadResult.UploadInfo> uploadFileList) {
            closeAnimatorUploading();
            StringBuffer imgUriBuffer = new StringBuffer();// 拼接上传图片名称路径
            for (UploadResult.UploadInfo uploadInfo : uploadFileList) {
                imgUriBuffer.append(uploadInfo.getName());
                imgUriBuffer.append(",");
            }
            String s = imgUriBuffer.toString();
            String text = commment_edit.getText().toString();
            text = EmojiUtil.replaceEmoji(text);
            evaluatePresenter.submintComment(orderno, merchantno, star, text, s);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        cameraUtil.onHandleActivityResult(requestCode, resultCode, data);
    }
}
