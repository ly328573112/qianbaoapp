package com.haodaibao.fanbeiapp.module.invoice;

import com.baseandroid.base.IView;
import com.haodaibao.fanbeiapp.repository.json.InvoiceBean;

/**
 * date: 2018/2/5.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public interface MyInvoiceContract {
    interface Presenter {
        void getInvoiceList(String page, String pageSize);
    }

    interface View extends IView {
        void updateUI(InvoiceBean invoiceBean);
        void updateFailed();
    }
}
