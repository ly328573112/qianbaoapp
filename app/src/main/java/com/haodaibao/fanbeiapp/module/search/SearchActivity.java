package com.haodaibao.fanbeiapp.module.search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Constant;
import com.baseandroid.widget.RouteeFlowLayout;
import com.baseandroid.widget.customtext.edittextview.EditTextWithDelNormal;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.database.GreenDaoDbHelp;
import com.haodaibao.fanbeiapp.repository.json.SearchCacheEntity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by hepeng on 2018/4/25.
 */

public class SearchActivity extends BaseActivity {
    @BindView(R.id.search_etwd)
    EditTextWithDelNormal searchEtwd;
    @BindView(R.id.search_history)
    RouteeFlowLayout search_history;
    @BindView(R.id.iv_history_delete)
    ImageView iv_history_delete;
    @BindView(R.id.tv_cancle)
    TextView tv_cancle;
    @BindView(R.id.history_ly)
    LinearLayout history_ly;
    List<SearchCacheEntity> mSearchCacheEntities;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_search;
    }

    @Override
    protected void setupView() {
        searchEtwd.setOnEditorActionListener(editorActionListener);
        searchEtwd.postDelayed(new Runnable() {
            @Override
            public void run() {
                forceShowSoftInput(searchEtwd);
            }
        }, 100);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        mSearchCacheEntities = new ArrayList<>();
        mSearchCacheEntities.addAll(GreenDaoDbHelp.loadallSearchInfo());
//        history_ly.setVisibility(mSearchCacheEntities.size() == 0 ? View.GONE : View.VISIBLE);
        setSearchHistory();
    }

    /**
     * 强制显示软键盘
     *
     * @param view 接受软键盘输入的视图
     */
    private void forceShowSoftInput(View view) {
        InputMethodManager imm = ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE));
        if (imm != null && view != null) {
            imm.showSoftInput(view, InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    private void setSearchHistory() {
        search_history.removeAllViews();
        history_ly.setVisibility(mSearchCacheEntities.size() == 0 ? View.GONE : View.VISIBLE);
        for (int i = 0; i < mSearchCacheEntities.size(); i++) {
            TextView view = (TextView) LayoutInflater.from(this).inflate(R.layout.item_search_history, null);
            final SearchCacheEntity searchCacheEntity = mSearchCacheEntities.get(i);
            view.setText(searchCacheEntity.getSearchContent());
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String result = searchCacheEntity.getSearchContent();
                    searchEtwd.setText(result);
                    searchEtwd.setSelection(result.length());
                    Intent intent = new Intent(SearchActivity.this, SearchDeliciousActivity.class);
                    intent.putExtra("result", result);
                    startActivityForResult(intent, 0);
                }
            });
            search_history.addView(view);
        }
    }

    private TextView.OnEditorActionListener editorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                hideInput(SearchActivity.this, searchEtwd);
                String result = searchEtwd.getText().toString().trim();
                if (!TextUtils.isEmpty(result)) {
                    for (SearchCacheEntity searchCacheEntity : mSearchCacheEntities) {
                        if (searchCacheEntity.getSearchContent().equals(result)) {
                            mSearchCacheEntities.remove(searchCacheEntity);
                            GreenDaoDbHelp.deletSearchInfo(searchCacheEntity);
                            break;
                        }
                    }
                    SearchCacheEntity searchCacheEntity = new SearchCacheEntity();
                    searchCacheEntity.setTimeId("" + System.currentTimeMillis());
                    searchCacheEntity.setSearchContent(result);
                    mSearchCacheEntities.add(0, searchCacheEntity);
                    GreenDaoDbHelp.insertSearchInfo(searchCacheEntity);
                    setSearchHistory();
                    Intent intent = new Intent(SearchActivity.this, SearchDeliciousActivity.class);
                    intent.putExtra("result", result);
                    startActivityForResult(intent, 0);
                }
                return true;
            }
            return false;
        }
    };

    /**
     * 强制隐藏输入法键盘
     *
     * @param context Context
     * @param view    EditText
     */
    public static void hideInput(Context context, View view) {
        if (view == null) {
            return;
        }
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @OnClick({R.id.iv_history_delete, R.id.tv_cancle})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_history_delete:
                GreenDaoDbHelp.deleteallSearchInfo();
                mSearchCacheEntities.clear();
                search_history.removeAllViews();
                history_ly.setVisibility(View.GONE);
                break;
            case R.id.tv_cancle:
                hideInput(SearchActivity.this, searchEtwd);
                searchEtwd.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, 100);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Constant.SEARCH_RESULT) {
            searchEtwd.setText("");
        } else if (resultCode == Constant.SEARCH_BACK) {
            hideInput(SearchActivity.this, searchEtwd);
            searchEtwd.postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            }, 100);
        }
    }
}
