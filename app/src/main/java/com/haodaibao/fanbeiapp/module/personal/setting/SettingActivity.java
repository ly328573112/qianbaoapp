package com.haodaibao.fanbeiapp.module.personal.setting;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Global;
import com.baseandroid.utils.CacheUtil;
import com.baseandroid.utils.RxUtils;
import com.baseandroid.utils.StatusBarHelper;
import com.bumptech.glide.Glide;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.event.PersonalFragmentEvent;
import com.haodaibao.fanbeiapp.module.login.LoginActivity;
import com.haodaibao.fanbeiapp.module.personal.MoreActivity;
import com.haodaibao.fanbeiapp.module.personal.help.HelpActivity;
import com.haodaibao.fanbeiapp.module.suggest.SuggestActivity;
import com.haodaibao.fanbeiapp.repository.json.UserDate;
import com.jayfeng.lesscode.core.FileLess;
import com.jayfeng.lesscode.core.ToastLess;
import com.qianbao.shiningwhitelibrary.utils.ActivityJumpUtil;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class SettingActivity extends BaseActivity implements UserinfoSettingContract.View, View.OnClickListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.setting_help_rl)
    RelativeLayout settingHelpRl;
    @BindView(R.id.setting_feedback_ll)
    RelativeLayout settingFeedbackLl;
    @BindView(R.id.setting_guanyu_rl)
    RelativeLayout settingGuanyuRl;
    @BindView(R.id.setting_cache_rl)
    RelativeLayout settingCacheRl;
    @BindView(R.id.setting_qianbao_telephone_rl)
    RelativeLayout settingTelephoneRl;
    @BindView(R.id.setting_qianbao_bhh_rl)
    RelativeLayout settingBhhRl;
    @BindView(R.id.tv_logout)
    TextView tvLogout;
    @BindView(R.id.tv_cache)
    TextView mTvCache;

    private UserinfoSettingPresenter mPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_setting;
    }

    @Override
    protected void setupView() {
        mToolbarTitle.setText("设置");
        toolbarBack.setOnClickListener(this);
        settingHelpRl.setOnClickListener(this);
        settingFeedbackLl.setOnClickListener(this);
        settingGuanyuRl.setOnClickListener(this);
        settingCacheRl.setOnClickListener(this);
        settingTelephoneRl.setOnClickListener(this);
        settingBhhRl.setOnClickListener(this);
        tvLogout.setOnClickListener(this);
        StatusBarHelper.translateStatusBar(SettingActivity.this);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        if (mPresenter == null) {
            mPresenter = new UserinfoSettingPresenter(this);
        }
        try {
            mTvCache.setText(CacheUtil.getAllCacheSizeString(Global.getContext()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
            tvLogout.setVisibility(View.INVISIBLE);
        } else {
            tvLogout.setVisibility(View.VISIBLE);
        }

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.setting_help_rl:
                ActivityJumpUtil.next(this, HelpActivity.class);
                break;
            case R.id.setting_feedback_ll:
                if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                    ActivityJumpUtil.next(this, LoginActivity.class, null, 1000);
                } else {
                    ActivityJumpUtil.next(this, SuggestActivity.class);
                }
                break;
            case R.id.setting_guanyu_rl:
                ActivityJumpUtil.next(this, MoreActivity.class);
                break;
            case R.id.setting_cache_rl:
                clearCache();
                break;
            case R.id.setting_qianbao_telephone_rl:
                dial();
                break;
            case R.id.setting_qianbao_bhh_rl:
                dialBHH();
                break;
            case R.id.tv_logout:
                mPresenter.checkOut();
                break;
        }
    }

    /**
     * 清除缓存
     */
    protected void clearCache() {

        ToastLess.$(R.string.cache_clearing);

        Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> e) throws Exception {
                Glide.get(Global.getContext()).clearDiskCache();
                FileLess.$del(Global.getContext().getCacheDir());
                FileLess.$del(Global.getContext().getExternalCacheDir());

                try {
                    FileLess.$del(new File(Environment.getExternalStorageDirectory()
                            .getPath() + "/" + Global.getContext()
                            .getPackageName() + "/download"));
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                } catch (Exception ex_else) {
                    ex_else.printStackTrace();
                }

                e.onNext(true);
                e.onComplete();
            }
        }).compose(RxUtils.<Object>applySchedulersLifeCycle(SettingActivity
                .this)).subscribe(new Observer<Object>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Object value) {
                ToastLess.$(R.string.cache_clear_completed);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {
                try {
                    mTvCache.setText(CacheUtil.getAllCacheSizeString(Global.getContext()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void dial() {
        Intent dial = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(R.string.Tel)));
        dial.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(dial);
    }

    private void dialBHH() {
        Intent dial = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(R.string.bhh_tel)));
        dial.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(dial);
    }


    @Override
    public void setupUserinfo(UserDate bean) {

    }

    @Override
    public void uploadHeadViewSuccess() {

    }

    @Override
    public void checkOutSuccess() {
        Global.loginOutclear();
        PersonalFragmentEvent event = new PersonalFragmentEvent();
        event.setLogOut(true);
        EventBus.getDefault().post(event);
        finish();
    }
}
