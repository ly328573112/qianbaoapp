package com.haodaibao.fanbeiapp.module.personal.message;

import android.app.Activity;
import android.os.Bundle;

/**
 * 开发者：LuoYi
 * Time: 2017 20:28 2017/10/11 10
 */

public class MessageJumpBean {

    public Bundle aBundle;
    public Class<? extends Activity> aClass;
    public boolean closeActivity = false;

    public Bundle getaBundle() {
        return aBundle;
    }

    public void setaBundle(Bundle aBundle) {
        this.aBundle = aBundle;
    }

    public Class<? extends Activity> getaClass() {
        return aClass;
    }

    public void setaClass(Class<? extends Activity> aClass) {
        this.aClass = aClass;
    }
}
