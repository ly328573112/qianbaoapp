package com.haodaibao.fanbeiapp.imageadapter;

import com.baseandroid.camera.CameraUtil;

interface CameraView {

    void showCameraDialog(CameraUtil cameraUtil, int optionalNumber);

}