package com.haodaibao.fanbeiapp.imageadapter;

import android.os.Environment;
import android.support.design.widget.BottomSheetDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.base.IView;
import com.baseandroid.camera.CameraUtil;
import com.baseandroid.utils.ToastUtils;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.PhotoInfo;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * 开发者：LuoYi
 * Time: 2017 14:47 2017/9/21 09
 */

public class CameraSelectView implements CameraView, View.OnClickListener {
    public static String FILEPATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
    public static String FILENAME = "QianbaoComment_";
    public static String SUFFIX = ".png";

    private final BaseActivity activity;
    private BottomSheetDialog sheetDialog;

    private static final int COLUMN = 9;//图片可选数量
    private File file;
    private CameraUtil cameraUtil;
    private int optionalNumber;
    private List<PhotoInfo> photoList = new ArrayList<>();// 照片uri列表
    private boolean needResize = false;

    private CameraPotoListener cameraPotoListener;

    public CameraSelectView(IView evaluateView) {
        activity = (BaseActivity) evaluateView;
    }

    public void setCameraPotoListener(CameraPotoListener cameraPotoListener) {
        this.cameraPotoListener = cameraPotoListener;
    }

    public void setPictureCut(boolean needResize) {
        this.needResize = needResize;
    }

    @Override
    public void showCameraDialog(CameraUtil cameraUtil, int optionalNumber) {
        this.cameraUtil = cameraUtil;
        this.optionalNumber = optionalNumber;
        photoList.clear();

        View selectView = LayoutInflater.from(activity).inflate(R.layout.dialog_select, null);
        selectView.findViewById(R.id.btn_take_photo).setOnClickListener(this);
        selectView.findViewById(R.id.btn_pick_photo).setOnClickListener(this);
        selectView.findViewById(R.id.btn_cancel).setOnClickListener(this);

        sheetDialog = new BottomSheetDialog(activity);
        sheetDialog.setContentView(selectView);
        sheetDialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_take_photo:
                if (optionalNumber == 0) {
                    ToastUtils.showShortToastSafe("抱歉！最多只能选择" + COLUMN + "张图片", 1000);
                } else {
                    file = new File(FILEPATH + "/" + FILENAME + getSystemTime() + SUFFIX);
                    cameraUtil.openCamera(needResize, new CameraUtil.ResultCallback() {
                        @Override
                        public void getResult(List<File> saveFile) {
                            PhotoInfo info = new PhotoInfo(0, saveFile.get(0).toString(), false);
                            photoList.add(info);
                            cameraPotoListener.onResultPotoList(photoList);
                        }

                        @Override
                        public void onError(int code, String message) {
                            ToastUtils.showShortToastSafe(message);
                        }
                    }, file);
                }
                sheetDialog.dismiss();
                break;
            case R.id.btn_pick_photo:
                if (optionalNumber == 0) {
                    ToastUtils.showShortToastSafe("抱歉！最多只能选择" + COLUMN + "张图片", 1000);
                } else {
                    file = new File(FILEPATH + "/" + FILENAME + getSystemTime() + SUFFIX);
                    cameraUtil.openPicture(needResize, optionalNumber, new CameraUtil.ResultCallback() {
                        @Override
                        public void getResult(List<File> saveFile) {
                            for (File file : saveFile) {
                                PhotoInfo info = new PhotoInfo(0, file.toString(), false);
                                photoList.add(info);
                            }
                            cameraPotoListener.onResultPotoList(photoList);
                        }

                        @Override
                        public void onError(int code, String message) {
                            ToastUtils.showShortToastSafe(message);
                        }
                    }, file);
                }
                sheetDialog.dismiss();
                break;
            case R.id.btn_cancel:
                sheetDialog.dismiss();
                break;
            default:
                break;
        }
    }

    private String getSystemTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyMMddHHmmss", Locale.getDefault());
        Date curDate = new Date(System.currentTimeMillis());//获取当前时间
        return formatter.format(curDate);
    }

    public interface CameraPotoListener {

        void onResultPotoList(List<PhotoInfo> photoList);

    }
}
