package com.haodaibao.fanbeiapp.imageadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.baseandroid.widget.recycleadpter.BaseRecycleViewAdapter;
import com.bumptech.glide.Glide;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.PhotoInfo;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by hdb on 2017/7/10.
 */

public class PhotoImageAdapter extends BaseRecycleViewAdapter<PhotoInfo> {

    private ImageClickListener imageClickListener;

    public PhotoImageAdapter(Context context) {
        super(context);
    }


    public void setImageClickListener(ImageClickListener imageClickListener) {
        this.imageClickListener = imageClickListener;
    }

    @Override
    protected void onBindBaseViewHolder(final RecyclerView.ViewHolder holder, PhotoInfo item) {
        if (holder instanceof ImageHolder) {
            ImageHolder imageHolder = (ImageHolder) holder;
            /* 是否处于可删除状态 */
            boolean isDelete = item.isDelete();
            if (isDelete) {
                imageHolder.ivDelete.setVisibility(View.VISIBLE);
            } else {
                imageHolder.ivDelete.setVisibility(View.GONE);
            }

            String imgUri = item.getPhotoUri();
            if (imgUri == null) {
                Glide.with(mContext).load(R.drawable.icon_photo_camera).into(imageHolder.ivPhoto);
            } else {
                Glide.with(mContext).load(imgUri).error(R.drawable.ic_split_graph).into(imageHolder.ivPhoto);
            }
            imageHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    imageClickListener.openCamera(holder.getLayoutPosition());
                }
            });

            imageHolder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageClickListener.onDeleteImage(holder.getLayoutPosition());
                }
            });
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new ImageHolder(View.inflate(mContext, R.layout.comment_image_item, null));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }


    class ImageHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_photo)
        ImageView ivPhoto;
        @BindView(R.id.iv_delete)
        ImageView ivDelete;


        ImageHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
