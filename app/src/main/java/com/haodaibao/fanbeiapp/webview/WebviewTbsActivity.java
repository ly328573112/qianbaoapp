package com.haodaibao.fanbeiapp.webview;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Global;
import com.baseandroid.gaode.GaoDeMap;
import com.baseandroid.okhttp.OkHttpUtils;
import com.baseandroid.okhttp.StringCallbackWeakReference;
import com.baseandroid.retrofit.OkHttpClientManager;
import com.baseandroid.utils.AndroidUtils;
import com.baseandroid.utils.LogUtil;
import com.baseandroid.utils.RxUtils;
import com.baseandroid.utils.TimeUtils;
import com.baseandroid.utils.ToastUtils;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.haodaibao.fanbeiapp.BuildConfig;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.UIApplication;
import com.haodaibao.fanbeiapp.database.GreenDaoDbHelp;
import com.haodaibao.fanbeiapp.event.CitySelectChangEvent;
import com.haodaibao.fanbeiapp.event.ShowFindEvent;
import com.haodaibao.fanbeiapp.module.home.HomeActivity;
import com.haodaibao.fanbeiapp.module.login.LoginActivity;
import com.haodaibao.fanbeiapp.module.merchant.MerchantDetailActivity;
import com.haodaibao.fanbeiapp.module.personal.coupon.CouponActivity;
import com.haodaibao.fanbeiapp.module.search.SearchDeliciousActivity;
import com.haodaibao.fanbeiapp.repository.RxObserver;
import com.haodaibao.fanbeiapp.repository.UserInfoRepository;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.OpenCityBean;
import com.haodaibao.fanbeiapp.repository.json.UserDate;
import com.haodaibao.fanbeiapp.wxapi.WXEntryActivity;
import com.jayfeng.lesscode.core.DisplayLess;
import com.qianbao.shiningwhitelibrary.BHHManager;
import com.tencent.smtt.export.external.interfaces.SslError;
import com.tencent.smtt.export.external.interfaces.SslErrorHandler;
import com.tencent.smtt.sdk.CookieManager;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static com.baseandroid.config.Constant.ERROR_LOGOUT;
import static com.haodaibao.fanbeiapp.database.GreenDaoDbHelp.loadallCityInfo;

/**
 * Created by hepeng on 2018/4/12.
 */

public class WebviewTbsActivity extends BaseActivity {
    @BindView(R.id.webview)
    WebView webview;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @BindView(R.id.progress)
    TextView progress;
    @BindView(R.id.toolbar_back)
    View toolbar_back;
    @BindView(R.id.right_text)
    TextView right_text;
    public static final String MINITTITLE_KEY = "mInitTitle";
    public static final String MINIURL_KEY = "url";
    private String url;
    protected HashMap<String, String> mWebViewHeader;
    private String mInitTitle;
    private HashMap<String, String> mCallBackMethodMap = new HashMap<>();

    public static void start(Context context, String mInitTitle, String url) {
        Intent intent = new Intent(context, WebviewTbsActivity.class);
        intent.putExtra(MINITTITLE_KEY, mInitTitle);
        intent.putExtra(MINIURL_KEY, url);
        context.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_webview_tbs;
    }

    @Override
    protected void setupView() {
        initWebview();
    }

    private void initWebview() {
        WebSettings webSettings = webview.getSettings();
        //如果访问的页面中要与Javascript交互，则webview必须设置支持Javascript
        //若加载的html里有JS在执行动画等操作，会造成资源浪费（CPU、电量）在onStop和onResume
        //里分别把 setJavaScriptEnabled()给设置成false和true即可
        webSettings.setJavaScriptEnabled(true);
        // 设置允许JS弹窗
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        //设置自适应屏幕，两者合用
        webSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
        webSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小
        //缩放操作
        //webSettings.setSupportZoom(true); //支持缩放，默认为true。是下面二个的前提。
        //webSettings.setBuiltInZoomControls(true); //设置内置的缩放控件。若为false，则该WebView不可缩放
        //webSettings.setDisplayZoomControls(false); //隐藏原生的缩放控件
        webSettings.setDomStorageEnabled(true); // 开启 DOM storage API 功能
        webSettings.setDatabaseEnabled(true);   //开启 database storage API 功能
        //String cacheDirPath = getFilesDir().getAbsolutePath() + APP_CACAHE_DIRNAME;
        //webSettings.setAppCachePath(cacheDirPath); //设置  Application Caches 缓存目录
        webSettings.setAppCacheEnabled(false);//开启 Application Caches 功能
        //设置WebView是否使用其内置的变焦机制，该机制结合屏幕缩放控件使用，默认是false，不使用内置变焦机制。
        webSettings.setAllowContentAccess(false);
        //设置WebView是否保存表单数据，默认true，保存数据。
        webSettings.setSaveFormData(true);
        //缓存模式如下：
        //LOAD_CACHE_ONLY: 不使用网络，只读取本地缓存数据
        //LOAD_DEFAULT: （默认）根据cache-control决定是否从网络上取数据。
        //LOAD_NO_CACHE: 不使用缓存，只从网络获取数据.
        //LOAD_CACHE_ELSE_NETWORK，只要本地有，无论是否过期，或者no-cache，都使用缓存中的数据。
        if (AndroidUtils.isNetworkAvailable()) {
            webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        } else {
            webSettings.setCacheMode(WebSettings.LOAD_CACHE_ONLY);
        }
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        mWebViewHeader = new HashMap<>();
        mWebViewHeader.put("platform", "Android");
        mWebViewHeader.put("version", BuildConfig.VERSION_CODE + "");
        mWebViewHeader.put("_ac_token", Global.getUserTokenInfo().getAc_token());
        mWebViewHeader.put("channel", "02");
        mWebViewHeader.put("Custom-Agent", Build.MODEL + ";" + Build.VERSION.SDK_INT + ";" + BuildConfig.APPLICATION_ID + ";" + BuildConfig.VERSION_CODE + ";" + BuildConfig.VERSION_NAME);
        webview.setWebChromeClient(mWebChromeClient);
        webview.setWebViewClient(mWebViewClient);
        webview.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    webview.loadUrl(url, mWebViewHeader);
                } catch (Exception e) {

                }
            }
        }, 200);
    }

    private void initTitle() {
        toolbar_title.setText(mInitTitle);
        right_text.setTextColor(ContextCompat.getColor(this, R.color.text_gray3));
        right_text.setText("刷新");
        right_text.setVisibility(View.VISIBLE);
    }

    @OnClick({R.id.toolbar_back, R.id.right_text})
    public void onclick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                if (webview.canGoBack()) {
                    webview.goBack();
                } else {
                    finish();
                }
                break;
            case R.id.right_text:
                webview.reload();
                break;

        }
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        Intent intent = getIntent();
        mInitTitle = intent.getStringExtra(MINITTITLE_KEY);
        url = intent.getStringExtra(MINIURL_KEY);
        if (TextUtils.isEmpty(url)) {
            Bundle extras = intent.getExtras();
            url = extras.getString(MINITTITLE_KEY);
        }
        initTitle();
    }

    //辅助WebView处理Javascript的对话框,网站图标,网站标题等
    private WebChromeClient mWebChromeClient = new WebChromeClient() {

        private String mTitle;

        //获取Web页中的标题
        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
//            this.mTitle = title;
//            toolbar_title.setText(mTitle);
        }

        //获得网页的加载进度并显示
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            if (!isFinishing()) {
                if (newProgress < 99) {
                    toolbar_title.setText(getResources().getString(R.string.webview_start_load));
                } else {
                    toolbar_title.setText(mInitTitle);
                }

                if (newProgress == 100) {
                    progress.setVisibility(View.GONE);
                } else {
                    progress.setVisibility(View.VISIBLE);
                    ViewGroup.LayoutParams lp = progress.getLayoutParams();
                    lp.width = DisplayLess.$width(WebviewTbsActivity.this) * newProgress / 100;
                    progress.setLayoutParams(lp);
                    LogUtil.e("width=" + lp.width);
                }
            }
        }
    };

    public void goJsEnableQB() {
        exec("javascript:enableQB();");
    }

    private WebViewClient mWebViewClient = new WebViewClient() {

        //复写shouldOverrideUrlLoading()方法，使得打开网页时不调用系统浏览器，
        //而是在本WebView中显示
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Timber.e("url = " + url);
            goJsEnableQB();
            Uri uriparse = Uri.parse(url);
            if (url.contains("qianbao:")) {
                if (url.contains("qianbao://requset")) {
                    String httpurl = uriparse.getQueryParameter("url");
                    String httpmethod = uriparse.getQueryParameter("method");
                    String httpbody = uriparse.getQueryParameter("body");
                    String callbackMethod = uriparse.getQueryParameter("callback");
                    mCallBackMethodMap.put(callbackMethod, callbackMethod);
                    requestHttpMethod(httpurl, httpmethod, httpbody, callbackMethod);
                    return true;
                } else if (url.contains("qianbao://goNative")) {
                    String page = uriparse.getQueryParameter("page");
                    String params = uriparse.getQueryParameter("params");
                    Timber.e("page=" + page);
                    Timber.e("params=" + params);
                    if (TextUtils.equals(page, "merchant_list")) {
                        Map<String, String> hashMap = new Gson().fromJson(params, new TypeToken<HashMap<String, String>>() {
                        }.getType());
                        String cityType = hashMap.get("cityType");
                        String cityCode = hashMap.get("cityCode");
                        String cityName = hashMap.get("cityName");
//                        Intent intent = new Intent(WebviewTbsActivity.this, SearchDeliciousActivity.class);
//                        intent.putExtra("longitude", TextUtils.isEmpty(hashMap.get("longitude")) ? Global.getMyLocation().longitude + "" : hashMap.get("longitude"));
//                        intent.putExtra("latitude", TextUtils.isEmpty(hashMap.get("latitude")) ? Global.getMyLocation().latitude + "" : hashMap.get("latitude"));
//                        intent.putExtra("keyword", hashMap.get("keyword"));
//                        intent.putExtra("categoryNo", hashMap.get("categoryNo"));
//                        intent.putExtra("cityType", TextUtils.isEmpty(hashMap.get("cityType")) ? Global.getSelectCitySiteType() : hashMap.get("cityType"));
//                        intent.putExtra("cityCode", TextUtils.isEmpty(hashMap.get("cityCode")) ? Global.getSelectCityCode() : hashMap.get("cityCode"));
//                        //                        intent.putExtra("sousuo", "sousuo");
//                        startActivity(intent);

                        List<OpenCityBean> openCityBeans = GreenDaoDbHelp.queryCityByGaodeNameAdCode().list();
                        CitySelectChangEvent changEvent = new CitySelectChangEvent();
                        if (!TextUtils.isEmpty(cityCode)) {
                            if (openCityBeans.size() > 0) {
                                if (openCityBeans.get(0).getCode().equals(cityCode)) {
                                    changEvent.currentLocation = true;
                                } else {
                                    changEvent.currentLocation = false;
                                }
                            }
//                            List<OpenCityBean> citysList = loadallCityInfo();
                            List<OpenCityBean> list = GreenDaoDbHelp.queryCityInfoByCode(cityCode).list();
                            if (list.size() > 0) {
                                Global.setSelectCity(list.get(0).getName());
                                Global.setSelectCityCode(cityCode);
                                Global.setSelectCitySiteType(list.get(0).getType());
                            }
                        } else {
                            changEvent.currentLocation = true;
                            if (openCityBeans.size() > 0) {
                                Global.setSelectCity(openCityBeans.get(0).getName());
                                Global.setSelectCityCode(openCityBeans.get(0).getCode());
                                Global.setSelectCitySiteType(openCityBeans.get(0).getType());
                            }

                        }
                        EventBus.getDefault().post(changEvent);
                        EventBus.getDefault().post(new ShowFindEvent());
                        finish();
                    } else if (TextUtils.equals(page, "merchant_detail")) {
                        Map<String, String> hashMap = new Gson().fromJson(params, new TypeToken<HashMap<String, String>>() {
                        }.getType());
                        Intent intent = new Intent(WebviewTbsActivity.this, MerchantDetailActivity.class);
                        intent.putExtra("merchantno", hashMap.get("merchantNo"));
                        startActivity(intent);
                    } else if (TextUtils.equals(page, "coupon_list")) {
                        if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                            Intent intent = new Intent(WebviewTbsActivity.this, LoginActivity.class);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(WebviewTbsActivity.this, CouponActivity.class);
                            startActivity(intent);
                        }

                    } else if (TextUtils.equals(page, "baihuahua")) {
                        if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                            Intent intent = new Intent(WebviewTbsActivity.this, LoginActivity.class);
                            intent.putExtra("Bhh", "Bhh");
                            startActivity(intent);
                        } else {
                            BHHManager.openViewForRelease(WebviewTbsActivity.this, Global.getUserInfo().bhhAcessToken, Global.getUserInfo().getUserno(), false);
                        }
                    } else if (TextUtils.equals(page, "login")) {
                        Intent intent = new Intent(WebviewTbsActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                    return true;
                } else if (url.contains("qianbao://getInfo")) {
                    if (TextUtils.isEmpty(Global.getUserInfo().getUserno())) {
                        goJsResultCallbackHttp("getInfo", 1, "0");
                    } else {
                        UserInfoRepository.getInstance().getUserinfo().subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .compose(RxUtils.<Data<UserDate>>bindToLifecycle(WebviewTbsActivity.this))
                                .subscribe(new RxObserver<Data<UserDate>>() {
                                    @Override
                                    public void onNext(@NonNull Data<UserDate> data) {
                                        if (checkJsonCode(data, false)) {
                                            goJsResultCallbackHttp("getInfo", 1, "1");
                                        } else {
                                            if (!TextUtils.isEmpty(OkHttpClientManager.getHttpCookieString()) && data
                                                    .getStatus()
                                                    .endsWith(ERROR_LOGOUT)) {
                                                goJsResultCallbackHttp("getInfo", 1, "0");
                                            }
                                        }
                                    }
                                });
                    }

                }
                return true;
            }

//            view.loadUrl(url, mWebViewHeader);
//            return true;
            return super.shouldOverrideUrlLoading(view, url);
        }

        //开始载入页面调用的
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        //在页面加载结束时调用
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        //在加载页面资源时会调用，每一个资源（比如图片）的加载都会调用一次。
        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
        }

        @Override
        public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
            //handler.cancel(); 默认的处理方式，WebView变成空白页
            //接受证书
            sslErrorHandler.proceed();
            //handleMessage(Message msg); 其他处理
        }

        //加载页面的服务器出现错误时（如404）调用
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            try {
                webview.setVisibility(View.GONE);
                //mEmptyView.setVisibility(View.VISIBLE);
                toolbar_title.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        toolbar_title.setText(mInitTitle);
                    }
                }, 200);
            } catch (Exception e) {

            }

        }

    };

    private void requestHttpMethod(String url, String method, String body, final String callbackMethod) {
        if (method.equalsIgnoreCase("get")) {
            Map<String, String> hashMap = new Gson().fromJson(body, new TypeToken<HashMap<String, String>>() {
            }.getType());
            OkHttpUtils.get(url)
                    .queries(hashMap)
                    .execute(new StringCallbackWeakReference(WebviewTbsActivity.this) {
                        @Override
                        public void handleHttpRespone(String response) {
                            goJsResultCallbackHttp(mCallBackMethodMap.get(callbackMethod), 1, response);
                        }

                        @Override
                        public boolean handleException(Throwable throwable) {
                            goJsResultCallbackHttp(mCallBackMethodMap.get(callbackMethod), 0, "{errMsg:'错误了'}");
                            return super.handleException(throwable);
                        }
                    });

        } else if (method.equalsIgnoreCase("post")) {
            Map<String, String> hashMap = new Gson().fromJson(body, new TypeToken<HashMap<String, String>>() {
            }.getType());
            if (hashMap.size() == 0) {
                hashMap.put("xxx", "xxx");
            }
            OkHttpUtils.post(url)
                    .forms(hashMap)
                    .execute(new StringCallbackWeakReference(WebviewTbsActivity.this) {
                        @Override
                        public void handleHttpRespone(String response) {
                            goJsResultCallbackHttp(mCallBackMethodMap.get(callbackMethod), 1, response);
                        }

                        @Override
                        public boolean handleException(Throwable throwable) {
                            goJsResultCallbackHttp(mCallBackMethodMap.get(callbackMethod), 0, "{errMsg:'错误了'}");
                            return super.handleException(throwable);
                        }
                    });
        }
    }

    public void goJsResultCallbackHttp(String callbackname, int resultCode, String json) {
        //exec("javascript:enableQB();");
        exec("javascript:" + callbackname + "(" + resultCode + ", " + json + ");");
    }

    private void exec(final String trigger) {
        if (Build.VERSION.SDK_INT >= 19) {// 4.4以上支持
            if (webview != null) {
                webview.evaluateJavascript(trigger, null);
            }
        } else {
            if (webview != null) {
                webview.loadUrl(trigger);
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if (webview.canGoBack()) {
                    webview.goBack();
                } else {
                    finish();
                }
                break;

            default:
                break;
        }
        return super.onKeyDown(keyCode, event);
    }
}
