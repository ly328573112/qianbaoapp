package com.haodaibao.fanbeiapp.webview;

import android.support.v4.content.ContextCompat;
import android.view.View;

import com.baseandroid.base.BaseWebviewActivity;
import com.haodaibao.fanbeiapp.R;

public class WebviewActivity extends BaseWebviewActivity {
    @Override
    protected void provideHeader() {

    }

    @Override
    protected void setupView() {
        super.setupView();

        right_text.setVisibility(View.VISIBLE);
        right_text.setTextColor(ContextCompat.getColor(this, R.color.text_gray3));
        right_text.setText("刷新");
        right_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webview.loadUrl(webview.getUrl());
            }
        });
    }
}
