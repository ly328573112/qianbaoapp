package com.haodaibao.fanbeiapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.baseandroid.config.Constant;
import com.baseandroid.config.Global;
import com.github.yuweiguocn.library.greendao.MigrationHelper;
import com.haodaibao.fanbeiapp.greedaodb.DaoMaster;
import com.haodaibao.fanbeiapp.greedaodb.DaoSession;
import com.haodaibao.fanbeiapp.greedaodb.DistrictsBeanDao;
import com.haodaibao.fanbeiapp.greedaodb.OpenCityBeanDao;
import com.haodaibao.fanbeiapp.greedaodb.SearchCacheEntityDao;
import com.haodaibao.fanbeiapp.greedaodb.UserDao;
import com.haodaibao.fanbeiapp.repository.json.DistrictsBean;
import com.haodaibao.fanbeiapp.repository.json.OpenCityBean;
import com.haodaibao.fanbeiapp.repository.json.SearchCacheEntity;

import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.List;

import static com.haodaibao.fanbeiapp.greedaodb.DaoMaster.OpenHelper;

public class GreenDaoDbHelp extends DaoMaster.OpenHelper {

    private static final String DATABASE_NAME = "dababase_db";

    public GreenDaoDbHelp(Context context, String name, SQLiteDatabase.CursorFactory factory) {
        super(context, name, factory);
    }

    private static DaoMaster mDaoMaster;
    private static DaoSession mDaoSession;

    public static DaoMaster getDaoMaster(Context context) {
        if (mDaoMaster == null) {
            OpenHelper helper = new GreenDaoDbHelp(context, DATABASE_NAME, null);
            mDaoMaster = new DaoMaster(helper.getWritableDatabase());
        }
        return mDaoMaster;
    }

    public static DaoSession getDaoSession(Context context) {
        if (mDaoSession == null) {
            if (mDaoMaster == null) {
                mDaoMaster = getDaoMaster(context);
            }
            mDaoSession = mDaoMaster.newSession();
        }
        return mDaoSession;
    }

    @Override
    public void onUpgrade(Database db, int oldVersion, int newVersion) {
        // all dao must be as param
        MigrationHelper.migrate(db, new MigrationHelper.ReCreateAllTableListener() {
            @Override
            public void onCreateAllTables(Database db, boolean ifNotExists) {
                DaoMaster.createAllTables(db, ifNotExists);
            }

            @Override
            public void onDropAllTables(Database db, boolean ifExists) {
                DaoMaster.dropAllTables(db, ifExists);
            }
        }, UserDao.class, OpenCityBeanDao.class, DistrictsBeanDao.class, SearchCacheEntityDao.class);
    }

    @NonNull
    public static void insertallCityInfo(List<OpenCityBean> openCityInfoEntities) {
        List<OpenCityBean> nullCityList = new ArrayList<>();
        for (OpenCityBean info : openCityInfoEntities) {
            String cityName = info.getName();
            if (TextUtils.isEmpty(cityName)) {
                nullCityList.add(info);
                continue;
            }
            if (cityName.contains("市")) {
                cityName = cityName.substring(0, cityName.length() - 1);
            }
            info.setName(cityName);
        }
        openCityInfoEntities.removeAll(nullCityList);
        GreenDaoDbHelp.getDaoSession(Global.getContext()).getOpenCityBeanDao().insertOrReplaceInTx(openCityInfoEntities);
    }

    public static void deleteallCityInfo() {
        GreenDaoDbHelp.getDaoSession(Global.getContext()).getOpenCityBeanDao().deleteAll();
    }

    @NonNull
    public static QueryBuilder queryCityByGaodeNameAdCode() {
        if (!TextUtils.isEmpty(Global.getOpenCitysResp().getCitySvitch()) && Global.getOpenCitysResp().getCitySvitch().equals("1")) {
            if (TextUtils.isEmpty(Global.getMyLocation()
                    .getCityNameOrig()) && TextUtils.isEmpty(Global.getMyLocation().getDistrictName())) {
                QueryBuilder qbnamesearch = GreenDaoDbHelp.getDaoSession(Global.getContext()).getOpenCityBeanDao().queryBuilder();
                // return empty list
                qbnamesearch.where(OpenCityBeanDao.Properties.Code.eq("888888"));
                return qbnamesearch;
            }
            // 按区域名称查找
            String districName = Global.getMyLocation().getDistrictName();
            QueryBuilder qbnamesearch = queryOpenCityBydistricName(districName);
            if (qbnamesearch.list().size() > 0) {
                return qbnamesearch;
            }
            // 按区域名称删除市查找
            if (districName.endsWith("市")) {
                districName = districName.substring(0, districName.length() - 1);
                qbnamesearch = queryOpenCityBydistricName(districName);
                if (qbnamesearch.list().size() > 0) {
                    return qbnamesearch;
                }
            }
            // 按城市名称查找
            String cityName = Global.getMyLocation().getCityNameOrig();
            qbnamesearch = GreenDaoDbHelp.getDaoSession(Global.getContext()).getOpenCityBeanDao().queryBuilder();
            qbnamesearch.where(OpenCityBeanDao.Properties.AreaName.eq(cityName));
            if (qbnamesearch.list().size() > 0) {
                return qbnamesearch;
            }
            qbnamesearch = GreenDaoDbHelp.getDaoSession(Global.getContext()).getOpenCityBeanDao().queryBuilder();
            qbnamesearch.where(OpenCityBeanDao.Properties.Name.eq(cityName));
            if (qbnamesearch.list().size() > 0) {
                return qbnamesearch;
            }
            // 按城市名称删除市查找
            if (cityName.endsWith("市")) {
                cityName = cityName.substring(0, cityName.length() - 1);
                qbnamesearch = GreenDaoDbHelp.getDaoSession(Global.getContext()).getOpenCityBeanDao().queryBuilder();
                qbnamesearch.where(OpenCityBeanDao.Properties.AreaName.eq(cityName));
                if (qbnamesearch.list().size() > 0) {
                    return qbnamesearch;
                }
                qbnamesearch = GreenDaoDbHelp.getDaoSession(Global.getContext()).getOpenCityBeanDao().queryBuilder();
                qbnamesearch.where(OpenCityBeanDao.Properties.Name.eq(cityName));
                if (qbnamesearch.list().size() > 0) {
                    return qbnamesearch;
                }
            }
            return qbnamesearch;
        } else {
            QueryBuilder qbsearch = GreenDaoDbHelp.getDaoSession(Global.getContext()).getOpenCityBeanDao().queryBuilder();
            String adCode = Constant.SELECTCITYAD;
            if (!TextUtils.isEmpty(Global.getMyLocation().getAdCode())) {
                adCode = Global.getMyLocation().getAdCode();
            }
            // 开放站点进行查找
            qbsearch.where(OpenCityBeanDao.Properties.Code.eq(adCode));
            if (qbsearch.list().size() > 0) {
                return qbsearch;
            }
            // 没有查找到，总表中进行查询
            QueryBuilder qbDistsearch = queryDistrictsInfoByCode(adCode);
            if (qbDistsearch.list().size() > 0) {
                List<DistrictsBean> districtsBeanList = qbDistsearch.list();
                adCode = districtsBeanList.get(0).getCitycode();
                QueryBuilder qbresearch = GreenDaoDbHelp.getDaoSession(Global.getContext()).getOpenCityBeanDao().queryBuilder();
                // 开放站点进行查找
                qbresearch.where(OpenCityBeanDao.Properties.Code.eq(adCode));
                if (qbresearch.list().size() > 0) {
                    List<OpenCityBean> openCityBeans = qbsearch.list();
                    return qbresearch;
                }
            }
            return qbsearch;
        }
    }

    @Nullable
    private static QueryBuilder queryOpenCityBydistricName(String districName) {
        QueryBuilder qbnamesearch = GreenDaoDbHelp.getDaoSession(Global.getContext()).getOpenCityBeanDao().queryBuilder();
        qbnamesearch.where(OpenCityBeanDao.Properties.AreaName.eq(districName));
        if (qbnamesearch.list().size() > 0) {
            if (qbnamesearch.list().size() == 1) {
                // 当查找到一项,且该项属于市单位,唯一则直接返回
                if ("1".equals(((OpenCityBean) qbnamesearch.list().get(0)).getType())) {
                    return qbnamesearch;
                }
            }
            List<OpenCityBean> openCityBeans = qbnamesearch.list();
            for (OpenCityBean openCityBean : openCityBeans) {
                List<DistrictsBean> districtsBeans = queryDistrictsInfoByCode(openCityBean.getCode()).list();
                if (districtsBeans.size() > 0) {
                    String cityname = districtsBeans.get(0).getCityName();
                    String gaodeCityname1 = Global.getMyLocation().getCityNameOrig();
                    String gaodeCityname2 = gaodeCityname1;
                    if (gaodeCityname1.endsWith("市")) {
                        gaodeCityname2 = gaodeCityname1.substring(0, gaodeCityname1.length() - 1);
                    }
                    if (cityname.equals(gaodeCityname1) || cityname.equals(gaodeCityname2)) {
                        qbnamesearch = GreenDaoDbHelp.getDaoSession(Global.getContext()).getOpenCityBeanDao().queryBuilder();
                        qbnamesearch.where(OpenCityBeanDao.Properties.AreaName.eq(districtsBeans
                                .get(0).getName()), OpenCityBeanDao.Properties.Code.eq(districtsBeans.get(0).getCode()));
                        return qbnamesearch;
                    }
                }
            }
            qbnamesearch = GreenDaoDbHelp.getDaoSession(Global.getContext()).getOpenCityBeanDao().queryBuilder();
            qbnamesearch.where(OpenCityBeanDao.Properties.Code.eq("88888888"));
            return qbnamesearch;
        }
        qbnamesearch = GreenDaoDbHelp.getDaoSession(Global.getContext()).getOpenCityBeanDao().queryBuilder();
        qbnamesearch.where(OpenCityBeanDao.Properties.Name.eq(districName));
        if (qbnamesearch.list().size() > 0) {
            if (qbnamesearch.list().size() == 1) {
                // 当查找到一项,且该项属于市单位,唯一则直接返回
                if ("1".equals(((OpenCityBean) qbnamesearch.list().get(0)).getType())) {
                    return qbnamesearch;
                }
            }
            List<OpenCityBean> openCityBeans = qbnamesearch.list();
            for (OpenCityBean openCityBean : openCityBeans) {
                List<DistrictsBean> districtsBeans = queryDistrictsInfoByCode(openCityBean.getCode()).list();
                if (districtsBeans.size() > 0) {
                    String cityname = districtsBeans.get(0).getCityName();
                    String gaodeCityname1 = Global.getMyLocation().getCityNameOrig();
                    String gaodeCityname2 = gaodeCityname1;
                    if (gaodeCityname1.endsWith("市")) {
                        gaodeCityname2 = gaodeCityname1.substring(0, gaodeCityname1.length() - 1);
                    }
                    if (cityname.equals(gaodeCityname1) || cityname.equals(gaodeCityname2)) {
                        qbnamesearch = GreenDaoDbHelp.getDaoSession(Global.getContext()).getOpenCityBeanDao().queryBuilder();
                        qbnamesearch.where(OpenCityBeanDao.Properties.Name.eq(districtsBeans
                                .get(0).getName()), OpenCityBeanDao.Properties.Code.eq(districtsBeans.get(0).getCode()));
                        return qbnamesearch;
                    }
                }
            }
            qbnamesearch = GreenDaoDbHelp.getDaoSession(Global.getContext()).getOpenCityBeanDao().queryBuilder();
            qbnamesearch.where(OpenCityBeanDao.Properties.Code.eq("88888888"));
            return qbnamesearch;
        }

        return qbnamesearch;
    }

    @NonNull
    public static QueryBuilder queryCityInfoByName(String cityName) {
        QueryBuilder qb = GreenDaoDbHelp.getDaoSession(Global.getContext()).getOpenCityBeanDao().queryBuilder();
        qb.where(OpenCityBeanDao.Properties.Name.eq(cityName));
        return qb;
    }

    @NonNull
    public static QueryBuilder queryCityInfoByCode() {
        QueryBuilder qb = GreenDaoDbHelp.getDaoSession(Global.getContext()).getOpenCityBeanDao().queryBuilder();
        qb.where(OpenCityBeanDao.Properties.Code.eq(Global.getSelectCityCode()));
        return qb;
    }
    @NonNull
    public static QueryBuilder queryCityInfoByCode(String code) {
        QueryBuilder qb = GreenDaoDbHelp.getDaoSession(Global.getContext()).getOpenCityBeanDao().queryBuilder();
        qb.where(OpenCityBeanDao.Properties.Code.eq(code));
        return qb;
    }
    public static QueryBuilder queryCityInfoLike(String key) {
        QueryBuilder qb = GreenDaoDbHelp.getDaoSession(Global.getContext()).getOpenCityBeanDao().queryBuilder();
        qb.where(OpenCityBeanDao.Properties.Name.like("%" + key + "%"));
        return qb;
    }

    public static List<OpenCityBean> loadallCityInfo() {
        return GreenDaoDbHelp.getDaoSession(Global.getContext()).getOpenCityBeanDao().loadAll();
    }

    /*----insert all DistrictsBean-----*/
    @NonNull
    public static void insertallDistrictsInfo(List<DistrictsBean> districtsBeanList) {
        GreenDaoDbHelp.getDaoSession(Global.getContext()).getDistrictsBeanDao().insertOrReplaceInTx(districtsBeanList);
    }

    @NonNull
    public static QueryBuilder queryDistrictsInfoByCode(String adCode) {
        QueryBuilder qb = GreenDaoDbHelp.getDaoSession(Global.getContext()).getDistrictsBeanDao().queryBuilder();
        qb.where(DistrictsBeanDao.Properties.Code.eq(adCode));
        return qb;
    }

    public static List<DistrictsBean> loadallDistrictsInfo() {
        return GreenDaoDbHelp.getDaoSession(Global.getContext()).getDistrictsBeanDao().loadAll();
    }

    public static List<SearchCacheEntity> loadallSearchInfo() {
        QueryBuilder qb = GreenDaoDbHelp.getDaoSession(Global.getContext()).getSearchCacheEntityDao().queryBuilder();
        //        qb.orderAsc().list();
        qb.orderDesc(SearchCacheEntityDao.Properties.Id).list();
        return qb.list();
    }

    public static void insertSearchInfo(SearchCacheEntity searchCacheEntity) {
        GreenDaoDbHelp.getDaoSession(Global.getContext()).getSearchCacheEntityDao().insertOrReplaceInTx(searchCacheEntity);
    }

    public static void deletSearchInfo(SearchCacheEntity searchCacheEntity) {
        GreenDaoDbHelp.getDaoSession(Global.getContext()).getSearchCacheEntityDao().delete(searchCacheEntity);
    }

    public static void deleteallSearchInfo() {
        GreenDaoDbHelp.getDaoSession(Global.getContext()).getSearchCacheEntityDao().deleteAll();
    }

}
