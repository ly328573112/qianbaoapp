package com.haodaibao.fanbeiapp.database;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Transient;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class User {

    @Id(autoincrement = true)
    private Long id;

    @Property(nameInDb = "no")
    private String userno;

    @Index(unique = true)
    private String name;

    @NotNull
    private String mobile;

    @Transient
    private int tempUsageCount;

    @Generated(hash = 2034045280)
    public User(Long id, String userno, String name, @NotNull String mobile) {
        this.id = id;
        this.userno = userno;
        this.name = name;
        this.mobile = mobile;
    }

    @Generated(hash = 586692638)
    public User() {
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserno() {
        return this.userno;
    }

    public void setUserno(String userno) {
        this.userno = userno;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}

/* --------------------------
@Entity：数据表注解

@ID：long/Long属性作为数据库中的主键,autoincrement=true表示主键会自增,default为false

@Property 可以自定义一个该属性在数据库中的名称，默认情况下数据库中该属性名称是Bean对象中的属性名,
但是不是以驼峰式而是以大写与下划线组合形式来命名的.比如：customName将命名为 CUSTOM_NAME;
注意：外键不能使用该属性；

@NotNull 确保属性值不会为null值;

@Transient 使用该注释的属性不会被存入数据库中;

主键限制 每个实体类都应该有一个long或者LONG型属性作为主键；如果不想用long或者LONG型作为主键，
可以使用一个唯一索引(使用@Index(unique = true)注释使普通属性改变成唯一索引属性)属性作为关键属性。

@Index(unique = true)
private String key;
索引属性使用@Index 可以将一个属性变为数据库索引,unique给索引增加一个唯一约束，迫使该值唯一

    //增
    private void insert(){
        User mUser = new User((long)2,"0000012542","anye3","18600000000");
        mUserDao.insert(mUser);//添加一个
    }

    //删
    private void delete(){
        //mUserDao.deleteByKey(id);
    }

    //改
    private void update(){
        User mUser = new User((long)2,"0000012542","anye3","18600000000");
        mUserDao.update(mUser);
    }

    //查
    private void load(){
        List<User> users = mUserDao.loadAll();
    }

    QueryBuilder qb = userDao.queryBuilder();
    qb.where(Properties.FirstName.eq("Joe"),//第一个约束条件姓乔
    qb.or(Properties.YearOfBirth.gt(1970),//或者出生日期大于1970年
    qb.and(Properties.YearOfBirth.eq(1970),
        Properties.MonthOfBirth.ge(10))//并且在1970年出生 但是月份大于10月的));
    List youngJoes = qb.list();
    greenDao除了eq()操作之外还有很多其他方法大大方便了我们日常查询操作比如：
    eq()：==
    noteq()：!=
    gt()： >
    lt()：<
    ge：>=
    le:<= <="" li="">
    like()：包含
    between：俩者之间
    in：在某个值内
    notIn：不在某个值内

-------------------------- */