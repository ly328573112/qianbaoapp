package com.haodaibao.fanbeiapp.event;

import android.support.annotation.Keep;

@Keep
public class OpencitysResultEvent {
    boolean mbResult;

    public OpencitysResultEvent(boolean bresult) {
        this.mbResult = bresult;
    }

    public boolean isMbResult() {
        return mbResult;
    }

    public void setMbResult(boolean mbResult) {
        this.mbResult = mbResult;
    }
}
