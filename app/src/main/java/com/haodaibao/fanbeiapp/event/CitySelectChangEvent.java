package com.haodaibao.fanbeiapp.event;

import android.support.annotation.Keep;

@Keep
public class CitySelectChangEvent {

    //选择的城市是否是当前设备城市
    public boolean currentLocation = false;

}
