package com.haodaibao.fanbeiapp.event;

import android.support.annotation.Keep;

@Keep
public class ShangquanResultEvent {

    boolean mbResult;

    public ShangquanResultEvent(boolean bresult) {
        this.mbResult = bresult;
    }

    public boolean isMbResult() {
        return mbResult;
    }

    public void setMbResult(boolean mbResult) {
        this.mbResult = mbResult;
    }
}
