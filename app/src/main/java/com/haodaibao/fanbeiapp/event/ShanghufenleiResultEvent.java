package com.haodaibao.fanbeiapp.event;

import android.support.annotation.Keep;

@Keep
public class ShanghufenleiResultEvent {

    boolean mbResult;

    public ShanghufenleiResultEvent(boolean bresult) {
        this.mbResult = bresult;
    }

    public boolean isMbResult() {
        return mbResult;
    }

    public void setMbResult(boolean mbResult) {
        this.mbResult = mbResult;
    }
}
