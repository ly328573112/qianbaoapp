package com.haodaibao.fanbeiapp.event;

import android.support.annotation.Keep;

/**
 * Created by Routee on 2017/9/19.
 * description: ${cusor}
 */
@Keep
public class PersonalFragmentEvent {
    private boolean refreshBhhItem;
    private boolean refreshUserInfo;
    private boolean logOut;
    public boolean refreshCouponCount;

    public boolean isRefreshBhhItem() {
        return refreshBhhItem;
    }

    public void setRefreshBhhItem(boolean refreshBhhItem) {
        this.refreshBhhItem = refreshBhhItem;
    }

    public boolean isRefreshUserInfo() {
        return refreshUserInfo;
    }

    public void setRefreshUserInfo(boolean refreshUserInfo) {
        this.refreshUserInfo = refreshUserInfo;
    }

    public boolean isLogOut() {
        return logOut;
    }

    public void setLogOut(boolean logOut) {
        this.logOut = logOut;
    }
}
