package com.haodaibao.fanbeiapp.event;

import android.support.annotation.Keep;

@Keep
public class LocationResultEvent {

    String locationType;// allC（定位刷新全部），homeC（定位刷新首页），findC（定位刷新找店页）
    boolean mbResult;

    public LocationResultEvent(boolean bresult, String locationType) {
        this.mbResult = bresult;
        this.locationType = locationType;
    }

    public boolean isMbResult() {
        return mbResult;
    }

    public String getLocationType() {
        return locationType;
    }
}
