package com.haodaibao.fanbeiapp.event;

import android.support.annotation.Keep;

/**
 * Created by Routee on 2017/9/27.
 * description: ${cusor}
 */
@Keep
public class PaylistEvent {
    private boolean isComment;

    public boolean isComment() {
        return isComment;
    }

    public void setComment(boolean comment) {
        isComment = comment;
    }
}
