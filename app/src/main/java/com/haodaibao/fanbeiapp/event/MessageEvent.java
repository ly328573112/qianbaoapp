package com.haodaibao.fanbeiapp.event;

/**
 * Created by Routee on 2017/9/23.
 * description: ${cusor}
 */

public class MessageEvent {
    private String mType = "ALL";
    private String mMsgNo = "";
    private boolean delete = false;

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public String getMsgNo() {
        return mMsgNo;
    }

    public void setMsgNo(String msgNo) {
        mMsgNo = msgNo;
    }
}
