package com.haodaibao.fanbeiapp.event;

import android.support.annotation.Keep;

@Keep
public class LocationSwitchEvent {

    // allC（定位刷新全部），homeC（定位刷新首页），findC（定位刷新找店页）
    public String locationType = "allC";
    //点击定位是否刷新数据
    public boolean locationDialog = false;
    //选择的城市是否是当前设备城市
    public boolean cLocation = false;
    //点击首页定位（或找店页定位）
    public boolean isHomeLocation = true;

}
