package com.haodaibao.fanbeiapp.pageadpter;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class ViewPageAdapter extends PagerAdapter {

    public ArrayList<View> listViews;

    private int size;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public ViewPageAdapter(ArrayList<View> listViews) {
        this.listViews = listViews;
        size = listViews == null ? 0 : listViews.size();
    }

    public void setListViews(ArrayList<View> listViews) {
        this.listViews = listViews;
        size = listViews == null ? 0 : listViews.size();
    }

    @Override
    public int getCount() {
        return size;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView(listViews.get(position % size));
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        try {
            ((ViewPager) container).addView(listViews.get(position % size), 0);
        } catch (Exception e) {

        }
        return listViews.get(position % size);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
