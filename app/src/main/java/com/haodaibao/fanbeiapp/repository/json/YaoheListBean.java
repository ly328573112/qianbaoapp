package com.haodaibao.fanbeiapp.repository.json;


import android.support.annotation.Keep;

import java.io.Serializable;
import java.util.List;

@Keep
public class YaoheListBean implements Serializable {

    private static final long serialVersionUID = 626115150608737108L;
    private List<YaoheBean> items;
    public List<YaoheBean> getItems() {
        return items;
    }

    public void setItems(List<YaoheBean> items) {
        this.items = items;
    }

    @Keep
    public class YaoheBean {
        private String messageNo;
        private String merchantNo;
        private String merchantName;
        private String merImg;
        private String title;

        public String getMessageNo() {
            return messageNo;
        }

        public void setMessageNo(String messageNo) {
            this.messageNo = messageNo;
        }

        public String getMerchantNo() {
            return merchantNo;
        }

        public void setMerchantNo(String merchantNo) {
            this.merchantNo = merchantNo;
        }

        public String getMerchantName() {
            return merchantName;
        }

        public void setMerchantName(String merchantName) {
            this.merchantName = merchantName;
        }

        public String getMerImg() {
            return merImg;
        }

        public void setMerImg(String merImg) {
            this.merImg = merImg;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
