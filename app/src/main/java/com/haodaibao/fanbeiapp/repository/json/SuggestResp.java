package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;
import java.util.List;

/**
 * 意见反馈列表返回实体类
 */

@Keep
public class SuggestResp implements Serializable {
    private static final long serialVersionUID = 7849531066705150881L;
    private List<SuggestInfo> list;


    public List<SuggestInfo> getList() {
        return list;
    }

    public void setList(List<SuggestInfo> list) {
        this.list = list;
    }

}
