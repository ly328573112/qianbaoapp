package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.util.List;

/**
 * Created by Routee on 2017/9/4.
 * description: ${cusor}
 */
@Keep
public class CollectionsBean {

    private String totalCount;// 总可用商户数
    private String page;// 页数
    private String pageSize;// 页面容量
    private boolean lastPage;// 是否是最后一页
    private List<MerchantInfoEntiy> merchant;
    private List<ProductDetailInfo> products;

    public CollectionsBean() {
        super();
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public boolean isLastPage() {
        return lastPage;
    }

    public void setLastPage(boolean lastPage) {
        this.lastPage = lastPage;
    }

    public List<MerchantInfoEntiy> getMerchant() {
        return merchant;
    }

    public void setMerchant(List<MerchantInfoEntiy> merchant) {
        this.merchant = merchant;
    }

    public List<ProductDetailInfo> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDetailInfo> products) {
        this.products = products;
    }
}
