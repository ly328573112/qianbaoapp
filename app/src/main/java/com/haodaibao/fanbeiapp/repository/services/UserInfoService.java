package com.haodaibao.fanbeiapp.repository.services;

import com.haodaibao.fanbeiapp.repository.json.BhhInfoBean;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.LoginInfo;
import com.haodaibao.fanbeiapp.repository.json.UserDate;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public interface UserInfoService {

    @GET("v1/member/user/info")
    Observable<Data<UserDate>> getUserinfo();

    @GET("v1/member/user/getLoginSmsCodeNew")
    Observable<Data> getSmsCode(@QueryMap Map<String, String> paramMap);

    //语音验证码
    @POST("v1/member/user/getLoginVmsCode")
    @FormUrlEncoded
    Observable<Data> getVmsCode(@FieldMap Map<String, String> paramMap);

    @POST("v1/member/login")
    @FormUrlEncoded
    Observable<Data<LoginInfo>> requestLogin(@FieldMap Map<String, String> paramMap);

    @GET("v1/member/logout")
    Observable<Data> checkOut();

    @POST("v1/member/user/updateMyInfo")
    @FormUrlEncoded
    Observable<Data> updateNickname(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("v1/member/user/updateMyInfo")
    Observable<Data> updateHeadView(@FieldMap Map<String, String> params);

    //获取白花花信息
    @FormUrlEncoded
    @POST("v1/member/user/myBhhInfo")
    Observable<Data<BhhInfoBean>> getBhhInfo(@FieldMap Map<String, String> params);
}
