package com.haodaibao.fanbeiapp.repository.json;

import java.io.Serializable;

/**
 * 开发者：LuoYi
 * Time: 2017 13:54 2017/11/6 11
 */

public class PackageListBean implements Serializable {
    private static final long serialVersionUID = 3516720558127138144L;

//     "preferentialPrice" : "99",
//             "faceUserTypeCode" : "02",
//             "packageName" : "testtc4"

    public String preferentialPrice = "";// 套餐价格
    public String faceUserTypeCode = "";//面向用户类型 01:所有顾客 02:新顾客 03:老顾客
    public String packageName = "";// 套餐名

    public String packageNo = "";// 套餐编号
    public String originalprice = "";// 套餐原价

    public PackageListBean() {
    }

    public String getPreferentialPrice() {
        return preferentialPrice;
    }

    public String getFaceUserTypeCode() {
        return faceUserTypeCode;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getPackageNo() {
        return packageNo;
    }

    public String getOriginalprice() {
        return originalprice;
    }

    @Override
    public String toString() {
        return "PackageListBean{" +
                "preferentialPrice='" + preferentialPrice + '\'' +
                ", faceUserTypeCode='" + faceUserTypeCode + '\'' +
                ", packageName='" + packageName + '\'' +
                ", packageNo='" + packageNo + '\'' +
                ", originalprice='" + originalprice + '\'' +
                '}';
    }
}
