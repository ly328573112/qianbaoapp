package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;
import java.util.List;

@Keep
public class ProductDetailInfo implements Serializable {
    private static final long serialVersionUID = -849974066550996965L;
    private String scopemerchantnum;// 适用商户的商户数量
    private String scopemerchantStr;// 适用其他商户商户号
    private String imagetextdetail;// 商品图片详情
    private String productremark;//
    private String saleprice;// 商品售价
    private String userule;// 使用规则
    private int sumnum;// 总份数
    private int salenum;// 已售份数
    private String refundrule;// 退款规则
    private String originalprice;// 商品原价
    private String textdetail;// 商品详情
    private String productno;// 商品号
    private String productname;// 商品名称
    private String attention;// 是否关注
    private int limitnum;// 限购数量
    private String iflimit;// 0非限购1限购
    private String usestartdate;// 使用开始日期
    private String usestarttime;// 使用开始时间
    private String useenddate;// 使用结束日期
    private String useendtime;// 使用结束时间
    private String selltimetype;// 发放时间类型 1：一次性 2：周期性
    private String sellstartdate;// 发放开始日期
    private String sellenddate;// 发放结束日期
    private String sellstarttime;// 发放开始时间
    private String sellendtime;// 发放结束时间
    private String imageurl;// 套餐图片
    private String systemTime;// 系统时间
    private List<MerchantImageInfo> productImageList;// 系统时间

    public ProductDetailInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String getImagetextdetail() {
        return imagetextdetail;
    }

    public void setImagetextdetail(String imagetextdetail) {
        this.imagetextdetail = imagetextdetail;
    }

    public String getProductremark() {
        return productremark;
    }

    public void setProductremark(String productremark) {
        this.productremark = productremark;
    }

    public String getUserule() {
        return userule;
    }

    public void setUserule(String userule) {
        this.userule = userule;
    }

    public int getSumnum() {
        return sumnum;
    }

    public void setSumnum(int sumnum) {
        this.sumnum = sumnum;
    }

    public int getSalenum() {
        return salenum;
    }

    public void setSalenum(int salenum) {
        this.salenum = salenum;
    }

    public String getRefundrule() {
        return refundrule;
    }

    public void setRefundrule(String refundrule) {
        this.refundrule = refundrule;
    }

    public String getTextdetail() {
        return textdetail;
    }

    public void setTextdetail(String textdetail) {
        this.textdetail = textdetail;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String isAttention() {
        return attention;
    }

    public void setAttention(String attention) {
        this.attention = attention;
    }

    public int getLimitnum() {
        return limitnum;
    }

    public void setLimitnum(int limitnum) {
        this.limitnum = limitnum;
    }

    public String getIflimit() {
        return iflimit;
    }

    public void setIflimit(String iflimit) {
        this.iflimit = iflimit;
    }

    public String getUsestarttime() {
        return usestarttime;
    }

    public void setUsestarttime(String usestarttime) {
        this.usestarttime = usestarttime;
    }

    public String getUseendtime() {
        return useendtime;
    }

    public void setUseendtime(String useendtime) {
        this.useendtime = useendtime;
    }

    public String getUsestartdate() {
        return usestartdate;
    }

    public void setUsestartdate(String usestartdate) {
        this.usestartdate = usestartdate;
    }

    public String getUseenddate() {
        return useenddate;
    }

    public void setUseenddate(String useenddate) {
        this.useenddate = useenddate;
    }

    public String getSellstartdate() {
        return sellstartdate;
    }

    public void setSellstartdate(String sellstartdate) {
        this.sellstartdate = sellstartdate;
    }

    public String getSellenddate() {
        return sellenddate;
    }

    public void setSellenddate(String sellenddate) {
        this.sellenddate = sellenddate;
    }

    public String getSellstarttime() {
        return sellstarttime;
    }

    public void setSellstarttime(String sellstarttime) {
        this.sellstarttime = sellstarttime;
    }

    public String getSellendtime() {
        return sellendtime;
    }

    public void setSellendtime(String sellendtime) {
        this.sellendtime = sellendtime;
    }

    public String getProductno() {
        return productno;
    }

    public void setProductno(String productno) {
        this.productno = productno;
    }

    public String getScopemerchantnum() {
        return scopemerchantnum;
    }

    public void setScopemerchantnum(String scopemerchantnum) {
        this.scopemerchantnum = scopemerchantnum;
    }

    public String getSaleprice() {
        return saleprice;
    }

    public void setSaleprice(String saleprice) {
        this.saleprice = saleprice;
    }

    public String getOriginalprice() {
        return originalprice;
    }

    public void setOriginalprice(String originalprice) {
        this.originalprice = originalprice;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getScopemerchantStr() {
        return scopemerchantStr;
    }

    public void setScopemerchantStr(String scopemerchantStr) {
        this.scopemerchantStr = scopemerchantStr;
    }

    public String getSelltimetype() {
        return selltimetype;
    }

    public void setSelltimetype(String selltimetype) {
        this.selltimetype = selltimetype;
    }

    public String getSystemTime() {
        return systemTime;
    }

    public void setSystemTime(String systemTime) {
        this.systemTime = systemTime;
    }

    public List<MerchantImageInfo> getProductImageList() {
        return productImageList;
    }

    public void setProductImageList(List<MerchantImageInfo> productImageList) {
        this.productImageList = productImageList;
    }
}
