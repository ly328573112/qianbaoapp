package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

@Keep
public class UploadResult {

    /**
     * uploadFile : {"originalName":"mmexport1500360940917.jpg","name":"82d7196f61e9b1462e5ef3e779549360.jpg","fileUrl":"http://img9.qianbaocard.com/public/qianbaolife/","url":"http://img9.qianbaocard.com/public/qianbaolife/82d7196f61e9b1462e5ef3e779549360.jpg","fileType":"multipart/form-data","ext":".jpg","size":136241,"width":0,"height":0,"state":"SUCCESS","fid":"238,145676445010bfcd"}
     * resultCode : 00000000 resultMessage : 操作成功
     */

    private UploadInfo uploadFile;
    private String status;
    private String resultCode;
    private String resultMessage;

    @Keep
    public class UploadInfo {
        private String originalName;// 上传之前原文件名
        private String name;// 保存后的文件名
        private String fileUrl;// 文件上传目的地
        private String url;// 文件上传目的地
        private String fileType;// 文件类型
        private String ext;// 文件后缀
        private long size;// 文件大小
        private long width;// 文件宽度
        private long height;// 文件高度
        private String state;// 上传状态

        public UploadInfo() {
            super();
            // TODO Auto-generated constructor stub
        }

        public String getOriginalName() {
            return originalName;
        }

        public void setOriginalName(String originalName) {
            this.originalName = originalName;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFileUrl() {
            return fileUrl;
        }

        public void setFileUrl(String fileUrl) {
            this.fileUrl = fileUrl;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getFileType() {
            return fileType;
        }

        public void setFileType(String fileType) {
            this.fileType = fileType;
        }

        public String getExt() {
            return ext;
        }

        public void setExt(String ext) {
            this.ext = ext;
        }

        public long getSize() {
            return size;
        }

        public void setSize(long size) {
            this.size = size;
        }

        public long getWidth() {
            return width;
        }

        public void setWidth(long width) {
            this.width = width;
        }

        public long getHeight() {
            return height;
        }

        public void setHeight(long height) {
            this.height = height;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        @Override
        public String toString() {
            return "UploadInfo [originalName=" + originalName + ", name=" + name + ", fileUrl=" + fileUrl + ", url=" + url + ", fileType=" + fileType + ", ext=" + ext + ", size=" + size + ", width="
                    + width + ", height=" + height + ", state=" + state + "]";
        }

    }


    public UploadInfo getUploadFile() {
        return uploadFile;
    }

    public void setUploadFile(UploadInfo uploadFile) {
        this.uploadFile = uploadFile;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
