package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;

@Keep
public class CitiesBean implements Serializable {
    private static final long serialVersionUID = -8420470844890091499L;

    private String code;
    private String name;
    private String character;
    private String provincecode;
    private String hot;
    private String open;
    private String districtList;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getProvincecode() {
        return provincecode;
    }

    public void setProvincecode(String provincecode) {
        this.provincecode = provincecode;
    }

    public String getHot() {
        return hot;
    }

    public void setHot(String hot) {
        this.hot = hot;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getDistrictList() {
        return districtList;
    }

    public void setDistrictList(String districtList) {
        this.districtList = districtList;
    }

}
