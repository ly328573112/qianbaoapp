package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

@Keep
public class UserDate {

    private UserInfo user;

    public UserInfo getUser() {
        return user;
    }

    public void setUser(UserInfo user) {
        this.user = user;
    }
}
