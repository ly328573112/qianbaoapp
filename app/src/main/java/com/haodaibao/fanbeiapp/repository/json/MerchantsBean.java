package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;
import java.util.List;

@Keep
public class MerchantsBean implements Serializable {
    /**
     * activityno :
     * merchantno : M20180327693925
     * merchantname : 猪蹄老街坊
     * groupno :
     * categoryno : 20150429100068
     * subcategoryno : 20160406100538
     * subcategoryname : 火锅
     * provincecode :
     * citycode :
     * districtcode :
     * areacode :
     * address :
     * longitude : 121.38631
     * latitude : 31.164145
     * phone :
     * mobile :
     * receivemobile :
     * opentime :
     * opentimeext :
     * personaverage :
     * cashaccountno :
     * couponaccountno :
     * refundmode :
     * offlinerefundmode :
     * ifrefundperiod :
     * discount :
     * discount2 :
     * refundperiodsource :
     * usertype :
     * returnrate : 0.66
     * returnrate2 :
     * returnbeginamount :
     * returnbegin2amount :
     * topamount :
     * prepaymode :
     * prepaybalance :
     * prepaylimit :
     * prepaywarningbalance :
     * posmerchantno :
     * posterminalno :
     * settlemode :
     * saleperson :
     * signtime :
     * organizationno :
     * createtime :
     * status :
     * qbmemberno :
     * merfee :
     * posmerfee :
     * accountNo :
     * accountName :
     * bankNo :
     * distance : 0.0
     * star :
     * recommend :
     * categoryName : 美食
     * imageurl :
     * productnum :
     * vouchernum :
     * attention :
     * productList : []
     * recordCount :
     * story :
     * notice :
     * promotion :
     * refundnote :
     * areaname :
     * giftList :
     * album :
     * merchantImageList : []
     * labels : []
     * prieds : []
     * packageList : []
     * bhhenable :
     * facepay :
     */

    private String activityno;
    private String merchantno;
    private String merchantname;
    private String groupno;
    private String categoryno;
    private String subcategoryno;
    private String subcategoryname;
    private String provincecode;
    private String citycode;
    private String districtcode;
    private String areacode;
    private String address;
    private String longitude;
    private String latitude;
    private String phone;
    private String mobile;
    private String receivemobile;
    private String opentime;
    private String opentimeext;
    private String personaverage;
    private String cashaccountno;
    private String couponaccountno;
    private String refundmode;
    private String offlinerefundmode;
    private String ifrefundperiod;
    private String discount;
    private String discount2;
    private String refundperiodsource;
    private String usertype;
    private String returnrate;
    private String returnrate2;
    private String returnbeginamount;
    private String returnbegin2amount;
    private String topamount;
    private String prepaymode;
    private String prepaybalance;
    private String prepaylimit;
    private String prepaywarningbalance;
    private String posmerchantno;
    private String posterminalno;
    private String settlemode;
    private String saleperson;
    private String signtime;
    private String organizationno;
    private String createtime;
    private String status;
    private String qbmemberno;
    private String merfee;
    private String posmerfee;
    private String accountNo;
    private String accountName;
    private String bankNo;
    private double distance;
    private String star;
    private String recommend;
    private String categoryName;
    private String imageurl;
    private String productnum;
    private String vouchernum;
    private String attention;
    private String recordCount;
    private String story;
    private String notice;
    private String promotion;
    private String refundnote;
    private String areaname;
    private String giftList;
    private String album;
    private String bhhenable;
    private String facepay;
    private List<?> productList;
    private List<?> merchantImageList;
    private List<?> labels;
    private List<?> prieds;
    private List<?> packageList;

    public String getActivityno() {
        return activityno;
    }

    public void setActivityno(String activityno) {
        this.activityno = activityno;
    }

    public String getMerchantno() {
        return merchantno;
    }

    public void setMerchantno(String merchantno) {
        this.merchantno = merchantno;
    }

    public String getMerchantname() {
        return merchantname;
    }

    public void setMerchantname(String merchantname) {
        this.merchantname = merchantname;
    }

    public String getGroupno() {
        return groupno;
    }

    public void setGroupno(String groupno) {
        this.groupno = groupno;
    }

    public String getCategoryno() {
        return categoryno;
    }

    public void setCategoryno(String categoryno) {
        this.categoryno = categoryno;
    }

    public String getSubcategoryno() {
        return subcategoryno;
    }

    public void setSubcategoryno(String subcategoryno) {
        this.subcategoryno = subcategoryno;
    }

    public String getSubcategoryname() {
        return subcategoryname;
    }

    public void setSubcategoryname(String subcategoryname) {
        this.subcategoryname = subcategoryname;
    }

    public String getProvincecode() {
        return provincecode;
    }

    public void setProvincecode(String provincecode) {
        this.provincecode = provincecode;
    }

    public String getCitycode() {
        return citycode;
    }

    public void setCitycode(String citycode) {
        this.citycode = citycode;
    }

    public String getDistrictcode() {
        return districtcode;
    }

    public void setDistrictcode(String districtcode) {
        this.districtcode = districtcode;
    }

    public String getAreacode() {
        return areacode;
    }

    public void setAreacode(String areacode) {
        this.areacode = areacode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getReceivemobile() {
        return receivemobile;
    }

    public void setReceivemobile(String receivemobile) {
        this.receivemobile = receivemobile;
    }

    public String getOpentime() {
        return opentime;
    }

    public void setOpentime(String opentime) {
        this.opentime = opentime;
    }

    public String getOpentimeext() {
        return opentimeext;
    }

    public void setOpentimeext(String opentimeext) {
        this.opentimeext = opentimeext;
    }

    public String getPersonaverage() {
        return personaverage;
    }

    public void setPersonaverage(String personaverage) {
        this.personaverage = personaverage;
    }

    public String getCashaccountno() {
        return cashaccountno;
    }

    public void setCashaccountno(String cashaccountno) {
        this.cashaccountno = cashaccountno;
    }

    public String getCouponaccountno() {
        return couponaccountno;
    }

    public void setCouponaccountno(String couponaccountno) {
        this.couponaccountno = couponaccountno;
    }

    public String getRefundmode() {
        return refundmode;
    }

    public void setRefundmode(String refundmode) {
        this.refundmode = refundmode;
    }

    public String getOfflinerefundmode() {
        return offlinerefundmode;
    }

    public void setOfflinerefundmode(String offlinerefundmode) {
        this.offlinerefundmode = offlinerefundmode;
    }

    public String getIfrefundperiod() {
        return ifrefundperiod;
    }

    public void setIfrefundperiod(String ifrefundperiod) {
        this.ifrefundperiod = ifrefundperiod;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscount2() {
        return discount2;
    }

    public void setDiscount2(String discount2) {
        this.discount2 = discount2;
    }

    public String getRefundperiodsource() {
        return refundperiodsource;
    }

    public void setRefundperiodsource(String refundperiodsource) {
        this.refundperiodsource = refundperiodsource;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public String getReturnrate() {
        return returnrate;
    }

    public void setReturnrate(String returnrate) {
        this.returnrate = returnrate;
    }

    public String getReturnrate2() {
        return returnrate2;
    }

    public void setReturnrate2(String returnrate2) {
        this.returnrate2 = returnrate2;
    }

    public String getReturnbeginamount() {
        return returnbeginamount;
    }

    public void setReturnbeginamount(String returnbeginamount) {
        this.returnbeginamount = returnbeginamount;
    }

    public String getReturnbegin2amount() {
        return returnbegin2amount;
    }

    public void setReturnbegin2amount(String returnbegin2amount) {
        this.returnbegin2amount = returnbegin2amount;
    }

    public String getTopamount() {
        return topamount;
    }

    public void setTopamount(String topamount) {
        this.topamount = topamount;
    }

    public String getPrepaymode() {
        return prepaymode;
    }

    public void setPrepaymode(String prepaymode) {
        this.prepaymode = prepaymode;
    }

    public String getPrepaybalance() {
        return prepaybalance;
    }

    public void setPrepaybalance(String prepaybalance) {
        this.prepaybalance = prepaybalance;
    }

    public String getPrepaylimit() {
        return prepaylimit;
    }

    public void setPrepaylimit(String prepaylimit) {
        this.prepaylimit = prepaylimit;
    }

    public String getPrepaywarningbalance() {
        return prepaywarningbalance;
    }

    public void setPrepaywarningbalance(String prepaywarningbalance) {
        this.prepaywarningbalance = prepaywarningbalance;
    }

    public String getPosmerchantno() {
        return posmerchantno;
    }

    public void setPosmerchantno(String posmerchantno) {
        this.posmerchantno = posmerchantno;
    }

    public String getPosterminalno() {
        return posterminalno;
    }

    public void setPosterminalno(String posterminalno) {
        this.posterminalno = posterminalno;
    }

    public String getSettlemode() {
        return settlemode;
    }

    public void setSettlemode(String settlemode) {
        this.settlemode = settlemode;
    }

    public String getSaleperson() {
        return saleperson;
    }

    public void setSaleperson(String saleperson) {
        this.saleperson = saleperson;
    }

    public String getSigntime() {
        return signtime;
    }

    public void setSigntime(String signtime) {
        this.signtime = signtime;
    }

    public String getOrganizationno() {
        return organizationno;
    }

    public void setOrganizationno(String organizationno) {
        this.organizationno = organizationno;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getQbmemberno() {
        return qbmemberno;
    }

    public void setQbmemberno(String qbmemberno) {
        this.qbmemberno = qbmemberno;
    }

    public String getMerfee() {
        return merfee;
    }

    public void setMerfee(String merfee) {
        this.merfee = merfee;
    }

    public String getPosmerfee() {
        return posmerfee;
    }

    public void setPosmerfee(String posmerfee) {
        this.posmerfee = posmerfee;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getBankNo() {
        return bankNo;
    }

    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getRecommend() {
        return recommend;
    }

    public void setRecommend(String recommend) {
        this.recommend = recommend;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getProductnum() {
        return productnum;
    }

    public void setProductnum(String productnum) {
        this.productnum = productnum;
    }

    public String getVouchernum() {
        return vouchernum;
    }

    public void setVouchernum(String vouchernum) {
        this.vouchernum = vouchernum;
    }

    public String getAttention() {
        return attention;
    }

    public void setAttention(String attention) {
        this.attention = attention;
    }

    public String getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(String recordCount) {
        this.recordCount = recordCount;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    public String getRefundnote() {
        return refundnote;
    }

    public void setRefundnote(String refundnote) {
        this.refundnote = refundnote;
    }

    public String getAreaname() {
        return areaname;
    }

    public void setAreaname(String areaname) {
        this.areaname = areaname;
    }

    public String getGiftList() {
        return giftList;
    }

    public void setGiftList(String giftList) {
        this.giftList = giftList;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getBhhenable() {
        return bhhenable;
    }

    public void setBhhenable(String bhhenable) {
        this.bhhenable = bhhenable;
    }

    public String getFacepay() {
        return facepay;
    }

    public void setFacepay(String facepay) {
        this.facepay = facepay;
    }

    public List<?> getProductList() {
        return productList;
    }

    public void setProductList(List<?> productList) {
        this.productList = productList;
    }

    public List<?> getMerchantImageList() {
        return merchantImageList;
    }

    public void setMerchantImageList(List<?> merchantImageList) {
        this.merchantImageList = merchantImageList;
    }

    public List<?> getLabels() {
        return labels;
    }

    public void setLabels(List<?> labels) {
        this.labels = labels;
    }

    public List<?> getPrieds() {
        return prieds;
    }

    public void setPrieds(List<?> prieds) {
        this.prieds = prieds;
    }

    public List<?> getPackageList() {
        return packageList;
    }

    public void setPackageList(List<?> packageList) {
        this.packageList = packageList;
    }
}
