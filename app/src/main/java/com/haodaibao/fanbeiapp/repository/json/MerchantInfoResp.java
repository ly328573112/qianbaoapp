package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;
import java.util.List;

@Keep
public class MerchantInfoResp implements Serializable {
    private String imageServerUrl;// 图片服务地址
    private String totalCount;// 总可用商户数
    private String page;// 页数
    private String headMerchant;// 智能排序头部商户添加字段
    private String pageSize;// 页面容量
    private boolean lastPage;// 是否是最后一页
    private List<MerchantInfoEntiy> merchantList;

    public MerchantInfoResp() {
        super();
    }

    public String getHeadMerchant() {
        return headMerchant;
    }

    public void setHeadMerchant(String headMerchant) {
        this.headMerchant = headMerchant;
    }

    public String getImageServerUrl() {
        return imageServerUrl;
    }

    public void setImageServerUrl(String imageServerUrl) {
        this.imageServerUrl = imageServerUrl;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public boolean isLastPage() {
        return lastPage;
    }

    public void setLastPage(boolean lastPage) {
        this.lastPage = lastPage;
    }

    public List<MerchantInfoEntiy> getMerchantList() {
        return merchantList;
    }

    public void setMerchantList(List<MerchantInfoEntiy> merchantList) {
        this.merchantList = merchantList;
    }
}
