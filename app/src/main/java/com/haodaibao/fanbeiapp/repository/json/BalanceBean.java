package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

/**
 * Created by Routee on 2017/9/19.
 * description: ${cusor}
 */
@Keep
public class BalanceBean {

    /**
     * balance : 0
     * balanceFrozen : 0
     * rewardsum : 0.00
     * balanceAvailable : 0
     */

    private String balance;
    private String balanceFrozen;
    private String rewardsum;
    private String balanceAvailable;

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getBalanceFrozen() {
        return balanceFrozen;
    }

    public void setBalanceFrozen(String balanceFrozen) {
        this.balanceFrozen = balanceFrozen;
    }

    public String getRewardsum() {
        return rewardsum;
    }

    public void setRewardsum(String rewardsum) {
        this.rewardsum = rewardsum;
    }

    public String getBalanceAvailable() {
        return balanceAvailable;
    }

    public void setBalanceAvailable(String balanceAvailable) {
        this.balanceAvailable = balanceAvailable;
    }
}
