package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;

/**
 * 商户图片信息
 */
@Keep
public class MerchantImageInfo implements Serializable {
	private static final long serialVersionUID = 8018110581600216329L;
	private String imageno;// 图片编号
	private String merchantno;// 商户号
	private String productno;// 套餐号
	private String imageurl;// 图片地址
	private String mainimage;// 1主图0非主图

	public MerchantImageInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getImageno() {
		return imageno;
	}

	public void setImageno(String imageno) {
		this.imageno = imageno;
	}

	public String getMerchantno() {
		return merchantno;
	}

	public void setMerchantno(String merchantno) {
		this.merchantno = merchantno;
	}

	public String getImageurl() {
		return imageurl;
	}

	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}

	public String getMainimage() {
		return mainimage;
	}

	public void setMainimage(String mainimage) {
		this.mainimage = mainimage;
	}

	public String getProductno() {
		return productno;
	}

	public void setProductno(String productno) {
		this.productno = productno;
	}

}
