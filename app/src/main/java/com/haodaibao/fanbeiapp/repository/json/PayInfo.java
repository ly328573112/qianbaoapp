package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

/**
 * Created by zhenglei on 2017/3/19.
 * 调用支付sdk支付请求参数
 */

@Keep
public class PayInfo {
    private String cashierId;//调用支付sdk参数
    private String cashierToken;//调用支付sdk参数
    private String orderno;//查询订单状态参数
    private String cashierEnv;//

    public String getCashierId() {
        return cashierId;
    }

    public void setCashierId(String cashierId) {
        this.cashierId = cashierId;
    }

    public String getCashierToken() {
        return cashierToken;
    }

    public void setCashierToken(String cashierToken) {
        this.cashierToken = cashierToken;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getCashierEnv() {
        return cashierEnv;
    }

    public void setCashierEnv(String cashierEnv) {
        this.cashierEnv = cashierEnv;
    }
}
