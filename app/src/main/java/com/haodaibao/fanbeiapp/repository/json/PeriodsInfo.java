package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;

/**
 * 分时段优惠实体类
 */
@Keep
public class PeriodsInfo implements Serializable {
    private static final long serialVersionUID = 5058996273997925094L;
    private String returnrate;// 比例或金额
    private String returnbeginamount;// 起始优惠金额
    private String returnrate2; // 结束返现起点的用户返现比率(当有两个返现起点金额)
    private String returnbegin2amount;// 返现起点结束金额(当有两个返现起点金额)
    private String timestring;// 优惠时间点
    private String daystring;// 优惠时间段
    private String discount;// 商户结算比例

    public String getReturnrate() {
        return returnrate;
    }

    public void setReturnrate(String returnrate) {
        this.returnrate = returnrate;
    }

    public String getTimestring() {
        return timestring;
    }

    public void setTimestring(String timestring) {
        this.timestring = timestring;
    }

    public String getDaystring() {
        return daystring;
    }

    public void setDaystring(String daystring) {
        this.daystring = daystring;
    }

    public String getReturnbeginamount() {
        return returnbeginamount;
    }

    public void setReturnbeginamount(String returnbeginamount) {
        this.returnbeginamount = returnbeginamount;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getReturnrate2() {
        return returnrate2;
    }

    public void setReturnrate2(String returnrate2) {
        this.returnrate2 = returnrate2;
    }

    public String getReturnbegin2amount() {
        return returnbegin2amount;
    }

    public void setReturnbegin2amount(String returnbegin2amount) {
        this.returnbegin2amount = returnbegin2amount;
    }

}
