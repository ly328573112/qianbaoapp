package com.haodaibao.fanbeiapp.repository;

import com.baseandroid.retrofit.RetrofitManager;
import com.haodaibao.fanbeiapp.repository.json.BhhInfoBean;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.LoginInfo;
import com.haodaibao.fanbeiapp.repository.json.UserDate;
import com.haodaibao.fanbeiapp.repository.services.UserInfoService;

import java.util.Map;

import io.reactivex.Observable;

public class UserInfoRepository {

    private volatile static UserInfoRepository instance;

    private UserInfoRepository() {

    }

    public static UserInfoRepository getInstance() {
        if (instance == null) {
            synchronized (UserInfoRepository.class) {
                if (instance == null) {
                    instance = new UserInfoRepository();
                }
            }
        }
        return instance;
    }

    public Observable<Data<UserDate>> getUserinfo() {
        UserInfoService service = RetrofitManager.getRxRetrofit()
                .create(UserInfoService.class);
        return service.getUserinfo();
    }

    public Observable<Data> getSmsCode(Map<String, String> paramMap) {
        UserInfoService service = RetrofitManager.getRxRetrofit()
                .create(UserInfoService.class);
        return service.getSmsCode(paramMap);
    }

    //获取语音验证码
    public Observable<Data> getVmsCode(Map<String, String> paramMap) {
        UserInfoService service = RetrofitManager.getRxRetrofit()
                .create(UserInfoService.class);
        return service.getVmsCode(paramMap);
    }


    public Observable<Data<LoginInfo>> requestLogin(Map<String, String> paramMap) {
        UserInfoService service = RetrofitManager.getRxRetrofit()
                .create(UserInfoService.class);
        return service.requestLogin(paramMap);
    }

    //登出
    public Observable checkOut() {
        UserInfoService service = RetrofitManager.getRxRetrofit().create(UserInfoService.class);
        return service.checkOut();
    }

    //修改用户名
    public Observable updateNickname(Map<String, String> params) {
        UserInfoService service = RetrofitManager.getRxRetrofit().create(UserInfoService.class);
        return service.updateNickname(params);
    }

    //更新头像地址
    public Observable<Data> updateHeadView(Map<String, String> params) {
        UserInfoService service = RetrofitManager.getRxRetrofit().create(UserInfoService.class);
        return service.updateHeadView(params);
    }

    //获取白花花信息
    public Observable<Data<BhhInfoBean>> getBhhInfo(Map<String, String> params) {
        UserInfoService service = RetrofitManager.getRxRetrofit().create(UserInfoService.class);
        return service.getBhhInfo(params);
    }
}