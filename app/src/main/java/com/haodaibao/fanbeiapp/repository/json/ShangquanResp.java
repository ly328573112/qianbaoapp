package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.util.List;

/*商圈信息响应*/
@Keep
public class ShangquanResp {
    private List<CdaList> cdaList;

    public List<CdaList> getCdaList() {
        return cdaList;
    }

    public void setCdaList(List<CdaList> cdaList) {
        this.cdaList = cdaList;
    }

}
