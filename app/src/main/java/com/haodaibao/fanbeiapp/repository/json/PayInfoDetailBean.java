package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Routee on 2017/9/11.
 * description: ${cusor}
 */

@Keep
public class PayInfoDetailBean implements Serializable {

    private static final long serialVersionUID = 8517730197752634474L;

    private String couponamt;
    private String refundamt;
    private String ordertime;
    private String paytime;
    private String rewardamount;
    private String merchantno;
    private String orderamt;
    private String attention;
    private String notrefundamt;
    private String pickcoed;
    private String status;
    private String refundstatus;
    private String hostreturncode;
    private String userno;
    private String merchantImageUrl;
    private String cashamt;
    private String consumptionmode;
    private String authremark;
    private String hostreturnmessage;
    private String reduceamount;
    private String merchantname;
    private List<?> orderDetails;
    private List<PayflowsBean> payflows;
    private String refundmode; //商家折扣1/满减3
    public String citycode;
    public String address;
    public String relationNo;
    public String relationType;
    public String preferentialPackage;
    public String packageNo;
    public String preferentialPrice;
    public String originalPrice;
    public String faceUserTypeCode;
    public String packageName;
    public String commentstatus;
    public String invoiceId;
    public String invoiceStatus;
    public String canInvoice;

    private List<ActivitiesBean> activities;//参与的活动列表

    public List<ActivitiesBean> getActivities() {
        return activities;
    }

    public void setActivities(List<ActivitiesBean> activities) {
        this.activities = activities;
    }

    @Keep
    public class ActivitiesBean implements Serializable {
        private String activityNo;//活动编号
        private String labelName;//标签名
        private String benefitAmount;//活动优惠金额

        public ActivitiesBean(String activityNo, String labelName, String benefitAmount) {
            this.activityNo = activityNo;
            this.labelName = labelName;
            this.benefitAmount = benefitAmount;
        }

        public String getActivityNo() {
            return activityNo;
        }

        public void setActivityNo(String activityNo) {
            this.activityNo = activityNo;
        }

        public String getLabelName() {
            return labelName;
        }

        public void setLabelName(String labelName) {
            this.labelName = labelName;
        }

        public String getBenefitAmount() {
            return benefitAmount;
        }

        public void setBenefitAmount(String benefitAmount) {
            this.benefitAmount = benefitAmount;
        }
    }

    public String getRefundmode() {
        return refundmode;
    }

    public void setRefundmode(String refundmode) {
        this.refundmode = refundmode;
    }

    public String getCouponamt() {
        return couponamt;
    }

    public void setCouponamt(String couponamt) {
        this.couponamt = couponamt;
    }

    public String getRefundamt() {
        return refundamt;
    }

    public void setRefundamt(String refundamt) {
        this.refundamt = refundamt;
    }

    public String getOrdertime() {
        return ordertime;
    }

    public void setOrdertime(String ordertime) {
        this.ordertime = ordertime;
    }

    public String getPaytime() {
        return paytime;
    }

    public void setPaytime(String paytime) {
        this.paytime = paytime;
    }

    public String getRewardamount() {
        return rewardamount;
    }

    public void setRewardamount(String rewardamount) {
        this.rewardamount = rewardamount;
    }

    public String getMerchantno() {
        return merchantno;
    }

    public void setMerchantno(String merchantno) {
        this.merchantno = merchantno;
    }

    public String getOrderamt() {
        return orderamt;
    }

    public void setOrderamt(String orderamt) {
        this.orderamt = orderamt;
    }

    public String getAttention() {
        return attention;
    }

    public void setAttention(String attention) {
        this.attention = attention;
    }

    public String getNotrefundamt() {
        return notrefundamt;
    }

    public void setNotrefundamt(String notrefundamt) {
        this.notrefundamt = notrefundamt;
    }

    public String getPickcoed() {
        return pickcoed;
    }

    public void setPickcoed(String pickcoed) {
        this.pickcoed = pickcoed;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRefundstatus() {
        return refundstatus;
    }

    public void setRefundstatus(String refundstatus) {
        this.refundstatus = refundstatus;
    }

    public String getHostreturncode() {
        return hostreturncode;
    }

    public void setHostreturncode(String hostreturncode) {
        this.hostreturncode = hostreturncode;
    }

    public String getUserno() {
        return userno;
    }

    public void setUserno(String userno) {
        this.userno = userno;
    }

    public String getMerchantImageUrl() {
        return merchantImageUrl;
    }

    public void setMerchantImageUrl(String merchantImageUrl) {
        this.merchantImageUrl = merchantImageUrl;
    }

    public String getCashamt() {
        return cashamt;
    }

    public void setCashamt(String cashamt) {
        this.cashamt = cashamt;
    }

    public String getConsumptionmode() {
        return consumptionmode;
    }

    public void setConsumptionmode(String consumptionmode) {
        this.consumptionmode = consumptionmode;
    }

    public String getAuthremark() {
        return authremark;
    }

    public void setAuthremark(String authremark) {
        this.authremark = authremark;
    }

    public String getHostreturnmessage() {
        return hostreturnmessage;
    }

    public void setHostreturnmessage(String hostreturnmessage) {
        this.hostreturnmessage = hostreturnmessage;
    }

    public String getReduceamount() {
        return reduceamount;
    }

    public void setReduceamount(String reduceamount) {
        this.reduceamount = reduceamount;
    }

    public String getMerchantname() {
        return merchantname;
    }

    public void setMerchantname(String merchantname) {
        this.merchantname = merchantname;
    }

    public List<?> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<?> orderDetails) {
        this.orderDetails = orderDetails;
    }

    public List<PayflowsBean> getPayflows() {
        return payflows;
    }

    public void setPayflows(List<PayflowsBean> payflows) {
        this.payflows = payflows;
    }

    @Keep
    public static class PayflowsBean implements Serializable {
        private static final long serialVersionUID = 1884183321197115832L;
        /**
         * serialno : PAY2017083110000000398940
         * orderno : ORD2017083110000000398939
         * paytype : 23
         * payno : 85983175679
         * payname :
         * bankno :
         * bankname :
         * paytime : 20170831202402
         * amount : 0.01
         * couponamt : 0.00
         * hostserialno : 20170831202419
         * hostreturncode :
         * hostreturnmessage : 交易成功
         * status : 20
         * transferstatus : 0
         * sourceorganizationno : O201501011000000000
         * channel : 02
         */

        private String serialno;
        private String orderno;
        private String paytype;
        private String payno;
        private String payname;
        private String bankno;
        private String bankname;
        private String paytime;
        private String amount;
        private String couponamt;
        private String hostserialno;
        private String hostreturncode;
        private String hostreturnmessage;
        private String status;
        private String transferstatus;
        private String sourceorganizationno;
        private String channel;

        public String getSerialno() {
            return serialno;
        }

        public void setSerialno(String serialno) {
            this.serialno = serialno;
        }

        public String getOrderno() {
            return orderno;
        }

        public void setOrderno(String orderno) {
            this.orderno = orderno;
        }

        public String getPaytype() {
            return paytype;
        }

        public void setPaytype(String paytype) {
            this.paytype = paytype;
        }

        public String getPayno() {
            return payno;
        }

        public void setPayno(String payno) {
            this.payno = payno;
        }

        public String getPayname() {
            return payname;
        }

        public void setPayname(String payname) {
            this.payname = payname;
        }

        public String getBankno() {
            return bankno;
        }

        public void setBankno(String bankno) {
            this.bankno = bankno;
        }

        public String getBankname() {
            return bankname;
        }

        public void setBankname(String bankname) {
            this.bankname = bankname;
        }

        public String getPaytime() {
            return paytime;
        }

        public void setPaytime(String paytime) {
            this.paytime = paytime;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getCouponamt() {
            return couponamt;
        }

        public void setCouponamt(String couponamt) {
            this.couponamt = couponamt;
        }

        public String getHostserialno() {
            return hostserialno;
        }

        public void setHostserialno(String hostserialno) {
            this.hostserialno = hostserialno;
        }

        public String getHostreturncode() {
            return hostreturncode;
        }

        public void setHostreturncode(String hostreturncode) {
            this.hostreturncode = hostreturncode;
        }

        public String getHostreturnmessage() {
            return hostreturnmessage;
        }

        public void setHostreturnmessage(String hostreturnmessage) {
            this.hostreturnmessage = hostreturnmessage;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTransferstatus() {
            return transferstatus;
        }

        public void setTransferstatus(String transferstatus) {
            this.transferstatus = transferstatus;
        }

        public String getSourceorganizationno() {
            return sourceorganizationno;
        }

        public void setSourceorganizationno(String sourceorganizationno) {
            this.sourceorganizationno = sourceorganizationno;
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
        }
    }
}
