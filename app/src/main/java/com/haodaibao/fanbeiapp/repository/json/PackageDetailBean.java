package com.haodaibao.fanbeiapp.repository.json;

import java.io.Serializable;

/**
 * date: 2017/11/7.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class PackageDetailBean implements Serializable{
    private static final long serialVersionUID = -7858925476909551737L;
    public String id;
    public String packageno;
    public String packagename;
    public String merchantno;
    public String begindate;
    public String enddate;
    public String callbackdate;
    public String finishdate;
    public String selloutdate;
    public String faceusertypecode;
    public String packagestatuscode;
    public String originalprice;
    public String preferentialprice;
    public String amount;
    public String usedaytypecode;
    public String useDayAmount;
    public String useamount;
    public String packagedesciptionurl;
    public String usetimerangebegin;
    public String usetimerangeend;
    public String createuser;
    public String createdate;
    public String lastmodifieduser;
    public String lastmodifieddate;
    public String metadata;
    public String packageRule;
}
