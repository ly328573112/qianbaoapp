package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;
import java.util.List;

/*父类商户信息*/
@Keep
public class ShanghufenleiList implements Serializable {
    private static final long serialVersionUID = 1981382348977840471L;
    private String categoryno;// 分类编号
    private String name;// 分类名称
    private String parentno;// 父分类编号
    private String hot;// 是否热点
    private String status;// 状态
    private List<ShanghufenleiInfo> list;

    public ShanghufenleiList(String categoryno, String name, String parentno, String hot, String status, List<ShanghufenleiInfo> list) {
        this.categoryno = categoryno;
        this.name = name;
        this.parentno = parentno;
        this.hot = hot;
        this.status = status;
        this.list = list;
    }

    public String getCategoryno() {
        return categoryno;
    }

    public void setCategoryno(String categoryno) {
        this.categoryno = categoryno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentno() {
        return parentno;
    }

    public void setParentno(String parentno) {
        this.parentno = parentno;
    }

    public String getHot() {
        return hot;
    }

    public void setHot(String hot) {
        this.hot = hot;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ShanghufenleiInfo> getList() {
        return list;
    }

    public void setList(List<ShanghufenleiInfo> list) {
        this.list = list;
    }

}
