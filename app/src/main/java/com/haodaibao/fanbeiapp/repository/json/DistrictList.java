package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.util.List;

@Keep
public class DistrictList {
    private String code;// 区县编码
    private String name;// 区县名称
    private String citycode;// 城市编码
    private List<AreaList> areaList;

    public DistrictList(String code, String name, String citycode, List<AreaList> areaList) {
        super();
        this.code = code;
        this.name = name;
        this.citycode = citycode;
        this.areaList = areaList;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCitycode() {
        return citycode;
    }

    public void setCitycode(String citycode) {
        this.citycode = citycode;
    }

    public List<AreaList> getAreaList() {
        return areaList;
    }

    public void setAreaList(List<AreaList> areaList) {
        this.areaList = areaList;
    }

}

