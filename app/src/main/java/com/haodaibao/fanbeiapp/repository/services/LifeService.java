package com.haodaibao.fanbeiapp.repository.services;

import com.haodaibao.fanbeiapp.repository.json.AdInfoResp;
import com.haodaibao.fanbeiapp.repository.json.BalanceBean;
import com.haodaibao.fanbeiapp.repository.json.BhhStatueBean;
import com.haodaibao.fanbeiapp.repository.json.CollectionsBean;
import com.haodaibao.fanbeiapp.repository.json.CouponBean;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.HelpBean;
import com.haodaibao.fanbeiapp.repository.json.InvoiceBean;
import com.haodaibao.fanbeiapp.repository.json.InvoiceDetailBean;
import com.haodaibao.fanbeiapp.repository.json.InvoiceHeadBean;
import com.haodaibao.fanbeiapp.repository.json.InvoiceRiseDetailBean;
import com.haodaibao.fanbeiapp.repository.json.LabelListResp;
import com.haodaibao.fanbeiapp.repository.json.MakeoutApplyBean;
import com.haodaibao.fanbeiapp.repository.json.MakeoutInvoiceBean;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoResp;
import com.haodaibao.fanbeiapp.repository.json.MessageListBean;
import com.haodaibao.fanbeiapp.repository.json.MyCouponsResp;
import com.haodaibao.fanbeiapp.repository.json.OpenCitysInfoResp;
import com.haodaibao.fanbeiapp.repository.json.PackageAllBean;
import com.haodaibao.fanbeiapp.repository.json.PayInfoDetailBean;
import com.haodaibao.fanbeiapp.repository.json.PayProcessResp;
import com.haodaibao.fanbeiapp.repository.json.PaylistItemBean;
import com.haodaibao.fanbeiapp.repository.json.PaylistTopBean;
import com.haodaibao.fanbeiapp.repository.json.ProvinCityDistInfoResp;
import com.haodaibao.fanbeiapp.repository.json.ShanghufenleiResp;
import com.haodaibao.fanbeiapp.repository.json.ShangquanResp;
import com.haodaibao.fanbeiapp.repository.json.ShopKindListBean;
import com.haodaibao.fanbeiapp.repository.json.ShopKindsBean;
import com.haodaibao.fanbeiapp.repository.json.SuggestBean;
import com.haodaibao.fanbeiapp.repository.json.SuggestResp;
import com.haodaibao.fanbeiapp.repository.json.YaoheListBean;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public interface LifeService {

    @GET("v1/member/user/myUserInterest")
    Observable<Data<CollectionsBean>> getCollections(@QueryMap Map<String, String> params);

    //获取优惠券列表
    @GET("v1/trade/coupon/getCoupons")
    Observable<Data<MyCouponsResp>> getCoupons(@QueryMap Map<String, String> params);

    //
    @FormUrlEncoded
    @POST("v1/trade/merchant/getMerchantActivityList")
    Observable<Data<LabelListResp>> getMerchantActivityList(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("v1/trade/advert/selectAdvertInfos")
    Observable<Data<AdInfoResp>> getAdvertInfos(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("v1/trade/message/listMerMsgs")
    Observable<Data<YaoheListBean>> getYaoheList(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST("v1/trade/merchant/getCategorysNew")
    Observable<Data<ShanghufenleiResp>> getClassification(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("v1/trade/merchant/getPubCitysByOpen")
    Observable<Data<OpenCitysInfoResp>> getPubCitysByOpen(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("v1/trade/merchant/getProvinceCityDistrict")
    Observable<Data<ProvinCityDistInfoResp>> getProvinceCityDistrict(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("v1/trade/merchant/getCityDistrictArea")
    Observable<Data<ShangquanResp>> getShangquan(@FieldMap Map<String, String> params);

    // @FormUrlEncoded
    // @POST("v1/trade/merchant/queryMerchantNew")
    // Observable<Data<MerchantInfoResp>> getShanghuListInfo(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("v1/trade/merchant/getActivityLabelList")
    Observable<Data<LabelListResp>> getLabelList(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("v1/trade/merchant/new/queryMerchant")
    Observable<Data<MerchantInfoResp>> getShanghuListInfo(@FieldMap Map<String, String> params);

    //获取买单记录页面消费及优惠信息
    @GET("v1/trade/order/myTotalPayAndDiscount")
    Observable<Data<PaylistTopBean>> getPaylistTopData();

    //获取买单记录页面
    @GET("v1/trade/order/myOrdersInfo")
    Observable<Data<PaylistItemBean>> getPaylistRvData(@QueryMap Map<String, String> params);

    //获取订单详情
    @GET("v1/trade/order/myOrderDetailAndLPayflow")
    Observable<Data<PayInfoDetailBean>> getPayInfoDetail(@QueryMap Map<String, String> params);

    //获取优惠券列表
    @GET("v1/trade/coupon/myCoupons")
    Observable<Data<CouponBean>> getCouponsList(@QueryMap Map<String, String> params);

    //创建订单
    @GET("v1/trade/order/co")
    Observable<Data<PayProcessResp>> createOrder(@QueryMap Map<String, String> params);

    //关闭订单
    @GET("v1/trade/order/closeOrder")
    Observable<Data<Object>> cancelPayment(@QueryMap Map<String, String> params);

    //获取白花花状态信息
    @GET("v1/trade/version/getVersionInfo")
    Observable<Data<BhhStatueBean>> getBhhStatue(@QueryMap Map<String, String> params);

    //获取余额
    @GET("v1/member/user/myBalance")
    Observable<Data<BalanceBean>> getBalance();

    //获取消息列表
    @FormUrlEncoded
    @POST("v1/trade/message/userMsgs")
    Observable<Data<MessageListBean>> getMessageList(@FieldMap Map<String, String> params);

    //标记消息已读
    @FormUrlEncoded
    @POST("v1/trade/message/read")
    Observable<Data> setMsgReaded(@FieldMap Map<String, String> params);

    //删除消息
    @FormUrlEncoded
    @POST("v1/trade/message/delete")
    Observable<Data> deletMsg(@FieldMap Map<String, String> params);

    //提交意见
    @FormUrlEncoded
    @POST("v1/basic/feedback/save_old")
    Observable<Data<SuggestBean>> submitSuggest(@FieldMap Map<String, String> params);

    //获取意见列表
    @GET("v1/basic/feedback/history")
    Observable<Data<SuggestResp>> getSuggestList(@QueryMap Map<String, String> params);

    //获取帮助列表
    @FormUrlEncoded
    @POST("v1/basic/help/list_visible")
    Observable<Data<HelpBean>> getHelpList(@FieldMap Map<String, String> params);//获取帮助列表

    //获取白花花帮助列表
    @FormUrlEncoded
    @POST("v1/basic/help/list_visible_uid")
    Observable<Data<HelpBean>> getBhhHelpList(@FieldMap Map<String, String> params);

    //获取套餐详情
    @FormUrlEncoded
    @POST("v1/trade/merchant/queryMerchantPackageDetail")
    Observable<Data<PackageAllBean>> getPackageDetail(@FieldMap Map<String, String> params);

    //套餐购买优惠规则验证
    @FormUrlEncoded
    @POST("v1/trade/order/checkMerchantPackageRule")
    Observable<Data<PackageAllBean>> checkMerchantPackageRule(@FieldMap Map<String, String> params);

    //首页地图商户列表
    @FormUrlEncoded
    @POST("v1/trade/merchant/new/queryMerchantList")
    Observable<Data<MerchantInfoResp>> getMapShanghuListInfo(@FieldMap Map<String, String> params);

    //首页地图吆喝
    @FormUrlEncoded
    @POST("v1/trade/merchant/new/queryMerchantMsgList")
    Observable<Data<YaoheListBean>> queryMerchantMsgList(@FieldMap Map<String, String> params);

    //我的发票
    @FormUrlEncoded
    @POST("v1/trade/invoice/list")
    Observable<Data<InvoiceBean>> getInvoiceList(@FieldMap Map<String, String> params);

    //获取申请发票页面信息
    @FormUrlEncoded
    @POST("v1/trade/invoice/apply")
    Observable<Data<MakeoutApplyBean>> getApplyInfo(@FieldMap Map<String, String> params);

    //开票
    @FormUrlEncoded
    @POST("v1/trade/invoice/addOrModify")
    Observable<Data<MakeoutInvoiceBean>> applyInvoice(@FieldMap Map<String, String> params);

    //我的抬头列表
    @FormUrlEncoded
    @POST("v1/trade/rise/list")
    Observable<Data<InvoiceHeadBean>> getRise(@FieldMap Map<String, String> params);

    //添加修改抬头
    @FormUrlEncoded
    @POST("v1/trade/rise/addOrModify")
    Observable<Data> addOrModify(@FieldMap Map<String, String> params);

    //我的发票明细
    @FormUrlEncoded
    @POST("v1/trade/invoice/detail")
    Observable<Data<InvoiceDetailBean>> getInvoiceDetail(@FieldMap Map<String, String> params);

    //我的抬头明细
    @FormUrlEncoded
    @POST("v1/trade/rise/detail")
    Observable<Data<InvoiceRiseDetailBean>> getInvoiceRiseDetail(@FieldMap Map<String, String> params);

    /**
     * 获取首页商铺分类列表
     * @param parms
     * @return
     */
    @FormUrlEncoded
    @POST("v1/trade/index/queryRecommendMerchant")
    Observable<Data<ShopKindListBean>> getShopKind(@FieldMap HashMap<String, String> parms);

}
