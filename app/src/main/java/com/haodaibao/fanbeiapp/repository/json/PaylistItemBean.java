package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.util.List;

/**
 * Created by Routee on 2017/9/7.
 * description: ${cusor}
 */
@Keep
public class PaylistItemBean {

    /**
     * orders : {"limit":"5","slider":["1"],"lastPage":true,"hasNextPage":false,"nextPage":"1","hasPrePage":false,"endRow":"1","prePage":"1","totalCount":"1","page":"1","items":[{"orderno":"ORD2017083110000000398939","userno":"U1201704181000129558","bsncode":"1001","consumptionmode":"1","accountno":"","accountname":"","bankno":"","bankname":"","merchantno":"M20170412307306","ordertime":"20170831202402","terminaltime":"","orderamt":"0.01","paytime":"20170831202419","couponamt":"","cashamt":"0.01","notrefundamt":"0.00","refundamt":"0.01","userrem":"","merchantrem":"","posuser":"","pickcode":"","picktime":"","pickmerchantno":"","orderstatus":"20","instatus":"","refundstatus":"01","commentstatus":"50","settlestatus":"","rewardstatus":"20","clearstatus":"","settleamount":"","rewardamount":"0.00","reduceamount":"0.00","saleprofit":"","settleno":"","rewardno":"","clearno":"","sourceorganizationno":"","channel":"","authremark":"","payfee":"","pickUpDate":"","productList":[],"statusmessage":"买单成功","bsncodemessage":"支付","merchantname":"喜相逢","tradeNo":"","balanceAmt":"","productname":"","merfee":"","refundmode":"","discount":"","discount2":"","returnrate":"","returnrate2":"","returnbeginamount":"","returnbegin2amount":"","imageUrl":"5f36fb63b042a1a004a2b7be10b94db6.jpg","payType":"23","topamount":"","couponsharestatus":"","prepayId":"","nonceStr":"","timeStamp":"","partnerId":"","weChatPackage":"","orderInfo":"","sign":"","notifyUrl":"","submitMethod":"","submitUrl":"","submitParams":"","hostreturncode":"","hostreturnmessage":"","hostserialno":"","voucherNo":"","terminalno":"","bankReduceAmount":""}],"startRow":"1","firstPage":true,"offset":"0","totalPages":"1"}
     */

    private OrdersBean orders;

    public OrdersBean getOrders() {
        return orders;
    }

    public void setOrders(OrdersBean orders) {
        this.orders = orders;
    }

    @Keep
    public static class OrdersBean {
        /**
         * limit : 5
         * slider : ["1"]
         * lastPage : true
         * hasNextPage : false
         * nextPage : 1
         * hasPrePage : false
         * endRow : 1
         * prePage : 1
         * totalCount : 1
         * page : 1
         * items : [{"orderno":"ORD2017083110000000398939","userno":"U1201704181000129558","bsncode":"1001","consumptionmode":"1","accountno":"","accountname":"","bankno":"","bankname":"","merchantno":"M20170412307306","ordertime":"20170831202402","terminaltime":"","orderamt":"0.01","paytime":"20170831202419","couponamt":"","cashamt":"0.01","notrefundamt":"0.00","refundamt":"0.01","userrem":"","merchantrem":"","posuser":"","pickcode":"","picktime":"","pickmerchantno":"","orderstatus":"20","instatus":"","refundstatus":"01","commentstatus":"50","settlestatus":"","rewardstatus":"20","clearstatus":"","settleamount":"","rewardamount":"0.00","reduceamount":"0.00","saleprofit":"","settleno":"","rewardno":"","clearno":"","sourceorganizationno":"","channel":"","authremark":"","payfee":"","pickUpDate":"","productList":[],"statusmessage":"买单成功","bsncodemessage":"支付","merchantname":"喜相逢","tradeNo":"","balanceAmt":"","productname":"","merfee":"","refundmode":"","discount":"","discount2":"","returnrate":"","returnrate2":"","returnbeginamount":"","returnbegin2amount":"","imageUrl":"5f36fb63b042a1a004a2b7be10b94db6.jpg","payType":"23","topamount":"","couponsharestatus":"","prepayId":"","nonceStr":"","timeStamp":"","partnerId":"","weChatPackage":"","orderInfo":"","sign":"","notifyUrl":"","submitMethod":"","submitUrl":"","submitParams":"","hostreturncode":"","hostreturnmessage":"","hostserialno":"","voucherNo":"","terminalno":"","bankReduceAmount":""}]
         * startRow : 1
         * firstPage : true
         * offset : 0
         * totalPages : 1
         */

        private String limit;
        private boolean lastPage;
        private boolean hasNextPage;
        private String nextPage;
        private boolean hasPrePage;
        private String endRow;
        private String prePage;
        private String totalCount;
        private String page;
        private String startRow;
        private boolean firstPage;
        private String offset;
        private String totalPages;
        private List<String> slider;
        private List<ItemsBean> items;

        public String getLimit() {
            return limit;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

        public boolean isLastPage() {
            return lastPage;
        }

        public void setLastPage(boolean lastPage) {
            this.lastPage = lastPage;
        }

        public boolean isHasNextPage() {
            return hasNextPage;
        }

        public void setHasNextPage(boolean hasNextPage) {
            this.hasNextPage = hasNextPage;
        }

        public String getNextPage() {
            return nextPage;
        }

        public void setNextPage(String nextPage) {
            this.nextPage = nextPage;
        }

        public boolean isHasPrePage() {
            return hasPrePage;
        }

        public void setHasPrePage(boolean hasPrePage) {
            this.hasPrePage = hasPrePage;
        }

        public String getEndRow() {
            return endRow;
        }

        public void setEndRow(String endRow) {
            this.endRow = endRow;
        }

        public String getPrePage() {
            return prePage;
        }

        public void setPrePage(String prePage) {
            this.prePage = prePage;
        }

        public String getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(String totalCount) {
            this.totalCount = totalCount;
        }

        public String getPage() {
            return page;
        }

        public void setPage(String page) {
            this.page = page;
        }

        public String getStartRow() {
            return startRow;
        }

        public void setStartRow(String startRow) {
            this.startRow = startRow;
        }

        public boolean isFirstPage() {
            return firstPage;
        }

        public void setFirstPage(boolean firstPage) {
            this.firstPage = firstPage;
        }

        public String getOffset() {
            return offset;
        }

        public void setOffset(String offset) {
            this.offset = offset;
        }

        public String getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(String totalPages) {
            this.totalPages = totalPages;
        }

        public List<String> getSlider() {
            return slider;
        }

        public void setSlider(List<String> slider) {
            this.slider = slider;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        @Keep
        public static class ItemsBean {

            private String orderno;
            private String userno;
            private String bsncode;
            private String consumptionmode;
            private String accountno;
            private String accountname;
            private String bankno;
            private String bankname;
            private String merchantno;
            private String ordertime;
            private String terminaltime;
            private String orderamt;
            private String paytime;
            private String couponamt;
            private String cashamt;
            private String notrefundamt;
            private String refundamt;
            private String userrem;
            private String merchantrem;
            private String posuser;
            private String pickcode;
            private String picktime;
            private String pickmerchantno;
            private String orderstatus;
            private String instatus;
            private String refundstatus;
            private String commentstatus;
            private String settlestatus;
            private String rewardstatus;
            private String clearstatus;
            private String settleamount;
            private String rewardamount;
            private String reduceamount;
            private String saleprofit;
            private String settleno;
            private String rewardno;
            private String clearno;
            private String sourceorganizationno;
            private String channel;
            private String authremark;
            private String payfee;
            private String pickUpDate;
            private String statusmessage;
            private String bsncodemessage;
            private String merchantname;
            private String tradeNo;
            private String balanceAmt;
            private String productname;
            private String merfee;
            private String refundmode;
            private String discount;
            private String discount2;
            private String returnrate;
            private String returnrate2;
            private String returnbeginamount;
            private String returnbegin2amount;
            private String imageUrl;
            private String payType;
            private String topamount;
            private String couponsharestatus;
            private String prepayId;
            private String nonceStr;
            private String timeStamp;
            private String partnerId;
            private String weChatPackage;
            private String orderInfo;
            private String sign;
            private String notifyUrl;
            private String submitMethod;
            private String submitUrl;
            private String submitParams;
            private String hostreturncode;
            private String hostreturnmessage;
            private String hostserialno;
            private String voucherNo;
            private String terminalno;
            private String bankReduceAmount;
            private List<?> productList;
            public String relationNo;
            public String relationType;
            public String preferentialPackage;
            public String packageNo;
            public String preferentialPrice;
            public String originalPrice;
            public String faceUserTypeCode;
            public String packageName;
            public String citycode;
            public String address;
            public String invoiceId;
            public String invoiceStatus;
            public String canInvoice;


            public String getOrderno() {
                return orderno;
            }

            public void setOrderno(String orderno) {
                this.orderno = orderno;
            }

            public String getUserno() {
                return userno;
            }

            public void setUserno(String userno) {
                this.userno = userno;
            }

            public String getBsncode() {
                return bsncode;
            }

            public void setBsncode(String bsncode) {
                this.bsncode = bsncode;
            }

            public String getConsumptionmode() {
                return consumptionmode;
            }

            public void setConsumptionmode(String consumptionmode) {
                this.consumptionmode = consumptionmode;
            }

            public String getAccountno() {
                return accountno;
            }

            public void setAccountno(String accountno) {
                this.accountno = accountno;
            }

            public String getAccountname() {
                return accountname;
            }

            public void setAccountname(String accountname) {
                this.accountname = accountname;
            }

            public String getBankno() {
                return bankno;
            }

            public void setBankno(String bankno) {
                this.bankno = bankno;
            }

            public String getBankname() {
                return bankname;
            }

            public void setBankname(String bankname) {
                this.bankname = bankname;
            }

            public String getMerchantno() {
                return merchantno;
            }

            public void setMerchantno(String merchantno) {
                this.merchantno = merchantno;
            }

            public String getOrdertime() {
                return ordertime;
            }

            public void setOrdertime(String ordertime) {
                this.ordertime = ordertime;
            }

            public String getTerminaltime() {
                return terminaltime;
            }

            public void setTerminaltime(String terminaltime) {
                this.terminaltime = terminaltime;
            }

            public String getOrderamt() {
                return orderamt;
            }

            public void setOrderamt(String orderamt) {
                this.orderamt = orderamt;
            }

            public String getPaytime() {
                return paytime;
            }

            public void setPaytime(String paytime) {
                this.paytime = paytime;
            }

            public String getCouponamt() {
                return couponamt;
            }

            public void setCouponamt(String couponamt) {
                this.couponamt = couponamt;
            }

            public String getCashamt() {
                return cashamt;
            }

            public void setCashamt(String cashamt) {
                this.cashamt = cashamt;
            }

            public String getNotrefundamt() {
                return notrefundamt;
            }

            public void setNotrefundamt(String notrefundamt) {
                this.notrefundamt = notrefundamt;
            }

            public String getRefundamt() {
                return refundamt;
            }

            public void setRefundamt(String refundamt) {
                this.refundamt = refundamt;
            }

            public String getUserrem() {
                return userrem;
            }

            public void setUserrem(String userrem) {
                this.userrem = userrem;
            }

            public String getMerchantrem() {
                return merchantrem;
            }

            public void setMerchantrem(String merchantrem) {
                this.merchantrem = merchantrem;
            }

            public String getPosuser() {
                return posuser;
            }

            public void setPosuser(String posuser) {
                this.posuser = posuser;
            }

            public String getPickcode() {
                return pickcode;
            }

            public void setPickcode(String pickcode) {
                this.pickcode = pickcode;
            }

            public String getPicktime() {
                return picktime;
            }

            public void setPicktime(String picktime) {
                this.picktime = picktime;
            }

            public String getPickmerchantno() {
                return pickmerchantno;
            }

            public void setPickmerchantno(String pickmerchantno) {
                this.pickmerchantno = pickmerchantno;
            }

            public String getOrderstatus() {
                return orderstatus;
            }

            public void setOrderstatus(String orderstatus) {
                this.orderstatus = orderstatus;
            }

            public String getInstatus() {
                return instatus;
            }

            public void setInstatus(String instatus) {
                this.instatus = instatus;
            }

            public String getRefundstatus() {
                return refundstatus;
            }

            public void setRefundstatus(String refundstatus) {
                this.refundstatus = refundstatus;
            }

            public String getCommentstatus() {
                return commentstatus;
            }

            public void setCommentstatus(String commentstatus) {
                this.commentstatus = commentstatus;
            }

            public String getSettlestatus() {
                return settlestatus;
            }

            public void setSettlestatus(String settlestatus) {
                this.settlestatus = settlestatus;
            }

            public String getRewardstatus() {
                return rewardstatus;
            }

            public void setRewardstatus(String rewardstatus) {
                this.rewardstatus = rewardstatus;
            }

            public String getClearstatus() {
                return clearstatus;
            }

            public void setClearstatus(String clearstatus) {
                this.clearstatus = clearstatus;
            }

            public String getSettleamount() {
                return settleamount;
            }

            public void setSettleamount(String settleamount) {
                this.settleamount = settleamount;
            }

            public String getRewardamount() {
                return rewardamount;
            }

            public void setRewardamount(String rewardamount) {
                this.rewardamount = rewardamount;
            }

            public String getReduceamount() {
                return reduceamount;
            }

            public void setReduceamount(String reduceamount) {
                this.reduceamount = reduceamount;
            }

            public String getSaleprofit() {
                return saleprofit;
            }

            public void setSaleprofit(String saleprofit) {
                this.saleprofit = saleprofit;
            }

            public String getSettleno() {
                return settleno;
            }

            public void setSettleno(String settleno) {
                this.settleno = settleno;
            }

            public String getRewardno() {
                return rewardno;
            }

            public void setRewardno(String rewardno) {
                this.rewardno = rewardno;
            }

            public String getClearno() {
                return clearno;
            }

            public void setClearno(String clearno) {
                this.clearno = clearno;
            }

            public String getSourceorganizationno() {
                return sourceorganizationno;
            }

            public void setSourceorganizationno(String sourceorganizationno) {
                this.sourceorganizationno = sourceorganizationno;
            }

            public String getChannel() {
                return channel;
            }

            public void setChannel(String channel) {
                this.channel = channel;
            }

            public String getAuthremark() {
                return authremark;
            }

            public void setAuthremark(String authremark) {
                this.authremark = authremark;
            }

            public String getPayfee() {
                return payfee;
            }

            public void setPayfee(String payfee) {
                this.payfee = payfee;
            }

            public String getPickUpDate() {
                return pickUpDate;
            }

            public void setPickUpDate(String pickUpDate) {
                this.pickUpDate = pickUpDate;
            }

            public String getStatusmessage() {
                return statusmessage;
            }

            public void setStatusmessage(String statusmessage) {
                this.statusmessage = statusmessage;
            }

            public String getBsncodemessage() {
                return bsncodemessage;
            }

            public void setBsncodemessage(String bsncodemessage) {
                this.bsncodemessage = bsncodemessage;
            }

            public String getMerchantname() {
                return merchantname;
            }

            public void setMerchantname(String merchantname) {
                this.merchantname = merchantname;
            }

            public String getTradeNo() {
                return tradeNo;
            }

            public void setTradeNo(String tradeNo) {
                this.tradeNo = tradeNo;
            }

            public String getBalanceAmt() {
                return balanceAmt;
            }

            public void setBalanceAmt(String balanceAmt) {
                this.balanceAmt = balanceAmt;
            }

            public String getProductname() {
                return productname;
            }

            public void setProductname(String productname) {
                this.productname = productname;
            }

            public String getMerfee() {
                return merfee;
            }

            public void setMerfee(String merfee) {
                this.merfee = merfee;
            }

            public String getRefundmode() {
                return refundmode;
            }

            public void setRefundmode(String refundmode) {
                this.refundmode = refundmode;
            }

            public String getDiscount() {
                return discount;
            }

            public void setDiscount(String discount) {
                this.discount = discount;
            }

            public String getDiscount2() {
                return discount2;
            }

            public void setDiscount2(String discount2) {
                this.discount2 = discount2;
            }

            public String getReturnrate() {
                return returnrate;
            }

            public void setReturnrate(String returnrate) {
                this.returnrate = returnrate;
            }

            public String getReturnrate2() {
                return returnrate2;
            }

            public void setReturnrate2(String returnrate2) {
                this.returnrate2 = returnrate2;
            }

            public String getReturnbeginamount() {
                return returnbeginamount;
            }

            public void setReturnbeginamount(String returnbeginamount) {
                this.returnbeginamount = returnbeginamount;
            }

            public String getReturnbegin2amount() {
                return returnbegin2amount;
            }

            public void setReturnbegin2amount(String returnbegin2amount) {
                this.returnbegin2amount = returnbegin2amount;
            }

            public String getImageUrl() {
                return imageUrl;
            }

            public void setImageUrl(String imageUrl) {
                this.imageUrl = imageUrl;
            }

            public String getPayType() {
                return payType;
            }

            public void setPayType(String payType) {
                this.payType = payType;
            }

            public String getTopamount() {
                return topamount;
            }

            public void setTopamount(String topamount) {
                this.topamount = topamount;
            }

            public String getCouponsharestatus() {
                return couponsharestatus;
            }

            public void setCouponsharestatus(String couponsharestatus) {
                this.couponsharestatus = couponsharestatus;
            }

            public String getPrepayId() {
                return prepayId;
            }

            public void setPrepayId(String prepayId) {
                this.prepayId = prepayId;
            }

            public String getNonceStr() {
                return nonceStr;
            }

            public void setNonceStr(String nonceStr) {
                this.nonceStr = nonceStr;
            }

            public String getTimeStamp() {
                return timeStamp;
            }

            public void setTimeStamp(String timeStamp) {
                this.timeStamp = timeStamp;
            }

            public String getPartnerId() {
                return partnerId;
            }

            public void setPartnerId(String partnerId) {
                this.partnerId = partnerId;
            }

            public String getWeChatPackage() {
                return weChatPackage;
            }

            public void setWeChatPackage(String weChatPackage) {
                this.weChatPackage = weChatPackage;
            }

            public String getOrderInfo() {
                return orderInfo;
            }

            public void setOrderInfo(String orderInfo) {
                this.orderInfo = orderInfo;
            }

            public String getSign() {
                return sign;
            }

            public void setSign(String sign) {
                this.sign = sign;
            }

            public String getNotifyUrl() {
                return notifyUrl;
            }

            public void setNotifyUrl(String notifyUrl) {
                this.notifyUrl = notifyUrl;
            }

            public String getSubmitMethod() {
                return submitMethod;
            }

            public void setSubmitMethod(String submitMethod) {
                this.submitMethod = submitMethod;
            }

            public String getSubmitUrl() {
                return submitUrl;
            }

            public void setSubmitUrl(String submitUrl) {
                this.submitUrl = submitUrl;
            }

            public String getSubmitParams() {
                return submitParams;
            }

            public void setSubmitParams(String submitParams) {
                this.submitParams = submitParams;
            }

            public String getHostreturncode() {
                return hostreturncode;
            }

            public void setHostreturncode(String hostreturncode) {
                this.hostreturncode = hostreturncode;
            }

            public String getHostreturnmessage() {
                return hostreturnmessage;
            }

            public void setHostreturnmessage(String hostreturnmessage) {
                this.hostreturnmessage = hostreturnmessage;
            }

            public String getHostserialno() {
                return hostserialno;
            }

            public void setHostserialno(String hostserialno) {
                this.hostserialno = hostserialno;
            }

            public String getVoucherNo() {
                return voucherNo;
            }

            public void setVoucherNo(String voucherNo) {
                this.voucherNo = voucherNo;
            }

            public String getTerminalno() {
                return terminalno;
            }

            public void setTerminalno(String terminalno) {
                this.terminalno = terminalno;
            }

            public String getBankReduceAmount() {
                return bankReduceAmount;
            }

            public void setBankReduceAmount(String bankReduceAmount) {
                this.bankReduceAmount = bankReduceAmount;
            }

            public List<?> getProductList() {
                return productList;
            }

            public void setProductList(List<?> productList) {
                this.productList = productList;
            }
        }
    }
}
