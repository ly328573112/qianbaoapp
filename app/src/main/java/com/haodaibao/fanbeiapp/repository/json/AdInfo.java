package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

@Keep
public class AdInfo extends JingPinBean {
    private String contentid;// 物料id
    private String positionid;// 广告位id
    private String contentname;// 物料名称
    private String contentrem;// 物料备注
    private String imageurl;// 图片地址
    private String linktype;// 链接类型
    private String content;// 物料内容
    private String defaultcontent;// 是否默认
    private String positionname;// 广告位名称
    private String contentnum;// 数量
    private String contentlength;// 长度
    private String contentwidth;// 宽度

    public String getContentid() {
        return contentid;
    }

    public void setContentid(String contentid) {
        this.contentid = contentid;
    }

    public String getPositionid() {
        return positionid;
    }

    public void setPositionid(String positionid) {
        this.positionid = positionid;
    }

    public String getContentname() {
        return contentname;
    }

    public void setContentname(String contentname) {
        this.contentname = contentname;
    }

    public String getContentrem() {
        return contentrem;
    }

    public void setContentrem(String contentrem) {
        this.contentrem = contentrem;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getLinktype() {
        return linktype;
    }

    public void setLinktype(String linktype) {
        this.linktype = linktype;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDefaultcontent() {
        return defaultcontent;
    }

    public void setDefaultcontent(String defaultcontent) {
        this.defaultcontent = defaultcontent;
    }

    public String getPositionname() {
        return positionname;
    }

    public void setPositionname(String positionname) {
        this.positionname = positionname;
    }

    public String getContentnum() {
        return contentnum;
    }

    public void setContentnum(String contentnum) {
        this.contentnum = contentnum;
    }

    public String getContentlength() {
        return contentlength;
    }

    public void setContentlength(String contentlength) {
        this.contentlength = contentlength;
    }

    public String getContentwidth() {
        return contentwidth;
    }

    public void setContentwidth(String contentwidth) {
        this.contentwidth = contentwidth;
    }

    @Override
    public String toString() {
        return "AdInfo{" +
                "contentid='" + contentid + '\'' +
                ", positionid='" + positionid + '\'' +
                ", contentname='" + contentname + '\'' +
                ", contentrem='" + contentrem + '\'' +
                ", imageurl='" + imageurl + '\'' +
                ", linktype='" + linktype + '\'' +
                ", content='" + content + '\'' +
                ", defaultcontent='" + defaultcontent + '\'' +
                ", positionname='" + positionname + '\'' +
                ", contentnum='" + contentnum + '\'' +
                ", contentlength='" + contentlength + '\'' +
                ", contentwidth='" + contentwidth + '\'' +
                '}';
    }
}
