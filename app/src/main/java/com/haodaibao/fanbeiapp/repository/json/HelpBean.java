package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.util.List;

/**
 * Created by hdb on 2017/5/22.
 * description: ${TODO}
 */

@Keep
public class HelpBean {

    private List<HelpListBean> helpList;

    public List<HelpListBean> getHelpList() {
        return helpList;
    }

    public void setHelpList(List<HelpListBean> helpList) {
        this.helpList = helpList;
    }

    @Keep
    public static class HelpListBean implements HelpInterface {
        /**
         * createTime : 20170921143808 createUserId : U4201501011000000001 helpContentList
         * : [{"content":"提现请拨行等信息","contenttype":"01","createTime":"20170921144130","createUserId":"U4201501011000000001","id":"248","modifyTime":"20170921144437","modifyUserId":"U4201501011000000001","showFlag":"1","sortPosition":"0","title":"余额如何提现？","topicId":"138","uid":"3_5859511973"}]
         * modifyTime : 20170921144148 modifyUserId : U4201501011000000001 sortPosition :
         * 0 topicId : 138 topicName : 余额提现 topicParentId : 1 topicPlatform : 3
         * topicShowFlag : 1 topicUid : 3_5822058223
         */

        private String createTime;
        private String createUserId;
        private String modifyTime;
        private String modifyUserId;
        private String sortPosition;
        private String topicId;
        private String topicName;
        private String topicParentId;
        private String topicPlatform;
        private String topicShowFlag;
        private String topicUid;
        private List<HelpContentListBean> helpContentList;

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getCreateUserId() {
            return createUserId;
        }

        public void setCreateUserId(String createUserId) {
            this.createUserId = createUserId;
        }

        public String getModifyTime() {
            return modifyTime;
        }

        public void setModifyTime(String modifyTime) {
            this.modifyTime = modifyTime;
        }

        public String getModifyUserId() {
            return modifyUserId;
        }

        public void setModifyUserId(String modifyUserId) {
            this.modifyUserId = modifyUserId;
        }

        public String getSortPosition() {
            return sortPosition;
        }

        public void setSortPosition(String sortPosition) {
            this.sortPosition = sortPosition;
        }

        public String getTopicId() {
            return topicId;
        }

        public void setTopicId(String topicId) {
            this.topicId = topicId;
        }

        public String getTopicName() {
            return topicName;
        }

        public void setTopicName(String topicName) {
            this.topicName = topicName;
        }

        public String getTopicParentId() {
            return topicParentId;
        }

        public void setTopicParentId(String topicParentId) {
            this.topicParentId = topicParentId;
        }

        public String getTopicPlatform() {
            return topicPlatform;
        }

        public void setTopicPlatform(String topicPlatform) {
            this.topicPlatform = topicPlatform;
        }

        public String getTopicShowFlag() {
            return topicShowFlag;
        }

        public void setTopicShowFlag(String topicShowFlag) {
            this.topicShowFlag = topicShowFlag;
        }

        public String getTopicUid() {
            return topicUid;
        }

        public void setTopicUid(String topicUid) {
            this.topicUid = topicUid;
        }

        public List<HelpContentListBean> getHelpContentList() {
            return helpContentList;
        }

        public void setHelpContentList(List<HelpContentListBean> helpContentList) {
            this.helpContentList = helpContentList;
        }

        @Keep
        public static class HelpContentListBean implements HelpInterface {
            /**
             * content : 提现请拨行等信息
             * contenttype : 01
             * createTime : 20170921144130
             * createUserId : U4201501011000000001
             * id : 248
             * modifyTime : 20170921144437
             * modifyUserId : U4201501011000000001
             * showFlag : 1
             * sortPosition : 0
             * title : 余额如何提现？
             * topicId : 138
             * uid : 3_5859511973
             */

            private String content;
            private String contenttype;
            private String createTime;
            private String createUserId;
            private String id;
            private String modifyTime;
            private String modifyUserId;
            private String showFlag;
            private String sortPosition;
            private String title;
            private String topicId;
            private String uid;

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getContenttype() {
                return contenttype;
            }

            public void setContenttype(String contenttype) {
                this.contenttype = contenttype;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public String getCreateUserId() {
                return createUserId;
            }

            public void setCreateUserId(String createUserId) {
                this.createUserId = createUserId;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getModifyTime() {
                return modifyTime;
            }

            public void setModifyTime(String modifyTime) {
                this.modifyTime = modifyTime;
            }

            public String getModifyUserId() {
                return modifyUserId;
            }

            public void setModifyUserId(String modifyUserId) {
                this.modifyUserId = modifyUserId;
            }

            public String getShowFlag() {
                return showFlag;
            }

            public void setShowFlag(String showFlag) {
                this.showFlag = showFlag;
            }

            public String getSortPosition() {
                return sortPosition;
            }

            public void setSortPosition(String sortPosition) {
                this.sortPosition = sortPosition;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getTopicId() {
                return topicId;
            }

            public void setTopicId(String topicId) {
                this.topicId = topicId;
            }

            public String getUid() {
                return uid;
            }

            public void setUid(String uid) {
                this.uid = uid;
            }
        }
    }
}
