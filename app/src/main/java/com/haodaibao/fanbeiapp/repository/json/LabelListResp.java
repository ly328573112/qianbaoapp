package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;
import java.util.List;

/**
 * 开发者：LuoYi
 * Time: 2018 15:48 2018/4/11 04
 */
@Keep
public class LabelListResp implements Serializable{

    private List<LableInfo> labels;
    private List<LableInfo> activities;

    public List<LableInfo> getLableInfo() {
        return labels;
    }

    public List<LableInfo> getActivities() {
        return activities;
    }
}
