package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

/**
 * 商圈信息
 */
@Keep
public class AreaList {
    // 商圈编号
    private String code;
    // 商圈名称
    private String name;
    // 区县编号
    private String districtcode;
    private String status;

    public AreaList(String code, String name, String districtcode, String status) {
        super();
        this.code = code;
        this.name = name;
        this.districtcode = districtcode;
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDistrictcode() {
        return districtcode;
    }

    public void setDistrictcode(String districtcode) {
        this.districtcode = districtcode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
