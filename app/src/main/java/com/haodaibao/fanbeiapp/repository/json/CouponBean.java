package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.util.List;

/**
 * Created by Routee on 2017/9/12.
 * description: ${cusor}
 */
@Keep
public class CouponBean {

    /**
     * couponList : {"limit":"10","slider":["1"],"lastPage":true,"hasNextPage":false,"nextPage":"1","hasPrePage":false,"endRow":"3","prePage":"1","totalCount":"3","page":"1","items":[{"couponno":"20170824691885","couponshortname":"满1.01减1","usescope":"00","merchantno":"O201501011000000000","merchantname":"","amount":"1.00","starttime":"20170825102733","endtime":"20170826235959","status":"0","createtime":"","role":"满1.01减1","usebeginamt":"1.01","categoryno":"ALL","serialno":"2017082510000000367170","gettype":"1","userno":"U1201708251000243411","gettime":"20170825102733","orderamt":"","usetime":"","orderno":"","shareid":"","sharetype":"","isFailure":"02","groupno":"","categoryNo":"","couponName":"新用户注册红包0824","couponText":"","categoryText":"","bankNo":"","bankName":"","citys":[]},{"couponno":"20170825691886","couponshortname":"11111","usescope":"00","merchantno":"O201501011000000000","merchantname":"","amount":"3.00","starttime":"20170825000000","endtime":"20170828235959","status":"0","createtime":"","role":"111111","usebeginamt":"3.01","categoryno":"ALL","serialno":"2017082510000000367219","gettype":"2","userno":"U1201708251000243411","gettime":"20170825104320","orderamt":"","usetime":"","orderno":"","shareid":"","sharetype":"","isFailure":"02","groupno":"MCG2017082510000000367214","categoryNo":"","couponName":"新用户注册红包0825","couponText":"","categoryText":"","bankNo":"","bankName":"","citys":[]},{"couponno":"20170825691886","couponshortname":"11111","usescope":"00","merchantno":"O201501011000000000","merchantname":"","amount":"3.00","starttime":"20170901000000","endtime":"20170904235959","status":"0","createtime":"","role":"111111","usebeginamt":"3.01","categoryno":"ALL","serialno":"2017090110000000368367","gettype":"2","userno":"U1201708251000243411","gettime":"20170901135637","orderamt":"","usetime":"","orderno":"","shareid":"","sharetype":"","isFailure":"02","groupno":"MCG2017090110000000368363","categoryNo":"","couponName":"新用户注册红包0825","couponText":"","categoryText":"","bankNo":"","bankName":"","citys":[]}],"startRow":"1","firstPage":true,"offset":"0","totalPages":"1"}
     * isPay :
     * payCouponCount : 3
     */

    private CouponListBean couponList;
    private String isPay;
    private String payCouponCount;

    public CouponListBean getCouponList() {
        return couponList;
    }

    public void setCouponList(CouponListBean couponList) {
        this.couponList = couponList;
    }

    public String getIsPay() {
        return isPay;
    }

    public void setIsPay(String isPay) {
        this.isPay = isPay;
    }

    public String getPayCouponCount() {
        return payCouponCount;
    }

    public void setPayCouponCount(String payCouponCount) {
        this.payCouponCount = payCouponCount;
    }

    @Keep
    public static class CouponListBean {
        /**
         * limit : 10
         * slider : ["1"]
         * lastPage : true
         * hasNextPage : false
         * nextPage : 1
         * hasPrePage : false
         * endRow : 3
         * prePage : 1
         * totalCount : 3
         * page : 1
         * items : [{"couponno":"20170824691885","couponshortname":"满1.01减1","usescope":"00","merchantno":"O201501011000000000","merchantname":"","amount":"1.00","starttime":"20170825102733","endtime":"20170826235959","status":"0","createtime":"","role":"满1.01减1","usebeginamt":"1.01","categoryno":"ALL","serialno":"2017082510000000367170","gettype":"1","userno":"U1201708251000243411","gettime":"20170825102733","orderamt":"","usetime":"","orderno":"","shareid":"","sharetype":"","isFailure":"02","groupno":"","categoryNo":"","couponName":"新用户注册红包0824","couponText":"","categoryText":"","bankNo":"","bankName":"","citys":[]},{"couponno":"20170825691886","couponshortname":"11111","usescope":"00","merchantno":"O201501011000000000","merchantname":"","amount":"3.00","starttime":"20170825000000","endtime":"20170828235959","status":"0","createtime":"","role":"111111","usebeginamt":"3.01","categoryno":"ALL","serialno":"2017082510000000367219","gettype":"2","userno":"U1201708251000243411","gettime":"20170825104320","orderamt":"","usetime":"","orderno":"","shareid":"","sharetype":"","isFailure":"02","groupno":"MCG2017082510000000367214","categoryNo":"","couponName":"新用户注册红包0825","couponText":"","categoryText":"","bankNo":"","bankName":"","citys":[]},{"couponno":"20170825691886","couponshortname":"11111","usescope":"00","merchantno":"O201501011000000000","merchantname":"","amount":"3.00","starttime":"20170901000000","endtime":"20170904235959","status":"0","createtime":"","role":"111111","usebeginamt":"3.01","categoryno":"ALL","serialno":"2017090110000000368367","gettype":"2","userno":"U1201708251000243411","gettime":"20170901135637","orderamt":"","usetime":"","orderno":"","shareid":"","sharetype":"","isFailure":"02","groupno":"MCG2017090110000000368363","categoryNo":"","couponName":"新用户注册红包0825","couponText":"","categoryText":"","bankNo":"","bankName":"","citys":[]}]
         * startRow : 1
         * firstPage : true
         * offset : 0
         * totalPages : 1
         */

        private String limit;
        private boolean         lastPage;
        private boolean         hasNextPage;
        private String          nextPage;
        private boolean         hasPrePage;
        private String          endRow;
        private String          prePage;
        private String          totalCount;
        private String          page;
        private String          startRow;
        private boolean         firstPage;
        private String          offset;
        private String          totalPages;
        private List<String>    slider;
        private List<ItemsBean> items;

        public String getLimit() {
            return limit;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

        public boolean isLastPage() {
            return lastPage;
        }

        public void setLastPage(boolean lastPage) {
            this.lastPage = lastPage;
        }

        public boolean isHasNextPage() {
            return hasNextPage;
        }

        public void setHasNextPage(boolean hasNextPage) {
            this.hasNextPage = hasNextPage;
        }

        public String getNextPage() {
            return nextPage;
        }

        public void setNextPage(String nextPage) {
            this.nextPage = nextPage;
        }

        public boolean isHasPrePage() {
            return hasPrePage;
        }

        public void setHasPrePage(boolean hasPrePage) {
            this.hasPrePage = hasPrePage;
        }

        public String getEndRow() {
            return endRow;
        }

        public void setEndRow(String endRow) {
            this.endRow = endRow;
        }

        public String getPrePage() {
            return prePage;
        }

        public void setPrePage(String prePage) {
            this.prePage = prePage;
        }

        public String getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(String totalCount) {
            this.totalCount = totalCount;
        }

        public String getPage() {
            return page;
        }

        public void setPage(String page) {
            this.page = page;
        }

        public String getStartRow() {
            return startRow;
        }

        public void setStartRow(String startRow) {
            this.startRow = startRow;
        }

        public boolean isFirstPage() {
            return firstPage;
        }

        public void setFirstPage(boolean firstPage) {
            this.firstPage = firstPage;
        }

        public String getOffset() {
            return offset;
        }

        public void setOffset(String offset) {
            this.offset = offset;
        }

        public String getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(String totalPages) {
            this.totalPages = totalPages;
        }

        public List<String> getSlider() {
            return slider;
        }

        public void setSlider(List<String> slider) {
            this.slider = slider;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        @Keep
        public static class ItemsBean {
            /**
             * couponno : 20170824691885
             * couponshortname : 满1.01减1
             * usescope : 00
             * merchantno : O201501011000000000
             * merchantname :
             * amount : 1.00
             * starttime : 20170825102733
             * endtime : 20170826235959
             * status : 0
             * createtime :
             * role : 满1.01减1
             * usebeginamt : 1.01
             * categoryno : ALL
             * serialno : 2017082510000000367170
             * gettype : 1
             * userno : U1201708251000243411
             * gettime : 20170825102733
             * orderamt :
             * usetime :
             * orderno :
             * shareid :
             * sharetype :
             * isFailure : 02
             * groupno :
             * categoryNo :
             * couponName : 新用户注册红包0824
             * couponText :
             * categoryText :
             * bankNo :
             * bankName :
             * citys : []
             */

            private String couponno;
            private String  couponshortname;
            private String  usescope;
            private String  merchantno;
            private String  merchantname;
            private String  amount;
            private String  starttime;
            private String  endtime;
            private String  status;
            private String  createtime;
            private String  role;
            private String  usebeginamt;
            private String  categoryno;
            private String  serialno;
            private String  gettype;
            private String  userno;
            private String  gettime;
            private String  orderamt;
            private String  usetime;
            private String  orderno;
            private String  shareid;
            private String  sharetype;
            private String  isFailure;
            private String  groupno;
            private String  categoryName;
            private String  couponName;
            private String  couponText;
            private String  categoryText;
            private String  bankNo;
            private String  bankName;
            private List<?> citys;

            public String getCouponno() {
                return couponno;
            }

            public void setCouponno(String couponno) {
                this.couponno = couponno;
            }

            public String getCouponshortname() {
                return couponshortname;
            }

            public void setCouponshortname(String couponshortname) {
                this.couponshortname = couponshortname;
            }

            public String getUsescope() {
                return usescope;
            }

            public void setUsescope(String usescope) {
                this.usescope = usescope;
            }

            public String getMerchantno() {
                return merchantno;
            }

            public void setMerchantno(String merchantno) {
                this.merchantno = merchantno;
            }

            public String getMerchantname() {
                return merchantname;
            }

            public void setMerchantname(String merchantname) {
                this.merchantname = merchantname;
            }

            public String getAmount() {
                return amount;
            }

            public void setAmount(String amount) {
                this.amount = amount;
            }

            public String getStarttime() {
                return starttime;
            }

            public void setStarttime(String starttime) {
                this.starttime = starttime;
            }

            public String getEndtime() {
                return endtime;
            }

            public void setEndtime(String endtime) {
                this.endtime = endtime;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getCreatetime() {
                return createtime;
            }

            public void setCreatetime(String createtime) {
                this.createtime = createtime;
            }

            public String getRole() {
                return role;
            }

            public void setRole(String role) {
                this.role = role;
            }

            public String getUsebeginamt() {
                return usebeginamt;
            }

            public void setUsebeginamt(String usebeginamt) {
                this.usebeginamt = usebeginamt;
            }

            public String getCategoryno() {
                return categoryno;
            }

            public void setCategoryno(String categoryno) {
                this.categoryno = categoryno;
            }

            public String getSerialno() {
                return serialno;
            }

            public void setSerialno(String serialno) {
                this.serialno = serialno;
            }

            public String getGettype() {
                return gettype;
            }

            public void setGettype(String gettype) {
                this.gettype = gettype;
            }

            public String getUserno() {
                return userno;
            }

            public void setUserno(String userno) {
                this.userno = userno;
            }

            public String getGettime() {
                return gettime;
            }

            public void setGettime(String gettime) {
                this.gettime = gettime;
            }

            public String getOrderamt() {
                return orderamt;
            }

            public void setOrderamt(String orderamt) {
                this.orderamt = orderamt;
            }

            public String getUsetime() {
                return usetime;
            }

            public void setUsetime(String usetime) {
                this.usetime = usetime;
            }

            public String getOrderno() {
                return orderno;
            }

            public void setOrderno(String orderno) {
                this.orderno = orderno;
            }

            public String getShareid() {
                return shareid;
            }

            public void setShareid(String shareid) {
                this.shareid = shareid;
            }

            public String getSharetype() {
                return sharetype;
            }

            public void setSharetype(String sharetype) {
                this.sharetype = sharetype;
            }

            public String getIsFailure() {
                return isFailure;
            }

            public void setIsFailure(String isFailure) {
                this.isFailure = isFailure;
            }

            public String getGroupno() {
                return groupno;
            }

            public void setGroupno(String groupno) {
                this.groupno = groupno;
            }

            public String getCategoryName() {
                return categoryName;
            }

            public void setCategoryName(String categoryName) {
                this.categoryName = categoryName;
            }

            public String getCouponName() {
                return couponName;
            }

            public void setCouponName(String couponName) {
                this.couponName = couponName;
            }

            public String getCouponText() {
                return couponText;
            }

            public void setCouponText(String couponText) {
                this.couponText = couponText;
            }

            public String getCategoryText() {
                return categoryText;
            }

            public void setCategoryText(String categoryText) {
                this.categoryText = categoryText;
            }

            public String getBankNo() {
                return bankNo;
            }

            public void setBankNo(String bankNo) {
                this.bankNo = bankNo;
            }

            public String getBankName() {
                return bankName;
            }

            public void setBankName(String bankName) {
                this.bankName = bankName;
            }

            public List<?> getCitys() {
                return citys;
            }

            public void setCitys(List<?> citys) {
                this.citys = citys;
            }
        }
    }
}
