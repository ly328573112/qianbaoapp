package com.haodaibao.fanbeiapp.repository;

import android.net.ParseException;
import android.text.TextUtils;

import com.baseandroid.config.Global;
import com.baseandroid.retrofit.OkHttpClientManager;
import com.baseandroid.utils.LogUtil;
import com.baseandroid.utils.ToastUtils;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParseException;
import com.haodaibao.fanbeiapp.event.LoginInvalideEvent;
import com.haodaibao.fanbeiapp.repository.json.Data;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import retrofit2.HttpException;

import static com.baseandroid.config.Constant.ERROR_LOGOUT;

public class RxObserver<T> implements Observer<T> {

    @Override
    public void onSubscribe(@NonNull Disposable d) {

    }

    @Override
    public void onNext(@NonNull T t) {

    }

    @Override
    public void onError(@NonNull Throwable e) {
        LogUtil.e(e.getMessage());
        String msg = "";
        if (e instanceof ConnectException) {
            msg = "网络不可用";
        } else if (e instanceof UnknownHostException) {
//            msg = "未知主机错误";
            msg = "网络不可用";
        } else if (e instanceof SocketTimeoutException) {
            msg = "请求网络超时";
        } else if (e instanceof HttpException) {
            HttpException httpException = (HttpException) e;
            msg = convertStatusCode(httpException);
        } else if (e instanceof JsonParseException || e instanceof ParseException || e instanceof JSONException || e instanceof JsonIOException) {
            msg = "数据解析错误";
        }

        if (!msg.isEmpty()) {
            ToastUtils.showShortToastSafe(msg);
        }
    }

    @Override
    public void onComplete() {
    }

    private String convertStatusCode(HttpException httpException) {
        String msg;
        if (httpException.code() == 500) {
            msg = "服务器发生错误";
        } else if (httpException.code() == 404) {
            msg = "请求地址不存在";
        } else if (httpException.code() == 403) {
            msg = "请求被服务器拒绝";
        } else if (httpException.code() == 307) {
            msg = "请求被重定向到其他页面";
        } else {
            msg = httpException.message();
        }
        return msg;
    }

    public static <T> boolean checkJsonCode(T t, boolean showToast) {

        if (t == null) {
            return false;
        }

        if (t instanceof Data) {
            if (!TextUtils.isEmpty(OkHttpClientManager.getHttpCookieString()) && ((Data) t)
                    .getStatus()
                    .endsWith(ERROR_LOGOUT)) {
                OkHttpClientManager.clearCookie();
                Global.loginOutclear();
                EventBus.getDefault().post(new LoginInvalideEvent());
                return false;
            } else if (((Data) t).getStatus().startsWith("2")) {
                return true;
            } else {
                if (showToast) {
                    ToastUtils.showShortToast(((Data) t).getMessage());
                }
            }
        }

        return false;
    }
}
