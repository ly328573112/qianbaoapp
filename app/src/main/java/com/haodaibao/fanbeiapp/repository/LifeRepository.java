package com.haodaibao.fanbeiapp.repository;

import com.baseandroid.config.Constant;
import com.baseandroid.config.Global;
import com.baseandroid.retrofit.RetrofitManager;
import com.baseandroid.retrofit.RxRetrofitCache;
import com.google.gson.reflect.TypeToken;
import com.haodaibao.fanbeiapp.repository.json.AdInfoResp;
import com.haodaibao.fanbeiapp.repository.json.BalanceBean;
import com.haodaibao.fanbeiapp.repository.json.BhhStatueBean;
import com.haodaibao.fanbeiapp.repository.json.CollectionsBean;
import com.haodaibao.fanbeiapp.repository.json.CouponBean;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.HelpBean;
import com.haodaibao.fanbeiapp.repository.json.InvoiceBean;
import com.haodaibao.fanbeiapp.repository.json.InvoiceDetailBean;
import com.haodaibao.fanbeiapp.repository.json.InvoiceHeadBean;
import com.haodaibao.fanbeiapp.repository.json.InvoiceRiseDetailBean;
import com.haodaibao.fanbeiapp.repository.json.LabelListResp;
import com.haodaibao.fanbeiapp.repository.json.MakeoutApplyBean;
import com.haodaibao.fanbeiapp.repository.json.MakeoutInvoiceBean;
import com.haodaibao.fanbeiapp.repository.json.MerchantDetailResp;
import com.haodaibao.fanbeiapp.repository.json.MerchantInfoResp;
import com.haodaibao.fanbeiapp.repository.json.MessageListBean;
import com.haodaibao.fanbeiapp.repository.json.MyCouponsResp;
import com.haodaibao.fanbeiapp.repository.json.OpenCitysInfoResp;
import com.haodaibao.fanbeiapp.repository.json.PackageAllBean;
import com.haodaibao.fanbeiapp.repository.json.PayInfoDetailBean;
import com.haodaibao.fanbeiapp.repository.json.PayProcessResp;
import com.haodaibao.fanbeiapp.repository.json.PaylistItemBean;
import com.haodaibao.fanbeiapp.repository.json.PaylistTopBean;
import com.haodaibao.fanbeiapp.repository.json.ProvinCityDistInfoResp;
import com.haodaibao.fanbeiapp.repository.json.ShanghufenleiResp;
import com.haodaibao.fanbeiapp.repository.json.ShangquanResp;
import com.haodaibao.fanbeiapp.repository.json.ShopKindListBean;
import com.haodaibao.fanbeiapp.repository.json.ShopKindsBean;
import com.haodaibao.fanbeiapp.repository.json.SuggestBean;
import com.haodaibao.fanbeiapp.repository.json.SuggestResp;
import com.haodaibao.fanbeiapp.repository.json.YaoheListBean;
import com.haodaibao.fanbeiapp.repository.services.LifeService;
import com.haodaibao.fanbeiapp.repository.services.MerchantService;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

public class LifeRepository {
    private static LifeRepository sLifeRepository = null;
    private static LifeService service;

    private LifeRepository() {
    }

    public static LifeRepository getInstance() {
        synchronized (LifeRepository.class) {
            if (sLifeRepository == null) {
                sLifeRepository = new LifeRepository();
            }
        }
        service = RetrofitManager.getRxRetrofit().create(LifeService.class);
        return sLifeRepository;
    }

    /*收藏列表*/
    public Observable<Data<CollectionsBean>> getCollections(Map<String, String> params) {
        return service.getCollections(params);
    }

    /*获取商户详情*/
    public Observable<Data<MerchantDetailResp>> getMerchantDetails(Map<String, String> fields, boolean forceRefresh) {
        String cacheKey = "getMerchantDetails:" + fields.hashCode();
        MerchantService merchantService = RetrofitManager.getRxRetrofit()
                .create(MerchantService.class);
        Observable<Data<MerchantDetailResp>> fromNetwork = merchantService.getMerchantInfo(fields);
        return RxRetrofitCache.load(cacheKey, Constant.TIME_ONE_HOUR, fromNetwork, forceRefresh, new TypeToken<Data<MerchantDetailResp>>() {
        }.getType());
    }

    /* 首页广告位 */
    public Observable<Data<AdInfoResp>> getAdvertInfos(Map<String, String> params) {
//        final long expireTime = 3 * Constant.TIME_ONE_MINITUE;
//        final String cacheKey = "getAdvertInfos:" + params.hashCode();
//        return RxRetrofitCache.load(cacheKey, expireTime, service.getAdvertInfos(params), false, new TypeToken<Data<AdInfoResp>>() {
//        }.getType());
        return service.getAdvertInfos(params);
    }

    /* 吆喝滚动列表 */
    public Observable<Data<YaoheListBean>> getYaoheListServer(Map<String, String> params) {
        final long expireTime = 5 * Constant.TIME_ONE_MINITUE;
        final String cacheKey = "getYaoheListServer:" + params.hashCode();
        return RxRetrofitCache.load(cacheKey, expireTime, service.getYaoheList(params), false, new TypeToken<Data<YaoheListBean>>() {
        }.getType());
    }

    /* 获取商户分类 */
    public Observable<Data<ShanghufenleiResp>> getClassification(Map<String, String> params) {
        final long expireTime = 3 * Constant.TIME_ONE_HOUR;
        final String cacheKey = "getClassification:" + params.hashCode();
        return RxRetrofitCache.load(cacheKey, expireTime, service.getClassification(params), false, new TypeToken<Data<ShanghufenleiResp>>() {
        }.getType());
    }

    /* 获取商户商圈 */
    public Observable<Data<ShangquanResp>> getShangquan(Map<String, String> params) {
        final long expireTime = 3 * Constant.TIME_ONE_HOUR;
        final String cacheKey = "getShangquan:" + params.hashCode();
        return RxRetrofitCache.load(cacheKey, expireTime, service.getShangquan(params), false, new TypeToken<Data<ShangquanResp>>() {
        }.getType());
    }

    /* 获取开通城市列表 */
    public Observable<Data<OpenCitysInfoResp>> getPubCitysByOpen(Map<String, String> params) {
//        final long expireTime = 2 * Constant.TIME_ONE_HOUR;
//        final String cacheKey = "getPubCitysByOpen:" + params.hashCode();
//        LifeService service = RetrofitManager.getRxRetrofit().create(LifeService.class);
//        return RxRetrofitCache.load(cacheKey, expireTime, service.getPubCitysByOpen(params), false, new TypeToken<Data<OpenCitysInfoResp>>() {
//        }.getType());
        return service.getPubCitysByOpen(params);
    }

    /* 获取省市区市列表 */
    public Observable<Data<ProvinCityDistInfoResp>> getProvinceCityDistrict(Map<String, String> params) {
        final long expireTime = 3 * 24 * Constant.TIME_ONE_HOUR;
        final String cacheKey = "getProvinceCityDistrict:" + params.hashCode();
        LifeService service = RetrofitManager.getRxRetrofit().create(LifeService.class);
        return RxRetrofitCache.load(cacheKey, expireTime, service.getProvinceCityDistrict(params), false, new TypeToken<Data<ProvinCityDistInfoResp>>() {
        }.getType());
    }

    /* 获取头部标签*/
    public Observable<Data<LabelListResp>> getLebelList() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("siteType", Global.getSelectCitySiteType());
        hashMap.put("siteCode", Global.getSelectCityCode());
        return service.getLabelList(hashMap);
    }

    /* 获取附近商户列表 */
    public Observable<Data<MerchantInfoResp>> getShanghuListInfo(Map<String, String> params, boolean forcenetwork) {
//        final long expireTime = Constant.TIME_ONE_HOUR;
//        final String cacheKey = "getShanghuListInfo:" + params.hashCode();
//        return RxRetrofitCache.load(cacheKey, expireTime, service.getShanghuListInfo(params), forcenetwork, new TypeToken<Data<MerchantInfoResp>>() {
//        }.getType());
        return service.getShanghuListInfo(params);
    }

    public Observable<Data<MerchantInfoResp>> getShanghuList(Map<String, String> params) {
        return service.getShanghuListInfo(params);
    }

    //获取优惠券列表
    public Observable<Data<MyCouponsResp>> getCoupons(Map<String, String> params) {
        return service.getCoupons(params);
    }

    public Observable<Data<LabelListResp>> getMerchantActivityList(Map<String, String> params) {
        return service.getMerchantActivityList(params);
    }

    //获取买单记录页面消费及优惠信息
    public Observable<Data<PaylistTopBean>> getPaylistTopData() {
        return service.getPaylistTopData();
    }

    //获取支付列表
    public Observable<Data<PaylistItemBean>> getPaylistRvData(Map<String, String> params) {
        return service.getPaylistRvData(params);
    }

    //获取订单信息
    public Observable<Data<PayInfoDetailBean>> getPayInfoDetail(Map<String, String> params) {
        return service.getPayInfoDetail(params);
    }

    //获取优惠券列表
    public Observable<Data<CouponBean>> getCouponsList(Map<String, String> params) {
        return service.getCouponsList(params);
    }

    //创建订单
    public Observable<Data<PayProcessResp>> createOrder(Map<String, String> params) {
        return service.createOrder(params);
    }

    //关闭订单
    public Observable<Data<Object>> cancelPayment(Map<String, String> params) {
        return service.cancelPayment(params);
    }

    //获取白花花状态
    public Observable<Data<BhhStatueBean>> getBhhStatue(Map<String, String> params) {
        return service.getBhhStatue(params);
    }

    //获取余额
    public Observable<Data<BalanceBean>> getBalance() {
        return service.getBalance();
    }

    //获取消息列表
    public Observable<Data<MessageListBean>> getMessageList(Map<String, String> params) {
        return service.getMessageList(params);
    }

    //设定消息为已读
    public Observable<Data> setMsgReaded(Map<String, String> params) {
        return service.setMsgReaded(params);
    }

    //删除消息
    public Observable<Data> deletMsg(Map<String, String> params) {
        return service.deletMsg(params);
    }

    //提交意见反馈
    public Observable<Data<SuggestBean>> submitSuggest(Map<String, String> params) {
        return service.submitSuggest(params);
    }

    //获取意见列表
    public Observable<Data<SuggestResp>> getSubmitList(Map<String, String> params) {
        return service.getSuggestList(params);
    }

    //获取帮助列表
    public Observable<Data<HelpBean>> getHelpList(Map<String, String> params) {
        return service.getHelpList(params);
    }

    //获取帮助列表
    public Observable<Data<HelpBean>> getBhhHelpList(Map<String, String> params) {
        return service.getBhhHelpList(params);
    }

    //获取套餐详情
    public Observable<Data<PackageAllBean>> getPackageDetail(Map<String, String> params) {
        return service.getPackageDetail(params);
    }

    //套餐购买优惠规则验证
    public Observable<Data<PackageAllBean>> checkMerchantPackageRule(Map<String, String> params) {
        return service.checkMerchantPackageRule(params);
    }

    /* 获取首页地图商户列表 */
    public Observable<Data<MerchantInfoResp>> getMapShanghuListInfo(Map<String, String> params, boolean forcenetwork) {
        final long expireTime = Constant.TIME_ONE_HOUR;
        final String cacheKey = "queryMerchantList:" + params.hashCode();
        return RxRetrofitCache.load(cacheKey, expireTime, service.getMapShanghuListInfo(params), forcenetwork, new TypeToken<Data<MerchantInfoResp>>() {
        }.getType());
    }

    /* 获取首页地图吆喝 */
    public Observable<Data<YaoheListBean>> queryMerchantMsgList(Map<String, String> params, boolean forcenetwork) {
        final long expireTime = Constant.TIME_ONE_HOUR;
        final String cacheKey = "queryMerchantMsgList:" + params.hashCode();
        return RxRetrofitCache.load(cacheKey, expireTime, service.queryMerchantMsgList(params), forcenetwork, new TypeToken<Data<YaoheListBean>>() {
        }.getType());
    }

    /*获取我的发票列表*/
    public Observable<Data<InvoiceBean>> getInvoiceList(Map<String, String> params, boolean forcenetwork) {
        final long expireTime = Constant.TIME_ONE_HOUR;
        final String cacheKey = "InvoiceBean:" + params.hashCode();
        return RxRetrofitCache.load(cacheKey, expireTime, service.getInvoiceList(params), forcenetwork, new TypeToken<Data<InvoiceBean>>() {
        }.getType());
    }

    /*申请发票页面获取申请信息*/
    public Observable<Data<MakeoutApplyBean>> getApplyInfo(Map<String, String> params, boolean forcenetwork) {
        final long expireTime = Constant.TIME_ONE_HOUR;
        final String cacheKey = "MakeoutApplyBean" + params.hashCode();
        return RxRetrofitCache.load(cacheKey, expireTime, service.getApplyInfo(params), forcenetwork, new TypeToken<Data<MakeoutApplyBean>>() {
        }.getType());
    }

    /*开票*/
    public Observable<Data<MakeoutInvoiceBean>> applyInvoice(Map<String, String> params, boolean forcenetwork) {
        final long expireTime = Constant.TIME_ONE_HOUR;
        final String cacheKey = "MakeoutInvoiceBean" + params.hashCode();
        return RxRetrofitCache.load(cacheKey, expireTime, service.applyInvoice(params), forcenetwork, new TypeToken<Data<MakeoutApplyBean>>() {
        }.getType());
    }

    /*我的抬头列表*/
    public Observable<Data<InvoiceHeadBean>> getRise(Map<String, String> params, boolean forcenetwork) {
        final long expireTime = Constant.TIME_ONE_HOUR;
        final String cacheKey = "InvoiceHeadBean" + params.hashCode();
        return RxRetrofitCache.load(cacheKey, expireTime, service.getRise(params), forcenetwork, new TypeToken<Data<InvoiceHeadBean>>() {
        }.getType());
    }

    /*添加修改抬头*/
    public Observable<Data> addOrModify(Map<String, String> params) {
        return RxRetrofitCache.load("", 1, service.addOrModify(params), true, new TypeToken<Data>() {
        }.getType());
    }

    /*我的发票明细*/
    public Observable<Data<InvoiceDetailBean>> getInvoiceDetail(Map<String, String> params) {
        return RxRetrofitCache.load("", 1, service.getInvoiceDetail(params), true, new TypeToken<Data<InvoiceDetailBean>>() {
        }.getType());
    }

    /*我的抬头明细*/
    public Observable<Data<InvoiceRiseDetailBean>> getInvoiceRiseDetail(Map<String, String> params) {
        return RxRetrofitCache.load("", 1, service.getInvoiceRiseDetail(params), true, new TypeToken<Data<InvoiceRiseDetailBean>>() {
        }.getType());
    }

    /**
     * 获取首页商铺类型分类
     * @param parms
     * @return
     */
//    public Observable<Data<ShopKindListBean>> getShopKind(HashMap<String, String> parms) {
//        return RxRetrofitCache.load("", 1, service.getShopKind(parms), true, new TypeToken<Data<ShopKindListBean>>() {
//        }.getType());
//    }
    public Observable<Data<ShopKindListBean>> getShopKind(HashMap<String, String> parms) {
        return RxRetrofitCache.load("", 1, service.getShopKind(parms), true, new TypeToken<Data<ShopKindListBean>>() {
        }.getType());
//        return service.getShopKind(parms);
    }
}

