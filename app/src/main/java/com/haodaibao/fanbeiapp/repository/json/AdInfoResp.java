package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.util.List;

@Keep
public class AdInfoResp {
    private List<AdInfo> adverts;

    public List<AdInfo> getAdverts() {
        return adverts;
    }

    public void setAdverts(List<AdInfo> adverts) {
        this.adverts = adverts;
    }
}
