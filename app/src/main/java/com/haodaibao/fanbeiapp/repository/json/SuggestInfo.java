package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;
import java.util.List;

/**
 * 意见反馈实体类
 */

@Keep
public class SuggestInfo implements Serializable{
	private static final long serialVersionUID = -7312736156671813873L;
	private String id;// 反馈id
	private String type;// 反馈类型
	private String userId;// 用户号
	private String merchantno;// 商户号
	private String scoreno;//
	private String reportlist;//
	private String content;//
	private String createTime;// 反馈时间
	private String dealuserno;// 客服号
	private String dealtime;// 客服处理时间
	private String status;//
	private String msgContent;// 反馈内容
	private List<FeedbackAttachmentFiles> attFlList;// 图片列表


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getMerchantno() {
		return merchantno;
	}

	public void setMerchantno(String merchantno) {
		this.merchantno = merchantno;
	}

	public String getScoreno() {
		return scoreno;
	}

	public void setScoreno(String scoreno) {
		this.scoreno = scoreno;
	}

	public String getReportlist() {
		return reportlist;
	}

	public void setReportlist(String reportlist) {
		this.reportlist = reportlist;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getDealuserno() {
		return dealuserno;
	}

	public void setDealuserno(String dealuserno) {
		this.dealuserno = dealuserno;
	}

	public String getDealtime() {
		return dealtime;
	}

	public void setDealtime(String dealtime) {
		this.dealtime = dealtime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMsgContent() {
		return msgContent;
	}

	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}

	public List<FeedbackAttachmentFiles> getAttFlList() {
		return attFlList;
	}

	public void setAttFlList(List<FeedbackAttachmentFiles> attFlList) {
		this.attFlList = attFlList;
	}


}
