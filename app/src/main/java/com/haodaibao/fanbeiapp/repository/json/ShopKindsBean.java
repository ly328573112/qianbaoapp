package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;
import java.util.List;

@Keep
public class ShopKindsBean implements Serializable {

    /**
     * image : 20160406100538.jpg
     * returnrate : 0.99
     * categoryno : 20160406100538  //一级分类编号
     * name : 火锅
     * categorytype     //品类类型 1一级分类  2二级分类
     * subcategoryno    //二级分类编号
     * merchants : [{"activityno":"","merchantno":"M20180327693925","merchantname":"猪蹄老街坊","groupno":"","categoryno":"20150429100068","subcategoryno":"20160406100538","subcategoryname":"火锅","provincecode":"","citycode":"","districtcode":"","areacode":"","address":"","longitude":"121.38631","latitude":"31.164145","phone":"","mobile":"","receivemobile":"","opentime":"","opentimeext":"","personaverage":"","cashaccountno":"","couponaccountno":"","refundmode":"","offlinerefundmode":"","ifrefundperiod":"","discount":"","discount2":"","refundperiodsource":"","usertype":"","returnrate":"0.66","returnrate2":"","returnbeginamount":"","returnbegin2amount":"","topamount":"","prepaymode":"","prepaybalance":"","prepaylimit":"","prepaywarningbalance":"","posmerchantno":"","posterminalno":"","settlemode":"","saleperson":"","signtime":"","organizationno":"","createtime":"","status":"","qbmemberno":"","merfee":"","posmerfee":"","accountNo":"","accountName":"","bankNo":"","distance":0,"star":"","recommend":"","categoryName":"美食","imageurl":"","productnum":"","vouchernum":"","attention":"","productList":[],"recordCount":"","story":"","notice":"","promotion":"","refundnote":"","areaname":"","giftList":"","album":"","merchantImageList":[],"labels":[],"prieds":[],"packageList":[],"bhhenable":"","facepay":""},{"activityno":"","merchantno":"M20180322693878","merchantname":"罗曼蒂克烧腊","groupno":"","categoryno":"20150429100068","subcategoryno":"20160406100538","subcategoryname":"火锅","provincecode":"","citycode":"","districtcode":"","areacode":"","address":"","longitude":"121.38631","latitude":"31.164145","phone":"","mobile":"","receivemobile":"","opentime":"","opentimeext":"","personaverage":"","cashaccountno":"","couponaccountno":"","refundmode":"","offlinerefundmode":"","ifrefundperiod":"","discount":"","discount2":"","refundperiodsource":"","usertype":"","returnrate":"0.0","returnrate2":"","returnbeginamount":"","returnbegin2amount":"","topamount":"","prepaymode":"","prepaybalance":"","prepaylimit":"","prepaywarningbalance":"","posmerchantno":"","posterminalno":"","settlemode":"","saleperson":"","signtime":"","organizationno":"","createtime":"","status":"","qbmemberno":"","merfee":"","posmerfee":"","accountNo":"","accountName":"","bankNo":"","distance":0,"star":"","recommend":"","categoryName":"美食","imageurl":"","productnum":"","vouchernum":"","attention":"","productList":[],"recordCount":"","story":"","notice":"","promotion":"","refundnote":"","areaname":"","giftList":"","album":"","merchantImageList":[],"labels":[],"prieds":[],"packageList":[],"bhhenable":"","facepay":""},{"activityno":"","merchantno":"M20161118116579","merchantname":"七婆串串香火锅(宜山路店)","groupno":"","categoryno":"20150429100068","subcategoryno":"20160406100538","subcategoryname":"火锅","provincecode":"","citycode":"","districtcode":"","areacode":"","address":"","longitude":"121.38552","latitude":"31.16697","phone":"","mobile":"","receivemobile":"","opentime":"","opentimeext":"","personaverage":"","cashaccountno":"","couponaccountno":"","refundmode":"","offlinerefundmode":"","ifrefundperiod":"","discount":"","discount2":"","refundperiodsource":"","usertype":"","returnrate":"0.0","returnrate2":"","returnbeginamount":"","returnbegin2amount":"","topamount":"","prepaymode":"","prepaybalance":"","prepaylimit":"","prepaywarningbalance":"","posmerchantno":"","posterminalno":"","settlemode":"","saleperson":"","signtime":"","organizationno":"","createtime":"","status":"","qbmemberno":"","merfee":"","posmerfee":"","accountNo":"","accountName":"","bankNo":"","distance":0,"star":"","recommend":"","categoryName":"美食","imageurl":"","productnum":"","vouchernum":"","attention":"","productList":[],"recordCount":"","story":"","notice":"","promotion":"","refundnote":"","areaname":"","giftList":"","album":"","merchantImageList":[],"labels":[],"prieds":[],"packageList":[],"bhhenable":"","facepay":""}]
     */

    private String image;
    private String returnrate;
    private String categoryno;
    private String name;
    private String categorytype ;
    private String subcategoryno ;
    public String getCategorytype() {
        return categorytype;
    }

    public void setCategorytype(String categorytype) {
        this.categorytype = categorytype;
    }

    private List<MerchantsBean> merchants;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getReturnrate() {
        return returnrate;
    }

    public void setReturnrate(String returnrate) {
        this.returnrate = returnrate;
    }

    public String getCategoryno() {
        return categoryno;
    }

    public void setCategoryno(String categoryno) {
        this.categoryno = categoryno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubcategoryno() {
        return subcategoryno;
    }

    public void setSubcategoryno(String subcategoryno) {
        this.subcategoryno = subcategoryno;
    }

    public List<MerchantsBean> getMerchants() {
        return merchants;
    }

    public void setMerchants(List<MerchantsBean> merchants) {
        this.merchants = merchants;
    }

    @Override
    public String toString() {
        return "ShopKindsBean{" +
                "image='" + image + '\'' +
                ", returnrate='" + returnrate + '\'' +
                ", categoryno='" + categoryno + '\'' +
                ", name='" + name + '\'' +
                ", categorytype='" + categorytype + '\'' +
                ", subcategoryno='" + subcategoryno + '\'' +
                ", merchants=" + merchants +
                '}';
    }
}
