package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;

/**
 * 开发者：LuoYi
 * Time: 2017 21:35 2017/9/11 09
 */

@Keep
public class PriedListBean implements Serializable {

    private static final long serialVersionUID = 6730950739328560583L;
    public String startStatus;
    public String refundContent; // 优惠说明
    public String dateStr; // 时间
    public String isShow;// 是否高亮显示 1、高亮 2、不高亮
    public String adaptType;// 1、仅新用户 2、所有用户
    public String activityno;
    public String discountContent;

    public PriedListBean(String startStatus, String refundContent, String dateStr, String isShow, String adaptType, String activityno) {
        this.startStatus = startStatus;
        this.refundContent = refundContent;
        this.dateStr = dateStr;
        this.isShow = isShow;
        this.adaptType = adaptType;
        this.activityno = activityno;
    }

    @Override
    public String toString() {
        return "PriedListBean{" +
                "startStatus='" + startStatus + '\'' +
                ", refundContent='" + refundContent + '\'' +
                ", dateStr='" + dateStr + '\'' +
                ", isShow='" + isShow + '\'' +
                ", adaptType='" + adaptType + '\'' +
                ", activityno='" + activityno + '\'' +
                '}';
    }
}
