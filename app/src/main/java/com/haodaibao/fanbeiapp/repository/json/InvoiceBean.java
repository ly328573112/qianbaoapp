package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;
import java.util.List;

/**
 * date: 2018/2/5.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */
@Keep
public class InvoiceBean implements Serializable {
    private static final long serialVersionUID = -7233661464083430184L;
    public InvoiceListBean list;
    public InvoiceRise rise;

    @Keep
    public class InvoiceListBean implements Serializable {
        private static final long serialVersionUID = 1198594331276365524L;
        public List<InvoiceItem> items;
    }

    @Keep
    public class InvoiceItem implements Serializable {

        private static final long serialVersionUID = 6619970456713446291L;
        public String order_no;
        public String invoice_type_code;
        public String merchant_no;
        public String create_time;
        public String rise_name;
        public String mobile;
        public String rise_type_code;
        public String invoice_project;
        public String invoice_amount;
        public String user_no;
        public String is_deleted;
        public String tax_no;
        public String trade_no;
        public String invoice_date;
        public String id;
        public String email;
        public String status;
        public String merchant_register_name;
        public String invoice_id;

        public void setData(InvoiceItem invoiceItem) {
            order_no = invoiceItem.order_no;
            invoice_type_code = invoiceItem.invoice_type_code;
            merchant_no = invoiceItem.merchant_no;
            create_time = invoiceItem.create_time;
            rise_name = invoiceItem.rise_name;
            mobile = invoiceItem.mobile;
            rise_type_code = invoiceItem.rise_type_code;
            invoice_project = invoiceItem.invoice_project;
            invoice_amount = invoiceItem.invoice_amount;
            user_no = invoiceItem.user_no;
            is_deleted = invoiceItem.is_deleted;
            tax_no = invoiceItem.tax_no;
            trade_no = invoiceItem.trade_no;
            id = invoiceItem.id;
            email = invoiceItem.email;
            status = invoiceItem.status;
            merchant_register_name = invoiceItem.merchant_register_name;
            invoice_id = invoiceItem.invoice_id;
        }
    }

    @Keep
    public class InvoiceRise implements Serializable {

        private static final long serialVersionUID = -231130890786883542L;
        public String rise_no;
        public String company_bank_name;
        public String create_time;
        public String rise_id;
        public String rise_name;
        public String mobile;
        public String rise_type_code;
        public String company_bank_no;
        public String is_default;
        public String company_address;
        public String company_mobile;
        public String user_no;
        public String is_deleted;
        public String tax_no;
        public String id;
        public String email;
    }
}
