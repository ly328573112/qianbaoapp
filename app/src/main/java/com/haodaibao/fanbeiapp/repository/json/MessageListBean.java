package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.util.List;

/**
 * Created by Routee on 2017/9/21.
 * description: ${cusor}
 */
@Keep
public class MessageListBean {

    /**
     * result : {"limit":"20","slider":["1"],"lastPage":true,"hasNextPage":false,"nextPage":"1","hasPrePage":false,"endRow":"1","prePage":"1","totalCount":"1","page":"1","items":[{"messageno":"201708311000053268","fromuser":"9999","createtime":"20170831202419","tousertype":"1","userchannel":"11","touser":"U1201704181000129558","type":"01","title":"您有一笔订单已经支付成功啦","contenttype":"03","content":"order_detail?orderNo=ORD2017083110000000398939","effectivetime":"20170831202419","sendstarttime":"20170831202420","sendendtime":"20170831202420","sendchannel":"110","status":"2","readstatus":"0","toUserList":""}],"startRow":"1","firstPage":true,"offset":"0","totalPages":"1"}
     */

    private ResultBean result;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    @Keep
    public static class ResultBean {
        /**
         * limit : 20
         * slider : ["1"]
         * lastPage : true
         * hasNextPage : false
         * nextPage : 1
         * hasPrePage : false
         * endRow : 1
         * prePage : 1
         * totalCount : 1
         * page : 1
         * items : [{"messageno":"201708311000053268","fromuser":"9999","createtime":"20170831202419","tousertype":"1","userchannel":"11","touser":"U1201704181000129558","type":"01","title":"您有一笔订单已经支付成功啦","contenttype":"03","content":"order_detail?orderNo=ORD2017083110000000398939","effectivetime":"20170831202419","sendstarttime":"20170831202420","sendendtime":"20170831202420","sendchannel":"110","status":"2","readstatus":"0","toUserList":""}]
         * startRow : 1
         * firstPage : true
         * offset : 0
         * totalPages : 1
         */

        private String limit;
        private boolean lastPage;
        private boolean hasNextPage;
        private String nextPage;
        private boolean hasPrePage;
        private String endRow;
        private String prePage;
        private String totalCount;
        private String page;
        private String startRow;
        private boolean firstPage;
        private String offset;
        private String totalPages;
        private List<String> slider;
        private List<ItemsBean> items;

        public String getLimit() {
            return limit;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

        public boolean isLastPage() {
            return lastPage;
        }

        public void setLastPage(boolean lastPage) {
            this.lastPage = lastPage;
        }

        public boolean isHasNextPage() {
            return hasNextPage;
        }

        public void setHasNextPage(boolean hasNextPage) {
            this.hasNextPage = hasNextPage;
        }

        public String getNextPage() {
            return nextPage;
        }

        public void setNextPage(String nextPage) {
            this.nextPage = nextPage;
        }

        public boolean isHasPrePage() {
            return hasPrePage;
        }

        public void setHasPrePage(boolean hasPrePage) {
            this.hasPrePage = hasPrePage;
        }

        public String getEndRow() {
            return endRow;
        }

        public void setEndRow(String endRow) {
            this.endRow = endRow;
        }

        public String getPrePage() {
            return prePage;
        }

        public void setPrePage(String prePage) {
            this.prePage = prePage;
        }

        public String getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(String totalCount) {
            this.totalCount = totalCount;
        }

        public String getPage() {
            return page;
        }

        public void setPage(String page) {
            this.page = page;
        }

        public String getStartRow() {
            return startRow;
        }

        public void setStartRow(String startRow) {
            this.startRow = startRow;
        }

        public boolean isFirstPage() {
            return firstPage;
        }

        public void setFirstPage(boolean firstPage) {
            this.firstPage = firstPage;
        }

        public String getOffset() {
            return offset;
        }

        public void setOffset(String offset) {
            this.offset = offset;
        }

        public String getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(String totalPages) {
            this.totalPages = totalPages;
        }

        public List<String> getSlider() {
            return slider;
        }

        public void setSlider(List<String> slider) {
            this.slider = slider;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        @Keep
        public static class ItemsBean {
            /**
             * messageno : 201708311000053268
             * fromuser : 9999
             * createtime : 20170831202419
             * tousertype : 1
             * userchannel : 11
             * touser : U1201704181000129558
             * type : 01
             * title : 您有一笔订单已经支付成功啦
             * contenttype : 03
             * content : order_detail?orderNo=ORD2017083110000000398939
             * effectivetime : 20170831202419
             * sendstarttime : 20170831202420
             * sendendtime : 20170831202420
             * sendchannel : 110
             * status : 2
             * readstatus : 0
             * toUserList :
             */

            private String messageno;// 消息编号
            private String fromuser;// 发送者
            private String touser;// 接收者
            private String type;// 消息类型, ALL：所有类型，01:提醒消息，02:优惠消息，03:系统消息
            private String contenttype;// 消息内容类型 01：文本 02：地址URL 03：APP界面
            private String title;// 消息标题
            private String content;// 消息内容
            private String effectivetime;// 生效时间
            private String userchannel;// 接收者渠道
            private String tousertype;// 接收者类型
            private String channellist;//
            private String status;//
            private String readstatus;// 消息状态 0：未读取,1：已读取,9：已删除
            private String userno;// 用户编号
            private String createtime;
            private String sendstarttime;
            private String sendendtime;
            private String sendchannel;
            private String toUserList;
            private boolean checked;

            public String getMessageno() {
                return messageno;
            }

            public void setMessageno(String messageno) {
                this.messageno = messageno;
            }

            public String getFromuser() {
                return fromuser;
            }

            public void setFromuser(String fromuser) {
                this.fromuser = fromuser;
            }

            public String getTouser() {
                return touser;
            }

            public void setTouser(String touser) {
                this.touser = touser;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getContenttype() {
                return contenttype;
            }

            public void setContenttype(String contenttype) {
                this.contenttype = contenttype;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getEffectivetime() {
                return effectivetime;
            }

            public void setEffectivetime(String effectivetime) {
                this.effectivetime = effectivetime;
            }

            public String getUserchannel() {
                return userchannel;
            }

            public void setUserchannel(String userchannel) {
                this.userchannel = userchannel;
            }

            public String getTousertype() {
                return tousertype;
            }

            public void setTousertype(String tousertype) {
                this.tousertype = tousertype;
            }

            public String getChannellist() {
                return channellist;
            }

            public void setChannellist(String channellist) {
                this.channellist = channellist;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getReadstatus() {
                return readstatus;
            }

            public void setReadstatus(String readstatus) {
                this.readstatus = readstatus;
            }

            public String getUserno() {
                return userno;
            }

            public void setUserno(String userno) {
                this.userno = userno;
            }

            public String getCreatetime() {
                return createtime;
            }

            public void setCreatetime(String createtime) {
                this.createtime = createtime;
            }

            public String getSendstarttime() {
                return sendstarttime;
            }

            public void setSendstarttime(String sendstarttime) {
                this.sendstarttime = sendstarttime;
            }

            public String getSendendtime() {
                return sendendtime;
            }

            public void setSendendtime(String sendendtime) {
                this.sendendtime = sendendtime;
            }

            public String getSendchannel() {
                return sendchannel;
            }

            public void setSendchannel(String sendchannel) {
                this.sendchannel = sendchannel;
            }

            public String getToUserList() {
                return toUserList;
            }

            public void setToUserList(String toUserList) {
                this.toUserList = toUserList;
            }

            public boolean isChecked() {
                return checked;
            }

            public void setChecked(boolean checked) {
                this.checked = checked;
            }
        }
    }
}
