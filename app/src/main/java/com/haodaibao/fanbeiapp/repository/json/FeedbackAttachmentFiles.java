package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;

@Keep
public class FeedbackAttachmentFiles implements Serializable{
	private static final long serialVersionUID = -6572377610609892311L;
	private String fileNm;
	private String reportno;

	public String getFileNm() {
		return fileNm;
	}

	public void setFileNm(String fileNm) {
		this.fileNm = fileNm;
	}

	public String getReportno() {
		return reportno;
	}

	public void setReportno(String reportno) {
		this.reportno = reportno;
	}


}
