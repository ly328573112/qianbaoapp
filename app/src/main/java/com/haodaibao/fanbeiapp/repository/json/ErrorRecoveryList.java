package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.util.List;

/**
 * 开发者：LuoYi
 * Time: 2017 16:58 2017/11/28 11
 */
@Keep
public class ErrorRecoveryList {

    public List<ErrorResult> result;

    @Keep
    public static class ErrorResult {
        public String code;
        public String name;
        public String desc;

        @Override
        public String toString() {
            return "ErrorRecoveryList{" +
                    "code='" + code + '\'' +
                    ", name='" + name + '\'' +
                    ", desc='" + desc + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "ErrorRecoveryList{" +
                "result=" + result +
                '}';
    }
}
