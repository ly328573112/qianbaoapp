package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Index;

import java.io.Serializable;
import org.greenrobot.greendao.annotation.Generated;

@Keep
@Entity
public class DistrictsBean implements Serializable {
    private static final long serialVersionUID = 8971422997550000021L;

    @Index(unique = true)
    private String code;
    private String name;
    private String citycode;
    private String areaList;
    private String cityName;

    @Generated(hash = 1479276759)
    public DistrictsBean(String code, String name, String citycode, String areaList,
            String cityName) {
        this.code = code;
        this.name = name;
        this.citycode = citycode;
        this.areaList = areaList;
        this.cityName = cityName;
    }

    @Generated(hash = 1089062420)
    public DistrictsBean() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCitycode() {
        return citycode;
    }

    public void setCitycode(String citycode) {
        this.citycode = citycode;
    }

    public String getAreaList() {
        return areaList;
    }

    public void setAreaList(String areaList) {
        this.areaList = areaList;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
