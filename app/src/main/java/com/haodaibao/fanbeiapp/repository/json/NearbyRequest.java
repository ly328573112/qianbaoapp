package com.haodaibao.fanbeiapp.repository.json;

public class NearbyRequest {
    private int nearbyType = 0;
    private String keyword = ""; // 商户名称
    private String categoryno = "";// 分类编号
    private String districtcode = "";// 区域
    private String areacode = "";// 商圈
    private String sort = "";// 排序条件
    private String refundmode = "";// 1返现商户
    private String isProduct = "";// 10优惠套餐
    private String isVoucher = "";// 1 有优惠券
    private String showText = "";
    private String showText1 = "";
    private String showText2 = "";

    public void cleanNearby() {
        nearbyType = 0;
        keyword = ""; // 商户名称
        categoryno = "";// 分类编号
        districtcode = "";// 区域
        areacode = "";// 商圈
        sort = "";// 排序条件
        refundmode = "";// 1返现商户
        isProduct = "";// 10优惠套餐
        isVoucher = "";// 1 有优惠券
        showText = "";
        showText1 = "";
        showText2 = "";
    }

    public NearbyRequest() {
        super();
    }

    public int getNearbyType() {
        return nearbyType;
    }

    public void setNearbyType(int nearbyType) {
        this.nearbyType = nearbyType;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getCategoryno() {
        return categoryno;
    }

    public void setCategoryno(String categoryno) {
        this.categoryno = categoryno;
    }

    public String getDistrictcode() {
        return districtcode;
    }

    public void setDistrictcode(String districtcode) {
        this.districtcode = districtcode;
    }

    public String getAreacode() {
        return areacode;
    }

    public void setAreacode(String areacode) {
        this.areacode = areacode;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getRefundmode() {
        return refundmode;
    }

    public void setRefundmode(String refundmode) {
        this.refundmode = refundmode;
    }

    public String getIsProduct() {
        return isProduct;
    }

    public void setIsProduct(String isProduct) {
        this.isProduct = isProduct;
    }

    public String getIsVoucher() {
        return isVoucher;
    }

    public void setIsVoucher(String isVoucher) {
        this.isVoucher = isVoucher;
    }

    public String getShowText() {
        return showText;
    }

    public void setShowText(String showText) {
        this.showText = showText;
    }

    public String getShowText1() {
        return showText1;
    }

    public void setShowText1(String showText1) {
        this.showText1 = showText1;
    }

    public String getShowText2() {
        return showText2;
    }

    public void setShowText2(String showText2) {
        this.showText2 = showText2;
    }

    @Override
    public String toString() {
        return "NearbyRequest{" +
                "nearbyType=" + nearbyType +
                ", keyword='" + keyword + '\'' +
                ", categoryno='" + categoryno + '\'' +
                ", districtcode='" + districtcode + '\'' +
                ", areacode='" + areacode + '\'' +
                ", sort='" + sort + '\'' +
                ", refundmode='" + refundmode + '\'' +
                ", isProduct='" + isProduct + '\'' +
                ", isVoucher='" + isVoucher + '\'' +
                ", showText='" + showText + '\'' +
                ", showText1='" + showText1 + '\'' +
                ", showText2='" + showText2 + '\'' +
                '}';
    }
}