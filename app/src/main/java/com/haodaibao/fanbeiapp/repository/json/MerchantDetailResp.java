package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;
import java.util.List;

/**
 * 商户详情响应实体
 */
@Keep
public class MerchantDetailResp implements Serializable {
    private static final long serialVersionUID = -6806087194996304001L;
    private String imageServerUrl;// 图片服务地址
    private String merchantBranch;// 分支个数
    private String attention;// false未关注true专注
    private MerchantInfoEntiy merchant;
    private List<ProductDetailInfo> productList;// 套餐列表
    private List<PeriodsInfo> periods;// 分时段优惠
    //	private List<VoucherInfo> voucherList;// 优惠券列表
    private List<PriedListBean> priedList;
    private List<PackageListBean> packageList;

    public MerchantDetailResp() {
        super();
    }

    public String getImageServerUrl() {
        return imageServerUrl;
    }

    public void setImageServerUrl(String imageServerUrl) {
        this.imageServerUrl = imageServerUrl;
    }

    public MerchantInfoEntiy getMerchant() {
        return merchant;
    }

    public void setMerchant(MerchantInfoEntiy merchant) {
        this.merchant = merchant;
    }

    public List<ProductDetailInfo> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductDetailInfo> productList) {
        this.productList = productList;
    }

    public String getMerchantBranch() {
        return merchantBranch;
    }

    public void setMerchantBranch(String merchantBranch) {
        this.merchantBranch = merchantBranch;
    }

    public String getAttention() {
        return attention;
    }

    public void setAttention(String attention) {
        this.attention = attention;
    }

    public List<PeriodsInfo> getPeriods() {
        return periods;
    }

    public void setPeriods(List<PeriodsInfo> periods) {
        this.periods = periods;
    }

    public List<PriedListBean> getPriedList() {
        return priedList;
    }

    public void setPriedList(List<PriedListBean> priedList) {
        this.priedList = priedList;
    }

    public List<PackageListBean> getPackageLists() {
        return packageList;
    }
}
