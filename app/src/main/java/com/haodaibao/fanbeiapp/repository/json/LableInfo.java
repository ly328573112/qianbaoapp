package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;

/**
 * Created by zhenglei on 2017/2/22.
 * 商户活动信息
 */
@Keep
public class LableInfo implements Serializable {
    private static final long serialVersionUID = 1548726855338998059L;
    private String labelno;//编号
    private String labelname;//名称
    private String labelremark;//
    private String labeltype;//
    private String showlabel;//
    private String labelicon;//
    private String iflimit;//
    private String limitnum;//
    private String selltimetype;//
    private String status;//
    private String itemlist;//

    private String startdate;//
    private String enddate;//
    private String starttime;//
    private String endtime;//
    private String createtime;//
    private String itemno;//
    private String activityNo;//

    private boolean labelSelected = false;

    public String benefitAmount;
    public String userType;
    public String firstOrder;
    public String isEnabled;
    public String reason;
    public boolean isLongTerm;

    public String getLabelno() {
        return labelno;
    }

    public void setLabelno(String labelno) {
        this.labelno = labelno;
    }

    public String getLabelname() {
        return labelname;
    }

    public void setLabelname(String labelname) {
        this.labelname = labelname;
    }

    public String getLabelremark() {
        return labelremark;
    }

    public void setLabelremark(String labelremark) {
        this.labelremark = labelremark;
    }

    public String getLabeltype() {
        return labeltype;
    }

    public void setLabeltype(String labeltype) {
        this.labeltype = labeltype;
    }

    public String getShowlabel() {
        return showlabel;
    }

    public void setShowlabel(String showlabel) {
        this.showlabel = showlabel;
    }

    public String getLabelicon() {
        return labelicon;
    }

    public void setLabelicon(String labelicon) {
        this.labelicon = labelicon;
    }

    public String getIflimit() {
        return iflimit;
    }

    public void setIflimit(String iflimit) {
        this.iflimit = iflimit;
    }

    public String getLimitnum() {
        return limitnum;
    }

    public void setLimitnum(String limitnum) {
        this.limitnum = limitnum;
    }

    public String getSelltimetype() {
        return selltimetype;
    }

    public void setSelltimetype(String selltimetype) {
        this.selltimetype = selltimetype;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getItemlist() {
        return itemlist;
    }

    public void setItemlist(String itemlist) {
        this.itemlist = itemlist;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getItemno() {
        return itemno;
    }

    public void setItemno(String itemno) {
        this.itemno = itemno;
    }

    public String getActivityNo() {
        return activityNo;
    }

    public void setActivityNo(String activityNo) {
        this.activityNo = activityNo;
    }

    public boolean isLabelSelected() {
        return labelSelected;
    }

    public void setLabelSelected(boolean labelSelected) {
        this.labelSelected = labelSelected;
    }

    public String getBenefitAmount() {
        return benefitAmount;
    }

    public void setBenefitAmount(String benefitAmount) {
        this.benefitAmount = benefitAmount;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getFirstOrder() {
        return firstOrder;
    }

    public void setFirstOrder(String firstOrder) {
        this.firstOrder = firstOrder;
    }

    public String getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(String isEnabled) {
        this.isEnabled = isEnabled;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public boolean isLongTerm() {
        return isLongTerm;
    }

    public void setLongTerm(boolean longTerm) {
        isLongTerm = longTerm;
    }

    @Override
    public String toString() {
        return "LableInfo{" +
                "labelno='" + labelno + '\'' +
                ", labelname='" + labelname + '\'' +
                ", labelremark='" + labelremark + '\'' +
                ", labeltype='" + labeltype + '\'' +
                ", showlabel='" + showlabel + '\'' +
                ", labelicon='" + labelicon + '\'' +
                ", iflimit='" + iflimit + '\'' +
                ", limitnum='" + limitnum + '\'' +
                ", selltimetype='" + selltimetype + '\'' +
                ", status='" + status + '\'' +
                ", itemlist='" + itemlist + '\'' +
                ", startdate='" + startdate + '\'' +
                ", enddate='" + enddate + '\'' +
                ", starttime='" + starttime + '\'' +
                ", endtime='" + endtime + '\'' +
                ", createtime='" + createtime + '\'' +
                ", itemno='" + itemno + '\'' +
                ", activityNo='" + activityNo + '\'' +
                ", labelSelected=" + labelSelected +
                ", benefitAmount='" + benefitAmount + '\'' +
                ", userType='" + userType + '\'' +
                ", firstOrder='" + firstOrder + '\'' +
                ", isEnabled='" + isEnabled + '\'' +
                ", reason='" + reason + '\'' +
                ", isLongTerm=" + isLongTerm +
                '}';
    }
}
