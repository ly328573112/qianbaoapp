package com.haodaibao.fanbeiapp.repository.json;

import java.io.Serializable;
import java.util.List;

/**
 * date: 2018/2/6.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class InvoiceHeadBean implements Serializable {
    private static final long serialVersionUID = 4199968361672153850L;
    public InvoiceHeadList list;

    public class InvoiceHeadList implements Serializable {

        private static final long serialVersionUID = -746543763319613818L;
        public List<InvoiceHeadItem> items;

    }

    public class InvoiceHeadItem implements Serializable {
        private static final long serialVersionUID = -6294635721116921031L;
        public boolean checked;
        public String rise_no;
        public String company_bank_name;
        public String create_time;
        public String rise_id;
        public String modify_time;
        public String rise_name;
        public String mobile;
        public String rise_type_code;
        public String company_bank_no;
        public String is_default;
        public String company_address;
        public String company_mobile;
        public String user_no;
        public String is_deleted;
        public String tax_no;
        public String id;
        public String email;

        public void setData(InvoiceHeadItem invoiceHeadItem) {
            rise_no = invoiceHeadItem.rise_no;
            company_bank_name = invoiceHeadItem.company_bank_name;
            create_time = invoiceHeadItem.create_time;
            rise_id = invoiceHeadItem.rise_id;
            modify_time = invoiceHeadItem.modify_time;
            rise_name = invoiceHeadItem.rise_name;
            mobile = invoiceHeadItem.mobile;
            rise_type_code = invoiceHeadItem.rise_type_code;
            company_bank_no = invoiceHeadItem.company_bank_no;
            is_default = invoiceHeadItem.is_default;
            company_address = invoiceHeadItem.company_address;
            company_mobile = invoiceHeadItem.company_mobile;
            user_no = invoiceHeadItem.user_no;
            is_deleted = invoiceHeadItem.is_deleted;
            tax_no = invoiceHeadItem.tax_no;
            id = invoiceHeadItem.id;
            email = invoiceHeadItem.email;
        }
    }
}
