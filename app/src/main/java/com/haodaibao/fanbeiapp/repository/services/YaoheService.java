package com.haodaibao.fanbeiapp.repository.services;


import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.YaoheDetailsBean;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface YaoheService {
    //数据处理移到子线程
    @GET("v1/trade/message/merMsgDetail")
    Observable<Data<YaoheDetailsBean>> getYaoheDetails1(@QueryMap Map<String, String> fields);
}