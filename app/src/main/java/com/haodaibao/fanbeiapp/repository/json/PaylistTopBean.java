package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

/**
 * Created by Routee on 2017/9/7.
 * description: ${cusor}
 */
@Keep
public class PaylistTopBean {

    /**
     * payAmt : 0.00
     * discountAmt : 0.00
     */

    private String payAmt;
    private String discountAmt;

    public String getPayAmt() {
        return payAmt;
    }

    public void setPayAmt(String payAmt) {
        this.payAmt = payAmt;
    }

    public String getDiscountAmt() {
        return discountAmt;
    }

    public void setDiscountAmt(String discountAmt) {
        this.discountAmt = discountAmt;
    }
}
