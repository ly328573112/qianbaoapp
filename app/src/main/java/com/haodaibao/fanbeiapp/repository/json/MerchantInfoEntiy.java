package com.haodaibao.fanbeiapp.repository.json;


import android.support.annotation.Keep;

import com.haodaibao.fanbeiapp.module.merchant.temporarydata.MerchantTemporary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Entity mapped to table MERCHANT_INFO_ENTIY.
 */
@Keep
public class MerchantInfoEntiy extends JingPinBean implements Serializable, MerchantTemporary {

    private static final long serialVersionUID = 4231578760999756787L;
    /* 商户号 */
    private String merchantno = "";
    /* 活动编号*/
    private String activityno = "";
    /* 商户存入DB入口类型*/
    private String merchantDBType = "";
    /* */
    private String subcategoryname = "";
    /* 城市 Code*/
    private String citycode = "";
    /* 商户是否下架 */
    private String status = "";
    /* 商户地址*/
    private String address = "";
    /* 经度*/
    private String longitude = "";
    /* 纬度*/
    private String latitude = "";
    /* 距离*/
    private String distance = "";
    /* 分类名称*/
    private String categoryName = "";
    /* 商圈名称*/
    private String areaname = "";
    /* 图片地址*/
    private String imageurl = "";
    /* 人均消费*/
    private String personaverage = "";
    /* 商户名称*/
    private String merchantname = "";
    /* 评论数量*/
    private String comentCount = "";
    /* 星级*/
    private String star = "";
    /* 关注度*/
    private String attention = "";
    /* 是否返现（优惠模式0：非返现(default)1：全额按比例减2：满额减3：每满额减）*/
    private String refundmode = "";
    /* 全额返时用户返现比率%,满额和每满额返时用户返现金额*/
    private String returnrate = "";
    /* 结束返现起点的用户返现比率(当有两个返现起点金额)*/
    private String returnrate2 = "";
    /* 全额返时返现起始消费金额,满额和每满额返时满额金额*/
    private String returnbeginamount = "";
    /* 返现起点结束金额(当有两个返现起点金额)*/
    private String returnbegin2amount = "";
    /* 座机号*/
    private String phone = "";
    /* 商品数量*/
    private String productnum = "";
    /* 优惠券数量*/
    private String vouchernum = "";
    /* 营业时间*/
    private String opentime = "";
    /* 评论数量*/
    private String recordCount = "";
    /* 集团商户号*/
    private String groupno = "";
    /* */
    private String subcategoryno = "";
    /* */
    private String painame = "";
    /* 最高金额*/
    private String topamount = "";
    /* 营业时间扩展*/
    private String opentimeext = "";
    /* 商户推荐*/
    private String recommend = "";
    /* 优惠活动*/
    private String promotion = "";
    /* 返现说明*/
    private String refundnote = "";
    /* 门店故事*/
    private String story = "";
    /* 公告*/
    private String notice = "";
    /* 轮播图列表*/
    private List<MerchantImageInfo> merchantImageList;
    /* 优惠活动列表*/
    private List<LableInfo> labels;
    /* 商户相册个数*/
    private String album = "";
    /*  是否开通白花花  1:开通 0:未开通*/
    private String bhhenable;

    private List<PriedsBean> prieds = new ArrayList<>();

    public List<PackageListBean> packageList;
    public List<PriedListBean> priedList;
    public String categoryno;

    /**
     * Not-null value.
     */
    public String getMerchantno() {
        return merchantno;
    }

    /**
     * Not-null value; ensure this value is available before it is saved to the database.
     */
    public void setMerchantno(String merchantno) {
        this.merchantno = merchantno;
    }

    public String getMerchantDBType() {
        return merchantDBType;
    }

    public void setMerchantDBType(String merchantDBType) {
        this.merchantDBType = merchantDBType;
    }

    public String getSubcategoryname() {
        return subcategoryname;
    }

    public void setSubcategoryname(String subcategoryname) {
        this.subcategoryname = subcategoryname;
    }

    public String getCityCode() {
        return citycode;
    }

    public void setCityCode(String cityCode) {
        this.citycode = cityCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getAreaname() {
        return areaname;
    }

    public void setAreaname(String areaname) {
        this.areaname = areaname;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getPersonaverage() {
        return personaverage;
    }

    public void setPersonaverage(String personaverage) {
        this.personaverage = personaverage;
    }

    public String getMerchantname() {
        return merchantname;
    }

    public void setMerchantname(String merchantname) {
        this.merchantname = merchantname;
    }

    public String getComentCount() {
        return comentCount;
    }

    public void setComentCount(String comentCount) {
        this.comentCount = comentCount;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getAttention() {
        return attention;
    }

    public void setAttention(String attention) {
        this.attention = attention;
    }

    public String getRefundmode() {
        return refundmode;
    }

    public void setRefundmode(String refundmode) {
        this.refundmode = refundmode;
    }

    public String getReturnrate() {
        return returnrate;
    }

    public void setReturnrate(String returnrate) {
        this.returnrate = returnrate;
    }

    public String getReturnrate2() {
        return returnrate2;
    }

    public void setReturnrate2(String returnrate2) {
        this.returnrate2 = returnrate2;
    }

    public String getReturnbeginamount() {
        return returnbeginamount;
    }

    public void setReturnbeginamount(String returnbeginamount) {
        this.returnbeginamount = returnbeginamount;
    }

    public String getReturnbegin2amount() {
        return returnbegin2amount;
    }

    public void setReturnbegin2amount(String returnbegin2amount) {
        this.returnbegin2amount = returnbegin2amount;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProductnum() {
        return productnum;
    }

    public void setProductnum(String productnum) {
        this.productnum = productnum;
    }

    public String getVouchernum() {
        return vouchernum;
    }

    public void setVouchernum(String vouchernum) {
        this.vouchernum = vouchernum;
    }

    public String getOpentime() {
        return opentime;
    }

    public void setOpentime(String opentime) {
        this.opentime = opentime;
    }

    public String getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(String recordCount) {
        this.recordCount = recordCount;
    }

    public String getGroupno() {
        return groupno;
    }

    public void setGroupno(String groupno) {
        this.groupno = groupno;
    }

    public String getSubcategoryno() {
        return subcategoryno;
    }

    public void setSubcategoryno(String subcategoryno) {
        this.subcategoryno = subcategoryno;
    }

    public String getPainame() {
        return painame;
    }

    public void setPainame(String painame) {
        this.painame = painame;
    }

    public String getTopamount() {
        return topamount;
    }

    public void setTopamount(String topamount) {
        this.topamount = topamount;
    }

    public String getOpentimeext() {
        return opentimeext;
    }

    public void setOpentimeext(String opentimeext) {
        this.opentimeext = opentimeext;
    }

    public String getRecommend() {
        return recommend;
    }

    public void setRecommend(String recommend) {
        this.recommend = recommend;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    public String getRefundnote() {
        return refundnote;
    }

    public void setRefundnote(String refundnote) {
        this.refundnote = refundnote;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public List<MerchantImageInfo> getMerchantImageList() {
        return merchantImageList;
    }

    public void setMerchantImageList(List<MerchantImageInfo> merchantImageList) {
        this.merchantImageList = merchantImageList;
    }

    public List<LableInfo> getLabels() {
        return labels;
    }

    public void setLabels(List<LableInfo> labels) {
        this.labels = labels;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public List<PriedsBean> getPrieds() {
        return prieds;
    }

    public void setPrieds(List<PriedsBean> prieds) {
        this.prieds = prieds;
    }

    public List<PackageListBean> getPackageList() {
        return packageList;
    }

    public String getActivityno() {
        return activityno;
    }

    public void setActivityno(String activityno) {
        this.activityno = activityno;
    }

    public String getBhhenable() {
        return bhhenable;
    }

    public void setBhhenable(String bhhenable) {
        this.bhhenable = bhhenable;
    }

    @Override
    public String toString() {
        return "MerchantInfoEntiy{" +
                "merchantno='" + merchantno + '\'' +
                ", activityno='" + activityno + '\'' +
                ", merchantDBType='" + merchantDBType + '\'' +
                ", subcategoryname='" + subcategoryname + '\'' +
                ", citycode='" + citycode + '\'' +
                ", status='" + status + '\'' +
                ", address='" + address + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", distance='" + distance + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", areaname='" + areaname + '\'' +
                ", imageurl='" + imageurl + '\'' +
                ", personaverage='" + personaverage + '\'' +
                ", merchantname='" + merchantname + '\'' +
                ", comentCount='" + comentCount + '\'' +
                ", star='" + star + '\'' +
                ", attention='" + attention + '\'' +
                ", refundmode='" + refundmode + '\'' +
                ", returnrate='" + returnrate + '\'' +
                ", returnrate2='" + returnrate2 + '\'' +
                ", returnbeginamount='" + returnbeginamount + '\'' +
                ", returnbegin2amount='" + returnbegin2amount + '\'' +
                ", phone='" + phone + '\'' +
                ", productnum='" + productnum + '\'' +
                ", vouchernum='" + vouchernum + '\'' +
                ", opentime='" + opentime + '\'' +
                ", recordCount='" + recordCount + '\'' +
                ", groupno='" + groupno + '\'' +
                ", subcategoryno='" + subcategoryno + '\'' +
                ", painame='" + painame + '\'' +
                ", topamount='" + topamount + '\'' +
                ", opentimeext='" + opentimeext + '\'' +
                ", recommend='" + recommend + '\'' +
                ", promotion='" + promotion + '\'' +
                ", refundnote='" + refundnote + '\'' +
                ", story='" + story + '\'' +
                ", notice='" + notice + '\'' +
                ", merchantImageList=" + merchantImageList +
                ", labels=" + labels +
                ", album='" + album + '\'' +
                ", bhhenable='" + bhhenable + '\'' +
                ", prieds=" + prieds +
                ", packageList=" + packageList +
                ", priedList=" + priedList +
                ", categoryno='" + categoryno + '\'' +
                '}';
    }

    @Override
    public int getmType() {
        return 1;
    }
}
