package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

/**
 * 开发者：LuoYi
 * Time: 2017 16:01 2017/12/14 12
 */
@Keep
public class ActivityDetailBean {

    public ActivityBean activity;
    public ActivityAuthBean activityAuth;

    @Override
    public String toString() {
        return "ActivityDetailBean{" +
                "activity=" + activity +
                ", activityAuth=" + activityAuth +
                '}';
    }

    @Keep
    public class ActivityBean {
        public String id = "";
        public String no = "";
        public String startDate = "";
        public String endDate = "";
        public String startTime = "";
        public String endTime = "";
        public String dateType = "";
        public String dateConfig = "";
        public String userType = "";
        public String userTypeName = "";
        public String refundMode = "";
        public String returnRate = "";
        public String discount = "";
        public String returnBeginAmount = "";
        public String topAmount = "";
        public String djVoucher = "";
        public String tradeNum = "";
        public String discountAmount = "";
        public String tradeAmount = "";
        public String createTime = "";
        public String source = "";
        public String isStart = "";
        public String discountTitle = ""; // 优惠title
        public String topAmountTitle = "";//最高优惠
        public String activityTime = ""; // 活动时间
        public String useTime = ""; // 使用时间
        public String notUseName = ""; // 可用日期
        public String discountContent = ""; // 使用规则
        public String discountLimitType = "";
        public String discountLimitOption = "";
        public String discountLimitText = "";
        public String currdiscount = "";
        public String nos = "";
        public String times = "";
        public String details = "";

        @Override
        public String toString() {
            return "ActivityBean{" +
                    "id='" + id + '\'' +
                    ", no='" + no + '\'' +
                    ", startDate='" + startDate + '\'' +
                    ", endDate='" + endDate + '\'' +
                    ", startTime='" + startTime + '\'' +
                    ", endTime='" + endTime + '\'' +
                    ", dateType='" + dateType + '\'' +
                    ", dateConfig='" + dateConfig + '\'' +
                    ", userType='" + userType + '\'' +
                    ", userTypeName='" + userTypeName + '\'' +
                    ", refundMode='" + refundMode + '\'' +
                    ", returnRate='" + returnRate + '\'' +
                    ", discount='" + discount + '\'' +
                    ", returnBeginAmount='" + returnBeginAmount + '\'' +
                    ", topAmount='" + topAmount + '\'' +
                    ", djVoucher='" + djVoucher + '\'' +
                    ", tradeNum='" + tradeNum + '\'' +
                    ", discountAmount='" + discountAmount + '\'' +
                    ", tradeAmount='" + tradeAmount + '\'' +
                    ", createTime='" + createTime + '\'' +
                    ", source='" + source + '\'' +
                    ", isStart='" + isStart + '\'' +
                    ", discountTitle='" + discountTitle + '\'' +
                    ", topAmountTitle='" + topAmountTitle + '\'' +
                    ", activityTime='" + activityTime + '\'' +
                    ", useTime='" + useTime + '\'' +
                    ", notUseName='" + notUseName + '\'' +
                    ", discountContent='" + discountContent + '\'' +
                    ", discountLimitType='" + discountLimitType + '\'' +
                    ", discountLimitOption='" + discountLimitOption + '\'' +
                    ", discountLimitText='" + discountLimitText + '\'' +
                    ", currdiscount='" + currdiscount + '\'' +
                    ", nos='" + nos + '\'' +
                    ", times='" + times + '\'' +
                    ", details='" + details + '\'' +
                    '}';
        }
    }

    @Keep
    public class ActivityAuthBean {
        public String activityStatus = "";
        public String activityMessage = "";//该优惠仅在00:00~06:00可以使用，请晚点再来

        @Override
        public String toString() {
            return "ActivityAuthBean{" +
                    "activityStatus='" + activityStatus + '\'' +
                    ", activityMessage='" + activityMessage + '\'' +
                    '}';
        }
    }

}
