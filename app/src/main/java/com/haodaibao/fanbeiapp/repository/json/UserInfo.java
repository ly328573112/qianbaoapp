package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

@Keep
public class UserInfo {

    private String certauth;// 实名认证标识 0未认证，1已认证(判断是否绑卡)
    private String username;
    private String businesspwd;// 交易密码设置状态 0未设置，1已设置
    private String alias;// 别名
    private String certno;// 身份证编号
    private String organizationno;// 机构号
    private String userno;// 用户号
    private String mobile;// 电话号
    private String face;// 头像
    private String rewardsum;// 返现总金额
    private String cashsum;// 支付总金额
    private String cardNum;// 银行卡数量
    public String bhhAcessToken;

    public UserInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String getCertauth() {
        return certauth;
    }

    public void setCertauth(String certauth) {
        this.certauth = certauth;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBusinesspwd() {
        return businesspwd;
    }

    public void setBusinesspwd(String businesspwd) {
        this.businesspwd = businesspwd;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getCertno() {
        return certno;
    }

    public void setCertno(String certno) {
        this.certno = certno;
    }

    public String getOrganizationno() {
        return organizationno;
    }

    public void setOrganizationno(String organizationno) {
        this.organizationno = organizationno;
    }

    public String getUserno() {
        return userno;
    }

    public void setUserno(String userno) {
        this.userno = userno;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFace() {
        return face;
    }

    public void setFace(String face) {
        this.face = face;
    }

    public String getRewardsum() {
        return rewardsum;
    }

    public void setRewardsum(String rewardsum) {
        this.rewardsum = rewardsum;
    }

    public String getCashsum() {
        return cashsum;
    }

    public void setCashsum(String cashsum) {
        this.cashsum = cashsum;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

}
