package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

@Keep
public class MarqueeBean {
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MarqueeBean that = (MarqueeBean) o;

        if (imageUrl != null ? !imageUrl.equals(that.imageUrl) : that.imageUrl != null) {
            return false;
        }
        if (titleText != null ? !titleText.equals(that.titleText) : that.titleText != null) {
            return false;
        }
        if (contentText != null ? !contentText.equals(that.contentText) : that.contentText != null) {
            return false;
        }
        return messageNo != null ? messageNo.equals(that.messageNo) : that.messageNo == null;

    }

    @Override
    public int hashCode() {
        int result = imageUrl != null ? imageUrl.hashCode() : 0;
        result = 31 * result + (titleText != null ? titleText.hashCode() : 0);
        result = 31 * result + (contentText != null ? contentText.hashCode() : 0);
        result = 31 * result + (messageNo != null ? messageNo.hashCode() : 0);
        return result;
    }

    public String imageUrl;
    public String titleText;
    public String contentText;
    public String messageNo;

    @Override
    public String toString() {
        return "MarqueeBean{" + "imageUrl='" + imageUrl + '\'' + ", titleText='" + titleText + '\'' + ", contentText='" + contentText + '\'' + '}';
    }
}