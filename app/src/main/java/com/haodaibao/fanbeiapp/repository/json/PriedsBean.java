package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;

/**
 * 开发者：LuoYi
 * Time: 2017 11:11 2017/9/18 09
 */

@Keep
public class PriedsBean implements Serializable{

    private static final long serialVersionUID = -7904901105852155461L;
    private String content;// 优惠说明
    private String adaptType; // 1、仅新用户  2、所有用户

    public PriedsBean(String content, String adaptType) {
        this.content = content;
        this.adaptType = adaptType;
    }

    public String getContent() {
        return content;
    }

    public String getAdaptType() {
        return adaptType;
    }

    @Override
    public String toString() {
        return "PriedsBean{" +
                "content='" + content + '\'' +
                ", adaptType='" + adaptType + '\'' +
                '}';
    }
}