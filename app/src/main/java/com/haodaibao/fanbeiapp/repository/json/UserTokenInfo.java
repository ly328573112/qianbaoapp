package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

@Keep
public class UserTokenInfo {
    private String loanAccessToken;
    private String ac_token;
    private String rf_token;
    private String token_expire_at;

    public String getLoanAccessToken() {
        return loanAccessToken;
    }

    public void setLoanAccessToken(String loanAccessToken) {
        this.loanAccessToken = loanAccessToken;
    }

    public String getAc_token() {
        return ac_token;
    }

    public void setAc_token(String ac_token) {
        this.ac_token = ac_token;
    }

    public String getRf_token() {
        return rf_token;
    }

    public void setRf_token(String rf_token) {
        this.rf_token = rf_token;
    }

    public String getToken_expire_at() {
        return token_expire_at;
    }

    public void setToken_expire_at(String token_expire_at) {
        this.token_expire_at = token_expire_at;
    }
}
