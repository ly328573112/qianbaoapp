package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;
import android.text.TextUtils;

@Keep
public class MyLocation {
    public double longitude = -180.f;
    public double latitude = -180.f;
    public String address = "";
    public String countryName = "";
    public String provinceName = "";
    public String cityName = "";
    public String cityNameOrig = "";
    public String districtName = "";
    public String streetName = "";
    public String cityCode = "";
    public String adCode = "";
    public boolean locationSuccess = false;

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityNameOrig() {
        if (TextUtils.isEmpty(cityNameOrig)){
            cityNameOrig = cityName;
        }
        return cityNameOrig;
    }

    public void setCityNameOrig(String cityNameOrig) {
        this.cityNameOrig = cityNameOrig;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getAdCode() {
        return adCode;
    }

    public void setAdCode(String adCode) {
        this.adCode = adCode;
    }

    public boolean isLocationSuccess() {
        return locationSuccess;
    }

    public void setLocationSuccess(boolean locationSuccess) {
        this.locationSuccess = locationSuccess;
    }
}
