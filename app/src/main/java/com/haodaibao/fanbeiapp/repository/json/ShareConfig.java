package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;

@Keep
public class ShareConfig implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private int shareType;
	private String title;
	private String message;
	private String text;
	private String imageurl;
	private String webpageurl;
	private String musicurl;
	private String videourl;
	private String voiceurl;

	public ShareConfig() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getShareType() {
		return shareType;
	}

	public void setShareType(int shareType) {
		this.shareType = shareType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getImageurl() {
		return imageurl;
	}

	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}

	public String getWebpageurl() {
		return webpageurl;
	}

	public void setWebpageurl(String webpageurl) {
		this.webpageurl = webpageurl;
	}

	public String getMusicurl() {
		return musicurl;
	}

	public void setMusicurl(String musicurl) {
		this.musicurl = musicurl;
	}

	public String getVideourl() {
		return videourl;
	}

	public void setVideourl(String videourl) {
		this.videourl = videourl;
	}

	public String getVoiceurl() {
		return voiceurl;
	}

	public void setVoiceurl(String voiceurl) {
		this.voiceurl = voiceurl;
	}

	@Override
	public String toString() {
		return "ShareConfig [shareType=" + shareType + ", title=" + title + ", message=" + message + ", text=" + text + ", imageurl=" + imageurl + ", webpageurl=" + webpageurl + ", musicurl="
				+ musicurl + ", videourl=" + videourl + ", voiceurl=" + voiceurl + "]";
	}

}
