package com.haodaibao.fanbeiapp.repository;

import com.baseandroid.config.Constant;
import com.baseandroid.retrofit.RetrofitManager;
import com.baseandroid.retrofit.RxRetrofitCache;
import com.google.gson.reflect.TypeToken;
import com.haodaibao.fanbeiapp.repository.json.ActivityDetailBean;
import com.haodaibao.fanbeiapp.repository.json.CommentListBean;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.ErrorRecoveryList;
import com.haodaibao.fanbeiapp.repository.json.MerchantDetailResp;
import com.haodaibao.fanbeiapp.repository.services.MerchantService;

import java.util.Map;

import io.reactivex.Observable;

/**
 * 开发者：LuoYi
 * Time: 2017 16:40 2017/9/4 09
 */

public class MerchantRepository {

    public static final long TIME_ONE_HOUR = 3600000;
    private static MerchantRepository instance = null;

    private MerchantRepository() {
    }

    public static MerchantRepository getInstance() {
        synchronized (LifeRepository.class) {
            if (instance == null) {
                instance = new MerchantRepository();
            }
        }
        return instance;
    }

    /**
     * 获取商户详情
     */
    public Observable<Data<MerchantDetailResp>> getMerchantDetails(Map<String, String> fields, boolean forceRefresh) {
        String cacheKey = MerchantDetailResp.class.getName() + fields.hashCode();
        MerchantService merchantService = RetrofitManager.getRxRetrofit().create(MerchantService.class);
        Observable<Data<MerchantDetailResp>> fromNetwork = merchantService.getMerchantInfo(fields);
        return RxRetrofitCache.load(cacheKey, TIME_ONE_HOUR, fromNetwork, forceRefresh, new TypeToken<Data<MerchantDetailResp>>() {
        }.getType());
    }

    /**
     * 收藏
     */
    public Observable<Data> followMerchant(Map<String, String> follow) {
        MerchantService merchantService = RetrofitManager.getRxRetrofit().create(MerchantService.class);
        return merchantService.followMerchant(follow);
    }

    /**
     * 评价列表（简略）
     */
    public Observable<Data<CommentListBean>> getCommentList(Map<String, String> followList, boolean forceRefresh) {
        String cacheKey = CommentListBean.class.getName() + followList.hashCode();
        MerchantService merchantService = RetrofitManager.getRxRetrofit().create(MerchantService.class);
        Observable<Data<CommentListBean>> commentList = merchantService.getCommentList(followList);
        return RxRetrofitCache.load(cacheKey, TIME_ONE_HOUR, commentList, forceRefresh, new TypeToken<Data<CommentListBean>>() {
        }.getType());
    }

    /**
     * 获取评价列表(详细版)
     */
    public Observable<Data<CommentListBean>> getCommentListMoreServer(Map<String, String> fields, boolean forceRefresh) {
        String cacheKey = CommentListBean.class.getName() + fields.hashCode();
        final long expireTime = Constant.TIME_ONE_HOUR;
        MerchantService service = RetrofitManager.getRxRetrofit().create(MerchantService.class);
        Observable<Data<CommentListBean>> fromNetwork = service.getCommentListMore(fields);
        return RxRetrofitCache.load(cacheKey, expireTime, fromNetwork, forceRefresh, new TypeToken<Data<CommentListBean>>() {
        }.getType());
    }

    /**
     * 提交评价
     */
    public Observable<Data<Object>> submitComment(Map<String, String> fields, boolean forceRefresh) {
        String cacheKey = CommentListBean.class.getName() + fields.hashCode();
        final long expireTime = Constant.TIME_ONE_HOUR;
        MerchantService service = RetrofitManager.getRxRetrofit().create(MerchantService.class);
        Observable<Data<Object>> fromNetwork = service.submitComment(fields);
        return RxRetrofitCache.load(cacheKey, expireTime, fromNetwork, forceRefresh, new TypeToken<Data<Object>>() {
        }.getType());
    }

    /**
     * 商户纠错信息
     *
     * @param errorRecoveryList
     * @return
     */
    public Observable<Data<ErrorRecoveryList>> getErrorRecoveryList(Map<String, String> errorRecoveryList) {
        MerchantService merchantService = RetrofitManager.getRxRetrofit().create(MerchantService.class);
        return merchantService.getErrorRecoveryList(errorRecoveryList);
    }

    /**
     * 商户纠错提交
     *
     * @param errorRecovery
     * @return
     */
    public Observable<Data> merchantErrorRecovery(Map<String, String> errorRecovery) {
        MerchantService merchantService = RetrofitManager.getRxRetrofit().create(MerchantService.class);
        return merchantService.merchantErrorRecovery(errorRecovery);
    }

    /**
     * 优惠说明
     *
     * @param fields
     * @return
     */
    public Observable<Data<ActivityDetailBean>> getActivityDetail(Map<String, String> fields) {
        MerchantService merchantService = RetrofitManager.getRxRetrofit().create(MerchantService.class);
        return merchantService.getActivityDetail(fields);
    }

    /**
     * 未读消息
     *
     * @param unReadMsg
     * @return
     */
    public Observable<Data<Object>> unReadMsgsNum(Map<String, String> unReadMsg) {
        MerchantService merchantService = RetrofitManager.getRxRetrofit().create(MerchantService.class);
        return merchantService.unReadMsgsNum(unReadMsg);
    }
}
