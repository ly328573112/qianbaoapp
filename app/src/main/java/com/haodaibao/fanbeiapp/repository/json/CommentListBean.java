package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import com.haodaibao.fanbeiapp.module.merchant.temporarydata.MerchantTemporary;

import java.util.List;

/**
 * Created by hdb on 2017/7/11.
 */
@Keep
public class CommentListBean {

    private List<MyHeader> head;

    private String count;
    private Comment list;

    public Comment getList() {
        return list;
    }

    public void setList(Comment list) {
        this.list = list;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }


    public List<MyHeader> getHead() {
        return head;
    }

    public void setHead(List<MyHeader> head) {
        this.head = head;
    }

    @Keep
    public static class CommentBean implements MerchantTemporary {
        private String scoreno;
        private String merchantno;
        private String userno;
        private String star;
        private String comment;
        private String time;
        private String status;
        private String orderno;

        private String face;
        private String alias;
        private String mobile;
        private List<String> imgs;

        public String getFace() {
            return face;
        }

        public void setFace(String face) {
            this.face = face;
        }

        public String getAlias() {
            return alias;
        }

        public void setAlias(String alias) {
            this.alias = alias;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getScoreno() {
            return scoreno;
        }

        public void setScoreno(String scoreno) {
            this.scoreno = scoreno;
        }

        public String getMerchantno() {
            return merchantno;
        }

        public void setMerchantno(String merchantno) {
            this.merchantno = merchantno;
        }

        public String getUserno() {
            return userno;
        }

        public void setUserno(String userno) {
            this.userno = userno;
        }

        public String getStar() {
            return star;
        }

        public void setStar(String star) {
            this.star = star;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getOrderno() {
            return orderno;
        }

        public void setOrderno(String orderno) {
            this.orderno = orderno;
        }

        public List<String> getImgs() {
            return imgs;
        }

        public void setImgs(List<String> imgs) {
            this.imgs = imgs;
        }

        @Override
        public int getmType() {
            return 0;
        }
    }

    @Keep
    public class MyHeader {
        private String queryType;
        private String count;
        private String word;

        public String getQueryType() {
            return queryType;
        }

        public void setQueryType(String queryType) {
            this.queryType = queryType;
        }

        public String getCount() {
            return count;
        }

        public void setCount(String count) {
            this.count = count;
        }

        public String getWord() {
            return word;
        }

        public void setWord(String word) {
            this.word = word;
        }
    }

    @Keep
    public class Comment {
        private List<CommentBean> items;

        public List<CommentBean> getItems() {
            return items;
        }

        public void setItems(List<CommentBean> items) {
            this.items = items;
        }
    }


}
