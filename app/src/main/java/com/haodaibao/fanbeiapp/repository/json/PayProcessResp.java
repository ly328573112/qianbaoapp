package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

@Keep
public class PayProcessResp {
    private String orderno;// 订单号
    private PayInfo bhh;
    private ActivityError activityError;

    public PayProcessResp() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public PayInfo getBhh() {
        return bhh;
    }

    public void setBhh(PayInfo bhh) {
        this.bhh = bhh;
    }

    public ActivityError getActivityError() {
        return activityError;
    }

    public void setActivityError(ActivityError activityError) {
        this.activityError = activityError;
    }

    @Keep
    public class ActivityError {
        public String errorCode;
        public String reason;

        public String getErrorCode() {
            return errorCode;
        }

        public String getReason() {
            return reason;
        }
    }
}
