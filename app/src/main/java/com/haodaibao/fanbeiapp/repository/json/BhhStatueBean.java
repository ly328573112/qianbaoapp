package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

/**
 * Created by Routee on 2017/9/19.
 * description: ${cusor}
 */
@Keep
public class BhhStatueBean {

    /**
     * SHOWBHH : 1
     * MIN_INSTALLABLE_AMT : 10000
     */

    private String SHOWBHH;
    private String MIN_INSTALLABLE_AMT;

    public String getSHOWBHH() {
        return SHOWBHH;
    }

    public void setSHOWBHH(String SHOWBHH) {
        this.SHOWBHH = SHOWBHH;
    }

    public String getMIN_INSTALLABLE_AMT() {
        return MIN_INSTALLABLE_AMT;
    }

    public void setMIN_INSTALLABLE_AMT(String MIN_INSTALLABLE_AMT) {
        this.MIN_INSTALLABLE_AMT = MIN_INSTALLABLE_AMT;
    }
}
