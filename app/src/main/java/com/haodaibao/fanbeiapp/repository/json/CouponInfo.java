package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;
import java.util.List;

/**
 * 摇一摇——红包信息 实体类
 */
@Keep
public class CouponInfo implements Serializable {

    private static final long serialVersionUID = 6232494022802196535L;
    private String serialno;// 领取编号
    private String couponno;// 红包编号
    private String couponshortname;// 红包名称
    private String gettype;// 领取类型1：主动领取2：被动领取
    private String userno;// 用户编号
    private String amount;// 金额
    private String usescope;// 使用范围(00：平台发(所有用户可领取)01：普通商户发(所有用户可领取，只能在该商户使用)02：连锁商户发(所有用户可领取，只能在该连锁使用)03：合作机构发(只有该机构用户可领取))
    private String merchantno;// 商户编号或合作机构编号
    private String gettime;// 领取时间
    private String starttime;// 生效时间
    private String endtime;// 失效时间
    private String status;// 0：未使用1：已使用
    private String isFailure;// 00未失效01未生效02已失效
    private String usebeginamt;// 使用起始金额
    private String couponName;// 商户、机构、总店名称
    private String couponText;// 商户、机构、总店名称拼接
    private String categoryText;// 分类拼接
    private String bankName;// 银行名称
    private String bankNo;// 银行编号
    private String role;
    private String userAmount;
    private String canUse;//0:可用；1：不在开放城市范围内
    private String isSelect;
    private String unReason;//不可用原因
    private List<CityInfo> citys;

    public List<CityInfo> getCitys() {
        return citys;
    }

    public void setCitys(List<CityInfo> citys) {
        this.citys = citys;
    }

    public String getUnReason() {
        return unReason;
    }

    public void setUnReason(String unReason) {
        this.unReason = unReason;
    }

    public String getIsSelect() {
        return isSelect;
    }

    public void setIsSelect(String isSelect) {
        this.isSelect = isSelect;
    }

    public String getCanUse() {
        return canUse;
    }

    public void setCanUse(String canUse) {
        this.canUse = canUse;
    }

    public String getUserAmount() {
        return userAmount;
    }

    public void setUserAmount(String userAmount) {
        this.userAmount = userAmount;
    }

    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    public String getCouponno() {
        return couponno;
    }

    public void setCouponno(String couponno) {
        this.couponno = couponno;
    }

    public String getCouponshortname() {
        return couponshortname;
    }

    public void setCouponshortname(String couponshortname) {
        this.couponshortname = couponshortname;
    }

    public String getGettype() {
        return gettype;
    }

    public void setGettype(String gettype) {
        this.gettype = gettype;
    }

    public String getUserno() {
        return userno;
    }

    public void setUserno(String userno) {
        this.userno = userno;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getUsescope() {
        return usescope;
    }

    public void setUsescope(String usescope) {
        this.usescope = usescope;
    }

    public String getMerchantno() {
        return merchantno;
    }

    public void setMerchantno(String merchantno) {
        this.merchantno = merchantno;
    }

    public String getGettime() {
        return gettime;
    }

    public void setGettime(String gettime) {
        this.gettime = gettime;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsFailure() {
        return isFailure;
    }

    public void setIsFailure(String isFailure) {
        this.isFailure = isFailure;
    }

    public String getUsebeginamt() {
        return usebeginamt;
    }

    public void setUsebeginamt(String usebeginamt) {
        this.usebeginamt = usebeginamt;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getCouponText() {
        return couponText;
    }

    public void setCouponText(String couponText) {
        this.couponText = couponText;
    }

    public String getCategoryText() {
        return categoryText;
    }

    public void setCategoryText(String categoryText) {
        this.categoryText = categoryText;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankNo() {
        return bankNo;
    }

    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Keep
    public class CityInfo implements Serializable {

        private static final long serialVersionUID = -9047567306800435744L;
        private String code;
        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}
