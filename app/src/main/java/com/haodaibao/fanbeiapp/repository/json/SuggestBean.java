package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

/**
 * Created by hdb on 2017/6/29.
 */
@Keep
public class SuggestBean {
    private String res;

    public String getRes() {
        return res;
    }

    public void setRes(String res) {
        this.res = res;
    }
}
