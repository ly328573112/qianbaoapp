package com.haodaibao.fanbeiapp.repository.services;

import com.haodaibao.fanbeiapp.repository.json.ActivityDetailBean;
import com.haodaibao.fanbeiapp.repository.json.CommentListBean;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.ErrorRecoveryList;
import com.haodaibao.fanbeiapp.repository.json.MerchantDetailResp;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * 开发者：LuoYi
 * Time: 2017 16:37 2017/9/4 09
 */

public interface MerchantService {

    //获取商户详情
    @FormUrlEncoded
    @POST("v1/trade/merchant/queryLifeMerchantDetail")
    Observable<Data<MerchantDetailResp>> getMerchantInfo(@FieldMap Map<String, String> detail);

    //收藏
    @FormUrlEncoded
    @POST("v1/member/user/attentionOperation")
    Observable<Data> followMerchant(@FieldMap Map<String, String> follow);

    //获取评价列表简略版
    @FormUrlEncoded
    @POST("v1/trade/merchant/usrscore/list/simple")
    Observable<Data<CommentListBean>> getCommentList(@FieldMap Map<String, String> comment);

    //获取评价列表详细
    @FormUrlEncoded
    @POST("v1/trade/merchant/usrscore/list")
    Observable<Data<CommentListBean>> getCommentListMore(@FieldMap Map<String, String> fields);

    //提交评价
    @FormUrlEncoded
    @POST("v1/trade/merchant/userScordRecord")
    Observable<Data<Object>> submitComment(@FieldMap Map<String, String> fields);

    //商户纠错列表
    @FormUrlEncoded
    @POST("v1/trade/merchant/getErrorRecoveryList")
    Observable<Data<ErrorRecoveryList>> getErrorRecoveryList(@FieldMap Map<String, String> errorRecovery);

    //商户纠错提交
    @FormUrlEncoded
    @POST("v1/trade/merchant/merchantErrorRecovery")
    Observable<Data> merchantErrorRecovery(@FieldMap Map<String, String> errorRecovery);

    //优惠说明
    @FormUrlEncoded
    @POST("v1/trade/merchant/activityDetail")
    Observable<Data<ActivityDetailBean>> getActivityDetail(@FieldMap Map<String, String> fieldes);

    //未读消息
    @FormUrlEncoded
    @POST("v1/trade/message/unReadMsgsNum")
    Observable<Data<Object>> unReadMsgsNum(@FieldMap Map<String, String> params);
}
