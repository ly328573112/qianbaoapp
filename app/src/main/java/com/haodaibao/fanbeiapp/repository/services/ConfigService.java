package com.haodaibao.fanbeiapp.repository.services;

import com.haodaibao.fanbeiapp.repository.json.CheckUpdate;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.ServerTime;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Url;

public interface ConfigService {

    @GET("v1/basic/get_server_time")
    Observable<Data<ServerTime>> getServerTime();

    @FormUrlEncoded
    @POST("v1/trade/version/getVersionInfo")
    Observable<Data<CheckUpdate>> checkUpdate(@FieldMap Map<String, String> updateMap);


    @Multipart
    @POST("file/upload.do")
    Observable<ResponseBody> uploadFileWithPartMap(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part file);

    @GET
    Observable<String> getHtmltext(@Url String httpuri);

}