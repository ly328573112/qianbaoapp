package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;
import java.util.List;

@Keep
public class YaoheDetailsBean implements Serializable {

    private String title;
    private String sendendtime;
    private String merNo;

    public String getMerNo() {
        return merNo;
    }

    public void setMerNo(String merNo) {
        this.merNo = merNo;
    }

    public String getMerName() {
        return merName;
    }

    public void setMerName(String merName) {
        this.merName = merName;
    }

    private String merName;
    private List<MessageBody> items;

    @Keep
    public static class MessageBody implements Serializable{
        private String type;//"type":"text",image
        private String content;//"content": "我是文本"
        private String time;
        private String merchantName;
        private String merNo;

        public String getMerNo() {
            return merNo;
        }

        public void setMerNo(String merNo) {
            this.merNo = merNo;
        }

        public String getMerchantName() {
            return merchantName;
        }

        public void setMerchantName(String merchantName) {
            this.merchantName = merchantName;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSendendtime() {
        return sendendtime;
    }

    public void setSendendtime(String sendendtime) {
        this.sendendtime = sendendtime;
    }

    public List<MessageBody> getItems() {
        return items;
    }

    public void setItems(List<MessageBody> items) {
        this.items = items;
    }
}