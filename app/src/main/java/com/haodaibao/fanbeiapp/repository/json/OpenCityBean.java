package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import com.haodaibao.fanbeiapp.module.search.adpter.Indexable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Transient;

import java.io.Serializable;

@Keep
@Entity
public class OpenCityBean implements Indexable, Serializable {
    private static final long serialVersionUID = -407061951823459189L;

    private int id;
    @Index(unique = true)
    private String code;
    private String name;
    private String areaName;
    private String type;
    private int open;
    private String createdTime;
    private String modifiedTime;

    @Transient
    private String sortLetters;

    @Generated(hash = 1476644913)
    public OpenCityBean(int id, String code, String name, String areaName,
                        String type, int open, String createdTime, String modifiedTime) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.areaName = areaName;
        this.type = type;
        this.open = open;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }

    @Generated(hash = 192631096)
    public OpenCityBean() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getOpen() {
        return open;
    }

    public void setOpen(int open) {
        this.open = open;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    @Override
    public String getIndex() {
        return sortLetters;
    }

    public String getSortLetters() {
        return sortLetters;
    }

    public void setSortLetters(String sortLetters) {
        this.sortLetters = sortLetters;
    }


    @Override
    public String toString() {
        return "OpenCityBean{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", areaName='" + areaName + '\'' +
                ", type='" + type + '\'' +
                ", open=" + open +
                ", createdTime='" + createdTime + '\'' +
                ", modifiedTime='" + modifiedTime + '\'' +
                ", sortLetters='" + sortLetters + '\'' +
                '}';
    }
}
