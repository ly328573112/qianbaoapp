package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.util.List;

/*商户分类响应实体类*/
@Keep
public class ShanghufenleiResp {
    private List<ShanghufenleiInfo> categoryList;

    public List<ShanghufenleiInfo> getCategoryList() {
        return categoryList;
    }

    public void setCatagoryList(List<ShanghufenleiInfo> categoryList) {
        this.categoryList = categoryList;
    }

}
