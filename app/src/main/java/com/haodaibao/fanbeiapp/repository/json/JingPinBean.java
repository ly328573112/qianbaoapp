package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

@Keep
public class JingPinBean {

	public String positionType;// 显示类型
	public String typeAd;

	public String getTypeAd() {
		return typeAd;
	}

	public void setTypeAd(String typeAd) {
		this.typeAd = typeAd;
	}

	public JingPinBean() {
		super();
	}

	public String getPositionType() {
		return positionType;
	}

	public void setPositionType(String positionType) {
		this.positionType = positionType;
	}


}
