package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

/*商户分类信息实体类*/
@Keep
public class ShanghufenleiInfo {
    private String categoryno;// 分类编号
    private String name;// 分类名称
    private String parentno;// 父分类编号
    private String hot;// 是否热点
    private String status;// 状态

    public ShanghufenleiInfo(String categoryno, String name, String parentno, String hot, String status) {
        this.categoryno = categoryno;
        this.name = name;
        this.parentno = parentno;
        this.hot = hot;
        this.status = status;
    }

    public String getCategoryno() {
        return categoryno;
    }

    public void setCategoryno(String categoryno) {
        this.categoryno = categoryno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentno() {
        return parentno;
    }

    public void setParentno(String parentno) {
        this.parentno = parentno;
    }

    public String getHot() {
        return hot;
    }

    public void setHot(String hot) {
        this.hot = hot;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
