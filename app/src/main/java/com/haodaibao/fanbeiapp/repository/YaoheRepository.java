package com.haodaibao.fanbeiapp.repository;

import com.baseandroid.retrofit.RetrofitManager;
import com.haodaibao.fanbeiapp.repository.json.Data;
import com.haodaibao.fanbeiapp.repository.json.YaoheDetailsBean;
import com.haodaibao.fanbeiapp.repository.services.YaoheService;

import java.util.Map;

import io.reactivex.Observable;

public class YaoheRepository {
    private volatile static YaoheRepository instance;

    private YaoheRepository() {
    }


    public static YaoheRepository getInstance() {
        if (instance == null) {
            synchronized (YaoheRepository.class) {
                if (instance == null) {
                    instance = new YaoheRepository();
                }
            }
        }
        return instance;
    }

    public Observable<Data<YaoheDetailsBean>> getYaoheDetails(Map<String, String> fields) {
        YaoheService service = RetrofitManager.getRxRetrofit().create(YaoheService.class);
        return service.getYaoheDetails1(fields);
    }
}