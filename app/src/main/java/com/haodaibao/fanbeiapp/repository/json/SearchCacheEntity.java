package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Keep
@Entity
public class SearchCacheEntity {

    @Id(autoincrement = true)
    private Long id;

    private String searchContent;
    private String timeId;
    @Generated(hash = 922710135)
    public SearchCacheEntity(Long id, String searchContent, String timeId) {
        this.id = id;
        this.searchContent = searchContent;
        this.timeId = timeId;
    }
    @Generated(hash = 705544793)
    public SearchCacheEntity() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getSearchContent() {
        return this.searchContent;
    }
    public void setSearchContent(String searchContent) {
        this.searchContent = searchContent;
    }
    public String getTimeId() {
        return this.timeId;
    }
    public void setTimeId(String timeId) {
        this.timeId = timeId;
    }

}