package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

/**
 * 照片信息实体类
 */
@Keep
public class PhotoInfo {

    int photoNumber;
    String photoUri;// uri路径
    boolean delete = false;// 是否处于可编辑状态

    public String failUrl;//上传失败的图片

    UploadFileResp resp;


    public PhotoInfo(int photoNumber, String photoUri, boolean delete) {
        this.photoNumber = photoNumber;
        this.photoUri = photoUri;
        this.delete = delete;
    }

    public PhotoInfo(String failUrl) {
        this.failUrl = failUrl;
    }

    public PhotoInfo(UploadFileResp resp) {
        this.resp = resp;
    }

    public int getPhotoNumber() {
        return photoNumber;
    }

    public String getPhotoUri() {
        return photoUri;
    }

    public boolean isDelete() {
        return delete;
    }

    public UploadFileResp getResp() {
        return resp;
    }
}