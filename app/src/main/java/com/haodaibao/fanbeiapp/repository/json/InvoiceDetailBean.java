package com.haodaibao.fanbeiapp.repository.json;

import java.io.Serializable;

/**
 * date: 2018/2/24.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class InvoiceDetailBean implements Serializable {
    public Invoice invoice;

    public class Invoice implements Serializable{
        private static final long serialVersionUID = 4861480702065267423L;
        public String order_no;
        public String invoice_type_code;
        public String rise_no;
        public String company_bank_name;
        public String modify_time;
        public String old_trade_no;
        public String rise_name;
        public String resieve_address;
        public String invoice_project;
        public String invoice_amount;
        public String company_address;
        public String company_mobile;
        public String user_no;
        public String invoice_date;
        public String is_deleted;
        public String check_code;
        public String MIF_MERCHANTNAME;
        public String invoice_id;
        public String id;
        public String merchant_register_name;
        public String invoice_number;
        public String email;
        public String merchant_no;
        public String create_time;
        public String invoice_no;
        public String mobile;
        public String rise_type_code;
        public String company_bank_no;
        public String invoice_type_no;
        public String trade_no;
        public String dowload_address;
        public String status;
    }
}
