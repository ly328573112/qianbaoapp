package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;

@Keep
public class LoginInfo {

    public UserInfoData data;
    public CouponBean coupon;
    public String newUser;

    @Keep
    public class UserInfoData {
        public String ac_token;
        public String rf_token;
        public String token_expire_at;
    }

    @Keep
    public class CouponBean {
        public String resultCode;
        public String shareToInfo;
        public String resultMessage;
        public String activityImageUrl;
        public String pickResult;
        public CouponPickInfo couponPickInfo;

        @Keep
        public class CouponPickInfo implements Serializable {
            public String appName;
            public String merchantName;
            public String photoUrl;
            public String amount;
            public String qbmemberno;
            public String userNo;
            public String mobile;
        }
    }
}
