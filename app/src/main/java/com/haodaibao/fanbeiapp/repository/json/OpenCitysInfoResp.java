package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;
import java.util.List;

@Keep
public class OpenCitysInfoResp implements Serializable {
    private static final long serialVersionUID = -1255951444878937954L;

    private List<OpenCityBean> sites;

    private String citySvitch;

    public List<OpenCityBean> getSites() {
        return sites;
    }

    public void setSites(List<OpenCityBean> sites) {
        this.sites = sites;
    }

    public String getCitySvitch() {
        return citySvitch;
    }

    public void setCitySvitch(String citySvitch) {
        this.citySvitch = citySvitch;
    }
}
