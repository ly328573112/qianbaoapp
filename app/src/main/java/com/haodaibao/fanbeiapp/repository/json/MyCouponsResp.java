package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;

/**
 * 我的红包响应实体
 */
@Keep
public class MyCouponsResp implements Serializable {
	private static final long serialVersionUID = -4508531340909744492L;
	private String payCouponCount;// 支付时可用红包数量
	private CouponInfoList couponList;

	public String getPayCouponCount() {
		return payCouponCount;
	}

	public void setPayCouponCount(String payCouponCount) {
		this.payCouponCount = payCouponCount;
	}

	public CouponInfoList getCouponList() {
		return couponList;
	}

	public void setCouponList(CouponInfoList couponList) {
		this.couponList = couponList;
	}

}
