package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Keep
public class ShopKindListBean implements Serializable{

    private List<ShopKindsBean> list ;

    public List<ShopKindsBean> getList() {
        return list;
    }

    public void setList(List<ShopKindsBean> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "ShopKindListBean{" +
                "list=" + list +
                '}';
    }
}
