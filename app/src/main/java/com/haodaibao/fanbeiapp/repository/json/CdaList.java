package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.util.List;

/**
 * 商圈城市信息
 */
@Keep
public class CdaList {
    private String code;// 城市编码
    private String name;// 城市名称
    private String character;
    private String provincecode;// 省编码
    private String hot;
    private List<DistrictList> districtList;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getProvincecode() {
        return provincecode;
    }

    public void setProvincecode(String provincecode) {
        this.provincecode = provincecode;
    }

    public String getHot() {
        return hot;
    }

    public void setHot(String hot) {
        this.hot = hot;
    }

    public List<DistrictList> getDistrictList() {
        return districtList;
    }

    public void setDistrictList(List<DistrictList> districtList) {
        this.districtList = districtList;
    }

}