package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

/**
 * 开发者：LuoYi
 * Time: 2017 19:50 2017/9/22 09
 */

@Keep
public class UploadingBean {

    private String status;
    private String message;
    private String code;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "UploadingBean{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
