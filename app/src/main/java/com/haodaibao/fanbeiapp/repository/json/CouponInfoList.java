package com.haodaibao.fanbeiapp.repository.json;

import android.support.annotation.Keep;

import java.io.Serializable;
import java.util.ArrayList;

@Keep
public class CouponInfoList implements Serializable {
    private static final long serialVersionUID = 343531173622543102L;
    private boolean lastPage;
    private ArrayList<CouponInfo> items;


    public boolean isLastPage() {
        return lastPage;
    }

    public void setLastPage(boolean lastPage) {
        this.lastPage = lastPage;
    }

    public ArrayList<CouponInfo> getItems() {
        return items;
    }

    public void setItems(ArrayList<CouponInfo> items) {
        this.items = items;
    }

}
