package com.haodaibao.fanbeiapp.wxapi;

import android.os.Handler;

/**
 * 开发者：LuoYi
 * Time: 2017 10:40 2017/9/8 09
 */

public class HandlerAPP {

    private Handler appHandler;

    public Handler getAppHandler() {
        return appHandler;
    }

    public void setAppHandler(Handler appHandler) {
        this.appHandler = appHandler;
    }

}
