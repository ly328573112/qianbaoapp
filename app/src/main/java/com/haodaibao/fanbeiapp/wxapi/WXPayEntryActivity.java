package com.haodaibao.fanbeiapp.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.baseandroid.config.BizUtils;
import com.baseandroid.utils.LogUtil;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {
	private static final int PAY_FLAG = 1;
	private static final int CHECK_PAY_FLAG = 2;
	private static final int RECREATE_ORDER_FLAG = 3;
	private static final int BUTTON_CLICKABLE_FLAG = 4;
	private static final String TAG = "MicroMsg.SDKSample.WXPayEntryActivity";

	private IWXAPI api;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		api = WXAPIFactory.createWXAPI(this, BizUtils.WX_APP_ID);

		api.handleIntent(getIntent(), this);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		api.handleIntent(intent, this);
	}

	@Override
	public void onReq(BaseReq req) {
	}

	@Override
	public void onResp(BaseResp resp) {
		System.out.println("-------------------------微信支付回调----------------------------");
		LogUtil.d("onPayFinish, errCode = " + resp.errCode);
		// 恢复确认支付按钮的可点击状态
		setNextBtnClickable(true);
		if (resp.errCode == 0) {// 支付成功
			finish();
			checkPayResult();
		} else if (resp.errCode == -1) {// 支付失败
			// ToastManager.showNDebug(this, "订单支付失败");
			Log.e("微信", "订单支付失败1");
			finish();
			reCreateOrder();
		} else if (resp.errCode == -2) {// 支付取消
			// ToastManager.showNDebug(this, "支付取消");
			Log.e("微信", "支付取消2");
			finish();
			reCreateOrder();
		} else {
			// ToastManager.showNDebug(this, "订单支付失败");
			Log.e("微信", "订单支付失败1");
			finish();
			reCreateOrder();

		}

	}

	private void checkPayResult() {
		HandlerAPP app = new HandlerAPP();
		Handler appHandler = app.getAppHandler();
		appHandler.sendEmptyMessage(CHECK_PAY_FLAG);
	}

	private void reCreateOrder() {
		HandlerAPP app = new HandlerAPP();
		Handler appHandler = app.getAppHandler();
		appHandler.sendEmptyMessage(RECREATE_ORDER_FLAG);
	}

	private void setNextBtnClickable(boolean clickable) {
		HandlerAPP app = new HandlerAPP();
		Handler appHandler = app.getAppHandler();
		Message msg = Message.obtain();
		Bundle data = new Bundle();
		data.putBoolean("clickable", clickable);
		msg.setData(data);
		msg.what = BUTTON_CLICKABLE_FLAG;
		appHandler.sendMessage(msg);
	}

}