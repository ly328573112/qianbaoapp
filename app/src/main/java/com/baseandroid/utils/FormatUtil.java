package com.baseandroid.utils;

import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.widget.EditText;

import com.haodaibao.fanbeiapp.repository.json.LableInfo;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.baseandroid.utils.CountUtil.round;

public class FormatUtil {

    public static String NONE = "0";// 无优惠
    public static String ALL = "1";// 立减N%
    public static String FULL = "2";// 满M立减N
    public static String PERFULL = "3";// 每满M立减N

    /**
     * 金额格式转换
     */

    public static String formatAmount(int lenth, String amount) {
        DecimalFormat decimalFormat = null;
        if (TextUtils.isEmpty(amount)) {
            return "0";
        }
        double a = round(amount, lenth, BigDecimal.ROUND_HALF_UP);
        if (lenth == 0) {
            decimalFormat = new DecimalFormat("0");
        } else if (lenth > 0) {
            StringBuffer buff = new StringBuffer();
            buff.append("0.");
            for (int i = 0; i < lenth; i++) {
                buff.append("0");
            }
            decimalFormat = new DecimalFormat(buff.toString());
        }

        String formatSub = decimalFormat.format(a);
        int length = formatSub.length();
        if (length == 0) {
            return "0";
        } else if (length == 1) {
            formatSub = decimalFormat.format(a);
        } else {
            return filterDecimalPoint(formatSub);
        }
        return formatSub;
    }

    /**
     * 过滤金额.0或.00
     *
     * @param formatSub
     * @return
     */
    public static String filterDecimalPoint(String formatSub) {
        if (TextUtils.isEmpty(formatSub)) {
            return "0";
        }
        int length = formatSub.length();
        String formatStr = formatSub.substring(length - 1, length);
        if (formatSub.toString().contains(".00")) {
            return formatSub.substring(0, length - 3);
        } else if (formatSub.toString().contains(".0") && formatStr.equals("0")) {
            return formatSub.substring(0, length - 2);
        } else if (formatSub.toString().contains(".") && formatStr.equals(".")) {
            return formatSub.substring(0, length - 1);
        } else if (formatSub.toString().contains(".") && formatStr.equals("0")) {
            return formatSub.substring(0, length - 1);
        } else {
            return formatSub;
        }
    }

    public static String subStrAmount(double amount) {
        amount = CountUtil.round(amount, 2, BigDecimal.ROUND_HALF_DOWN);
        return filterDecimalPoint(amount + "");
    }

    public static Spanned setFanxian(String refundMode, String returnBeginAmount, String returnRate) {
        String string = "";
        if (TextUtils.isEmpty(refundMode) && TextUtils.isEmpty(returnBeginAmount) && TextUtils.isEmpty(returnRate)) {
            return Html.fromHtml(string);
        }
        DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
        returnBeginAmount = decimalFormat.format(new BigDecimal(returnBeginAmount));
        if (refundMode.equals("0")) {
        } else if (refundMode.equals("1")) {
            if (!TextUtils.isEmpty(returnRate)) {
                returnRate = FormatUtil.formatAmount(1, "" + (1 - Double.valueOf(returnRate)) * 10);
                if (0 < Double.parseDouble(returnRate)) {
                    string = "买单<big><big><font color='#F62241'>" + returnRate + "</font></big></big>" + "折";
                }
            }
        } else if (refundMode.equals("2")) {
            returnRate = decimalFormat.format(new BigDecimal(returnRate));
            string = "满" + returnBeginAmount + "减" + "<big><big><font color='#F62241'>" + returnRate + "</font></big></big>" + "元";
        } else if (refundMode.equals(PERFULL)) {
            returnRate = decimalFormat.format(new BigDecimal(returnRate));
            string = "每满" + returnBeginAmount + "减" + "<big><big><font color='#F62241'>" + returnRate + "</font></big></big>" + "元";
        }
        return Html.fromHtml(string);
    }

    public static Spanned setFanxian1(String refundMode, String returnBeginAmount, String returnRate) {
        String string = "";
        if (TextUtils.isEmpty(refundMode) && TextUtils.isEmpty(returnBeginAmount) && TextUtils.isEmpty(returnRate)) {
            return Html.fromHtml(string);
        }
        DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
        returnBeginAmount = decimalFormat.format(new BigDecimal(returnBeginAmount));
        if (refundMode.equals("0")) {
        } else if (refundMode.equals("1")) {
            if (!TextUtils.isEmpty(returnRate)) {
                returnRate = FormatUtil.formatAmount(1, "" + (1 - Double.valueOf(returnRate)) * 10);
                if (0 < Double.parseDouble(returnRate)) {
                    string = "买单" + returnRate + "折";
                }
            }
        } else if (refundMode.equals("2")) {
            returnRate = decimalFormat.format(new BigDecimal(returnRate));
            string = "满" + returnBeginAmount + "减" + returnRate + "元";
        } else if (refundMode.equals(PERFULL)) {
            returnRate = decimalFormat.format(new BigDecimal(returnRate));
            string = "每满" + returnBeginAmount + "减" + returnRate + "元";
        }
        return Html.fromHtml(string);
    }

    public static String setMapDiscount(String refundMode, String returnBeginAmount, String returnRate) {
        try {
            String string = "";
            if (TextUtils.isEmpty(refundMode) && TextUtils.isEmpty(returnBeginAmount) && TextUtils
                    .isEmpty(returnRate)) {
                return string;
            }
            returnBeginAmount = FormatUtil.formatAmount(0, returnBeginAmount);
            if (refundMode.equals("0")) {

            } else if (refundMode.equals("1")) {
                if (!TextUtils.isEmpty(returnRate)) {
                    returnRate = FormatUtil.formatAmount(1, "" + (1 - Double.valueOf(returnRate)) * 10);
                    if (0 < Double.parseDouble(returnRate)) {
                        string = "<big><big>" + returnRate + "</big></big>" + "折";
                    }
                }

            } else if (refundMode.equals("2")) {
//            returnRate = FormatUtil.formatAmount(0, returnRate);
                DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
                returnRate = decimalFormat.format(new BigDecimal(returnRate));
                string = "满" + "<big><big>" + returnBeginAmount + "</big></big>" + "减" + "<big><big>" + returnRate + "</big></big>" + "元";
            } else if (refundMode.equals(PERFULL)) {
//            returnRate = FormatUtil.formatAmount(0, returnRate);
                DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
                returnRate = decimalFormat.format(new BigDecimal(returnRate));
                string = "<big><big>" + returnBeginAmount + "</big></big>" + "减" + "<big><big>" + returnRate + "</big></big>";
            }
            return string;
        } catch (Exception e) {
            return "";
        }

    }

    /**
     * 显示优惠信息,附近商户列表活动信息
     *
     * @param refundMode
     * @param returnBeginAmount
     * @param returnRate
     * @return if(refundMode = 1)立减returnRate% if(refundMode = 2)满returnBeginAmount立减returnRate
     * if(refundMode = 3)每满returnBeginAmount立减returnRate
     */
    public static String setDiscountContent(String refundMode, String returnBeginAmount, String returnRate) {
        String string = "";
        if (TextUtils.isEmpty(returnBeginAmount) && TextUtils.isEmpty(returnRate)) {
            return string;
        }
        returnBeginAmount = FormatUtil.formatAmount(0, returnBeginAmount);
        if (refundMode.equals(NONE)) {
        } else if (refundMode.equals(ALL)) {
            if (!TextUtils.isEmpty(returnRate)) {
                returnRate = FormatUtil.formatAmount(1, "" + (1 - Double.valueOf(returnRate)) * 10);
                if (0 < Double.parseDouble(returnRate)) {
                    string = "优惠买单<center><big><big><font color='#F62241'>" + returnRate + "</font></big></big></center>折";
                }
            }
        } else if (refundMode.equals(FULL)) {
//            returnRate = FormatUtil.formatAmount(0, returnRate);
            DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
            returnRate = decimalFormat.format(new BigDecimal(returnRate));
            string = " 满" + returnBeginAmount + "减" + "<center><big><big><font color='#F62241'>" + returnRate + "</font></big></big></center>元";
        } else if (refundMode.equals(PERFULL)) {
//            returnRate = FormatUtil.formatAmount(0, returnRate);
            DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
            returnRate = decimalFormat.format(new BigDecimal(returnRate));
            string = "每满" + returnBeginAmount + "减" + "<center><big><big><font color='#F62241'>" + returnRate + "</font></big></big></center>元";
        }
        return string;
    }

    /**
     * 拼接优惠活动信息
     */
    public static String setActivityInfo(List<LableInfo> lableList) {
        StringBuilder builder = new StringBuilder();
        for (LableInfo lableInfo : lableList) {
            //图标
            String lableIcon = CommonUtils.getImageUrl(lableInfo.getLabelicon());
            String html_hui = "<img src='" + lableIcon + "'/>";
            //活动名称
            String labelRemark = lableInfo.getLabelremark();

            String lableStr = html_hui + " " + labelRemark;
            builder.append(lableStr);
            builder.append("<br>");
        }
        String s = builder.toString();
        if (s.contains("<br>")) {
            return s.substring(0, s.lastIndexOf("<br>"));
        } else {
            return s;
        }
    }

    /**
     * 显示优惠信息,全单打折或者全单满减
     */
    public static String setdicountstyle(String refundMode, String returnBeginAmount, String returnRate) {
        String string = "";
        if (TextUtils.isEmpty(returnBeginAmount) && TextUtils.isEmpty(returnRate)) {
            return string;
        }
        if (refundMode.equals(NONE)) {
            string = "无优惠";
        } else if (refundMode.equals(ALL)) {
            string = "全单打折";
        } else if (refundMode.equals(FULL)) {
            string = "全单满减";
        } else if (refundMode.equals(PERFULL)) {
            string = "全单满减";
        }
        return string;
    }

    /**
     * yyyyMMddHHmmss 转 yyyy/MM/dd HH:mm
     *
     * @param time
     * @return
     */
    public static String changeTime(String time, String timeFormat) {
        if (TextUtils.isEmpty(time)) {
            return "";
        }
        SimpleDateFormat oldFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
        SimpleDateFormat newFormat = new SimpleDateFormat(timeFormat, Locale.getDefault());
        Date currentTime = new Date();
        try {
            currentTime = oldFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newFormat.format(currentTime);
    }

    public static String getCurrentDate(String string) {
        if (TextUtils.isEmpty(string)) {
            return "时间格式错误";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
        try {
            Date date = sdf.parse(string);
            Date currentDate = new Date();
            long day = (date.getTime() + 8 * 1000 * 60 * 60) / 1000 / 60 / 60 / 24;
            long currentDay = (currentDate.getTime() + 8 * 1000 * 60 * 60) / 1000 / 60 / 60 / 24;
            if (currentDay == day + 1) {
                return "昨天";
            } else if (currentDay == day) {
                return new SimpleDateFormat("HH:mm", Locale.getDefault()).format(date);
            } else {
                return new SimpleDateFormat("MM-dd", Locale.getDefault()).format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "时间格式错误";
    }


    /**
     * 输入手机号时自动添加空格
     */
    public static void formatMobile(CharSequence s, int count, EditText et) {
        if (count == 1) {
            int length = s.toString().length();
            if (length == 3 || length == 8) {
                et.setText(s + " ");
                et.setSelection(et.getText().toString().length());
            }
        }
    }

    public static void addEditSpace(CharSequence s, int start, int before, EditText _text) {
        if (s == null || s.length() == 0) {
            return;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (i != 3 && i != 8 && s.charAt(i) == ' ') {

            } else {
                sb.append(s.charAt(i));
                if ((sb.length() == 4 || sb.length() == 9) && sb.charAt(sb.length() - 1) != ' ') {
                    sb.insert(sb.length() - 1, ' ');
                }
            }
        }
        if (!sb.toString().equals(s.toString())) {
            int index = start + 1;
            if (sb.charAt(start) == ' ') {
                if (before == 0) {
                    index++;
                } else {
                    index--;
                }
            } else {
                if (before == 1) {
                    index--;
                }
            }
            _text.setText(sb.toString());
            _text.setSelection(index);

        }
    }

    public static String formatDistance(String distance) {
        double dis = round(distance, 2, BigDecimal.ROUND_HALF_UP);
        if (dis == -1) {
            return "";
        }
        if (dis < 1000) {
            return formatAmount(0, String.valueOf(dis)) + "m";
        } else {
            return formatAmount(1, String.valueOf(dis / 1000)) + "km";
        }
    }

}
