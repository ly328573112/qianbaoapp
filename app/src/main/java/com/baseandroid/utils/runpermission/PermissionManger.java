package com.baseandroid.utils.runpermission;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.view.View;

import com.baseandroid.base.BaseActivity;
import com.baseandroid.config.Global;
import com.baseandroid.gaode.GaoDeMap;
import com.baseandroid.utils.LogUtil;
import com.haodaibao.fanbeiapp.module.dialog.MessageDialog;
import com.tbruyelle.rxpermissions2.RxPermissions;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by hdb on 2017/5/2.
 */

public class PermissionManger {
    public static final int READ_PHONE_REQUESTCODE = 1;
    PermissionListener listener;
    static PermissionManger permissionManger;
    private String[] permissions, opsPermissions;
    MessageDialog messageDialog;
    RxPermissions rxPermissions;

    public static PermissionManger getInstance() {
        synchronized (PermissionManger.class) {
            if (permissionManger == null) {
                synchronized (PermissionManger.class) {
                    permissionManger = new PermissionManger();
                }
            }
        }

        return permissionManger;
    }


    public void startLocation(final BaseActivity activity) {
        rxPermissions = new RxPermissions(activity);
        rxPermissions.request(Manifest.permission.ACCESS_FINE_LOCATION)
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull Boolean aBoolean) {
                        if (!aBoolean) {
                            if (messageDialog == null) {
                                messageDialog = MessageDialog.newInstance();
                            }

                            if (!messageDialog.isVisible()) {
                                messageDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        String city = Global.getMyLocation().getCityName();
                                        messageDialog.setTitle("定位服务未开启");
                                        messageDialog.setContent("开启定位可获得更精准的商户推荐");
                                        messageDialog.setConfirmText("去设置");
                                        messageDialog.setCancelText("暂不");
                                        messageDialog.setConfirmOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                messageDialog.dismiss();
                                                PermissionManger.getInstance().startSetting(activity);
                                            }
                                        });
                                        messageDialog.setCancelOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                messageDialog.dismiss();
                                            }
                                        });
                                    }
                                });
                                if (!messageDialog.isAdded()) {
                                    messageDialog.show(activity.getSupportFragmentManager(), "MessageDialog");
                                }

                            }
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        LogUtil.e("NO : onError");
                    }

                    @Override
                    public void onComplete() {
                        LogUtil.e(" NO : onComplete");
                        GaoDeMap.getInstance().startMap("allC");
                    }
                });
    }

    /**
     * 跳转到设置界面
     */
    public void startSetting(BaseActivity activity) {
        if (PermissionMiuiOs.isMIUI()) {
            Intent intent = PermissionMiuiOs.getSettingIntent(activity);
            if (PermissionMiuiOs.isIntentAvailable(activity, intent)) {
                activity.startActivity(intent);
            }
        }
        try {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).setData(Uri.parse("package:" + activity.getPackageName()));
            activity.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            try {
                Intent intent = new Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
                activity.startActivity(intent);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

    }
}
