package com.baseandroid.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * 统一管理Log日志输出
 * <p>
 * 变更时间：2016-2-18
 *
 * @author 罗毅
 *         <p>
 *         Copyright 钱包生活
 */
public class LogUtil {
	private final static boolean logFlag = true;

    private final static String tag = "[钱包生活]";
    private final static int logLevel = Log.VERBOSE;

    public static String getFunctionName() {
        StackTraceElement[] sts = Thread.currentThread().getStackTrace();
        if (sts == null) {
            return null;
        }
        String callingClass = "";
        for (int i = 2; i < sts.length; i++) {
            @SuppressWarnings("rawtypes")
            Class clazz = sts[i].getClass();
            if (!clazz.equals(LogUtil.class)) {
                callingClass = sts[i].getClassName();
                // callingClass = callingClass.substring(callingClass
                // .lastIndexOf('.') + 1);
                break;
            }
        }
        for (StackTraceElement st : sts) {
            if (st.isNativeMethod()) {
                continue;
            }
            if (st.getClassName().equals(Thread.class.getName())) {
                continue;
            }
            if (st.getClassName().equals(callingClass.toString())) {
                continue;
            }
            return "LogUtil" + "[ " + Thread.currentThread().getName() + ": " + st.getFileName() + ":" + st.getLineNumber() + " " + st.getMethodName() + " ]";
        }
        return null;
    }

    public static void out(String str) {
        if (logFlag) {
            String name = getFunctionName();
            if (name != null) {
                System.out.println(tag + name + " - " + str);
            } else {
                System.out.println(tag + str.toString());
            }
        }
    }

    public static void i(Object str) {
        if (logFlag && logLevel <= Log.INFO) {
            String name = getFunctionName();
            if (name != null) {
                Log.i(tag, name + " - " + str);
            } else {
                Log.i(tag, str.toString());
            }

        }

    }

    public static void d(Object str) {
        if (logFlag && logLevel <= Log.DEBUG) {
            String name = getFunctionName();
            if (name != null) {
                Log.d(tag, name + " - " + str);
            } else {
                Log.d(tag, str.toString());
            }
        }

    }

    public static void v(Object str) {
        if (logFlag && logLevel <= Log.VERBOSE) {
            String name = getFunctionName();
            if (name != null) {
                Log.v(tag, name + " - " + str);
            } else {
                Log.v(tag, str.toString());
            }

        }
    }

    public static void w(Object str) {
        if (logFlag && logLevel <= Log.WARN) {
            String name = getFunctionName();
            if (name != null) {
                Log.w(tag, name + " - " + str);
            } else {
                Log.w(tag, str.toString());
            }

        }
    }

    public static void e(Object str) {
        if (logFlag && logLevel <= Log.ERROR) {
            String name = getFunctionName();
            if (name != null) {
                Log.e(tag, name + " - " + str);
            } else {
                Log.e(tag, str.toString());
            }
        }

    }

    public static void e(Exception ex) {
        if (logFlag) {
            if (logLevel <= Log.ERROR) {
                Log.e(tag, "error", ex);
            }
        }
    }

    public static void w(String log, Throwable tr) {
        if (logFlag) {
            String line = getFunctionName();
            Log.w(tag, "{Thread:" + Thread.currentThread().getName() + "}" + "[" + "LogUtil" + line + ":] " + log + "\n", tr);
        }
    }

    public static void e(String log, Throwable tr) {
        if (logFlag) {
            String line = getFunctionName();
            Log.e(tag, "{Thread:" + Thread.currentThread().getName() + "}" + "[" + "LogUtil" + line + ":] " + log + "\n", tr);
        }
    }

    /* Toast优化 */
    public static void showNDebug(Context ctx, String text) {
        if (ctx != null) {
            Toast.makeText(ctx, text, Toast.LENGTH_LONG).show();
        }
    }
}
