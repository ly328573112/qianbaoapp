package com.baseandroid.utils;

/**
 * 开发者：LuoYi
 * Time: 2018 11:35 2018/4/2 04
 */

public class SystemUtil {

    public static String getSystemPhone() {
        return android.os.Build.BRAND;
    }

    /**
     * 获取手机型号
     *
     * @return 手机型号
     */
    public static String getSystemModel() {
        return android.os.Build.MODEL;
    }

}
