package com.baseandroid.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.text.TextUtils;

import com.baseandroid.config.BizUtils;
import com.haodaibao.fanbeiapp.R;
import com.tencent.connect.share.QQShare;
import com.tencent.connect.share.QzoneShare;
import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.modelmsg.WXAppExtendObject;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXMusicObject;
import com.tencent.mm.sdk.modelmsg.WXTextObject;
import com.tencent.mm.sdk.modelmsg.WXVideoObject;
import com.tencent.mm.sdk.modelmsg.WXWebpageObject;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.open.utils.ThreadManager;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;

import java.util.ArrayList;

/**
 * 分享功能工具类 目前支持：微信聊天、微信朋友圈、QQ聊天、QQ空间
 */
public class ShareUtil {
	/*
	 * 分享内容类型
	 */
	public static final int TYPE_TEXT = 1;// 文字
	public static final int TYPE_PIC = 2;// 图片
	public static final int TYPE_MEDIA = 3;// 音频
	public static final int TYPE_VIDEO = 4;// 视频
	public static final int TYPE_WEBPAGE = 5;// 网页
	public static final int TYPE_FILEDATA = 6;// 文件
	public static final int TYPE_EMOJI = 7;// 表情
	private static ShareUtil shareUtil;
	private Context ctx;
	private IWXAPI api;
	private Tencent mTencent;

	public static ShareUtil getInstance(Context context) {
		if (shareUtil == null) {
			shareUtil = new ShareUtil(context);
		}
		return shareUtil;
	}

	private ShareUtil(Context context) {
		super();
		this.ctx = context;
		/* 初始化微信分享 */
		initWXShare();
		/* 初始化QQ分享 */
		initQQShare();
	}

	private void initWXShare() {
		api = WXAPIFactory.createWXAPI(ctx, BizUtils.WX_APP_ID, false);
		// 将本应用注册到微信
		api.registerApp(BizUtils.WX_APP_ID);
	}

	private void initQQShare() {
		mTencent = Tencent.createInstance(BizUtils.QQ_APP_ID, ctx);

	}

	public boolean isWXAppInstalled() {
		return api.isWXAppInstalled();
	}

	/**
	 * 发送短信
	 * 
	 * @param smsBody
	 */

	public void sendToSMS(String smsBody) {
		Uri smsToUri = Uri.parse("smsto:");
		Intent intent = new Intent(Intent.ACTION_SENDTO, smsToUri);
		intent.putExtra("sms_body", smsBody);
		ctx.startActivity(intent);

	}

	/**
	 * @author zhenglei
	 * @date 2015-04-22
	 * @param toWhere
	 *            分享到：微信、微信朋友圈
	 * @param contentType
	 *            1文字 2图片二进制 3音频url 4视频url 5网页url 6文件 7表情
	 * @param title
	 *            分享标题
	 * @param message
	 *            分享内容
	 * @param content
	 *            分享内容，例如：网页，即指网页的url；再如图片，即指图片的byte[]
	 */
	public void sendToWeixin(String toWhere, int contentType, String title, String message, String[] content) {
		// 若未安装微信，则提示安装
		if (!isWXAppInstalled()) {
			showInstallAppDialog();
			return;
		}
		SendMessageToWX.Req req = new SendMessageToWX.Req();
		WXMediaMessage msg = new WXMediaMessage();
		switch (contentType) {
		case TYPE_TEXT:// 文本
			WXTextObject textObj = new WXTextObject(message);

			msg.mediaObject = textObj;
			msg.title = title;
			msg.description = message;

			req.transaction = buildTransaction("text");
			req.message = msg;

			break;
		case TYPE_PIC:// 图片二进制
			break;
		case TYPE_MEDIA:// 音频url
			WXMusicObject music = new WXMusicObject();
			music.musicUrl = "http://staff2.ustc.edu.cn/~wdw/softdown/index.asp/0042515_05.ANDY.mp3";

			msg.mediaObject = music;
			msg.title = title;
			msg.description = message;

			Bitmap thumb = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.img_comm_back);
			msg.thumbData = Util.bmpToByteArray(thumb, true);

			req.transaction = buildTransaction("music");
			req.message = msg;
			break;
		case TYPE_VIDEO:
			WXVideoObject video = new WXVideoObject();
			video.videoUrl = "http://www.baidu.com";

			msg.mediaObject = video;
			msg.title = title;
			msg.description = message;
			Bitmap thumb1 = BitmapFactory.decodeResource(ctx.getResources(), R.mipmap.logo_icon);
			msg.thumbData = Util.bmpToByteArray(thumb1, true);

			req.transaction = buildTransaction("video");
			req.message = msg;

			break;
		case TYPE_WEBPAGE:
			final String imageUrl = content[0];
			String link = content[1];
			WXWebpageObject webpage = new WXWebpageObject();
			webpage.webpageUrl = link;
			msg.mediaObject = webpage;
			msg.title = title;
			msg.description = message;
			Bitmap bitmap = null;
			if (TextUtils.isEmpty(imageUrl)) {
				bitmap = BitmapFactory.decodeResource(ctx.getResources(), R.mipmap.logo_icon);
			} else {
				bitmap = ImageUtil.returnBitMap(imageUrl);
			}
			if (bitmap == null) {
				bitmap = BitmapFactory.decodeResource(ctx.getResources(), R.mipmap.logo_icon);
			}
			Bitmap Bmp1 = Bitmap.createScaledBitmap(bitmap, 180, 144, true);
			msg.thumbData = ImageUtil.compressImageToByte(Bmp1);
			LogUtil.out("上传图片大小" + msg.thumbData.length + "B");
			req.transaction = buildTransaction("webpage");
			req.message = msg;
			break;
		case TYPE_FILEDATA:
			final WXAppExtendObject appdata = new WXAppExtendObject();
			final String path = content[0];
			appdata.fileData = Util.readFromFile(path, 0, -1);
			appdata.extInfo = "this is ext info";

			msg.setThumbImage(Util.extractThumbNail(path, 150, 150, true));
			msg.title = title;
			msg.description = message;
			msg.mediaObject = appdata;

			req.transaction = buildTransaction("appdata");
			req.message = msg;
			break;
		case TYPE_EMOJI:
			break;

		}

		req.transaction = String.valueOf(System.currentTimeMillis());
		if (toWhere.equals("wxchat")) {// 微信聊天
			req.scene = SendMessageToWX.Req.WXSceneSession;
		} else if (toWhere.equals("wxfriends")) {// 微信朋友圈
			req.scene = SendMessageToWX.Req.WXSceneTimeline;
		}

		api.sendReq(req);
	}

	/* 提示先安装微信App */
	private void showInstallAppDialog() {
		Looper.prepare();
		// DialogUtil.showDialog(ctx, 0, "系统提示","请先安装微信！"
		// , null, null);
		LogUtil.showNDebug(ctx, "请安装微信客户端！");
		Looper.loop();

	}

	private String buildTransaction(final String type) {
		return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
	}

	/**
	 * qq分享
	 */
	public void sendToQQ(int contentType, String title, String message, String[] content) {
		switch (contentType) {
		case QQShare.SHARE_TO_QQ_TYPE_DEFAULT:
			String imageUrl = content[0];
			String link = content[1];
			final Bundle params = new Bundle();
			params.putInt(QQShare.SHARE_TO_QQ_KEY_TYPE, QQShare.SHARE_TO_QQ_TYPE_DEFAULT);
			params.putString(QQShare.SHARE_TO_QQ_TITLE, title);
			params.putString(QQShare.SHARE_TO_QQ_TARGET_URL, link);
			params.putString(QQShare.SHARE_TO_QQ_SUMMARY, message);
			params.putString(QQShare.SHARE_TO_QQ_IMAGE_URL, imageUrl);
			params.putString(QQShare.SHARE_TO_QQ_APP_NAME, "钱包生活");
			// params.putInt(QzoneShare.SHARE_TO_QQ_EXT_INT,QQShare.SHARE_TO_QQ_FLAG_QZONE_AUTO_OPEN);
			doShareToQQ(params);
			break;
		}

	}

	/**
	 * qq空间分享
	 */
	public void sendToQQZone(int contentType, String title, String message, String[] content) {
		switch (contentType) {
		case QzoneShare.SHARE_TO_QZONE_TYPE_IMAGE_TEXT:
			String imageUrl = content[0];
			String link = content[1];
			final Bundle params = new Bundle();
			params.putInt(QzoneShare.SHARE_TO_QZONE_KEY_TYPE, QzoneShare.SHARE_TO_QZONE_TYPE_IMAGE_TEXT);
			params.putString(QzoneShare.SHARE_TO_QQ_TITLE, title);
			params.putString(QzoneShare.SHARE_TO_QQ_SUMMARY, message);
			params.putString(QzoneShare.SHARE_TO_QQ_TARGET_URL, link);

			ArrayList<String> imageUrls = new ArrayList<String>();
			imageUrls.add(imageUrl);
			params.putStringArrayList(QzoneShare.SHARE_TO_QQ_IMAGE_URL, imageUrls);
			params.putString(QzoneShare.SHARE_TO_QQ_APP_NAME, "钱包生活");
			doShareToQQZone(params);
			break;
		}

	}

	private void doShareToQQ(final Bundle params) {
		// QQ分享要在主线程做
		ThreadManager.getMainHandler().post(new Runnable() {

			@Override
			public void run() {
				if (null != mTencent) {
					mTencent.shareToQQ((Activity) ctx, params, qqShareListener);

				}
			}
		});
	}

	private void doShareToQQZone(final Bundle params) {
		// QQ分享要在主线程做
		ThreadManager.getMainHandler().post(new Runnable() {

			@Override
			public void run() {
				if (null != mTencent) {
					mTencent.shareToQzone((Activity) ctx, params, qqShareListener);

				}
			}
		});
	}

	IUiListener qqShareListener = new IUiListener() {
		@Override
		public void onCancel() {
			LogUtil.showNDebug(ctx, "onCancel: ");

		}

		@Override
		public void onComplete(Object response) {
			// TODO Auto-generated method stub
			LogUtil.showNDebug(ctx, "onComplete: " + response.toString());

		}

		@Override
		public void onError(UiError e) {
			// TODO Auto-generated method stub
			LogUtil.showNDebug(ctx, "onError: " + e.errorMessage);
		}
	};

	public IWXAPI getApi() {
		return api;
	}

	public Tencent getmTencent() {
		return mTencent;
	}

}
