/**
 * @author: luoan
 * @date: 2015-08-14
 * @Description: 时间工具
 */
package com.baseandroid.utils;

import android.text.TextUtils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class TimeUtils {

    public static final String TIME_TYPE_1 = "M 月 d 日 H:mm";

    /**
     * @param seconds （秒）
     * @return
     */

    public static String getRemainString(int seconds) {
        DecimalFormat df = new DecimalFormat("00");
        int second = seconds % 60; // 秒
        int minute = (seconds - second) / 60 % 60; // 分钟
        int hour = ((seconds - second) / 60 - minute) / 60 % 24; // 小时
        int d = (((seconds - second) / 60 - minute) / 60 - hour) / 24;// 天
        String remainStr = "仅剩" + d + "天" + df.format(hour) + ":" + df.format(minute) + ":" + df.format(second);
        return remainStr;
    }

    /**
     * @param seconds
     * @return
     * @author:luoan
     */
    public static String getTwoRemainString(int seconds) {
        DecimalFormat df = new DecimalFormat("00");
        int second = seconds % 60; // 秒
        int minute = (seconds - second) / 60 % 60; // 分钟
        int hour = ((seconds - second) / 60 - minute) / 60 % 24; // 小时
        int d = (((seconds - second) / 60 - minute) / 60 - hour) / 24;// 天
        String remainStr = "剩" + d + "天 " + df.format(hour) + "时" + df.format(minute) + "分" + df.format(second) + "秒";
        return remainStr;
    }

    /**
     * @param
     * @return
     * @author:luoan
     */
    public static String getTwoRemainSecString(long second) {
        Long msecond = second * 1000;
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");// 初始化Formatter的转换格式
        msecond = msecond - TimeZone.getDefault().getRawOffset();
        String hms = formatter.format(msecond);
        String remainStr = "还剩" + hms;
        return remainStr;
    }

    public static String getRemainSecString(int second) {
        Long msecond = (long) (second * 1000);
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");// 初始化Formatter的转换格式
        msecond = msecond - TimeZone.getDefault().getRawOffset();
        String hms = formatter.format(msecond);
        String remainStr = hms;
        return remainStr;
    }

    /**
     * 判断是否是同一天
     */
    public static boolean isSameDay(long time1, long time2) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String ds1 = sdf.format(new Date(time1));
        String ds2 = sdf.format(new Date(time2));
        return ds1.equals(ds2);
    }

    public static String getCurrentDate(String string) {
        if (TextUtils.isEmpty(string)) {
            return "时间格式错误";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        try {
            Date date = sdf.parse(string);
            Date currentDate = new Date();
            long day = (date.getTime() + 8 * 1000 * 60 * 60) / 1000 / 60 / 60 / 24;
            long currentDay = (currentDate.getTime() + 8 * 1000 * 60 * 60) / 1000 / 60 / 60 / 24;
            if (currentDay == day + 1) {
                return "昨天";
            } else if (currentDay == day) {
                return new SimpleDateFormat("HH:mm").format(date);
            } else {
                return new SimpleDateFormat("MM-dd").format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "时间格式错误";
    }

    public static String getPayTime(String payTime) {
        if (TextUtils.isEmpty(payTime)) {
            return "时间格式错误";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        try {
            Date date = sdf.parse(payTime);
            return new SimpleDateFormat("HH:mm").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "时间格式错误";
    }

    /**
     * 按指定格式显示时间
     *
     * @param time       (毫秒)
     * @param regulation
     * @return
     */
    public static String getTime(long time, String regulation) {
        // time = System.currentTimeMillis();
        SimpleDateFormat format = new SimpleDateFormat(regulation);
        return format.format(new Date(time));

    }

    /**
     * @return 当前年月日
     */
    public static String getSimpleCurrentDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(new Date());
    }

    public static String getCurrentDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        return sdf.format(new Date());
    }

    public static String getTime(String t) {
        String[] p = {"", ".", ".", " ", ":", ":", ""};
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < t.length() / 2; i++) {
            sb.append(t.substring(i * 2, 2 * i + 2)).append(p[i]);
        }
        return sb.toString();
    }

    public static String getTimeMonth(String t) {
        StringBuffer sb = new StringBuffer();
        if (t.length() > 8) {
            sb.append(t.substring(0, 4)).append("-").append(t.substring(4, 6)).append("-").append(t.substring(6, 8));
            return sb.toString();
        }
        return t;
    }

    public static       String[] Week_DAYS = new String[]{"周日", "周一", "周二", "周三", "周四", "周五", "周六"};
    public static final int      WEEKDAYS  = 7;

    /**
     * 日期转星期
     *
     * @param date
     * @return
     */
    public static String DateToWeek(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
        Date parse = null;
        try {
            parse = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(parse);
        int dayIndex = calendar.get(Calendar.DAY_OF_WEEK);
        if (dayIndex < 1 || dayIndex > WEEKDAYS) {
            return null;
        }

        return Week_DAYS[dayIndex - 1];
    }

    /**
     * PayInfoDetailActivity页面根据返回的
     * yyyyMMddHHmmss转换为"今天/yyyy.MM.dd" + HH：mm*/
    public static String getOrderTime(String orderTime) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd");
        // 当天日期
        Calendar calendar = Calendar.getInstance();
        String ds = format.format(calendar.getTime());
        if (!TextUtils.isEmpty(orderTime)) {
            String date1 = orderTime.substring(0, 4)// 年
                    + "." + orderTime.substring(4, 6)// 月
                    + "." + orderTime.substring(6, 8);// 日
            if (TextUtils.equals(ds, date1)) {
                date1 = "今天";
            }
            String time1 = orderTime.substring(8, 10)// 时
                    + ":" + orderTime.substring(10, 12);// 分
            orderTime = date1 + " " + time1;
            return orderTime;// 下单时间
        }
        return null;
    }
}
