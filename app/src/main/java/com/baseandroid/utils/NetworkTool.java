package com.baseandroid.utils;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.IBinder;

/**
 * 
 * 检测网络
 * 
 * @author 郑磊
 * @date 2013-11-5
 * 
 */
public class NetworkTool {

	/** 网络不可用 */
	public static final int NONETWORK = 0;
	/** 是wifi连接 */
	public static final int WIFI = 1;
	/** 不是wifi连接 */
	public static final int NOWIFI = 2;

	private static Context ctx;

	private static NetStateChangeInterFace mCallback;
	private static Intent intentService;
	private static NetworkTool networkTool;

	/* ApplicationContext */
	public static NetworkTool getInstance(Context context) {
		if (networkTool == null) {
			networkTool = new NetworkTool();
		}
		ctx = context;
		return networkTool;
	}

	/**
	 * 检验网络连接 并判断是否是wifi连接
	 * @return
	 *         <li>没有网络：Network.NONETWORK;</li>
	 *         <li>wifi 连接：Network.WIFI;</li>
	 *         <li>mobile 连接：Network.NOWIFI</li>
	 */
	public int checkNetWorkType() {
		ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = cm.getActiveNetworkInfo();
		if (info != null && info.isAvailable()) {
			if (cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting()) {
				return NetworkTool.WIFI;
			} else {
				return NetworkTool.NOWIFI;
			}
		} else {
			return NetworkTool.NONETWORK;
		}

	}

	/**
	 * 获取本机wifi网络下的ip地址
	 * 
	 * @return
	 */
	public String getWifiIp() {
		WifiManager wifiManager = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
		int ip = wifiManager.getConnectionInfo().getIpAddress();
		return intToIp(ip);
	}

	private static String intToIp(int i) {
		return (i & 0xFF) + "." + ((i >> 8) & 0xFF) + "." + ((i >> 16) & 0xFF) + "." + ((i >> 24) & 0xFF);
	}

	/**
	 * 网络状态监听服务
	 */
	public class ListenNetStateService extends Service {
		private ConnectivityManager connectivityManager;
		private NetworkInfo info;

		private BroadcastReceiver mReceiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				String action = intent.getAction();
				if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
					LogUtil.d("ListenNetStateService + 网络状态已经改变");
					connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
					info = connectivityManager.getActiveNetworkInfo();
					if (info != null && info.isAvailable()) {
						String name = info.getTypeName();
						LogUtil.d("ListenNetStateService" + "当前网络名称：" + name);
						if (mCallback != null) {
							mCallback.OnNetStateChange(true, name);
						}

					} else {
						LogUtil.d("ListenNetStateService" + "网络连接中断");
						LogUtil.showNDebug(context, "网络连接中断");
						if (mCallback != null) {
							mCallback.OnNetStateChange(false, "");
						}
					}
				}
			}

		};

		@Override
		public IBinder onBind(Intent intent) {
			return null;
		}

		@Override
		public void onCreate() {
			super.onCreate();
			IntentFilter mFilter = new IntentFilter();
			mFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
			registerReceiver(mReceiver, mFilter);
		}

		@Override
		public void onDestroy() {
			super.onDestroy();
			unregisterReceiver(mReceiver);
		}

		@Override
		public int onStartCommand(Intent intent, int flags, int startId) {
			return super.onStartCommand(intent, flags, startId);
		}

	}

	public interface NetStateChangeInterFace {
		void OnNetStateChange(boolean connected, String name);
	}

	@SuppressWarnings("static-access")
	public void setOnNetStateListener(NetStateChangeInterFace callback) {
		this.mCallback = callback;
		startNetStateListen();
	}

	public void startNetStateListen() {
		/* 启动网络监测Service */
		intentService = new Intent(ctx, ListenNetStateService.class);
		ctx.startService(intentService);
	}

	public void removeNetStateListener() {
		ctx.stopService(intentService);
	}

}
