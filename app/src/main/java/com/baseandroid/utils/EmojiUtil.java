package com.baseandroid.utils;

/**
 * 开发者：LuoYi
 * Time: 2017 18:00 2017/9/25 09
 */

public class EmojiUtil {

    /**
     * * 检测是否有emoji表情 *
     *
     * @param source *
     * @return
     */
    public static String replaceEmoji(String source) {
        int len = source.length();
        for (int i = 0; i < len; i++) {
            char codePoint = source.charAt(i);
            if (!isEmojiCharacter(codePoint)) {// 是emoji字符
                source = source.replace(codePoint, " ".charAt(0));
            }
        }
        source = source.trim();
        return source;
    }

    /**
     * * 判断是否是Emoji *
     *
     * @param codePoint
     *            比较的单个字符 *
     * @return
     */
    private static boolean isEmojiCharacter(char codePoint) {
        return (codePoint == 0x0) || (codePoint == 0x9) || (codePoint == 0xA) || (codePoint == 0xD) || ((codePoint >= 0x20) && (codePoint <= 0xD7FF))
                || ((codePoint >= 0xE000) && (codePoint <= 0xFFFD)) || ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF));
    }

}
