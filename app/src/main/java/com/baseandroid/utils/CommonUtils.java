package com.baseandroid.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.amap.api.maps.model.LatLng;
import com.baseandroid.config.Api;
import com.baseandroid.config.Global;
import com.google.gson.Gson;
import com.haodaibao.fanbeiapp.BuildConfig;
import com.jayfeng.lesscode.core.LogLess;
import com.jayfeng.lesscode.core.NetworkLess;
import com.mcxiaoke.packer.helper.PackerNg;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.baseandroid.utils.CountUtil.round;
import static com.baseandroid.utils.FormatUtil.formatAmount;

public class CommonUtils {

    public static void setFullScreen(Activity activity, boolean isFullScreen) {
        if (isFullScreen) {
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            activity.getWindow()
                    .clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        } else {
            activity.getWindow()
                    .addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }

    public static AlertDialog.Builder getConfirmDialog(Context context, String title, String message, String positiveText, String negativeText, boolean cancelable, DialogInterface.OnClickListener positiveListener, DialogInterface.OnClickListener negativeListener) {
        return new AlertDialog.Builder(context).setCancelable(cancelable)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(positiveText, positiveListener)
                .setNegativeButton(negativeText, negativeListener);
    }

    public static void goSetPremission(Context context) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", context.getPackageName(), null);
        intent.setData(uri);
        context.startActivity(intent);
    }

    public static boolean isGreater(String serverVersion, String curVersion, boolean needContains) {

        if (serverVersion == null || serverVersion.trim().equals("")) {
            return false;
        }
        if (curVersion == null || curVersion.trim().equals("")) {
            return true;
        }
        if (serverVersion.contains("v") || serverVersion.contains("V")) {
            serverVersion = serverVersion.substring(1, serverVersion.length());
        }
        if (curVersion.contains("v") || curVersion.contains("V")) {
            curVersion = curVersion.substring(1, curVersion.length());
        }

        if (needContains) {
            if (TextUtils.equals(serverVersion, curVersion)) {
                return true;
            } else {
                StringTokenizer firstToken = new StringTokenizer(serverVersion, ".");
                StringTokenizer secondToken = new StringTokenizer(curVersion, ".");
                while (firstToken.hasMoreElements()) {
                    String firstStr = firstToken.nextToken();
                    String secondStr = null;
                    if (secondToken.hasMoreElements()) {
                        secondStr = secondToken.nextToken();
                    }
                    if (secondStr == null) {
                        return true;
                    }
                    int firstValue = Integer.parseInt(firstStr);
                    int secondValue = Integer.parseInt(secondStr);
                    if (firstValue > secondValue) {
                        return true;
                    } else if (firstValue < secondValue) {
                        return false;
                    }
                }
            }
        } else {
            StringTokenizer firstToken = new StringTokenizer(serverVersion, ".");
            StringTokenizer secondToken = new StringTokenizer(curVersion, ".");
            while (firstToken.hasMoreElements()) {
                String firstStr = firstToken.nextToken();
                String secondStr = null;
                if (secondToken.hasMoreElements()) {
                    secondStr = secondToken.nextToken();
                }
                if (secondStr == null) {
                    return true;
                }
                int firstValue = Integer.parseInt(firstStr);
                int secondValue = Integer.parseInt(secondStr);
                if (firstValue > secondValue) {
                    return true;
                } else if (firstValue < secondValue) {
                    return false;
                }
            }
        }

        return false;
    }

    public static boolean isMobile(String str) {
        Pattern p = null;
        Matcher m = null;
        boolean b = false;
        //p = Pattern.compile("^[1][3,4,5,7,8][0-9]{9}$"); // 验证手机号
        p = Pattern.compile("^[1]\\d{10}$"); // 验证手机号
        m = p.matcher(str);
        b = m.matches();
        return b;
    }

    //判断距上次点击事件是否在一秒内
    private static long lastClickTime;

    public static boolean isFastDoubleClick() {
        long time = System.currentTimeMillis();
        long timeD = time - lastClickTime;
        if (0 < timeD && timeD < 300) {
            return true;
        }
        lastClickTime = time;
        return false;
    }

    public static String getImageUrl(String url) {
        if (TextUtils.isEmpty(url)) {
            return "";
        }
        return Api.sImageUrl + url;
    }

    /**
     * 返回自定义Custom-Agent，格式如下：
     * 系统名;系统版本名;系统版本号;应用版本名;应用版本号;网络类型;手机型号;IMEI;MAC
     *
     * @return Android;4.4.4;19;V1.0-Alpha-201506121401;2;WIFI_FAST;Xiaomi;
     */
    public static String formatUserAgent() {
        StringBuilder result = new StringBuilder();
        result.append("Android");
        result.append(";");
        result.append(Build.VERSION.RELEASE);
        result.append(";");
        result.append(Build.VERSION.SDK_INT);
        result.append(";");
        result.append(BuildConfig.VERSION_NAME);
        result.append(";");
        result.append(BuildConfig.VERSION_CODE);
        result.append(";");
        result.append(NetworkLess.$type().name());
        result.append(";");
        result.append(Build.MODEL);
        result.append(";");
        result.append(CommonUtils.getChannel());
        LogLess.$d("custom user agent:" + result.toString());
        return result.toString().replaceAll("\\s*", "");
    }

    public static String getChannel() {
        String chanel = PackerNg.getChannel(Global.getContext());
        return TextUtils.isEmpty(chanel) ? "official" : chanel;
    }

    public static <T> boolean isSameObject(T objectOne, T objectTwo) {
        Gson gson = new Gson();
        String objectonestr = gson.toJson(objectOne);
        String objecttwostr = gson.toJson(objectTwo);
        if (objectonestr.equals(objecttwostr)) {
            return true;
        }
        return false;
    }

    /**
     * 距离格式转换 小于1分钟，显示x秒 大于1分钟小于1小时，显示x分钟 大于1小时，显示x小时x分钟
     */
    public static String formatTime(String second) {
        double time1 = round(second, 0, BigDecimal.ROUND_HALF_UP);
        if (time1 < 60) {// 显示x秒
            return time1 + "秒";
        } else if (time1 >= 60 && time1 < 3600) {
            int minute = (int) (time1 / 60);
            return minute + "分钟";
        } else {
            int hour = (int) (time1 / 3600);
            int minute = (int) ((time1 - hour * 3600) / 60);
            return hour + "小时" + minute + "分钟";
        }

    }

    public static String formatDistance(String distance) {
        double dis = round(distance, 2, BigDecimal.ROUND_HALF_UP);
        if (dis == -1) {
            return "";
        }
        if (dis < 1000) {
            return formatAmount(0, String.valueOf(dis)) + "m";
        } else {
            return formatAmount(1, String.valueOf(dis / 1000)) + "km";
        }
    }

    /**
     * 数字格式转换 小于10的数字前补0
     */
    public static String formatNum(int num) {
        if (num < 10) {// 显示x秒
            return "0" + num;
        } else {
            return "" + num;
        }

    }

    /**
     * 计算两点距离
     *
     * @param start
     * @param end
     * @return
     */
    public static double getDistance(LatLng start, LatLng end) {

        double lon1 = (Math.PI / 180) * start.longitude;
        double lon2 = (Math.PI / 180) * end.longitude;
        double lat1 = (Math.PI / 180) * start.latitude;
        double lat2 = (Math.PI / 180) * end.latitude;

        // 地球半径
        double R = 6371;

        // 两点间距离 km，如果想要米的话，结果*1000就可以了
        double d = Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math
                .cos(lon2 - lon1)) * R;

        return d * 1000;
    }

    /**
     * 判断是否存在虚拟按键导航栏
     *
     * @return 是否存在
     */
    public static boolean hasNavigationBar(Activity activity) {
        boolean hasNavigationBar = false;
        Resources rs = activity.getResources();
        int id = rs.getIdentifier("config_showNavigationBar", "bool", "android");
        if (id > 0) {
            hasNavigationBar = rs.getBoolean(id);
        }
        try {
            Class<?> systemPropertiesClass = Class.forName("android.os.SystemProperties");
            Method m = systemPropertiesClass.getMethod("get", String.class);
            String navBarOverride = (String) m.invoke(systemPropertiesClass, "qemu.hw.mainkeys");
            if ("1".equals(navBarOverride)) {
                hasNavigationBar = false;
            } else if ("0".equals(navBarOverride)) {
                hasNavigationBar = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hasNavigationBar;
    }

    /**
     * 隐藏虚拟按键导航栏
     *
     * @param activity activity
     */
    public static void hideNavigationBar(Activity activity) {
        if (activity != null) {
            if (Build.VERSION.SDK_INT < 19 || !hasNavigationBar(activity)) {
                //一定要判断是否存在按键，否则在没有按键的手机调用会影响别的功能。
                return;
            } else {
                Window window = activity.getWindow();
                WindowManager.LayoutParams params = window.getAttributes();
                params.systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
                window.setAttributes(params);
            }
        }
    }
}
