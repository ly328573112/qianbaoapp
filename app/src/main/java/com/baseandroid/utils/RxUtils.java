package com.baseandroid.utils;

import com.baseandroid.base.BaseFragment;
import com.trello.rxlifecycle2.LifecycleProvider;
import com.trello.rxlifecycle2.LifecycleTransformer;
import com.trello.rxlifecycle2.android.ActivityEvent;
import com.trello.rxlifecycle2.android.FragmentEvent;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class RxUtils {
    public static <T> ObservableTransformer<T, T> applySchedulersLifeCycle(final LifecycleProvider lifecycleProvider) {
        return new ObservableTransformer<T, T>() {
            @Override
            public Observable<T> apply(Observable<T> observable) {
                return observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .compose(RxUtils.<T>bindToLifecycle(lifecycleProvider));
            }
        };
    }

    public static <T> LifecycleTransformer<T> bindToLifecycle(LifecycleProvider lifecycleProvider) {
        if (lifecycleProvider instanceof BaseFragment) {
            return lifecycleProvider.bindUntilEvent(FragmentEvent.DESTROY);
        }
        return lifecycleProvider.bindUntilEvent((ActivityEvent.DESTROY));
    }

}
