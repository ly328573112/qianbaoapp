package com.baseandroid.okhttp.callback;

import java.lang.reflect.Type;

public abstract class GsonCallback<T> implements HttpCallback<T> {
    private Type mType;

    public GsonCallback(final Type type) {
        this.mType = type;
    }

    @Override
    public void onRequestProgress(long bytesWritten, long contentLength, long networkSpeed) {

    }

    @Override
    public void onResponseProgress(long bytesRead, long contentLength, boolean done) {

    }

    public Type getType() {
        return mType;
    }

    public void setType(Type type) {
        mType = type;
    }
}
