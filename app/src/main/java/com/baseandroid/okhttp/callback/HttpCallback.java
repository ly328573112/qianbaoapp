package com.baseandroid.okhttp.callback;


import com.baseandroid.okhttp.RequestProgressListener;
import com.baseandroid.okhttp.ResponProgressListener;

public interface HttpCallback<T> extends RequestProgressListener, ResponProgressListener {

    void handleResponse(T response);

    boolean handleException(Throwable throwable);
}
