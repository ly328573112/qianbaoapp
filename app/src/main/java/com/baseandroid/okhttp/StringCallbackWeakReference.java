package com.baseandroid.okhttp;

import android.app.Activity;
import android.net.ParseException;
import android.support.annotation.UiThread;

import com.baseandroid.okhttp.callback.StringCallback;
import com.baseandroid.utils.ToastUtils;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParseException;

import org.json.JSONException;

import java.lang.ref.WeakReference;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

public abstract class StringCallbackWeakReference extends StringCallback {

    private WeakReference<Activity> mActivityWeakReference;

    public StringCallbackWeakReference(Activity activity) {
        mActivityWeakReference = new WeakReference<Activity>(activity);
    }

    @UiThread
    public Activity getActivityWeakReference() {
        return mActivityWeakReference == null ? null : mActivityWeakReference.get();
    }


    @Override
    public void handleResponse(String response) {
        if (getActivityWeakReference() != null) {
            handleHttpRespone(response);
        }
    }

    @Override
    public boolean handleException(Throwable throwable) {
        if (getActivityWeakReference() != null) {

            String msg = "";
            if (throwable instanceof ConnectException) {
                msg = "网络不可用";
            } else if (throwable instanceof UnknownHostException) {
                // msg = "未知主机错误";
                msg = "网络不可用";
            } else if (throwable instanceof SocketTimeoutException) {
                msg = "请求网络超时";
            } else if (throwable instanceof OkHttpException) {
                OkHttpException httpException = (OkHttpException) throwable;
                msg = convertStatusCode(httpException);
            } else if (throwable instanceof JsonParseException || throwable instanceof ParseException || throwable instanceof JSONException || throwable instanceof JsonIOException) {
                msg = "数据解析错误";
            }

            if (!msg.isEmpty()) {
                ToastUtils.showShortToastSafe(msg);
            }

        }
        return false;
    }

    private String convertStatusCode(OkHttpException httpException) {
        String msg;
        if (httpException.code() == 500) {
            msg = "服务器发生错误";
        } else if (httpException.code() == 404) {
            msg = "请求地址不存在";
        } else if (httpException.code() == 403) {
            msg = "请求被服务器拒绝";
        } else if (httpException.code() == 307) {
            msg = "请求被重定向到其他页面";
        } else {
            msg = httpException.message();
        }
        return msg;
    }

    public abstract void handleHttpRespone(String response);
}
