package com.baseandroid.okhttp;

/*
There is no longer a global singleton connection pool. In OkHttp 2.x, all OkHttpClient
instances shared a common connection pool by default. In OkHttp 3.x, each new
OkHttpClient gets its own private connection pool. Applications should avoid creating
many connection pools as doing so prevents connection reuse.

OkHttpClient now implements the new Call.Factory interface.

OkHttp now does cookies. This new cookie model follows the latest RFC.

Form and Multipart bodies are now modeled. upgraded FormBody and FormBody.Builder
upgraded MultipartBody, MultipartBody.Part, and MultipartBody.Builder.

Canceling batches of calls is now the application's responsibility.
*/

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;


import com.baseandroid.config.Api;
import com.baseandroid.config.Global;
import com.baseandroid.retrofit.OkHttpClientManager;
import com.baseandroid.utils.AndroidUtils;
import com.baseandroid.utils.DeviceUuid;
import com.baseandroid.utils.EncryptUtil;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;

import com.haodaibao.fanbeiapp.BuildConfig;
import com.qianbaocard.coresdk.SignUtil;

import java.io.File;
import java.io.IOException;

import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;

public final class OkHttpUtils {

    static class SingletonHolder {
        static OkHttpUtils INSTANCE = new OkHttpUtils();
    }

    public static OkHttpUtils getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private OkHttpClient mHttpClient = null;
    private Handler mDelivery;

    public void init(Context context) {

        mHttpClient = OkHttpClientManager.getOkHttpClient();

        mDelivery = new Handler(Looper.getMainLooper());
    }

    public static class UserAgentInterceptor implements Interceptor {
        private static final String USER_AGENT_HEADER_NAME = "User-Agent";
        private final String userAgentHeaderValue;

        public UserAgentInterceptor(String userAgentHeaderValue) {
            this.userAgentHeaderValue = userAgentHeaderValue;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            final Request originalRequest = chain.request();
            final Request requestWithUserAgent = originalRequest.newBuilder()
                    .removeHeader(USER_AGENT_HEADER_NAME)
                    .addHeader(USER_AGENT_HEADER_NAME, userAgentHeaderValue)
                    .build();
            return chain.proceed(requestWithUserAgent);
        }
    }


    public OkHttpClient getHttpClient() {
        return mHttpClient;
    }

    public static OkHttpRequest head(final String url) {
        return new OkHttpRequest(RequestMethod.HEAD, url);
    }

    public static OkHttpRequest get(final String url) {
        return new OkHttpRequest(RequestMethod.GET, url);
    }

    public static OkHttpRequest delete(final String url) {
        return new OkHttpRequest(RequestMethod.DELETE, url);
    }

    public static OkHttpRequest post(final String url) {
        return new OkHttpRequest(RequestMethod.POST, url);
    }

    public static OkHttpRequest put(final String url) {
        return new OkHttpRequest(RequestMethod.PUT, url);
    }

    public Handler getDelivery() {
        return mDelivery;
    }

    public void cancelTag(Object tag) {
        for (Call call : mHttpClient.dispatcher().queuedCalls()) {
            if (tag.equals(call.request().tag())) {
                call.cancel();
            }
        }

        for (Call call : mHttpClient.dispatcher().runningCalls()) {
            if (tag.equals(call.request().tag())) {
                call.cancel();
            }
        }
    }
}
