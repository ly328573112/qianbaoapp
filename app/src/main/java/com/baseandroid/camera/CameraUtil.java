package com.baseandroid.camera;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;

import com.android.photocameralib.media.SelectImageActivity;
import com.android.photocameralib.media.config.SelectOptions;
import com.android.photocameralib.media.crop.CropActivity;
import com.jayfeng.lesscode.core.DisplayLess;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

import static android.app.Activity.RESULT_CANCELED;

/**
 * Created by zhenglei on 2017/4/1.
 * 调用系统相机、图库，
 * 兼容7.0
 */

public class CameraUtil {
    private File file;
    public static final int REQUEST_CAMERA = 111;
    public static final int REQUEST_PICTURE = 222;
    public static final int REQUEST_RESIZE = 333;
    public static final int REQUEST_CROP = 0x04;
    private Uri imageUri;

    private Activity activity;
    private boolean needResize;
    private ResultCallback callback;

    ArrayList<File> fileList = new ArrayList<>();


    public CameraUtil(Activity activity) {
        this.activity = activity;
    }

    /**
     * 调用相机拍照
     */
    public void openCamera(boolean needResize, ResultCallback callback, File saveFile) {
        this.file = saveFile;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {  //针对Android7.0，需要通过FileProvider封装过的路径，提供给外部调用
            imageUri = FileProvider.getUriForFile(activity, "com.haodaibao.fanbeiapp.fileprovider", file);//通过FileProvider创建一个content类型的Uri，进行封装
        } else { //7.0以下，如果直接拿到相机返回的intent值，拿到的则是拍照的原图大小，很容易发生OOM，所以我们同样将返回的地址，保存到指定路径，返回到Activity时，去指定路径获取，压缩图片
            imageUri = Uri.fromFile(file);
        }
        this.needResize = needResize;
        this.callback = callback;
        setCameraPermission();
    }

    /**
     * 从相册获取
     *
     * @param needResize 是否裁剪
     * @param selectSize 选择图片数量
     * @param callback   选择结果
     * @param saveFile   图片存放路径
     */
    public void openPicture(final boolean needResize, int selectSize, final ResultCallback callback, File saveFile) {
        this.file = saveFile;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {  //针对Android7.0，需要通过FileProvider封装过的路径，提供给外部调用
            imageUri = FileProvider.getUriForFile(activity, "com.haodaibao.fanbeiapp.fileprovider", file);//通过FileProvider创建一个content类型的Uri，进行封装
        } else { //7.0以下，如果直接拿到相机返回的intent值，拿到的则是拍照的原图大小，很容易发生OOM，所以我们同样将返回的地址，保存到指定路径，返回到Activity时，去指定路径获取，压缩图片
            imageUri = Uri.fromFile(file);
        }
        this.needResize = needResize;
        this.callback = callback;
        SelectImageActivity.show(activity, new SelectOptions.Builder()
                .setHasCam(true)
                .setSelectCount(selectSize)
                .setCallback(new SelectOptions.Callback() {
                    @Override
                    public void doSelected(String[] images) {
                        if (needResize) {
                            resizeImage(activity, images);
                        } else {
                            fileList.clear();
                            for (String imgpath : images) {
//                                File fileUri = new File(getRealPathFromURI(imageUri));
                                File fileUri = new File(imgpath);
                                fileList.add(fileUri);
                            }
                            callback.getResult(fileList);
                        }
                    }
                })
                .build());
    }

    public void setCameraPermission() {
        boolean isPermissionsGranted = new RxPermissions(activity).isGranted(Manifest.permission.CAMERA);
        if (!isPermissionsGranted) {
            new RxPermissions(activity).request(Manifest.permission.CAMERA)
                    .subscribe(new Observer<Boolean>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Boolean granted) {
                            if (granted) {
                                Intent intent = new Intent();
                                intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);//设置Action为拍照
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);//将拍取的照片保存到指定URI
                                activity.startActivityForResult(intent, REQUEST_CAMERA);//启动拍照
                            } else {
                                com.android.photocameralib.media.utils.Util.getConfirmDialog(activity, "", "摄像头启动失败, 请尝试在手机应用权限管理中打开权限", "去设置", "取消", false, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        new SelectImageActivity().goSetPremission();
                                    }
                                }, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                }).show();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else {
            Intent intent = new Intent();
            intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);//设置Action为拍照
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);//将拍取的照片保存到指定URI
            activity.startActivityForResult(intent, REQUEST_CAMERA);//启动拍照
        }
    }


    public interface ResultCallback {
        void getResult(List<File> saveFile);

        void onError(int code, String message);
    }

    public void onHandleActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CROP:
                if (data == null) {
                    return;
                }
                fileList.clear();
                String cropPath = data.getStringExtra("crop_path");
                file = new File(cropPath);
                fileList.add(file);
                callback.getResult(fileList);
                break;

            case REQUEST_CAMERA:
                if (resultCode == RESULT_CANCELED) {
                    return;
                }
                if (needResize) {
                    resizeImage(activity, new String[]{imageUri.toString()});
                } else {
                    fileList.clear();
                    fileList.add(file);
                    callback.getResult(fileList);
                }

                break;
            case REQUEST_PICTURE:
                if (data != null) {
                    imageUri = data.getData();
                    if (needResize) {
                        resizeImage(activity, new String[]{imageUri.toString()});
                    } else {
                        fileList.clear();
                        file = new File(getRealPathFromURI(imageUri));
                        fileList.add(file);
                        callback.getResult(fileList);
                    }
                }


                break;

            case REQUEST_RESIZE:
                if (data == null) {
                    callback.onError(1, "取消");
                    return;
                }
                fileList.clear();
                Bundle extras = data.getExtras();
                if (extras == null) {
                    callback.onError(1, "取消");
                    return;
                }
                Bitmap bitmap = extras.getParcelable("data");
                try {
                    FileOutputStream fos = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                    bitmap = null;
                    fos.flush();
                    fos.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                fileList.add(file);
                callback.getResult(fileList);
                break;
            default:

                break;
        }
    }

    /**
     * 裁剪图片(系统)
     */
//    private void resizeImage(Activity activity, Uri uri) {
//        Intent intent = new Intent("com.android.camera.action.CROP");
//        intent.setDataAndType(uri, "image/*");
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        }
//        intent.putExtra("crop", "true");
//        intent.putExtra("aspectX", 1);
//        intent.putExtra("aspectY", 1);
//        intent.putExtra("outputX", 150);
//        intent.putExtra("outputY", 150);
//        intent.putExtra("return-data", true);
//        LogUtil.e("第三步" + "第三步,开始裁剪");
//        // 打开裁剪的activity,并且获取到裁剪图片(在第二步的RESIZE_REQUEST_CODE请求码中处理)
//        activity.startActivityForResult(intent, REQUEST_RESIZE);
//    }


    /**
     * 裁剪图片
     */
    private void resizeImage(Activity activity, String[] uri) {
        if (uri == null || uri.length == 0) {
            return;
        }
        CropActivity.show(activity, new SelectOptions.Builder()
                .setSelectedImages(uri)
                .setCrop(DisplayLess.$dp2px(256), DisplayLess.$dp2px(256))
                .build());
    }

    private String getRealPathFromURI(Uri contentUri) {
        String res = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] proj = {MediaStore.Images.Media.DATA};
            Cursor cursor = activity.getContentResolver().query(contentUri, proj, null, null, null);
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                res = cursor.getString(column_index);
            }
            cursor.close();
        } else {
            String[] proj = {MediaStore.Images.Media.DATA};
            Cursor cursor = activity.managedQuery(contentUri, proj, null, null, null);
            int actual_image_column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            res = cursor.getString(actual_image_column_index);
            cursor.close();
        }
        return res;
    }


    public static boolean isCameraUseable() {
        boolean canUse = true;
        Camera mCamera = null;
        try {
            mCamera = Camera.open();
            // setParameters 是针对魅族MX5。MX5通过Camera.open()拿到的Camera对象不为null
            Camera.Parameters mParameters = mCamera.getParameters();
            mCamera.setParameters(mParameters);
        } catch (Exception e) {
            canUse = false;
        }
        if (mCamera != null) {
            mCamera.release();
        }
        return canUse;
    }

}