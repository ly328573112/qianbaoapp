package com.baseandroid.download.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class MySQLiteOpenHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "download.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_NAME = "threadinfo";
    public static final String COLUMN_NAME_ID = "id";
    public static final String COLUMN_NAME_URI = "uri";
    public static final String COLUMN_NAME_TAG = "tag";
    public static final String COLUMN_NAME_STARTOFFSET = "startoffset";
    public static final String COLUMN_NAME_ENDOFFSET = "endoffset";
    public static final String COLUMN_NAME_FINISHED = "finished";

    private static final String TYPE_TEXT = " TEXT";
    private static final String TYPE_IONG = " INTEGER";
    private static final String COMMA = ", ";

    private static final String SQL_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" + COLUMN_NAME_ID + COMMA + COLUMN_NAME_URI + TYPE_TEXT + COMMA + COLUMN_NAME_TAG + TYPE_TEXT + COMMA + COLUMN_NAME_STARTOFFSET + TYPE_IONG + COMMA + COLUMN_NAME_ENDOFFSET + TYPE_IONG + COMMA + COLUMN_NAME_FINISHED + TYPE_IONG + COMMA + "PRIMARY KEY(id,tag)" + " )";
    private static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + SQL_CREATE_TABLE;

    private SQLiteDatabase mSqliteDB;

    public MySQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DROP_TABLE);
        onCreate(sqLiteDatabase);
    }

    public List<ThreadInfo> qureyThreadinfoByTag(String tag) {
        List<ThreadInfo> threadInfoList = new ArrayList<>();
        Cursor cursor = mSqliteDB.query(TABLE_NAME, null, COLUMN_NAME_TAG + " =?", new String[]{tag}, null, null, null);
        while (cursor.moveToNext()) {
            ThreadInfo threadInfo = new ThreadInfo();
            threadInfo.setId(cursor.getLong(cursor.getColumnIndex(COLUMN_NAME_ID)));
            threadInfo.setUri(cursor.getString(cursor.getColumnIndex(COLUMN_NAME_URI)));
            threadInfo.setTag(cursor.getString(cursor.getColumnIndex(COLUMN_NAME_TAG)));
            threadInfo.setStartoffset(cursor.getLong(cursor.getColumnIndex(COLUMN_NAME_STARTOFFSET)));
            threadInfo.setEndoffset(cursor.getLong(cursor.getColumnIndex(COLUMN_NAME_ENDOFFSET)));
            threadInfo.setFinished(cursor.getLong(cursor.getColumnIndex(COLUMN_NAME_FINISHED)));
            threadInfoList.add(threadInfo);
        }
        if (cursor != null) {
            cursor.close();
        }
        return threadInfoList;
    }

    public long insertThreadinfo(ThreadInfo threadInfo) {
        long result = 0;
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_ID, threadInfo.getId());
        values.put(COLUMN_NAME_URI, threadInfo.getUri());
        values.put(COLUMN_NAME_TAG, threadInfo.getTag());
        values.put(COLUMN_NAME_STARTOFFSET, threadInfo.getStartoffset());
        values.put(COLUMN_NAME_ENDOFFSET, threadInfo.getEndoffset());
        values.put(COLUMN_NAME_FINISHED, threadInfo.getFinished());
        result = mSqliteDB.insert(TABLE_NAME, null, values);
        return result;
    }

    public long updateThreadinfo(ThreadInfo threadInfo) {
        long result = 0;
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_ID, threadInfo.getId());
        values.put(COLUMN_NAME_URI, threadInfo.getUri());
        values.put(COLUMN_NAME_TAG, threadInfo.getTag());
        values.put(COLUMN_NAME_STARTOFFSET, threadInfo.getStartoffset());
        values.put(COLUMN_NAME_ENDOFFSET, threadInfo.getEndoffset());
        values.put(COLUMN_NAME_FINISHED, threadInfo.getFinished());
        result = mSqliteDB.update(TABLE_NAME, values, COLUMN_NAME_TAG + " =?", new String[]{threadInfo.getTag()});
        return result;
    }

    public long replaceThreadinfo(ThreadInfo threadInfo) {
        long result = 0;
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_ID, threadInfo.getId());
        values.put(COLUMN_NAME_URI, threadInfo.getUri());
        values.put(COLUMN_NAME_TAG, threadInfo.getTag());
        values.put(COLUMN_NAME_STARTOFFSET, threadInfo.getStartoffset());
        values.put(COLUMN_NAME_ENDOFFSET, threadInfo.getEndoffset());
        values.put(COLUMN_NAME_FINISHED, threadInfo.getFinished());
        result = mSqliteDB.replace(TABLE_NAME, null, values);
        return result;
    }

    public long deleteThreadinfoByTag(String tag) {
        long result = 0;
        result = mSqliteDB.delete(TABLE_NAME, COLUMN_NAME_TAG + " =?", new String[]{tag});
        return result;
    }

    public SQLiteDatabase initSqliteDB() {
        mSqliteDB = getWritableDatabase();
        return mSqliteDB;
    }

}
