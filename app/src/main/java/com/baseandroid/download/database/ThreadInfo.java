package com.baseandroid.download.database;

public class ThreadInfo {
    long id;
    String uri;
    String tag;
    long startoffset;
    long endoffset;
    long finished;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public long getStartoffset() {
        return startoffset;
    }

    public void setStartoffset(long startoffset) {
        this.startoffset = startoffset;
    }

    public long getEndoffset() {
        return endoffset;
    }

    public void setEndoffset(long endoffset) {
        this.endoffset = endoffset;
    }

    public long getFinished() {
        return finished;
    }

    public void setFinished(long finished) {
        this.finished = finished;
    }
}
