package com.baseandroid.download.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileUtils {

    private static final String MSG_NOT_A_DIRECTORY = "Not a directory: ";
    private static final String MSG_CANT_CREATE = "Can't create: ";
    private static final String MSG_NOT_FOUND = "Not found: ";
    private static final String MSG_NOT_A_FILE = "Not a file: ";
    private static final String MSG_UNABLE_TO_DELETE = "Unable to delete: ";

    /**
     * Returns <code>true</code> if file exists.
     */
    public static boolean isExistingFile(File file) {
        if (file == null) {
            return false;
        }
        return file.exists() && file.isFile();
    }

    /**
     * Returns <code>true</code> if folder exists.
     */
    public static boolean isExistingFolder(File folder) {
        if (folder == null) {
            return false;
        }
        return folder.exists() && folder.isDirectory();
    }

    /**
     * Creates all folders at once.
     * @see #mkdirs(File)
     */
    public static void mkdirs(String dirs) throws IOException {
        mkdirs(new File(dirs));
    }

    /**
     * Creates all folders at once.
     */
    public static void mkdirs(File dirs) throws IOException {
        if (dirs.exists()) {
            if (!dirs.isDirectory()) {
                throw new IOException(MSG_NOT_A_DIRECTORY + dirs);
            }
            return;
        }
        if (!dirs.mkdirs()) {
            throw new IOException(MSG_CANT_CREATE + dirs);
        }
    }

    /**
     * Creates single folder.
     * @see #mkdir(File)
     */
    public static void mkdir(String dir) throws IOException {
        mkdir(new File(dir));
    }

    /**
     * Creates single folders.
     */
    public static void mkdir(File dir) throws IOException {
        if (dir.exists()) {
            if (!dir.isDirectory()) {
                throw new IOException(MSG_NOT_A_DIRECTORY + dir);
            }
            return;
        }
        if (!dir.mkdir()) {
            throw new IOException(MSG_CANT_CREATE + dir);
        }
    }

    /*
   * smart delete
   */
    public static void delete(String dest) throws IOException {
        delete(new File(dest));
    }

    /**
     * Smart delete of destination file or directory.
     */
    public static void delete(File dest) throws IOException {
        if (dest.isDirectory()) {
            deleteDir(dest);
            return;
        }
        deleteFile(dest);
    }

    public static void deleteFile(String dest) throws IOException {
        deleteFile(new File(dest));
    }

    public static void deleteFile(File dest) throws IOException {
        if (!dest.isFile()) {
            throw new IOException(MSG_NOT_A_FILE + dest);
        }
        if (!dest.delete()) {
            throw new IOException(MSG_UNABLE_TO_DELETE + dest);
        }
    }

    public static void deleteDir(String dest) throws IOException {
        deleteDir(new File(dest));
    }

    public static void deleteDir(File dest) throws IOException {
        cleanDir(dest);
        if (!dest.delete()) {
            throw new IOException(MSG_UNABLE_TO_DELETE + dest);
        }
    }

    public static void cleanDir(String dest) throws IOException {
        cleanDir(new File(dest));
    }

    /**
     * Cleans a directory without deleting it.
     */
    public static void cleanDir(File dest) throws IOException {
        if (!dest.exists()) {
            throw new FileNotFoundException(MSG_NOT_FOUND + dest);
        }

        if (!dest.isDirectory()) {
            throw new IOException(MSG_NOT_A_DIRECTORY + dest);
        }

        File[] files = dest.listFiles();
        if (files == null) {
            throw new IOException("Failed to list contents of: " + dest);
        }

        IOException exception = null;
        for (File file : files) {
            try {
                if (file.isDirectory()) {
                    deleteDir(file);
                } else {
                    file.delete();
                }
            } catch (IOException ioex) {
                exception = ioex;
                continue;
            }
        }

        if (exception != null) {
            throw exception;
        }
    }

}
