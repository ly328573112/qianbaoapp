package com.baseandroid.download.utils;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;

/**
 * Created by hdb on 2018/3/19.
 */

public class StreamUtil {
    /**
     * Closes silently the closable object. If it is <code>FLushable</code>, it
     * will be flushed first. No exception will be thrown if an I/O error occurs.
     */
    public static void close(Closeable closeable) {
        if (closeable != null) {
            if (closeable instanceof Flushable) {
                try {
                    ((Flushable) closeable).flush();
                } catch (IOException ignored) {
                }
            }

            try {
                closeable.close();
            } catch (IOException ignored) {
            }
        }
    }
}
