package com.baseandroid.download.downinterface;


import com.baseandroid.download.DownloadException;

public interface OnDownloadListener {

    void onDownloadProgress(long finished, long length);

    void onDownloadPaused();

    void onDownloadCanceled();

    void onDownloadCompleted();

    void onDownloadFailed(DownloadException de);
}