package com.baseandroid.download.downinterface;


import com.baseandroid.download.DownloadException;

public interface OnConnectListener {

    void onConnecting();

    void onConnected(long time, long length, boolean isAcceptRanges);

    void onConnectCanceled();

    void onConnectFailed(DownloadException de);
}