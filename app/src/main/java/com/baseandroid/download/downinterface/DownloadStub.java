package com.baseandroid.download.downinterface;

public interface DownloadStub {
    void start();

    void pause();

    void cancel();

    boolean isRunning();
}
