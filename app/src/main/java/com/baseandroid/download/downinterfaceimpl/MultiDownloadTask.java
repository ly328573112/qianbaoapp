package com.baseandroid.download.downinterfaceimpl;

import com.baseandroid.download.DownloadInfo;
import com.baseandroid.download.database.MySQLiteOpenHelper;
import com.baseandroid.download.database.ThreadInfo;
import com.baseandroid.download.downinterface.OnDownloadListener;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

public class MultiDownloadTask extends DownloadTaskImpl {

    private MySQLiteOpenHelper mDatabaseHelper;

    public MultiDownloadTask(DownloadInfo downloadInfo, ThreadInfo threadInfo, MySQLiteOpenHelper databaseHelper, OnDownloadListener listener) {
        super(downloadInfo, threadInfo, listener);
        this.mDatabaseHelper = databaseHelper;
    }

    @Override
    protected void insertIntoDB(ThreadInfo info) {
        insertOrUpdateDb(info);
    }

    @Override
    protected int getResponseCode() {
        return HttpURLConnection.HTTP_PARTIAL;
    }

    @Override
    protected void updateDB(ThreadInfo info) {
        insertOrUpdateDb(info);
    }

    @Override
    protected Map<String, String> getHttpHeaders(ThreadInfo info) {
        Map<String, String> headers = new HashMap<String, String>();
        long start = info.getStartoffset() + info.getFinished();
        long end = info.getEndoffset();
        headers.put("Range", "bytes=" + start + "-" + end);
        return headers;
    }

    @Override
    protected RandomAccessFile getFile(File dir, String name, long offset) throws
            IOException {
        File file = new File(dir, name);
        RandomAccessFile raf = new RandomAccessFile(file, "rwd");
        raf.seek(offset);
        return raf;
    }

    protected void insertOrUpdateDb(final ThreadInfo info) {
        mDatabaseHelper.replaceThreadinfo(info);
    }
}