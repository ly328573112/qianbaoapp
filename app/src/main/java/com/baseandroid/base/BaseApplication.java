package com.baseandroid.base;

import android.content.Context;
import android.os.StrictMode;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.baseandroid.config.Global;
import com.baseandroid.okhttp.OkHttpUtils;
import com.baseandroid.utils.AndroidUtils;
import com.facebook.stetho.Stetho;
import com.haodaibao.fanbeiapp.BuildConfig;
import com.jayfeng.lesscode.core.$;
import com.orhanobut.hawk.Hawk;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

public class BaseApplication extends MultiDexApplication {

    private static RefWatcher mRefWatcher;
    private static BaseAppManager mAppManager;

    @Override
    public void onCreate() {
        super.onCreate();

        Global.setContext(this);
        AndroidUtils.init(this);
        OkHttpUtils.getInstance().init(this);
        $.getInstance().context(this);

        Hawk.init(this).build();


        if (BuildConfig.DEBUG) {
            mRefWatcher = LeakCanary.install(this);

            Stetho.initializeWithDefaults(this);

            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork()
                    .penaltyLog()
                    .build());

            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .build());
        }
    }

    public static RefWatcher getRefWatcher(Context context) {
        // LeakCanary: Detect all memory leaks!
        // LeakCanary.install() returns a pre configured RefWatcher. It also
        // installs an ActivityRefWatcher that automatically detects if an activity is
        // leaking after Activity.onDestroy() has been called.
        return mRefWatcher;
    }

    public static BaseAppManager getAppManager() {
        if (mAppManager == null) {
            synchronized (BaseApplication.class) {
                mAppManager = BaseAppManager.getAppManager();
            }
        }
        return mAppManager;
    }

    @Override
    public void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        // you must install multiDex whatever tinker is installed!
        MultiDex.install(base);
    }

    public static void exitApp() {
        mAppManager.AppExit();
    }

}
