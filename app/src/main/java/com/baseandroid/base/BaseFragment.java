package com.baseandroid.base;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.baseandroid.widget.LoadingFileHintDialog;
import com.haodaibao.fanbeiapp.BuildConfig;
import com.haodaibao.fanbeiapp.module.dialog.LoadingDialog;
import com.squareup.leakcanary.RefWatcher;
import com.trello.rxlifecycle2.components.support.RxFragment;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import ly.count.android.sdk.Countly;

public abstract class BaseFragment extends RxFragment implements IView {
    protected Context mContext;
    protected Unbinder mUnbinder;
    private String pageName = this.getClass().getSimpleName();
    protected LoadingDialog mLoadingDialog;
    private LoadingFileHintDialog loadingFileHintDialog;

    public BaseFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (this.getUserVisibleHint()) {
            //LazyLoad
            this.onVisibilityChangedToUser(true, false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (this.getUserVisibleHint()) {
            this.onVisibilityChangedToUser(false, false);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        //UI可见时才进行数据加载操作,优先于onCreateView,onResume
        if (this.isResumed()) {
            // LazyLoad
            this.onVisibilityChangedToUser(isVisibleToUser, true);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContext = getActivity();
        View view = inflater.inflate(getLayoutId(), null);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setup(savedInstanceState);
    }

    private void setup(Bundle savedInstanceState) {
        setupView();
        setupData(savedInstanceState);
    }

    protected abstract int getLayoutId();

    protected abstract void setupView();

    protected abstract void setupData(Bundle savedInstanceState);



    @Override
    public void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
        if (BuildConfig.DEBUG) {
            // use the RefWatcher to watch for fragment leaks:
            RefWatcher refWatcher = BaseApplication.getRefWatcher(getActivity());
            refWatcher.watch(this);
        }
    }

    public void onVisibilityChangedToUser(boolean isVisibleToUser, boolean isHappenedInSetUserVisibleHintMethod) {
        if (isVisibleToUser) {
            if (this.pageName != null) {
                Countly.sharedInstance().onStart(this.pageName);
            }
        } else if (this.pageName != null) {
            Countly.sharedInstance().onStop(this.pageName);
        }

    }

    @Override
    public void showLoadingDialog() {
        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog();
        }

        if (!mLoadingDialog.isVisible()) {
            mLoadingDialog.show(getActivity().getSupportFragmentManager(), "waitingdialog");
        }
    }

    @Override
    public void closeLoadingDialog() {
        if (mLoadingDialog != null) {
            getView().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mLoadingDialog != null && mLoadingDialog.isVisible()) {
                        mLoadingDialog.dismissAllowingStateLoss();
                    }
                }
            }, 200);
        }

    }

    @Override
    public void showUploading() {
        if (loadingFileHintDialog == null) {
            loadingFileHintDialog = new LoadingFileHintDialog(getContext());
        }
        if (!loadingFileHintDialog.isShowing()) {
            loadingFileHintDialog.show();
        }
    }

    @Override
    public void processUploading(String processText) {
        if (loadingFileHintDialog != null && !TextUtils.isEmpty(processText)) {
            if (loadingFileHintDialog.isShowing()) {
                loadingFileHintDialog.setProcessText(processText);
            }
        }
    }


    @Override
    public void closeAnimatorUploading() {
        if (loadingFileHintDialog != null) {
            if (loadingFileHintDialog.isShowing()) {
                loadingFileHintDialog.cleanFileAnimator();
            }
        }
    }

    @Override
    public void closeUploading() {
        if (loadingFileHintDialog != null) {
            if (loadingFileHintDialog.isShowing()) {
                loadingFileHintDialog.cancel();
            }
            loadingFileHintDialog = null;
        }
    }

    @Override
    public void showToast(String message) {

    }
}

