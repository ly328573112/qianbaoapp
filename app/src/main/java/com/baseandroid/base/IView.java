package com.baseandroid.base;

/**
 * Created by hdb on 2017/9/6.
 */

public interface IView {

    void showLoadingDialog();

    void closeLoadingDialog();

    void showUploading();

    void processUploading(String processText);

    void closeAnimatorUploading();

    void closeUploading();

    void showToast(String message);

}