package com.baseandroid.base;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.baseandroid.retrofit.OkHttpClientManager;
import com.baseandroid.utils.CommonUtils;
import com.haodaibao.fanbeiapp.BuildConfig;
import com.haodaibao.fanbeiapp.R;

import java.util.HashMap;

import butterknife.BindView;

public abstract class BaseWebviewActivity extends BaseActivity {
    public static final String WEBVIEW_URL = "webview_url";
    public static final String WEBVIEW_TITLE = "webview_title";

    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @BindView(R.id.toolbar_back)
    View toolbar_back;
    @BindView(R.id.webview)
    protected WebView webview;
    @BindView(R.id.right_text)
    protected TextView right_text;
    protected String mUrl;
    protected HashMap<String, String> mWebViewHeader;
    protected String mInitTitle;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_webview_layout;
    }

    @Override
    protected void setupView() {
        mUrl = getIntent().getStringExtra(WEBVIEW_URL);
        mInitTitle = getIntent().getStringExtra(WEBVIEW_TITLE);
        toolbar_title.setText(R.string.webview_start_load);
        toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (webview.canGoBack()) {
                    webview.goBack();
                } else {
                    finish();
                }
            }
        });

        webview.setWebChromeClient(mWebChromeClient);
        webview.setWebViewClient(mWebViewClient);

        WebSettings webSettings = webview.getSettings();
        //如果访问的页面中要与Javascript交互，则webview必须设置支持Javascript
        //若加载的html里有JS在执行动画等操作，会造成资源浪费（CPU、电量）在onStop和onResume
        //里分别把 setJavaScriptEnabled()给设置成false和true即可
        webSettings.setJavaScriptEnabled(true);
        //设置自适应屏幕，两者合用
        webSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
        webSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小
        //缩放操作
        //webSettings.setSupportZoom(true); //支持缩放，默认为true。是下面二个的前提。
        //webSettings.setBuiltInZoomControls(true); //设置内置的缩放控件。若为false，则该WebView不可缩放
        //webSettings.setDisplayZoomControls(false); //隐藏原生的缩放控件
        webSettings.setDomStorageEnabled(true); // 开启 DOM storage API 功能
        webSettings.setDatabaseEnabled(true);   //开启 database storage API 功能
        webSettings.setAppCacheEnabled(true);//开启 Application Caches 功能
        //String cacheDirPath = getFilesDir().getAbsolutePath() + APP_CACAHE_DIRNAME;
        //webSettings.setAppCachePath(cacheDirPath); //设置  Application Caches 缓存目录
        webSettings.setAllowContentAccess(true);
        webSettings.setSaveFormData(false);
        //缓存模式如下：
        //LOAD_CACHE_ONLY: 不使用网络，只读取本地缓存数据
        //LOAD_DEFAULT: （默认）根据cache-control决定是否从网络上取数据。
        //LOAD_NO_CACHE: 不使用缓存，只从网络获取数据.
        //LOAD_CACHE_ELSE_NETWORK，只要本地有，无论是否过期，或者no-cache，都使用缓存中的数据。
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);

        mWebViewHeader = new HashMap<>();
        mWebViewHeader.put("platform", "Android");
        mWebViewHeader.put("version", BuildConfig.VERSION_CODE + "");
        //mWebViewHeader.put("token", Global.getUserToken());
        //mWebViewHeader.put("channel", AppUtils.getChannel());
        mWebViewHeader.put("Custom-Agent", CommonUtils.formatUserAgent());
        provideHeader();


        String cookies = OkHttpClientManager.getHttpCookieString();
        if (!TextUtils.isEmpty(cookies)) {
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            cookieManager.setCookie(mUrl, cookies);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                webview.loadUrl(mUrl, mWebViewHeader);
            }
        }, 100);
    }

    protected abstract void provideHeader();

    @Override
    protected void setupData(Bundle savedInstanceState) {

    }

    //辅助WebView处理Javascript的对话框,网站图标,网站标题等
    private WebChromeClient mWebChromeClient = new WebChromeClient() {

//        private String mTitle;

        //获取Web页中的标题
        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
//            this.mTitle = title;
        }

        //获得网页的加载进度并显示
        public void onProgressChanged(WebView view, int progress) {
            super.onProgressChanged(view, progress);
            if (isFinishing()) {
                return;
            }
            if (progress < 99) {
                toolbar_title.setText(getResources().getString(R.string.webview_start_load));
            } else {
                toolbar_title.setText(mInitTitle);
            }
        }
    };

    private WebViewClient mWebViewClient = new WebViewClient() {

        //复写shouldOverrideUrlLoading()方法，使得打开网页时不调用系统浏览器，
        //而是在本WebView中显示
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith("http://iapppay.newpay.com/?transid=")) {
                return true;
            } else if (url.startsWith("app://feedback")) {
                return true;
            } else if (url.startsWith("https://rules")) {
                return true;
            }

            view.loadUrl(url, mWebViewHeader);
            return true;
        }

        //开始载入页面调用的
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        //在页面加载结束时调用
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        //在加载页面资源时会调用，每一个资源（比如图片）的加载都会调用一次。
        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
        }

        //加载页面的服务器出现错误时（如404）调用
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            webview.setVisibility(View.GONE);
            //mEmptyView.setVisibility(View.VISIBLE);
            toolbar_title.postDelayed(new Runnable() {
                @Override
                public void run() {
                    toolbar_title.setText(mInitTitle);
                }
            }, 200);
        }

    };

    @Override
    public void onBackPressed() {
        if (webview.canGoBack()) {
            webview.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        if (webview != null) {
            webview.loadDataWithBaseURL(null, "", "text/html", "utf-8", null);
            webview.clearHistory();

            ((ViewGroup) webview.getParent()).removeView(webview);
            webview.destroy();
            webview = null;
        }
        super.onDestroy();

        //清除网页访问留下的缓存
        //由于内核缓存是全局的因此这个方法不仅仅针对webview而是针对整个应用程序.
        //Webview.clearCache(true);

        //清除当前webview访问的历史记录
        //只会webview访问历史记录里的所有记录除了当前访问记录
        //Webview.clearHistory()；

        //这个api仅仅清除自动完成填充的表单数据，并不会清除WebView存储到本地的数据
        //Webview.clearFormData()；

    }
}
