package com.baseandroid.base;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;

import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.home.HomeActivity;
import com.haodaibao.fanbeiapp.module.start.StartActivity;
import com.jayfeng.lesscode.core.DisplayLess;

public class BasePopupWindow extends PopupWindow {
    private static OnOutSideTouchListner s;
    private View mContentView;
    private View mParentView;
    private boolean isOutsideTouch;
    private boolean isFocus;
    private Drawable mBackgroundDrawable;
    private int mAnimationStyle;
    private boolean isWidthWrap;
    private boolean isHeightWrap;
    private static Builder builder;
    private static AnimatorSet animatorSet;

    private BasePopupWindow(Builder builder) {
        this.mContentView = builder.contentView;
        this.mParentView = builder.parentView;
        this.isOutsideTouch = builder.isOutsideTouch;
        this.isFocus = builder.isFocus;
        this.mBackgroundDrawable = builder.backgroundDrawable;
        this.mAnimationStyle = builder.animationStyle;
        this.isWidthWrap = builder.isWidthWrap;
        this.isHeightWrap = builder.isHeightWrap;
        initLayout();
    }

    public static Builder builder() {
        if (builder == null) {
            synchronized (BasePopupWindow.class) {
                if (builder == null) {
                    builder = new Builder();
                }
            }

        }
        return builder;
    }

    private void initLayout() {
        setWidth(isWidthWrap ? LayoutParams.WRAP_CONTENT : LayoutParams.MATCH_PARENT);
        setHeight(isHeightWrap ? LayoutParams.WRAP_CONTENT : LayoutParams.MATCH_PARENT);
        setFocusable(isFocus);
        setOutsideTouchable(isOutsideTouch);
        setBackgroundDrawable(mBackgroundDrawable);
        if (mAnimationStyle != -1)//如果设置了动画则使用动画
        {
            setAnimationStyle(mAnimationStyle);
        } else {
            setAnimationStyle(android.R.style.Animation);
        }
        setContentView(mContentView);
    }

    public View getContentView() {
        return mContentView;
    }

    public static View inflateView(ContextThemeWrapper context, int layoutId) {
        return LayoutInflater.from(context)
                .inflate(layoutId, null);
    }

    public void show() {
        if (Build.VERSION.SDK_INT < 24) {
            showAsDropDown(mParentView, DisplayLess.$dp2px(0), DisplayLess.$dp2px(0));
        } else if (Build.VERSION.SDK_INT == 24) {
            // 适配 android 7.0
            int[] location = new int[2];
            mParentView.getLocationOnScreen(location);
            int x = location[0];
            int y = location[1];
            showAtLocation(mParentView, Gravity.NO_GRAVITY, DisplayLess.$dp2px(0), y + mParentView.getHeight() + DisplayLess.$dp2px(0));
        } else {
            // 适配 android 7.1.1+
            View rootView = mParentView.getRootView();
            Rect rect = new Rect();
            rootView.getWindowVisibleDisplayFrame(rect);
            int[] xy = new int[2];
            mParentView.getLocationInWindow(xy);
            int anchorY = xy[1] + mParentView.getHeight();
            int height = rect.bottom - anchorY;
            if (!isHeightWrap) {
                setHeight(height);
            }
            showAtLocation(mParentView, Gravity.NO_GRAVITY, 0, anchorY);
        }
    }

    public static final class Builder {
        private View contentView;
        private View parentView;
        private boolean isOutsideTouch = true;//默认为true
        private boolean isFocus = true;//默认为true
        private Drawable backgroundDrawable = new ColorDrawable(0x00000000);//默认为透明
        private int animationStyle = -1;
        private boolean isWidthWrap;
        private boolean isHeightWrap;

        private Builder() {
        }

        public Builder contentView(View contentView) {
            this.contentView = contentView;
            return this;
        }

        public Builder parentView(View parentView) {
            this.parentView = parentView;
            return this;
        }

        public Builder isWidthWrap(boolean isWrap) {
            this.isWidthWrap = isWrap;
            return this;
        }

        public Builder isHeightWrap(boolean isWrap) {
            this.isHeightWrap = isWrap;
            return this;
        }

        public Builder isOutsideTouch(boolean isOutsideTouch) {
            this.isOutsideTouch = isOutsideTouch;
            return this;
        }

        public Builder isFocus(boolean isFocus) {
            this.isFocus = isFocus;
            return this;
        }

        public Builder backgroundDrawable(Drawable backgroundDrawable) {
            this.backgroundDrawable = backgroundDrawable;
            return this;
        }

        public Builder animationStyle(int animationStyle) {
            this.animationStyle = animationStyle;
            return this;
        }

        public BasePopupWindow build() {
            if (contentView == null) {
                throw new IllegalStateException("contentView is required");
            }

            return new BasePopupWindow(this);
        }
    }

    public static BasePopupWindow matchParentPopupWindow(final Activity activity, View parentView, View contentView, boolean isFocus, final OnOutSideTouchListner outSideTouchListner) {
        BasePopupWindow popupWindow = null;
        final FrameLayout frameLayout = new FrameLayout(activity);
        final FrameLayout frameLayoutbg = new FrameLayout(activity);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        frameLayout.addView(frameLayoutbg);

        layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final FrameLayout frameLayoutContent = new FrameLayout(activity);
        frameLayoutContent.addView(contentView);
        frameLayoutContent.setBackgroundColor(Color.WHITE);
        frameLayout.addView(frameLayoutContent, layoutParams);

        frameLayoutContent.post(new Runnable() {
            @Override
            public void run() {
                FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) frameLayoutContent.getLayoutParams();
                if (frameLayoutContent.getHeight() >= DisplayLess.$height(activity) * 0.618f) {
                    layoutParams.height = (int) (DisplayLess.$height(activity) * 0.618f);
                } else if (frameLayoutContent.getHeight() <= DisplayLess.$height(activity) * 0.318f) {
                    layoutParams.height = (int) (DisplayLess.$height(activity) * 0.318f);
                } else {
                    layoutParams.height = frameLayoutContent.getHeight();
                }
                frameLayoutContent.setLayoutParams(layoutParams);
            }
        });

        popupWindow = BasePopupWindow.builder()
                .parentView(parentView)
                .contentView(frameLayout)
                .isFocus(false)
                .isWidthWrap(false)
                .isHeightWrap(false)
                .isOutsideTouch(false)
                .build();

        final BasePopupWindow finalPopupWindow = popupWindow;
        finalPopupWindow.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss() {
//                outSideTouchListner.onOutSideTouch();
            }
        });

        frameLayoutbg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (finalPopupWindow != null) {
                    ViewGroup contentframe = (ViewGroup) ((ViewGroup) finalPopupWindow.getContentView()).getChildAt(1);
                    ViewGroup content = (ViewGroup) contentframe.getChildAt(0);
                    AnimatorSet animatorSet = new AnimatorSet();
                    contentframe.clearAnimation();
                    ObjectAnimator translationY = ObjectAnimator.ofFloat(contentframe, "translationY", 0, -contentframe.getHeight());
                    translationY.setDuration(360);
                    translationY.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        @Override
                        public void onAnimationUpdate(ValueAnimator animation) {
                            float alpha = animation.getAnimatedFraction();
                            float bgAlpha = 0xb0 * (1 - alpha) * (1 - alpha) * (1 - alpha);
                            ((ViewGroup) finalPopupWindow.getContentView()).getChildAt(0)
                                    .setBackgroundColor(Color.argb((int) (bgAlpha), 0, 0, 0));
                        }
                    });

                    translationY.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {
                            outSideTouchListner.onOutSideTouch();
                            finalPopupWindow.dismiss();
                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animator) {

                        }
                    });

                    animatorSet.play(translationY);
                    animatorSet.start();
                }
            }
        });

        return popupWindow;
    }

    public static BasePopupWindow popupWindowOnce;

    public static BasePopupWindow matchParentPopupWindowOnce(final Activity activity, View parentView, final View contentView, boolean isFocus, OnOutSideTouchListner outSideTouchListner) {
        final int maxh = (int) (DisplayLess.$height(activity) * 0.6);
        Log.e("hpo", "matchParentPopupWindowOnce: ");
        try {
            if (animatorSet != null) {
                animatorSet.cancel();
            }

            s = outSideTouchListner;
            final FrameLayout frameLayoutbg = new FrameLayout(activity);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            final FrameLayout frameLayoutContent = new FrameLayout(activity);
            contentView.setAlpha(0);
            if (contentView.getParent() != null) {
                ((ViewGroup) contentView.getParent()).removeAllViews();
            }
            frameLayoutContent.addView(contentView);
            frameLayoutContent.setBackgroundColor(Color.WHITE);
            frameLayoutContent.post(new Runnable() {
                @Override
                public void run() {
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) frameLayoutContent.getLayoutParams();
//                    if (frameLayoutContent.getHeight() >= DisplayLess.$height(activity) * 0.618f) {
//                        layoutParams.height = (int) (DisplayLess.$height(activity) * 0.618f);
//                    } else if (frameLayoutContent.getHeight() <= DisplayLess.$height(activity) * 0.318f) {
//                        layoutParams.height = (int) (DisplayLess.$height(activity) * 0.318f);
//                    } else {
//                        layoutParams.height = frameLayoutContent.getHeight();
//                    }
                    layoutParams.height = frameLayoutContent.getHeight() >= maxh ? maxh : frameLayoutContent.getHeight();
                    frameLayoutContent.setLayoutParams(layoutParams);
                }
            });

            if (popupWindowOnce == null) {
                final FrameLayout frameLayout = new FrameLayout(activity);
                frameLayout.addView(frameLayoutbg);
                frameLayout.addView(frameLayoutContent, layoutParams);
                popupWindowOnce = BasePopupWindow.builder()
                        .parentView(parentView)
                        .contentView(frameLayout)
                        .isFocus(false)
                        .isWidthWrap(false)
                        .isHeightWrap(false)
                        .isOutsideTouch(false)
                        .build();
            } else {
                final ViewGroup viewGroup = (ViewGroup) popupWindowOnce.getContentView();
                final ViewGroup contentframe = (ViewGroup) ((ViewGroup) popupWindowOnce.getContentView()).getChildAt(1);
                final ViewGroup content = (ViewGroup) contentframe.getChildAt(0);
                frameLayoutContent.setAlpha(0);

                viewGroup.addView(frameLayoutContent, layoutParams);
                frameLayoutContent.post(new Runnable() {
                    @Override
                    public void run() {
                        final int hw = frameLayoutContent.getMeasuredHeight() > maxh ? maxh : frameLayoutContent.getMeasuredHeight();
                        final int h = contentframe.getHeight();
                        viewGroup.removeView(frameLayoutContent);
                        contentframe.removeView(content);
                        frameLayoutContent.removeAllViews();
                        contentView.setAlpha(0);
                        if (contentView.getParent() != null) {
                            ((ViewGroup) contentView.getParent()).removeAllViews();
                        }
                        contentframe.addView(contentView);
                        final ViewGroup.LayoutParams params = contentframe.getLayoutParams();
                        ValueAnimator mValueAnimator = ValueAnimator.ofInt(0, 1);
                        mValueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                            @Override
                            public void onAnimationUpdate(ValueAnimator animation) {
                                Log.e("hpo", "onAnimationUpdate: h=" + h + "hw=" + hw);
                                params.height = (int) (h + (hw - h) * animation.getAnimatedFraction());
                                contentframe.setLayoutParams(params);
                            }
                        });
                        mValueAnimator.setDuration(360);
                        ViewGroup c = (ViewGroup) contentframe.getChildAt(0);

                        ObjectAnimator translationYsmall = ObjectAnimator.ofFloat(c, "translationY", DisplayLess.$dp2px(8), DisplayLess.$dp2px(10), 0);
                        translationYsmall.setDuration(240);

                        ObjectAnimator alphasmall = ObjectAnimator.ofFloat(c, "alpha", 0.1f, 1f);
                        alphasmall.setDuration(240);
                        if (animatorSet == null) {
                            animatorSet = new AnimatorSet();
                        }

                        animatorSet.play(mValueAnimator)
                                .before(translationYsmall)
                                .before(alphasmall);
                        animatorSet.start();
                    }
                });

            }


            frameLayoutbg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (popupWindowOnce != null) {
                        ViewGroup contentframe = (ViewGroup) ((ViewGroup) popupWindowOnce.getContentView()).getChildAt(1);
//                    ViewGroup content = (ViewGroup) contentframe.getChildAt(0);
                        AnimatorSet animatorSet = new AnimatorSet();
                        contentframe.clearAnimation();
                        ObjectAnimator translationY = ObjectAnimator.ofFloat(contentframe, "translationY", 0, -contentframe.getHeight());
                        translationY.setDuration(360);
                        translationY.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                            @Override
                            public void onAnimationUpdate(ValueAnimator animation) {
                                try {
                                    float alpha = animation.getAnimatedFraction();
                                    float bgAlpha = 0xb0 * (1 - alpha) * (1 - alpha) * (1 - alpha);
                                    ((ViewGroup) popupWindowOnce.getContentView()).getChildAt(0)
                                            .setBackgroundColor(Color.argb((int) (bgAlpha), 0, 0, 0));
                                } catch (Exception e) {

                                }

                            }
                        });

                        translationY.addListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {
                                s.onOutSideTouch();

                                if (popupWindowOnce != null) {
                                    popupWindowOnce.dismiss();
                                    ((ViewGroup) popupWindowOnce.getContentView()).removeAllViews();
                                }
                                popupWindowOnce = null;
                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        });

                        animatorSet.play(translationY);
                        animatorSet.start();
                    }
                }
            });


        } catch (Exception e) {

        }
        return popupWindowOnce;
    }

    public void disMissWithAlpha(final BasePopupWindow poopupWindow) {
        if (poopupWindow != null) {
            ViewGroup contentframe = (ViewGroup) ((ViewGroup) poopupWindow.getContentView()).getChildAt(1);
//            ViewGroup content = (ViewGroup) contentframe.getChildAt(0);
            AnimatorSet animatorSet = new AnimatorSet();
            contentframe.clearAnimation();
            ObjectAnimator translationY = ObjectAnimator.ofFloat(contentframe, "translationY", 0, -contentframe.getHeight());
            translationY.setDuration(360);
            translationY.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float alpha = animation.getAnimatedFraction();
                        float bgAlpha = 0xb0 * (1 - alpha) * (1 - alpha) * (1 - alpha);
                        ((ViewGroup) poopupWindow.getContentView()).getChildAt(0)
                                .setBackgroundColor(Color.argb((int) (bgAlpha), 0, 0, 0));
                    } catch (Exception e) {

                    }

                }
            });

            translationY.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {

                    if (popupWindowOnce != null) {
                        poopupWindow.dismiss();
                        ((ViewGroup) popupWindowOnce.getContentView()).removeAllViews();
                    }

                    popupWindowOnce = null;
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

            animatorSet.play(translationY);
            animatorSet.start();
        }
    }

    public void showMatchParentPopupDialogWithAlpha(final BasePopupWindow popupWindow) {

        popupWindow.show();
        ((ViewGroup) popupWindow.getContentView()).getChildAt(1)
                .post(new Runnable() {
                    @Override
                    public void run() {
                        ViewGroup contentframe = (ViewGroup) ((ViewGroup) popupWindow.getContentView()).getChildAt(1);
                        ViewGroup content = (ViewGroup) contentframe.getChildAt(0);
                        AnimatorSet animatorSet = new AnimatorSet();
                        contentframe.clearAnimation();
                        content.clearAnimation();
                        content.setAlpha(0.0f);
                        ObjectAnimator translationY = ObjectAnimator.ofFloat(contentframe, "translationY", -contentframe.getHeight(), 0);
                        translationY.setDuration(360);
                        translationY.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                            @Override
                            public void onAnimationUpdate(ValueAnimator animation) {
                                try {
                                    float alpha = animation.getAnimatedFraction();
                                    float bgAlpha = 0xb0 * alpha * alpha * alpha;
                                    ((ViewGroup) popupWindow.getContentView()).getChildAt(0)
                                            .setBackgroundColor(Color.argb((int) bgAlpha, 0, 0, 0));
                                } catch (Exception e) {

                                }

                            }
                        });

                        ObjectAnimator translationYsmall = ObjectAnimator.ofFloat(content, "translationY", DisplayLess.$dp2px(8), DisplayLess.$dp2px(10), 0);
                        translationYsmall.setDuration(240);

                        ObjectAnimator alphasmall = ObjectAnimator.ofFloat(content, "alpha", 0.1f, 1f);
                        alphasmall.setDuration(240);

                        animatorSet.play(translationY)
                                .before(alphasmall)
                                .before(translationYsmall);

                        animatorSet.start();
                    }
                });
    }

    public interface OnOutSideTouchListner {
        void onOutSideTouch();
    }

    public interface AnimaitonListener {
        void start();

        void end();
    }

    public static void destroy() {
        if (popupWindowOnce != null) {
            popupWindowOnce.dismiss();
            popupWindowOnce = null;
        }
    }
}
