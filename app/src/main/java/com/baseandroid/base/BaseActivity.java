package com.baseandroid.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.widget.Toast;

import com.baseandroid.utils.LogUtil;
import com.baseandroid.widget.LoadingFileHintDialog;
import com.haodaibao.fanbeiapp.module.dialog.LoadingDialog;
import com.haodaibao.fanbeiapp.module.home.HomeActivity;
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;
import com.umeng.analytics.MobclickAgent;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import hugo.weaving.DebugLog;

@DebugLog
public abstract class BaseActivity extends RxAppCompatActivity implements IView {

    protected Unbinder mUnbinder;
    protected BaseDoubleClickExitHelper mDoubleClickExitHelper;
    protected LoadingDialog mLoadingDialog;
    private LoadingFileHintDialog loadingFileHintDialog;
    protected Context mContext ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        mUnbinder = ButterKnife.bind(this);
        setup(savedInstanceState);
        BaseApplication.getAppManager().addActivity(this);
        mDoubleClickExitHelper = new BaseDoubleClickExitHelper(this);
    }

    private void setup(Bundle savedInstanceState) {
        setupView();
        setupData(savedInstanceState);
    }

    protected abstract int getLayoutId();

    protected abstract void setupView();

    protected abstract void setupData(Bundle savedInstanceState);

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if (this instanceof HomeActivity) {
                    return mDoubleClickExitHelper.onKeyDown(keyCode, event);
                }
                break;

            default:
                break;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void showToast(int resId) {
        Toast.makeText(this, this.getResources().getText(resId), Toast.LENGTH_LONG)
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onStart() {
//        Countly.sharedInstance().onStart(this);
        super.onStart();

    }

    @Override
    protected void onStop() {
//        Countly.sharedInstance().onStop(this);
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    public void showLoadingDialog() {
        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog();
        }

        if (!mLoadingDialog.isVisible()) {
            mLoadingDialog.show(getSupportFragmentManager(), "waitingdialog");
        }
    }

    @Override
    public void closeLoadingDialog() {
        if (mLoadingDialog != null) {
            if (mLoadingDialog.getView() == null) {
                return;
            }
            try {
                mLoadingDialog.getView().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (mLoadingDialog != null && mLoadingDialog.isVisible()) {
                            mLoadingDialog.dismissAllowingStateLoss();
                        }
                    }
                }, 200);
            } catch (NullPointerException e) {
                e.printStackTrace();
                LogUtil.e(e);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
        BaseApplication.getAppManager().removeActivity(this);
    }

    @Override
    public void showUploading() {
        if (loadingFileHintDialog == null) {
            loadingFileHintDialog = new LoadingFileHintDialog(this);
        }
        if (!loadingFileHintDialog.isShowing()) {
            loadingFileHintDialog.show();
        }
    }

    @Override
    public void processUploading(String processText) {
        if (loadingFileHintDialog != null && !TextUtils.isEmpty(processText)) {
            if (loadingFileHintDialog.isShowing()) {
                loadingFileHintDialog.setProcessText(processText);
            }
        }
    }


    @Override
    public void closeAnimatorUploading() {
        if (loadingFileHintDialog != null) {
            if (loadingFileHintDialog.isShowing()) {
                loadingFileHintDialog.cleanFileAnimator();
            }
        }
    }


    @Override
    public void closeUploading() {
        if (loadingFileHintDialog != null) {
            if (loadingFileHintDialog.isShowing()) {
                loadingFileHintDialog.cancel();
            }
            loadingFileHintDialog = null;
        }
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
