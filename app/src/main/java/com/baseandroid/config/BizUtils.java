package com.baseandroid.config;

import android.app.Application;
import android.text.TextUtils;

import com.tencent.bugly.crashreport.CrashReport;

// 业务工具类, 第三方分享, 支付, Webview
public class BizUtils {

    public static void buglyInit(boolean isDeveloper) {
        if (isDeveloper) {
            // 测试环境
            CrashReport.initCrashReport(Global.getContext(), "db855b79f7", true);
        } else {
            // 生产环境
            CrashReport.initCrashReport(Global.getContext(), "261dc3ac71", true);
        }
    }

    /* 微信APPID */
    public static String WX_APP_ID = "wxfcc9cb8bb7df7630";
    /* QQ分享APPID */
    public static final String QQ_APP_ID = "1104609227";

    public static void initSWhiteSDK(Application context) {
        /*String httpurl = Api.sServerApiUrl.replace("/life/", "");
        SWhiteManager.getInstance().initAppConstant(Api.sSH5Url, httpurl, Api.domain);
        SWhiteManager.getInstance().initSDK(context, new TokenTimeOutListener() {
            @Override
            public void tokenTimeOut() {
                //重新登录
                if (!TextUtils.isEmpty(OkHttpClientManager.getHttpCookieString())) {
                    OkHttpClientManager.clearCookie();
                    EventBus.getDefault().post(new LoginInvalideEvent());
                }
            }
        }, new JumpToQianbaoListener() {

            @Override
            public void jumpToQianbao(Activity activity) {
                //返回首页
                Intent intent = new Intent(activity, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                activity.startActivity(intent);
            }

            @Override
            public void jumpToHelper(Activity activity) {
                Intent i = new Intent(activity, HelpActivity.class);
                i.putExtra("uid", "01");
                i.putExtra("title", "帮助中心");
                activity.startActivity(i);
            }
        });*/

    }

    /**
     * 隐藏密码中间4位
     * @param password
     * @return
     */
    public static String hintPwd(String password) {
        if (!TextUtils.isEmpty(password) && password.length() > 6) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < password.length(); i++) {
                char c = password.charAt(i);
                if (i >= 3 && i <= 6) {
                    sb.append('*');
                } else {
                    sb.append(c);
                }
            }
            return sb.toString();
        }else{
            return "";
        }
    }

}
