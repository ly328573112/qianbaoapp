package com.baseandroid.config;

import android.support.annotation.Keep;

@Keep
public class Constant {

    /*组织编号*/
    public static final String ORGNO = "O201501011000000000";
    /*默认上海城市*/
    public static final String SELECTCITY = "上海";
    /*默认上海城市*/
    public static final String SELECTCITYSITETYPE = "1";
    /*默认上海地区编码*/
    public static final String SELECTCITYAD = "310000";
    /*默认上海城市Code*/
    public static final String SELECTCITYCODE = "310000";
    /*默认上海城市latitude*/
    public static final double LATITUDE = 31.16449;
    /*默认上海城市longitude*/
    public static final double LONGITUDE = 121.385902;

    /**
     * api status code
     */
    public static final int WEB_SUCCESS = 200;

    /**
     * 网络请求正确code
     */
    public static final String SUCCESS_CODE = "20000151";
    /**
     * 登录超时错误码
     */
    public static final String ERROR_LOGOUT = "20090151";
    /**
     * 退出登录
     */
    public static final String EXIT = "20091151";


    /**
     * Error Retry Count
     */
    public static final int RETRY_COUNT = 3;

    /**
     * long time
     */
    public static final long TIME_ONE_SECOND = 1000;
    public static final long TIME_ONE_MINITUE = 60000;
    public static final long TIME_ONE_HOUR = 3600000;

    public static final int VALIDETE_CODE_WATING_TIME_MAX = 60;

    /*
    * preference_key
    */
    public static String PREFERENCE_KEY_USER_TOKEN_INFO = "user_token_info";
    public static String PREFERENCE_KEY_USER_LOGIN_INFO = "user_login_info";
    public static String PREFERENCE_KEY_USER_INFO = "user_info";
    public static String PREFERENCE_KEY_USER_JPUSH_ALIAS = "jpush_alias";
    public static String PREFERENCE_KEY_USER_LOCATION = "user_location";
    public static String PREFERENCE_KEY_USER_HISTORY_LOCATION = "history_location" ;
    public static String PREFERENCE_KEY_USER_REGEOCODEADDRESS = "user_regeocodeAddress";
    public static String PREFERENCE_KEY_USER_PHONE = "user_phone";
    public static String PREFERENCE_KEY_FIRST_USING = "first_using";
    public static String PREFERENCE_KEY_OPEN_CITYS_RESP = "open_citys_resp";
    public static String PREFERENCE_KEY_PROVIN_CITY_DIST_RESP = "provin_city_dist_resp";
    public static String PREFERENCE_KEY_CATEGORYS = "categorys";
    public static String PREFERENCE_KEY_SHANGQUANS = "shangquans";
    public static String PREFERENCE_KEY_SELECTCITY = "selectcity";
    public static String PREFERENCE_KEY_LAST_SELECTCITY = "lastSelectcity";
    public static String PREFERENCE_KEY_SELECTCITYCODE = "selectcitycode";
    public static String PREFERENCE_KEY_SELECTCITYSITETYPE = "selectcitysitetype";
    public static String PREFERENCE_KEY_MAPSELECTCITYCODE = "mapselectcitycode";
    public static String PREFERENCE_KEY_PAGE_SWITCHING = "page_switching_status";
    public static String PREFERENCE_KEY_PAGE_SWITCHING1 = "page_switching_status1";

    public static String ANDROIDVERSION = "ANDROID_USER_VERSION";
    public static String DEATAILSDB = "MDEATAILSDB";
    /**
     * 支付页面
     */
    public static final int CHOOSE_COUPON_RESULTCODE = 10001;
    public static final int PAYSUCC_COMMENT_RESULTCODE = 10002;
    public static final int PAYSUCC_PRESS_BACK_RESULTCODE = 10003;//成功页面按返回键
    public static final String FROM = "from";
    /*支付状态*/
    public static final String WEIZHIFU = "10";// 未支付
    public static final String CHULIZHONG = "11";// 支付处理中
    public static final String ZHIFUCHENGGONG = "12";// 支付成功(远期消费已支付未到店消费)
    public static final String JIAOYICHENGGONG = "20";// 交易支付成功
    /*支付方式*/
    public static final String XIANJIN = "01";// 现金账户支付
    public static final String KUAIJIE = "02";// 银行卡支付
    public static final String WEIXIN = "03";// 微信
    public static final String SHUAKA = "06";// 刷卡
    public static final String ALIPAY_4 = "04";// 支付宝
    public static final String ALIPAY_7 = "07";// 支付宝
    public static final String ALIPAY_8 = "08";// 支付宝
    public static final String GONGZHONGHAO = "05";// 公众号支付
    public static final String POS = "06";// POS刷卡支付
    public static final String HONGBAO = "11";// 红包抵扣

    public static final String PAYSDK_WEIXIN = "23";// 收银台-微信app支付
    public static final String PAYSDK_ALIPAY = "24";// 收银台-支付宝app支付
    public static final String PAYSDK_SWHITE = "26";// 收银台-白花花支付
    /**
     * 套餐支付页面
     */
    public static final String PACKAGE_STATUS = "00000000";//套餐是可用状态标志
    public static final String PACKAGE_STATUS01 = "RULE500001";//套餐结束
    /**
     * 智能搜索
     */
    public static final String TAGNO = "20171130100065";
    public static final String AD = "20170830117934";
    /**
     * 发票页面
     */
    public static final int HEADER_SELECT_RESULTCODE = 10004;
    public static final int INVOICE_DETAIL_RESULTCODE = 10005;
    public static final String INVOICEHEADITEM = "invoiceHeadItem";
    public static final String INVOICEID = "invoiceId";

    /**
     *  搜索页面
     */
    public static final int SEARCH_RESULT = 10001;
    public static final int SEARCH_BACK = 10002;

}