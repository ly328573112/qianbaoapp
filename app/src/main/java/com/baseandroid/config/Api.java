package com.baseandroid.config;

public class Api {

    public static AppEnviron environment = AppEnviron.of(2);
    public static boolean isDevelop = true;

    /*----must add one "/" after url -----*/
    public static String sServerApiUrl = "https://apis.qianbao.com/life/";
    public final static String sDevDoneUri = "http://118.31.48.228:10982/life/";
    public final static String sPreRelUri = "https://pre-apis.qianbao.com/life/";
    public final static String sRelDoneUri = "https://apis.qianbao.com/life/";

    /*----七牛图片地址---*/
    public static String sImageUrl = "http://img0.qianbaocard.com/public/qianbaolife/";
    public final static String sImageTestUrl = "http://img0.qianbaocard" + ".com/public/qianbaolifetest/";
    //    public final static String sImageTestUrl = "http://img9.qianbaocard.com/public/qianbaolife/";
    public final static String sImageDoneUrl = "http://img0.qianbaocard.com/public/qianbaolife/";

    /*收银台初始化*/
    public static String sPayUrl = "https://apis.qianbao.com/payment/";
    public static String sPayDevDoneUri = "https://sit-apis.qianbao.com/payment/";
    public static String sPayPreRelUri = "https://pre-apis.qianbao.com/payment/";
    public static String sPayRelDoneUri = "https://apis.qianbao.com/payment/";

    /*H5_SERVER*/
    public static String sH5Url = "http://h5.qianbaocard.com";
    public final static String sH5DevDoneUri = "http://10.10.13.12:8001";
    public final static String sH5PreRelUri = "http://h5.qianbaocard.com";
    public final static String sH5RelDoneUri = "http://h5.qianbaocard.com";

    /*S_H5_SERVER*/
    public static String sSH5Url = "http://s.qianbaocard.com";
    public final static String sSH5DevDoneUri = "http://test.qianbaocard.com:23480/s";
    public final static String sSH5PreRelUri = "http://s.qianbaocard.com";
    public final static String sSH5RelDoneUri = "http://s.qianbaocard.com";

    /*DOMAIN-------cookie using*/
    public static String domain = "apis.qianbao.com";
    public final static String sDomainDevDoneUri = "118.31.48.228";
    public final static String sDomainPreRelUri = "pre-apis.qianbao.com";
    public final static String sDomainRelDoneUri = "apis.qianbao.com";

    /*UPLOADING*/
    public static String sUpLoadingUrl = "http://s.qianbaocard.com/";
    public final static String sUpLoadingDevDoneUri = "http://test.qianbaocard.org/";
    //    public final static String sUpLoadingDevDoneUri = "http://114.55.249.157/";
//    public final static String sUpLoadingDevDoneUri = "http://s.qianbaocard.com/";
    public final static String sUpLoadingPreRelUri = "http://s.qianbaocard.com/";
    public final static String sUpLoadingRelDoneUri = "http://s.qianbaocard.com/";

    /* 版本更新文件下载地址 */
    public static String UpdateUrl = "http://s.qianbaocard.com/app/qianbao.apk";
    public final static String UpdateUrlDev = "http://s.qianbaocard.org/app/qianbao.apk";
    public final static String UpdateUrlDone = "http://s.qianbaocard.com/app/qianbao.apk";

    /* 商户分享链接地址 */
    public final static String MER_SHARE_LINK = sH5Url + "/" + "detail";

    /**
     * environment切换环境：
     * 0：开发环境；
     * 1：预发布环境；
     * 2: 线上环境;
     */
    public enum AppEnviron {
        DEV_DONE_ENV(0),
        REL_PRE_ENV(1),
        REL_DONE_ENV(2);

        private int value;

        private AppEnviron(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static AppEnviron of(int intValue) {

            switch (intValue) {
                case 0:
                    return DEV_DONE_ENV;

                case 1:
                    return REL_PRE_ENV;

                case 2:
                    return REL_DONE_ENV;

                default:
                    return DEV_DONE_ENV;
            }
        }
    }

    public static void setEnviroment(int env) {
        switch (AppEnviron.of(env)) {
            case DEV_DONE_ENV:
                isDevelop = true;
                UpdateUrl = UpdateUrlDev;
                sImageUrl = sImageTestUrl;
                sServerApiUrl = sDevDoneUri;
                sPayUrl = sPayDevDoneUri;
                sH5Url = sH5DevDoneUri;
                sSH5Url = sSH5DevDoneUri;
                domain = sDomainDevDoneUri;
                sUpLoadingUrl = sUpLoadingDevDoneUri;
                break;

            case REL_PRE_ENV:
                isDevelop = false;
                UpdateUrl = UpdateUrlDev;
                sServerApiUrl = sPreRelUri;
                sImageUrl = sImageDoneUrl;
                sPayUrl = sPayPreRelUri;
                sH5Url = sH5PreRelUri;
                sSH5Url = sSH5PreRelUri;
                domain = sDomainPreRelUri;
                sUpLoadingUrl = sUpLoadingPreRelUri;
                break;

            case REL_DONE_ENV:
                isDevelop = false;
                UpdateUrl = UpdateUrlDone;
                sServerApiUrl = sRelDoneUri;
                sImageUrl = sImageDoneUrl;
                sPayUrl = sPayRelDoneUri;
                sH5Url = sH5RelDoneUri;
                sSH5Url = sSH5RelDoneUri;
                domain = sDomainRelDoneUri;
                sUpLoadingUrl = sUpLoadingRelDoneUri;
                break;

            default:
                isDevelop = true;
                UpdateUrl = UpdateUrlDone;
                sServerApiUrl = sDevDoneUri;
                sImageUrl = sImageTestUrl;
                sPayUrl = sPayDevDoneUri;
                sH5Url = sH5DevDoneUri;
                sSH5Url = sSH5DevDoneUri;
                domain = sDomainDevDoneUri;
                sUpLoadingUrl = sUpLoadingDevDoneUri;
        }
    }
}
