package com.baseandroid.config;

import android.content.Context;
import android.text.TextUtils;

import com.baseandroid.retrofit.OkHttpClientManager;
import com.haodaibao.fanbeiapp.database.GreenDaoDbHelp;
import com.haodaibao.fanbeiapp.repository.json.DistrictList;
import com.haodaibao.fanbeiapp.repository.json.HistoryLocation;
import com.haodaibao.fanbeiapp.repository.json.LoginInfo;
import com.haodaibao.fanbeiapp.repository.json.MyLocation;
import com.haodaibao.fanbeiapp.repository.json.OpenCityBean;
import com.haodaibao.fanbeiapp.repository.json.OpenCitysInfoResp;
import com.haodaibao.fanbeiapp.repository.json.ProvinCityDistInfoResp;
import com.haodaibao.fanbeiapp.repository.json.SearchCacheEntity;
import com.haodaibao.fanbeiapp.repository.json.ShanghufenleiList;
import com.haodaibao.fanbeiapp.repository.json.UserInfo;
import com.haodaibao.fanbeiapp.repository.json.UserTokenInfo;
import com.jayfeng.lesscode.core.SharedPreferenceLess;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

public class Global {

    private static Context sContext;
    private static LoginInfo sLoginInfo;
    private static UserTokenInfo sUserTokenInfo;
    private static UserInfo sUserInfo;
    private static String sJpushAlias;
    private static boolean sPageSwitching = true;
    private static boolean sPageSwitching1 = true;
    private static MyLocation sMyLocation;
    private static HistoryLocation historyLocation ;
    private static OpenCitysInfoResp sOpenCitysResp;
    private static ProvinCityDistInfoResp sProvinCityDistInfoResp;
    private static List<ShanghufenleiList> sShanghufenleiList;
    private static List<DistrictList> sDistrictList;
    private static List<SearchCacheEntity> sSearchCacheEntityList;

    public static Context getContext() {
        return sContext;
    }

    public static void setContext(Context context) {
        sContext = context;
    }

     /*-------------------Hawk-------------------------------------------*/

    /*------Logininfo------*/
    public static LoginInfo getLoginInfo() {
        if (sLoginInfo == null) {
            sLoginInfo = Hawk.get(Constant.PREFERENCE_KEY_USER_LOGIN_INFO);
            if (sLoginInfo == null) {
                synchronized (Global.class) {
                    if (sLoginInfo == null) {
                        sLoginInfo = new LoginInfo();
                    }
                }
            }
        }
        return sLoginInfo;
    }

    public static void setLoginInfo(LoginInfo loginInfo) {
        Hawk.put(Constant.PREFERENCE_KEY_USER_LOGIN_INFO, loginInfo);
        Global.sLoginInfo = loginInfo;
    }

    /*------UserTokenInfo------*/
    public static UserTokenInfo getUserTokenInfo() {
        if (sUserTokenInfo == null) {
            sUserTokenInfo = Hawk.get(Constant.PREFERENCE_KEY_USER_TOKEN_INFO);
            if (sUserTokenInfo == null) {
                synchronized (Global.class) {
                    if (sUserTokenInfo == null) {
                        sUserTokenInfo = new UserTokenInfo();
                    }
                }
            }
        }
        return sUserTokenInfo;
    }

    public static void setUserTokenInfo(UserTokenInfo userTokenInfo) {
        Hawk.put(Constant.PREFERENCE_KEY_USER_TOKEN_INFO, userTokenInfo);
        Global.sUserTokenInfo = userTokenInfo;
    }

    /*------UserInfo------*/
    public static UserInfo getUserInfo() {
        if (sUserInfo == null) {
            sUserInfo = Hawk.get(Constant.PREFERENCE_KEY_USER_INFO);
            if (sUserInfo == null) {
                synchronized (Global.class) {
                    if (sUserInfo == null) {
                        sUserInfo = new UserInfo();
                    }
                }
            }
        }
        return sUserInfo;
    }

    /*------LocationInfo------*/
    public static void setUserInfo(UserInfo userinfo) {
        Hawk.put(Constant.PREFERENCE_KEY_USER_INFO, userinfo);
        Global.sUserInfo = userinfo;
    }

    public static MyLocation getMyLocation() {
        if (sMyLocation == null) {
            sMyLocation = Hawk.get(Constant.PREFERENCE_KEY_USER_LOCATION);
            if (sMyLocation == null) {
                synchronized (Global.class) {
                    if (sMyLocation == null) {
                        sMyLocation = new MyLocation();
                    }
                }
            }
        }
        return sMyLocation;
    }

    public static HistoryLocation getHistoryLocation(){
        if (historyLocation == null){
            historyLocation = Hawk.get(Constant.PREFERENCE_KEY_USER_HISTORY_LOCATION);
            if(historyLocation == null){
                synchronized ((Global.class)){
                    if(historyLocation == null){
                        historyLocation = new HistoryLocation();
                    }
                }
            }
        }
        return historyLocation ;
    }

    public static void setHistoryLocation(HistoryLocation historyLocation){
        Hawk.put(Constant.PREFERENCE_KEY_USER_HISTORY_LOCATION,historyLocation);
        Global.historyLocation = historyLocation;
    }

    public static void setMyLocation(MyLocation myLocation) {
        Hawk.put(Constant.PREFERENCE_KEY_USER_LOCATION, myLocation);
        Global.sMyLocation = myLocation;
    }

    /*------OpenCitys------*/
    public static OpenCitysInfoResp getOpenCitysResp() {
        if (sOpenCitysResp == null) {
            sOpenCitysResp = Hawk.get(Constant.PREFERENCE_KEY_OPEN_CITYS_RESP);
            if (sOpenCitysResp == null) {
                synchronized (Global.class) {
                    if (sOpenCitysResp == null) {
                        sOpenCitysResp = new OpenCitysInfoResp();
                    }
                }
            }
        }
        return sOpenCitysResp;
    }

    public static void setOpenCitysResp(OpenCitysInfoResp openCitysResp) {
        Hawk.put(Constant.PREFERENCE_KEY_OPEN_CITYS_RESP, openCitysResp);
        Global.sOpenCitysResp = openCitysResp;
    }

    /*------OpenCitys------*/
    public static ProvinCityDistInfoResp getProvCityDistResp() {
        if (sProvinCityDistInfoResp == null) {
            sProvinCityDistInfoResp = Hawk.get(Constant.PREFERENCE_KEY_PROVIN_CITY_DIST_RESP);
            if (sProvinCityDistInfoResp == null) {
                synchronized (Global.class) {
                    if (sProvinCityDistInfoResp == null) {
                        sProvinCityDistInfoResp = new ProvinCityDistInfoResp();
                    }
                }
            }
        }
        return sProvinCityDistInfoResp;
    }

    public static void setProvCityDistResp(ProvinCityDistInfoResp provCityDistResp) {
        Hawk.put(Constant.PREFERENCE_KEY_PROVIN_CITY_DIST_RESP, provCityDistResp);
        Global.sProvinCityDistInfoResp = provCityDistResp;
    }


    /*------Categorys------*/
    public static List<ShanghufenleiList> getCategorys() {
        if (sShanghufenleiList == null) {
            sShanghufenleiList = Hawk.get(Constant.PREFERENCE_KEY_CATEGORYS);
            if (sShanghufenleiList == null) {
                synchronized (Global.class) {
                    if (sShanghufenleiList == null) {
                        sShanghufenleiList = new ArrayList<>();
                    }
                }
            }
        }
        return sShanghufenleiList;
    }

    public static void setCategorys(List<ShanghufenleiList> shanghufenleiResp) {
        Hawk.put(Constant.PREFERENCE_KEY_CATEGORYS, shanghufenleiResp);
        Global.sShanghufenleiList = shanghufenleiResp;
    }

    /*------Shangquans------*/
    public static List<DistrictList> getShangquans() {
        if (sDistrictList == null) {
            sDistrictList = Hawk.get(Constant.PREFERENCE_KEY_SHANGQUANS);
            if (sDistrictList == null) {
                synchronized (Global.class) {
                    if (sDistrictList == null) {
                        sDistrictList = new ArrayList<>();
                    }
                }
            }
        }
        return sDistrictList;
    }

    public static void setShangquans(List<DistrictList> districtLists) {
        Hawk.put(Constant.PREFERENCE_KEY_SHANGQUANS, districtLists);
        Global.sDistrictList = districtLists;
    }

    /*-------------------SharedPreferenceLess-------------------------------*/


    /*---JPushAlias---*/
    public static String getsJpushAlias() {
        if (TextUtils.isEmpty(sJpushAlias)) {
            sJpushAlias = SharedPreferenceLess.$get(Constant.PREFERENCE_KEY_USER_JPUSH_ALIAS, "");
        }
        return sJpushAlias;
    }

    public static void setsJpushAlias(String sJpushAlias) {
        SharedPreferenceLess.$put(Constant.PREFERENCE_KEY_USER_JPUSH_ALIAS, sJpushAlias);
        Global.sJpushAlias = sJpushAlias;
    }

    /*---FirstUsing---*/
    public static boolean getFirstUsing() {
        return SharedPreferenceLess.$get(Constant.PREFERENCE_KEY_FIRST_USING, false);
    }

    public static void setFirstUsing(boolean firstusing) {
        SharedPreferenceLess.$put(Constant.PREFERENCE_KEY_FIRST_USING, firstusing);
    }

    /*---PhotoNumber---*/
    public static String getUsrePhotoNumber() {
        return SharedPreferenceLess.$get(Constant.PREFERENCE_KEY_USER_PHONE, "");
    }

    public static void setUsrePhotoNumber(String photoNumber) {
        SharedPreferenceLess.$put(Constant.PREFERENCE_KEY_USER_PHONE, photoNumber);
    }

    /*---SelectCity---*/
    public static String getSelectCity() {
        return SharedPreferenceLess.$get(Constant.PREFERENCE_KEY_SELECTCITY, "");
    }

    public static void setSelectCity(String selectCity) {
        SharedPreferenceLess.$put(Constant.PREFERENCE_KEY_SELECTCITY, selectCity);
    }

    public static void setMapSelectCity(String selectCity) {
        SharedPreferenceLess.$put(Constant.PREFERENCE_KEY_SELECTCITY, selectCity);
    }

    /*---lastSelectCity---*/
    public static String getLastSelectCity() {
        return SharedPreferenceLess.$get(Constant.PREFERENCE_KEY_LAST_SELECTCITY, "");
    }

    public static void setLastSelectCity(String selectCity) {
        SharedPreferenceLess.$put(Constant.PREFERENCE_KEY_LAST_SELECTCITY, selectCity);
    }

    /*---SelectCityCode---*/
    public static String getSelectCityCode() {
        return SharedPreferenceLess.$get(Constant.PREFERENCE_KEY_SELECTCITYCODE, "");
    }

    public static void setSelectCityCode(String selectCityCode) {
        SharedPreferenceLess.$put(Constant.PREFERENCE_KEY_SELECTCITYCODE, selectCityCode);
    }

    /*---SelectCitySiteType---*/
    public static String getSelectCitySiteType() {
        return SharedPreferenceLess.$get(Constant.PREFERENCE_KEY_SELECTCITYSITETYPE, "");
    }

    public static void setSelectCitySiteType(String sitetype) {
        SharedPreferenceLess.$put(Constant.PREFERENCE_KEY_SELECTCITYSITETYPE, sitetype);
    }

    /*---SelectCityCode---*/
    public static String getmapSelectCityCode() {
        return SharedPreferenceLess.$get(Constant.PREFERENCE_KEY_MAPSELECTCITYCODE, "");
    }

    public static void setmapSelectCityCode(String selectCityCode) {
        SharedPreferenceLess.$put(Constant.PREFERENCE_KEY_MAPSELECTCITYCODE, selectCityCode);
    }

    public static void loginOutclear() {
        OkHttpClientManager.clearCookie();
        setLoginInfo(new LoginInfo());
        setUserInfo(new UserInfo());
        setUserTokenInfo(new UserTokenInfo());
        setsJpushAlias("");
    }

    public static void setSelectCityNameAndCodeByGaodeNameAdcode() {
        List<OpenCityBean> openCityBeans = GreenDaoDbHelp.queryCityByGaodeNameAdCode().list();
        if (openCityBeans.size() > 0) {
            Global.setLastSelectCity(Global.getSelectCity());
            Global.setSelectCity(openCityBeans.get(0).getName());
            Global.setSelectCityCode(openCityBeans.get(0).getCode());
            Global.setSelectCitySiteType(openCityBeans.get(0).getType());
        } else {
            Global.setLastSelectCity(Global.getSelectCity());
            Global.setSelectCity(Constant.SELECTCITY);
            Global.setSelectCityCode(Constant.SELECTCITYCODE);
            Global.setSelectCitySiteType(Constant.SELECTCITYSITETYPE);
        }
    }
}
