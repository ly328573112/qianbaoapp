package com.baseandroid.widget;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.haodaibao.fanbeiapp.R;

/**
 * 开发者：LuoYi(文件上传)
 * Time: 2017 13:32 2017/7/31 07
 */

public class LoadingFileHintDialog extends Dialog {

    private ObjectAnimator icon_anim;
    private ImageView rotationIv;
    private ImageView successIv;
    private TextView processTv;
    private TextView hintTextTv;

    public LoadingFileHintDialog(@NonNull Context context) {
        super(context, R.style.LoadingDialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_upload_loading);
        setCanceledOnTouchOutside(false);
        setOnKeyListener(keylistener);
        rotationIv = (ImageView) findViewById(R.id.rotation_iv);
        successIv = (ImageView) findViewById(R.id.success_iv);
        processTv = (TextView) findViewById(R.id.process_tv);
        hintTextTv = (TextView) findViewById(R.id.hint_text_tv);

        icon_anim = ObjectAnimator.ofFloat(rotationIv, "rotation", 0.0F, 359.0F);
        icon_anim.setRepeatCount(-1);
        icon_anim.setDuration(1000);
        LinearInterpolator interpolator = new LinearInterpolator();
        icon_anim.setInterpolator(interpolator); //设置匀速旋转，不卡顿
        icon_anim.start();
    }

    public void setProcessText(String processText) {
        processTv.setText(processText);
    }

    public void cleanFileAnimator() {
        icon_anim.end();
        rotationIv.setVisibility(View.INVISIBLE);
        processTv.setVisibility(View.INVISIBLE);
        successIv.setVisibility(View.VISIBLE);
        hintTextTv.setText("上传成功");
    }

    OnKeyListener keylistener = new OnKeyListener() {
        public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
            if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
                return true;
            } else {
                return false;
            }
        }
    };
}
