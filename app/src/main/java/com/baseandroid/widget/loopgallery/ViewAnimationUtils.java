package com.baseandroid.widget.loopgallery;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.View;
import android.view.animation.LinearInterpolator;

import com.jayfeng.lesscode.core.DisplayLess;

/**
 * date: 2017/11/3.
 * 功能描述:</br>
 * 注意事项:</br>
 * ------------1.本类由Helegehe开发,阅读、修改时请勿随意修改代码排版格式后提交到git。</br>
 * ------------2.阅读本类时，发现不合理请及时指正.</br>
 * ------------3.如需在本类内部进行修改,请先联系Helegehe,若未经同意修改此类后造成损失本人概不负责。</br>
 * author: Helegehe
 * mail: hepeng@qianbaocard.com
 */

public class ViewAnimationUtils {
    public static void startAnimation(Context context, final View view, final View view2, final int isVisible, final AnimationListener animationListener) {
        Animator operatingAnim = isVisible == 0 ? hideAnimation(view) : showAnimation(view);
        Animator operatingAnim2 = isVisible == 0 ? hideAnimationAlPha(view2) : showAnimationAlpha(view2);
        LinearInterpolator lin = new LinearInterpolator();
        operatingAnim.setInterpolator(lin);
        operatingAnim2.setInterpolator(lin);
        operatingAnim.setDuration(200);
        operatingAnim2.setDuration(200);
        operatingAnim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                if (isVisible != 0) {
                    view.setVisibility(View.VISIBLE);
//                    view2.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (isVisible == 0) {
                    view.setVisibility(View.GONE);
//                    view2.setVisibility(View.GONE);
                } else {
                    if (animationListener != null) {
                        animationListener.end();
                    }
                }
            }
        });
        operatingAnim2.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                if (isVisible != 0) {
                    view2.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (isVisible == 0) {
                    view2.setVisibility(View.GONE);
                } else {
                    if (animationListener != null) {
                        animationListener.end();
                    }
                }
            }
        });
        operatingAnim.start();
        operatingAnim2.start();
    }

    public static void startAnimation(Context context, final View view, final int isVisible, final AnimationListener animationListener) {
        Animator operatingAnim = isVisible == 0 ? hideAnimation(view) : showAnimation(view);
        LinearInterpolator lin = new LinearInterpolator();
        operatingAnim.setInterpolator(lin);
        operatingAnim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                if (isVisible != 0) {
                    view.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (isVisible == 0) {
                    view.setVisibility(View.GONE);
                } else {
                    if (animationListener != null) {
                        animationListener.end();
                    }
                }
            }
        });
        operatingAnim.start();
    }

    public interface AnimationListener {
        void start();

        void end();
    }

    public static Animator showAnimation(View view) {
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
                ObjectAnimator.ofFloat(view, "alpha", 0f, 1f),
                ObjectAnimator.ofFloat(view, "scaleX", 0.1f, 1f),
                ObjectAnimator.ofFloat(view, "scaleY", 0.1f, 1f)
        );
        return set;
    }

    public static Animator showAnimationAlpha(View view) {
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
                ObjectAnimator.ofFloat(view, "alpha", 0f, 1f)
        );
        return set;
    }

    public static Animator hideAnimation(View view) {
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
                ObjectAnimator.ofFloat(view, "alpha", 1f, 0f),
                ObjectAnimator.ofFloat(view, "scaleX", 1f, 0.1f),
                ObjectAnimator.ofFloat(view, "scaleY", 1f, 0.1f)
        );
        return set;
    }

    public static Animator hideAnimationAlPha(View view) {
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
                ObjectAnimator.ofFloat(view, "alpha", 1f, 0f)

        );
        return set;
    }

    public static void verticalRundown(final View view, final Context context) {

        ObjectAnimator animator1 = (ObjectAnimator) ObjectAnimator.ofFloat(view, "translationY", 0, DisplayLess.$dp2px(133))
                .setDuration(800);
        animator1.start();
    }

    public static void verticalRunup(final View view) {
        ObjectAnimator animator1 = (ObjectAnimator) ObjectAnimator.ofFloat(view, "translationY", view.getTranslationY(), 0)
                .setDuration(300);
        animator1.start();
    }
}
