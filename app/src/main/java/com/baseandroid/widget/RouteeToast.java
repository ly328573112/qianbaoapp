package com.baseandroid.widget;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.app.ActivityManager;
import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.ImageView;

import com.baseandroid.config.Global;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.module.home.HomeActivity;
import com.haodaibao.fanbeiapp.module.maphome.MaphomeActivity;

import java.util.List;

public class RouteeToast implements OnTouchListener {
    private View                       mView;
    private WindowManager              mWM;
    private WindowManager.LayoutParams mParams;
    private WindowManager.LayoutParams mTipViewParams;
    private Context                    mContext;
    private int                        startX;
    private int                        startY;
    private int[]                      rocketLocation;
    private int[]                      tipLocation;

    private static RouteeToast sRouteeToast = null;
    private ClickListen mListener;
    private boolean     mHasMove;
    private boolean     mCurrentState;
    private ImageView   mIv;
    private Handler     mHandler;

    public static RouteeToast getInstance() {
        synchronized (RouteeToast.class) {
            if (sRouteeToast == null) {
                sRouteeToast = new RouteeToast(Global.getContext());
            }
        }
        return sRouteeToast;
    }

    private RouteeToast(Context context) {
        this.mContext = context;
        mWM = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        mParams = new WindowManager.LayoutParams();
        mParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        mParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        mParams.format = PixelFormat.TRANSLUCENT;// 设置窗口以像素级别来显示
        mParams.type = WindowManager.LayoutParams.TYPE_PRIORITY_PHONE;// 设置当前的窗口的类型
        mParams.setTitle("Toast");
        // 设置窗口的一些标记
        mParams.flags = WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                                | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        // | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE;
        //让火箭设置在左上角
        mParams.gravity = Gravity.LEFT | Gravity.TOP;


        //创建 提示框的布局参数
        mTipViewParams = new WindowManager.LayoutParams();
        mTipViewParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        mTipViewParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        mTipViewParams.format = PixelFormat.TRANSLUCENT;// 设置窗口以像素级别来显示
        mTipViewParams.type = WindowManager.LayoutParams.TYPE_TOAST;// 设置当前的窗口的类型
        mTipViewParams.setTitle("Toast");
        // 设置窗口的一些标记
        mTipViewParams.flags = WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                                       | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        // | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE;
        //让提示框设置在底部并水平居中
        mTipViewParams.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;

        //创建记录火箭与提示框的数组对象
        rocketLocation = new int[2];

        tipLocation = new int[2];
    }

    public void showToast() {
        mView = View.inflate(mContext, R.layout.view_toast, null);// 在窗口中需要显示的视图对象
        mView.setOnTouchListener(this);
        mIv = (ImageView) mView.findViewById(R.id.iv);
        mWM.addView(mView, mParams);
    }

    public void cancelToast() {
        if (mView != null) {
            if (mView.getParent() != null) {
                mWM.removeView(mView);
            }
            mView = null;
        }
    }

    public void hideToast() {
        if (mView != null) {
            if (mView.getParent() != null) {
                mWM.removeView(mView);
            }
        }
    }

    /**
     * v 被触摸的view event 触摸事件
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                mHasMove = false;
                startX = (int) event.getRawX();
                startY = (int) event.getRawY();
                break;
            case MotionEvent.ACTION_MOVE:
                mHasMove = true;
                int endX = (int) event.getRawX();
                int endY = (int) event.getRawY();

                int diffX = endX - startX;
                int diffY = endY - startY;

                mParams.x = (int) (mParams.x + diffX);
                mParams.y += diffY;

                mWM.updateViewLayout(mView, mParams);

                // 5.重新初始化起始点
                startX = endX;
                startY = endY;
            case MotionEvent.ACTION_UP:// 手指抬起
                locateToast();
                if (!mHasMove) {
                    if (mListener != null) {
                        checkClick();
                    }
                }
                break;

            default:
                break;
        }
        return true;
    }

    private void checkClick() {
        ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> infos = am.getRunningTasks(100);
        ActivityManager.RunningTaskInfo runningTaskInfo = infos.get(0);
        if (runningTaskInfo.topActivity.getClassName().equals(MaphomeActivity.class.getName())) {
            mListener.click(true);
        } else if (runningTaskInfo.topActivity.getClassName().equals(HomeActivity.class.getName())) {
            mListener.click(false);
        }
        if (mHandler == null) {
            mHandler = new Handler(Looper.getMainLooper());
        }
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                changeState();
            }
        }, 220);
    }

    //发射火箭
    private void locateToast() {
        //值动画：集没有动，又没有画，只做数据的模拟
        int width = mWM.getDefaultDisplay().getWidth();
        ValueAnimator va = ValueAnimator.ofInt(mParams.x, 0);
        va.setDuration(200);
        va.addUpdateListener(new AnimatorUpdateListener() {
            //将当前正在模拟的值，返回回来
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer animatedValue = (Integer) animation.getAnimatedValue();
                //				System.out.println(animatedValue);
                //将对应的值获取到并设置给布局参数，动态修改火箭窗口的位置
                mParams.x = animatedValue;
                mWM.updateViewLayout(mView, mParams);
            }
        });
        va.start();
    }

    public void changeState() {
        if (mView != null) {
            if (mCurrentState) {
                mIv.setImageResource(R.drawable.icon_map_go_old);
                mCurrentState = false;
            } else {
                mIv.setImageResource(R.drawable.icon_map_go_new);
                mCurrentState = true;
            }
        }
    }

    public interface ClickListen {
        void click(boolean finish);
    }

    public void setClickListen(ClickListen listen) {
        mListener = listen;
    }
}
