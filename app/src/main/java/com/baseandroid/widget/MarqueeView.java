package com.baseandroid.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.baseandroid.utils.CommonUtils;
import com.baseandroid.widget.customimageview.CircleImageViewWithStroke;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.haodaibao.fanbeiapp.R;
import com.haodaibao.fanbeiapp.repository.json.MarqueeBean;

import java.util.ArrayList;
import java.util.List;

public class MarqueeView extends ViewFlipper {

    private Context mContext;
    private List<MarqueeBean> mNotices = new ArrayList<>();
    private List<MarqueeBean> newNotices;
    private boolean isSetAnimDuration = false;
    private OnItemClickListener onItemClickListener;

    private int interval = 2000;
    private int animDuration = 500;
    private int textSize = 14;
    private int textColor = 0xffffffff;

    private boolean singleLine = false;
    private int gravity = Gravity.LEFT | Gravity.CENTER_VERTICAL;
    private static final int TEXT_GRAVITY_LEFT = 0, TEXT_GRAVITY_CENTER = 1, TEXT_GRAVITY_RIGHT = 2;

    public MarqueeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        this.mContext = context;

        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.MarqueeViewStyle, defStyleAttr, 0);
        interval = typedArray.getInteger(R.styleable.MarqueeViewStyle_mvInterval, interval);
        isSetAnimDuration = typedArray.hasValue(R.styleable.MarqueeViewStyle_mvAnimDuration);
        singleLine = typedArray.getBoolean(R.styleable.MarqueeViewStyle_mvSingleLine, false);
        animDuration = typedArray.getInteger(R.styleable.MarqueeViewStyle_mvAnimDuration, animDuration);
        if (typedArray.hasValue(R.styleable.MarqueeViewStyle_mvTextSize)) {
            textSize = (int) typedArray.getDimension(R.styleable.MarqueeViewStyle_mvTextSize, textSize);
        }
        textColor = typedArray.getColor(R.styleable.MarqueeViewStyle_mvTextColor, textColor);
        int gravityType = typedArray.getInt(R.styleable.MarqueeViewStyle_mvGravity, TEXT_GRAVITY_LEFT);
        switch (gravityType) {
            case TEXT_GRAVITY_CENTER:
                gravity = Gravity.CENTER;
                break;
            case TEXT_GRAVITY_RIGHT:
                gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
                break;
        }
        typedArray.recycle();

        setFlipInterval(interval);

        Animation animIn = AnimationUtils.loadAnimation(mContext, R.anim.anim_marquee_in);
        if (isSetAnimDuration) {
            animIn.setDuration(animDuration);
        }
        setInAnimation(animIn);

        Animation animOut = AnimationUtils.loadAnimation(mContext, R.anim.anim_marquee_out);
        if (isSetAnimDuration) {
            animOut.setDuration(animDuration);
        }
        setOutAnimation(animOut);
    }

    public List<MarqueeBean> getmNotices() {
        return mNotices;
    }

    public void startWithList(List<MarqueeBean> notices) {
        newNotices = notices;
        stopFlipping();
        mNotices.clear();
        for (int i = 0; i < getChildCount(); i++) {
            View view = getChildAt(i);
            view.clearAnimation();
        }
        removeAllViews();

        for (MarqueeBean item : notices) {
            mNotices.add(item);
        }

        start();
    }

    public int getSize() {
        return mNotices.size();
    }

    public void resetMarquee() {
        stopFlipping();
        for (int i = 0; i < getChildCount(); i++) {
            View view = getChildAt(i);
            view.clearAnimation();
            view.setVisibility(GONE);
        }

    }

    public boolean start() {
        if (mNotices == null || mNotices.size() == 0) {
            return false;
        }
        for (int i = 0; i < mNotices.size(); i++) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.fragment_marquee_view, null);
            setUpTextView(view, mNotices.get(i), i);

            final int finalI = i;
            final View viewLayout = view;
            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick((String) v.getTag(), viewLayout);
                    }
                }
            });
            addView(view);
        }

        if (mNotices.size() > 1) {
            startFlipping();
        }

        return true;
    }

    private void setUpTextView(View view, MarqueeBean marqueeBean, int position) {
        final CircleImageViewWithStroke marqueeIconCiv = (CircleImageViewWithStroke) view.findViewById(R.id.marquee_icon_civ);
        TextView marqueeTitleTv = (TextView) view.findViewById(R.id.marquee_title_tv);
        TextView marqueeContentTv = (TextView) view.findViewById(R.id.marquee_content_tv);
        String imgurl = CommonUtils.getImageUrl(marqueeBean.imageUrl);
        int commIcon = R.drawable.yaohe_default_icon;
        Glide.with(mContext)
                .load(imgurl)
                .asBitmap()
                .placeholder(commIcon)
                .error(commIcon)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        marqueeIconCiv.setImageBitmap(resource);
                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        super.onLoadFailed(e, errorDrawable);
                    }
                });
        marqueeTitleTv.setText(marqueeBean.titleText);
        marqueeContentTv.setText(marqueeBean.contentText);
        view.setTag(marqueeBean.messageNo);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(String position, View view);
    }

    private OnReachLastListener mOnReachLastListener;

    public interface OnReachLastListener {
        void onReachLast();
    }

    public OnReachLastListener getmOnReachLastListener() {
        return mOnReachLastListener;
    }

    public void setmOnReachLastListener(OnReachLastListener mOnReachLastListener) {
        this.mOnReachLastListener = mOnReachLastListener;
    }

}
