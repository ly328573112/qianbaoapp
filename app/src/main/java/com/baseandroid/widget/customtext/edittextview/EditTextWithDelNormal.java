package com.baseandroid.widget.customtext.edittextview;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;

import com.haodaibao.fanbeiapp.R;

public class EditTextWithDelNormal extends android.support.v7.widget.AppCompatEditText {

    private Drawable imgInable;
    private Drawable imgAble;
    private Context mContext;

    public EditTextWithDelNormal(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public EditTextWithDelNormal(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        init();
    }

    public EditTextWithDelNormal(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    private void init() {

        imgInable = ContextCompat.getDrawable(mContext, R.drawable.delete_gray_img);
        imgAble = ContextCompat.getDrawable(mContext, R.drawable.delete_gray_img);
        // 事件监听
        addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (hasFocus()) {
                    setDrawable();
                }

            }
        });
        setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                } else {
                    setDrawable();
                }
            }
        });
//        setDrawable();
    }

    // 设置删除图片
    private void setDrawable() {
        if (length() >= 1) {
            // 表示没有输入文本内容时显示的图片，个人觉得不需要设置默认图片
            setCompoundDrawablesWithIntrinsicBounds(null, null, imgInable, null);
        } else {
            setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        }
    }

    // 处理删除事件
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (imgAble != null && event.getAction() == MotionEvent.ACTION_UP) {
            int eventX = (int) event.getRawX();
            int eventY = (int) event.getRawY();
            Rect rect = new Rect();
            getGlobalVisibleRect(rect);
            rect.left = rect.right - 70;
            if (rect.contains(eventX, eventY)) {
                setText("");
            }
        }
        return super.onTouchEvent(event);
    }


    /**
     * 设置晃动动画
     */
    public void setShakeAnimation() {
        this.startAnimation(shakeAnimation(5));
    }

    /**
     * 晃动动画
     *
     * @param counts 1秒钟晃动多少下
     * @return
     */
    private Animation shakeAnimation(int counts) {
        Animation translateAnimation = new TranslateAnimation(0, 10, 0, 0);
        translateAnimation.setInterpolator(new CycleInterpolator(counts));
        translateAnimation.setDuration(1000);
        return translateAnimation;
    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        super.onSelectionChanged(selStart, selEnd);
        // 保证光标始终在最后面
//        if (selStart == selEnd) {
//            // 防止不能多选
//            setSelection(getText().length());
//        }
    }

}
