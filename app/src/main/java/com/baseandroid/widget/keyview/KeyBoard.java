package com.baseandroid.widget.keyview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.haodaibao.fanbeiapp.R;

import static android.graphics.Color.WHITE;

/**
 * Created by ${mario} on 2015/12/16.
 */
@SuppressLint("ClickableViewAccessibility")
public class KeyBoard extends LinearLayout implements View.OnClickListener,
        View.OnTouchListener {

    /*protected final int COLOR_V_LINE = Color.GRAY;
    protected final int COLOR_H_LINE = Color.GRAY;*/
    protected final int COLOR_V_LINE = getResources().getColor(R.color.bgcolor);
    protected final int COLOR_H_LINE = getResources().getColor(R.color.bgcolor);
    protected final int SIZE_V_LINE = 1;
    protected final int SIZE_H_LINE = 1;
    //    protected final int COLOR_KEYBACK_NORMAL = Color.rgb(243, 243, 243);
    protected final int COLOR_KEYBACK_NORMAL = getResources().getColor(R.color.white);
    protected final int COLOR_KEYBACK_PRESSED = Color.rgb(201, 201, 201);
    protected final int COLOR_DEL_NORMAL = Color.rgb(229, 104, 104);
    protected final int COLOR_DEL_PRESSED = Color.rgb(201, 205, 208);
    //    protected final int COLOR_OK_NORMAL = Color.rgb(104, 208, 121);
//    protected final int COLOR_OK_PRESSED = Color.rgb(201, 205, 208);
    protected final int COLOR_OK_NORMAL = Color.rgb(247, 143, 18);
    protected final int COLOR_OK_PRESSED = Color.rgb(241, 132, 0);

    //    protected final int COLOR_KEY_NORMAL = Color.BLACK;
    protected final int COLOR_KEY_NORMAL = getResources().getColor(R.color.black);
    protected final int COLOR_KEY_PRESSED = WHITE;

    protected View[] keyWordViews = null;
    /**
     *
     */
    protected static final int ID_DEL = 0xFFF001;
    protected static final int ID_OK = ID_DEL + 1;
    protected static final int ID_DOT = ID_OK + 1;
    protected static final int ID_CLEAN = ID_DOT + 1;
    protected static final int ID_EMPTY = ID_CLEAN + 1;

    protected StringBuilder number = new StringBuilder();

    protected String inputString = "";
    protected String resultString = "";

    protected KeyBoradListener listener;

    private int maxlength = 17;

    public KeyBoard(Context context) {
        super(context);
        init();
    }

    public KeyBoard(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        keyWordViews = new View[getRowCounts() * getColumnCounts()];
        setOrientation(LinearLayout.VERTICAL);
        generateView();
        setContent();
    }

    public void setListener(KeyBoradListener listener) {
        this.listener = listener;
    }

    public void setMaxlength(int maxlength) {
        this.maxlength = maxlength;
    }

    /**
     *
     */
    public void reset() {
        this.inputString = "";
        this.resultString = "";
        this.number = new StringBuilder();
    }

    /**
     *
     */
    protected void setContent() {
    }

    protected void generateView() {
    }

    protected int getRowCounts() {
        return 4;
    }

    protected int getColumnCounts() {
        return 3;
    }

    protected TextView getTextView(int id) {
        TextView tv = new TextView(getContext());
        tv.setId(id);
        tv.setGravity(Gravity.CENTER);
        WindowManager wm = (WindowManager) getContext()
                .getSystemService(Context.WINDOW_SERVICE);

        int height = wm.getDefaultDisplay().getHeight();
        LayoutParams mTextViewParams = new LayoutParams(
                LayoutParams.MATCH_PARENT,
                height / 14);
        mTextViewParams.weight = 1;
        mTextViewParams.setMargins(1, 1, 1, 1);

        tv.setLayoutParams(mTextViewParams);
        tv.setPadding(5, 5, 5, 5);
        tv.setTextSize(22);
        tv.setTextColor(COLOR_KEY_NORMAL);
        tv.setBackgroundColor(COLOR_KEYBACK_NORMAL);
        tv.getPaint().setFakeBoldText(false);
        tv.setOnClickListener(this);
        tv.setOnTouchListener(this);
        return tv;
    }

    protected View getHLine() {
        View line = new View(getContext());
        LayoutParams mViewParams = new LayoutParams(SIZE_H_LINE,
                LayoutParams.MATCH_PARENT);
        line.setBackgroundColor(COLOR_H_LINE);
        line.setLayoutParams(mViewParams);
        return line;
    }

    protected View getVLine() {
        View line = new View(getContext());
        LayoutParams mViewParams = new LayoutParams(
                LayoutParams.MATCH_PARENT, SIZE_V_LINE);
        line.setBackgroundColor(COLOR_V_LINE);
        line.setLayoutParams(mViewParams);
        return line;
    }

    protected void setDeleteTextView(TextView tv) {
//        tv.setTextColor(Color.BLACK);
//        tv.setBackgroundColor(COLOR_KEYBACK_NORMAL);
//        Drawable d = getResources().getDrawable(R.drawable.icon_delete);
//        d.setBounds(0,0,d.getMinimumWidth(),d.getMinimumHeight());
//        tv.setCompoundDrawables(d,null,null,null);
//        tv.setText("删除");
        // tv.setBackgroundResource(R.drawable.icon_delete);

        tv.setBackgroundResource(R.drawable.icon_delete_normal);

//        Drawable nav_up=getResources().getDrawable(R.mipmap.icon_delete_normal);
//        nav_up.setBounds(0, 0, nav_up.getMinimumWidth(), nav_up.getMinimumHeight());
//        tv.setCompoundDrawables(null, null, nav_up, null);
    }

    protected void setConfirmTextView(TextView tv) {
        tv.setTextColor(WHITE);
        tv.setBackgroundColor(COLOR_OK_NORMAL);
        tv.setText("确定");
    }

    protected void setDotTextView(TextView tv) {
        tv.setTextColor(COLOR_KEY_NORMAL);
        tv.setBackgroundColor(COLOR_KEYBACK_NORMAL);
        tv.setText(".");
    }

    protected void setEmptyTextView(TextView tv) {
        tv.setTextColor(WHITE);
        tv.setBackgroundColor(WHITE);
        tv.setText("");
    }

    protected void setKeyTextView(TextView tv, String text) {
        tv.setText(text);
        tv.setTextColor(COLOR_KEY_NORMAL);
        tv.setBackgroundColor(COLOR_KEYBACK_NORMAL);
    }

    private void setKeyState(TextView tv, boolean isPressed) {
        int id = tv.getId();
        switch (id) {
            case ID_DEL:
                tv.setBackgroundResource(isPressed ? R.drawable.icon_delete_pressed
                        : R.drawable.icon_delete_normal);
                break;
            case ID_OK:
                tv.setBackgroundColor(isPressed ? COLOR_OK_PRESSED
                        : COLOR_OK_NORMAL);
                break;
            case ID_CLEAN:
                tv.setBackgroundColor(isPressed ? COLOR_DEL_PRESSED
                        : COLOR_DEL_NORMAL);
                break;
            case ID_EMPTY:
                tv.setBackgroundColor(isPressed ? COLOR_KEY_PRESSED
                        : COLOR_KEY_PRESSED);
                break;
            default:
                tv.setBackgroundColor(isPressed ? COLOR_KEYBACK_PRESSED
                        : COLOR_KEYBACK_NORMAL);
                tv.setTextColor(isPressed ? COLOR_KEY_PRESSED : COLOR_KEY_NORMAL);
                break;
        }
    }

    @Override
    public void onClick(View arg0) {
        int id = arg0.getId();
        switch (id) {
            case ID_DEL:
                del();
                break;
            case ID_OK:
                ok();
                return;
            case ID_DOT:
                dot();
                break;
            case ID_CLEAN:
                clean();
                break;
            case ID_EMPTY:
                break;

            default:
                inputNumber(((TextView) arg0).getText().toString());
                break;
        }
        dealInputText();
    }

    protected void dealInputText() {
        if (number != null) {
            inputString = number.toString();
            resultString = number.toString();
            if (this.listener != null) {
                this.listener.onKeyBoardClick(inputString, resultString);
            }
        }
    }

    protected void showInputText() {
    }

    public void clean() {
        number = null;
    }

    public void clear(String str) {
        if (number.length() > 0) {
            number.delete(0, number.length());
        }
        number.append(str);
    }

    protected void del() {

        number.append("#");
//        if (number.length() > 0) {
//            if(number.charAt(number.length()-1)==' '){
//                number = number.deleteCharAt(number.length() - 1);
//                number = number.deleteCharAt(number.length() - 1);
//            }else {
//                number = number.deleteCharAt(number.length() - 1);
//            }
//
//        } else {
//            if (this.listener != null) {
//                this.listener.cancel();
//            }
//        }
    }

    protected void ok() {
        if (this.listener != null) {
            this.listener.ok(resultString);
        }
    }

    protected void dot() {
        String str = number.toString();
//        if (str.length() == 0) {
//            number.append("0.");
//        } else
        if (!str.contains(".")) {
            number.append(".");
        }
    }

    protected void addCommand(String command) {

    }

    protected void inputNumber(String text) {
        if (number.length() == maxlength) {
            return;
        }
//        if (number.length() == 4 || number.length() == 9|| number.length() == 14|| number.length() == 19) {
//            number.append(" ");
//        }
        number.append(text);
    }

    @Override
    public boolean onTouch(View arg0, MotionEvent arg1) {
        int action = arg1.getAction();
        setKeyState((TextView) arg0, action != MotionEvent.ACTION_OUTSIDE
                && action != MotionEvent.ACTION_UP);
        return false;
    }

    public interface KeyBoradListener {

        void onKeyBoardClick(String inputStr, String resultString);

        /**
         * 确定
         */
        void ok(Object o);

        /**
         * 取消
         */
        void cancel();

    }
}
