package com.baseandroid.widget.keyview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by ${mario} on 2015/12/16.
 */
public class InputNumberKeyBoard extends KeyBoard {
    public InputNumberKeyBoard(Context context) {
        super(context);
    }

    public InputNumberKeyBoard(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    protected void setContent() {
        int keyNum = 1;
        for (int i = 0; i < keyWordViews.length; i++) {
            View view = keyWordViews[i];
            if (view != null) {
                switch (view.getId()) {
                    case ID_DEL:
                        setDeleteTextView((TextView) view);
                        break;
                    case ID_OK:
                        setConfirmTextView((TextView)view);
                        break;
                    case ID_DOT:
                        setDotTextView((TextView)view);
                        break;
                    default:
                        if (i!=11){
                        setKeyTextView((TextView) view, (keyNum++) % 10 + "");
                        }
                        break;
                }
            }
        }
    }

    private List<String> randomKeyNum() {
        List<String> keySet = new ArrayList<String>(10);
        Random random = new Random();
        while (keySet.size() < 10) {
            String num = random.nextInt(10) + "";
            if (!keySet.contains(num)) {
                keySet.add(num);
            }
        }
        return keySet;
    }

    protected void generateView() {
        int row = getRowCounts();
        int column = getColumnCounts();
        setWeightSum(row);
        for (int i = 0; i < row; i++) {
            LinearLayout mLinearLayout = new LinearLayout(getContext());
            LayoutParams mLinearLayoutParams = new LayoutParams(
                    LayoutParams.MATCH_PARENT,
                    LayoutParams.MATCH_PARENT);
            mLinearLayoutParams.weight = 1;
            mLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
            mLinearLayout.setLayoutParams(mLinearLayoutParams);
            mLinearLayout.setWeightSum(column);
            for (int j = 0; j < column; j++) {
                int id = i * column + j;
                int tvID = id;

                if (id == 9) {
//                    ttvID = ID_DEL;
                    tvID = ID_DOT;
                } else if (id == 11) {
                    tvID = ID_DEL;
//                    tvID = ID_OK;
                }else if (id == 12){
                    return;
                }
                TextView tv = getTextView(tvID);
                keyWordViews[id] = tv;
                mLinearLayout.addView(tv);

//				if (j != column - 1) {
//					View line = getHLine();
//					mLinearLayout.addView(line);
//				}
            }
            addView(mLinearLayout);
//			if (i != row - 1) {
//				View line = getVLine();
//				addView(line);
//			}
        }
    }

}
