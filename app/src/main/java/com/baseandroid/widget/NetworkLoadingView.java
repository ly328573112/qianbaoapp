package com.baseandroid.widget;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.haodaibao.fanbeiapp.R;

/**
 * 开发者：LuoYi
 * Time: 2017 16:28 2017/11/16 11
 */

public class NetworkLoadingView extends LinearLayout {

    private String content = null;
    private AnimationDrawable animationDrawable;

    public NetworkLoadingView(Context context) {
        super(context);
    }

    public NetworkLoadingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public NetworkLoadingView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void initView(Context context) {
        LayoutInflater mInflater = LayoutInflater.from(context);
        View myView = mInflater.inflate(R.layout.network_loading_view, null);
        addView(myView);

        ImageView progress = (ImageView) findViewById(R.id.progress);
        TextView dialogTv = (TextView) findViewById(R.id.dialog_tv);
        if (TextUtils.isEmpty(content)) {
            progress.setVisibility(View.VISIBLE);
            dialogTv.setVisibility(View.GONE);
            progress.setImageResource(R.drawable.loading_toast_anim);
            animationDrawable = (AnimationDrawable) progress.getDrawable();
            animationDrawable.start();
        } else {
            progress.setVisibility(View.GONE);
            dialogTv.setVisibility(View.VISIBLE);
            dialogTv.setText(content);
        }
    }

    public void setContent(String content) {
        this.content = content;
    }

}
