package com.baseandroid.widget.customscrollview;

/**
 * Created by hdb on 2017/7/6.
 */

public interface ScrollListener {
    void onScrollChanged(int l, int t, int oldl, int oldt);
}
