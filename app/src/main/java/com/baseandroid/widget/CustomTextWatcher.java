package com.baseandroid.widget;

import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.widget.EditText;

import com.baseandroid.base.BaseActivity;

/**
 * 开发者：LuoYi
 * Time: 2017 18:10 2017/9/20 09
 */

public class CustomTextWatcher implements TextWatcher {

    BaseActivity activity;
    EditText editText;
    int astrictSize;

    public CustomTextWatcher(BaseActivity activity, EditText editText, int astrictSize) {
        this.activity = activity;
        this.editText = editText;
        this.astrictSize = astrictSize;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.toString().length() > astrictSize) {
            activity.showToast("最多输入" + astrictSize + "字！");
            int selEndIndex = Selection.getSelectionEnd(s);
            editText.setText(s.toString().substring(0, astrictSize));
            Editable editable = editText.getText();

            //新字符串的长度
            int newLen = editable.toString().length();
            //旧光标位置超过字符串长度
            if (selEndIndex > newLen) {
                selEndIndex = editable.length();
            }
            //设置新光标所在的位置
            Selection.setSelection(editable, selEndIndex);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
