package com.baseandroid.retrofit;

import android.os.Build;
import android.text.TextUtils;

import com.baseandroid.config.Api;
import com.baseandroid.config.Global;
import com.baseandroid.utils.DeviceUuid;
import com.baseandroid.utils.EncryptUtil;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.haodaibao.fanbeiapp.BuildConfig;
import com.qianbaocard.coresdk.SignUtil;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import okhttp3.Cache;
import okhttp3.Cookie;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;

public class OkHttpClientManager {

    private static ClearableCookieJar sCookieJar;

    private OkHttpClientManager() {

    }

    private static class SingletonHolder {
        private static final OkHttpClient instance = createOkHttpClient();
    }

    public static OkHttpClient getOkHttpClient() {
        return SingletonHolder.instance;
    }

    private static OkHttpClient createOkHttpClient() {

        HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor();
        logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        sCookieJar = new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(Global
                .getContext()));

        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();
        okHttpBuilder.hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        })
                .addInterceptor(headerInterceptor)
                .addInterceptor(logInterceptor)
                .cookieJar(sCookieJar)
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .cache(getOkHttpCache());

        if (BuildConfig.DEBUG) {
            okHttpBuilder.addNetworkInterceptor(new StethoInterceptor());
        }

        return okHttpBuilder.build();
    }

    private static Interceptor headerInterceptor = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request originalRequest = chain.request();

            Request.Builder newRequestBuilder = originalRequest.newBuilder();
            newRequestBuilder.addHeader("Accept-Resthub-Spec", "application/vnd.resthub.v2+json");
            newRequestBuilder.addHeader("Uni-Source", "QBLife/Android (OS/" + Build.VERSION.SDK_INT + "APP/" + BuildConfig.VERSION_NAME + ")");

            String access_token = "";
            if (Global.getUserTokenInfo().getAc_token() != null) {
                access_token = Global.getUserTokenInfo().getAc_token();
            }

            HttpUrl.Builder builderUrl = originalRequest.url()
                    .newBuilder()
                    .addQueryParameter("_ac_token", access_token)
                    .addQueryParameter("_platform", "app")
                    .addQueryParameter("_os", "android")
                    .addQueryParameter("_sysVersion", "" + Build.VERSION.SDK_INT)
                    .addQueryParameter("_appVersion", "" + BuildConfig.VERSION_NAME)
                    .addQueryParameter("_model", "" + "" + Build.MODEL)
                    .addQueryParameter("_caller", "qbsh")
                    .addQueryParameter("_appChannel", "")
                    .addQueryParameter("channel", "02") /*old version channel*/
                    .addQueryParameter("_openUDID", DeviceUuid.getDeviceId(Global.getContext()));

            if (Api.isDevelop) {
                builderUrl.addQueryParameter("&__intern__show-error-mesg", "1");
            }

            String requestUrl = builderUrl.build().toString();
            StringBuilder queryString = new StringBuilder(requestUrl.substring(requestUrl.indexOf("?") + 1));

            StringBuilder postString = new StringBuilder();
            if (RequestMethod.supportBody(originalRequest.method())) {
                FormBody.Builder formbuilder = new FormBody.Builder();
                FormBody formBody = (FormBody) originalRequest.body();
                for (int i = 0; i < formBody.size(); i++) {
                    formbuilder.add(formBody.name(i), formBody.value(i));
                }
                // formbuilder.add("channel", "02");

                formBody = (FormBody) formbuilder.build();
                for (int i = 0; i < formBody.size(); i++) {
                    postString.append(postString.length() > 0 ? "&" : "")
                            .append(formBody.encodedName(i))
                            .append("=")
                            .append(formBody.encodedValue(i) == null ? "" : formBody.encodedValue(i));
                }

                newRequestBuilder.method(originalRequest.method(), formbuilder.build());
            }

            String signvalue = SignUtil.generateSign(Global.getContext(), RequestMethod.supportBody(originalRequest
                    .method()) ? "POST" : "GET", queryString.append("&")
                    .toString(), postString.toString());
            builderUrl.addQueryParameter("_sign", signvalue);

            Request newRequest = newRequestBuilder.url(builderUrl.build()).build();

            Response response = chain.proceed(newRequestBuilder.build());
            String responseEncrypt = TextUtils.isEmpty(response.header("Content-Encrypt")) ? response
                    .header("Content_Encrypt") : response.header("Content-Encrypt");// Content-Encrypt="AES/QBSH"
            if (!TextUtils.isEmpty(responseEncrypt) && "AES/QBSH".equals(responseEncrypt)) {
                MediaType mediaType = response.body().contentType();
                try {
                    String responseContent = response.body().string();
                    responseContent = EncryptUtil.decryptBase64(responseContent);
                    return response.newBuilder()
                            .body(ResponseBody.create(mediaType, responseContent))
                            .build();
                } catch (Exception e) {
                    return response.newBuilder()
                            .body(ResponseBody.create(mediaType, ""))
                            .build();
                }
            }
            return response;
        }
    };

    private static Cache getOkHttpCache() {
        File cacheFile = new File(Global.getContext().getCacheDir(), "OkCache");
        Cache cache = new Cache(cacheFile, 1024 * 1024 * 50);
        return cache;
    }

    public static List<Cookie> getHttpCookieList() {
        return sCookieJar.loadForRequest(HttpUrl.parse(Api.sServerApiUrl));
    }

    public static String getHttpCookieString() {
        StringBuilder stringBuilder = new StringBuilder();
        List<Cookie> cookieList = getHttpCookieList();
        for (Cookie cookie : cookieList) {
            stringBuilder.append(cookie.toString()).append(";");
        }
        if (stringBuilder.length() > 0) {
            int last = stringBuilder.lastIndexOf(";");
            if (stringBuilder.length() - 1 == last) {
                stringBuilder.deleteCharAt(last);
            }
        }

        return stringBuilder.toString();
    }

    public static void clearCookie() {
        sCookieJar.clear();
    }
}
