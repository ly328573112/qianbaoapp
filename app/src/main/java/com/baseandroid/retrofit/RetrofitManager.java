package com.baseandroid.retrofit;

import com.baseandroid.config.Api;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofitManager {

    private static Retrofit.Builder buildBaseRetrofit() {
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().client(OkHttpClientManager
                .getOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create(gson));
        return retrofitBuilder;
    }

    private static Retrofit.Builder buildBaseRetrofit1() {
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().client(new
                OkHttpClient.Builder().build()).addConverterFactory(ScalarsConverterFactory.create());
        return retrofitBuilder;
    }

    public static Retrofit getRxRetrofit() {
        return buildBaseRetrofit().baseUrl(Api.sServerApiUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    public static Retrofit getRxRetrofitLife() {
        return buildBaseRetrofit1().baseUrl(Api.sUpLoadingUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }
}
